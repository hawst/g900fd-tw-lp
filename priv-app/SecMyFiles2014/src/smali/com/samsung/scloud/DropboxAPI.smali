.class public Lcom/samsung/scloud/DropboxAPI;
.super Ljava/lang/Object;
.source "DropboxAPI.java"

# interfaces
.implements Lcom/samsung/scloud/SCloudAPI;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;,
        Lcom/samsung/scloud/DropboxAPI$CancelableUpload;,
        Lcom/samsung/scloud/DropboxAPI$CancelableDownload;,
        Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    }
.end annotation


# static fields
.field private static final MAX_RETRY:I = 0x5

.field private static mInstance:Lcom/samsung/scloud/DropboxAPI;


# instance fields
.field private accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

.field private dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/client2/SamsungDropboxAPI",
            "<+",
            "Lcom/dropbox/client2/session/AbstractSession;",
            ">;"
        }
    .end annotation
.end field

.field private isAndroid:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    .line 93
    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    .line 95
    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->mContext:Landroid/content/Context;

    .line 96
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/scloud/DropboxAPI;->isAndroid:Z

    .line 165
    iput-object p1, p0, Lcom/samsung/scloud/DropboxAPI;->mContext:Landroid/content/Context;

    .line 166
    new-instance v0, Lcom/dropbox/client2/session/AppKeyPair;

    sget-object v2, Lcom/samsung/scloud/auth/dropbox/DropboxConstants;->APP_KEY:Ljava/lang/String;

    sget-object v3, Lcom/samsung/scloud/auth/dropbox/DropboxConstants;->APP_SECRET:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lcom/dropbox/client2/session/AppKeyPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .local v0, "appKeyPair":Lcom/dropbox/client2/session/AppKeyPair;
    new-instance v1, Lcom/dropbox/client2/android/AndroidAuthSession;

    sget-object v2, Lcom/dropbox/client2/session/Session$AccessType;->DROPBOX:Lcom/dropbox/client2/session/Session$AccessType;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/client2/android/AndroidAuthSession;-><init>(Lcom/dropbox/client2/session/AppKeyPair;Lcom/dropbox/client2/session/Session$AccessType;)V

    .line 170
    .local v1, "session":Lcom/dropbox/client2/android/AndroidAuthSession;
    new-instance v2, Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-direct {v2, v1}, Lcom/dropbox/client2/SamsungDropboxAPI;-><init>(Lcom/dropbox/client2/session/Session;)V

    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    .line 172
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "accessTokenSecret"    # Ljava/lang/String;
    .param p3, "isAndroid"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    .line 93
    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    .line 95
    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->mContext:Landroid/content/Context;

    .line 96
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/scloud/DropboxAPI;->isAndroid:Z

    .line 134
    iput-boolean p3, p0, Lcom/samsung/scloud/DropboxAPI;->isAndroid:Z

    .line 135
    new-instance v0, Lcom/dropbox/client2/session/AppKeyPair;

    sget-object v2, Lcom/samsung/scloud/auth/dropbox/DropboxConstants;->APP_KEY:Ljava/lang/String;

    sget-object v3, Lcom/samsung/scloud/auth/dropbox/DropboxConstants;->APP_SECRET:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lcom/dropbox/client2/session/AppKeyPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    .local v0, "appKeys":Lcom/dropbox/client2/session/AppKeyPair;
    if-eqz p3, :cond_0

    .line 141
    new-instance v1, Lcom/dropbox/client2/android/AndroidAuthSession;

    sget-object v2, Lcom/dropbox/client2/session/Session$AccessType;->DROPBOX:Lcom/dropbox/client2/session/Session$AccessType;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/client2/android/AndroidAuthSession;-><init>(Lcom/dropbox/client2/session/AppKeyPair;Lcom/dropbox/client2/session/Session$AccessType;)V

    .line 144
    .local v1, "was":Lcom/dropbox/client2/session/AbstractSession;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->setAuthToken(Ljava/lang/String;)V

    .line 145
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    if-eqz v2, :cond_1

    .line 146
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    invoke-virtual {v1, v2}, Lcom/dropbox/client2/session/AbstractSession;->setAccessTokenPair(Lcom/dropbox/client2/session/AccessTokenPair;)V

    .line 150
    if-eqz p3, :cond_2

    .line 151
    new-instance v2, Lcom/dropbox/client2/SamsungDropboxAPI;

    check-cast v1, Lcom/dropbox/client2/android/AndroidAuthSession;

    .end local v1    # "was":Lcom/dropbox/client2/session/AbstractSession;
    invoke-direct {v2, v1}, Lcom/dropbox/client2/SamsungDropboxAPI;-><init>(Lcom/dropbox/client2/session/Session;)V

    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    .line 155
    :goto_1
    return-void

    .line 143
    :cond_0
    new-instance v1, Lcom/dropbox/client2/session/WebAuthSession;

    sget-object v2, Lcom/dropbox/client2/session/Session$AccessType;->DROPBOX:Lcom/dropbox/client2/session/Session$AccessType;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/client2/session/WebAuthSession;-><init>(Lcom/dropbox/client2/session/AppKeyPair;Lcom/dropbox/client2/session/Session$AccessType;)V

    .restart local v1    # "was":Lcom/dropbox/client2/session/AbstractSession;
    goto :goto_0

    .line 148
    :cond_1
    new-instance v2, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v3, "invalid accessToken pair"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 154
    :cond_2
    new-instance v2, Lcom/dropbox/client2/SamsungDropboxAPI;

    check-cast v1, Lcom/dropbox/client2/session/WebAuthSession;

    .end local v1    # "was":Lcom/dropbox/client2/session/AbstractSession;
    invoke-direct {v2, v1}, Lcom/dropbox/client2/SamsungDropboxAPI;-><init>(Lcom/dropbox/client2/session/Session;)V

    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    goto :goto_1
.end method

.method private DateToLong(Ljava/lang/String;)J
    .locals 6
    .param p1, "dateInString"    # Ljava/lang/String;

    .prologue
    .line 1248
    const-wide/16 v2, -0x1

    .line 1249
    .local v2, "dateInLong":J
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-DD-MM\'T\'HH:MM:SS"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1251
    .local v4, "formatter":Ljava/text/DateFormat;
    :try_start_0
    invoke-virtual {v4, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 1252
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1257
    .end local v0    # "date":Ljava/util/Date;
    :goto_0
    return-wide v2

    .line 1254
    :catch_0
    move-exception v1

    .line 1255
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method private NodeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1690
    const/16 v1, 0x2f

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 1691
    .local v0, "ind":I
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static synthetic access$000(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/scloud/DropboxAPI;->dd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/scloud/DropboxAPI;I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI;
    .param p1, "x1"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/scloud/DropboxAPI;->getDropboxThumbSize(I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/Exception;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/scloud/DropboxAPI;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI;
    .param p1, "x1"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI;
    .param p1, "x1"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/scloud/DropboxAPI;)Lcom/dropbox/client2/SamsungDropboxAPI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/scloud/DropboxAPI;Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI;
    .param p1, "x1"    # Lcom/dropbox/client2/DropboxAPI$Entry;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v0

    return-object v0
.end method

.method private convertExifToDegree(I)I
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 2854
    const/4 v0, 0x0

    .line 2855
    .local v0, "orientationInDegrees":I
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 2856
    const/4 v0, 0x0

    .line 2865
    :cond_0
    :goto_0
    return v0

    .line 2857
    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    .line 2858
    const/16 v0, 0xb4

    goto :goto_0

    .line 2859
    :cond_2
    const/4 v1, 0x6

    if-ne p1, v1, :cond_3

    .line 2860
    const/16 v0, 0x5a

    goto :goto_0

    .line 2861
    :cond_3
    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    .line 2862
    const/16 v0, 0x10e

    goto :goto_0
.end method

.method private dd(Ljava/lang/String;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/String;

    .prologue
    .line 1509
    invoke-direct {p0}, Lcom/samsung/scloud/DropboxAPI;->isShipMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1510
    const-string v0, "SCLOUD_SDK"

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1512
    :cond_0
    return-void
.end method

.method private doBackOff(I)V
    .locals 8
    .param p1, "numTry"    # I

    .prologue
    .line 1539
    if-ltz p1, :cond_0

    const/4 v1, 0x5

    if-le p1, v1, :cond_1

    .line 1548
    :cond_0
    :goto_0
    return-void

    .line 1541
    :cond_1
    const-wide/16 v4, 0x3e8

    const/4 v1, 0x1

    rem-int/lit8 v6, p1, 0x5

    shl-int/2addr v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-long v6, v1

    mul-long v2, v4, v6

    .line 1543
    .local v2, "ms":J
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doBackOff ["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "] sleep(ms)="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->dd(Ljava/lang/String;)V

    .line 1544
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1545
    :catch_0
    move-exception v0

    .line 1546
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private getDropboxThumbSize(I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .locals 3
    .param p1, "thumbnail_type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 608
    const/4 v0, 0x0

    .line 610
    .local v0, "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    if-nez p1, :cond_0

    .line 611
    const/4 v0, 0x0

    .line 631
    :goto_0
    return-object v0

    .line 612
    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 613
    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_1024x768:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    .line 614
    :cond_1
    const/4 v1, 0x5

    if-ne p1, v1, :cond_2

    .line 615
    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_320x240:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    .line 616
    :cond_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    .line 617
    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_480x320:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    .line 618
    :cond_3
    const/4 v1, 0x3

    if-ne p1, v1, :cond_4

    .line 619
    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_640x480:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    .line 620
    :cond_4
    const/4 v1, 0x2

    if-ne p1, v1, :cond_5

    .line 621
    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_960x640:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    .line 622
    :cond_5
    const/4 v1, 0x6

    if-ne p1, v1, :cond_6

    .line 623
    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->ICON_256x256:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    .line 624
    :cond_6
    const/4 v1, 0x7

    if-ne p1, v1, :cond_7

    .line 625
    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->ICON_128x128:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    .line 626
    :cond_7
    const/16 v1, 0x8

    if-ne p1, v1, :cond_8

    .line 627
    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->ICON_64x64:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    .line 629
    :cond_8
    new-instance v1, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v2, "wrong thumbnail size"

    invoke-direct {v1, v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getHiddenThumbnail(Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;
    .locals 10
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "os"    # Ljava/io/OutputStream;
    .param p3, "size"    # Ljava/lang/String;
    .param p4, "format"    # Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    .param p5, "listener"    # Lcom/dropbox/client2/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 657
    const/4 v6, 0x1

    .line 658
    .local v6, "VERSION":I
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v5

    check-cast v5, Lcom/dropbox/client2/session/AbstractSession;

    .line 659
    .local v5, "session":Lcom/dropbox/client2/session/AbstractSession;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Lcom/dropbox/client2/session/AbstractSession;->getAccessType()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 660
    .local v2, "target":Ljava/lang/String;
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "size"

    aput-object v1, v4, v0

    aput-object p3, v4, v3

    const/4 v0, 0x2

    const-string v1, "format"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-virtual {p4}, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v1, 0x5

    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/session/AbstractSession;

    invoke-virtual {v0}, Lcom/dropbox/client2/session/AbstractSession;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 663
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    invoke-virtual {v5}, Lcom/dropbox/client2/session/AbstractSession;->getContentServer()Ljava/lang/String;

    move-result-object v1

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->streamRequest(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;

    move-result-object v8

    .line 665
    .local v8, "rr":Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;
    new-instance v9, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    iget-object v0, v8, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->request:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v1, v8, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->response:Lorg/apache/http/HttpResponse;

    invoke-direct {v9, v0, v1}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    .line 667
    .local v9, "thumb":Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;
    invoke-virtual {v9, p2, p5}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->copyStreamToOutput(Ljava/io/OutputStream;Lcom/dropbox/client2/ProgressListener;)V

    .line 668
    invoke-virtual {v9}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->getFileInfo()Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    move-result-object v7

    .line 669
    .local v7, "info":Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;
    invoke-virtual {v9}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->close()V

    .line 671
    return-object v7
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/scloud/DropboxAPI;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 158
    const-class v1, Lcom/samsung/scloud/DropboxAPI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/scloud/DropboxAPI;->mInstance:Lcom/samsung/scloud/DropboxAPI;

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lcom/samsung/scloud/DropboxAPI;->mInstance:Lcom/samsung/scloud/DropboxAPI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :goto_0
    monitor-exit v1

    return-object v0

    .line 160
    :cond_0
    :try_start_1
    new-instance v0, Lcom/samsung/scloud/DropboxAPI;

    invoke-direct {v0, p0}, Lcom/samsung/scloud/DropboxAPI;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/scloud/DropboxAPI;->mInstance:Lcom/samsung/scloud/DropboxAPI;

    .line 161
    sget-object v0, Lcom/samsung/scloud/DropboxAPI;->mInstance:Lcom/samsung/scloud/DropboxAPI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getMediaType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 1695
    const/4 v1, 0x0

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1696
    .local v0, "type":Ljava/lang/String;
    return-object v0
.end method

.method private getSamsungHiddenThumbnailStream(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
    .locals 8
    .param p1, "serverFilePath"    # Ljava/lang/String;
    .param p2, "size"    # Ljava/lang/String;
    .param p3, "format"    # Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 778
    const/4 v6, 0x1

    .line 780
    .local v6, "VERSION":I
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v5

    check-cast v5, Lcom/dropbox/client2/session/AbstractSession;

    .line 781
    .local v5, "session":Lcom/dropbox/client2/session/AbstractSession;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/samsung_thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Lcom/dropbox/client2/session/AbstractSession;->getAccessType()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 782
    .local v2, "target":Ljava/lang/String;
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "size"

    aput-object v1, v4, v0

    aput-object p2, v4, v3

    const/4 v0, 0x2

    const-string v1, "format"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-virtual {p3}, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    invoke-virtual {v5}, Lcom/dropbox/client2/session/AbstractSession;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 786
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    invoke-virtual {v5}, Lcom/dropbox/client2/session/AbstractSession;->getContentServer()Ljava/lang/String;

    move-result-object v1

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->streamRequest(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;

    move-result-object v7

    .line 789
    .local v7, "rr":Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;

    iget-object v1, v7, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->request:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v3, v7, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->response:Lorg/apache/http/HttpResponse;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v0
.end method

.method private handleDropboxExceptions(Ljava/lang/Exception;)V
    .locals 5
    .param p1, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1579
    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    .line 1581
    .local v0, "callerMethodName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleDropboxException of [callerMethodName] - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->dd(Ljava/lang/String;)V

    .line 1582
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxUnlinkedException;

    if-eqz v2, :cond_0

    .line 1583
    new-instance v2, Lcom/samsung/scloud/exception/SCloudAuthException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>()V

    throw v2

    .line 1584
    :cond_0
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxServerException;

    if-eqz v2, :cond_1

    move-object v2, p1

    .line 1585
    check-cast v2, Lcom/dropbox/client2/exception/DropboxServerException;

    iget v1, v2, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    .line 1586
    .local v1, "errno":I
    sparse-switch v1, :sswitch_data_0

    .line 1606
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1588
    :sswitch_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "server file not exists["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1591
    :sswitch_1
    new-instance v2, Lcom/samsung/scloud/exception/SCloudAuthException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1594
    :sswitch_2
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1597
    :sswitch_3
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1600
    :sswitch_4
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1603
    :sswitch_5
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1608
    .end local v1    # "errno":I
    :cond_1
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxIOException;

    if-eqz v2, :cond_2

    .line 1609
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1611
    :cond_2
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxFileSizeException;

    if-eqz v2, :cond_3

    .line 1612
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1614
    :cond_3
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxLocalStorageFullException;

    if-eqz v2, :cond_4

    .line 1615
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1617
    :cond_4
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxParseException;

    if-eqz v2, :cond_5

    .line 1618
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1620
    :cond_5
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxSSLException;

    if-eqz v2, :cond_6

    .line 1621
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1623
    :cond_6
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxPartialFileException;

    if-eqz v2, :cond_7

    .line 1624
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1627
    :cond_7
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1586
    :sswitch_data_0
    .sparse-switch
        0x193 -> :sswitch_1
        0x194 -> :sswitch_0
        0x1f4 -> :sswitch_3
        0x1f6 -> :sswitch_4
        0x1f7 -> :sswitch_5
        0x1fb -> :sswitch_2
    .end sparse-switch
.end method

.method private isRetryNeeded(Ljava/lang/Exception;)Z
    .locals 3
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    const/4 v1, 0x1

    .line 1552
    const/4 v0, 0x0

    .line 1553
    .local v0, "value":Z
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxServerException;

    if-eqz v2, :cond_2

    .line 1554
    check-cast p1, Lcom/dropbox/client2/exception/DropboxServerException;

    .end local p1    # "e":Ljava/lang/Exception;
    iget v1, p1, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v1, v0

    .line 1573
    :cond_1
    :goto_1
    return v1

    .line 1558
    :pswitch_1
    const/4 v0, 0x1

    .line 1559
    goto :goto_0

    .line 1563
    .restart local p1    # "e":Ljava/lang/Exception;
    :cond_2
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxSSLException;

    if-nez v2, :cond_1

    .line 1565
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxPartialFileException;

    if-nez v2, :cond_1

    .line 1567
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxIOException;

    if-eqz v2, :cond_0

    goto :goto_1

    .line 1554
    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private isShipMode()Z
    .locals 7

    .prologue
    .line 1515
    const/4 v3, 0x0

    .line 1517
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 1519
    .local v1, "method":Ljava/lang/reflect/Method;
    :try_start_0
    const-class v4, Landroid/os/Debug;

    const-string v5, "isProductShip"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1520
    const/4 v4, 0x0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    .line 1522
    .local v2, "mode":I
    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    .line 1523
    const/4 v3, 0x1

    .line 1535
    .end local v2    # "mode":I
    :cond_0
    :goto_0
    return v3

    .line 1525
    :catch_0
    move-exception v0

    .line 1526
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 1527
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 1528
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1529
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 1530
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 1531
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 1532
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private parseAlbum(Lcom/dropbox/client2/SamsungDropboxAPI$Album;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 8
    .param p1, "album"    # Lcom/dropbox/client2/SamsungDropboxAPI$Album;

    .prologue
    .line 1727
    if-nez p1, :cond_0

    .line 1728
    const/4 v3, 0x0

    .line 1742
    :goto_0
    return-object v3

    .line 1729
    :cond_0
    new-instance v3, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-direct {v3}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 1730
    .local v3, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1731
    .local v2, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    iget-object v5, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->contents:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/DropboxAPI$Entry;

    .line 1732
    .local v0, "entry":Lcom/dropbox/client2/DropboxAPI$Entry;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v1

    check-cast v1, Lcom/samsung/scloud/data/SCloudFile;

    .line 1733
    .local v1, "file":Lcom/samsung/scloud/data/SCloudFile;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1735
    .end local v0    # "entry":Lcom/dropbox/client2/DropboxAPI$Entry;
    .end local v1    # "file":Lcom/samsung/scloud/data/SCloudFile;
    :cond_1
    iget-object v5, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    iget-object v5, v5, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/samsung/scloud/data/SCloudFolder;->setName(Ljava/lang/String;)V

    .line 1736
    iget-object v5, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    iget-object v5, v5, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->id:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/samsung/scloud/data/SCloudFolder;->setId(Ljava/lang/String;)V

    .line 1737
    iget-object v5, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    iget-wide v6, v5, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->count:J

    invoke-virtual {v3, v6, v7}, Lcom/samsung/scloud/data/SCloudFolder;->setFileCount(J)V

    .line 1738
    iget-object v5, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    iget-object v5, v5, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->modified:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/samsung/scloud/data/SCloudFolder;->setUpdatedTimestamp(J)V

    .line 1739
    iget-object v5, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    iget-object v5, v5, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->cover:Lcom/dropbox/client2/DropboxAPI$Entry;

    invoke-direct {p0, v5}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    check-cast v5, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v3, v5}, Lcom/samsung/scloud/data/SCloudFolder;->setCover(Lcom/samsung/scloud/data/SCloudFile;)V

    .line 1740
    invoke-virtual {v3, v2}, Lcom/samsung/scloud/data/SCloudFolder;->setFiles(Ljava/util/ArrayList;)V

    .line 1741
    iget-object v5, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->rev:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/samsung/scloud/data/SCloudFolder;->setRevision(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private parseAlbumEntry(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;)Lcom/samsung/scloud/data/SCloudNode;
    .locals 10
    .param p1, "metadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;

    .prologue
    .line 1105
    new-instance v5, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v5}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1106
    .local v5, "ret":Lcom/samsung/scloud/data/SCloudNode;
    const/4 v2, 0x0

    .line 1107
    .local v2, "albums":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/data/AlbumData;>;"
    const/4 v0, 0x0

    .line 1108
    .local v0, "albumData":Lcom/samsung/scloud/data/AlbumData;
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    if-eqz v7, :cond_0

    .line 1109
    sget-object v7, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FILE:Ljava/lang/String;

    invoke-virtual {v5, v7}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    move-object v7, v5

    .line 1110
    check-cast v7, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v8, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v8, v8, Lcom/dropbox/client2/DropboxAPI$Entry;->hash:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudFile;->setMd5Checksum(Ljava/lang/String;)V

    move-object v7, v5

    .line 1111
    check-cast v7, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v8, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v8, v8, Lcom/dropbox/client2/DropboxAPI$Entry;->mimeType:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudFile;->setMimeType(Ljava/lang/String;)V

    move-object v7, v5

    .line 1113
    check-cast v7, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v8, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v8, v8, Lcom/dropbox/client2/DropboxAPI$Entry;->mimeType:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/scloud/DropboxAPI;->getMediaType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudFile;->setMediaType(Ljava/lang/String;)V

    .line 1115
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v7, v7, Lcom/dropbox/client2/DropboxAPI$Entry;->clientMtime:Ljava/lang/String;

    invoke-static {v7}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/samsung/scloud/data/SCloudNode;->setCreatedTimestamp(J)V

    move-object v7, v5

    .line 1117
    check-cast v7, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v8, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v8, v8, Lcom/dropbox/client2/DropboxAPI$Entry;->icon:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudFile;->setIconName(Ljava/lang/String;)V

    move-object v7, v5

    .line 1118
    check-cast v7, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v8, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v8, v8, Lcom/dropbox/client2/DropboxAPI$Entry;->root:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudFile;->setRootName(Ljava/lang/String;)V

    .line 1120
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v7, v7, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-virtual {v5, v7}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1121
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-wide v8, v7, Lcom/dropbox/client2/DropboxAPI$Entry;->bytes:J

    invoke-virtual {v5, v8, v9}, Lcom/samsung/scloud/data/SCloudNode;->setSize(J)V

    .line 1122
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-boolean v7, v7, Lcom/dropbox/client2/DropboxAPI$Entry;->isDeleted:Z

    invoke-virtual {v5, v7}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1123
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v7, v7, Lcom/dropbox/client2/DropboxAPI$Entry;->modified:Ljava/lang/String;

    invoke-static {v7}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/samsung/scloud/data/SCloudNode;->setUpdatedTimestamp(J)V

    .line 1125
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v7, v7, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-virtual {v5, v7}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1126
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v7, v7, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/samsung/scloud/DropboxAPI;->NodeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/samsung/scloud/data/SCloudNode;->setName(Ljava/lang/String;)V

    .line 1127
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-boolean v7, v7, Lcom/dropbox/client2/DropboxAPI$Entry;->thumbExists:Z

    invoke-virtual {v5, v7}, Lcom/samsung/scloud/data/SCloudNode;->setThumbExists(Z)V

    .line 1130
    :cond_0
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->albums:Ljava/util/List;

    if-eqz v7, :cond_2

    .line 1131
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "albums":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/data/AlbumData;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1132
    .restart local v2    # "albums":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/data/AlbumData;>;"
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->albums:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;

    .line 1133
    .local v1, "albumEntry":Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;
    if-eqz v1, :cond_1

    .line 1134
    new-instance v0, Lcom/samsung/scloud/data/AlbumData;

    .end local v0    # "albumData":Lcom/samsung/scloud/data/AlbumData;
    invoke-direct {v0}, Lcom/samsung/scloud/data/AlbumData;-><init>()V

    .line 1135
    .restart local v0    # "albumData":Lcom/samsung/scloud/data/AlbumData;
    iget-object v7, v1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;->id:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/samsung/scloud/data/AlbumData;->setId(Ljava/lang/String;)V

    .line 1136
    iget-object v7, v1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;->name:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/samsung/scloud/data/AlbumData;->setName(Ljava/lang/String;)V

    .line 1138
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1141
    .end local v1    # "albumEntry":Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    if-eqz v7, :cond_a

    .line 1142
    new-instance v4, Lcom/samsung/scloud/data/PhotoMetaData;

    invoke-direct {v4}, Lcom/samsung/scloud/data/PhotoMetaData;-><init>()V

    .line 1143
    .local v4, "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->width:Ljava/lang/Integer;

    if-eqz v7, :cond_3

    .line 1144
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->width:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/PhotoMetaData;->setWidth(I)V

    .line 1145
    :cond_3
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->height:Ljava/lang/Integer;

    if-eqz v7, :cond_4

    .line 1146
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->height:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/PhotoMetaData;->setHeight(I)V

    .line 1147
    :cond_4
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->orientation:Ljava/lang/Integer;

    if-eqz v7, :cond_5

    .line 1148
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->orientation:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/PhotoMetaData;->setOrientation(I)V

    .line 1149
    :cond_5
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->latitude:Ljava/lang/Double;

    if-eqz v7, :cond_6

    .line 1150
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->latitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/PhotoMetaData;->setLatitude(Ljava/lang/String;)V

    .line 1151
    :cond_6
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->longitude:Ljava/lang/Double;

    if-eqz v7, :cond_7

    .line 1152
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->longitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/PhotoMetaData;->setLongitude(Ljava/lang/String;)V

    .line 1153
    :cond_7
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->dateTaken:Ljava/lang/String;

    if-eqz v7, :cond_8

    .line 1154
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->dateTaken:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/samsung/scloud/DropboxAPI;->DateToLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lcom/samsung/scloud/data/PhotoMetaData;->setDateTaken(J)V

    .line 1155
    :cond_8
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->albums:Ljava/util/List;

    if-eqz v7, :cond_9

    .line 1156
    invoke-virtual {v4, v2}, Lcom/samsung/scloud/data/PhotoMetaData;->setAlbums(Ljava/util/List;)V

    :cond_9
    move-object v7, v5

    .line 1158
    check-cast v7, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v7, v4}, Lcom/samsung/scloud/data/SCloudFile;->setPhotoMetaData(Lcom/samsung/scloud/data/PhotoMetaData;)V

    .line 1160
    .end local v4    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :cond_a
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    if-eqz v7, :cond_10

    .line 1161
    new-instance v6, Lcom/samsung/scloud/data/VideoMetaData;

    invoke-direct {v6}, Lcom/samsung/scloud/data/VideoMetaData;-><init>()V

    .line 1162
    .local v6, "videoMeta":Lcom/samsung/scloud/data/VideoMetaData;
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->width:Ljava/lang/Integer;

    if-eqz v7, :cond_b

    .line 1163
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->width:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/scloud/data/VideoMetaData;->setWidth(I)V

    .line 1164
    :cond_b
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->height:Ljava/lang/Integer;

    if-eqz v7, :cond_c

    .line 1165
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->height:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/scloud/data/VideoMetaData;->setHeight(I)V

    .line 1166
    :cond_c
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->duration:Ljava/lang/Integer;

    if-eqz v7, :cond_d

    .line 1167
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->duration:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Lcom/samsung/scloud/data/VideoMetaData;->setDuration(J)V

    .line 1168
    :cond_d
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->bitrate:Ljava/lang/Integer;

    if-eqz v7, :cond_e

    .line 1169
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    iget-object v7, v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->bitrate:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Lcom/samsung/scloud/data/VideoMetaData;->setDuration(J)V

    .line 1170
    :cond_e
    iget-object v7, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->albums:Ljava/util/List;

    if-eqz v7, :cond_f

    .line 1171
    invoke-virtual {v6, v2}, Lcom/samsung/scloud/data/VideoMetaData;->setAlbums(Ljava/util/List;)V

    :cond_f
    move-object v7, v5

    .line 1173
    check-cast v7, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v7, v6}, Lcom/samsung/scloud/data/SCloudFile;->setVideoMetaData(Lcom/samsung/scloud/data/VideoMetaData;)V

    .line 1175
    .end local v6    # "videoMeta":Lcom/samsung/scloud/data/VideoMetaData;
    :cond_10
    return-object v5
.end method

.method private parseAlbumMetadata(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 4
    .param p1, "metadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    .prologue
    .line 1708
    if-nez p1, :cond_0

    .line 1709
    const/4 v0, 0x0

    .line 1716
    :goto_0
    return-object v0

    .line 1710
    :cond_0
    new-instance v0, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-direct {v0}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 1711
    .local v0, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    iget-object v1, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudFolder;->setName(Ljava/lang/String;)V

    .line 1712
    iget-object v1, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudFolder;->setId(Ljava/lang/String;)V

    .line 1713
    iget-wide v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->count:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/data/SCloudFolder;->setFileCount(J)V

    .line 1714
    iget-object v1, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->modified:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/data/SCloudFolder;->setUpdatedTimestamp(J)V

    .line 1715
    iget-object v1, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->cover:Lcom/dropbox/client2/DropboxAPI$Entry;

    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v1

    check-cast v1, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudFolder;->setCover(Lcom/samsung/scloud/data/SCloudFile;)V

    goto :goto_0
.end method

.method private parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;
    .locals 4
    .param p1, "e"    # Lcom/dropbox/client2/DropboxAPI$Entry;

    .prologue
    .line 1634
    iget-boolean v1, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->isDir:Z

    if-eqz v1, :cond_0

    .line 1635
    new-instance v0, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-direct {v0}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 1636
    .local v0, "ret":Lcom/samsung/scloud/data/SCloudNode;
    sget-object v1, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    .line 1647
    :goto_0
    iget-object v1, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1648
    iget-wide v2, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->bytes:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setSize(J)V

    .line 1649
    iget-boolean v1, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->isDeleted:Z

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1650
    iget-object v1, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->modified:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setUpdatedTimestamp(J)V

    .line 1651
    iget-object v1, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1652
    iget-object v1, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->NodeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudNode;->setName(Ljava/lang/String;)V

    .line 1653
    iget-boolean v1, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->thumbExists:Z

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudNode;->setThumbExists(Z)V

    .line 1654
    return-object v0

    .line 1638
    .end local v0    # "ret":Lcom/samsung/scloud/data/SCloudNode;
    :cond_0
    new-instance v0, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v0}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1639
    .restart local v0    # "ret":Lcom/samsung/scloud/data/SCloudNode;
    sget-object v1, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FILE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    move-object v1, v0

    .line 1640
    check-cast v1, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v2, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->mimeType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudFile;->setMimeType(Ljava/lang/String;)V

    move-object v1, v0

    .line 1641
    check-cast v1, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v2, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->hash:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudFile;->setMd5Checksum(Ljava/lang/String;)V

    move-object v1, v0

    .line 1642
    check-cast v1, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v2, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->mimeType:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->getMediaType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudFile;->setMediaType(Ljava/lang/String;)V

    .line 1643
    iget-object v1, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->clientMtime:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setCreatedTimestamp(J)V

    move-object v1, v0

    .line 1645
    check-cast v1, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v2, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->rev:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudFile;->setParentRev(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private parseFolderMetaData(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 8
    .param p1, "e"    # Lcom/dropbox/client2/DropboxAPI$Entry;

    .prologue
    .line 1659
    iget-boolean v5, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->isDir:Z

    if-nez v5, :cond_0

    .line 1660
    const/4 v4, 0x0

    .line 1682
    :goto_0
    return-object v4

    .line 1663
    :cond_0
    new-instance v4, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 1665
    .local v4, "ret":Lcom/samsung/scloud/data/SCloudFolder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1666
    .local v1, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1668
    .local v2, "folders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    sget-object v5, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FOLDER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudFolder;->setNodeType(Ljava/lang/String;)V

    .line 1669
    iget-object v5, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudFolder;->setAbsolutePath(Ljava/lang/String;)V

    .line 1671
    iget-object v5, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->contents:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/DropboxAPI$Entry;

    .line 1672
    .local v0, "entry":Lcom/dropbox/client2/DropboxAPI$Entry;
    iget-boolean v5, v0, Lcom/dropbox/client2/DropboxAPI$Entry;->isDir:Z

    if-eqz v5, :cond_1

    .line 1673
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    check-cast v5, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1675
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    check-cast v5, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1678
    .end local v0    # "entry":Lcom/dropbox/client2/DropboxAPI$Entry;
    :cond_2
    iget-object v5, p1, Lcom/dropbox/client2/DropboxAPI$Entry;->contents:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Lcom/samsung/scloud/data/SCloudFolder;->setFileCount(J)V

    .line 1679
    invoke-virtual {v4, v1}, Lcom/samsung/scloud/data/SCloudFolder;->setFiles(Ljava/util/ArrayList;)V

    .line 1680
    invoke-virtual {v4, v2}, Lcom/samsung/scloud/data/SCloudFolder;->setFolders(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private parseMusicEntry(Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;)Lcom/samsung/scloud/data/SCloudNode;
    .locals 6
    .param p1, "metadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;

    .prologue
    .line 992
    new-instance v1, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v1}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 993
    .local v1, "ret":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    if-eqz v2, :cond_0

    .line 994
    sget-object v2, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FILE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    move-object v2, v1

    .line 995
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v3, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v3, v3, Lcom/dropbox/client2/DropboxAPI$Entry;->hash:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setMd5Checksum(Ljava/lang/String;)V

    move-object v2, v1

    .line 996
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v3, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v3, v3, Lcom/dropbox/client2/DropboxAPI$Entry;->mimeType:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/samsung/scloud/DropboxAPI;->getMediaType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setMediaType(Ljava/lang/String;)V

    move-object v2, v1

    .line 998
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v3, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v3, v3, Lcom/dropbox/client2/DropboxAPI$Entry;->mimeType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setMimeType(Ljava/lang/String;)V

    .line 1000
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v2, v2, Lcom/dropbox/client2/DropboxAPI$Entry;->clientMtime:Ljava/lang/String;

    invoke-static {v2}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setCreatedTimestamp(J)V

    move-object v2, v1

    .line 1003
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v3, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v3, v3, Lcom/dropbox/client2/DropboxAPI$Entry;->clientMtime:Ljava/lang/String;

    invoke-static {v3}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/scloud/data/SCloudFile;->setCreatedTimestamp(J)V

    move-object v2, v1

    .line 1005
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v3, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v3, v3, Lcom/dropbox/client2/DropboxAPI$Entry;->icon:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setIconName(Ljava/lang/String;)V

    move-object v2, v1

    .line 1006
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    iget-object v3, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v3, v3, Lcom/dropbox/client2/DropboxAPI$Entry;->root:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/data/SCloudFile;->setRootName(Ljava/lang/String;)V

    .line 1008
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v2, v2, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1009
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-wide v2, v2, Lcom/dropbox/client2/DropboxAPI$Entry;->bytes:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setSize(J)V

    .line 1010
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-boolean v2, v2, Lcom/dropbox/client2/DropboxAPI$Entry;->isDeleted:Z

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1011
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v2, v2, Lcom/dropbox/client2/DropboxAPI$Entry;->modified:Ljava/lang/String;

    invoke-static {v2}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/data/SCloudNode;->setUpdatedTimestamp(J)V

    .line 1013
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v2, v2, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1014
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-object v2, v2, Lcom/dropbox/client2/DropboxAPI$Entry;->path:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->NodeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setName(Ljava/lang/String;)V

    .line 1015
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    iget-boolean v2, v2, Lcom/dropbox/client2/DropboxAPI$Entry;->thumbExists:Z

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/data/SCloudNode;->setThumbExists(Z)V

    .line 1018
    :cond_0
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    if-eqz v2, :cond_7

    .line 1019
    new-instance v0, Lcom/samsung/scloud/data/AudioMetaData;

    invoke-direct {v0}, Lcom/samsung/scloud/data/AudioMetaData;-><init>()V

    .line 1020
    .local v0, "audioMetaData":Lcom/samsung/scloud/data/AudioMetaData;
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->album:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1021
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->album:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/data/AudioMetaData;->setAlbumTitle(Ljava/lang/String;)V

    .line 1022
    :cond_1
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->artist:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1023
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->artist:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/data/AudioMetaData;->setArtistName(Ljava/lang/String;)V

    .line 1024
    :cond_2
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->duration:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 1025
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->duration:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/data/AudioMetaData;->setDuration(J)V

    .line 1026
    :cond_3
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->genre:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 1027
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->genre:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/data/AudioMetaData;->setGenre(Ljava/lang/String;)V

    .line 1028
    :cond_4
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->title:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 1029
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->title:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/data/AudioMetaData;->setTrackTitle(Ljava/lang/String;)V

    .line 1031
    :cond_5
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->track:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 1032
    iget-object v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    iget-object v2, v2, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->track:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/data/AudioMetaData;->setTrackNumber(J)V

    :cond_6
    move-object v2, v1

    .line 1034
    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {v2, v0}, Lcom/samsung/scloud/data/SCloudFile;->setAudioMetaData(Lcom/samsung/scloud/data/AudioMetaData;)V

    .line 1037
    .end local v0    # "audioMetaData":Lcom/samsung/scloud/data/AudioMetaData;
    :cond_7
    return-object v1
.end method

.method private putNewFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 14
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1784
    const/4 v5, 0x0

    .line 1785
    .local v5, "fis":Ljava/io/FileInputStream;
    const/4 v13, 0x0

    .line 1786
    .local v13, "uploadedFile":Lcom/dropbox/client2/DropboxAPI$Entry;
    const/4 v12, 0x0

    .local v12, "i":I
    move-object v11, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v11, "fis":Ljava/io/FileInputStream;
    :goto_0
    const/4 v3, 0x5

    if-ge v12, v3, :cond_4

    .line 1788
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1789
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :try_start_1
    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x0

    new-instance v9, Lcom/samsung/scloud/DropboxAPI$2;

    move-object/from16 v0, p4

    invoke-direct {v9, p0, p1, v0}, Lcom/samsung/scloud/DropboxAPI$2;-><init>(Lcom/samsung/scloud/DropboxAPI;Ljava/io/File;Lcom/samsung/scloud/response/ProgressListener;)V

    invoke-virtual/range {v3 .. v9}, Lcom/dropbox/client2/SamsungDropboxAPI;->putFile(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$Entry;

    move-result-object v13

    .line 1807
    if-eqz p4, :cond_0

    .line 1808
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const-wide/16 v6, 0x64

    move-object/from16 v0, p4

    invoke-interface {v0, v3, v4, v6, v7}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 1810
    :cond_0
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1827
    :goto_1
    if-eqz v13, :cond_3

    .line 1828
    invoke-direct {p0, v13}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v3

    check-cast v3, Lcom/samsung/scloud/data/SCloudFile;

    .line 1830
    :goto_2
    return-object v3

    .line 1812
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v2

    move-object v5, v11

    .line 1814
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/lang/Exception;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :goto_3
    if-eqz v5, :cond_1

    .line 1815
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1820
    :cond_1
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x4

    if-ge v12, v3, :cond_2

    .line 1821
    invoke-direct {p0, v12}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1786
    :goto_4
    add-int/lit8 v12, v12, 0x1

    move-object v11, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 1816
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v10

    .line 1817
    .local v10, "e1":Ljava/io/IOException;
    new-instance v3, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v4, "file close failed"

    invoke-direct {v3, v4}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1824
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_2
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_4

    .line 1830
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 1812
    :catch_2
    move-exception v2

    goto :goto_3

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :cond_4
    move-object v5, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method


# virtual methods
.method public addComment(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2406
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public addPushTrigger(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "hook_type"    # Lcom/dropbox/client2/SamsungDropboxAPI$Hook;
    .param p2, "deltaCursor"    # Ljava/lang/String;
    .param p3, "registrationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1460
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 1462
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v2, p1, p2, p3}, Lcom/dropbox/client2/SamsungDropboxAPI;->addTrigger(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1471
    :goto_1
    return-object v2

    .line 1463
    :catch_0
    move-exception v0

    .line 1464
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 1465
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1460
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1468
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 1471
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public copy(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 4
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1935
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 1937
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 1939
    :cond_1
    const/4 v1, 0x0

    .line 1941
    .local v1, "fileData":Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v2, p1, p2}, Lcom/dropbox/client2/SamsungDropboxAPI;->copy(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1945
    :goto_0
    if-eqz v1, :cond_2

    .line 1946
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    .line 1948
    :goto_1
    return-object v2

    .line 1942
    :catch_0
    move-exception v0

    .line 1943
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1948
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public createRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repo"    # Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2328
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public createRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .param p2, "spaceAmount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2320
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public createShareURL(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/data/ShareInfo;
    .locals 6
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "endTimestamp"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2064
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 2065
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 2066
    :cond_1
    const/4 v2, 0x0

    .line 2067
    .local v2, "link":Lcom/dropbox/client2/DropboxAPI$DropboxLink;
    const/4 v3, 0x0

    .line 2068
    .local v3, "shareInfo":Lcom/samsung/scloud/data/ShareInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x5

    if-ge v1, v4, :cond_2

    .line 2070
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v4, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->share(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DropboxLink;
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2084
    :cond_2
    if-eqz v2, :cond_3

    .line 2085
    new-instance v3, Lcom/samsung/scloud/data/ShareInfo;

    .end local v3    # "shareInfo":Lcom/samsung/scloud/data/ShareInfo;
    invoke-direct {v3}, Lcom/samsung/scloud/data/ShareInfo;-><init>()V

    .line 2086
    .restart local v3    # "shareInfo":Lcom/samsung/scloud/data/ShareInfo;
    iget-object v4, v2, Lcom/dropbox/client2/DropboxAPI$DropboxLink;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/ShareInfo;->setShareURL(Ljava/lang/String;)V

    .line 2087
    iget-object v4, v2, Lcom/dropbox/client2/DropboxAPI$DropboxLink;->expires:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/ShareInfo;->setExpireTimestamp(J)V

    .line 2089
    :cond_3
    return-object v3

    .line 2072
    :catch_0
    move-exception v0

    .line 2077
    .local v0, "e":Lcom/dropbox/client2/exception/DropboxException;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    if-ge v1, v4, :cond_4

    .line 2078
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 2068
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2081
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public delete(Ljava/lang/String;)Z
    .locals 4
    .param p1, "fileOrFolderPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 430
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v2, :cond_1

    .line 431
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 432
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_3

    .line 434
    :try_start_0
    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v3, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->delete(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 444
    :goto_1
    return v2

    .line 436
    :catch_0
    move-exception v0

    .line 437
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x4

    if-ge v1, v3, :cond_2

    .line 438
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 432
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 441
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 444
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public deleteDeviceProfile()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2311
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public deleteRepository(Ljava/lang/String;)V
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2379
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public editComment(Ljava/lang/String;JLjava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "commentID"    # J
    .param p4, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2397
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public finishAuth()V
    .locals 1

    .prologue
    .line 1414
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/android/AndroidAuthSession;

    invoke-virtual {v0}, Lcom/dropbox/client2/android/AndroidAuthSession;->finishAuthentication()Ljava/lang/String;

    .line 1415
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/session/AbstractSession;

    invoke-virtual {v0}, Lcom/dropbox/client2/session/AbstractSession;->getAccessTokenPair()Lcom/dropbox/client2/session/AccessTokenPair;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    .line 1416
    return-void
.end method

.method public getAlbumInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 6
    .param p1, "folderIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 257
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 258
    :cond_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3

    .line 259
    :cond_1
    const/4 v0, 0x0

    .line 260
    .local v0, "albumData":Lcom/dropbox/client2/SamsungDropboxAPI$Album;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v4, 0x5

    if-ge v2, v4, :cond_2

    .line 262
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Lcom/dropbox/client2/SamsungDropboxAPI;->album(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/SamsungDropboxAPI$Album;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 272
    :cond_2
    if-nez v0, :cond_4

    .line 274
    :goto_1
    return-object v3

    .line 264
    :catch_0
    move-exception v1

    .line 265
    .local v1, "e":Ljava/lang/Exception;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    if-ge v2, v4, :cond_3

    .line 266
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 260
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 269
    :cond_3
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 274
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    iget-object v3, v0, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    invoke-direct {p0, v3}, Lcom/samsung/scloud/DropboxAPI;->parseAlbumMetadata(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v3

    goto :goto_1
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1373
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    if-nez v0, :cond_0

    .line 1374
    const/4 v0, 0x0

    .line 1375
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    iget-object v1, v1, Lcom/dropbox/client2/session/AccessTokenPair;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    iget-object v1, v1, Lcom/dropbox/client2/session/AccessTokenPair;->secret:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCollaboration(Ljava/lang/String;)Lcom/samsung/scloud/data/Collaboration;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2441
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getCollaborations()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaboration;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2458
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getComments(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Comment;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2388
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 9
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1276
    const/4 v6, 0x0

    .line 1277
    .local v6, "page":Lcom/dropbox/client2/DropboxAPI$DeltaPage;, "Lcom/dropbox/client2/DropboxAPI$DeltaPage<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 1278
    .local v4, "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1279
    .local v5, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v8, 0x5

    if-ge v2, v8, :cond_0

    .line 1281
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v8, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->delta(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1295
    :cond_0
    if-eqz v6, :cond_5

    .line 1296
    iget-object v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->cursor:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setCursor(Ljava/lang/String;)V

    .line 1297
    iget-boolean v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->hasMore:Z

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setHasMore(Z)V

    .line 1298
    iget-boolean v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->reset:Z

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setReset(Z)V

    .line 1299
    const/4 v7, 0x0

    .line 1300
    .local v7, "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->entries:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;

    .line 1301
    .local v1, "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    if-eqz v1, :cond_1

    .line 1302
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    if-nez v8, :cond_3

    .line 1308
    new-instance v7, Lcom/samsung/scloud/data/SCloudFile;

    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    invoke-direct {v7}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1309
    .restart local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->lcPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1310
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->lcPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1311
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1316
    :cond_1
    :goto_2
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1283
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :catch_0
    move-exception v0

    .line 1288
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x4

    if-ge v2, v8, :cond_2

    .line 1289
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1279
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1292
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_3

    .line 1312
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_3
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    if-eqz v8, :cond_1

    .line 1313
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    check-cast v8, Lcom/dropbox/client2/DropboxAPI$Entry;

    invoke-direct {p0, v8}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v7

    goto :goto_2

    .line 1318
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    :cond_4
    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudNodeList;->setNodes(Ljava/util/List;)V

    .line 1320
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_5
    return-object v4
.end method

.method public getDeviceProfile()Lcom/samsung/scloud/data/Device;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2278
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDeviceProfile(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2286
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDownloadURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1332
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 1333
    :cond_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3

    .line 1334
    :cond_1
    const/4 v2, 0x0

    .line 1335
    .local v2, "mediaLink":Lcom/dropbox/client2/DropboxAPI$DropboxLink;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_2

    .line 1337
    :try_start_0
    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lcom/dropbox/client2/SamsungDropboxAPI;->media(Ljava/lang/String;Z)Lcom/dropbox/client2/DropboxAPI$DropboxLink;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1347
    :cond_2
    if-eqz v2, :cond_4

    .line 1348
    iget-object v3, v2, Lcom/dropbox/client2/DropboxAPI$DropboxLink;->url:Ljava/lang/String;

    .line 1350
    :goto_1
    return-object v3

    .line 1339
    :catch_0
    move-exception v0

    .line 1340
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    if-ge v1, v3, :cond_3

    .line 1341
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1335
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1344
    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 1350
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 0
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p3, "version"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2011
    return-void
.end method

.method public getFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 10
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 468
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-ge v6, v7, :cond_1

    .line 470
    :cond_0
    new-instance v6, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v6}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v6

    .line 472
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez p3, :cond_2

    .line 473
    new-instance v6, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v6}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v6

    .line 475
    :cond_2
    const/4 v3, 0x0

    .line 476
    .local v3, "fileOut":Ljava/io/FileOutputStream;
    move-object v2, p2

    .line 477
    .local v2, "fileName":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "i":I
    move-object v4, v3

    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .local v4, "fileOut":Ljava/io/FileOutputStream;
    :goto_0
    const/4 v6, 0x5

    if-ge v5, v6, :cond_7

    .line 479
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v7, 0x0

    new-instance v8, Lcom/samsung/scloud/DropboxAPI$1;

    invoke-direct {v8, p0, v2, p4, p1}, Lcom/samsung/scloud/DropboxAPI$1;-><init>(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;Ljava/io/File;)V

    invoke-virtual {v6, p2, v7, v3, v8}, Lcom/dropbox/client2/SamsungDropboxAPI;->getFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    .line 500
    if-eqz p4, :cond_3

    .line 501
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    const-wide/16 v8, 0x64

    invoke-interface {p4, v6, v7, v8, v9}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 503
    :cond_3
    if-eqz v3, :cond_4

    .line 504
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 521
    :cond_4
    :goto_1
    return-void

    .line 506
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 508
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    :goto_2
    if-eqz v3, :cond_5

    .line 509
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 514
    :cond_5
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v6, 0x4

    if-ge v5, v6, :cond_6

    .line 515
    invoke-direct {p0, v5}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 477
    :goto_3
    add-int/lit8 v5, v5, 0x1

    move-object v4, v3

    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 510
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 511
    .local v1, "e1":Ljava/io/IOException;
    new-instance v6, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v7, "file close failed"

    invoke-direct {v6, v7}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 518
    .end local v1    # "e1":Ljava/io/IOException;
    :cond_6
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    :cond_7
    move-object v3, v4

    .line 521
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 506
    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method public getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 10
    .param p1, "filIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 345
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 346
    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    .line 347
    :cond_1
    const/4 v7, 0x0

    .line 348
    .local v7, "fileData":Lcom/dropbox/client2/DropboxAPI$Entry;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/4 v0, 0x5

    if-ge v8, v0, :cond_2

    .line 350
    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/client2/SamsungDropboxAPI;->metadata(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 360
    :cond_2
    if-eqz v7, :cond_4

    .line 361
    invoke-direct {p0, v7}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/data/SCloudFile;

    .line 363
    :goto_1
    return-object v0

    .line 352
    :catch_0
    move-exception v6

    .line 353
    .local v6, "e":Ljava/lang/Exception;
    invoke-direct {p0, v6}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    if-ge v8, v0, :cond_3

    .line 354
    invoke-direct {p0, v8}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 348
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 357
    :cond_3
    invoke-direct {p0, v6}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .end local v6    # "e":Ljava/lang/Exception;
    :cond_4
    move-object v0, v9

    .line 363
    goto :goto_1
.end method

.method public getFilesAsZip(Ljava/io/File;Ljava/util/List;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 0
    .param p1, "localFile"    # Ljava/io/File;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/samsung/scloud/response/ProgressListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2024
    .local p2, "IdOrpath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 10
    .param p1, "folederIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 279
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 280
    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    .line 281
    :cond_1
    const/4 v7, 0x0

    .line 282
    .local v7, "folderData":Lcom/dropbox/client2/DropboxAPI$Entry;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/4 v0, 0x5

    if-ge v8, v0, :cond_2

    .line 284
    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/client2/SamsungDropboxAPI;->metadata(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 294
    :cond_2
    if-nez v7, :cond_4

    move-object v0, v9

    .line 296
    :goto_1
    return-object v0

    .line 286
    :catch_0
    move-exception v6

    .line 287
    .local v6, "e":Lcom/dropbox/client2/exception/DropboxException;
    invoke-direct {p0, v6}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    if-ge v8, v0, :cond_3

    .line 288
    invoke-direct {p0, v8}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 282
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 291
    :cond_3
    invoke-direct {p0, v6}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 296
    .end local v6    # "e":Lcom/dropbox/client2/exception/DropboxException;
    :cond_4
    invoke-direct {p0, v7}, Lcom/samsung/scloud/DropboxAPI;->parseFolderMetaData(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v0

    goto :goto_1
.end method

.method public getHooks(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$Hook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1431
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 1433
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v2, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->hooks(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1442
    :goto_1
    return-object v2

    .line 1434
    :catch_0
    move-exception v0

    .line 1435
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 1436
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1431
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1439
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 1442
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 6
    .param p1, "folderIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 313
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 314
    :cond_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3

    .line 315
    :cond_1
    const/4 v0, 0x0

    .line 316
    .local v0, "albumData":Lcom/dropbox/client2/SamsungDropboxAPI$Album;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v4, 0x5

    if-ge v2, v4, :cond_2

    .line 318
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Lcom/dropbox/client2/SamsungDropboxAPI;->album(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/SamsungDropboxAPI$Album;

    move-result-object v0

    .line 319
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->parseAlbum(Lcom/dropbox/client2/SamsungDropboxAPI$Album;)Lcom/samsung/scloud/data/SCloudFolder;
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 328
    :cond_2
    return-object v3

    .line 320
    :catch_0
    move-exception v1

    .line 321
    .local v1, "e":Lcom/dropbox/client2/exception/DropboxException;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    if-ge v2, v4, :cond_3

    .line 322
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 316
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 325
    :cond_3
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public getMetadata(Ljava/lang/String;)Lcom/samsung/scloud/data/Metadata;
    .locals 1
    .param p1, "fileIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1915
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMusicCoverArt(Ljava/lang/String;Ljava/io/File;)V
    .locals 7
    .param p1, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p2, "localFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2101
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-ge v5, v6, :cond_1

    .line 2103
    :cond_0
    new-instance v5, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v5}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v5

    .line 2105
    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2106
    new-instance v5, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v5}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v5

    .line 2108
    :cond_2
    const/4 v2, 0x0

    .line 2109
    .local v2, "fileOut":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .local v4, "i":I
    move-object v3, v2

    .end local v2    # "fileOut":Ljava/io/FileOutputStream;
    .local v3, "fileOut":Ljava/io/FileOutputStream;
    :goto_0
    const/4 v5, 0x5

    if-ge v4, v5, :cond_6

    .line 2111
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2112
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v2    # "fileOut":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v5, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v5, p1, v2}, Lcom/dropbox/client2/SamsungDropboxAPI;->getMusicCoverArt(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 2113
    if-eqz v2, :cond_3

    .line 2114
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2131
    :cond_3
    :goto_1
    return-void

    .line 2116
    .end local v2    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 2118
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v2    # "fileOut":Ljava/io/FileOutputStream;
    :goto_2
    if-eqz v2, :cond_4

    .line 2119
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2124
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x4

    if-ge v4, v5, :cond_5

    .line 2125
    invoke-direct {p0, v4}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 2109
    :goto_3
    add-int/lit8 v4, v4, 0x1

    move-object v3, v2

    .end local v2    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 2120
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v2    # "fileOut":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 2121
    .local v1, "e1":Ljava/io/IOException;
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v6, "file close failed"

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2128
    .end local v1    # "e1":Ljava/io/IOException;
    :cond_5
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    :cond_6
    move-object v2, v3

    .line 2131
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v2    # "fileOut":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 2116
    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method public getMyCloudProvider()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1360
    const-string v0, "Dropbox"

    return-object v0
.end method

.method public getMyContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1419
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getRecycleBin(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2483
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getRepository(Ljava/lang/String;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2345
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getRepositoryInfo()Lcom/samsung/scloud/data/RepositoryInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2336
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getShareURLInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/ShareInfo;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2173
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStreamingInfoForMusic(Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 2141
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v5, :cond_1

    .line 2142
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 2143
    :cond_1
    const/4 v2, 0x0

    .line 2144
    .local v2, "mediaLink":Lcom/dropbox/client2/DropboxAPI$DropboxLink;
    new-instance v3, Lcom/samsung/scloud/data/StreamingInfo;

    invoke-direct {v3}, Lcom/samsung/scloud/data/StreamingInfo;-><init>()V

    .line 2145
    .local v3, "streamData":Lcom/samsung/scloud/data/StreamingInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x5

    if-ge v1, v4, :cond_2

    .line 2147
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v5, 0x1

    invoke-virtual {v4, p1, v5}, Lcom/dropbox/client2/SamsungDropboxAPI;->media(Ljava/lang/String;Z)Lcom/dropbox/client2/DropboxAPI$DropboxLink;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2157
    :cond_2
    if-eqz v2, :cond_3

    .line 2158
    iget-object v4, v2, Lcom/dropbox/client2/DropboxAPI$DropboxLink;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/StreamingInfo;->setStreamUrl(Ljava/lang/String;)V

    .line 2159
    iget-object v4, v2, Lcom/dropbox/client2/DropboxAPI$DropboxLink;->expires:Ljava/util/Date;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/StreamingInfo;->setExpiry(Ljava/util/Date;)V

    .line 2162
    :cond_3
    return-object v3

    .line 2149
    :catch_0
    move-exception v0

    .line 2150
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    if-ge v1, v4, :cond_4

    .line 2151
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 2145
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2154
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public getStreamingInfoForVideo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "screenResolution"    # Ljava/lang/String;
    .param p3, "connectionType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 839
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 840
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 841
    :cond_1
    const/4 v2, 0x0

    .line 842
    .local v2, "link":Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;
    new-instance v3, Lcom/samsung/scloud/data/StreamingInfo;

    invoke-direct {v3}, Lcom/samsung/scloud/data/StreamingInfo;-><init>()V

    .line 843
    .local v3, "streamData":Lcom/samsung/scloud/data/StreamingInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x5

    if-ge v1, v4, :cond_2

    .line 846
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v4, p1, p2, p3}, Lcom/dropbox/client2/SamsungDropboxAPI;->mediaTranscode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;
    :try_end_0
    .catch Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 865
    :cond_2
    if-eqz v2, :cond_3

    .line 867
    iget-object v4, v2, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/StreamingInfo;->setStreamUrl(Ljava/lang/String;)V

    .line 868
    iget-object v4, v2, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;->expires:Ljava/util/Date;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/StreamingInfo;->setExpiry(Ljava/util/Date;)V

    .line 870
    :cond_3
    return-object v3

    .line 848
    :catch_0
    move-exception v0

    .line 850
    .local v0, "e":Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;
    :try_start_1
    new-instance v4, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_1 .. :try_end_1} :catch_1

    .line 853
    .end local v0    # "e":Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;
    :catch_1
    move-exception v0

    .line 858
    .local v0, "e":Lcom/dropbox/client2/exception/DropboxException;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    if-ge v1, v4, :cond_4

    .line 859
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 843
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 862
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public getThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 17
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFilePath"    # Ljava/lang/String;
    .param p3, "thumbnail_type"    # I
    .param p4, "thumbnail_format"    # I
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 550
    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_1

    .line 551
    sget-object v6, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->PNG:Lcom/dropbox/client2/DropboxAPI$ThumbFormat;

    .line 558
    .local v6, "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    :goto_0
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/samsung/scloud/DropboxAPI;->getDropboxThumbSize(I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    move-result-object v5

    .line 560
    .local v5, "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    const/4 v4, 0x0

    .line 562
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    .line 568
    .local v15, "filename":Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    const/4 v2, 0x5

    move/from16 v0, v16

    if-ge v0, v2, :cond_8

    .line 570
    :try_start_1
    new-instance v7, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;

    move-object/from16 v0, p5

    invoke-direct {v7, v0, v15}, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;-><init>(Lcom/samsung/scloud/response/ProgressListener;Ljava/lang/String;)V

    .line 572
    .local v7, "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    if-eqz v5, :cond_3

    .line 573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lcom/dropbox/client2/SamsungDropboxAPI;->getThumbnail(Ljava/lang/String;Ljava/io/OutputStream;Lcom/dropbox/client2/DropboxAPI$ThumbSize;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    .line 579
    :goto_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580
    const/4 v2, 0x0

    .line 594
    if-eqz v4, :cond_0

    .line 596
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 603
    .end local v7    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    :cond_0
    :goto_3
    return-object v2

    .line 552
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v5    # "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .end local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    .end local v15    # "filename":Ljava/lang/String;
    .end local v16    # "i":I
    :cond_1
    if-nez p4, :cond_2

    .line 553
    sget-object v6, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->JPEG:Lcom/dropbox/client2/DropboxAPI$ThumbFormat;

    .restart local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    goto :goto_0

    .line 555
    .end local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    :cond_2
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v3, "wrong thumbnail type"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 563
    .restart local v5    # "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .restart local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    :catch_0
    move-exception v14

    .line 564
    .local v14, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v14}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 576
    .end local v14    # "e":Ljava/lang/Exception;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    .restart local v15    # "filename":Ljava/lang/String;
    .restart local v16    # "i":I
    :cond_3
    :try_start_3
    const-string v11, "2048x1536_bestfit"

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move-object v10, v4

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/samsung/scloud/DropboxAPI;->getHiddenThumbnail(Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 581
    .end local v7    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    :catch_1
    move-exception v14

    .line 582
    .restart local v14    # "e":Ljava/lang/Exception;
    :try_start_4
    instance-of v2, v14, Lcom/dropbox/client2/exception/DropboxServerException;

    if-eqz v2, :cond_5

    .line 583
    move-object v0, v14

    check-cast v0, Lcom/dropbox/client2/exception/DropboxServerException;

    move-object v2, v0

    iget v2, v2, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    const/16 v3, 0x19f

    if-ne v2, v3, :cond_5

    .line 584
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v3, "no thumbnail"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 594
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v4, :cond_4

    .line 596
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 599
    :cond_4
    :goto_4
    throw v2

    .line 597
    .restart local v7    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    :catch_2
    move-exception v14

    .line 598
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 588
    .end local v7    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    .local v14, "e":Ljava/lang/Exception;
    :cond_5
    :try_start_6
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x4

    move/from16 v0, v16

    if-ge v0, v2, :cond_7

    .line 589
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 594
    if-eqz v4, :cond_6

    .line 596
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 568
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_5
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 597
    .restart local v14    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v14

    .line 598
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 592
    .local v14, "e":Ljava/lang/Exception;
    :cond_7
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 594
    if-eqz v4, :cond_6

    .line 596
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto :goto_5

    .line 597
    :catch_4
    move-exception v14

    .line 598
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 597
    .end local v14    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v14

    .line 598
    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 603
    .end local v14    # "e":Ljava/io/IOException;
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_3
.end method

.method public getThumbnailWithMeta(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 17
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFilePath"    # Ljava/lang/String;
    .param p3, "thumbnail_type"    # I
    .param p4, "thumbnail_format"    # I
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 700
    const/4 v15, 0x1

    move/from16 v0, p4

    if-ne v0, v15, :cond_4

    .line 701
    sget-object v6, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->PNG:Lcom/dropbox/client2/DropboxAPI$ThumbFormat;

    .line 708
    .local v6, "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    :goto_0
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/samsung/scloud/DropboxAPI;->getDropboxThumbSize(I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    move-result-object v14

    .line 710
    .local v14, "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    const/4 v7, 0x0

    .line 712
    .local v7, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 717
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .line 718
    .local v3, "fileData":Lcom/samsung/scloud/data/SCloudFile;
    const/4 v11, 0x0

    .line 719
    .local v11, "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 720
    .local v5, "filename":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    move-object v12, v11

    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .local v12, "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v4, v3

    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .local v4, "fileData":Lcom/samsung/scloud/data/SCloudFile;
    :goto_1
    const/4 v15, 0x5

    if-ge v8, v15, :cond_b

    .line 722
    const/4 v13, 0x0

    .line 724
    .local v13, "sThumbData":Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
    :try_start_1
    new-instance v10, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;

    move-object/from16 v0, p5

    invoke-direct {v10, v0, v5}, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;-><init>(Lcom/samsung/scloud/response/ProgressListener;Ljava/lang/String;)V

    .line 726
    .local v10, "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    if-eqz v14, :cond_6

    .line 727
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    move-object/from16 v0, p2

    invoke-virtual {v15, v0, v14, v6}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSamsungThumbnailStream(Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbSize;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;

    move-result-object v13

    .line 732
    :goto_2
    invoke-virtual {v13, v7, v10}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->copyStreamToOutput(Ljava/io/OutputStream;Lcom/dropbox/client2/ProgressListener;)V

    .line 733
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 735
    invoke-virtual {v13}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->getPhotoMetadata()Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v15

    if-eqz v15, :cond_c

    .line 736
    new-instance v11, Lcom/samsung/scloud/data/PhotoMetaData;

    invoke-direct {v11}, Lcom/samsung/scloud/data/PhotoMetaData;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 737
    .end local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :try_start_2
    invoke-virtual {v13}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->getPhotoMetadata()Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v15

    iget-object v15, v15, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->width:Ljava/lang/Integer;

    if-eqz v15, :cond_0

    .line 738
    invoke-virtual {v13}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->getPhotoMetadata()Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v15

    iget-object v15, v15, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->width:Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-virtual {v11, v15}, Lcom/samsung/scloud/data/PhotoMetaData;->setWidth(I)V

    .line 739
    :cond_0
    invoke-virtual {v13}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->getPhotoMetadata()Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v15

    iget-object v15, v15, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->height:Ljava/lang/Integer;

    if-eqz v15, :cond_1

    .line 740
    invoke-virtual {v13}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->getPhotoMetadata()Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v15

    iget-object v15, v15, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->height:Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-virtual {v11, v15}, Lcom/samsung/scloud/data/PhotoMetaData;->setHeight(I)V

    .line 741
    :cond_1
    invoke-virtual {v13}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->getPhotoMetadata()Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v15

    iget-object v15, v15, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->orientation:Ljava/lang/Integer;

    if-eqz v15, :cond_2

    .line 742
    invoke-virtual {v13}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->getPhotoMetadata()Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v15

    iget-object v15, v15, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->orientation:Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/scloud/DropboxAPI;->convertExifToDegree(I)I

    move-result v9

    .line 743
    .local v9, "orientation":I
    invoke-virtual {v11, v9}, Lcom/samsung/scloud/data/PhotoMetaData;->setOrientation(I)V

    .line 745
    .end local v9    # "orientation":I
    :cond_2
    new-instance v3, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v3}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 746
    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    :try_start_3
    invoke-virtual {v3, v11}, Lcom/samsung/scloud/data/SCloudFile;->setPhotoMetaData(Lcom/samsung/scloud/data/PhotoMetaData;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 764
    :goto_3
    if-eqz v7, :cond_3

    .line 766
    :try_start_4
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    :goto_4
    move-object v4, v3

    .line 773
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v10    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    .end local v13    # "sThumbData":Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
    :goto_5
    return-object v4

    .line 702
    .end local v5    # "filename":Ljava/lang/String;
    .end local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "i":I
    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .end local v14    # "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    :cond_4
    if-nez p4, :cond_5

    .line 703
    sget-object v6, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->JPEG:Lcom/dropbox/client2/DropboxAPI$ThumbFormat;

    .restart local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    goto/16 :goto_0

    .line 705
    .end local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    :cond_5
    new-instance v15, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v16, "wrong thumbnail type"

    invoke-direct/range {v15 .. v16}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 713
    .restart local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    .restart local v14    # "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    :catch_0
    move-exception v2

    .line 714
    .local v2, "e":Ljava/lang/Exception;
    new-instance v15, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 729
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "i":I
    .restart local v10    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    .restart local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v13    # "sThumbData":Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
    :cond_6
    :try_start_5
    const-string v15, "2048x1536_bestfit"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v15, v6}, Lcom/samsung/scloud/DropboxAPI;->getSamsungHiddenThumbnailStream(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v13

    goto/16 :goto_2

    .line 767
    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catch_1
    move-exception v2

    .line 768
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 751
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v10    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catch_2
    move-exception v2

    move-object v11, v12

    .end local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v3, v4

    .line 752
    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .local v2, "e":Ljava/lang/Exception;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    :goto_6
    :try_start_6
    instance-of v15, v2, Lcom/dropbox/client2/exception/DropboxServerException;

    if-eqz v15, :cond_8

    .line 753
    move-object v0, v2

    check-cast v0, Lcom/dropbox/client2/exception/DropboxServerException;

    move-object v15, v0

    iget v15, v15, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    const/16 v16, 0x19f

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 754
    new-instance v15, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v16, "no thumbnail"

    invoke-direct/range {v15 .. v16}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 764
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v15

    :goto_7
    if-eqz v7, :cond_7

    .line 766
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 769
    :cond_7
    :goto_8
    throw v15

    .line 758
    .restart local v2    # "e":Ljava/lang/Exception;
    :cond_8
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v15

    if-eqz v15, :cond_a

    const/4 v15, 0x4

    if-ge v8, v15, :cond_a

    .line 759
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 764
    if-eqz v7, :cond_9

    .line 766
    :try_start_9
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 720
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_9
    :goto_9
    add-int/lit8 v8, v8, 0x1

    move-object v12, v11

    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v4, v3

    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto/16 :goto_1

    .line 767
    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :catch_3
    move-exception v2

    .line 768
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 762
    .local v2, "e":Ljava/lang/Exception;
    :cond_a
    :try_start_a
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 764
    if-eqz v7, :cond_9

    .line 766
    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_9

    .line 767
    :catch_4
    move-exception v2

    .line 768
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 767
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 768
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .end local v13    # "sThumbData":Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :cond_b
    move-object v11, v12

    .end local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v3, v4

    .line 773
    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto/16 :goto_5

    .line 764
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v13    # "sThumbData":Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
    :catchall_1
    move-exception v15

    move-object v11, v12

    .end local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v3, v4

    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto :goto_7

    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v10    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    :catchall_2
    move-exception v15

    move-object v3, v4

    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto :goto_7

    .line 751
    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    :catch_6
    move-exception v2

    move-object v3, v4

    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto :goto_6

    :catch_7
    move-exception v2

    goto :goto_6

    .end local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .end local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    :cond_c
    move-object v11, v12

    .end local v12    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    .restart local v11    # "photoMeta":Lcom/samsung/scloud/data/PhotoMetaData;
    move-object v3, v4

    .end local v4    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    .restart local v3    # "fileData":Lcom/samsung/scloud/data/SCloudFile;
    goto/16 :goto_3
.end method

.method public getUpdate(JJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "beginTimeStamp"    # J
    .param p3, "endTimeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2270
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getUserInfo()Lcom/samsung/scloud/data/User;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 191
    const/4 v0, 0x0

    .line 192
    .local v0, "account":Lcom/dropbox/client2/DropboxAPI$Account;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v4, 0x5

    if-ge v2, v4, :cond_0

    .line 194
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v4}, Lcom/dropbox/client2/SamsungDropboxAPI;->accountInfo()Lcom/dropbox/client2/DropboxAPI$Account;
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 204
    :cond_0
    if-nez v0, :cond_2

    .line 205
    const/4 v3, 0x0

    .line 212
    :goto_1
    return-object v3

    .line 196
    :catch_0
    move-exception v1

    .line 197
    .local v1, "e":Lcom/dropbox/client2/exception/DropboxException;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x4

    if-ge v2, v4, :cond_1

    .line 198
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 192
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 201
    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 206
    .end local v1    # "e":Lcom/dropbox/client2/exception/DropboxException;
    :cond_2
    new-instance v3, Lcom/samsung/scloud/data/User;

    invoke-direct {v3}, Lcom/samsung/scloud/data/User;-><init>()V

    .line 207
    .local v3, "ret":Lcom/samsung/scloud/data/User;
    iget-object v4, v0, Lcom/dropbox/client2/DropboxAPI$Account;->displayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/User;->setName(Ljava/lang/String;)V

    .line 208
    iget-wide v4, v0, Lcom/dropbox/client2/DropboxAPI$Account;->quota:J

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setQuotaBytesTotal(J)V

    .line 209
    iget-wide v4, v0, Lcom/dropbox/client2/DropboxAPI$Account;->quotaNormal:J

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setQuotaBytesUsed(J)V

    .line 210
    iget-wide v4, v0, Lcom/dropbox/client2/DropboxAPI$Account;->quotaShared:J

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setQuotaBytesShared(J)V

    .line 211
    iget-wide v4, v0, Lcom/dropbox/client2/DropboxAPI$Account;->uid:J

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setId(J)V

    goto :goto_1
.end method

.method public getVersions(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2034
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWholeFileList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 1
    .param p1, "maxResults"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2501
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getWholeFolderList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 8
    .param p1, "maxResults"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 803
    const/4 v1, 0x0

    .line 804
    .local v1, "albumList":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    const/4 v7, 0x5

    if-ge v5, v7, :cond_0

    .line 806
    :try_start_0
    iget-object v7, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v7}, Lcom/dropbox/client2/SamsungDropboxAPI;->albums()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 816
    :cond_0
    if-eqz v1, :cond_3

    .line 817
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 818
    .local v4, "folderList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    .line 819
    .local v0, "albumData":Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->parseAlbumMetadata(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v3

    .line 820
    .local v3, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    if-eqz v3, :cond_1

    .line 821
    invoke-virtual {v4}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 808
    .end local v0    # "albumData":Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    .end local v3    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v4    # "folderList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    .end local v6    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 809
    .local v2, "e":Ljava/lang/Exception;
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v7, 0x4

    if-ge v5, v7, :cond_2

    .line 810
    invoke-direct {p0, v5}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 804
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 813
    :cond_2
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 825
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v4, 0x0

    :cond_4
    return-object v4
.end method

.method public inviteCollaborators(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2467
    .local p2, "collaborators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Collaborator;>;"
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public isAuthSucceed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2245
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/android/AndroidAuthSession;

    invoke-virtual {v0}, Lcom/dropbox/client2/android/AndroidAuthSession;->authenticationSuccessful()Z

    move-result v0

    return v0
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/User;
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2475
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public logout()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    .line 2257
    return-void
.end method

.method public makeCurrentVersion(Ljava/lang/String;J)Ljava/util/ArrayList;
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "versionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2046
    const/4 v0, 0x0

    return-object v0
.end method

.method public makeFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 6
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "parentPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 223
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v4, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v4, :cond_1

    .line 225
    :cond_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3

    .line 226
    :cond_1
    const/4 v1, 0x0

    .line 227
    .local v1, "entry":Lcom/dropbox/client2/DropboxAPI$Entry;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x5

    if-ge v2, v3, :cond_3

    .line 229
    :try_start_0
    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dropbox/client2/SamsungDropboxAPI;->createFolder(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$Entry;

    move-result-object v1

    .line 230
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v3

    check-cast v3, Lcom/samsung/scloud/data/SCloudFolder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    :goto_1
    return-object v3

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x4

    if-ge v2, v3, :cond_2

    .line 233
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 227
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 236
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 239
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public mediaTranscode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 12
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "screenResolution"    # Ljava/lang/String;
    .param p3, "connectionType"    # Ljava/lang/String;
    .param p4, "container"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 890
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/media_transcode/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v1}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v1

    check-cast v1, Lcom/dropbox/client2/session/AbstractSession;

    invoke-virtual {v1}, Lcom/dropbox/client2/session/AbstractSession;->getAccessType()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 893
    .local v3, "target":Ljava/lang/String;
    const/16 v1, 0x10

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "locale"

    aput-object v2, v5, v1

    iget-object v1, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v1}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v1

    check-cast v1, Lcom/dropbox/client2/session/AbstractSession;

    invoke-virtual {v1}, Lcom/dropbox/client2/session/AbstractSession;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v4

    const/4 v1, 0x2

    const-string v2, "screen_resolution"

    aput-object v2, v5, v1

    const/4 v1, 0x3

    aput-object p2, v5, v1

    const/4 v1, 0x4

    const-string v2, "connection_type"

    aput-object v2, v5, v1

    const/4 v1, 0x5

    aput-object p3, v5, v1

    const/4 v1, 0x6

    const-string v2, "model"

    aput-object v2, v5, v1

    const/4 v1, 0x7

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v2, v5, v1

    const/16 v1, 0x8

    const-string v2, "sys_version"

    aput-object v2, v5, v1

    const/16 v1, 0x9

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v2, v5, v1

    const/16 v1, 0xa

    const-string v2, "platform"

    aput-object v2, v5, v1

    const/16 v1, 0xb

    const-string v2, "android"

    aput-object v2, v5, v1

    const/16 v1, 0xc

    const-string v2, "manufacturer"

    aput-object v2, v5, v1

    const/16 v1, 0xd

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v2, v5, v1

    const/16 v1, 0xe

    const-string v2, "container"

    aput-object v2, v5, v1

    const/16 v1, 0xf

    aput-object p4, v5, v1

    .line 901
    .local v5, "params":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 903
    .local v9, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    sget-object v1, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v2}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v2

    check-cast v2, Lcom/dropbox/client2/session/AbstractSession;

    invoke-virtual {v2}, Lcom/dropbox/client2/session/AbstractSession;->getAPIServer()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v6}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map;

    move-object v9, v0
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 910
    :goto_0
    const-string v1, "url"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 911
    .local v11, "url":Ljava/lang/String;
    const-string v1, "expires"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 913
    .local v8, "exp":Ljava/lang/String;
    new-instance v10, Lcom/samsung/scloud/data/StreamingInfo;

    invoke-direct {v10}, Lcom/samsung/scloud/data/StreamingInfo;-><init>()V

    .line 914
    .local v10, "ret":Lcom/samsung/scloud/data/StreamingInfo;
    invoke-virtual {v10, v11}, Lcom/samsung/scloud/data/StreamingInfo;->setStreamUrl(Ljava/lang/String;)V

    .line 915
    if-nez v8, :cond_0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v10, v1}, Lcom/samsung/scloud/data/StreamingInfo;->setExpiry(Ljava/util/Date;)V

    .line 917
    return-object v10

    .line 906
    .end local v8    # "exp":Ljava/lang/String;
    .end local v10    # "ret":Lcom/samsung/scloud/data/StreamingInfo;
    .end local v11    # "url":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 907
    .local v7, "e":Lcom/dropbox/client2/exception/DropboxException;
    invoke-direct {p0, v7}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_0

    .line 915
    .end local v7    # "e":Lcom/dropbox/client2/exception/DropboxException;
    .restart local v8    # "exp":Ljava/lang/String;
    .restart local v10    # "ret":Lcom/samsung/scloud/data/StreamingInfo;
    .restart local v11    # "url":Ljava/lang/String;
    :cond_0
    invoke-static {v8}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    goto :goto_1
.end method

.method public move(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 4
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1969
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 1971
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 1973
    :cond_1
    const/4 v1, 0x0

    .line 1975
    .local v1, "fileData":Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v2, p1, p2}, Lcom/dropbox/client2/SamsungDropboxAPI;->move(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1979
    :goto_0
    if-eqz v1, :cond_2

    .line 1980
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/data/SCloudFile;

    .line 1982
    :goto_1
    return-object v2

    .line 1976
    :catch_0
    move-exception v0

    .line 1977
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1982
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public musicDelta(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 9
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 934
    const/4 v6, 0x0

    .line 935
    .local v6, "page":Lcom/dropbox/client2/DropboxAPI$DeltaPage;, "Lcom/dropbox/client2/DropboxAPI$DeltaPage<Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;>;"
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 936
    .local v4, "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 937
    .local v5, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v8, 0x5

    if-ge v2, v8, :cond_0

    .line 939
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v8, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->musicDelta(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 953
    :cond_0
    if-eqz v6, :cond_5

    .line 954
    iget-object v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->cursor:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setCursor(Ljava/lang/String;)V

    .line 955
    iget-boolean v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->hasMore:Z

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setHasMore(Z)V

    .line 956
    iget-boolean v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->reset:Z

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setReset(Z)V

    .line 957
    const/4 v7, 0x0

    .line 958
    .local v7, "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->entries:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;

    .line 959
    .local v1, "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;>;"
    if-eqz v1, :cond_1

    .line 960
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    if-nez v8, :cond_3

    .line 966
    new-instance v7, Lcom/samsung/scloud/data/SCloudFile;

    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    invoke-direct {v7}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 967
    .restart local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->lcPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 968
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->lcPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 969
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 974
    :cond_1
    :goto_2
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 941
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :catch_0
    move-exception v0

    .line 946
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x4

    if-ge v2, v8, :cond_2

    .line 947
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 937
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 950
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_3

    .line 970
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_3
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    if-eqz v8, :cond_1

    .line 971
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    check-cast v8, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;

    invoke-direct {p0, v8}, Lcom/samsung/scloud/DropboxAPI;->parseMusicEntry(Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v7

    goto :goto_2

    .line 976
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;>;"
    :cond_4
    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudNodeList;->setNodes(Ljava/util/List;)V

    .line 978
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_5
    return-object v4
.end method

.method public photoDelta(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 9
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1056
    const/4 v6, 0x0

    .line 1057
    .local v6, "page":Lcom/dropbox/client2/DropboxAPI$DeltaPage;, "Lcom/dropbox/client2/DropboxAPI$DeltaPage<Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;>;"
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 1058
    .local v4, "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1059
    .local v5, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v8, 0x5

    if-ge v2, v8, :cond_0

    .line 1061
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v8, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->photosDelta(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1075
    :cond_0
    if-eqz v6, :cond_5

    .line 1076
    iget-object v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->cursor:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setCursor(Ljava/lang/String;)V

    .line 1077
    iget-boolean v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->hasMore:Z

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setHasMore(Z)V

    .line 1078
    iget-boolean v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->reset:Z

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setReset(Z)V

    .line 1079
    const/4 v7, 0x0

    .line 1080
    .local v7, "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->entries:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;

    .line 1081
    .local v1, "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;>;"
    if-eqz v1, :cond_1

    .line 1082
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    if-nez v8, :cond_3

    .line 1088
    new-instance v7, Lcom/samsung/scloud/data/SCloudFile;

    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    invoke-direct {v7}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1089
    .restart local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->lcPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1090
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->lcPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1091
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1096
    :cond_1
    :goto_2
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1063
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :catch_0
    move-exception v0

    .line 1068
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x4

    if-ge v2, v8, :cond_2

    .line 1069
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1059
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1072
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_3

    .line 1092
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_3
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    if-eqz v8, :cond_1

    .line 1093
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    check-cast v8, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;

    invoke-direct {p0, v8}, Lcom/samsung/scloud/DropboxAPI;->parseAlbumEntry(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v7

    goto :goto_2

    .line 1098
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;>;"
    :cond_4
    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudNodeList;->setNodes(Ljava/util/List;)V

    .line 1100
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_5
    return-object v4
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 2
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1761
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 1763
    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    .line 1766
    :cond_1
    if-eqz p4, :cond_2

    .line 1767
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v1, "Google Drive not support the overwriting the file with the filename, you should overwrite the file with file Id"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1769
    :cond_2
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/samsung/scloud/DropboxAPI;->putNewFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v0

    return-object v0
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 22
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "parentRev"    # Ljava/lang/String;
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1847
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-ge v5, v6, :cond_1

    .line 1849
    :cond_0
    new-instance v5, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v5}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v5

    .line 1852
    :cond_1
    new-instance v10, Lcom/samsung/scloud/DropboxAPI$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/scloud/DropboxAPI$3;-><init>(Lcom/samsung/scloud/DropboxAPI;Ljava/io/File;Lcom/samsung/scloud/response/ProgressListener;)V

    .line 1869
    .local v10, "listener":Lcom/dropbox/client2/ProgressListener;
    const/4 v7, 0x0

    .line 1870
    .local v7, "fis":Ljava/io/FileInputStream;
    const/16 v21, 0x0

    .line 1871
    .local v21, "uploadedFile":Lcom/dropbox/client2/DropboxAPI$Entry;
    const/16 v20, 0x0

    .local v20, "i":I
    move-object/from16 v19, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v19, "fis":Ljava/io/FileInputStream;
    :goto_0
    const/4 v5, 0x5

    move/from16 v0, v20

    if-ge v0, v5, :cond_7

    .line 1873
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1875
    .end local v19    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    if-eqz p3, :cond_3

    .line 1876
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual/range {v5 .. v10}, Lcom/dropbox/client2/SamsungDropboxAPI;->putFileOverwriteRequest(Ljava/lang/String;Ljava/io/InputStream;JLcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    move-result-object v5

    invoke-interface {v5}, Lcom/dropbox/client2/DropboxAPI$UploadRequest;->upload()Lcom/dropbox/client2/DropboxAPI$Entry;

    move-result-object v21

    .line 1882
    :goto_1
    if-eqz p5, :cond_2

    .line 1883
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    const-wide/16 v8, 0x64

    move-object/from16 v0, p5

    invoke-interface {v0, v5, v6, v8, v9}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 1885
    :cond_2
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1902
    :goto_2
    if-eqz v21, :cond_6

    .line 1903
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    check-cast v5, Lcom/samsung/scloud/data/SCloudFile;

    .line 1905
    :goto_3
    return-object v5

    .line 1879
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v14

    move-object v13, v7

    move-object/from16 v16, p4

    move-object/from16 v17, v10

    invoke-virtual/range {v11 .. v17}, Lcom/dropbox/client2/SamsungDropboxAPI;->putFileRequest(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    move-result-object v5

    invoke-interface {v5}, Lcom/dropbox/client2/DropboxAPI$UploadRequest;->upload()Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v21

    goto :goto_1

    .line 1887
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v19    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v4

    move-object/from16 v7, v19

    .line 1889
    .end local v19    # "fis":Ljava/io/FileInputStream;
    .local v4, "e":Ljava/lang/Exception;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :goto_4
    if-eqz v7, :cond_4

    .line 1890
    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1895
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x4

    move/from16 v0, v20

    if-ge v0, v5, :cond_5

    .line 1896
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1871
    :goto_5
    add-int/lit8 v20, v20, 0x1

    move-object/from16 v19, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v19    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .line 1891
    .end local v19    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v18

    .line 1892
    .local v18, "e1":Ljava/io/IOException;
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v6, "file close failed"

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1899
    .end local v18    # "e1":Ljava/io/IOException;
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_5

    .line 1905
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_6
    const/4 v5, 0x0

    goto :goto_3

    .line 1887
    :catch_2
    move-exception v4

    goto :goto_4

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v19    # "fis":Ljava/io/FileInputStream;
    :cond_7
    move-object/from16 v7, v19

    .end local v19    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public putFile(Ljava/io/FileInputStream;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "sourceFileInputStream"    # Ljava/io/FileInputStream;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1997
    const/4 v0, 0x0

    return-object v0
.end method

.method public removeCollaboration(J)V
    .locals 1
    .param p1, "collaborationId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2450
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public removeComment(Ljava/lang/String;J)V
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "commentID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2415
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public removePushTrigger(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;)Z
    .locals 3
    .param p1, "hook_type"    # Lcom/dropbox/client2/SamsungDropboxAPI$Hook;
    .param p2, "triggerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1490
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 1492
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v2, p1, p2}, Lcom/dropbox/client2/SamsungDropboxAPI;->removeTrigger(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1501
    :goto_1
    return v2

    .line 1493
    :catch_0
    move-exception v0

    .line 1494
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 1495
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1490
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1498
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_2

    .line 1501
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public removeShareURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2183
    const/4 v0, 0x0

    return v0
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 8
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 385
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v7, :cond_0

    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v5, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v7, :cond_1

    .line 388
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 390
    :cond_1
    const/4 v2, 0x0

    .line 391
    .local v2, "entry":Lcom/dropbox/client2/DropboxAPI$Entry;
    const/4 v0, 0x0

    .line 392
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v4, "."

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v5, :cond_3

    .line 393
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 400
    :goto_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    const/4 v4, 0x5

    if-ge v3, v4, :cond_2

    .line 402
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lcom/dropbox/client2/SamsungDropboxAPI;->move(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 412
    :cond_2
    if-eqz v2, :cond_5

    .line 413
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v4

    .line 414
    :goto_2
    return-object v4

    .line 396
    .end local v3    # "i":I
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto :goto_0

    .line 404
    .restart local v3    # "i":I
    :catch_0
    move-exception v1

    .line 405
    .local v1, "e":Lcom/dropbox/client2/exception/DropboxException;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    if-ge v3, v4, :cond_4

    .line 406
    invoke-direct {p0, v3}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 400
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 409
    :cond_4
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_3

    .line 414
    .end local v1    # "e":Lcom/dropbox/client2/exception/DropboxException;
    :cond_5
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public restoreFromRecycleBin(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "recycledBinIdOrPath"    # Ljava/lang/String;
    .param p2, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2492
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/samsung/scloud/data/SearchResult;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "fileLimit"    # I
    .param p4, "includeDeleted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2198
    const/4 v5, 0x0

    .line 2199
    .local v5, "responseScloudNodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v6, 0x0

    .line 2200
    .local v6, "searchResult":Lcom/samsung/scloud/data/SearchResult;
    const/4 v4, 0x0

    .line 2203
    .local v4, "responseEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    :try_start_0
    iget-object v7, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v7, p1, p2, p3, p4}, Lcom/dropbox/client2/SamsungDropboxAPI;->search(Ljava/lang/String;Ljava/lang/String;IZ)Ljava/util/List;
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 2208
    :goto_0
    if-eqz v4, :cond_1

    .line 2210
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "responseScloudNodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2212
    .restart local v5    # "responseScloudNodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/client2/DropboxAPI$Entry;

    .line 2214
    .local v1, "entry":Lcom/dropbox/client2/DropboxAPI$Entry;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v3

    .line 2215
    .local v3, "node":Lcom/samsung/scloud/data/SCloudNode;
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2205
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$Entry;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :catch_0
    move-exception v0

    .line 2206
    .local v0, "e":Lcom/dropbox/client2/exception/DropboxException;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_0

    .line 2218
    .end local v0    # "e":Lcom/dropbox/client2/exception/DropboxException;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 2219
    new-instance v6, Lcom/samsung/scloud/data/SearchResult;

    .end local v6    # "searchResult":Lcom/samsung/scloud/data/SearchResult;
    invoke-direct {v6}, Lcom/samsung/scloud/data/SearchResult;-><init>()V

    .line 2220
    .restart local v6    # "searchResult":Lcom/samsung/scloud/data/SearchResult;
    invoke-virtual {v6, v5}, Lcom/samsung/scloud/data/SearchResult;->setSearchResults(Ljava/util/List;)V

    .line 2225
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return-object v6
.end method

.method public setActiveRepository(Ljava/lang/String;)V
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2371
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setAuthToken(Ljava/lang/String;)V
    .locals 5
    .param p1, "tokenPair"    # Ljava/lang/String;

    .prologue
    .line 1389
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1390
    .local v2, "token":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1407
    :goto_0
    return-void

    .line 1393
    :cond_0
    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 1394
    .local v0, "mAccessKey":Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v1, v2, v3

    .line 1396
    .local v1, "mSecretKey":Ljava/lang/String;
    const-string v3, "Loading Access TOKEN_KEY"

    invoke-direct {p0, v3}, Lcom/samsung/scloud/DropboxAPI;->dd(Ljava/lang/String;)V

    .line 1398
    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 1399
    new-instance v3, Lcom/dropbox/client2/session/AccessTokenPair;

    invoke-direct {v3, v0, v1}, Lcom/dropbox/client2/session/AccessTokenPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    .line 1400
    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v3}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v3

    check-cast v3, Lcom/dropbox/client2/session/AbstractSession;

    iget-object v4, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    invoke-virtual {v3, v4}, Lcom/dropbox/client2/session/AbstractSession;->setAccessTokenPair(Lcom/dropbox/client2/session/AccessTokenPair;)V

    goto :goto_0

    .line 1401
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/scloud/DropboxAPI;->isAndroid:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 1402
    new-instance v3, Lcom/dropbox/client2/session/AccessTokenPair;

    invoke-direct {v3, v0, v1}, Lcom/dropbox/client2/session/AccessTokenPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/samsung/scloud/DropboxAPI;->accessTokenPair:Lcom/dropbox/client2/session/AccessTokenPair;

    goto :goto_0

    .line 1404
    :cond_2
    const-string v3, "Token is null !"

    invoke-direct {p0, v3}, Lcom/samsung/scloud/DropboxAPI;->dd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2424
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setDeviceProfile(Lcom/samsung/scloud/data/Device;)V
    .locals 1
    .param p1, "deviceProfile"    # Lcom/samsung/scloud/data/Device;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2294
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setDeviceProfile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2303
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public startAuth(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2235
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->getSession()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/android/AndroidAuthSession;

    invoke-virtual {v0, p1}, Lcom/dropbox/client2/android/AndroidAuthSession;->startAuthentication(Landroid/content/Context;)V

    .line 2236
    return-void
.end method

.method public unsetDescription(Ljava/lang/String;)V
    .locals 1
    .param p1, "IdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2432
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public updateRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repo"    # Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2362
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public updateRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .param p2, "spaceAmount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2354
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public videoDelta(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 9
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1194
    const/4 v6, 0x0

    .line 1195
    .local v6, "page":Lcom/dropbox/client2/DropboxAPI$DeltaPage;, "Lcom/dropbox/client2/DropboxAPI$DeltaPage<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 1196
    .local v4, "nodeList":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1197
    .local v5, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v8, 0x5

    if-ge v2, v8, :cond_0

    .line 1199
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v8, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->videosDelta(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1213
    :cond_0
    if-eqz v6, :cond_5

    .line 1214
    iget-object v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->cursor:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setCursor(Ljava/lang/String;)V

    .line 1215
    iget-boolean v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->hasMore:Z

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setHasMore(Z)V

    .line 1216
    iget-boolean v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->reset:Z

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setReset(Z)V

    .line 1217
    const/4 v7, 0x0

    .line 1218
    .local v7, "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v8, v6, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->entries:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;

    .line 1219
    .local v1, "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    if-eqz v1, :cond_1

    .line 1220
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    if-nez v8, :cond_3

    .line 1226
    new-instance v7, Lcom/samsung/scloud/data/SCloudFile;

    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    invoke-direct {v7}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1227
    .restart local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->lcPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1228
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->lcPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setAbsolutePath(Ljava/lang/String;)V

    .line 1229
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1234
    :cond_1
    :goto_2
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1201
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :catch_0
    move-exception v0

    .line 1206
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x4

    if-ge v2, v8, :cond_2

    .line 1207
    invoke-direct {p0, v2}, Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V

    .line 1197
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1210
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V

    goto :goto_3

    .line 1230
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_3
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    if-eqz v8, :cond_1

    .line 1231
    iget-object v8, v1, Lcom/dropbox/client2/DropboxAPI$DeltaEntry;->metadata:Ljava/lang/Object;

    check-cast v8, Lcom/dropbox/client2/DropboxAPI$Entry;

    invoke-direct {p0, v8}, Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v7

    goto :goto_2

    .line 1236
    .end local v1    # "entry":Lcom/dropbox/client2/DropboxAPI$DeltaEntry;, "Lcom/dropbox/client2/DropboxAPI$DeltaEntry<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    :cond_4
    invoke-virtual {v4, v5}, Lcom/samsung/scloud/data/SCloudNodeList;->setNodes(Ljava/util/List;)V

    .line 1238
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "updatedNode":Lcom/samsung/scloud/data/SCloudNode;
    :cond_5
    return-object v4
.end method
