.class Lcom/baidu/pcs/BaiduPCSThumbnailWithMeta;
.super Lcom/baidu/pcs/BaiduPCSActionBase;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduPCSActionBase;-><init>()V

    return-void
.end method


# virtual methods
.method public thumbnailWithMeta(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;
    .locals 4

    new-instance v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;

    invoke-direct {v1}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;-><init>()V

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduPCSThumbnailWithMeta;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/baidu/pcs/BaiduPCSThumbnail;

    invoke-direct {v2}, Lcom/baidu/pcs/BaiduPCSThumbnail;-><init>()V

    invoke-virtual {v2, v0}, Lcom/baidu/pcs/BaiduPCSThumbnail;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/baidu/pcs/BaiduPCSThumbnail;->thumbnail(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iput-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v2, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->bitmap:Landroid/graphics/Bitmap;

    iput-object v0, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;->bitmap:Landroid/graphics/Bitmap;

    new-instance v0, Lcom/baidu/pcs/BaiduPCSMeta;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSMeta;-><init>()V

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduPCSThumbnailWithMeta;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/baidu/pcs/BaiduPCSMeta;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSMeta;->meta(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v2, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->type:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse$MediaType;

    sget-object v3, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse$MediaType;->Media_Image:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse$MediaType;

    if-ne v2, v3, :cond_3

    instance-of v2, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iput-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget-object v2, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    iget v2, v2, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    if-nez v2, :cond_2

    check-cast v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;

    iput-object v0, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;->metaResponse:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;-><init>()V

    iput-object v0, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-object v0, v1

    goto :goto_0
.end method
