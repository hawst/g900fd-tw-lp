.class Lcom/baidu/pcs/BaiduOAuth$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/baidu/pcs/BaiduOAuth$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/baidu/pcs/BaiduOAuth$1;


# direct methods
.method constructor <init>(Lcom/baidu/pcs/BaiduOAuth$1;)V
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduOAuth$1$1;->this$1:Lcom/baidu/pcs/BaiduOAuth$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$1$1;->this$1:Lcom/baidu/pcs/BaiduOAuth$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduOAuth$1;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->cancel()V

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth mTimeoutEvent run 2 "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$1$1;->this$1:Lcom/baidu/pcs/BaiduOAuth$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduOAuth$1;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$800(Lcom/baidu/pcs/BaiduOAuth;)Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$1$1;->this$1:Lcom/baidu/pcs/BaiduOAuth$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduOAuth$1;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$800(Lcom/baidu/pcs/BaiduOAuth;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth mTimeoutEvent run 2 3 "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$1$1;->this$1:Lcom/baidu/pcs/BaiduOAuth$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduOAuth$1;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$800(Lcom/baidu/pcs/BaiduOAuth;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->removeInstance()V

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$1$1;->this$1:Lcom/baidu/pcs/BaiduOAuth$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduOAuth$1;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$000(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$1$1;->this$1:Lcom/baidu/pcs/BaiduOAuth$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduOAuth$1;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$000(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    move-result-object v0

    const-string v1, "time out"

    invoke-interface {v0, v1}, Lcom/baidu/pcs/BaiduOAuth$DialogListener;->onException(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
