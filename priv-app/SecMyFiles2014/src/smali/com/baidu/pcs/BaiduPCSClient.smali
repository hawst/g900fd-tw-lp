.class public Lcom/baidu/pcs/BaiduPCSClient;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;,
        Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;
    }
.end annotation


# static fields
.field public static final Key_AccessToken:Ljava/lang/String; = "access_token"

.field public static final Key_BDUSS:Ljava/lang/String; = "BDUSS"

.field public static final Key_ExpiresIn:Ljava/lang/String; = "expires_in"

.field public static final Key_UserName:Ljava/lang/String; = "username"

.field public static final Type_Media_MP4_360P:Ljava/lang/String; = "MP4_360P"

.field public static final Type_Media_MP4_480P:Ljava/lang/String; = "MP4_480P"

.field public static final Type_Stream_Audio:Ljava/lang/String; = "audio"

.field public static final Type_Stream_Doc:Ljava/lang/String; = "doc"

.field public static final Type_Stream_Image:Ljava/lang/String; = "image"

.field public static final Type_Stream_Video:Ljava/lang/String; = "video"


# instance fields
.field private mbAccessToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setEquipmentInfo(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/baidu/pcs/BaiduPCSClient;->setEquipmentInfo(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    iput-object p1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->setEquipmentInfo(Landroid/content/Context;)V

    return-void
.end method

.method public static handleExternalEvent(I)V
    .locals 0

    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->removeInstance()V

    return-void
.end method

.method private setEquipmentInfo(Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, Lcom/baidu/pcs/common/BaiduSDKIdentityManager;->getInstance(Landroid/content/Context;)Lcom/baidu/pcs/common/BaiduSDKIdentityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/common/BaiduSDKIdentityManager;->getCID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/pcs/BaiduPCSActionBase;->setTn(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/baidu/pcs/common/BaiduSDKIdentityManager;->getInstance(Landroid/content/Context;)Lcom/baidu/pcs/common/BaiduSDKIdentityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/common/BaiduSDKIdentityManager;->getUA()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/pcs/BaiduPCSActionBase;->setUa(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/baidu/pcs/common/BaiduSDKIdentityManager;->getInstance(Landroid/content/Context;)Lcom/baidu/pcs/common/BaiduSDKIdentityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/common/BaiduSDKIdentityManager;->getUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/pcs/BaiduPCSActionBase;->setUid(Ljava/lang/String;)V

    return-void
.end method

.method public static startSSOService(Landroid/content/Context;Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;)Z
    .locals 2

    const-string v0, "pcs"

    const-string v1, "startSSOService++++++++++++++++"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->instance()Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->startService(Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;Landroid/content/Context;)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static stopSSOService()V
    .locals 1

    invoke-static {}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->instance()Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->stopService()V

    return-void
.end method


# virtual methods
.method public accessToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public audioStream()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public audioStreamWithLimit(II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const-string v0, "audio"

    invoke-virtual {p0, v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public batchDownloadFile(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->batchDownloadFile(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public batchDownloadFile(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0, p2, p3}, Lcom/baidu/pcs/BaiduPCSClient;->batchDownloadFiles(Ljava/util/List;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public batchDownloadFiles(Ljava/util/List;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->batchDownloadFiles(Ljava/util/List;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public batchDownloadFiles(Ljava/util/List;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/baidu/pcs/BaiduPCSStatusListener;",
            ")",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;"
        }
    .end annotation

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDownloader;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDownloader;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDownloader;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/pcs/BaiduPCSDownloader;->batchDownloadFiles(Ljava/util/List;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public copy(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;

    invoke-direct {v1}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;-><init>()V

    iput-object p1, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;->from:Ljava/lang/String;

    iput-object p2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;->to:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->copy(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;

    move-result-object v0

    return-object v0
.end method

.method public copy(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;",
            ">;)",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;"
        }
    .end annotation

    new-instance v0, Lcom/baidu/pcs/BaiduPCSCopy;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSCopy;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSCopy;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSCopy;->copy(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;

    move-result-object v0

    return-object v0
.end method

.method public createFileLink(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSFileLink;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSFileLink;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSFileLink;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSFileLink;->createFileLink(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;

    move-result-object v0

    return-object v0
.end method

.method public deleteFile(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->deleteFiles(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public deleteFileLink(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSFileLink;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSFileLink;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSFileLink;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSFileLink;->deleteFileLink(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public deleteFiles(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;"
        }
    .end annotation

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDeleter;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDeleter;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDeleter;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSDeleter;->deleteFiles(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public diff()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->diff(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;

    move-result-object v0

    return-object v0
.end method

.method public diff(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDiffer;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDiffer;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDiffer;->setAccessToken(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/baidu/pcs/BaiduPCSDiffer;->diff(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;

    move-result-object v0

    return-object v0
.end method

.method public diffStream(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDiffer;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDiffer;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDiffer;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSDiffer;->diff(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;

    move-result-object v0

    return-object v0
.end method

.method public diffStreamWithMeta(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDifferWithMeta;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDifferWithMeta;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDifferWithMeta;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSDifferWithMeta;->diffWithMeta(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;

    move-result-object v0

    return-object v0
.end method

.method public docStream()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const-string v0, "doc"

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public docStreamWithLimit(II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const-string v0, "doc"

    invoke-virtual {p0, v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFile(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFile(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDownloader;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDownloader;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDownloader;->setAccessToken(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/baidu/pcs/BaiduPCSDownloader;->downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDownloader;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDownloader;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDownloader;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/pcs/BaiduPCSDownloader;->downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFileAsMP4360P(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2

    const-string v0, "MP4_360P"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFileAsSpecificCodecType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFileAsMP4360P(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    const-string v0, "MP4_360P"

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFileAsSpecificCodecType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFileAsMP4480P(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2

    const-string v0, "MP4_480P"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFileAsSpecificCodecType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFileAsMP4480P(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    const-string v0, "MP4_480P"

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFileAsSpecificCodecType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFileAsSpecificCodecType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFileAsSpecificCodecType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFileAsSpecificCodecType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDownloader;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDownloader;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDownloader;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/pcs/BaiduPCSDownloader;->downloadFileWithSpecificCodecType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFileFromStream(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/baidu/pcs/BaiduPCSClient;->downloadFileFromStream(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public downloadFileFromStream(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSDownloader;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSDownloader;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSDownloader;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/pcs/BaiduPCSDownloader;->downloadFileFromStream(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    move-result-object v0

    return-object v0
.end method

.method public getBunndle()Landroid/os/Bundle;
    .locals 1

    invoke-static {}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->instance()Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->getLoginStatus()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getMusicCoverArt(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    .locals 2

    const/16 v1, 0x352

    const/16 v0, 0x244

    if-le p3, v1, :cond_0

    move p3, v1

    :cond_0
    if-le p4, v0, :cond_1

    move p4, v0

    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/baidu/pcs/BaiduPCSClient;->thumbnail(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;

    move-result-object v0

    return-object v0
.end method

.method public getMusicStreamingURL(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSMusicStreamingURL;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSMusicStreamingURL;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSMusicStreamingURL;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSMusicStreamingURL;->getMusicStreamingURL(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;

    move-result-object v0

    return-object v0
.end method

.method public getStreamingURL(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSStreamingURL;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSStreamingURL;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSStreamingURL;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSStreamingURL;->getStreamingURL(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;

    move-result-object v0

    return-object v0
.end method

.method public imageStream()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public imageStreamWithLimit(II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const-string v0, "image"

    invoke-virtual {p0, v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public isLogin()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduPCSClient;->getBunndle()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public list(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSList;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSList;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSList;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/pcs/BaiduPCSList;->list(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public listRevision(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListRevisionResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSRevision;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSRevision;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSRevision;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/pcs/BaiduPCSRevision;->listRevision(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListRevisionResponse;

    move-result-object v0

    return-object v0
.end method

.method public logout()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 3

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSClient logout +++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->instance()Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->logout(Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {v1}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;-><init>()V

    if-eqz v0, :cond_0

    const-string v0, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSClient logout success"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    :cond_0
    const-string v0, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSClient logout ---"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method

.method public logout(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 3

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSClient logout(token) +++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->instance()Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->logout(Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {v1}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;-><init>()V

    if-eqz v0, :cond_0

    const-string v0, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSClient logout success"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    :cond_0
    const-string v0, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSClient logout(token) ---"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method

.method public makeDir(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSFolderMaker;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSFolderMaker;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSFolderMaker;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSFolderMaker;->makeDir(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public meta(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSMeta;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSMeta;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSMeta;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSMeta;->meta(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;

    move-result-object v0

    return-object v0
.end method

.method public move(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;

    invoke-direct {v1}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;-><init>()V

    iput-object p1, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;->from:Ljava/lang/String;

    iput-object p2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;->to:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->move(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;

    move-result-object v0

    return-object v0
.end method

.method public move(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;",
            ">;)",
            "Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;"
        }
    .end annotation

    new-instance v0, Lcom/baidu/pcs/BaiduPCSMove;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSMove;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSMove;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/baidu/pcs/BaiduPCSMove;->move(Ljava/util/List;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;

    move-result-object v0

    return-object v0
.end method

.method public quota()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSQuotaInfo;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSQuotaInfo;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSQuotaInfo;->setAccessToken(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSQuotaInfo;->quotaInfo(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;

    move-result-object v0

    return-object v0
.end method

.method public revert(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSRevertResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSRevert;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSRevert;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSRevert;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSRevert;->revert(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSRevertResponse;

    move-result-object v0

    return-object v0
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;Z)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSSearch;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSSearch;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSSearch;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/pcs/BaiduPCSSearch;->search(Ljava/lang/String;Ljava/lang/String;Z)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public setAccessToken(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    return-void
.end method

.method public startOAuth(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;)V
    .locals 2

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSClient startOAuth"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->instance()Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->getAccount(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;)Z

    return-void
.end method

.method public streamWithSpecificMediaType(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public streamWithSpecificMediaType(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSListStream;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSListStream;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSListStream;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/pcs/BaiduPCSListStream;->listStream(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public thumbnail(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;
    .locals 3

    if-lez p2, :cond_0

    if-lez p3, :cond_0

    if-gtz p4, :cond_1

    :cond_0
    new-instance v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;-><init>()V

    iget-object v1, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    const/16 v2, 0x792f

    iput v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    iget-object v1, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;->status:Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    const-string v2, " param error"

    iput-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->message:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/baidu/pcs/BaiduPCSThumbnail;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSThumbnail;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSThumbnail;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/pcs/BaiduPCSThumbnail;->thumbnail(Ljava/lang/String;III)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public uploadFile(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public uploadFile(Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSUploader;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSUploader;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSUploader;->setAccessToken(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/baidu/pcs/BaiduPCSUploader;->uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/baidu/pcs/BaiduPCSClient;->uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;
    .locals 2

    new-instance v0, Lcom/baidu/pcs/BaiduPCSUploader;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSUploader;-><init>()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSClient;->mbAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduPCSUploader;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/pcs/BaiduPCSUploader;->uploadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSStatusListener;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public videoStream()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const-string v0, "video"

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public videoStreamWithLimit(II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;
    .locals 1

    const-string v0, "video"

    invoke-virtual {p0, v0, p1, p2}, Lcom/baidu/pcs/BaiduPCSClient;->streamWithSpecificMediaType(Ljava/lang/String;II)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;

    move-result-object v0

    return-object v0
.end method
