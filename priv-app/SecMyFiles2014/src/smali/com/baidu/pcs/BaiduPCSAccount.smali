.class Lcom/baidu/pcs/BaiduPCSAccount;
.super Ljava/lang/Object;


# instance fields
.field accessToken:Ljava/lang/String;

.field bduss:Ljava/lang/String;

.field expiresDate:Ljava/lang/String;

.field userName:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccount;->accessToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccount;->bduss:Ljava/lang/String;

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccount;->userName:Ljava/lang/String;

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccount;->expiresDate:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Lcom/baidu/pcs/BaiduPCSAccount;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccount;->accessToken:Ljava/lang/String;

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSAccount;->accessToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccount;->bduss:Ljava/lang/String;

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSAccount;->bduss:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccount;->userName:Ljava/lang/String;

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSAccount;->userName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccount;->expiresDate:Ljava/lang/String;

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSAccount;->expiresDate:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
