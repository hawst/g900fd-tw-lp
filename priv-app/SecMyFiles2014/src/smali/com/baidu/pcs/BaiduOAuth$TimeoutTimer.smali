.class Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
.super Ljava/util/Timer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/pcs/BaiduOAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TimeoutTimer"
.end annotation


# instance fields
.field private mIsRunning:Z

.field final synthetic this$0:Lcom/baidu/pcs/BaiduOAuth;


# direct methods
.method constructor <init>(Lcom/baidu/pcs/BaiduOAuth;)V
    .locals 1

    iput-object p1, p0, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    invoke-direct {p0}, Ljava/util/Timer;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->mIsRunning:Z

    return-void
.end method


# virtual methods
.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->mIsRunning:Z

    return v0
.end method

.method public startRunning(Ljava/util/TimerTask;II)V
    .locals 6

    :try_start_0
    iget-boolean v0, p0, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->mIsRunning:Z

    if-nez v0, :cond_0

    int-to-long v2, p2

    int-to-long v4, p3

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->mIsRunning:Z

    return-void

    :catch_0
    move-exception v0

    const-string v0, "pcs"

    const-string v1, "is running -----------"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopRunning()V
    .locals 1

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->cancel()V

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->purge()I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->mIsRunning:Z

    return-void
.end method
