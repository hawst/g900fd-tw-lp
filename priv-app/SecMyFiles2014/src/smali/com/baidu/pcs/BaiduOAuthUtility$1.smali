.class Lcom/baidu/pcs/BaiduOAuthUtility$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/baidu/pcs/BaiduOAuthUtility;->register(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

.field final synthetic val$apiKey:Ljava/lang/String;

.field final synthetic val$sn:Ljava/lang/String;

.field final synthetic val$token:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/baidu/pcs/BaiduOAuthUtility;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    iput-object p2, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$sn:Ljava/lang/String;

    iput-object p3, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$apiKey:Ljava/lang/String;

    iput-object p4, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$token:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$sn:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$apiKey:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$token:Ljava/lang/String;

    if-eqz v1, :cond_1

    :try_start_0
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$apiKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$sn:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    const-string v3, ""

    # invokes: Lcom/baidu/pcs/BaiduOAuthUtility;->toHexString([BLjava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v3}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$000([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "access_token"

    iget-object v6, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$token:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "method"

    iget-object v6, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Value_Register:Ljava/lang/String;
    invoke-static {v6}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$100(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    iget-object v5, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Type:Ljava/lang/String;
    invoke-static {v5}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$200(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Value_Type_IMEI:Ljava/lang/String;
    invoke-static {v6}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$300(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    iget-object v5, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Key_SN:Ljava/lang/String;
    invoke-static {v5}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$400(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->val$sn:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    iget-object v5, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Model:Ljava/lang/String;
    invoke-static {v5}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$500(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    iget-object v4, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Key_ENC:Ljava/lang/String;
    invoke-static {v4}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$600(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Url_Register:Ljava/lang/String;
    invoke-static {v2}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$700(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    invoke-virtual {v2, v3}, Lcom/baidu/pcs/BaiduOAuthUtility;->buildParams(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    move v1, v0

    :goto_0
    if-nez v0, :cond_1

    iget-object v3, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Max_Tries:I
    invoke-static {v3}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$800(Lcom/baidu/pcs/BaiduOAuthUtility;)I

    move-result v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    invoke-virtual {v3, v2}, Lcom/baidu/pcs/BaiduOAuthUtility;->sendHttpRequest(Lorg/apache/http/client/methods/HttpRequestBase;)Lcom/baidu/pcs/BaiduPCSActionBase$PCSRawHTTPResponse;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, v3, Lcom/baidu/pcs/BaiduPCSActionBase$PCSRawHTTPResponse;->response:Lorg/apache/http/HttpResponse;

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v3, v1, 0x1

    iget-object v4, p0, Lcom/baidu/pcs/BaiduOAuthUtility$1;->this$0:Lcom/baidu/pcs/BaiduOAuthUtility;

    # getter for: Lcom/baidu/pcs/BaiduOAuthUtility;->Sleep_Interval:I
    invoke-static {v4}, Lcom/baidu/pcs/BaiduOAuthUtility;->access$900(Lcom/baidu/pcs/BaiduOAuthUtility;)I

    move-result v4

    mul-int/2addr v3, v4

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    :cond_1
    :goto_2
    return-void

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method
