.class public final Lcom/baidu/pcs/BaiduPCSActionInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSRevertResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileRevisionInfo;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListRevisionResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailWithMetaResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageResolutionInfoResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSStreamingURLResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileLinkResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffWithMetaResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDiffResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSDifferEntryInfo;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToInfo;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileFromToResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSListInfoResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSImageMetaResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSVideoMetaResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSAudioMetaResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSMetaResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileInfoResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSThumbnailResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSQuotaResponse;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSCommonFileInfo;,
        Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
