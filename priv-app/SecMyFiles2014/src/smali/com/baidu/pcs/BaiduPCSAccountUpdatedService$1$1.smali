.class Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->onComplete(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

.field final synthetic val$values:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iput-object p2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->val$values:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->val$values:Landroid/os/Bundle;

    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSAccountUpdatedService showLoginDialog null != token +++"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/baidu/pcs/BaiduOAuthUtility;

    invoke-direct {v2}, Lcom/baidu/pcs/BaiduOAuthUtility;-><init>()V

    invoke-virtual {v2, v1}, Lcom/baidu/pcs/BaiduOAuthUtility;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/baidu/pcs/BaiduOAuthUtility;->getUserName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$context:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$apiKey:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Lcom/baidu/pcs/BaiduOAuthUtility;->register(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/baidu/pcs/BaiduPCSAccount;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSAccount;-><init>()V

    iput-object v1, v0, Lcom/baidu/pcs/BaiduPCSAccount;->accessToken:Ljava/lang/String;

    iput-object v3, v0, Lcom/baidu/pcs/BaiduPCSAccount;->userName:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->val$values:Landroid/os/Bundle;

    const-string v4, "expires_in"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/baidu/pcs/BaiduPCSAccount;->expiresDate:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->val$values:Landroid/os/Bundle;

    const-string v4, "BDUSS"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/baidu/pcs/BaiduPCSAccount;->bduss:Ljava/lang/String;

    iget-object v4, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iget-object v4, v4, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # getter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v4}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$000(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v4

    iget-object v5, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iget-object v5, v5, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # setter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v5, v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$002(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Lcom/baidu/pcs/BaiduPCSAccount;

    iget-object v5, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iget-object v5, v5, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$context:Landroid/content/Context;

    invoke-static {v5, v0}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->addAccount2ContentProvider(Landroid/content/Context;Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$uiThreadHandler:Landroid/os/Handler;

    new-instance v4, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1$1;

    invoke-direct {v4, p0, v1, v3, v2}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1$1;-><init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService showLoginDialog null != token ---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->checkInsallProvider(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;->this$1:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    iget-object v0, v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # setter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v0, v4}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$002(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Lcom/baidu/pcs/BaiduPCSAccount;

    goto :goto_0
.end method
