.class Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$RemoveAccountTask;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoveAccountTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;


# direct methods
.method private constructor <init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)V
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$RemoveAccountTask;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$RemoveAccountTask;-><init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$RemoveAccountTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 2

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService remove account task"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    new-instance v0, Lcom/baidu/pcs/BaiduOAuthUtility;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduOAuthUtility;-><init>()V

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/baidu/pcs/BaiduOAuthUtility;->setAccessToken(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduOAuthUtility;->logout()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
