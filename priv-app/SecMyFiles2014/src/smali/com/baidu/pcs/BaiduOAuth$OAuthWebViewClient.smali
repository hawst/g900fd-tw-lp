.class public Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;
.super Landroid/webkit/WebViewClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/pcs/BaiduOAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OAuthWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/baidu/pcs/BaiduOAuth;


# direct methods
.method public constructor <init>(Lcom/baidu/pcs/BaiduOAuth;)V
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 8

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onPageFinished +++ "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->stopRunning()V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/webkit/WebView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    # invokes: Lcom/baidu/pcs/BaiduOAuth;->dismissSpinner()V
    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->access$100()V

    if-eqz p2, :cond_1

    const-string v0, "/login_success"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onPageFinished Key_LoginSuccess +++ "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # invokes: Lcom/baidu/pcs/BaiduOAuth;->parseUrl(Ljava/lang/String;)Landroid/os/Bundle;
    invoke-static {v0, p2}, Lcom/baidu/pcs/BaiduOAuth;->access$600(Lcom/baidu/pcs/BaiduOAuth;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    const-string v0, "expires_in"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    mul-long/2addr v0, v6

    add-long/2addr v0, v4

    const-wide/32 v4, 0x15180

    sub-long/2addr v0, v4

    :goto_0
    const-string v3, "expires_in"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "BDUSS"

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # invokes: Lcom/baidu/pcs/BaiduOAuth;->getBduss()Ljava/lang/String;
    invoke-static {v1}, Lcom/baidu/pcs/BaiduOAuth;->access$700(Lcom/baidu/pcs/BaiduOAuth;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$000(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/baidu/pcs/BaiduOAuth$DialogListener;->onComplete(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->removeInstance()V

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onPageFinished Key_LoginSuccess --- "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onPageFinished ---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onPageStarted +++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/webkit/WebView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onPageStarted 2 +++ "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    const-string v0, "authorize?response_type=token"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    # invokes: Lcom/baidu/pcs/BaiduOAuth;->dismissSpinner()V
    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->access$100()V

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->access$400()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->stopRunning()V

    :cond_1
    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutEvent:Ljava/util/TimerTask;
    invoke-static {v1}, Lcom/baidu/pcs/BaiduOAuth;->access$500(Lcom/baidu/pcs/BaiduOAuth;)Ljava/util/TimerTask;

    move-result-object v1

    const v2, 0x15f90

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->startRunning(Ljava/util/TimerTask;II)V

    :cond_2
    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onPageStarted ---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onReceivedError +++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/webkit/WebView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$000(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->access$000(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/baidu/pcs/BaiduOAuth$DialogListener;->onException(Ljava/lang/String;)V

    :cond_0
    # invokes: Lcom/baidu/pcs/BaiduOAuth;->dismissSpinner()V
    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->access$100()V

    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->removeInstance()V

    :cond_1
    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onReceivedError ---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth onReceivedError +++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "BDPCSSSOSDK"

    const-string v2, "BaiduOAuth shouldOverrideUrlLoading +++"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    const-string v1, "error=access_denied"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "BDPCSSSOSDK"

    const-string v2, "BaiduOAuth Key_UserDenied +++"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    invoke-static {v1}, Lcom/baidu/pcs/BaiduOAuth;->access$000(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    invoke-static {v1}, Lcom/baidu/pcs/BaiduOAuth;->access$000(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/baidu/pcs/BaiduOAuth$DialogListener;->onCancel()V

    :cond_0
    # invokes: Lcom/baidu/pcs/BaiduOAuth;->dismissSpinner()V
    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->access$100()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    invoke-static {v1}, Lcom/baidu/pcs/BaiduOAuth;->access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    # getter for: Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    invoke-static {v1}, Lcom/baidu/pcs/BaiduOAuth;->access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->stopRunning()V

    :cond_1
    invoke-virtual {p1}, Landroid/webkit/WebView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->removeInstance()V

    :cond_2
    :goto_0
    const-string v1, "BDPCSSSOSDK"

    const-string v2, "BaiduOAuth shouldOverrideUrlLoading ---"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_3
    const-string v1, "http:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "https:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    iget-object v3, p0, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;->this$0:Lcom/baidu/pcs/BaiduOAuth;

    invoke-virtual {v3}, Lcom/baidu/pcs/BaiduOAuth;->getContext()Landroid/content/Context;

    move-result-object v3

    # invokes: Lcom/baidu/pcs/BaiduOAuth;->startActivitySafely(Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {v2, v3, v1}, Lcom/baidu/pcs/BaiduOAuth;->access$300(Lcom/baidu/pcs/BaiduOAuth;Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
