.class Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/baidu/pcs/BaiduOAuth$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->showLoginDialog(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

.field final synthetic val$apiKey:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$listener:Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;

.field final synthetic val$uiThreadHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    iput-object p2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$listener:Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;

    iput-object p3, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$apiKey:Ljava/lang/String;

    iput-object p5, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$uiThreadHandler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$listener:Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$listener:Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;

    invoke-interface {v0}, Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;->onCancel()V

    :cond_0
    return-void
.end method

.method public onComplete(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService showLoginDialog onComplete +++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$listener:Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;

    invoke-direct {v1, p0, p1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1$1;-><init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;Landroid/os/Bundle;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService showLoginDialog onComplete ---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onException(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$listener:Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;->val$listener:Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;

    invoke-interface {v0, p1}, Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;->onException(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
