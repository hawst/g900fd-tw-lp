.class Lcom/baidu/pcs/BaiduOAuth;
.super Landroid/app/Dialog;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;,
        Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;,
        Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    }
.end annotation


# static fields
.field private static final Txt_PleaseWait:Ljava/lang/String; = "\u8bf7\u7b49\u5f85..."

.field private static mInstance:Lcom/baidu/pcs/BaiduOAuth;

.field private static mSpinner:Landroid/app/ProgressDialog;


# instance fields
.field private final CookieURL:Ljava/lang/String;

.field private final Key_LoginSuccess:Ljava/lang/String;

.field private final Key_StartLoading:Ljava/lang/String;

.field private final Key_UserDenied:Ljava/lang/String;

.field private final Msg_Timeout:Ljava/lang/String;

.field private final State_FinishLoading:I

.field private final Timeout_Interval:I

.field private final Url_Login:Ljava/lang/String;

.field private final Value_Margin:I

.field private mContent:Landroid/widget/FrameLayout;

.field private mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;

.field private mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

.field private mTimeoutEvent:Ljava/util/TimerTask;

.field private mUrlToLoad:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    sput-object v0, Lcom/baidu/pcs/BaiduOAuth;->mInstance:Lcom/baidu/pcs/BaiduOAuth;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x1030009

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    new-instance v0, Lcom/baidu/pcs/BaiduOAuth$1;

    invoke-direct {v0, p0}, Lcom/baidu/pcs/BaiduOAuth$1;-><init>(Lcom/baidu/pcs/BaiduOAuth;)V

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mTimeoutEvent:Ljava/util/TimerTask;

    iput-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    iput-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mContent:Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mUrlToLoad:Ljava/lang/String;

    new-instance v0, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    invoke-direct {v0, p0}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;-><init>(Lcom/baidu/pcs/BaiduOAuth;)V

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    const-string v0, "error=access_denied"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->Key_UserDenied:Ljava/lang/String;

    const-string v0, "/login_success"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->Key_LoginSuccess:Ljava/lang/String;

    const-string v0, "authorize?response_type=token"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->Key_StartLoading:Ljava/lang/String;

    const-string v0, "openapi.baidu.com"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->CookieURL:Ljava/lang/String;

    const/16 v0, 0xa

    iput v0, p0, Lcom/baidu/pcs/BaiduOAuth;->Value_Margin:I

    const v0, 0x15f90

    iput v0, p0, Lcom/baidu/pcs/BaiduOAuth;->Timeout_Interval:I

    const/16 v0, 0x64

    iput v0, p0, Lcom/baidu/pcs/BaiduOAuth;->State_FinishLoading:I

    const-string v0, "time out"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->Msg_Timeout:Ljava/lang/String;

    const-string v0, "https://openapi.baidu.com/oauth/2.0/authorize?response_type=token&force_login=1&redirect_uri=oob&display=mobile&client_id="

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->Url_Login:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$DialogListener;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    return-object v0
.end method

.method static synthetic access$100()V
    .locals 0

    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->dismissSpinner()V

    return-void
.end method

.method static synthetic access$200(Lcom/baidu/pcs/BaiduOAuth;)Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/baidu/pcs/BaiduOAuth;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/baidu/pcs/BaiduOAuth;->startActivitySafely(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$400()Landroid/app/ProgressDialog;
    .locals 1

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/baidu/pcs/BaiduOAuth;)Ljava/util/TimerTask;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mTimeoutEvent:Ljava/util/TimerTask;

    return-object v0
.end method

.method static synthetic access$600(Lcom/baidu/pcs/BaiduOAuth;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    invoke-direct {p0, p1}, Lcom/baidu/pcs/BaiduOAuth;->parseUrl(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/baidu/pcs/BaiduOAuth;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduOAuth;->getBduss()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/baidu/pcs/BaiduOAuth;)Landroid/webkit/WebView;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method private static createSpinner(Landroid/content/Context;)Landroid/app/ProgressDialog;
    .locals 2

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    const-string v1, "\u8bf7\u7b49\u5f85..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private decodeUrl(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 8

    const/4 v1, 0x0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    if-eqz p1, :cond_0

    const-string v0, "&"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v6, v5, v1

    invoke-static {v6}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v5, v5, v7

    invoke-static {v5}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private static dismissSpinner()V
    .locals 1

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private getBduss()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth getBduss "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    const-string v2, "openapi.baidu.com"

    invoke-virtual {v0, v2}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-object v3, v2, v0

    const-string v4, "BDUSS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    if-lt v4, v5, :cond_0

    aget-object v0, v3, v5

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static instance(Landroid/content/Context;)Lcom/baidu/pcs/BaiduOAuth;
    .locals 1

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mInstance:Lcom/baidu/pcs/BaiduOAuth;

    if-nez v0, :cond_0

    new-instance v0, Lcom/baidu/pcs/BaiduOAuth;

    invoke-direct {v0, p0}, Lcom/baidu/pcs/BaiduOAuth;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/baidu/pcs/BaiduOAuth;->mInstance:Lcom/baidu/pcs/BaiduOAuth;

    :cond_0
    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mInstance:Lcom/baidu/pcs/BaiduOAuth;

    return-object v0
.end method

.method private parseUrl(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduOAuth;->decodeUrl(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/baidu/pcs/BaiduOAuth;->decodeUrl(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public static removeInstance()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->dismissSpinner()V

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mInstance:Lcom/baidu/pcs/BaiduOAuth;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/baidu/pcs/BaiduOAuth;->mInstance:Lcom/baidu/pcs/BaiduOAuth;

    invoke-virtual {v0}, Lcom/baidu/pcs/BaiduOAuth;->dismiss()V

    :cond_0
    sput-object v1, Lcom/baidu/pcs/BaiduOAuth;->mSpinner:Landroid/app/ProgressDialog;

    sput-object v1, Lcom/baidu/pcs/BaiduOAuth;->mInstance:Lcom/baidu/pcs/BaiduOAuth;

    return-void
.end method

.method private setUpWebView()V
    .locals 7

    const/4 v6, -0x1

    const/4 v5, 0x1

    const/16 v4, 0xa

    const/4 v3, 0x0

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuth;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mContent:Landroid/widget/FrameLayout;

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuth;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuth;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;

    invoke-direct {v2, p0}, Lcom/baidu/pcs/BaiduOAuth$OAuthWebViewClient;-><init>(Lcom/baidu/pcs/BaiduOAuth;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mContent:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mUrlToLoad:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth mWebView.loadUrl(mUrlToLoad)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mContent:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduOAuth;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method private startActivitySafely(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/high16 v0, 0x10000000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BDPCSSSOSDK"

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "BDPCSSSOSDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Launcher does not have the permission to launch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Make sure to create a MAIN intent-filter for the corresponding activity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "or use the exported attribute for this activity."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/baidu/pcs/BaiduOAuth;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuth;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/pcs/BaiduOAuth;->createSpinner(Landroid/content/Context;)Landroid/app/ProgressDialog;

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduOAuth;->setUpWebView()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "BDPCSSSOSDK"

    const-string v2, "BaiduOAuth onKeyDown +++"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->stopLoading()V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mTimeoutCount:Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;

    invoke-virtual {v1}, Lcom/baidu/pcs/BaiduOAuth$TimeoutTimer;->stopRunning()V

    :cond_0
    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    invoke-interface {v1}, Lcom/baidu/pcs/BaiduOAuth$DialogListener;->onCancel()V

    :cond_1
    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->removeInstance()V

    :cond_2
    const-string v1, "BDPCSSSOSDK"

    const-string v2, "BaiduOAuth onKeyDown ---"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public startDialogAuth(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduOAuth$DialogListener;)Z
    .locals 3

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth startDialogAuth +++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeSessionCookie()V

    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    iput-object p4, p0, Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    const-string v0, "android.permission.INTERNET"

    invoke-virtual {p1, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mListener:Lcom/baidu/pcs/BaiduOAuth$DialogListener;

    const-string v1, "Application requires permission to access the Internet"

    invoke-interface {v0, v1}, Lcom/baidu/pcs/BaiduOAuth$DialogListener;->onException(Ljava/lang/String;)V

    :cond_1
    const-string v0, ""

    array-length v1, p2

    if-lez v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-static {v1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://openapi.baidu.com/oauth/2.0/authorize?response_type=token&force_login=1&redirect_uri=oob&display=mobile&client_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuth;->mUrlToLoad:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuth;->show()V

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduOAuth startDialogAuth ---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
