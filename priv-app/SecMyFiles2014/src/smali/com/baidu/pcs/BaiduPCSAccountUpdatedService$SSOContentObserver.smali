.class Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SSOContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;


# direct methods
.method private constructor <init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Landroid/os/Handler;Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;-><init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "BDPCSSSOSDK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BaiduPCSAccountUpdatedService onChange +++ ***"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # getter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$200(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->queryAccountInContentProvider(Landroid/content/Context;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # invokes: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->isAccountValid(Lcom/baidu/pcs/BaiduPCSAccount;)Z
    invoke-static {v2, v1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$300(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "BDPCSSSOSDK"

    const-string v3, "BaiduPCSAccountUpdatedService onChange isAccountValid"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # getter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v2}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$000(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/baidu/pcs/BaiduPCSAccount;->equals(Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # setter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v2, v1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$002(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Lcom/baidu/pcs/BaiduPCSAccount;

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # getter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;
    invoke-static {v2}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$400(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # getter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;
    invoke-static {v2}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$400(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # invokes: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->makeBundleByAccount(Lcom/baidu/pcs/BaiduPCSAccount;)Landroid/os/Bundle;
    invoke-static {v3, v1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$500(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;->oauthChanged(Landroid/os/Bundle;)V

    :cond_0
    :goto_0
    const-string v1, "BDPCSSSOSDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BaiduPCSAccountUpdatedService onChange ---  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # getter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v3}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$000(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # getter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$000(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService onChange false == found"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # invokes: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->notifyLogoutEvent()V
    invoke-static {v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$600(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)V

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # setter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v0, v4}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$002(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Lcom/baidu/pcs/BaiduPCSAccount;

    :cond_1
    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService onChange ---"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;->this$0:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    # setter for: Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;
    invoke-static {v1, v4}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->access$002(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Lcom/baidu/pcs/BaiduPCSAccount;

    goto :goto_0
.end method
