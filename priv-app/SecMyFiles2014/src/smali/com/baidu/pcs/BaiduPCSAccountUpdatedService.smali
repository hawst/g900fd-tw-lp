.class Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$RemoveAccountTask;,
        Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BDPCSSSOSDK"

.field private static mInstance:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;


# instance fields
.field private mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

.field private mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

.field private mbPermission:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mInstance:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "basic"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "netdisk"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "pcs_doc"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "pcs_album"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "pcs_music"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "pcs_video"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mbPermission:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Lcom/baidu/pcs/BaiduPCSAccount;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    return-object v0
.end method

.method static synthetic access$002(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Lcom/baidu/pcs/BaiduPCSAccount;
    .locals 0

    iput-object p1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    return-object p1
.end method

.method static synthetic access$200(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->isAccountValid(Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccount;)Landroid/os/Bundle;
    .locals 1

    invoke-direct {p0, p1}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->makeBundleByAccount(Lcom/baidu/pcs/BaiduPCSAccount;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;)V
    .locals 0

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->notifyLogoutEvent()V

    return-void
.end method

.method public static instance()Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;
    .locals 2

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "instance"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mInstance:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    if-nez v0, :cond_0

    new-instance v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;-><init>()V

    sput-object v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mInstance:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    :cond_0
    sget-object v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mInstance:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;

    return-object v0
.end method

.method private isAccountValid(Lcom/baidu/pcs/BaiduPCSAccount;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/baidu/pcs/BaiduPCSAccount;->accessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/baidu/pcs/BaiduPCSAccount;->userName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/baidu/pcs/BaiduPCSAccount;->expiresDate:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/baidu/pcs/BaiduPCSAccount;->expiresDate:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->isTokenExpired(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTokenExpired(Ljava/lang/String;)Z
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private makeBundleByAccount(Lcom/baidu/pcs/BaiduPCSAccount;)Landroid/os/Bundle;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "access_token"

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSAccount;->accessToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "BDUSS"

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSAccount;->bduss:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "username"

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSAccount;->userName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "expires_in"

    iget-object v2, p1, Lcom/baidu/pcs/BaiduPCSAccount;->expiresDate:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method private notifyLogoutEvent()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService notifyLogoutEvent +++"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->delAccountInContentProvider(Landroid/content/Context;)Z

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

    invoke-interface {v0}, Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;->logout()V

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService notifyLogoutEvent null != mListener"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService notifyLogoutEvent ---"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private readAccount()Lcom/baidu/pcs/BaiduPCSAccount;
    .locals 3

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService readAccount +++"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->queryAccountInContentProvider(Landroid/content/Context;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->isAccountValid(Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->delAccountInContentProvider(Landroid/content/Context;)Z

    :cond_0
    const-string v1, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSAccountUpdatedService readAccount ---"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private registerContentObserver()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "content://com.baidu.pcs_ss.sso.providers.acoountprovider/accountInfo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->unRegisterContentObserver()V

    iput-object v3, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    :cond_0
    new-instance v1, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2, v3}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;-><init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Landroid/os/Handler;Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;)V

    iput-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method private showLoginDialog(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;)V
    .locals 8

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "BaiduPCSAccountUpdatedService showLoginDialog +++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-static {p1}, Lcom/baidu/pcs/BaiduOAuth;->instance(Landroid/content/Context;)Lcom/baidu/pcs/BaiduOAuth;

    move-result-object v6

    iget-object v7, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mbPermission:[Ljava/lang/String;

    new-instance v0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;-><init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v6, p1, v7, p2, v0}, Lcom/baidu/pcs/BaiduOAuth;->startDialogAuth(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Lcom/baidu/pcs/BaiduOAuth$DialogListener;)Z

    return-void
.end method

.method private unRegisterContentObserver()V
    .locals 2

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mSSOObserver:Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$SSOContentObserver;

    :cond_0
    return-void
.end method


# virtual methods
.method public getAccount(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;)Z
    .locals 4

    const/4 v1, 0x1

    const-string v0, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSAccountUpdatedService getAccount +++"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-eqz p3, :cond_2

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    invoke-direct {p0, v2}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->isAccountValid(Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSAccountUpdatedService getAccount isAccountValid"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->makeBundleByAccount(Lcom/baidu/pcs/BaiduPCSAccount;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;->onComplete(Landroid/os/Bundle;)V

    move v0, v1

    :cond_0
    if-nez v0, :cond_1

    const-string v2, "BDPCSSSOSDK"

    const-string v3, "BaiduPCSAccountUpdatedService getAccount ret"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->queryAccountInContentProvider(Landroid/content/Context;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->isAccountValid(Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v0, "BDPCSSSOSDK"

    const-string v3, "BaiduPCSAccountUpdatedService getAccount isAccountValid 2"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    invoke-direct {p0, v2}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->makeBundleByAccount(Lcom/baidu/pcs/BaiduPCSAccount;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;->onComplete(Landroid/os/Bundle;)V

    move v0, v1

    :cond_1
    if-nez v0, :cond_2

    const-string v1, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSAccountUpdatedService getAccount showLoginDialog"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->showLoginDialog(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/pcs/BaiduPCSClient$OAuthListener;)V

    :cond_2
    const-string v1, "BDPCSSSOSDK"

    const-string v2, "BaiduPCSAccountUpdatedService getAccount ---"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getLoginStatus()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->queryAccountInContentProvider(Landroid/content/Context;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    :cond_0
    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->isAccountValid(Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->delAccountInContentProvider(Landroid/content/Context;)Z

    :cond_1
    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->makeBundleByAccount(Lcom/baidu/pcs/BaiduPCSAccount;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public logout(Ljava/lang/String;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "BDPCSSSOSDK"

    const-string v3, "BaiduPCSAccountUpdatedService logout +++"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->delAccountInContentProvider(Landroid/content/Context;)Z

    iget-object v2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/baidu/pcs/BaiduAccountContentResolverHelper;->queryAccountInContentProvider(Landroid/content/Context;)Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v2

    if-nez v2, :cond_1

    iput-object v4, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, "BDPCSSSOSDK"

    const-string v3, "BaiduPCSAccountUpdatedService RemoveAccountTask"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$RemoveAccountTask;

    invoke-direct {v2, p0, v4}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$RemoveAccountTask;-><init>(Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$1;)V

    new-array v3, v0, [Ljava/lang/String;

    aput-object p1, v3, v1

    invoke-virtual {v2, v3}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService$RemoveAccountTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    const-string v1, "BDPCSSSOSDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BaiduPCSAccountUpdatedService logout ---"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public setOAuthInfo(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Lcom/baidu/pcs/BaiduPCSAccount;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSAccount;-><init>()V

    const-string v1, "access_token"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/baidu/pcs/BaiduPCSAccount;->accessToken:Ljava/lang/String;

    const-string v1, "username"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/baidu/pcs/BaiduPCSAccount;->userName:Ljava/lang/String;

    const-string v1, "BDUSS"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/baidu/pcs/BaiduPCSAccount;->bduss:Ljava/lang/String;

    const-string v1, "expires_in"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/baidu/pcs/BaiduPCSAccount;->expiresDate:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->isAccountValid(Lcom/baidu/pcs/BaiduPCSAccount;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    :cond_0
    return-void
.end method

.method public startService(Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;Landroid/content/Context;)V
    .locals 2

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "startService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p2, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->registerContentObserver()V

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->readAccount()Lcom/baidu/pcs/BaiduPCSAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mAccount:Lcom/baidu/pcs/BaiduPCSAccount;

    return-void
.end method

.method public stopService()V
    .locals 2

    const-string v0, "BDPCSSSOSDK"

    const-string v1, "stopService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->unRegisterContentObserver()V

    invoke-static {}, Lcom/baidu/pcs/BaiduOAuth;->removeInstance()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/baidu/pcs/BaiduPCSAccountUpdatedService;->mListener:Lcom/baidu/pcs/BaiduPCSClient$SSOAccountListener;

    return-void
.end method
