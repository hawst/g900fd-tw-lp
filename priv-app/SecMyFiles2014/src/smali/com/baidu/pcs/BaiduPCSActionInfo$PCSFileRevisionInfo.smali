.class public Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileRevisionInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/pcs/BaiduPCSActionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PCSFileRevisionInfo"
.end annotation


# instance fields
.field public path:Ljava/lang/String;

.field public revision:Ljava/lang/String;

.field public size:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileRevisionInfo;->path:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileRevisionInfo;->size:J

    iput-object v2, p0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSFileRevisionInfo;->revision:Ljava/lang/String;

    return-void
.end method
