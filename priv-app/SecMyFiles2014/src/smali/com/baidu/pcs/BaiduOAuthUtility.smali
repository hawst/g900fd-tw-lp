.class Lcom/baidu/pcs/BaiduOAuthUtility;
.super Lcom/baidu/pcs/BaiduPCSActionBase;


# instance fields
.field private Key_ENC:Ljava/lang/String;

.field private Key_Format:Ljava/lang/String;

.field private Key_Logout_Result:Ljava/lang/String;

.field private Key_Model:Ljava/lang/String;

.field private Key_SN:Ljava/lang/String;

.field private Key_Type:Ljava/lang/String;

.field public Key_UserName:Ljava/lang/String;

.field private Max_Tries:I

.field private Sleep_Interval:I

.field private Url_ExpireToken:Ljava/lang/String;

.field private Url_Register:Ljava/lang/String;

.field private Url_UserInfo:Ljava/lang/String;

.field private Value_Format:Ljava/lang/String;

.field private Value_Register:Ljava/lang/String;

.field private Value_Type_IMEI:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduPCSActionBase;-><init>()V

    const-string v0, "https://openapi.baidu.com/rest/2.0/passport/auth/expireSession"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Url_ExpireToken:Ljava/lang/String;

    const-string v0, "https://openapi.baidu.com/rest/2.0/passport/users/getinfo"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Url_UserInfo:Ljava/lang/String;

    const-string v0, "https://pcs.baidu.com/rest/2.0/pcs/coupon"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Url_Register:Ljava/lang/String;

    const-string v0, "username"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_UserName:Ljava/lang/String;

    const-string v0, "format"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Format:Ljava/lang/String;

    const-string v0, "json"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Value_Format:Ljava/lang/String;

    const-string v0, "result"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Logout_Result:Ljava/lang/String;

    const-string v0, "register"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Value_Register:Ljava/lang/String;

    const-string v0, "type"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Type:Ljava/lang/String;

    const-string v0, "imei"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Value_Type_IMEI:Ljava/lang/String;

    const-string v0, "sn"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_SN:Ljava/lang/String;

    const-string v0, "enc"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_ENC:Ljava/lang/String;

    const-string v0, "model"

    iput-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Model:Ljava/lang/String;

    const/16 v0, 0x8

    iput v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Max_Tries:I

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Sleep_Interval:I

    return-void
.end method

.method static synthetic access$000([BLjava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0, p1}, Lcom/baidu/pcs/BaiduOAuthUtility;->toHexString([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Value_Register:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Type:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Value_Type_IMEI:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_SN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Model:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_ENC:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/baidu/pcs/BaiduOAuthUtility;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Url_Register:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/baidu/pcs/BaiduOAuthUtility;)I
    .locals 1

    iget v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Max_Tries:I

    return v0
.end method

.method static synthetic access$900(Lcom/baidu/pcs/BaiduOAuthUtility;)I
    .locals 1

    iget v0, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Sleep_Interval:I

    return v0
.end method

.method private static toHexString([BLjava/lang/String;)Ljava/lang/String;
    .locals 6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    const/16 v4, 0x30

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getUserName()Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuthUtility;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "access_token"

    invoke-direct {v3, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Url_UserInfo:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v2}, Lcom/baidu/pcs/BaiduOAuthUtility;->buildParams(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/baidu/pcs/BaiduOAuthUtility;->sendHttpRequest(Lorg/apache/http/client/methods/HttpRequestBase;)Lcom/baidu/pcs/BaiduPCSActionBase$PCSRawHTTPResponse;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/baidu/pcs/BaiduPCSActionBase$PCSRawHTTPResponse;->response:Lorg/apache/http/HttpResponse;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v1, v1, Lcom/baidu/pcs/BaiduPCSActionBase$PCSRawHTTPResponse;->response:Lorg/apache/http/HttpResponse;

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_UserName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_UserName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "-"
    :try_end_0
    .catch Lorg/apache/http/ParseException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method public logout()Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    .locals 6

    const/4 v1, 0x0

    const-string v0, "BDPCSSSOSDK"

    const-string v2, "BaiduOAuthUtility logout +++"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;

    invoke-direct {v0}, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;-><init>()V

    invoke-virtual {p0}, Lcom/baidu/pcs/BaiduOAuthUtility;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "access_token"

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    iget-object v4, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Format:Ljava/lang/String;

    iget-object v5, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Value_Format:Ljava/lang/String;

    invoke-direct {v2, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Url_ExpireToken:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v3}, Lcom/baidu/pcs/BaiduOAuthUtility;->buildParams(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v3, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/baidu/pcs/BaiduOAuthUtility;->sendHttpRequest(Lorg/apache/http/client/methods/HttpRequestBase;)Lcom/baidu/pcs/BaiduPCSActionBase$PCSRawHTTPResponse;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/baidu/pcs/BaiduPCSActionBase$PCSRawHTTPResponse;->response:Lorg/apache/http/HttpResponse;

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v2, v2, Lcom/baidu/pcs/BaiduPCSActionBase$PCSRawHTTPResponse;->response:Lorg/apache/http/HttpResponse;

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Logout_Result:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/baidu/pcs/BaiduOAuthUtility;->Key_Logout_Result:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v3, v2, :cond_1

    :goto_0
    iput v1, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I
    :try_end_0
    .catch Lorg/apache/http/ParseException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const-string v1, "BDPCSSSOSDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BaiduOAuthUtility logout ---"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-super {p0, v3}, Lcom/baidu/pcs/BaiduPCSActionBase;->parseSimplefiedResponse(Lorg/json/JSONObject;)Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;
    :try_end_1
    .catch Lorg/apache/http/ParseException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    :cond_3
    iput v1, v0, Lcom/baidu/pcs/BaiduPCSActionInfo$PCSSimplefiedResponse;->errorCode:I

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public register(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-super {p0}, Lcom/baidu/pcs/BaiduPCSActionBase;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/baidu/pcs/BaiduOAuthUtility$1;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/baidu/pcs/BaiduOAuthUtility$1;-><init>(Lcom/baidu/pcs/BaiduOAuthUtility;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method
