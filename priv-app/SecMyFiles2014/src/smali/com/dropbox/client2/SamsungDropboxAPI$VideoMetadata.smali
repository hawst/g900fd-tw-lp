.class public final Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMetadata"
.end annotation


# static fields
.field public static final JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/client2/jsonextract/JsonExtractor",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final bitrate:Ljava/lang/Integer;

.field public final duration:Ljava/lang/Integer;

.field public final height:Ljava/lang/Integer;

.field public final width:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 667
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata$1;

    invoke-direct {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata$1;-><init>()V

    sput-object v0, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "width"    # Ljava/lang/Integer;
    .param p2, "height"    # Ljava/lang/Integer;
    .param p3, "duration"    # Ljava/lang/Integer;
    .param p4, "bitrate"    # Ljava/lang/Integer;

    .prologue
    .line 660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 661
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->width:Ljava/lang/Integer;

    .line 662
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->height:Ljava/lang/Integer;

    .line 663
    iput-object p3, p0, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->duration:Ljava/lang/Integer;

    .line 664
    iput-object p4, p0, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->bitrate:Ljava/lang/Integer;

    .line 665
    return-void
.end method
