.class public final Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlbumMetadata"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x132df1aL


# instance fields
.field public final count:J

.field public final cover:Lcom/dropbox/client2/DropboxAPI$Entry;

.field public final id:Ljava/lang/String;

.field public final modified:Ljava/util/Date;

.field public final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/util/Date;Lcom/dropbox/client2/DropboxAPI$Entry;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "count"    # J
    .param p5, "modified"    # Ljava/util/Date;
    .param p6, "cover"    # Lcom/dropbox/client2/DropboxAPI$Entry;

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->name:Ljava/lang/String;

    .line 251
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->id:Ljava/lang/String;

    .line 252
    iput-wide p3, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->count:J

    .line 253
    iput-object p5, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->modified:Ljava/util/Date;

    .line 254
    iput-object p6, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->cover:Lcom/dropbox/client2/DropboxAPI$Entry;

    .line 255
    return-void
.end method

.method public static extractFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    .locals 10
    .param p0, "json"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v0

    .line 262
    .local v0, "jAlbum":Lcom/dropbox/client2/jsonextract/JsonMap;
    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v2

    .line 263
    .local v2, "name":Ljava/lang/String;
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v3

    .line 264
    .local v3, "id":Ljava/lang/String;
    const-string v1, "count"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt64()J

    move-result-wide v4

    .line 266
    .local v4, "count":J
    const-string v1, "modified"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v8

    .line 267
    .local v8, "modifiedField":Lcom/dropbox/client2/jsonextract/JsonThing;
    invoke-virtual {v8}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v9

    .line 268
    .local v9, "modifiedString":Ljava/lang/String;
    invoke-static {v9}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    .line 269
    .local v6, "modified":Ljava/util/Date;
    if-nez v6, :cond_0

    .line 270
    const-string v1, "invalid date format"

    invoke-virtual {v8, v1}, Lcom/dropbox/client2/jsonextract/JsonThing;->error(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonExtractionException;

    move-result-object v1

    throw v1

    .line 273
    :cond_0
    const-string v1, "cover"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v1

    # invokes: Lcom/dropbox/client2/SamsungDropboxAPI;->extractEntryFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/DropboxAPI$Entry;
    invoke-static {v1}, Lcom/dropbox/client2/SamsungDropboxAPI;->access$000(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/DropboxAPI$Entry;

    move-result-object v7

    .line 274
    .local v7, "coverImage":Lcom/dropbox/client2/DropboxAPI$Entry;
    new-instance v1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    invoke-direct/range {v1 .. v7}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/util/Date;Lcom/dropbox/client2/DropboxAPI$Entry;)V

    return-object v1
.end method
