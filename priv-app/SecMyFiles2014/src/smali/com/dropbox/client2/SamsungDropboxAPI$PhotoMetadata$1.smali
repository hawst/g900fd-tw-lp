.class final Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata$1;
.super Lcom/dropbox/client2/jsonextract/JsonExtractor;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/client2/jsonextract/JsonExtractor",
        "<",
        "Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 580
    invoke-direct {p0}, Lcom/dropbox/client2/jsonextract/JsonExtractor;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
    .locals 16
    .param p1, "jt"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 583
    invoke-virtual/range {p1 .. p1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v11

    .line 585
    .local v11, "map":Lcom/dropbox/client2/jsonextract/JsonMap;
    const-string v0, "date_taken"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v7

    .line 586
    .local v7, "dateTakenJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v7, :cond_0

    const/4 v1, 0x0

    .line 588
    .local v1, "dateTaken":Ljava/lang/String;
    :goto_0
    const-string v0, "width"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v13

    .line 589
    .local v13, "widthJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v13, :cond_1

    const/4 v2, 0x0

    .line 591
    .local v2, "width":Ljava/lang/Integer;
    :goto_1
    const-string v0, "height"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v8

    .line 592
    .local v8, "heightJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v8, :cond_2

    const/4 v3, 0x0

    .line 594
    .local v3, "height":Ljava/lang/Integer;
    :goto_2
    const-string v0, "orientation"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v12

    .line 595
    .local v12, "orientationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v12, :cond_3

    const/4 v4, 0x0

    .line 597
    .local v4, "orientation":Ljava/lang/Integer;
    :goto_3
    const-string v0, "latitude"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v9

    .line 598
    .local v9, "latitudeJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v9, :cond_4

    const/4 v5, 0x0

    .line 600
    .local v5, "latitude":Ljava/lang/Double;
    :goto_4
    const-string v0, "longitude"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v10

    .line 601
    .local v10, "longitudeJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v10, :cond_5

    const/4 v6, 0x0

    .line 603
    .local v6, "longitude":Ljava/lang/Double;
    :goto_5
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;)V

    return-object v0

    .line 586
    .end local v1    # "dateTaken":Ljava/lang/String;
    .end local v2    # "width":Ljava/lang/Integer;
    .end local v3    # "height":Ljava/lang/Integer;
    .end local v4    # "orientation":Ljava/lang/Integer;
    .end local v5    # "latitude":Ljava/lang/Double;
    .end local v6    # "longitude":Ljava/lang/Double;
    .end local v8    # "heightJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v9    # "latitudeJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v10    # "longitudeJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v12    # "orientationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v13    # "widthJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_0
    invoke-virtual {v7}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectStringOrNull()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 589
    .restart local v1    # "dateTaken":Ljava/lang/String;
    .restart local v13    # "widthJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_1
    invoke-virtual {v13}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1

    .line 592
    .restart local v2    # "width":Ljava/lang/Integer;
    .restart local v8    # "heightJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_2
    invoke-virtual {v8}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_2

    .line 595
    .restart local v3    # "height":Ljava/lang/Integer;
    .restart local v12    # "orientationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_3
    invoke-virtual {v12}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_3

    .line 598
    .restart local v4    # "orientation":Ljava/lang/Integer;
    .restart local v9    # "latitudeJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_4
    invoke-virtual {v9}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectFloat64()D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    goto :goto_4

    .line 601
    .restart local v5    # "latitude":Ljava/lang/Double;
    .restart local v10    # "longitudeJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_5
    invoke-virtual {v10}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectFloat64()D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    goto :goto_5
.end method

.method public bridge synthetic extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 580
    invoke-virtual {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata$1;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v0

    return-object v0
.end method
