.class public final Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlbumId"
.end annotation


# static fields
.field public static JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/client2/jsonextract/JsonExtractor",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final id:Ljava/lang/String;

.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 624
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId$1;

    invoke-direct {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId$1;-><init>()V

    sput-object v0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;->id:Ljava/lang/String;

    .line 621
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;->name:Ljava/lang/String;

    .line 622
    return-void
.end method
