.class public Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;
.super Ljava/io/FilterInputStream;
.source "DropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/DropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DropboxInputStream"
.end annotation


# instance fields
.field private final info:Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

.field private final request:Lorg/apache/http/client/methods/HttpUriRequest;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    .locals 4
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 544
    invoke-direct {p0, v3}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 546
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 547
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 548
    new-instance v2, Lcom/dropbox/client2/exception/DropboxException;

    const-string v3, "Didn\'t get entity from HttpResponse"

    invoke-direct {v2, v3}, Lcom/dropbox/client2/exception/DropboxException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 554
    :cond_0
    :try_start_0
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->in:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    iput-object p1, p0, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->request:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 560
    new-instance v2, Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    invoke-direct {v2, p2, v3}, Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;-><init>(Lorg/apache/http/HttpResponse;Lcom/dropbox/client2/DropboxAPI$1;)V

    iput-object v2, p0, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->info:Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    .line 561
    return-void

    .line 555
    :catch_0
    move-exception v0

    .line 556
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/dropbox/client2/exception/DropboxIOException;

    invoke-direct {v2, v0}, Lcom/dropbox/client2/exception/DropboxIOException;-><init>(Ljava/io/IOException;)V

    throw v2
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 574
    iget-object v0, p0, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->request:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 575
    return-void
.end method

.method public copyStreamToOutput(Ljava/io/OutputStream;Lcom/dropbox/client2/ProgressListener;)V
    .locals 20
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "listener"    # Lcom/dropbox/client2/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxIOException;,
            Lcom/dropbox/client2/exception/DropboxPartialFileException;,
            Lcom/dropbox/client2/exception/DropboxLocalStorageFullException;
        }
    .end annotation

    .prologue
    .line 610
    const/4 v2, 0x0

    .line 611
    .local v2, "bos":Ljava/io/BufferedOutputStream;
    const-wide/16 v14, 0x0

    .line 612
    .local v14, "totalRead":J
    const-wide/16 v6, 0x0

    .line 613
    .local v6, "lastListened":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->info:Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;->getFileSize()J

    move-result-wide v8

    .line 616
    .local v8, "length":J
    :try_start_0
    new-instance v3, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .local v3, "bos":Ljava/io/BufferedOutputStream;
    const/16 v16, 0x1000

    :try_start_1
    move/from16 v0, v16

    new-array v4, v0, [B

    .line 621
    .local v4, "buffer":[B
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->read([B)I

    move-result v11

    .line 622
    .local v11, "read":I
    if-gez v11, :cond_3

    .line 623
    const-wide/16 v16, 0x0

    cmp-long v16, v8, v16

    if-ltz v16, :cond_4

    cmp-long v16, v14, v8

    if-gez v16, :cond_4

    .line 625
    new-instance v16, Lcom/dropbox/client2/exception/DropboxPartialFileException;

    move-object/from16 v0, v16

    invoke-direct {v0, v14, v15}, Lcom/dropbox/client2/exception/DropboxPartialFileException;-><init>(J)V

    throw v16
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 654
    .end local v4    # "buffer":[B
    .end local v11    # "read":I
    :catch_0
    move-exception v5

    move-object v2, v3

    .line 655
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    .local v5, "e":Ljava/io/IOException;
    :goto_1
    :try_start_2
    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    .line 656
    .local v10, "message":Ljava/lang/String;
    if-eqz v10, :cond_8

    const-string v16, "No space"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 659
    new-instance v16, Lcom/dropbox/client2/exception/DropboxLocalStorageFullException;

    invoke-direct/range {v16 .. v16}, Lcom/dropbox/client2/exception/DropboxLocalStorageFullException;-><init>()V

    throw v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 670
    .end local v5    # "e":Ljava/io/IOException;
    .end local v10    # "message":Ljava/lang/String;
    :catchall_0
    move-exception v16

    :goto_2
    if-eqz v2, :cond_1

    .line 672
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 675
    :cond_1
    :goto_3
    if-eqz p1, :cond_2

    .line 677
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 683
    :cond_2
    :goto_4
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 684
    :goto_5
    throw v16

    .line 631
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v11    # "read":I
    :cond_3
    const/16 v16, 0x0

    :try_start_6
    move/from16 v0, v16

    invoke-virtual {v3, v4, v0, v11}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 633
    int-to-long v0, v11

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    .line 635
    if-eqz p2, :cond_0

    .line 636
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 637
    .local v12, "now":J
    sub-long v16, v12, v6

    invoke-virtual/range {p2 .. p2}, Lcom/dropbox/client2/ProgressListener;->progressInterval()J

    move-result-wide v18

    cmp-long v16, v16, v18

    if-lez v16, :cond_0

    .line 638
    move-wide v6, v12

    .line 639
    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15, v8, v9}, Lcom/dropbox/client2/ProgressListener;->onProgress(JJ)V

    goto :goto_0

    .line 670
    .end local v4    # "buffer":[B
    .end local v11    # "read":I
    .end local v12    # "now":J
    :catchall_1
    move-exception v16

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_2

    .line 644
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v11    # "read":I
    :cond_4
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V

    .line 645
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->flush()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 648
    :try_start_7
    move-object/from16 v0, p1

    instance-of v0, v0, Ljava/io/FileOutputStream;

    move/from16 v16, v0

    if-eqz v16, :cond_5

    .line 649
    move-object/from16 v0, p1

    check-cast v0, Ljava/io/FileOutputStream;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileDescriptor;->sync()V
    :try_end_7
    .catch Ljava/io/SyncFailedException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 670
    :cond_5
    :goto_6
    if-eqz v3, :cond_6

    .line 672
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    .line 675
    :cond_6
    :goto_7
    if-eqz p1, :cond_7

    .line 677
    :try_start_9
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 683
    :cond_7
    :goto_8
    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 686
    :goto_9
    return-void

    .line 667
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v4    # "buffer":[B
    .end local v11    # "read":I
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "e":Ljava/io/IOException;
    .restart local v10    # "message":Ljava/lang/String;
    :cond_8
    :try_start_b
    new-instance v16, Lcom/dropbox/client2/exception/DropboxPartialFileException;

    move-object/from16 v0, v16

    invoke-direct {v0, v14, v15}, Lcom/dropbox/client2/exception/DropboxPartialFileException;-><init>(J)V

    throw v16
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 673
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v5    # "e":Ljava/io/IOException;
    .end local v10    # "message":Ljava/lang/String;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v11    # "read":I
    :catch_1
    move-exception v16

    goto :goto_7

    .line 678
    :catch_2
    move-exception v16

    goto :goto_8

    .line 684
    :catch_3
    move-exception v16

    goto :goto_9

    .line 673
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v4    # "buffer":[B
    .end local v11    # "read":I
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    :catch_4
    move-exception v17

    goto :goto_3

    .line 678
    :catch_5
    move-exception v17

    goto :goto_4

    .line 684
    :catch_6
    move-exception v17

    goto :goto_5

    .line 654
    :catch_7
    move-exception v5

    goto/16 :goto_1

    .line 651
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v11    # "read":I
    :catch_8
    move-exception v16

    goto :goto_6
.end method

.method public getFileInfo()Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->info:Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    return-object v0
.end method
