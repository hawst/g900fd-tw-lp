.class final Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId$1;
.super Lcom/dropbox/client2/jsonextract/JsonExtractor;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/client2/jsonextract/JsonExtractor",
        "<",
        "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 624
    invoke-direct {p0}, Lcom/dropbox/client2/jsonextract/JsonExtractor;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;
    .locals 4
    .param p1, "jt"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 627
    invoke-virtual {p1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v1

    .line 628
    .local v1, "m":Lcom/dropbox/client2/jsonextract/JsonMap;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v0

    .line 629
    .local v0, "id":Ljava/lang/String;
    const-string v3, "name"

    invoke-virtual {v1, v3}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v2

    .line 630
    .local v2, "name":Ljava/lang/String;
    new-instance v3, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;

    invoke-direct {v3, v0, v2}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method

.method public bridge synthetic extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 624
    invoke-virtual {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId$1;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;

    move-result-object v0

    return-object v0
.end method
