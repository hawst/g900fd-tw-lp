.class final Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry$1;
.super Lcom/dropbox/client2/jsonextract/JsonExtractor;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/client2/jsonextract/JsonExtractor",
        "<",
        "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 513
    invoke-direct {p0}, Lcom/dropbox/client2/jsonextract/JsonExtractor;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;
    .locals 10
    .param p1, "jt"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 516
    invoke-virtual {p1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v3

    .line 517
    .local v3, "map":Lcom/dropbox/client2/jsonextract/JsonMap;
    sget-object v7, Lcom/dropbox/client2/DropboxAPI$Entry;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    const-string v9, "metadata"

    invoke-virtual {v3, v9}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/dropbox/client2/jsonextract/JsonExtractor;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/dropbox/client2/DropboxAPI$Entry;

    .line 519
    .local v4, "metadata":Lcom/dropbox/client2/DropboxAPI$Entry;
    const-string v7, "albums"

    invoke-virtual {v3, v7}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectList()Lcom/dropbox/client2/jsonextract/JsonList;

    move-result-object v7

    sget-object v9, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    invoke-virtual {v7, v9}, Lcom/dropbox/client2/jsonextract/JsonList;->extract(Lcom/dropbox/client2/jsonextract/JsonExtractor;)Ljava/util/ArrayList;

    move-result-object v0

    .line 521
    .local v0, "albums":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;>;"
    const-string v7, "photo_metadata"

    invoke-virtual {v3, v7}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v1

    .line 522
    .local v1, "jpm":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v1, :cond_0

    move-object v5, v8

    .line 524
    .local v5, "pm":Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
    :goto_0
    const-string v7, "video_metadata"

    invoke-virtual {v3, v7}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v2

    .line 525
    .local v2, "jvm":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v2, :cond_1

    move-object v6, v8

    .line 527
    .local v6, "vm":Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;
    :goto_1
    new-instance v7, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;

    invoke-direct {v7, v4, v0, v5, v6}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;-><init>(Lcom/dropbox/client2/DropboxAPI$Entry;Ljava/util/List;Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;)V

    return-object v7

    .line 522
    .end local v2    # "jvm":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v5    # "pm":Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
    .end local v6    # "vm":Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;
    :cond_0
    sget-object v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    invoke-virtual {v7, v1}, Lcom/dropbox/client2/jsonextract/JsonExtractor;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-object v5, v7

    goto :goto_0

    .line 525
    .restart local v2    # "jvm":Lcom/dropbox/client2/jsonextract/JsonThing;
    .restart local v5    # "pm":Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
    :cond_1
    sget-object v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    invoke-virtual {v7, v2}, Lcom/dropbox/client2/jsonextract/JsonExtractor;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    move-object v6, v7

    goto :goto_1
.end method

.method public bridge synthetic extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 513
    invoke-virtual {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry$1;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;

    move-result-object v0

    return-object v0
.end method
