.class final Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry$1;
.super Lcom/dropbox/client2/jsonextract/JsonExtractor;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/client2/jsonextract/JsonExtractor",
        "<",
        "Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 706
    invoke-direct {p0}, Lcom/dropbox/client2/jsonextract/JsonExtractor;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;
    .locals 6
    .param p1, "jt"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 709
    invoke-virtual {p1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v1

    .line 711
    .local v1, "map":Lcom/dropbox/client2/jsonextract/JsonMap;
    sget-object v4, Lcom/dropbox/client2/DropboxAPI$Entry;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    const-string v5, "metadata"

    invoke-virtual {v1, v5}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/dropbox/client2/jsonextract/JsonExtractor;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dropbox/client2/DropboxAPI$Entry;

    .line 713
    .local v2, "metadata":Lcom/dropbox/client2/DropboxAPI$Entry;
    const-string v4, "audio_metadata"

    invoke-virtual {v1, v4}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v0

    .line 714
    .local v0, "jmm":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v0, :cond_0

    const/4 v3, 0x0

    .line 716
    .local v3, "mm":Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;
    :goto_0
    new-instance v4, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;

    invoke-direct {v4, v2, v3}, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;-><init>(Lcom/dropbox/client2/DropboxAPI$Entry;Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;)V

    return-object v4

    .line 714
    .end local v3    # "mm":Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;
    :cond_0
    sget-object v4, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    invoke-virtual {v4, v0}, Lcom/dropbox/client2/jsonextract/JsonExtractor;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    move-object v3, v4

    goto :goto_0
.end method

.method public bridge synthetic extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 706
    invoke-virtual {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry$1;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;

    move-result-object v0

    return-object v0
.end method
