.class public Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DropboxTranscodeLink"
.end annotation


# instance fields
.field public final canSeek:Z

.field public final container:Ljava/lang/String;

.field public final expires:Ljava/util/Date;

.field public final url:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 867
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 868
    const-string v2, "url"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;->url:Ljava/lang/String;

    .line 869
    const-string v2, "container"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;->container:Ljava/lang/String;

    .line 870
    const-string v2, "expires"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 871
    .local v1, "exp":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 872
    invoke-static {v1}, Lcom/dropbox/client2/RESTUtility;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;->expires:Ljava/util/Date;

    .line 877
    :goto_0
    const-string v2, "can_seek"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 878
    .local v0, "canSeekValue":Ljava/lang/Boolean;
    if-eqz v0, :cond_1

    .line 879
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;->canSeek:Z

    .line 883
    :goto_1
    return-void

    .line 874
    .end local v0    # "canSeekValue":Ljava/lang/Boolean;
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;->expires:Ljava/util/Date;

    goto :goto_0

    .line 881
    .restart local v0    # "canSeekValue":Ljava/lang/Boolean;
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;->canSeek:Z

    goto :goto_1
.end method

.method synthetic constructor <init>(Ljava/util/Map;Lcom/dropbox/client2/SamsungDropboxAPI$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/Map;
    .param p2, "x1"    # Lcom/dropbox/client2/SamsungDropboxAPI$1;

    .prologue
    .line 853
    invoke-direct {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;-><init>(Ljava/util/Map;)V

    return-void
.end method
