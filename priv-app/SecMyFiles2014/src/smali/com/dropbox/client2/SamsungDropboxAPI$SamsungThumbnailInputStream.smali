.class public Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
.super Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SamsungThumbnailInputStream"
.end annotation


# instance fields
.field private final photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    .locals 1
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 1231
    invoke-direct {p0, p1, p2}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    .line 1234
    invoke-static {p2}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->parseXDropboxPhotoMetadata(Lorg/apache/http/HttpResponse;)Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    .line 1235
    return-void
.end method

.method private static parseXDropboxPhotoMetadata(Lorg/apache/http/HttpResponse;)Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
    .locals 7
    .param p0, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    const/4 v5, 0x0

    .line 1248
    if-nez p0, :cond_0

    move-object v4, v5

    .line 1269
    :goto_0
    return-object v4

    .line 1252
    :cond_0
    const-string v4, "x-dropbox-photo-metadata"

    invoke-interface {p0, v4}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v3

    .line 1254
    .local v3, "xDropboxPhotoMetadataHeader":Lorg/apache/http/Header;
    if-nez v3, :cond_1

    move-object v4, v5

    .line 1255
    goto :goto_0

    .line 1259
    :cond_1
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1260
    .local v1, "json":Ljava/lang/String;
    invoke-static {v1}, Lorg/json/simple/JSONValue;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 1261
    .local v2, "metadata":Ljava/lang/Object;
    if-nez v2, :cond_2

    move-object v4, v5

    .line 1262
    goto :goto_0

    .line 1266
    :cond_2
    :try_start_0
    sget-object v4, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    new-instance v6, Lcom/dropbox/client2/jsonextract/JsonThing;

    invoke-direct {v6, v2}, Lcom/dropbox/client2/jsonextract/JsonThing;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v4, v6}, Lcom/dropbox/client2/jsonextract/JsonExtractor;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1268
    :catch_0
    move-exception v0

    .local v0, "e":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    move-object v4, v5

    .line 1269
    goto :goto_0
.end method


# virtual methods
.method public getPhotoMetadata()Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
    .locals 1

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    return-object v0
.end method
