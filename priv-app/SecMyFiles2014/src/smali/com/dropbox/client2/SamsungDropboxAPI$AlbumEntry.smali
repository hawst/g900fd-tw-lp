.class public final Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlbumEntry"
.end annotation


# static fields
.field public static JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/client2/jsonextract/JsonExtractor",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final albums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;",
            ">;"
        }
    .end annotation
.end field

.field public final fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

.field public final photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

.field public final videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 513
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry$1;

    invoke-direct {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry$1;-><init>()V

    sput-object v0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/client2/DropboxAPI$Entry;Ljava/util/List;Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;)V
    .locals 0
    .param p1, "fileMetadata"    # Lcom/dropbox/client2/DropboxAPI$Entry;
    .param p3, "photoMetadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
    .param p4, "videoMetadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/client2/DropboxAPI$Entry;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;",
            ">;",
            "Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;",
            "Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;",
            ")V"
        }
    .end annotation

    .prologue
    .line 506
    .local p2, "albums":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 507
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    .line 508
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->albums:Ljava/util/List;

    .line 509
    iput-object p3, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->photoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;

    .line 510
    iput-object p4, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->videoMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    .line 511
    return-void
.end method
