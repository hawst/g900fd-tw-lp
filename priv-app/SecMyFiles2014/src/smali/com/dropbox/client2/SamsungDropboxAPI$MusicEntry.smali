.class public final Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MusicEntry"
.end annotation


# static fields
.field public static JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/client2/jsonextract/JsonExtractor",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

.field public final musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 706
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry$1;

    invoke-direct {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry$1;-><init>()V

    sput-object v0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/client2/DropboxAPI$Entry;Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;)V
    .locals 0
    .param p1, "fileMetadata"    # Lcom/dropbox/client2/DropboxAPI$Entry;
    .param p2, "musicMetadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    .prologue
    .line 701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 702
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->fileMetadata:Lcom/dropbox/client2/DropboxAPI$Entry;

    .line 703
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->musicMetadata:Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    .line 704
    return-void
.end method
