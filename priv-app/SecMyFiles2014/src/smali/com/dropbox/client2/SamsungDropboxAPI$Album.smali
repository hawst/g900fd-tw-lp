.class public final Lcom/dropbox/client2/SamsungDropboxAPI$Album;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Album"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x132df74L


# instance fields
.field public final contents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/DropboxAPI$Entry;",
            ">;"
        }
    .end annotation
.end field

.field public final metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

.field public final rev:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "metadata"    # Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    .param p2, "rev"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/DropboxAPI$Entry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 297
    .local p3, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    .line 299
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->rev:Ljava/lang/String;

    .line 300
    iput-object p3, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->contents:Ljava/util/List;

    .line 301
    return-void
.end method
