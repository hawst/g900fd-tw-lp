.class public Lcom/dropbox/client2/SamsungDropboxAPI;
.super Lcom/dropbox/client2/DropboxAPI;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dropbox/client2/SamsungDropboxAPI$1;,
        Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;,
        Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;,
        Lcom/dropbox/client2/SamsungDropboxAPI$Hook;,
        Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;,
        Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;,
        Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;,
        Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;,
        Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;,
        Lcom/dropbox/client2/SamsungDropboxAPI$AlbumId;,
        Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;,
        Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;,
        Lcom/dropbox/client2/SamsungDropboxAPI$Album;,
        Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SESS_T::",
        "Lcom/dropbox/client2/session/Session;",
        ">",
        "Lcom/dropbox/client2/DropboxAPI",
        "<TSESS_T;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/dropbox/client2/session/Session;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSESS_T;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    .local p1, "session":Lcom/dropbox/client2/session/Session;, "TSESS_T;"
    invoke-direct {p0, p1}, Lcom/dropbox/client2/DropboxAPI;-><init>(Lcom/dropbox/client2/session/Session;)V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/DropboxAPI$Entry;
    .locals 1
    .param p0, "x0"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-static {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->extractEntryFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/DropboxAPI$Entry;

    move-result-object v0

    return-object v0
.end method

.method private static extractEntryFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/DropboxAPI$Entry;
    .locals 3
    .param p0, "json"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 797
    :try_start_0
    new-instance v2, Lcom/dropbox/client2/DropboxAPI$Entry;

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/client2/jsonextract/JsonMap;->internal:Ljava/lang/Object;

    check-cast v1, Ljava/util/Map;

    invoke-direct {v2, v1}, Lcom/dropbox/client2/DropboxAPI$Entry;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v2

    .line 799
    :catch_0
    move-exception v0

    .line 801
    .local v0, "ex":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error parsing file metadata object ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/dropbox/client2/jsonextract/JsonThing;->error(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonExtractionException;

    move-result-object v1

    throw v1

    .line 803
    .end local v0    # "ex":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v0

    .line 805
    .local v0, "ex":Ljava/lang/NumberFormatException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error parsing file metadata object ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/dropbox/client2/jsonextract/JsonThing;->error(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonExtractionException;

    move-result-object v1

    throw v1
.end method

.method public static jsonExpect(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)V
    .locals 5
    .param p0, "parser"    # Lcom/dropbox/client2/jsonstream/JsonPullParser;
    .param p1, "type"    # Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v0

    .line 151
    .local v0, "ev":Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    if-eq p1, v0, :cond_0

    .line 152
    new-instance v1, Lcom/dropbox/client2/jsonstream/JsonException;

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->getPosition()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "got bad JSON: expecting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v1

    .line 155
    :cond_0
    return-void
.end method

.method private static parseAlbum(Lcom/dropbox/client2/jsonstream/JsonPullParser;)Lcom/dropbox/client2/SamsungDropboxAPI$Album;
    .locals 11
    .param p0, "parser"    # Lcom/dropbox/client2/jsonstream/JsonPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 158
    const/4 v4, 0x0

    .line 159
    .local v4, "metadata":Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    const/4 v7, 0x0

    .line 160
    .local v7, "rev":Ljava/lang/String;
    const/4 v0, 0x0

    .line 162
    .local v0, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    sget-object v8, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    invoke-static {p0, v8}, Lcom/dropbox/client2/SamsungDropboxAPI;->jsonExpect(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)V

    .line 163
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->getPosition()I

    move-result v6

    .line 165
    .local v6, "objectStartPosition":I
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->peek()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v8

    sget-object v9, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->FieldStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-ne v8, v9, :cond_7

    .line 166
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 167
    iget-object v5, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->fieldName:Ljava/lang/String;

    .line 168
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->getPosition()I

    move-result v2

    .line 170
    .local v2, "fieldStart":I
    const-string v8, "summary"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 171
    if-eqz v4, :cond_0

    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->getPosition()I

    move-result v9

    const-string v10, "got bad JSON: duplicate \"summary\" field"

    invoke-direct {v8, v9, v10}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 172
    :cond_0
    invoke-static {p0}, Lcom/dropbox/client2/jsonstream/JsonBuilder;->build(Lcom/dropbox/client2/jsonstream/JsonPullParser;)Ljava/lang/Object;

    move-result-object v3

    .line 174
    .local v3, "json":Ljava/lang/Object;
    :try_start_0
    new-instance v8, Lcom/dropbox/client2/jsonextract/JsonThing;

    invoke-direct {v8, v3}, Lcom/dropbox/client2/jsonextract/JsonThing;-><init>(Ljava/lang/Object;)V

    invoke-static {v8}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->extractFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 175
    :catch_0
    move-exception v1

    .line 176
    .local v1, "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "got bad JSON: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v2, v9}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 179
    .end local v1    # "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    .end local v3    # "json":Ljava/lang/Object;
    :cond_1
    const-string v8, "contents"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 180
    if-eqz v0, :cond_2

    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->getPosition()I

    move-result v9

    const-string v10, "got bad JSON: duplicate \"contents\" field"

    invoke-direct {v8, v9, v10}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 181
    :cond_2
    sget-object v8, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    invoke-static {p0, v8}, Lcom/dropbox/client2/SamsungDropboxAPI;->jsonExpect(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)V

    .line 182
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "contents":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .restart local v0    # "contents":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/DropboxAPI$Entry;>;"
    :goto_1
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->peek()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v8

    sget-object v9, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-eq v8, v9, :cond_3

    .line 184
    invoke-static {p0}, Lcom/dropbox/client2/jsonstream/JsonBuilder;->build(Lcom/dropbox/client2/jsonstream/JsonPullParser;)Ljava/lang/Object;

    move-result-object v3

    .line 186
    .restart local v3    # "json":Ljava/lang/Object;
    :try_start_1
    new-instance v8, Lcom/dropbox/client2/jsonextract/JsonThing;

    invoke-direct {v8, v3}, Lcom/dropbox/client2/jsonextract/JsonThing;-><init>(Ljava/lang/Object;)V

    invoke-static {v8}, Lcom/dropbox/client2/SamsungDropboxAPI;->extractEntryFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/DropboxAPI$Entry;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 187
    :catch_1
    move-exception v1

    .line 188
    .restart local v1    # "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "got bad JSON: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v2, v9}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 191
    .end local v1    # "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    .end local v3    # "json":Ljava/lang/Object;
    :cond_3
    sget-object v8, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    invoke-static {p0, v8}, Lcom/dropbox/client2/SamsungDropboxAPI;->jsonExpect(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)V

    goto/16 :goto_0

    .line 193
    :cond_4
    const-string v8, "rev"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 194
    if-eqz v4, :cond_5

    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->getPosition()I

    move-result v9

    const-string v10, "got bad JSON: duplicate \"rev\" field"

    invoke-direct {v8, v9, v10}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 195
    :cond_5
    invoke-static {p0}, Lcom/dropbox/client2/jsonstream/JsonBuilder;->build(Lcom/dropbox/client2/jsonstream/JsonPullParser;)Ljava/lang/Object;

    move-result-object v3

    .line 197
    .restart local v3    # "json":Ljava/lang/Object;
    :try_start_2
    new-instance v8, Lcom/dropbox/client2/jsonextract/JsonThing;

    invoke-direct {v8, v3}, Lcom/dropbox/client2/jsonextract/JsonThing;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v8}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;
    :try_end_2
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v7

    goto/16 :goto_0

    .line 198
    :catch_2
    move-exception v1

    .line 199
    .restart local v1    # "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "got bad JSON: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v2, v9}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 204
    .end local v1    # "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    .end local v3    # "json":Ljava/lang/Object;
    :cond_6
    invoke-static {p0}, Lcom/dropbox/client2/jsonstream/JsonBuilder;->skip(Lcom/dropbox/client2/jsonstream/JsonPullParser;)V

    goto/16 :goto_0

    .line 208
    .end local v2    # "fieldStart":I
    .end local v5    # "name":Ljava/lang/String;
    :cond_7
    sget-object v8, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    invoke-static {p0, v8}, Lcom/dropbox/client2/SamsungDropboxAPI;->jsonExpect(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)V

    .line 210
    if-nez v4, :cond_8

    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    const-string v9, "got bad JSON: missing field \"summary\""

    invoke-direct {v8, v6, v9}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 211
    :cond_8
    if-nez v0, :cond_9

    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    const-string v9, "got bad JSON: missing field \"contents\""

    invoke-direct {v8, v6, v9}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 212
    :cond_9
    if-nez v7, :cond_a

    new-instance v8, Lcom/dropbox/client2/jsonstream/JsonException;

    const-string v9, "got bad JSON: missing field \"rev\""

    invoke-direct {v8, v6, v9}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    throw v8

    .line 214
    :cond_a
    new-instance v8, Lcom/dropbox/client2/SamsungDropboxAPI$Album;

    invoke-direct {v8, v4, v7, v0}, Lcom/dropbox/client2/SamsungDropboxAPI$Album;-><init>(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;Ljava/lang/String;Ljava/util/List;)V

    return-object v8
.end method

.method private requestAsJson(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p1, "method"    # Lcom/dropbox/client2/RESTUtility$RequestMethod;
    .param p2, "server"    # Ljava/lang/String;
    .param p3, "urlPath"    # Ljava/lang/String;
    .param p4, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 850
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private streamRequest(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/io/InputStream;
    .locals 8
    .param p1, "method"    # Lcom/dropbox/client2/RESTUtility$RequestMethod;
    .param p2, "server"    # Ljava/lang/String;
    .param p3, "urlPath"    # Ljava/lang/String;
    .param p4, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 815
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->streamRequest(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;

    move-result-object v0

    iget-object v7, v0, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->response:Lorg/apache/http/HttpResponse;

    .line 817
    .local v7, "resp":Lorg/apache/http/HttpResponse;
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 819
    invoke-static {v7}, Lcom/dropbox/client2/RESTUtility;->parseAsJSON(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    .line 820
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "shouldn\'t get here"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 824
    :cond_0
    :try_start_0
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 826
    :catch_0
    move-exception v6

    .line 827
    .local v6, "ex":Ljava/io/IOException;
    new-instance v0, Lcom/dropbox/client2/exception/DropboxIOException;

    invoke-direct {v0, v6}, Lcom/dropbox/client2/exception/DropboxIOException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method private streamRequestAsReader(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/io/Reader;
    .locals 4
    .param p1, "method"    # Lcom/dropbox/client2/RESTUtility$RequestMethod;
    .param p2, "server"    # Ljava/lang/String;
    .param p3, "urlPath"    # Ljava/lang/String;
    .param p4, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 837
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    :try_start_0
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/client2/SamsungDropboxAPI;->streamRequest(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 839
    :catch_0
    move-exception v0

    .line 840
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported encoding: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method


# virtual methods
.method public addTrigger(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "hook"    # Lcom/dropbox/client2/SamsungDropboxAPI$Hook;
    .param p2, "deltaCursor"    # Ljava/lang/String;
    .param p3, "registrationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    .line 1021
    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "delta_cursor"

    aput-object v1, v4, v0

    aput-object p2, v4, v3

    const/4 v0, 0x2

    const-string v1, "hook_id"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->hookId:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "device_id"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p3, v4, v0

    const/4 v0, 0x6

    const-string v1, "type"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    const-string v1, "delta"

    aput-object v1, v4, v0

    .line 1028
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->POST:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/hooks/add_trigger"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 1032
    .local v6, "json":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "ping"

    const-string v1, "ret"

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1033
    const/4 v0, 0x0

    .line 1035
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "trigger_id"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public album(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/SamsungDropboxAPI$Album;
    .locals 7
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "lastRev"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v6, 0x0

    .line 125
    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->assertAuthenticated()V

    .line 128
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/samsung_album/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 129
    .local v3, "urlPath":Ljava/lang/String;
    new-array v2, v6, [Ljava/lang/String;

    .line 130
    .local v2, "params":[Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 131
    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/String;

    .end local v2    # "params":[Ljava/lang/String;
    const-string v4, "last_rev"

    aput-object v4, v2, v6

    const/4 v4, 0x1

    aput-object p2, v2, v4

    .line 135
    .restart local v2    # "params":[Ljava/lang/String;
    :cond_0
    :try_start_0
    sget-object v4, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v5}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5, v3, v2}, Lcom/dropbox/client2/SamsungDropboxAPI;->streamRequestAsReader(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/io/Reader;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/client2/jsonstream/JsonException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 137
    .local v1, "in":Ljava/io/Reader;
    :try_start_1
    new-instance v4, Lcom/dropbox/client2/jsonstream/JsonPullParser;

    invoke-direct {v4, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;-><init>(Ljava/io/Reader;)V

    invoke-static {v4}, Lcom/dropbox/client2/SamsungDropboxAPI;->parseAlbum(Lcom/dropbox/client2/jsonstream/JsonPullParser;)Lcom/dropbox/client2/SamsungDropboxAPI$Album;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 139
    :try_start_2
    invoke-virtual {v1}, Ljava/io/Reader;->close()V

    return-object v4

    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Ljava/io/Reader;->close()V

    throw v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/dropbox/client2/jsonstream/JsonException; {:try_start_2 .. :try_end_2} :catch_1

    .line 141
    .end local v1    # "in":Ljava/io/Reader;
    :catch_0
    move-exception v0

    .line 142
    .local v0, "ex":Ljava/io/IOException;
    new-instance v4, Lcom/dropbox/client2/exception/DropboxIOException;

    invoke-direct {v4, v0}, Lcom/dropbox/client2/exception/DropboxIOException;-><init>(Ljava/io/IOException;)V

    throw v4

    .line 143
    .end local v0    # "ex":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 144
    .local v0, "ex":Lcom/dropbox/client2/jsonstream/JsonException;
    new-instance v4, Lcom/dropbox/client2/exception/DropboxParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "got bad JSON: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonstream/JsonException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public albums()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->assertAuthenticated()V

    .line 75
    const-string v10, "/samsung_albums"

    .line 76
    .local v10, "urlPath":Ljava/lang/String;
    const/4 v6, 0x0

    .line 77
    .local v6, "params":[Ljava/lang/String;
    sget-object v11, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v12, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v12}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12, v10, v6}, Lcom/dropbox/client2/SamsungDropboxAPI;->requestAsJson(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 82
    .local v9, "responseJson":Ljava/lang/Object;
    :try_start_0
    new-instance v11, Lcom/dropbox/client2/jsonextract/JsonThing;

    invoke-direct {v11, v9}, Lcom/dropbox/client2/jsonextract/JsonThing;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v11}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v5

    .line 83
    .local v5, "jResponse":Lcom/dropbox/client2/jsonextract/JsonMap;
    const-string v11, "albums"

    invoke-virtual {v5, v11}, Lcom/dropbox/client2/jsonextract/JsonMap;->get(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v11

    invoke-virtual {v11}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectList()Lcom/dropbox/client2/jsonextract/JsonList;

    move-result-object v4

    .line 84
    .local v4, "jList":Lcom/dropbox/client2/jsonextract/JsonList;
    invoke-virtual {v4}, Lcom/dropbox/client2/jsonextract/JsonList;->length()I

    move-result v11

    new-array v0, v11, [Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    .line 85
    .local v0, "albumMetadatas":[Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    const/4 v7, 0x0

    .line 86
    .local v7, "pos":I
    invoke-virtual {v4}, Lcom/dropbox/client2/jsonextract/JsonList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    move v8, v7

    .end local v7    # "pos":I
    .local v8, "pos":I
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dropbox/client2/jsonextract/JsonThing;

    .line 87
    .local v3, "jElement":Lcom/dropbox/client2/jsonextract/JsonThing;
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    invoke-static {v3}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->extractFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    move-result-object v11

    aput-object v11, v0, v8
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    move v8, v7

    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    goto :goto_0

    .line 89
    .end local v0    # "albumMetadatas":[Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "jElement":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v4    # "jList":Lcom/dropbox/client2/jsonextract/JsonList;
    .end local v5    # "jResponse":Lcom/dropbox/client2/jsonextract/JsonMap;
    .end local v8    # "pos":I
    :catch_0
    move-exception v1

    .line 90
    .local v1, "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    new-instance v11, Lcom/dropbox/client2/exception/DropboxParseException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "server returned unxpected JSON: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 93
    .end local v1    # "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    .restart local v0    # "albumMetadatas":[Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "jList":Lcom/dropbox/client2/jsonextract/JsonList;
    .restart local v5    # "jResponse":Lcom/dropbox/client2/jsonextract/JsonMap;
    .restart local v8    # "pos":I
    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    return-object v11
.end method

.method public getMusicCoverArt(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 420
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    invoke-virtual {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->getMusicCoverArtStream(Ljava/lang/String;)Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;

    move-result-object v0

    .line 422
    .local v0, "cover":Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;
    invoke-virtual {v0, p2}, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;->copyStreamToOutput(Ljava/io/OutputStream;)V

    .line 423
    return-void
.end method

.method public getMusicCoverArtStream(Ljava/lang/String;)Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 427
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->assertAuthenticated()V

    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/music/cover/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAccessType()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 430
    .local v2, "target":Ljava/lang/String;
    const/4 v0, 0x0

    new-array v4, v0, [Ljava/lang/String;

    .line 431
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getContentServer()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->streamRequest(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;

    move-result-object v6

    .line 434
    .local v6, "rr":Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;

    iget-object v1, v6, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->request:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v3, v6, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->response:Lorg/apache/http/HttpResponse;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v0
.end method

.method public getSamsungThumbnailStream(Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbSize;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "size"    # Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .param p3, "format"    # Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    .line 470
    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->assertAuthenticated()V

    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/samsung_thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAccessType()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 473
    .local v2, "target":Ljava/lang/String;
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "size"

    aput-object v1, v4, v0

    invoke-virtual {p2}, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->toAPISize()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "format"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-virtual {p3}, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 477
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getContentServer()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->streamRequest(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;

    move-result-object v6

    .line 480
    .local v6, "rr":Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;

    iget-object v1, v6, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->request:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v3, v6, Lcom/dropbox/client2/DropboxAPI$RequestAndResponse;->response:Lorg/apache/http/HttpResponse;

    invoke-direct {v0, v1, v3}, Lcom/dropbox/client2/SamsungDropboxAPI$SamsungThumbnailInputStream;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v0
.end method

.method public hooks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$Hook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 950
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->hooks(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hooks(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "hookType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$Hook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    .line 968
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 969
    .local v10, "l":Ljava/util/List;, "Ljava/util/List<Lcom/dropbox/client2/SamsungDropboxAPI$Hook;>;"
    const/4 v4, 0x0

    .line 971
    .local v4, "params":[Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 972
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "params":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, "hook_type"

    aput-object v1, v4, v0

    aput-object p1, v4, v3

    .line 975
    .restart local v4    # "params":[Ljava/lang/String;
    :cond_0
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/hooks"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/json/simple/JSONArray;

    .line 978
    .local v9, "json":Lorg/json/simple/JSONArray;
    invoke-virtual {v9}, Lorg/json/simple/JSONArray;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 979
    .local v6, "entry":Ljava/lang/Object;
    instance-of v0, v6, Ljava/util/Map;

    if-eqz v0, :cond_1

    move-object v11, v6

    .line 981
    check-cast v11, Ljava/util/Map;

    .line 982
    .local v11, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "hook_id"

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 983
    .local v7, "hookId":Ljava/lang/String;
    const-string v0, "type"

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 984
    .local v12, "type":Ljava/lang/String;
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;

    const/4 v1, 0x0

    invoke-direct {v0, v7, v12, v1}, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/client2/SamsungDropboxAPI$1;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 987
    .end local v6    # "entry":Ljava/lang/Object;
    .end local v7    # "hookId":Ljava/lang/String;
    .end local v11    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v12    # "type":Ljava/lang/String;
    :cond_2
    return-object v10
.end method

.method public mediaTranscode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "screenResolution"    # Ljava/lang/String;
    .param p3, "connectionType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;,
            Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    .line 917
    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->assertAuthenticated()V

    .line 918
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/media_transcode/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAccessType()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 920
    .local v2, "target":Ljava/lang/String;
    const/16 v0, 0x10

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "locale"

    aput-object v1, v4, v0

    iget-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v0}, Lcom/dropbox/client2/session/Session;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "container"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "hls,mpegts"

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "screen_resolution"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p2, v4, v0

    const/4 v0, 0x6

    const-string v1, "connection_type"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    aput-object p3, v4, v0

    const/16 v0, 0x8

    const-string v1, "model"

    aput-object v1, v4, v0

    const/16 v0, 0x9

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xa

    const-string v1, "sys_version"

    aput-object v1, v4, v0

    const/16 v0, 0xb

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xc

    const-string v1, "platform"

    aput-object v1, v4, v0

    const/16 v0, 0xd

    const-string v1, "android"

    aput-object v1, v4, v0

    const/16 v0, 0xe

    const-string v1, "manufacturer"

    aput-object v1, v4, v0

    const/16 v0, 0xf

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 932
    .local v4, "params":[Ljava/lang/String;
    :try_start_0
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 934
    .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;

    const/4 v1, 0x0

    invoke-direct {v0, v6, v1}, Lcom/dropbox/client2/SamsungDropboxAPI$DropboxTranscodeLink;-><init>(Ljava/util/Map;Lcom/dropbox/client2/SamsungDropboxAPI$1;)V
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxServerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 935
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v7

    .line 936
    .local v7, "se":Lcom/dropbox/client2/exception/DropboxServerException;
    iget v0, v7, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    const/16 v1, 0x19f

    if-ne v0, v1, :cond_0

    .line 938
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;

    invoke-direct {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;-><init>()V

    throw v0

    .line 940
    :cond_0
    throw v7
.end method

.method public musicDelta(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    .locals 8
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/dropbox/client2/DropboxAPI$DeltaPage",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    .line 404
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "cursor"

    aput-object v1, v4, v0

    aput-object p1, v4, v3

    const/4 v0, 0x2

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 409
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->POST:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/music/delta"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v7

    .line 412
    .local v7, "json":Ljava/lang/Object;
    :try_start_0
    new-instance v0, Lcom/dropbox/client2/jsonextract/JsonThing;

    invoke-direct {v0, v7}, Lcom/dropbox/client2/jsonextract/JsonThing;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lcom/dropbox/client2/SamsungDropboxAPI$MusicEntry;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    invoke-static {v0, v1}, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->extractFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;Lcom/dropbox/client2/jsonextract/JsonExtractor;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 413
    :catch_0
    move-exception v6

    .line 414
    .local v6, "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    new-instance v0, Lcom/dropbox/client2/exception/DropboxParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error parsing /music/delta results: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public photosDelta(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    .locals 8
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/dropbox/client2/DropboxAPI$DeltaPage",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    .line 366
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "cursor"

    aput-object v1, v4, v0

    aput-object p1, v4, v3

    const/4 v0, 0x2

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 371
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->POST:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/samsung_photos_delta"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v7

    .line 374
    .local v7, "json":Ljava/lang/Object;
    :try_start_0
    new-instance v0, Lcom/dropbox/client2/jsonextract/JsonThing;

    invoke-direct {v0, v7}, Lcom/dropbox/client2/jsonextract/JsonThing;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumEntry;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    invoke-static {v0, v1}, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->extractFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;Lcom/dropbox/client2/jsonextract/JsonExtractor;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 375
    :catch_0
    move-exception v6

    .line 376
    .local v6, "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    new-instance v0, Lcom/dropbox/client2/exception/DropboxParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error parsing /samsung_photos_delta results: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeTrigger(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;)Z
    .locals 7
    .param p1, "hook"    # Lcom/dropbox/client2/SamsungDropboxAPI$Hook;
    .param p2, "triggerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    .line 1057
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "hook_id"

    aput-object v1, v4, v0

    iget-object v0, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->hookId:Ljava/lang/String;

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "trigger_id"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p2, v4, v0

    .line 1062
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->POST:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/hooks/remove_trigger"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 1066
    .local v6, "json":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "success"

    const-string v1, "ret"

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public videosDelta(Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    .locals 8
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/dropbox/client2/DropboxAPI$DeltaPage",
            "<",
            "Lcom/dropbox/client2/DropboxAPI$Entry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/dropbox/client2/SamsungDropboxAPI;, "Lcom/dropbox/client2/SamsungDropboxAPI<TSESS_T;>;"
    const/4 v3, 0x1

    .line 328
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "cursor"

    aput-object v1, v4, v0

    aput-object p1, v4, v3

    const/4 v0, 0x2

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 333
    .local v4, "params":[Ljava/lang/String;
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->POST:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->getAPIServer()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/samsung_videos_delta"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->session:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->request(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v7

    .line 336
    .local v7, "json":Ljava/lang/Object;
    :try_start_0
    new-instance v0, Lcom/dropbox/client2/jsonextract/JsonThing;

    invoke-direct {v0, v7}, Lcom/dropbox/client2/jsonextract/JsonThing;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lcom/dropbox/client2/DropboxAPI$Entry;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    invoke-static {v0, v1}, Lcom/dropbox/client2/DropboxAPI$DeltaPage;->extractFromJson(Lcom/dropbox/client2/jsonextract/JsonThing;Lcom/dropbox/client2/jsonextract/JsonExtractor;)Lcom/dropbox/client2/DropboxAPI$DeltaPage;
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 337
    :catch_0
    move-exception v6

    .line 338
    .local v6, "ex":Lcom/dropbox/client2/jsonextract/JsonExtractionException;
    new-instance v0, Lcom/dropbox/client2/exception/DropboxParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error parsing /samsung_videos_delta results: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
