.class public final Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MusicMetadata"
.end annotation


# static fields
.field public static final JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/client2/jsonextract/JsonExtractor",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final album:Ljava/lang/String;

.field public final artist:Ljava/lang/String;

.field public final duration:Ljava/lang/Integer;

.field public final genre:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final track:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 764
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata$1;

    invoke-direct {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata$1;-><init>()V

    sput-object v0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "album"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;
    .param p3, "track"    # Ljava/lang/Integer;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "genre"    # Ljava/lang/String;
    .param p6, "duration"    # Ljava/lang/Integer;

    .prologue
    .line 756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 757
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->album:Ljava/lang/String;

    .line 758
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->artist:Ljava/lang/String;

    .line 759
    iput-object p3, p0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->track:Ljava/lang/Integer;

    .line 760
    iput-object p4, p0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->title:Ljava/lang/String;

    .line 761
    iput-object p5, p0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->genre:Ljava/lang/String;

    .line 762
    iput-object p6, p0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;->duration:Ljava/lang/Integer;

    .line 763
    return-void
.end method
