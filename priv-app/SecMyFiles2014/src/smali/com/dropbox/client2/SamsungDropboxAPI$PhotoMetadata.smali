.class public final Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PhotoMetadata"
.end annotation


# static fields
.field public static final JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/client2/jsonextract/JsonExtractor",
            "<",
            "Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final dateTaken:Ljava/lang/String;

.field public final height:Ljava/lang/Integer;

.field public final latitude:Ljava/lang/Double;

.field public final longitude:Ljava/lang/Double;

.field public final orientation:Ljava/lang/Integer;

.field public final width:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 580
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata$1;

    invoke-direct {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata$1;-><init>()V

    sput-object v0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->JsonExtractor:Lcom/dropbox/client2/jsonextract/JsonExtractor;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 0
    .param p1, "dateTaken"    # Ljava/lang/String;
    .param p2, "width"    # Ljava/lang/Integer;
    .param p3, "height"    # Ljava/lang/Integer;
    .param p4, "orientation"    # Ljava/lang/Integer;
    .param p5, "latitude"    # Ljava/lang/Double;
    .param p6, "longitude"    # Ljava/lang/Double;

    .prologue
    .line 571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->dateTaken:Ljava/lang/String;

    .line 573
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->width:Ljava/lang/Integer;

    .line 574
    iput-object p3, p0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->height:Ljava/lang/Integer;

    .line 575
    iput-object p4, p0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->orientation:Ljava/lang/Integer;

    .line 576
    iput-object p5, p0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->latitude:Ljava/lang/Double;

    .line 577
    iput-object p6, p0, Lcom/dropbox/client2/SamsungDropboxAPI$PhotoMetadata;->longitude:Ljava/lang/Double;

    .line 578
    return-void
.end method
