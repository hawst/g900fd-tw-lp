.class final Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata$1;
.super Lcom/dropbox/client2/jsonextract/JsonExtractor;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/client2/jsonextract/JsonExtractor",
        "<",
        "Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 667
    invoke-direct {p0}, Lcom/dropbox/client2/jsonextract/JsonExtractor;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;
    .locals 11
    .param p1, "jt"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 670
    invoke-virtual {p1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v6

    .line 672
    .local v6, "map":Lcom/dropbox/client2/jsonextract/JsonMap;
    const-string v10, "width"

    invoke-virtual {v6, v10}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v8

    .line 673
    .local v8, "widthJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v8, :cond_0

    move-object v7, v9

    .line 675
    .local v7, "width":Ljava/lang/Integer;
    :goto_0
    const-string v10, "height"

    invoke-virtual {v6, v10}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v5

    .line 676
    .local v5, "heightJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v5, :cond_1

    move-object v4, v9

    .line 678
    .local v4, "height":Ljava/lang/Integer;
    :goto_1
    const-string v10, "duration"

    invoke-virtual {v6, v10}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v3

    .line 679
    .local v3, "durationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v3, :cond_2

    move-object v2, v9

    .line 681
    .local v2, "duration":Ljava/lang/Integer;
    :goto_2
    const-string v10, "bitrate"

    invoke-virtual {v6, v10}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v1

    .line 682
    .local v1, "bitrateJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v1, :cond_3

    move-object v0, v9

    .line 684
    .local v0, "bitrate":Ljava/lang/Integer;
    :goto_3
    new-instance v9, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    invoke-direct {v9, v7, v4, v2, v0}, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v9

    .line 673
    .end local v0    # "bitrate":Ljava/lang/Integer;
    .end local v1    # "bitrateJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v2    # "duration":Ljava/lang/Integer;
    .end local v3    # "durationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v4    # "height":Ljava/lang/Integer;
    .end local v5    # "heightJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v7    # "width":Ljava/lang/Integer;
    :cond_0
    invoke-virtual {v8}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_0

    .line 676
    .restart local v5    # "heightJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .restart local v7    # "width":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v5}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_1

    .line 679
    .restart local v3    # "durationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .restart local v4    # "height":Ljava/lang/Integer;
    :cond_2
    invoke-virtual {v3}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_2

    .line 682
    .restart local v1    # "bitrateJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .restart local v2    # "duration":Ljava/lang/Integer;
    :cond_3
    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_3
.end method

.method public bridge synthetic extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 667
    invoke-virtual {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata$1;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$VideoMetadata;

    move-result-object v0

    return-object v0
.end method
