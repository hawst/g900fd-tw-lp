.class public Lcom/dropbox/client2/SamsungDropboxAPI$Hook;
.super Ljava/lang/Object;
.source "SamsungDropboxAPI.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Hook"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x1bca3554df3941f0L


# instance fields
.field public final TYPE_ANDROID:Ljava/lang/String;

.field public final TYPE_WEB:Ljava/lang/String;

.field public final hookId:Ljava/lang/String;

.field public final type:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "hookId"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 1091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086
    const-string v0, "android"

    iput-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->TYPE_ANDROID:Ljava/lang/String;

    .line 1089
    const-string v0, "web"

    iput-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->TYPE_WEB:Ljava/lang/String;

    .line 1092
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->hookId:Ljava/lang/String;

    .line 1093
    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->type:Ljava/lang/String;

    .line 1094
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/client2/SamsungDropboxAPI$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/dropbox/client2/SamsungDropboxAPI$1;

    .prologue
    .line 1072
    invoke-direct {p0, p1, p2}, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
