.class final Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata$1;
.super Lcom/dropbox/client2/jsonextract/JsonExtractor;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/dropbox/client2/jsonextract/JsonExtractor",
        "<",
        "Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 764
    invoke-direct {p0}, Lcom/dropbox/client2/jsonextract/JsonExtractor;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;
    .locals 14
    .param p1, "jt"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 767
    invoke-virtual {p1}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectMap()Lcom/dropbox/client2/jsonextract/JsonMap;

    move-result-object v11

    .line 769
    .local v11, "map":Lcom/dropbox/client2/jsonextract/JsonMap;
    const-string v0, "album"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v7

    .line 770
    .local v7, "albumJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v7, :cond_0

    const/4 v1, 0x0

    .line 772
    .local v1, "album":Ljava/lang/String;
    :goto_0
    const-string v0, "artist"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v8

    .line 773
    .local v8, "artistJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v8, :cond_1

    const/4 v2, 0x0

    .line 775
    .local v2, "artist":Ljava/lang/String;
    :goto_1
    const-string v0, "track"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v13

    .line 776
    .local v13, "trackJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v13, :cond_2

    const/4 v3, 0x0

    .line 778
    .local v3, "track":Ljava/lang/Integer;
    :goto_2
    const-string v0, "title"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v12

    .line 779
    .local v12, "titleJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v12, :cond_3

    const/4 v4, 0x0

    .line 781
    .local v4, "title":Ljava/lang/String;
    :goto_3
    const-string v0, "genre"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v10

    .line 782
    .local v10, "genreJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v10, :cond_4

    const/4 v5, 0x0

    .line 784
    .local v5, "genre":Ljava/lang/String;
    :goto_4
    const-string v0, "duration"

    invoke-virtual {v11, v0}, Lcom/dropbox/client2/jsonextract/JsonMap;->getOrNull(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonThing;

    move-result-object v9

    .line 785
    .local v9, "durationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    if-nez v9, :cond_5

    const/4 v6, 0x0

    .line 787
    .local v6, "duration":Ljava/lang/Integer;
    :goto_5
    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0

    .line 770
    .end local v1    # "album":Ljava/lang/String;
    .end local v2    # "artist":Ljava/lang/String;
    .end local v3    # "track":Ljava/lang/Integer;
    .end local v4    # "title":Ljava/lang/String;
    .end local v5    # "genre":Ljava/lang/String;
    .end local v6    # "duration":Ljava/lang/Integer;
    .end local v8    # "artistJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v9    # "durationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v10    # "genreJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v12    # "titleJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    .end local v13    # "trackJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_0
    invoke-virtual {v7}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 773
    .restart local v1    # "album":Ljava/lang/String;
    .restart local v8    # "artistJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_1
    invoke-virtual {v8}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 776
    .restart local v2    # "artist":Ljava/lang/String;
    .restart local v13    # "trackJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_2
    invoke-virtual {v13}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_2

    .line 779
    .restart local v3    # "track":Ljava/lang/Integer;
    .restart local v12    # "titleJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_3
    invoke-virtual {v12}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 782
    .restart local v4    # "title":Ljava/lang/String;
    .restart local v10    # "genreJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_4
    invoke-virtual {v10}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 785
    .restart local v5    # "genre":Ljava/lang/String;
    .restart local v9    # "durationJson":Lcom/dropbox/client2/jsonextract/JsonThing;
    :cond_5
    invoke-virtual {v9}, Lcom/dropbox/client2/jsonextract/JsonThing;->expectInt32()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_5
.end method

.method public bridge synthetic extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/dropbox/client2/jsonextract/JsonThing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/jsonextract/JsonExtractionException;
        }
    .end annotation

    .prologue
    .line 764
    invoke-virtual {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata$1;->extract(Lcom/dropbox/client2/jsonextract/JsonThing;)Lcom/dropbox/client2/SamsungDropboxAPI$MusicMetadata;

    move-result-object v0

    return-object v0
.end method
