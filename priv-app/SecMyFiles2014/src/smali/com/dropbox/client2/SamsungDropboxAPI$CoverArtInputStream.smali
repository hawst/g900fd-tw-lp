.class public Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;
.super Ljava/io/FilterInputStream;
.source "SamsungDropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/SamsungDropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CoverArtInputStream"
.end annotation


# instance fields
.field private final request:Lorg/apache/http/client/methods/HttpUriRequest;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    .locals 4
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 1108
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1110
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 1111
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 1112
    new-instance v2, Lcom/dropbox/client2/exception/DropboxException;

    const-string v3, "Didn\'t get entity from HttpResponse"

    invoke-direct {v2, v3}, Lcom/dropbox/client2/exception/DropboxException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1118
    :cond_0
    :try_start_0
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1123
    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;->request:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1124
    return-void

    .line 1119
    :catch_0
    move-exception v0

    .line 1120
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/dropbox/client2/exception/DropboxIOException;

    invoke-direct {v2, v0}, Lcom/dropbox/client2/exception/DropboxIOException;-><init>(Ljava/io/IOException;)V

    throw v2
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1136
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;->request:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1138
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 1140
    return-void

    .line 1138
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    throw v0
.end method

.method public copyStreamToOutput(Ljava/io/OutputStream;)V
    .locals 14
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxIOException;,
            Lcom/dropbox/client2/exception/DropboxLocalStorageFullException;
        }
    .end annotation

    .prologue
    .line 1162
    const/4 v2, 0x0

    .line 1163
    .local v2, "bos":Ljava/io/BufferedOutputStream;
    const-wide/16 v10, 0x0

    .line 1164
    .local v10, "totalRead":J
    const-wide/16 v6, 0x0

    .line 1167
    .local v6, "lastListened":J
    :try_start_0
    new-instance v3, Ljava/io/BufferedOutputStream;

    invoke-direct {v3, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1169
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .local v3, "bos":Ljava/io/BufferedOutputStream;
    const/16 v12, 0x1000

    :try_start_1
    new-array v4, v12, [B

    .line 1172
    .local v4, "buffer":[B
    :goto_0
    invoke-virtual {p0, v4}, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;->read([B)I

    move-result v9

    .line 1173
    .local v9, "read":I
    if-gez v9, :cond_3

    .line 1183
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V

    .line 1184
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1187
    :try_start_2
    instance-of v12, p1, Ljava/io/FileOutputStream;

    if-eqz v12, :cond_0

    .line 1188
    move-object v0, p1

    check-cast v0, Ljava/io/FileOutputStream;

    move-object v12, v0

    invoke-virtual {v12}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v12

    invoke-virtual {v12}, Ljava/io/FileDescriptor;->sync()V
    :try_end_2
    .catch Ljava/io/SyncFailedException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1201
    :cond_0
    :goto_1
    if-eqz v3, :cond_1

    .line 1203
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1206
    :cond_1
    :goto_2
    if-eqz p1, :cond_2

    .line 1208
    :try_start_4
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 1214
    :cond_2
    :goto_3
    :try_start_5
    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    move-object v2, v3

    .line 1217
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v4    # "buffer":[B
    .end local v9    # "read":I
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    :goto_4
    return-void

    .line 1178
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v9    # "read":I
    :cond_3
    const/4 v12, 0x0

    :try_start_6
    invoke-virtual {v3, v4, v12, v9}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1180
    int-to-long v12, v9

    add-long/2addr v10, v12

    goto :goto_0

    .line 1215
    :catch_0
    move-exception v12

    move-object v2, v3

    .line 1216
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_4

    .line 1193
    .end local v4    # "buffer":[B
    .end local v9    # "read":I
    :catch_1
    move-exception v5

    .line 1194
    .local v5, "e":Ljava/io/IOException;
    :goto_5
    :try_start_7
    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    .line 1195
    .local v8, "message":Ljava/lang/String;
    if-eqz v8, :cond_6

    const-string v12, "No space"

    invoke-virtual {v8, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1198
    new-instance v12, Lcom/dropbox/client2/exception/DropboxLocalStorageFullException;

    invoke-direct {v12}, Lcom/dropbox/client2/exception/DropboxLocalStorageFullException;-><init>()V

    throw v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1201
    .end local v5    # "e":Ljava/io/IOException;
    .end local v8    # "message":Ljava/lang/String;
    :catchall_0
    move-exception v12

    :goto_6
    if-eqz v2, :cond_4

    .line 1203
    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 1206
    :cond_4
    :goto_7
    if-eqz p1, :cond_5

    .line 1208
    :try_start_9
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    .line 1214
    :cond_5
    :goto_8
    :try_start_a
    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 1215
    :goto_9
    throw v12

    .line 1201
    .restart local v5    # "e":Ljava/io/IOException;
    .restart local v8    # "message":Ljava/lang/String;
    :cond_6
    if-eqz v2, :cond_7

    .line 1203
    :try_start_b
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 1206
    :cond_7
    :goto_a
    if-eqz p1, :cond_8

    .line 1208
    :try_start_c
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    .line 1214
    :cond_8
    :goto_b
    :try_start_d
    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI$CoverArtInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2

    goto :goto_4

    .line 1215
    :catch_2
    move-exception v12

    goto :goto_4

    .line 1204
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v5    # "e":Ljava/io/IOException;
    .end local v8    # "message":Ljava/lang/String;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v9    # "read":I
    :catch_3
    move-exception v12

    goto :goto_2

    .line 1209
    :catch_4
    move-exception v12

    goto :goto_3

    .line 1204
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v4    # "buffer":[B
    .end local v9    # "read":I
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "e":Ljava/io/IOException;
    .restart local v8    # "message":Ljava/lang/String;
    :catch_5
    move-exception v12

    goto :goto_a

    .line 1209
    :catch_6
    move-exception v12

    goto :goto_b

    .line 1204
    .end local v5    # "e":Ljava/io/IOException;
    .end local v8    # "message":Ljava/lang/String;
    :catch_7
    move-exception v13

    goto :goto_7

    .line 1209
    :catch_8
    move-exception v13

    goto :goto_8

    .line 1215
    :catch_9
    move-exception v13

    goto :goto_9

    .line 1201
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v12

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_6

    .line 1193
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    :catch_a
    move-exception v5

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_5

    .line 1190
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v9    # "read":I
    :catch_b
    move-exception v12

    goto :goto_1
.end method
