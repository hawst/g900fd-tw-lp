.class Lcom/dropbox/client2/jsonstream/JsonPullParser$1;
.super Ljava/lang/Object;
.source "JsonPullParser.java"

# interfaces
.implements Lorg/json/simple/parser/ContentHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/client2/jsonstream/JsonPullParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;


# direct methods
.method constructor <init>(Lcom/dropbox/client2/jsonstream/JsonPullParser;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endArray()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    # setter for: Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    invoke-static {v0, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->access$002(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public endJSON()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->End:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    # setter for: Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    invoke-static {v0, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->access$002(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 106
    return-void
.end method

.method public endObject()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    # setter for: Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    invoke-static {v0, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->access$002(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public endObjectEntry()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    const/4 v0, 0x1

    return v0
.end method

.method public primitive(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->Primitive:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    # setter for: Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    invoke-static {v0, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->access$002(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 143
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    iput-object p1, v0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->primitiveValue:Ljava/lang/Object;

    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public startArray()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    # setter for: Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    invoke-static {v0, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->access$002(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method public startJSON()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    return-void
.end method

.method public startObject()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    # setter for: Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    invoke-static {v0, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->access$002(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public startObjectEntry(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/simple/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->FieldStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    # setter for: Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    invoke-static {v0, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->access$002(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 120
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;->this$0:Lcom/dropbox/client2/jsonstream/JsonPullParser;

    iput-object p1, v0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->fieldName:Ljava/lang/String;

    .line 121
    const/4 v0, 0x0

    return v0
.end method
