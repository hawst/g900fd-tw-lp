.class public final Lcom/dropbox/client2/jsonstream/JsonException;
.super Ljava/lang/Exception;
.source "JsonException.java"


# instance fields
.field public final message:Ljava/lang/String;

.field public final position:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-static {p1, p2}, Lcom/dropbox/client2/jsonstream/JsonException;->buildMessage(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 11
    iput p1, p0, Lcom/dropbox/client2/jsonstream/JsonException;->position:I

    .line 12
    iput-object p2, p0, Lcom/dropbox/client2/jsonstream/JsonException;->message:Ljava/lang/String;

    .line 13
    return-void
.end method

.method public constructor <init>(Lorg/json/simple/parser/ParseException;)V
    .locals 2
    .param p1, "ex"    # Lorg/json/simple/parser/ParseException;

    .prologue
    .line 25
    invoke-virtual {p1}, Lorg/json/simple/parser/ParseException;->getPosition()I

    move-result v0

    invoke-static {p1}, Lcom/dropbox/client2/jsonstream/JsonException;->translateParseException(Lorg/json/simple/parser/ParseException;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(ILjava/lang/String;)V

    .line 26
    return-void
.end method

.method private static buildMessage(ILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "offset"    # I
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 17
    if-gez p0, :cond_0

    .line 20
    .end local p1    # "message":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "message":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "offset "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static translateParseException(Lorg/json/simple/parser/ParseException;)Ljava/lang/String;
    .locals 3
    .param p0, "ex"    # Lorg/json/simple/parser/ParseException;

    .prologue
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .local v0, "buf":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/json/simple/parser/ParseException;->getErrorType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 42
    const-string v1, "unexpected parsing error"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 33
    :pswitch_0
    const-string v1, "unexpected character ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/json/simple/parser/ParseException;->getUnexpectedObject()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 36
    :pswitch_1
    const-string v1, "unexpected token "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/json/simple/parser/ParseException;->getUnexpectedObject()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 39
    :pswitch_2
    const-string v1, "unexpected exception while parsing: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/json/simple/parser/ParseException;->getUnexpectedObject()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
