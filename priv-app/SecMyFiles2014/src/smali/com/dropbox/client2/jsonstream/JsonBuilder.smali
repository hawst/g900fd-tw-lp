.class public Lcom/dropbox/client2/jsonstream/JsonBuilder;
.super Ljava/lang/Object;
.source "JsonBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dropbox/client2/jsonstream/JsonBuilder$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/dropbox/client2/jsonstream/JsonBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/dropbox/client2/jsonstream/JsonBuilder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method public static build(Lcom/dropbox/client2/jsonstream/JsonPullParser;)Ljava/lang/Object;
    .locals 8
    .param p0, "parser"    # Lcom/dropbox/client2/jsonstream/JsonPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v1

    .line 24
    .local v1, "e":Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    sget-object v5, Lcom/dropbox/client2/jsonstream/JsonBuilder$1;->$SwitchMap$com$dropbox$client2$jsonstream$JsonPullParser$Event:[I

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 52
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\'parser\' is in a bad state, got event: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 27
    :pswitch_0
    iget-object v2, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->primitiveValue:Ljava/lang/Object;

    .line 49
    :cond_0
    :goto_0
    return-object v2

    .line 30
    :pswitch_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 31
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v1

    .line 32
    :goto_1
    sget-object v5, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->FieldStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-ne v1, v5, :cond_1

    .line 33
    iget-object v3, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->fieldName:Ljava/lang/String;

    .line 34
    .local v3, "name":Ljava/lang/String;
    invoke-static {p0}, Lcom/dropbox/client2/jsonstream/JsonBuilder;->build(Lcom/dropbox/client2/jsonstream/JsonPullParser;)Ljava/lang/Object;

    move-result-object v4

    .line 35
    .local v4, "value":Ljava/lang/Object;
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v1

    .line 37
    goto :goto_1

    .line 38
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/Object;
    :cond_1
    sget-boolean v5, Lcom/dropbox/client2/jsonstream/JsonBuilder;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    sget-object v5, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-eq v1, v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 42
    .end local v2    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :pswitch_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v0, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :goto_2
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->peek()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v5

    sget-object v6, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-eq v5, v6, :cond_2

    .line 44
    invoke-static {p0}, Lcom/dropbox/client2/jsonstream/JsonBuilder;->build(Lcom/dropbox/client2/jsonstream/JsonPullParser;)Ljava/lang/Object;

    move-result-object v4

    .line 45
    .restart local v4    # "value":Ljava/lang/Object;
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 47
    .end local v4    # "value":Ljava/lang/Object;
    :cond_2
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v1

    .line 48
    sget-boolean v5, Lcom/dropbox/client2/jsonstream/JsonBuilder;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    sget-object v5, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-eq v1, v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->peek()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    :cond_3
    move-object v2, v0

    .line 49
    goto :goto_0

    .line 24
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static skip(Lcom/dropbox/client2/jsonstream/JsonPullParser;)V
    .locals 4
    .param p0, "parser"    # Lcom/dropbox/client2/jsonstream/JsonPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v0

    .line 63
    .local v0, "e":Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonBuilder$1;->$SwitchMap$com$dropbox$client2$jsonstream$JsonPullParser$Event:[I

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 86
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\'parser\' is in a bad state, got event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 69
    :pswitch_0
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v0

    .line 70
    :goto_0
    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->FieldStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-ne v0, v1, :cond_0

    .line 71
    invoke-static {p0}, Lcom/dropbox/client2/jsonstream/JsonBuilder;->skip(Lcom/dropbox/client2/jsonstream/JsonPullParser;)V

    .line 72
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_0
    sget-boolean v1, Lcom/dropbox/client2/jsonstream/JsonBuilder;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-eq v0, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 78
    :goto_1
    :pswitch_1
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->peek()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v1

    sget-object v2, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-eq v1, v2, :cond_1

    .line 79
    invoke-static {p0}, Lcom/dropbox/client2/jsonstream/JsonBuilder;->skip(Lcom/dropbox/client2/jsonstream/JsonPullParser;)V

    goto :goto_1

    .line 81
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v0

    .line 82
    sget-boolean v1, Lcom/dropbox/client2/jsonstream/JsonBuilder;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-eq v0, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->peek()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 83
    :cond_2
    :pswitch_2
    return-void

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
