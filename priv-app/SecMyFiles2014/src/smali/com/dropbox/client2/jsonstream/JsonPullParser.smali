.class public final Lcom/dropbox/client2/jsonstream/JsonPullParser;
.super Ljava/lang/Object;
.source "JsonPullParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    }
.end annotation


# instance fields
.field private final Handler:Lorg/json/simple/parser/ContentHandler;

.field private event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

.field public fieldName:Ljava/lang/String;

.field private final parser:Lorg/json/simple/parser/JSONParser;

.field public primitiveValue:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;

    invoke-direct {v0, p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser$1;-><init>(Lcom/dropbox/client2/jsonstream/JsonPullParser;)V

    iput-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->Handler:Lorg/json/simple/parser/ContentHandler;

    .line 18
    new-instance v0, Lorg/json/simple/parser/JSONParser;

    invoke-direct {v0}, Lorg/json/simple/parser/JSONParser;-><init>()V

    iput-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->parser:Lorg/json/simple/parser/JSONParser;

    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->crankParser(Ljava/io/Reader;Z)V

    .line 20
    return-void
.end method

.method static synthetic access$002(Lcom/dropbox/client2/jsonstream/JsonPullParser;Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    .locals 0
    .param p0, "x0"    # Lcom/dropbox/client2/jsonstream/JsonPullParser;
    .param p1, "x1"    # Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    return-object p1
.end method

.method private crankParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->crankParser(Ljava/io/Reader;Z)V

    .line 32
    return-void
.end method

.method private crankParser(Ljava/io/Reader;Z)V
    .locals 3
    .param p1, "in"    # Ljava/io/Reader;
    .param p2, "resume"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 24
    :try_start_0
    iget-object v1, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->parser:Lorg/json/simple/parser/JSONParser;

    iget-object v2, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->Handler:Lorg/json/simple/parser/ContentHandler;

    invoke-virtual {v1, p1, v2, p2}, Lorg/json/simple/parser/JSONParser;->parse(Ljava/io/Reader;Lorg/json/simple/parser/ContentHandler;Z)V
    :try_end_0
    .catch Lorg/json/simple/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    return-void

    .line 25
    :catch_0
    move-exception v0

    .line 26
    .local v0, "ex":Lorg/json/simple/parser/ParseException;
    new-instance v1, Lcom/dropbox/client2/jsonstream/JsonException;

    invoke-direct {v1, v0}, Lcom/dropbox/client2/jsonstream/JsonException;-><init>(Lorg/json/simple/parser/ParseException;)V

    throw v1
.end method


# virtual methods
.method public getPosition()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->parser:Lorg/json/simple/parser/JSONParser;

    invoke-virtual {v0}, Lorg/json/simple/parser/JSONParser;->getPosition()I

    move-result v0

    return v0
.end method

.method public next()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v1, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-nez v1, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->crankParser()V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 66
    .local v0, "ret":Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    .line 67
    return-object v0
.end method

.method public peek()Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/dropbox/client2/jsonstream/JsonException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    if-nez v0, :cond_0

    .line 47
    invoke-direct {p0}, Lcom/dropbox/client2/jsonstream/JsonPullParser;->crankParser()V

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/dropbox/client2/jsonstream/JsonPullParser;->event:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    return-object v0
.end method
