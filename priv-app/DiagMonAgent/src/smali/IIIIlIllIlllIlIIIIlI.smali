.class public LIIIIlIllIlllIlIIIIlI;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Ljavax/net/ssl/X509TrustManager;


# instance fields
.field private llIIIIlllllIIllIIllI:Ljavax/net/ssl/X509TrustManager;


# direct methods
.method constructor <init>(Ljava/security/KeyStore;)V
    .locals 2

    .prologue
    .line 1888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1891
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    .line 1892
    invoke-virtual {v0, p1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 1893
    invoke-direct {p0, v0}, LIIIIlIllIlllIlIIIIlI;->llIIIIlllllIIllIIllI(Ljavax/net/ssl/TrustManagerFactory;)Ljavax/net/ssl/X509TrustManager;

    move-result-object v0

    iput-object v0, p0, LIIIIlIllIlllIlIIIIlI;->llIIIIlllllIIllIIllI:Ljavax/net/ssl/X509TrustManager;

    .line 1895
    iget-object v0, p0, LIIIIlIllIlllIlIIIIlI;->llIIIIlllllIIllIIllI:Ljavax/net/ssl/X509TrustManager;

    if-nez v0, :cond_0

    .line 1897
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "X509TrustManager is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 1898
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "X509TrustManager is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1901
    :catch_0
    move-exception v0

    .line 1903
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1905
    :cond_0
    return-void
.end method

.method private llIIIIlllllIIllIIllI(Ljavax/net/ssl/TrustManagerFactory;)Ljavax/net/ssl/X509TrustManager;
    .locals 3

    .prologue
    .line 1929
    invoke-virtual {p1}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v1

    .line 1930
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1932
    aget-object v2, v1, v0

    instance-of v2, v2, Ljavax/net/ssl/X509TrustManager;

    if-eqz v2, :cond_0

    .line 1934
    aget-object v0, v1, v0

    check-cast v0, Ljavax/net/ssl/X509TrustManager;

    .line 1937
    :goto_1
    return-object v0

    .line 1930
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1937
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1910
    sget-boolean v0, LlIIIIIIIlllllllIIlll;->llIIIIlllllIIllIIllI:Z

    if-eqz v0, :cond_0

    .line 1911
    iget-object v0, p0, LIIIIlIllIlllIlIIIIlI;->llIIIIlllllIIllIIllI:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v0, p1, p2}, Ljavax/net/ssl/X509TrustManager;->checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 1912
    :cond_0
    return-void
.end method

.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1917
    sget-boolean v0, LlIIIIIIIlllllllIIlll;->llIIIIlllllIIllIIllI:Z

    if-eqz v0, :cond_0

    .line 1918
    iget-object v0, p0, LIIIIlIllIlllIlIIIIlI;->llIIIIlllllIIllIIllI:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v0, p1, p2}, Ljavax/net/ssl/X509TrustManager;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 1919
    :cond_0
    return-void
.end method

.method public getAcceptedIssuers()[Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 1924
    iget-object v0, p0, LIIIIlIllIlllIlIIIIlI;->llIIIIlllllIIllIIllI:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v0}, Ljavax/net/ssl/X509TrustManager;->getAcceptedIssuers()[Ljava/security/cert/X509Certificate;

    move-result-object v0

    return-object v0
.end method
