.class public LIIIlIIllIIIllIIlIllI;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field private llIIIIlllllIIllIIllI:Ljava/security/Key;

.field private lllIlIlIIIllIIlIllIl:Ljava/security/SecureRandom;

.field private llllIIIllIlIIIIllllI:Ljavax/crypto/Cipher;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "AES/CBC/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    iput-object v0, p0, LIIIlIIllIIIllIIlIllI;->llllIIIllIlIIIIllllI:Ljavax/crypto/Cipher;

    .line 37
    const-string v0, "SHA1PRNG"

    invoke-static {v0}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v0

    iput-object v0, p0, LIIIlIIllIIIllIIlIllI;->lllIlIlIIIllIIlIllIl:Ljava/security/SecureRandom;

    .line 38
    return-void
.end method

.method public static llIIIIlllllIIllIIllI()LIIIlIIllIIIllIIlIllI;
    .locals 1

    .prologue
    .line 41
    new-instance v0, LIIIlIIllIIIllIIlIllI;

    invoke-direct {v0}, LIIIlIIllIIIllIIlIllI;-><init>()V

    invoke-virtual {v0}, LIIIlIIllIIIllIIlIllI;->llllIIIllIlIIIIllllI()LIIIlIIllIIIllIIlIllI;

    move-result-object v0

    return-object v0
.end method

.method private llIlIIIIlIIIIIlIlIII()Ljava/security/Key;
    .locals 3

    .prologue
    .line 64
    const-string v0, "AES"

    invoke-static {v0}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v0

    .line 65
    const/16 v1, 0x80

    iget-object v2, p0, LIIIlIIllIIIllIIlIllI;->lllIlIlIIIllIIlIllIl:Ljava/security/SecureRandom;

    invoke-virtual {v0, v1, v2}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 66
    invoke-virtual {v0}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public declared-synchronized llIIIIlllllIIllIIllI([B)[B
    .locals 5

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LIIIlIIllIIIllIIlIllI;->llllIIIllIlIIIIllllI:Ljavax/crypto/Cipher;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v0

    new-array v0, v0, [B

    .line 72
    iget-object v1, p0, LIIIlIIllIIIllIIlIllI;->lllIlIlIIIllIIlIllIl:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 73
    iget-object v1, p0, LIIIlIIllIIIllIIlIllI;->llllIIIllIlIIIIllllI:Ljavax/crypto/Cipher;

    const/4 v2, 0x1

    iget-object v3, p0, LIIIlIIllIIIllIIlIllI;->llIIIIlllllIIllIIllI:Ljava/security/Key;

    new-instance v4, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v4, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v1, v2, v3, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 74
    iget-object v1, p0, LIIIlIIllIIIllIIlIllI;->llllIIIllIlIIIIllllI:Ljavax/crypto/Cipher;

    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    invoke-static {v0, v1}, LIlllIIllIIIIllllIlII;->llIIIIlllllIIllIIllI([B[B)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public lllIlIlIIIllIIlIllIl()[B
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LIIIlIIllIIIllIIlIllI;->llIIIIlllllIIllIIllI:Ljava/security/Key;

    invoke-interface {v0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    return-object v0
.end method

.method public llllIIIllIlIIIIllllI()LIIIlIIllIIIllIIlIllI;
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, LIIIlIIllIIIllIIlIllI;->llIlIIIIlIIIIIlIlIII()Ljava/security/Key;

    move-result-object v0

    iput-object v0, p0, LIIIlIIllIIIllIIlIllI;->llIIIIlllllIIllIIllI:Ljava/security/Key;

    .line 51
    return-object p0
.end method
