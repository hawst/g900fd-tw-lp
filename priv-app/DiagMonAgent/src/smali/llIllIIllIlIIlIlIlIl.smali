.class public LllIllIIllIlIIlIlIlIl;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static final llIIIIlllllIIllIIllI:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 125
    const-string v0, "^com\\.sec\\.android\\.log\\.([^.]+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI:Ljava/util/regex/Pattern;

    return-void
.end method

.method private static IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)Ljava/util/List;
    .locals 4

    .prologue
    .line 144
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->queryContentProviders(Ljava/lang/String;II)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 150
    :goto_0
    return-object v0

    .line 147
    :catch_0
    move-exception v0

    .line 150
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method private static llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com.sec.android.log."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 192
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 194
    :try_start_0
    const-string v1, "get"

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, p2, v2}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 195
    if-eqz v0, :cond_0

    .line 210
    :goto_0
    return-object v0

    .line 197
    :catch_0
    move-exception v0

    .line 198
    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/SecurityException;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User not agreed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 210
    :cond_0
    :goto_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    .line 201
    :cond_1
    invoke-static {v0}, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 202
    :catch_1
    move-exception v0

    .line 203
    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/IllegalArgumentException;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 204
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_1

    .line 206
    :cond_2
    invoke-static {v0}, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 207
    :catch_2
    move-exception v0

    .line 208
    invoke-static {v0}, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 176
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 177
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 178
    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "content"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "logList"

    invoke-static {p0, v3, v4}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_0
    return-object v1
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-static {p0}, LllIllIIllIlIIlIlIlIl;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 95
    const/4 v4, 0x0

    :try_start_0
    invoke-static {p0, v3, v4}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 93
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :catch_0
    move-exception v3

    .line 97
    invoke-static {v3}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/SecurityException;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 100
    invoke-static {v3}, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 101
    :catch_1
    move-exception v3

    .line 102
    invoke-static {v3}, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 104
    :cond_1
    return-void
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 57
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 59
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 60
    const-string v2, "value"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 61
    invoke-static {p1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "set"

    const-string v4, "registered"

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 74
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 63
    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/IllegalArgumentException;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_0
    throw v0

    .line 67
    :catch_1
    move-exception v0

    .line 68
    invoke-static {v0}, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 69
    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/SecurityException;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "Agreement would be changed in progress."

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 72
    :cond_1
    throw v0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 35
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 38
    :try_start_0
    invoke-static {p1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "get"

    const-string v3, "agreed"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_0

    .line 40
    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 53
    :goto_0
    return v0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/IllegalArgumentException;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 45
    :cond_1
    throw v0

    .line 46
    :catch_1
    move-exception v0

    .line 47
    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/SecurityException;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    throw v0
.end method

.method private static llIIIIlllllIIllIIllI(Ljava/lang/IllegalArgumentException;)Z
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unknown URI "

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/SecurityException;)Z
    .locals 2

    .prologue
    .line 27
    const-string v0, "Permission Denial"

    invoke-virtual {p0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static llIIIIlllllIIllIIllI([Ljava/lang/String;)[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 133
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 134
    array-length v3, p0

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, p0, v0

    .line 135
    sget-object v5, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 136
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 137
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_1
    new-array v0, v1, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)[Ljava/lang/String;
    .locals 5

    .prologue
    .line 154
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 155
    invoke-static {p0}, LllIllIIllIlIIlIlIlIl;->IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 156
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ProviderInfo;

    .line 157
    iget-object v3, v0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    const-string v4, "com.sec.android.log."

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 158
    iget-object v0, v0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 160
    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static lllIlIlIIIllIIlIllIl(Landroid/content/Context;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    invoke-static {p0}, LllIllIIllIlIIlIlIlIl;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static lllIlIlIIIllIIlIllIl(Landroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 166
    invoke-static {p1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "authorityList"

    invoke-static {p0, v0, v2}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 167
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 168
    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    .line 170
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static llllIIIllIlIIIIllllI(Landroid/content/Context;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 184
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 185
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 186
    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "content"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "plainLogList"

    invoke-static {p0, v3, v4}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    :cond_0
    return-object v1
.end method

.method public static llllIIIllIlIIIIllllI(Landroid/content/Context;)Z
    .locals 6

    .prologue
    .line 107
    const-string v1, "x6g1q14r75"

    .line 108
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 112
    :try_start_0
    invoke-static {v1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "get"

    const-string v4, "testPollingMode"

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 113
    if-eqz v0, :cond_0

    .line 114
    const-string v2, "result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 122
    :goto_0
    return v0

    .line 116
    :catch_0
    move-exception v0

    .line 117
    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/IllegalArgumentException;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 120
    :cond_1
    throw v0
.end method

.method public static llllIIIllIlIIIIllllI(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 77
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 80
    :try_start_0
    invoke-static {p1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "get"

    const-string v3, "registered"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/IllegalArgumentException;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 89
    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Permission Denial"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    throw v0
.end method
