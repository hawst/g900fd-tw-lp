.class public Lcom/diagmondm/noti/XNOTIAdapter;
.super LIIlIlIIIlIIlIlIIIIII;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static IllIlIIIIlIIlIIIllIl:Z = false

.field private static llIlIIIIlIIIIIlIlIII:Ljava/util/LinkedList; = null

.field private static lllIlIlIIIllIIlIllIl:Ljava/lang/String; = null

.field private static llllIIIllIlIIIIllllI:Lcom/diagmondm/noti/XNOTIWapPush; = null

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    sput-object v0, Lcom/diagmondm/noti/XNOTIAdapter;->llllIIIllIlIIIIllllI:Lcom/diagmondm/noti/XNOTIWapPush;

    .line 31
    sput-object v0, Lcom/diagmondm/noti/XNOTIAdapter;->lllIlIlIIIllIIlIllIl:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/diagmondm/noti/XNOTIAdapter;->llIlIIIIlIIIIIlIlIII:Ljava/util/LinkedList;

    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lcom/diagmondm/noti/XNOTIAdapter;->IllIlIIIIlIIlIIIllIl:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, LIIlIlIIIlIIlIlIIIIII;-><init>()V

    .line 41
    return-void
.end method

.method public static IIIIlIlIIIIIIIlIIlll()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 211
    sget-object v1, Lcom/diagmondm/noti/XNOTIAdapter;->llIlIIIIlIIIIIlIlIII:Ljava/util/LinkedList;

    monitor-enter v1

    .line 213
    :try_start_0
    sget-object v0, Lcom/diagmondm/noti/XNOTIAdapter;->llIlIIIIlIIIIIlIlIII:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIIIlllllIIllIIllI(Z)V

    .line 216
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "mPushDataQueue is empty. return"

    invoke-virtual {v0, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 217
    monitor-exit v1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    sget-object v0, Lcom/diagmondm/noti/XNOTIAdapter;->llIlIIIIlIIIIIlIlIII:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diagmondm/noti/llllIIIllIlIIIIllllI;

    .line 221
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v3, "mPushDataQueue poll"

    invoke-virtual {v2, v3}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 222
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    if-eqz v0, :cond_0

    .line 226
    iget v1, v0, Lcom/diagmondm/noti/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:I

    packed-switch v1, :pswitch_data_0

    .line 237
    invoke-static {v4}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIIIlllllIIllIIllI(Z)V

    .line 238
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "PUSH_TYPE is not exist"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 222
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 229
    :pswitch_0
    iget-object v0, v0, Lcom/diagmondm/noti/llllIIIllIlIIIIllllI;->llllIIIllIlIIIIllllI:[B

    invoke-static {v0}, Lcom/diagmondm/noti/XNOTIAdapter;->llllIIIllIlIIIIllllI([B)V

    goto :goto_0

    .line 233
    :pswitch_1
    iget-object v0, v0, Lcom/diagmondm/noti/llllIIIllIlIIIIllllI;->llllIIIllIlIIIIllllI:[B

    invoke-static {v0}, Lcom/diagmondm/noti/XNOTIAdapter;->lllIlIlIIIllIIlIllIl([B)V

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static IIIIllIlIIlIIIIlllIl(I)V
    .locals 3

    .prologue
    .line 150
    .line 155
    invoke-static {p0}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIllllIIlllIIIIlll(I)Lcom/diagmondm/noti/XNOTIWapPush;

    move-result-object v0

    .line 156
    if-nez v0, :cond_0

    .line 175
    :goto_0
    return-void

    .line 161
    :cond_0
    packed-switch p0, :pswitch_data_0

    .line 172
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "Not Support Content Type"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0

    .line 164
    :pswitch_0
    new-instance v1, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;

    invoke-direct {v1}, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;-><init>()V

    .line 165
    const/4 v2, 0x1

    iput v2, v1, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->llllIIIllIlIIIIllllI:I

    .line 166
    iget-object v2, v0, Lcom/diagmondm/noti/XNOTIWapPush;->pBody:[B

    iput-object v2, v1, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII:[B

    .line 167
    iget-object v0, v0, Lcom/diagmondm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/diagmondm/noti/XNOTIWapPushInfo;

    iget v0, v0, Lcom/diagmondm/noti/XNOTIWapPushInfo;->nBodyLen:I

    iput v0, v1, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->llIIllllIIlllIIIIlll:I

    .line 168
    const/16 v0, 0x1e

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/diagmondm/eng/core/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_0
    .end packed-switch
.end method

.method public static IIIIlllIIIlIlIlllIII()Z
    .locals 1

    .prologue
    .line 179
    sget-boolean v0, Lcom/diagmondm/noti/XNOTIAdapter;->IllIlIIIIlIIlIIIllIl:Z

    return v0
.end method

.method public static IIIlIIllIlIIllIlllII(I)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 423
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IIIIIIlIIIllllllIlII()Lcom/diagmondm/db/file/XDBSessionSaveInfo;

    move-result-object v0

    .line 424
    if-nez v0, :cond_0

    .line 426
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "Get Noti Info File Read Error"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 427
    const/4 v0, -0x1

    .line 480
    :goto_0
    return v0

    .line 430
    :cond_0
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nSessionSaveState:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 431
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nNotiUiEvent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 432
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nNotiRetryCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 434
    iget v1, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    if-ne v1, v4, :cond_6

    .line 436
    :cond_1
    iget v1, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_2

    .line 438
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "Noti Retry Count MAX. All Clear"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 439
    const/16 v0, 0x7d

    invoke-static {v5, v0}, Lcom/diagmondm/eng/core/IllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Object;I)V

    .line 441
    invoke-static {}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIIIlllllIIllllllI()V

    .line 442
    invoke-static {}, Lcom/diagmondm/noti/XNOTIAdapter;->IIlIlIIIlIIlIlIIIIII()V

    .line 480
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 446
    :cond_2
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "DiagMon DM always background mode!!"

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 448
    iget v1, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 450
    invoke-static {}, Lcom/diagmondm/ui/llIIIIlllllIIllIIllI;->lllIlIlIIIllIIlIllIl()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 452
    invoke-static {v4}, Lcom/diagmondm/ui/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI(I)V

    .line 469
    :cond_3
    :goto_2
    iget v1, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    iget v2, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    iget v0, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v1, v2, v0}, Lcom/diagmondm/db/file/XDB;->llllIIIllIlIIIIllllI(III)Z

    goto :goto_1

    .line 455
    :cond_4
    iget v1, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 457
    invoke-static {}, Lcom/diagmondm/ui/llIIIIlllllIIllIIllI;->lllIlIlIIIllIIlIllIl()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 459
    invoke-static {v4}, Lcom/diagmondm/ui/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI(I)V

    goto :goto_2

    .line 464
    :cond_5
    iget v1, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v1}, Lcom/diagmondm/db/file/XDB;->IllIlIIIIlIIlIIIllIl(I)V

    .line 465
    iget v1, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v1}, Lcom/diagmondm/noti/XNOTIAdapter;->llllllIllIlIlllIIlIl(I)I

    move-result v1

    .line 466
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SetEvent. Event is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 467
    invoke-static {v5, v1}, Lcom/diagmondm/eng/core/IllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Object;I)V

    goto :goto_2

    .line 474
    :cond_6
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "Current NOTI NOT SAVED State. EXIT. EXIT."

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 476
    invoke-static {}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIIIlllllIIllllllI()V

    .line 477
    invoke-static {}, Lcom/diagmondm/noti/XNOTIAdapter;->IIlIlIIIlIIlIlIIIIII()V

    goto :goto_1
.end method

.method public static IIlIlIIIlIIlIlIIIIII()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 346
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IIIlIIllIlIIllIlllII()Ljava/lang/String;

    move-result-object v0

    .line 348
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 350
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Delete Noti Msg sessionId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 351
    invoke-static {v0}, Lcom/diagmondm/db/file/llIIllllIIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 354
    :cond_0
    invoke-static {}, Lcom/diagmondm/db/file/llIIllllIIlllIIIIlll;->llIIIIlllllIIllIIllI()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 358
    invoke-static {}, Lcom/diagmondm/db/file/llIIllllIIlllIIIIlll;->lllIlIlIIIllIIlIllIl()Lcom/diagmondm/db/file/XDBNotiInfo;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_3

    .line 361
    iget-boolean v1, v0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDeviceInit:Z

    if-nez v1, :cond_2

    .line 363
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "Next Noti Msg Execute"

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 364
    const/16 v1, 0x1f

    invoke-static {v1, v0, v4}, Lcom/diagmondm/eng/core/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 384
    :cond_1
    :goto_0
    return-void

    .line 368
    :cond_2
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "Next Fail Intent Execute"

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 369
    const/16 v1, 0x25

    invoke-static {v1, v0, v4}, Lcom/diagmondm/eng/core/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 374
    :cond_3
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "notiInfo is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 379
    :cond_4
    invoke-static {}, LllIIlIlIlIlIIIIIIlIl;->llllllIllIlIlllIIlIl()Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/diagmondm/XDMService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-static {v0, v1, v2, v3}, Lcom/diagmondm/XDMDiagMonCloseService;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public static IlIlIlIlIlIIlllllIlI(I)V
    .locals 4

    .prologue
    .line 528
    .line 531
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/diagmondm/db/file/XDB;->llllIIIllIlIIIIllllI(III)Z

    .line 532
    invoke-static {}, LlIlIIlIlIIlIlIIIIlll;->llllIIIllIlIIIIllllI()I

    move-result v0

    .line 534
    if-nez v0, :cond_0

    .line 536
    invoke-static {p0}, Lcom/diagmondm/noti/XNOTIAdapter;->llllllIllIlIlllIIlIl(I)I

    move-result v0

    .line 537
    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/diagmondm/eng/core/IllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Object;I)V

    .line 545
    :goto_0
    return-void

    .line 542
    :cond_0
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nNetworkState is Used."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 543
    invoke-static {p0}, Lcom/diagmondm/noti/XNOTIAdapter;->llIlIllllllllllllllI(I)V

    goto :goto_0
.end method

.method public static lIIIIIIlIlllIIlllIIl()V
    .locals 4

    .prologue
    .line 388
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IIIlIIllIlIIllIlllII()Ljava/lang/String;

    move-result-object v0

    .line 390
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 392
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Delete Noti Msg sessionId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 393
    invoke-static {v0}, Lcom/diagmondm/db/file/llIIllllIIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 396
    :cond_0
    invoke-static {}, Lcom/diagmondm/db/file/llIIllllIIlllIIIIlll;->llIIIIlllllIIllIIllI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 398
    invoke-static {}, Lcom/diagmondm/db/file/llIIllllIIlllIIIIlll;->llllIIIllIlIIIIllllI()I

    move-result v1

    .line 399
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 402
    invoke-static {}, Lcom/diagmondm/db/file/llIIllllIIlllIIIIlll;->lllIlIlIIIllIIlIllIl()Lcom/diagmondm/db/file/XDBNotiInfo;

    move-result-object v2

    .line 403
    if-eqz v2, :cond_1

    .line 405
    iget-object v2, v2, Lcom/diagmondm/db/file/XDBNotiInfo;->szSessionId:Ljava/lang/String;

    invoke-static {v2}, Lcom/diagmondm/db/file/llIIllllIIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 399
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 411
    :cond_2
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "Noti was not saved."

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 413
    :cond_3
    return-void
.end method

.method public static lIIIIIIllIIllIlIllII()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 486
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IIIIIIlIIIllllllIlII()Lcom/diagmondm/db/file/XDBSessionSaveInfo;

    move-result-object v2

    .line 487
    if-nez v2, :cond_0

    .line 489
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "Get Noti Info File Read Error"

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 508
    :goto_0
    return v0

    .line 493
    :cond_0
    sget-object v3, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nSessionSaveState:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 494
    sget-object v3, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nNotiUiEvent:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 495
    sget-object v3, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nNotiRetryCount:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 497
    iget v3, v2, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    if-eq v3, v1, :cond_1

    iget v2, v2, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 499
    :cond_1
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "Current NOTI SAVED State"

    invoke-virtual {v0, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    move v0, v1

    .line 500
    goto :goto_0

    .line 504
    :cond_2
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "Current NOTI NOT SAVED State. EXIT. EXIT."

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Lcom/diagmondm/noti/XNOTIWapPush;I)I
    .locals 1

    .prologue
    .line 116
    if-nez p0, :cond_0

    .line 117
    const/4 v0, 0x1

    .line 121
    :goto_0
    return v0

    .line 119
    :cond_0
    sput-object p0, Lcom/diagmondm/noti/XNOTIAdapter;->llllIIIllIlIIIIllllI:Lcom/diagmondm/noti/XNOTIWapPush;

    .line 121
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(I[B)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 190
    if-nez p1, :cond_0

    .line 192
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "gPushData  Uri is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 206
    :goto_0
    return-void

    .line 196
    :cond_0
    array-length v0, p1

    new-array v0, v0, [B

    .line 197
    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 199
    new-instance v1, Lcom/diagmondm/noti/llllIIIllIlIIIIllllI;

    invoke-direct {v1, p0, v0}, Lcom/diagmondm/noti/llllIIIllIlIIIIllllI;-><init>(I[B)V

    .line 201
    sget-object v2, Lcom/diagmondm/noti/XNOTIAdapter;->llIlIIIIlIIIIIlIlIII:Ljava/util/LinkedList;

    monitor-enter v2

    .line 203
    :try_start_0
    sget-object v0, Lcom/diagmondm/noti/XNOTIAdapter;->llIlIIIIlIIIIIlIlIII:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 204
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "mPushDataQueue add"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 205
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static llIIIIlllllIIllIIllI(Z)V
    .locals 3

    .prologue
    .line 184
    sput-boolean p0, Lcom/diagmondm/noti/XNOTIAdapter;->IllIlIIIIlIIlIIIllIl:Z

    .line 185
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notiProcessing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public static llIIIIlllllIIllllllI()V
    .locals 2

    .prologue
    .line 272
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, ""

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 276
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lcom/diagmondm/ui/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(I)V

    .line 277
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/diagmondm/ui/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI(I)V

    .line 278
    const/4 v0, 0x0

    invoke-static {v0}, LIIlIlIIIlIIlIlIIIIII;->llIIIIlllllIIllIIllI(I)Z

    .line 279
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/diagmondm/db/file/XDB;->IllIlIIIIlIIlIIIllIl(I)V

    .line 280
    const/16 v0, 0x9

    invoke-static {v0}, Lcom/diagmondm/ui/IIllllIIlIIllIIIIlll;->llIIIIlllllIIllIIllI(I)V

    .line 282
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IlIlIlllIlllIlIIllII()V

    .line 283
    invoke-static {}, Lcom/diagmondm/noti/XNOTIAdapter;->llllIllIIIIIlIIllIII()V

    .line 285
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llllIIIllIlIIIIllllI()Ljava/lang/String;

    move-result-object v0

    .line 286
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/diagmondm/db/file/XDBDiagMonFunction;->llIIIIlllllIIllIIllI(Ljava/lang/String;I)V

    .line 287
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/diagmondm/db/file/XDBDiagMonFunction;->llllIIIllIlIIIIllllI(Ljava/lang/String;I)V

    .line 288
    const-string v1, ""

    invoke-static {v0, v1}, Lcom/diagmondm/db/file/XDBDiagMonFunction;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const/4 v0, 0x0

    sput v0, Lcom/diagmondm/XDMService;->IllIlIIIIlIIlIIIllIl:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    :goto_0
    return-void

    .line 292
    :catch_0
    move-exception v0

    .line 294
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIllllIIlllIIIIlll(I)Lcom/diagmondm/noti/XNOTIWapPush;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/diagmondm/noti/XNOTIAdapter;->llllIIIllIlIIIIllllI:Lcom/diagmondm/noti/XNOTIWapPush;

    return-object v0
.end method

.method public static llIlIllllllllllllllI(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 333
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, ""

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 334
    const/4 v0, 0x1

    invoke-static {v0, p0, v2}, Lcom/diagmondm/db/file/XDB;->llllIIIllIlIIIIllllI(III)Z

    .line 335
    invoke-static {v2}, Lcom/diagmondm/db/file/XDB;->IllIlIIIIlIIlIIIllIl(I)V

    .line 336
    return-void
.end method

.method public static lllIlIlIIIllIIlIllIl([B)V
    .locals 2

    .prologue
    .line 259
    if-nez p0, :cond_0

    .line 261
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "pushData is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 268
    :goto_0
    return-void

    .line 265
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIIIlllllIIllIIllI(Z)V

    .line 266
    new-instance v0, Lcom/diagmondm/noti/XNOTIAdapter;

    invoke-direct {v0}, Lcom/diagmondm/noti/XNOTIAdapter;-><init>()V

    .line 267
    array-length v1, p0

    invoke-virtual {v0, p0, v1}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIIIlllllIIllIIllI([BI)Z

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 140
    sput-object p0, Lcom/diagmondm/noti/XNOTIAdapter;->lllIlIlIIIllIIlIllIl:Ljava/lang/String;

    .line 141
    return-void
.end method

.method public static llllIIIllIlIIIIllllI([B)V
    .locals 3

    .prologue
    .line 246
    if-nez p0, :cond_0

    .line 248
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "pushData is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 255
    :goto_0
    return-void

    .line 252
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIIIlllllIIllIIllI(Z)V

    .line 253
    new-instance v0, Lcom/diagmondm/noti/XNOTIAdapter;

    invoke-direct {v0}, Lcom/diagmondm/noti/XNOTIAdapter;-><init>()V

    .line 254
    array-length v1, p0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Lcom/diagmondm/noti/XNOTIAdapter;->llIIIIlllllIIllIIllI([BILjava/lang/String;)Z

    goto :goto_0
.end method

.method public static llllIllIIIIIlIIllIII()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 513
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IIlIIIIIIllllIIlIIlI()Lcom/diagmondm/db/file/XDBSessionSaveInfo;

    move-result-object v0

    .line 515
    if-eqz v0, :cond_0

    iget v0, v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    if-eqz v0, :cond_0

    .line 517
    invoke-static {v1, v1, v1}, Lcom/diagmondm/db/file/XDB;->llllIIIllIlIIIIllllI(III)Z

    .line 520
    :cond_0
    return-void
.end method

.method public static lllllIIlIIIlIlIIIllI(I)V
    .locals 2

    .prologue
    .line 553
    .line 556
    invoke-static {}, LlIlIIlIlIIlIlIIIIlll;->llllIIIllIlIIIIllllI()I

    move-result v0

    .line 557
    if-eqz v0, :cond_0

    .line 559
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->lllllIIlIIIlIlIIIllI()I

    move-result v0

    .line 560
    packed-switch v0, :pswitch_data_0

    .line 569
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "CAN NOT EXCUTE Noti Resume. EXIT"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 575
    :goto_0
    :pswitch_0
    return-void

    .line 574
    :cond_0
    invoke-static {p0}, Lcom/diagmondm/noti/XNOTIAdapter;->IIIlIIllIlIIllIlllII(I)I

    goto :goto_0

    .line 560
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static llllllIllIlIlllIIlIl(I)I
    .locals 1

    .prologue
    const/16 v0, 0x79

    .line 305
    .line 306
    packed-switch p0, :pswitch_data_0

    .line 324
    :goto_0
    :pswitch_0
    return v0

    .line 312
    :pswitch_1
    const/16 v0, 0x7a

    .line 313
    goto :goto_0

    .line 315
    :pswitch_2
    const/16 v0, 0x7b

    .line 316
    goto :goto_0

    .line 318
    :pswitch_3
    const/16 v0, 0x7c

    .line 319
    goto :goto_0

    .line 306
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public llIIIIlllllIIllIIllI([BI)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 97
    .line 99
    new-instance v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;

    invoke-direct {v0}, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;-><init>()V

    .line 100
    iput v3, v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->llllIIIllIlIIIIllllI:I

    .line 101
    iput-object p1, v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl:[B

    .line 102
    iput p2, v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->IIIIllIlIIlIIIIlllIl:I

    .line 104
    const/16 v1, 0x1e

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/diagmondm/eng/core/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 105
    return v3
.end method

.method public llIIIIlllllIIllIIllI([BILjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 45
    .line 52
    invoke-static {p1, p2}, Lcom/diagmondm/noti/lllIlIlIIIllIIlIllIl;->llllIIIllIlIIIIllllI([BI)Lcom/diagmondm/noti/XNOTIWapPush;

    move-result-object v2

    .line 53
    if-nez v2, :cond_0

    .line 55
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "xnotiPushHdlrParsingWSPHeader Error"

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 92
    :goto_0
    return v0

    .line 59
    :cond_0
    iget-object v3, v2, Lcom/diagmondm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/diagmondm/noti/XNOTIWapPushInfo;

    iget v3, v3, Lcom/diagmondm/noti/XNOTIWapPushInfo;->nContentType:I

    packed-switch v3, :pswitch_data_0

    .line 72
    sget-object v3, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not Support Content Type :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "0x%x"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/diagmondm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/diagmondm/noti/XNOTIWapPushInfo;

    iget v2, v2, Lcom/diagmondm/noti/XNOTIWapPushInfo;->nContentType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v5, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :pswitch_0
    new-instance v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;

    invoke-direct {v0}, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;-><init>()V

    .line 64
    iput v1, v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->llllIIIllIlIIIIllllI:I

    .line 65
    new-array v2, p2, [B

    iput-object v2, v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII:[B

    .line 66
    iput p2, v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->llIIllllIIlllIIIIlll:I

    .line 67
    iput-object p1, v0, Lcom/diagmondm/noti/llIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII:[B

    .line 68
    const/16 v2, 0x1e

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/diagmondm/eng/core/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    move v0, v1

    .line 69
    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x44
        :pswitch_0
    .end packed-switch
.end method
