.class public Lcom/diagmondm/ui/XUISettingActivity;
.super Landroid/preference/PreferenceActivity;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static IIIIllIlIIlIIIIlllIl:Lcom/diagmondm/db/file/XDBProfileInfo;

.field private static IIIlIIllIlIIllIlllII:Lcom/diagmondm/ui/lIIIllIIIlIlIIIlllII;

.field private static IlIlIlIlIlIIlllllIlI:Landroid/app/AlertDialog;

.field private static IllIlIIIIlIIlIIIllIl:[Ljava/lang/String;

.field private static llIIIIlllllIIllIIllI:Landroid/preference/PreferenceScreen;

.field private static llIIllllIIlllIIIIlll:Lcom/diagmondm/db/file/XDBNetworkProfileList;

.field private static llIlIIIIlIIIIIlIlIII:I

.field private static llIlIllllllllllllllI:Landroid/app/Activity;

.field private static lllIlIlIIIllIIlIllIl:Landroid/preference/PreferenceScreen;

.field private static llllIIIllIlIIIIllllI:Landroid/preference/PreferenceScreen;

.field private static llllllIllIlIlllIIlIl:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/diagmondm/ui/XUISettingActivity;->IlIlIlIlIlIIlllllIlI:Landroid/app/AlertDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 138
    return-void
.end method

.method static synthetic IIIIIIlIIIllllllIlII()Landroid/preference/PreferenceScreen;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->lllIlIlIIIllIIlIllIl:Landroid/preference/PreferenceScreen;

    return-object v0
.end method

.method static synthetic IIIIllIlIIlIIIIlllIl()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->IlIlIlIlIlIIlllllIlI:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic IIIlIIllIlIIllIlllII()Landroid/preference/PreferenceScreen;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->llIIIIlllllIIllIIllI:Landroid/preference/PreferenceScreen;

    return-object v0
.end method

.method private static IIlIlIIlIlIIlIlllIIl()V
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->llIIllllIIlllIIIIlll:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBInfoConRef;->CNAP:Lcom/diagmondm/db/file/XDBConRefNAP;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBConRefNAP;->szAddr:Ljava/lang/String;

    .line 58
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    const-string v0, "Unknown"

    .line 61
    :cond_0
    sget-object v1, Lcom/diagmondm/ui/XUISettingActivity;->lllIlIlIIIllIIlIllIl:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 62
    return-void
.end method

.method static synthetic IlIIIllllIIlIIIIIlII()V
    .locals 0

    .prologue
    .line 29
    invoke-static {}, Lcom/diagmondm/ui/XUISettingActivity;->lIlIIlIlIIlIlIIIIlll()V

    return-void
.end method

.method static synthetic IlIlIlIlIlIIlllllIlI()Landroid/preference/PreferenceScreen;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->llllIIIllIlIIIIllllI:Landroid/preference/PreferenceScreen;

    return-object v0
.end method

.method private static IlIlllIIlIlIIIlIlIll()V
    .locals 3

    .prologue
    .line 51
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->llllIIIllIlIIIIllllI:Landroid/preference/PreferenceScreen;

    sget-object v1, Lcom/diagmondm/ui/XUISettingActivity;->IllIlIIIIlIIlIIIllIl:[Ljava/lang/String;

    sget v2, Lcom/diagmondm/ui/XUISettingActivity;->llIlIIIIlIIIIIlIlIII:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 52
    return-void
.end method

.method static synthetic IllIlIIIIlIIlIIIllIl()Lcom/diagmondm/db/file/XDBProfileInfo;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->IIIIllIlIIlIIIIlllIl:Lcom/diagmondm/db/file/XDBProfileInfo;

    return-object v0
.end method

.method private static lIlIIlIlIIlIlIIIIlll()V
    .locals 3

    .prologue
    .line 73
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/diagmondm/ui/XUISettingActivity;->llllllIllIlIlllIIlIl:Landroid/content/Context;

    const-class v2, Lcom/diagmondm/ui/XUINetProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    const-string v1, "profileIndex"

    sget v2, Lcom/diagmondm/ui/XUISettingActivity;->llIlIIIIlIIIIIlIlIII:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 75
    sget-object v1, Lcom/diagmondm/ui/XUISettingActivity;->llIlIllllllllllllllI:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 76
    return-void
.end method

.method static synthetic llIIIIlllllIIllIIllI(I)I
    .locals 0

    .prologue
    .line 29
    sput p0, Lcom/diagmondm/ui/XUISettingActivity;->llIlIIIIlIIIIIlIlIII:I

    return p0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->IlIlIlIlIlIIlllllIlI:Landroid/app/AlertDialog;

    return-object p0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->llIIIIlllllIIllIIllI:Landroid/preference/PreferenceScreen;

    return-object p0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBNetworkProfileList;)Lcom/diagmondm/db/file/XDBNetworkProfileList;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->llIIllllIIlllIIIIlll:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    return-object p0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBProfileInfo;)Lcom/diagmondm/db/file/XDBProfileInfo;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->IIIIllIlIIlIIIIlllIl:Lcom/diagmondm/db/file/XDBProfileInfo;

    return-object p0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/diagmondm/ui/lIIIllIIIlIlIIIlllII;)Lcom/diagmondm/ui/lIIIllIIIlIlIIIlllII;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->IIIlIIllIlIIllIlllII:Lcom/diagmondm/ui/lIIIllIIIlIlIIIlllII;

    return-object p0
.end method

.method static synthetic llIIIIlllllIIllIIllI()V
    .locals 0

    .prologue
    .line 29
    invoke-static {}, Lcom/diagmondm/ui/XUISettingActivity;->lllllIlIlIIIIIIIIlIl()V

    return-void
.end method

.method static synthetic llIIIIlllllIIllIIllI([Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->IllIlIIIIlIIlIIIllIl:[Ljava/lang/String;

    return-object p0
.end method

.method static synthetic llIIllllIIlllIIIIlll()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->IllIlIIIIlIIlIIIllIl:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic llIlIIIIlIIIIIlIlIII()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/diagmondm/ui/XUISettingActivity;->llIlIIIIlIIIIIlIlIII:I

    return v0
.end method

.method static synthetic llIlIllllllllllllllI()Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->llllllIllIlIlllIIlIl:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic lllIlIlIIIllIIlIllIl(Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->lllIlIlIIIllIIlIllIl:Landroid/preference/PreferenceScreen;

    return-object p0
.end method

.method static synthetic lllIlIlIIIllIIlIllIl()V
    .locals 0

    .prologue
    .line 29
    invoke-static {}, Lcom/diagmondm/ui/XUISettingActivity;->IIlIlIIlIlIIlIlllIIl()V

    return-void
.end method

.method static synthetic llllIIIllIlIIIIllllI(Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->llllIIIllIlIIIIllllI:Landroid/preference/PreferenceScreen;

    return-object p0
.end method

.method static synthetic llllIIIllIlIIIIllllI()V
    .locals 0

    .prologue
    .line 29
    invoke-static {}, Lcom/diagmondm/ui/XUISettingActivity;->IlIlllIIlIlIIIlIlIll()V

    return-void
.end method

.method static synthetic lllllIIlIIIlIlIIIllI()Lcom/diagmondm/ui/lIIIllIIIlIlIIIlllII;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->IIIlIIllIlIIllIlllII:Lcom/diagmondm/ui/lIIIllIIIlIlIIIlllII;

    return-object v0
.end method

.method private static lllllIlIlIIIIIIIIlIl()V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/diagmondm/ui/XUISettingActivity;->llllllIllIlIlllIIlIl:Landroid/content/Context;

    const-class v2, Lcom/diagmondm/ui/XUIProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    const-string v1, "profileIndex"

    sget v2, Lcom/diagmondm/ui/XUISettingActivity;->llIlIIIIlIIIIIlIlIII:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 68
    sget-object v1, Lcom/diagmondm/ui/XUISettingActivity;->llIlIllllllllllllllI:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 69
    return-void
.end method

.method static synthetic llllllIllIlIlllIIlIl()Lcom/diagmondm/db/file/XDBNetworkProfileList;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/diagmondm/ui/XUISettingActivity;->llIIllllIIlllIIIIlll:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 223
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 225
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->llllllIllIlIlllIIlIl:Landroid/content/Context;

    .line 226
    sput-object p0, Lcom/diagmondm/ui/XUISettingActivity;->llIlIllllllllllllllI:Landroid/app/Activity;

    .line 228
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUISettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/diagmondm/ui/IIllIIllIIIlllIlIIll;

    invoke-direct {v2}, Lcom/diagmondm/ui/IIllIIllIIIlllIlIIll;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 233
    const/4 v0, 0x0

    sput-object v0, Lcom/diagmondm/ui/XUISettingActivity;->IlIlIlIlIlIIlllllIlI:Landroid/app/AlertDialog;

    .line 234
    return-void
.end method
