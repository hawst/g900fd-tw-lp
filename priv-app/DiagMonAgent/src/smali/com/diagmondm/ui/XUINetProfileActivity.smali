.class public Lcom/diagmondm/ui/XUINetProfileActivity;
.super Landroid/app/Activity;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static llIIIIlllllIIllIIllI:I

.field private static llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

.field private static lllIlIlIIIllIIlIllIl:Z

.field private static llllIIIllIlIIIIllllI:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    sput v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llllIIIllIlIIIIllllI:I

    .line 36
    sput-boolean v0, Lcom/diagmondm/ui/XUINetProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private IllIlIIIIlIIlIIIllIl()V
    .locals 3

    .prologue
    .line 57
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/diagmondm/ui/XUIProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    const-string v1, "profileIndex"

    sget v2, Lcom/diagmondm/ui/XUINetProfileActivity;->llIIIIlllllIIllIIllI:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 59
    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 60
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->finish()V

    .line 61
    return-void
.end method

.method public static llIIIIlllllIIllIIllI(I)V
    .locals 1

    .prologue
    .line 48
    if-nez p0, :cond_0

    .line 49
    const/4 v0, 0x0

    sput-boolean v0, Lcom/diagmondm/ui/XUINetProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/diagmondm/ui/XUINetProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    goto :goto_0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/diagmondm/ui/XUINetProfileActivity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->IllIlIIIIlIIlIIIllIl()V

    return-void
.end method

.method public static llIIIIlllllIIllIIllI()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lcom/diagmondm/ui/XUINetProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    return v0
.end method

.method static synthetic llIlIIIIlIIIIIlIlIII()I
    .locals 1

    .prologue
    .line 32
    sget v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llllIIIllIlIIIIllllI:I

    return v0
.end method

.method static synthetic lllIlIlIIIllIIlIllIl()Lcom/diagmondm/db/file/XDBNetworkProfileList;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    return-object v0
.end method


# virtual methods
.method protected llllIIIllIlIIIIllllI()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 351
    const-string v0, ""

    .line 352
    sget-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBInfoConRef;->CPX:Lcom/diagmondm/db/file/XDBConRefPX;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBConRefPX;->szAddr:Ljava/lang/String;

    .line 353
    const-string v1, "0.0.0.0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 355
    sget-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iput-boolean v2, v0, Lcom/diagmondm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 362
    :goto_0
    new-instance v0, Lcom/diagmondm/db/file/XDBInfoConRef;

    invoke-direct {v0}, Lcom/diagmondm/db/file/XDBInfoConRef;-><init>()V

    .line 363
    sget-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    .line 364
    invoke-static {v0}, Lcom/diagmondm/db/file/XDB;->llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBInfoConRef;)V

    .line 366
    const-string v0, "Saved"

    invoke-static {p0, v0, v2}, Lcom/diagmondm/XDMService;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;I)V

    .line 367
    return-void

    .line 359
    :cond_0
    sget-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/diagmondm/db/file/XDBInfoConRef;->bProxyUse:Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 148
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 149
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->setContentView(I)V

    .line 150
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "profileIndex"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIIIIlllllIIllIIllI:I

    .line 151
    const/4 v0, 0x1

    sput-boolean v0, Lcom/diagmondm/ui/XUINetProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    .line 152
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, LlIllIlllIIllllIlIlIl;->llllIIIllIlIIIIllllI(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    const-string v0, "Please Setting APN"

    invoke-static {p0, v0, v2}, Lcom/diagmondm/XDMService;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;I)V

    .line 155
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->finish()V

    .line 157
    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 66
    packed-switch p1, :pswitch_data_0

    .line 112
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 69
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "PROFILE EDIT"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Do you want Save and Close Network info ?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/diagmondm/ui/IlIlIlIIIIIllllIlIIl;

    invoke-direct {v1, p0}, Lcom/diagmondm/ui/IlIlIlIIIIIllllIlIIl;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060027

    new-instance v2, Lcom/diagmondm/ui/lIlIIIlllIIlIIIlIlII;

    invoke-direct {v2, p0}, Lcom/diagmondm/ui/lIlIIIlllIIlIIIlIlII;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060020

    new-instance v2, Lcom/diagmondm/ui/IllIIlllIllIIIIIllII;

    invoke-direct {v2, p0}, Lcom/diagmondm/ui/IllIIlllIllIIIIIllII;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 139
    const-string v0, "SAVE"

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 140
    const/4 v0, 0x1

    const-string v1, "Revert"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 141
    const/4 v0, 0x2

    const-string v1, "Edit Profile Info"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108003e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 142
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 372
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 382
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 375
    :pswitch_0
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->llllIIIllIlIIIIllllI()V

    .line 376
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 372
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 118
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 133
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 121
    :pswitch_0
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->llllIIIllIlIIIIllllI()V

    .line 122
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 125
    :pswitch_1
    invoke-virtual {p0}, Lcom/diagmondm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 128
    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->showDialog(I)V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onStart()V
    .locals 6

    .prologue
    const v5, 0x1090009

    const v4, 0x1090008

    const/4 v2, 0x0

    .line 162
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 164
    new-instance v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;

    invoke-direct {v0}, Lcom/diagmondm/db/file/XDBNetworkProfileList;-><init>()V

    sput-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    .line 165
    sget-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    const-string v1, "NetWork Info"

    iput-object v1, v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;->szConRefName:Ljava/lang/String;

    .line 166
    sget-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IIIIIlIlIlIllIlIIllI()Lcom/diagmondm/db/file/XDBInfoConRef;

    move-result-object v1

    iput-object v1, v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    .line 168
    sget-object v0, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iget-object v0, v0, Lcom/diagmondm/db/file/XDBInfoConRef;->CNAP:Lcom/diagmondm/db/file/XDBConRefNAP;

    iget-object v1, v0, Lcom/diagmondm/db/file/XDBConRefNAP;->szAddr:Ljava/lang/String;

    .line 169
    const v0, 0x7f080005

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 170
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    const v0, 0x7f080007

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 173
    sget-object v1, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBInfoConRef;->CNAP:Lcom/diagmondm/db/file/XDBConRefNAP;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBConRefNAP;->szNetworkProfileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 174
    new-instance v1, Lcom/diagmondm/ui/IlIlIlllIllIIIIlIlll;

    invoke-direct {v1, p0}, Lcom/diagmondm/ui/IlIlIlllIllIIIIlIlll;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 193
    const v0, 0x7f080009

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 194
    sget-object v1, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBInfoConRef;->CPX:Lcom/diagmondm/db/file/XDBConRefPX;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBConRefPX;->szAddr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 195
    new-instance v1, Lcom/diagmondm/ui/IlIIIIlllIIlllIIlIIl;

    invoke-direct {v1, p0}, Lcom/diagmondm/ui/IlIIIIlllIIlllIIlIIl;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 214
    const v0, 0x7f08000b

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 215
    sget-object v1, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBInfoConRef;->CPX:Lcom/diagmondm/db/file/XDBConRefPX;

    iget v1, v1, Lcom/diagmondm/db/file/XDBConRefPX;->nPortNbr:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 217
    new-instance v1, Lcom/diagmondm/ui/llllIIIIlllIIIlIllIl;

    invoke-direct {v1, p0}, Lcom/diagmondm/ui/llllIIIIlllIIIlIllIl;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 239
    const v0, 0x7f08000d

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 240
    sget-object v1, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBInfoConRef;->CNAP:Lcom/diagmondm/db/file/XDBConRefNAP;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBConRefNAP;->CAuth:Lcom/diagmondm/db/file/XDBConRefAuth;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBConRefAuth;->szPAP_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 241
    new-instance v1, Lcom/diagmondm/ui/lIlIlIIIIlIIlIlIIlII;

    invoke-direct {v1, p0}, Lcom/diagmondm/ui/lIlIlIIIIlIIlIlIIlII;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 261
    const v0, 0x7f08000f

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 262
    sget-object v1, Lcom/diagmondm/ui/XUINetProfileActivity;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBNetworkProfileList;->CConRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBInfoConRef;->CNAP:Lcom/diagmondm/db/file/XDBConRefNAP;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBConRefNAP;->CAuth:Lcom/diagmondm/db/file/XDBConRefAuth;

    iget-object v1, v1, Lcom/diagmondm/db/file/XDBConRefAuth;->szPAP_Secret:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 263
    new-instance v1, Lcom/diagmondm/ui/IIIIlllIIIIIlllIIlll;

    invoke-direct {v1, p0}, Lcom/diagmondm/ui/IIIIlllIIIIIlllIIlll;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 282
    const v0, 0x7f080011

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 285
    sget-object v1, Lcom/diagmondm/ui/XUIMainActivity;->llIIIIlllllIIllIIllI:LIIIIlllIIIlIlIlllIII;

    iget-object v1, v1, LIIIIlllIIIlIlIlllIII;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    const/4 v1, -0x1

    .line 297
    :goto_0
    const/high16 v3, 0x7f050000

    invoke-static {p0, v3, v4}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v3

    .line 298
    invoke-virtual {v3, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 299
    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 300
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 301
    new-instance v1, Lcom/diagmondm/ui/IIIllIIllIllIlIIlIIl;

    invoke-direct {v1, p0}, Lcom/diagmondm/ui/IIIllIIllIllIlIIlIIl;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 324
    const v0, 0x7f080013

    invoke-virtual {p0, v0}, Lcom/diagmondm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 327
    sget-object v1, Lcom/diagmondm/ui/XUIMainActivity;->llIIIIlllllIIllIIllI:LIIIIlllIIIlIlIlllIII;

    iget-object v1, v1, LIIIIlllIIIlIlIlllIII;->IllIlIIIIlIIlIIIllIl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 328
    sget-object v1, Lcom/diagmondm/ui/XUIMainActivity;->llIIIIlllllIIllIIllI:LIIIIlllIIIlIlIlllIII;

    iget-object v1, v1, LIIIIlllIIIlIlIlllIII;->IllIlIIIIlIIlIIIllIl:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 330
    :cond_0
    const v1, 0x7f050002

    invoke-static {p0, v1, v4}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    .line 331
    invoke-virtual {v1, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 332
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 333
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 334
    new-instance v1, Lcom/diagmondm/ui/lIlIIlllllIllllllllI;

    invoke-direct {v1, p0}, Lcom/diagmondm/ui/lIlIIlllllIllllllllI;-><init>(Lcom/diagmondm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 347
    return-void

    .line 289
    :cond_1
    const-string v1, "*"

    sget-object v3, Lcom/diagmondm/ui/XUIMainActivity;->llIIIIlllllIIllIIllI:LIIIIlllIIIlIlIlllIII;

    iget-object v3, v3, LIIIIlllIIIlIlIlllIII;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 290
    goto :goto_0

    .line 291
    :cond_2
    const-string v1, "default"

    sget-object v3, Lcom/diagmondm/ui/XUIMainActivity;->llIIIIlllllIIllIIllI:LIIIIlllIIIlIlIlllIII;

    iget-object v3, v3, LIIIIlllIIIlIlIlllIII;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 292
    const/4 v1, 0x1

    goto :goto_0

    .line 294
    :cond_3
    const/4 v1, 0x2

    goto :goto_0
.end method
