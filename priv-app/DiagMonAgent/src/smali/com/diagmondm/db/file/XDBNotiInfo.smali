.class public Lcom/diagmondm/db/file/XDBNotiInfo;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/diagmondm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public bDevUiMode:Z

.field public bDeviceInit:Z

.field public nAppId:I

.field public nRowId:I

.field public nUiMode:I

.field public szDevFotaErr:Ljava/lang/String;

.field public szDevServiceId:Ljava/lang/String;

.field public szDevWorkId:Ljava/lang/String;

.field public szOpmode:Ljava/lang/String;

.field public szPushJobId:Ljava/lang/String;

.field public szServerId:Ljava/lang/String;

.field public szServiceId:Ljava/lang/String;

.field public szSessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->nRowId:I

    .line 29
    iput v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->nAppId:I

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->nUiMode:I

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szSessionId:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szServerId:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szOpmode:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szPushJobId:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szServiceId:Ljava/lang/String;

    .line 36
    iput-boolean v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDeviceInit:Z

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevServiceId:Ljava/lang/String;

    .line 38
    iput-boolean v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDevUiMode:Z

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevFotaErr:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevWorkId:Ljava/lang/String;

    .line 41
    return-void
.end method
