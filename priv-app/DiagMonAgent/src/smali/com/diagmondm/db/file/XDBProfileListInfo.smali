.class public Lcom/diagmondm/db/file/XDBProfileListInfo;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/diagmondm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public CUICResultKeep:Lcom/diagmondm/db/file/XDBUICResultKeepInfo;

.field public CnotiResumeState:Lcom/diagmondm/db/file/XDBSessionSaveInfo;

.field public nNotiEvent:I

.field public nProfileindex:I

.field public szOpmode:Ljava/lang/String;

.field public szProfileName:[Ljava/lang/String;

.field public szPushJobID:Ljava/lang/String;

.field public szServiceID:Ljava/lang/String;

.field public szSessionID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/diagmondm/db/file/XDBProfileListInfo;->nProfileindex:I

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileListInfo;->szProfileName:[Ljava/lang/String;

    .line 26
    new-instance v0, Lcom/diagmondm/db/file/XDBSessionSaveInfo;

    invoke-direct {v0}, Lcom/diagmondm/db/file/XDBSessionSaveInfo;-><init>()V

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileListInfo;->CnotiResumeState:Lcom/diagmondm/db/file/XDBSessionSaveInfo;

    .line 27
    new-instance v0, Lcom/diagmondm/db/file/XDBUICResultKeepInfo;

    invoke-direct {v0}, Lcom/diagmondm/db/file/XDBUICResultKeepInfo;-><init>()V

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileListInfo;->CUICResultKeep:Lcom/diagmondm/db/file/XDBUICResultKeepInfo;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileListInfo;->szOpmode:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileListInfo;->szPushJobID:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileListInfo;->szServiceID:Ljava/lang/String;

    .line 31
    return-void
.end method
