.class public Lcom/diagmondm/db/file/XDBProfileInfo;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public CconRef:Lcom/diagmondm/db/file/XDBInfoConRef;

.field public bChangedProtocol:Z

.field public nAuthType:I

.field public nClientNonceFormat:I

.field public nServerAuthType:I

.field public nServerNonceFormat:I

.field public nServerPort:I

.field public nServerPort_Org:I

.field public szAppID:Ljava/lang/String;

.field public szAuthLevel:Ljava/lang/String;

.field public szClientNonce:Ljava/lang/String;

.field public szPassword:Ljava/lang/String;

.field public szPath:Ljava/lang/String;

.field public szPath_Org:Ljava/lang/String;

.field public szProfileName:Ljava/lang/String;

.field public szProtocol:Ljava/lang/String;

.field public szProtocol_Org:Ljava/lang/String;

.field public szServerAuthLevel:Ljava/lang/String;

.field public szServerID:Ljava/lang/String;

.field public szServerIP:Ljava/lang/String;

.field public szServerIP_Org:Ljava/lang/String;

.field public szServerNonce:Ljava/lang/String;

.field public szServerPwd:Ljava/lang/String;

.field public szServerUrl:Ljava/lang/String;

.field public szServerUrl_Org:Ljava/lang/String;

.field public szUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szProtocol:Ljava/lang/String;

    .line 44
    const/16 v0, 0x50

    iput v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->nServerPort:I

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szServerUrl:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szServerIP:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szPath:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szProtocol_Org:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szServerUrl_Org:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szServerIP_Org:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szPath_Org:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->nAuthType:I

    .line 54
    iput v1, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->nServerAuthType:I

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szAppID:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szAuthLevel:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szServerAuthLevel:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szUserName:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szPassword:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szServerID:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szServerPwd:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szClientNonce:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szServerNonce:Ljava/lang/String;

    .line 66
    iput v1, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->nServerNonceFormat:I

    .line 67
    iput v1, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->nClientNonceFormat:I

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->szProfileName:Ljava/lang/String;

    .line 71
    new-instance v0, Lcom/diagmondm/db/file/XDBInfoConRef;

    invoke-direct {v0}, Lcom/diagmondm/db/file/XDBInfoConRef;-><init>()V

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBProfileInfo;->CconRef:Lcom/diagmondm/db/file/XDBInfoConRef;

    .line 72
    return-void
.end method
