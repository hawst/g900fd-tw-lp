.class public Lcom/diagmondm/db/file/XDBDiagMon;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/diagmondm/interfaces/XDMInterface;
.implements Lcom/diagmondm/interfaces/llIIIIlllllIIllIIllI;
.implements Ljava/io/Serializable;


# static fields
.field static final llIIIIlllllIIllIIllI:Landroid/net/Uri;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "content://com.wssyncmldm.ContentProvider/fumo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI:Landroid/net/Uri;

    return-void
.end method

.method public static IllIlIIIIlIIlIIIllIl()Ljava/lang/String;
    .locals 5

    .prologue
    .line 224
    const-string v1, ""

    .line 228
    :try_start_0
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;

    move-result-object v0

    .line 229
    iget-object v0, v0, Lcom/diagmondm/db/file/XDBDiagMonInfo;->szCurrentVersion:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :try_start_1
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "szCurrentVersion : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 237
    :goto_0
    return-object v0

    .line 232
    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 234
    :goto_1
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0

    .line 232
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;
    .locals 4

    .prologue
    .line 24
    new-instance v0, Lcom/diagmondm/db/file/XDBDiagMonInfo;

    invoke-direct {v0}, Lcom/diagmondm/db/file/XDBDiagMonInfo;-><init>()V

    .line 28
    const-wide/16 v2, 0x1

    :try_start_0
    invoke-static {v2, v3, v0}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(JLcom/diagmondm/db/file/XDBDiagMonInfo;)V
    :try_end_0
    .catch Lcom/diagmondm/db/file/XDBUserDBException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 42
    :cond_0
    :goto_0
    return-object v0

    .line 30
    :catch_0
    move-exception v1

    .line 32
    if-eqz v1, :cond_0

    .line 34
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v1}, Lcom/diagmondm/db/file/XDBUserDBException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 35
    invoke-virtual {v1}, Lcom/diagmondm/db/file/XDBUserDBException;->llIIIIlllllIIllIIllI()V

    goto :goto_0

    .line 38
    :catch_1
    move-exception v1

    .line 40
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(I)V
    .locals 3

    .prologue
    .line 177
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nPushRegiState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 180
    :try_start_0
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;

    move-result-object v0

    .line 181
    iput p0, v0, Lcom/diagmondm/db/file/XDBDiagMonInfo;->nPushRegiState:I

    .line 182
    invoke-static {v0}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBDiagMonInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    return-void

    .line 184
    :catch_0
    move-exception v0

    .line 186
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBDiagMonInfo;)V
    .locals 3

    .prologue
    .line 49
    const-wide/16 v0, 0x1

    :try_start_0
    invoke-static {v0, v1, p0}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(JLcom/diagmondm/db/file/XDBDiagMonInfo;)V
    :try_end_0
    .catch Lcom/diagmondm/db/file/XDBUserDBException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 53
    if-eqz v0, :cond_0

    .line 55
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Lcom/diagmondm/db/file/XDBUserDBException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 56
    invoke-virtual {v0}, Lcom/diagmondm/db/file/XDBUserDBException;->llIIIIlllllIIllIIllI()V

    goto :goto_0

    .line 59
    :catch_1
    move-exception v0

    .line 61
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 84
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "szActiveFunction : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 87
    :try_start_0
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;

    move-result-object v0

    .line 88
    iput-object p0, v0, Lcom/diagmondm/db/file/XDBDiagMonInfo;->szActiveFunction:Ljava/lang/String;

    .line 89
    invoke-static {v0}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBDiagMonInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 93
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIlIIIIlIIIIIlIlIII()Ljava/lang/String;
    .locals 3

    .prologue
    .line 192
    const-string v0, ""

    .line 196
    :try_start_0
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;

    move-result-object v1

    .line 197
    iget-object v0, v1, Lcom/diagmondm/db/file/XDBDiagMonInfo;->szServerNonce:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    return-object v0

    .line 199
    :catch_0
    move-exception v1

    .line 201
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static lllIlIlIIIllIIlIllIl()I
    .locals 3

    .prologue
    .line 160
    const/4 v0, 0x0

    .line 164
    :try_start_0
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;

    move-result-object v1

    .line 165
    iget v0, v1, Lcom/diagmondm/db/file/XDBDiagMonInfo;->nPushRegiState:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_0
    return v0

    .line 167
    :catch_0
    move-exception v1

    .line 169
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 242
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "szCurrentVersion : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 245
    :try_start_0
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;

    move-result-object v0

    .line 246
    iput-object p0, v0, Lcom/diagmondm/db/file/XDBDiagMonInfo;->szCurrentVersion:Ljava/lang/String;

    .line 247
    invoke-static {v0}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBDiagMonInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_0
    return-void

    .line 249
    :catch_0
    move-exception v0

    .line 251
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI()Ljava/lang/String;
    .locals 3

    .prologue
    .line 67
    const-string v0, ""

    .line 71
    :try_start_0
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;

    move-result-object v1

    .line 72
    iget-object v0, v1, Lcom/diagmondm/db/file/XDBDiagMonInfo;->szActiveFunction:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-object v0

    .line 74
    :catch_0
    move-exception v1

    .line 76
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 209
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "szServerNonce : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 212
    :try_start_0
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI()Lcom/diagmondm/db/file/XDBDiagMonInfo;

    move-result-object v0

    .line 213
    iput-object p0, v0, Lcom/diagmondm/db/file/XDBDiagMonInfo;->szServerNonce:Ljava/lang/String;

    .line 214
    invoke-static {v0}, Lcom/diagmondm/db/file/XDBDiagMon;->llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBDiagMonInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :goto_0
    return-void

    .line 216
    :catch_0
    move-exception v0

    .line 218
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method
