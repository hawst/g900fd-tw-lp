.class public Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/diagmondm/interfaces/XDMInterface;
.implements Lcom/diagmondm/interfaces/llIIIIlllllIIllIIllI;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public nRangeDetail:I

.field public nStatus:I

.field public nUiMode:I

.field public szCorrelator:Ljava/lang/String;

.field public szDiagDataUrl:Ljava/lang/String;

.field public szFotaErr:Ljava/lang/String;

.field public szFunctionId:Ljava/lang/String;

.field public szLogFilePath:Ljava/lang/String;

.field public szPackagingErr:Ljava/lang/String;

.field public szProcessingID:Ljava/lang/String;

.field public szResultCode:Ljava/lang/String;

.field public szResultReportSourceUri:Ljava/lang/String;

.field public szResultReportTargetUri:Ljava/lang/String;

.field public szServiceID:Ljava/lang/String;

.field public szUploadServerIp:Ljava/lang/String;

.field public szUploadServerPort:Ljava/lang/String;

.field public szUploadServerProtocol:Ljava/lang/String;

.field public szUploadServerUrl:Ljava/lang/String;

.field public szWorkID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szFunctionId:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szProcessingID:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szDiagDataUrl:Ljava/lang/String;

    .line 44
    iput v1, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->nStatus:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szResultReportSourceUri:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szResultReportTargetUri:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szResultCode:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szUploadServerProtocol:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szUploadServerUrl:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szUploadServerIp:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szUploadServerPort:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szWorkID:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szCorrelator:Ljava/lang/String;

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->nRangeDetail:I

    .line 60
    iput v1, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->nUiMode:I

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szServiceID:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szPackagingErr:Ljava/lang/String;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szFotaErr:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/diagmondm/db/file/XDBDiagMonFunctionInfo;->szLogFilePath:Ljava/lang/String;

    .line 65
    return-void
.end method
