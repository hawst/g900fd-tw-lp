.class public Lcom/diagmondm/eng/core/llllIIIllIlIIIIllllI;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/diagmondm/interfaces/XDMInterface;


# direct methods
.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 38
    const-string v0, "syncml:auth-basic"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 39
    const/4 v0, 0x0

    .line 45
    :goto_0
    return v0

    .line 40
    :cond_0
    const-string v0, "syncml:auth-md5"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 41
    const/4 v0, 0x1

    goto :goto_0

    .line 42
    :cond_1
    const-string v0, "syncml:auth-MAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 43
    const/4 v0, 0x2

    goto :goto_0

    .line 45
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    packed-switch p0, :pswitch_data_0

    .line 31
    const-string v0, "Not Support Auth Type"

    invoke-static {v0}, LIIllIlIIIIlIIIllIllI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 25
    :pswitch_0
    const-string v0, "syncml:auth-basic"

    goto :goto_0

    .line 27
    :pswitch_1
    const-string v0, "syncml:auth-md5"

    goto :goto_0

    .line 29
    :pswitch_2
    const-string v0, "syncml:auth-MAC"

    goto :goto_0

    .line 22
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static llIIIIlllllIIllIIllI(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 77
    .line 80
    const/16 v1, 0x10

    new-array v1, v1, [B

    .line 82
    packed-switch p0, :pswitch_data_0

    .line 114
    const-string v1, "Not Support Auth Type"

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 194
    :cond_0
    :goto_0
    return-object v0

    .line 86
    :pswitch_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 88
    :cond_1
    const-string v1, "userName or passWord is NULL"

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :pswitch_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p3, :cond_2

    if-gtz p4, :cond_4

    .line 97
    :cond_2
    const-string v1, "userName or passWord or nonce or nonceLength is NULL"

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :pswitch_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz p3, :cond_3

    if-lez p4, :cond_3

    if-eqz p5, :cond_3

    if-gtz p6, :cond_4

    .line 107
    :cond_3
    const-string v1, "userName or passWord or nonce or nonceLength or packetBody or bodyLength is NULL"

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_4
    packed-switch p0, :pswitch_data_1

    goto :goto_0

    .line 124
    :pswitch_3
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 126
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/diagmondm/eng/core/lllIlIlIIIllIIlIllIl;->llIIIIlllllIIllIIllI([B)[B

    move-result-object v2

    .line 127
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "XDM_CRED_TYPE_BASIC cred:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 135
    :pswitch_4
    new-instance v0, Lcom/diagmondm/eng/core/llIlIllllllllllllllI;

    invoke-direct {v0}, Lcom/diagmondm/eng/core/llIlIllllllllllllllI;-><init>()V

    .line 136
    invoke-virtual {v0, p1, p2, p3}, Lcom/diagmondm/eng/core/llIlIllllllllllllllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v1

    .line 137
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 139
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v1, p3, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 140
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "XDM_CRED_TYPE_MD5 nonce= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ret= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 150
    :pswitch_5
    :try_start_0
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 158
    :goto_1
    if-eqz v1, :cond_0

    .line 162
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 166
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 167
    invoke-static {v0}, Lcom/diagmondm/eng/core/lllIlIlIIIllIIlIllIl;->llIIIIlllllIIllIIllI([B)[B

    move-result-object v0

    .line 169
    new-instance v2, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 170
    invoke-virtual {v1, p5}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 171
    invoke-static {v0}, Lcom/diagmondm/eng/core/lllIlIlIIIllIIlIllIl;->llIIIIlllllIIllIIllI([B)[B

    move-result-object v0

    .line 172
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 174
    const-string v0, ":"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    new-instance v2, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v2, p3, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 179
    const/4 v0, 0x2

    if-ne p0, v0, :cond_5

    .line 181
    invoke-static {v1}, Lcom/diagmondm/eng/core/lllIlIlIIIllIIlIllIl;->llIIIIlllllIIllIIllI([B)[B

    move-result-object v1

    .line 182
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto/16 :goto_0

    .line 152
    :catch_0
    move-exception v1

    .line 154
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_1

    .line 187
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto/16 :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 119
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 273
    .line 278
    new-instance v1, Lcom/diagmondm/eng/core/llIlIllllllllllllllI;

    invoke-direct {v1}, Lcom/diagmondm/eng/core/llIlIllllllllllllllI;-><init>()V

    .line 284
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Nonce="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LIIllIlIIIIlIIIllIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 285
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "path="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IIlIlIIIlIIlIlIIIIII()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LIIllIlIIIIlIIIllIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 286
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "query="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LIIllIlIIIIlIIIllIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 287
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "szBody="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LIIllIlIIIIlIIIllIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 289
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->llIIlIlIlIlIIIIIIlIl()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/diagmondm/db/file/XDB;->lIllIllllIIIlllIlllI()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, LIIlIlIIlIlIIlIlllIIl;->llllIIIllIlIIIIllllI()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/diagmondm/db/file/XDB;->llllIlIIIllllIIlIlll()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/diagmondm/eng/core/llIlIllllllllllllllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    .line 290
    if-nez v2, :cond_0

    .line 292
    const-string v1, "md5digest1 is null"

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 316
    :goto_0
    return-object v0

    .line 295
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 298
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->IIlIlIIIlIIlIlIIIIII()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2, p3}, Lcom/diagmondm/eng/core/llIlIllllllllllllllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    .line 299
    if-nez v2, :cond_1

    .line 301
    const-string v1, "md5digest2 is null"

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :cond_1
    new-instance v4, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-direct {v4, v2, v5}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 307
    invoke-virtual {v1, v3, p0, p1, v4}, Lcom/diagmondm/eng/core/llIlIllllllllllllllI;->llllIIIllIlIIIIllllI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    .line 308
    if-nez v1, :cond_2

    .line 310
    const-string v1, "md5digest3 is null"

    invoke-static {v1}, LIIllIlIIIIlIIIllIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    goto :goto_0

    .line 313
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 65
    const-string v0, "BASIC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    .line 67
    :cond_0
    const-string v0, "MD5"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 68
    const/4 v0, 0x1

    goto :goto_0

    .line 69
    :cond_1
    const-string v0, "HMAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 70
    const/4 v0, 0x2

    goto :goto_0

    .line 72
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    packed-switch p0, :pswitch_data_0

    .line 59
    const-string v0, "NONE"

    :goto_0
    return-object v0

    .line 53
    :pswitch_0
    const-string v0, "BASIC"

    goto :goto_0

    .line 55
    :pswitch_1
    const-string v0, "MD5"

    goto :goto_0

    .line 57
    :pswitch_2
    const-string v0, "HMAC"

    goto :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
