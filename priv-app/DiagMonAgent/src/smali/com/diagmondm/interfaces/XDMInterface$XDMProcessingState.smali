.class public final enum Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;
.super Ljava/lang/Enum;
.source "llIIIIlllllIIllIIllI"


# static fields
.field public static final enum IIIIllIlIIlIIIIlllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum IIIlIIllIlIIllIlllII:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum IlIlIlIlIlIIlllllIlI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum IllIlIIIIlIIlIIIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llIIIIlllllIIllIIllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llIIllllIIlllIIIIlll:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llIlIllllllllllllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum lllIlIlIIIllIIlIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llllIIIllIlIIIIllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field private static final synthetic lllllIIlIIIlIlIIIllI:[Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llllllIllIlIlllIIlIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 118
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_NONE"

    invoke-direct {v0, v1, v3}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llIIIIlllllIIllIIllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 119
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_SYNCHDR"

    invoke-direct {v0, v1, v4}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llllIIIllIlIIIIllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 120
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_ALERT"

    invoke-direct {v0, v1, v5}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->lllIlIlIIIllIIlIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 121
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_STATUS"

    invoke-direct {v0, v1, v6}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 122
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_RESULTS"

    invoke-direct {v0, v1, v7}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->IllIlIIIIlIIlIIIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 123
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_GET"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llIIllllIIlllIIIIlll:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 124
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_EXEC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->IIIIllIlIIlIIIIlllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 125
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_ADD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llllllIllIlIlllIIlIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 126
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_REPLACE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llIlIllllllllllllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 127
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_DELETE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->IIIlIIllIlIIllIlllII:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 128
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_COPY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->IlIlIlIlIlIIlllllIlI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    .line 116
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    sget-object v1, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llIIIIlllllIIllIIllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llllIIIllIlIIIIllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->lllIlIlIIIllIIlIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llIlIIIIlIIIIIlIlIII:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->IllIlIIIIlIIlIIIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llIIllllIIlllIIIIlll:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->IIIIllIlIIlIIIIlllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llllllIllIlIlllIIlIl:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->llIlIllllllllllllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->IIIlIIllIlIIllIlllII:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->IlIlIlIlIlIIlllllIlI:Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->lllllIIlIIIlIlIIIllI:[Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;
    .locals 1

    .prologue
    .line 116
    const-class v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    return-object v0
.end method

.method public static values()[Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->lllllIIlIIIlIlIIIllI:[Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    invoke-virtual {v0}, [Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/diagmondm/interfaces/XDMInterface$XDMProcessingState;

    return-object v0
.end method
