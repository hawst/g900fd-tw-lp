.class public final enum Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;
.super Ljava/lang/Enum;
.source "llIIIIlllllIIllIIllI"


# static fields
.field public static final enum llIIIIlllllIIllIIllI:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

.field private static final synthetic llIlIIIIlIIIIIlIlIII:[Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

.field public static final enum lllIlIlIIIllIIlIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

.field public static final enum llllIIIllIlIIIIllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 133
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    const-string v1, "XDM_ATOMIC_NONE"

    invoke-direct {v0, v1, v2}, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->llIIIIlllllIIllIIllI:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    .line 134
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    const-string v1, "XDM_ATOMIC_STEP_ROLLBACK"

    invoke-direct {v0, v1, v3}, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->llllIIIllIlIIIIllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    .line 135
    new-instance v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    const-string v1, "XDM_ATOMIC_STEP_NOT_EXEC"

    invoke-direct {v0, v1, v4}, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->lllIlIlIIIllIIlIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    .line 131
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v1, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->llIIIIlllllIIllIIllI:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    aput-object v1, v0, v2

    sget-object v1, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->llllIIIllIlIIIIllllI:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    aput-object v1, v0, v3

    sget-object v1, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->lllIlIlIIIllIIlIllIl:Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    aput-object v1, v0, v4

    sput-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->llIlIIIIlIIIIIlIlIII:[Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;
    .locals 1

    .prologue
    .line 131
    const-class v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    return-object v0
.end method

.method public static values()[Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->llIlIIIIlIIIIIlIlIII:[Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    invoke-virtual {v0}, [Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/diagmondm/interfaces/XDMInterface$XDMAtomicStep;

    return-object v0
.end method
