.class public final enum Lcom/sec/android/diagmonagent/registration/state/State;
.super Ljava/lang/Enum;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;


# static fields
.field private static final synthetic IIIIIIlIIIllllllIlII:[Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum IIIIllIlIIlIIIIlllIl:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum IIIlIIllIlIIllIlllII:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum IlIlIlIlIlIIlllllIlI:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum IllIlIIIIlIIlIIIllIl:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum llIIllllIIlllIIIIlll:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum llIlIIIIlIIIIIlIlIII:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum llIlIllllllllllllllI:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum lllIlIlIIIllIIlIllIl:Lcom/sec/android/diagmonagent/registration/state/State;

.field public static final enum llllIIIllIlIIIIllllI:Lcom/sec/android/diagmonagent/registration/state/State;

.field private static lllllIIlIIIlIlIIIllI:Ljava/util/EnumMap;

.field public static final enum llllllIllIlIlllIIlIl:Lcom/sec/android/diagmonagent/registration/state/State;


# instance fields
.field public runnable:Lcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 9
    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "START"

    new-instance v3, Lcom/sec/android/diagmonagent/registration/state/IlIlllIIlIlIIIlIlIll;

    invoke-direct {v3}, Lcom/sec/android/diagmonagent/registration/state/IlIlllIIlIlIIIlIlIll;-><init>()V

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/registration/state/State;

    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "REGISTER_DEVICE"

    new-instance v3, Lcom/sec/android/diagmonagent/registration/state/llIlIIIIlIIIIIlIlIII;

    invoke-direct {v3}, Lcom/sec/android/diagmonagent/registration/state/llIlIIIIlIIIIIlIlIII;-><init>()V

    invoke-direct {v1, v2, v6, v3}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->llllIIIllIlIIIIllllI:Lcom/sec/android/diagmonagent/registration/state/State;

    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "REGISTER_DEVICE_FAIL"

    new-instance v3, Lcom/sec/android/diagmonagent/registration/state/llIIllllIIlllIIIIlll;

    invoke-direct {v3}, Lcom/sec/android/diagmonagent/registration/state/llIIllllIIlllIIIIlll;-><init>()V

    invoke-direct {v1, v2, v7, v3}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/diagmonagent/registration/state/State;

    .line 10
    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "REGISTER_DEVICE_SUCCESS"

    new-instance v3, Lcom/sec/android/diagmonagent/registration/state/IIIIllIlIIlIIIIlllIl;

    invoke-direct {v3}, Lcom/sec/android/diagmonagent/registration/state/IIIIllIlIIlIIIIlllIl;-><init>()V

    invoke-direct {v1, v2, v8, v3}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->llIlIIIIlIIIIIlIlIII:Lcom/sec/android/diagmonagent/registration/state/State;

    .line 11
    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "REGISTER_DEVICE_ALREADY_REGISTERED"

    new-instance v3, Lcom/sec/android/diagmonagent/registration/state/IllIlIIIIlIIlIIIllIl;

    invoke-direct {v3}, Lcom/sec/android/diagmonagent/registration/state/IllIlIIIIlIIlIIIllIl;-><init>()V

    invoke-direct {v1, v2, v9, v3}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->IllIlIIIIlIIlIIIllIl:Lcom/sec/android/diagmonagent/registration/state/State;

    .line 12
    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "SEND_DEVICE_INIT"

    const/4 v3, 0x5

    new-instance v4, Lcom/sec/android/diagmonagent/registration/state/IIIIIIlIIIllllllIlII;

    invoke-direct {v4}, Lcom/sec/android/diagmonagent/registration/state/IIIIIIlIIIllllllIlII;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->llIIllllIIlllIIIIlll:Lcom/sec/android/diagmonagent/registration/state/State;

    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "REGISTER_PUSH"

    const/4 v3, 0x6

    new-instance v4, Lcom/sec/android/diagmonagent/registration/state/llllllIllIlIlllIIlIl;

    invoke-direct {v4}, Lcom/sec/android/diagmonagent/registration/state/llllllIllIlIlllIIlIl;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->IIIIllIlIIlIIIIlllIl:Lcom/sec/android/diagmonagent/registration/state/State;

    .line 13
    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "REGISTER_PUSH_FAIL"

    const/4 v3, 0x7

    new-instance v4, Lcom/sec/android/diagmonagent/registration/state/IIIlIIllIlIIllIlllII;

    invoke-direct {v4}, Lcom/sec/android/diagmonagent/registration/state/IIIlIIllIlIIllIlllII;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->llllllIllIlIlllIIlIl:Lcom/sec/android/diagmonagent/registration/state/State;

    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "REGISTER_PUSH_SUCCESS"

    const/16 v3, 0x8

    new-instance v4, Lcom/sec/android/diagmonagent/registration/state/IlIlIlIlIlIIlllllIlI;

    invoke-direct {v4}, Lcom/sec/android/diagmonagent/registration/state/IlIlIlIlIlIIlllllIlI;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->llIlIllllllllllllllI:Lcom/sec/android/diagmonagent/registration/state/State;

    .line 14
    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "REGISTER_PUSH_ALREADY_REGISTERED"

    const/16 v3, 0x9

    new-instance v4, Lcom/sec/android/diagmonagent/registration/state/llIlIllllllllllllllI;

    invoke-direct {v4}, Lcom/sec/android/diagmonagent/registration/state/llIlIllllllllllllllI;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->IIIlIIllIlIIllIlllII:Lcom/sec/android/diagmonagent/registration/state/State;

    .line 15
    new-instance v1, Lcom/sec/android/diagmonagent/registration/state/State;

    const-string v2, "FINISH"

    const/16 v3, 0xa

    new-instance v4, Lcom/sec/android/diagmonagent/registration/state/lllIlIlIIIllIIlIllIl;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/sec/android/diagmonagent/registration/state/lllIlIlIIIllIIlIllIl;-><init>(Lcom/sec/android/diagmonagent/registration/state/llllIIIllIlIIIIllllI;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/diagmonagent/registration/state/State;-><init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->IlIlIlIlIlIIlllllIlI:Lcom/sec/android/diagmonagent/registration/state/State;

    .line 8
    const/16 v1, 0xb

    new-array v1, v1, [Lcom/sec/android/diagmonagent/registration/state/State;

    sget-object v2, Lcom/sec/android/diagmonagent/registration/state/State;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v2, v1, v0

    sget-object v2, Lcom/sec/android/diagmonagent/registration/state/State;->llllIIIllIlIIIIllllI:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v2, v1, v6

    sget-object v2, Lcom/sec/android/diagmonagent/registration/state/State;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v2, v1, v7

    sget-object v2, Lcom/sec/android/diagmonagent/registration/state/State;->llIlIIIIlIIIIIlIlIII:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v2, v1, v8

    sget-object v2, Lcom/sec/android/diagmonagent/registration/state/State;->IllIlIIIIlIIlIIIllIl:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v2, v1, v9

    const/4 v2, 0x5

    sget-object v3, Lcom/sec/android/diagmonagent/registration/state/State;->llIIllllIIlllIIIIlll:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/sec/android/diagmonagent/registration/state/State;->IIIIllIlIIlIIIIlllIl:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/sec/android/diagmonagent/registration/state/State;->llllllIllIlIlllIIlIl:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/sec/android/diagmonagent/registration/state/State;->llIlIllllllllllllllI:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/sec/android/diagmonagent/registration/state/State;->IIIlIIllIlIIllIlllII:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/sec/android/diagmonagent/registration/state/State;->IlIlIlIlIlIIlllllIlI:Lcom/sec/android/diagmonagent/registration/state/State;

    aput-object v3, v1, v2

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->IIIIIIlIIIllllllIlII:[Lcom/sec/android/diagmonagent/registration/state/State;

    .line 24
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, Lcom/sec/android/diagmonagent/registration/state/State;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/sec/android/diagmonagent/registration/state/State;->lllllIIlIIIlIlIIIllI:Ljava/util/EnumMap;

    .line 28
    invoke-static {}, Lcom/sec/android/diagmonagent/registration/state/State;->values()[Lcom/sec/android/diagmonagent/registration/state/State;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 29
    sget-object v4, Lcom/sec/android/diagmonagent/registration/state/State;->lllllIIlIIIlIlIIIllI:Ljava/util/EnumMap;

    iget-object v5, v3, Lcom/sec/android/diagmonagent/registration/state/State;->runnable:Lcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;

    invoke-virtual {v4, v3, v5}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput-object p3, p0, Lcom/sec/android/diagmonagent/registration/state/State;->runnable:Lcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;

    .line 41
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/diagmonagent/registration/state/State;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/android/diagmonagent/registration/state/State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/diagmonagent/registration/state/State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/diagmonagent/registration/state/State;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/android/diagmonagent/registration/state/State;->IIIIIIlIIIllllllIlII:[Lcom/sec/android/diagmonagent/registration/state/State;

    invoke-virtual {v0}, [Lcom/sec/android/diagmonagent/registration/state/State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/diagmonagent/registration/state/State;

    return-object v0
.end method


# virtual methods
.method public llIIIIlllllIIllIIllI(LIlIlIlIllIlllIllllll;)Lcom/sec/android/diagmonagent/registration/state/State;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/diagmonagent/registration/state/State;->runnable:Lcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;

    invoke-interface {v0, p1}, Lcom/sec/android/diagmonagent/registration/state/lllllIIlIIIlIlIIIllI;->llIIIIlllllIIllIIllI(LIlIlIlIllIlllIllllll;)Lcom/sec/android/diagmonagent/registration/state/State;

    move-result-object v0

    return-object v0
.end method
