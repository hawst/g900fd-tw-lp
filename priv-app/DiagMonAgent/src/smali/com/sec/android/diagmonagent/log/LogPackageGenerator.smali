.class public Lcom/sec/android/diagmonagent/log/LogPackageGenerator;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field private llIIIIlllllIIllIIllI:Landroid/content/Context;

.field private lllIlIlIIIllIIlIllIl:Ljava/lang/String;

.field private llllIIIllIlIIIIllllI:Ljava/util/zip/ZipOutputStream;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->lllIlIlIIIllIIlIllIl:Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 36
    invoke-virtual {p0, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->lllIlIlIIIllIIlIllIl:Ljava/lang/String;

    .line 37
    invoke-virtual {p0, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llllIIIllIlIIIIllllI(Ljava/io/File;)Ljava/util/zip/ZipOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llllIIIllIlIIIIllllI:Ljava/util/zip/ZipOutputStream;

    .line 38
    return-void
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 104
    :try_start_0
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;

    invoke-direct {v1, p0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 106
    :try_start_1
    invoke-virtual {v1, p1}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v1}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 116
    :try_start_2
    invoke-virtual {v1}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llllIIIllIlIIIIllllI()V
    :try_end_2
    .catch Ljava/util/zip/ZipException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    .line 127
    return-object v0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    :try_start_3
    const-string v1, "No entries"

    invoke-virtual {v0}, Ljava/util/zip/ZipException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "501"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2

    .line 131
    :catch_1
    move-exception v0

    .line 132
    const-string v1, "write failed: ENOSPC (No space left on device)"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 133
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "572"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 121
    :cond_0
    :try_start_4
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "502"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2

    .line 137
    :catch_2
    move-exception v0

    .line 138
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "571"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 123
    :catch_3
    move-exception v0

    .line 124
    :try_start_5
    const-string v1, "write failed: ENOSPC (No space left on device)"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "572"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 127
    :cond_1
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "502"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2

    .line 108
    :catch_4
    move-exception v0

    .line 109
    :try_start_6
    const-string v2, "write failed: ENOSPC (No space left on device)"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 110
    new-instance v2, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v3, "572"

    invoke-direct {v2, v3, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 115
    :catchall_0
    move-exception v0

    .line 116
    :try_start_7
    invoke-virtual {v1}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llllIIIllIlIIIIllllI()V
    :try_end_7
    .catch Ljava/util/zip/ZipException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2

    .line 127
    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_2

    .line 112
    :cond_2
    :try_start_9
    new-instance v2, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v3, "502"

    invoke-direct {v2, v3, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 117
    :catch_5
    move-exception v0

    .line 118
    :try_start_a
    const-string v1, "No entries"

    invoke-virtual {v0}, Ljava/util/zip/ZipException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 119
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "501"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 121
    :cond_3
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "502"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 123
    :catch_6
    move-exception v0

    .line 124
    const-string v1, "write failed: ENOSPC (No space left on device)"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 125
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "572"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 127
    :cond_4
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "502"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_2

    .line 135
    :cond_5
    new-instance v1, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;

    const-string v2, "502"

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator$LogPackageGeneratorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected llIIIIlllllIIllIIllI(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 49
    const-string v0, "log_"

    const-string v1, ".zip"

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public llIIIIlllllIIllIIllI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->lllIlIlIIIllIIlIllIl:Ljava/lang/String;

    return-object v0
.end method

.method protected llIIIIlllllIIllIIllI(Ljava/io/File;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public llIIIIlllllIIllIIllI(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-static {v0, p1}, LllIllIIllIlIIlIlIlIl;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI([Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public llIIIIlllllIIllIIllI(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 58
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/diagmonagent/log/llllIIIllIlIIIIllllI;

    .line 60
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llllIIIllIlIIIIllllI:Ljava/util/zip/ZipOutputStream;

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/diagmonagent/log/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/util/zip/ZipOutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 61
    :catch_0
    move-exception v1

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File not found: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/android/diagmonagent/log/llllIIIllIlIIIIllllI;->llllIIIllIlIIIIllllI:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/diagmonagent/log/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :catch_1
    move-exception v1

    .line 64
    invoke-static {v1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/SecurityException;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "User not agreed: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/android/diagmonagent/log/llllIIIllIlIIIIllllI;->llllIIIllIlIIIIllllI:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/diagmonagent/log/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_0

    .line 67
    :cond_0
    invoke-static {v1}, Lcom/sec/android/diagmonagent/log/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 68
    :catch_2
    move-exception v0

    .line 69
    invoke-static {v0}, Lcom/sec/android/diagmonagent/log/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 72
    :cond_1
    return-void
.end method

.method public llIIIIlllllIIllIIllI([Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 75
    const-class v0, Lcom/sec/android/diagmonagent/log/llllIIIllIlIIIIllllI;

    iget-object v1, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-static {v1, p1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/diagmonagent/log/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(Ljava/lang/Class;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v0

    .line 76
    const-class v1, Lcom/sec/android/diagmonagent/log/llIlIIIIlIIIIIlIlIII;

    iget-object v2, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-static {v2, p1}, LllIllIIllIlIIlIlIlIl;->llllIIIllIlIIIIllllI(Landroid/content/Context;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/diagmonagent/log/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(Ljava/lang/Class;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v1

    .line 77
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 78
    invoke-virtual {p0, v0}, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI(Ljava/util/List;)V

    .line 79
    return-void
.end method

.method protected llllIIIllIlIIIIllllI(Ljava/io/File;)Ljava/util/zip/ZipOutputStream;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Ljava/util/zip/ZipOutputStream;

    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v0, v1}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public llllIIIllIlIIIIllllI()V
    .locals 3

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/diagmonagent/log/lllIlIlIIIllIIlIllIl;

    const-string v1, "/log_package_generator.log"

    invoke-direct {v0, v1}, Lcom/sec/android/diagmonagent/log/lllIlIlIIIllIIlIllIl;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llllIIIllIlIIIIllllI:Ljava/util/zip/ZipOutputStream;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/diagmonagent/log/lllIlIlIIIllIIlIllIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/util/zip/ZipOutputStream;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/diagmonagent/log/LogPackageGenerator;->llllIIIllIlIIIIllllI:Ljava/util/zip/ZipOutputStream;

    invoke-virtual {v0}, Ljava/util/zip/ZipOutputStream;->close()V

    .line 55
    return-void
.end method
