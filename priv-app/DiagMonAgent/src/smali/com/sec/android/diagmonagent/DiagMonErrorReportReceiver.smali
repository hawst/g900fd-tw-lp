.class public Lcom/sec/android/diagmonagent/DiagMonErrorReportReceiver;
.super Landroid/content/BroadcastReceiver;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 34
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "Action is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 54
    :goto_0
    return-void

    .line 40
    :cond_0
    const-string v1, "com.sec.android.diagmon.intent.COPY_LOGPACKAGE_TO_SDCARD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "COPY_PACKAGE: Copy Log Package to SDCard"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 42
    const-string v0, "serviceId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-static {p1, v0}, LIlIIlIIlIllllIlllIII;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_1
    const-string v0, "serviceId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 46
    const-string v0, "uimode"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 48
    const-string v0, "workId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 49
    const-string v0, "errorCode"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 51
    const-string v2, "com.sec.android.diagmon.intent.REPORT_ERROR"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/diagmonagent/DiagMonErrorReportReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/diagmonagent/registration/RegistrationIntentService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 67
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v1, "serviceId"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v1, "uimode"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 71
    const-string v1, "workId"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v1, "errorCode"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 75
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 19
    if-nez p1, :cond_1

    .line 20
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "context is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    if-nez p2, :cond_2

    .line 24
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "intent is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 27
    :cond_2
    invoke-static {p2}, LlIIllllllllIlIlIllll;->llIIIIlllllIIllIIllI(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/sec/android/diagmonagent/DiagMonErrorReportReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
