.class public Lcom/sec/android/diagmonagent/GCMIntentService;
.super Lcom/google/android/gcm/llIIIIlllllIIllIIllI;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "GCMIntentService"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gcm/llIIIIlllllIIllIIllI;-><init>([Ljava/lang/String;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 29
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "GCM received"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 30
    const-string v0, "msg"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 33
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/diagmonagent/DiagMonAgentInitService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 36
    const-string v2, "com.sec.android.diagmon.intent.REPORT_ERROR"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    const-string v2, "initType"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 38
    const-string v2, "msg"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 42
    :cond_0
    return-void
.end method

.method protected llIlIIIIlIIIIIlIlIII(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 53
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "GCM unregistered"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 54
    invoke-static {}, LllllIlllIIIlIlIlIIII;->llIIIIlllllIIllIIllI()LllllIlllIIIlIlIlIIII;

    move-result-object v0

    .line 55
    const-string v1, ""

    invoke-virtual {v0, p2, v1}, LllllIlllIIIlIlIlIIII;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method protected lllIlIlIIIllIIlIllIl(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 46
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "GCM registered"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 47
    invoke-static {}, LllllIlllIIIlIlIlIIII;->llIIIIlllllIIllIIllI()LllllIlllIIIlIlIlIIII;

    move-result-object v0

    .line 48
    const-string v1, ""

    invoke-virtual {v0, p2, v1}, LllllIlllIIIlIlIlIIII;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method protected llllIIIllIlIIIIllllI(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 22
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GCM error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 23
    invoke-static {}, LllllIlllIIIlIlIlIIII;->llIIIIlllllIIllIIllI()LllllIlllIIIlIlIlIIII;

    move-result-object v0

    .line 24
    const-string v1, ""

    invoke-virtual {v0, v1, p2}, LllllIlllIIIlIlIlIIII;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void
.end method
