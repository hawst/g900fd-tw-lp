.class public Lcom/sec/android/diagmonagent/DiagMonAgentApplication;
.super Landroid/app/Application;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/diagmondm/interfaces/llIIIIlllllIIllIIllI;


# static fields
.field private static llIIIIlllllIIllIIllI:Landroid/content/Context;

.field private static llllIIIllIlIIIIllllI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    .line 28
    const-string v0, ""

    sput-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llllIIIllIlIIIIllllI:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static IllIlIIIIlIIlIIIllIl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llllIIIllIlIIIIllllI:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIlIIIIlIIIIIlIlIII()V

    .line 141
    :cond_0
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llllIIIllIlIIIIllllI:Ljava/lang/String;

    return-object v0
.end method

.method public static llIIIIlllllIIllIIllI()Landroid/content/Context;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    return-object v0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 129
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 131
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llllIIIllIlIIIIllllI:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIlIIIIlIIIIIlIlIII()V
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 124
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    .line 125
    :cond_0
    return-void
.end method

.method public static lllIlIlIIIllIIlIllIl()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 110
    const/4 v0, 0x0

    .line 113
    :try_start_0
    new-instance v1, LlllIIIlllIllIlIllIIl;

    sget-object v2, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-direct {v1, v2}, LlllIIIlllIllIlIllIIl;-><init>(Landroid/content/Context;)V

    .line 114
    invoke-virtual {v1}, LlllIIIlllIllIlIllIIl;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    .line 115
    :catch_0
    move-exception v1

    .line 116
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 99
    :try_start_0
    new-instance v1, LlllIIIlllIllIlIllIIl;

    sget-object v2, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-direct {v1, v2}, LlllIIIlllIllIlIllIIl;-><init>(Landroid/content/Context;)V

    .line 100
    invoke-virtual {v1}, LlllIIIlllIllIlIllIIl;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    .line 101
    :catch_0
    move-exception v1

    .line 102
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 84
    sput-object p1, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    .line 85
    return-void
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 32
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 34
    sput-object p0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    .line 35
    const-string v0, "DIAGMON_AGENT"

    invoke-static {v0}, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 37
    invoke-static {}, LlllllIlIlIIIIIIIIlIl;->llIIIIlllllIIllIIllI()V

    .line 38
    invoke-static {}, LlllllIlIlIIIIIIIIlIl;->llIlIIIIlIIIIIlIlIII()V

    .line 39
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    .line 41
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "myUserId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 43
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    .line 46
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v2, "It is disabled because you are not owner."

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 47
    sget-object v1, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 79
    :goto_0
    return-void

    .line 50
    :cond_0
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "PackageManager is null, so it is not disabled."

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_1
    invoke-static {}, Lcom/diagmondm/db/file/XDB;->llIIIIlllllIIllIIllI()Z

    .line 57
    invoke-static {}, LllIIlIlIlIlIIIIIIlIl;->llIIIIlllllIIllIIllI()Z

    .line 59
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->llllIIIllIlIIIIllllI()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/diagmondm/db/file/XDBDiagMonFunction;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)I

    move-result v0

    .line 61
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/diagmondm/db/file/XDBDiagMon;->IllIlIIIIlIIlIIIllIl()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, LIIlIlIIlIlIIlIlllIIl;->IlIlIlIlIlIIlllllIlI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_3

    .line 64
    if-eqz v0, :cond_2

    .line 65
    invoke-static {}, LIIIIIlIlIlIllIlIIllI;->IIlIlIIIlIIlIlIIIIII()V

    .line 78
    :cond_2
    :goto_1
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "DiagMon DM Application Start !"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_3
    if-eqz v0, :cond_2

    .line 69
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    if-nez v0, :cond_4

    .line 70
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "m_Context is null"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_1

    .line 72
    :cond_4
    sget-object v0, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v1, "DM Service Start!"

    invoke-virtual {v0, v1}, LlIlllIlIlIIIllIIIIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    const-class v3, Lcom/diagmondm/XDMService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1
.end method
