.class public Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;
.super Landroid/preference/PreferenceActivity;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static lllIlIlIIIllIIlIllIl:I

.field private static llllIIIllIlIIIIllllI:[Ljava/lang/String;


# instance fields
.field private llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Developer server"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Staging server"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Production server"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->llllIIIllIlIIIIllllI:[Ljava/lang/String;

    .line 44
    invoke-static {}, LIIIlIllIlIIIIIlIIIll;->llIlIIIIlIIIIIlIlIII()I

    move-result v0

    sput v0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->lllIlIlIIIllIIlIllIl:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;

    .line 346
    return-void
.end method

.method protected static llIIIIlllllIIllIIllI()V
    .locals 1

    .prologue
    .line 389
    invoke-static {}, LIIIlIllIlIIIIIlIIIll;->llIIIIlllllIIllIIllI()V

    .line 390
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llIIIIlllllIIllIIllI()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    .line 391
    return-void
.end method

.method static synthetic lllIlIlIIIllIIlIllIl()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->lllIlIlIIIllIIlIllIl:I

    return v0
.end method

.method static synthetic llllIIIllIlIIIIllllI(I)I
    .locals 0

    .prologue
    .line 34
    sput p0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->lllIlIlIIIllIIlIllIl:I

    return p0
.end method

.method static synthetic llllIIIllIlIIIIllllI()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->llllIIIllIlIIIIllllI:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected llIIIIlllllIIllIIllI(I)V
    .locals 0

    .prologue
    .line 394
    sput p1, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->lllIlIlIIIllIIlIllIl:I

    .line 395
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    new-instance v0, Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;

    invoke-direct {v0}, Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;-><init>()V

    iput-object v0, p0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;

    .line 52
    iget-object v0, p0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {v0, p0}, Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 54
    const v1, 0x1020002

    iget-object v2, p0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 55
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 56
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/diagmonagent/ui/DiagMonTestActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {v0}, Lcom/sec/android/diagmonagent/ui/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI()V

    .line 61
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 62
    return-void
.end method
