.class public Lcom/sec/android/diagmonagent/osp/http/RestClient;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field private llIIIIlllllIIllIIllI:I

.field private llllIIIllIlIIIIllllI:LIIlIIlIIlIlIllIIIIlI;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const v0, 0x2bf20

    :try_start_0
    iput v0, p0, Lcom/sec/android/diagmonagent/osp/http/RestClient;->llIIIIlllllIIllIIllI:I

    .line 86
    new-instance v0, LIIlIIlIIlIlIllIIIIlI;

    invoke-direct {v0, p1, p2, p3, p4}, LIIlIIlIIlIlIllIIIIlI;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/diagmonagent/osp/http/RestClient;->llllIIIllIlIIIIllllI:LIIlIIlIIlIlIllIIIIlI;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    new-instance v1, Lcom/sec/android/diagmonagent/osp/http/RestClientException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/sec/android/diagmonagent/osp/http/RestClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public llIIIIlllllIIllIIllI(Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 14

    .prologue
    .line 107
    const/4 v3, 0x0

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 113
    :try_start_0
    invoke-static {}, LIIIlIllIlIIIIIlIIIll;->lllIlIlIIIllIIlIllIl()Ljava/lang/String;

    move-result-object v2

    .line 116
    sget-object v4, Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;->llllIIIllIlIIIIllllI:Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;

    if-ne p1, v4, :cond_3

    .line 117
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, v5}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    move-object v3, v4

    .line 130
    :goto_0
    iget-object v4, p0, Lcom/sec/android/diagmonagent/osp/http/RestClient;->llllIIIllIlIIIIllllI:LIIlIIlIIlIlIllIIIIlI;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v4, v0, v1, v2}, LIIlIIlIIlIlIllIIIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 134
    const-string v4, "User-Agent"

    const-string v6, "SAMSUNG/Android"

    invoke-interface {v3, v4, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v4, "Accept"

    const-string v6, "*, */*"

    invoke-interface {v3, v4, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v4, "Accept-Encoding"

    const-string v6, "identity"

    invoke-interface {v3, v4, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v4, "Content-Type"

    const-string v6, "application/xml"

    invoke-interface {v3, v4, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v4, "Connection"

    const-string v6, "keep-alive"

    invoke-interface {v3, v4, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v4, "Authorization"

    invoke-interface {v3, v4, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    if-nez v2, :cond_0

    .line 144
    :try_start_1
    sget-object v2, Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;

    if-ne p1, v2, :cond_8

    .line 145
    move-object v0, v3

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    move-object v2, v0

    new-instance v4, Lorg/apache/http/entity/StringEntity;

    const-string v6, "UTF-8"

    move-object/from16 v0, p4

    invoke-direct {v4, v0, v6}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 158
    :cond_0
    :goto_1
    :try_start_2
    new-instance v4, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v4}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 160
    iget v2, p0, Lcom/sec/android/diagmonagent/osp/http/RestClient;->llIIIIlllllIIllIIllI:I

    invoke-static {v4, v2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 161
    iget v2, p0, Lcom/sec/android/diagmonagent/osp/http/RestClient;->llIIIIlllllIIllIIllI:I

    invoke-static {v4, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 164
    const-string v2, "http.proxyHost"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 165
    const-string v2, "http.proxyPort"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 166
    if-eqz v6, :cond_1

    if-eqz v7, :cond_1

    .line 167
    new-instance v2, Lorg/apache/http/HttpHost;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-direct {v2, v6, v8}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 168
    invoke-static {v4, v2}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 172
    :cond_1
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 173
    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v8

    .line 174
    invoke-virtual {v5}, Landroid/net/Uri;->getPort()I

    move-result v2

    .line 175
    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    .line 177
    const/4 v9, -0x1

    if-ne v2, v9, :cond_2

    .line 178
    const-string v2, "https"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    const/4 v9, 0x1

    if-ne v2, v9, :cond_a

    .line 179
    const/16 v2, 0x1bb

    .line 184
    :cond_2
    :goto_2
    new-instance v9, Lorg/apache/http/HttpHost;

    invoke-direct {v9, v8, v2, v5}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 187
    new-instance v5, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v5, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 188
    new-instance v2, Lcom/sec/android/diagmonagent/osp/http/llllIIIllIlIIIIllllI;

    invoke-direct {v2, p0}, Lcom/sec/android/diagmonagent/osp/http/llllIIIllIlIIIIllllI;-><init>(Lcom/sec/android/diagmonagent/osp/http/RestClient;)V

    invoke-virtual {v5, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 210
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Method: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 211
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "URI: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v8

    invoke-virtual {v8}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 212
    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    .line 213
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    const-string v8, "Header: "

    invoke-virtual {v2, v8}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 214
    array-length v8, v4

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v8, :cond_b

    aget-object v10, v4, v2

    .line 215
    sget-object v11, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v10}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "\n"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 214
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 118
    :cond_3
    sget-object v4, Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;->llIlIIIIlIIIIIlIlIII:Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;

    if-ne p1, v4, :cond_4

    .line 119
    new-instance v4, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v4, v5}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    move-object v3, v4

    goto/16 :goto_0

    .line 120
    :cond_4
    sget-object v4, Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;

    if-ne p1, v4, :cond_5

    .line 121
    new-instance v4, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v4, v5}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    move-object v3, v4

    goto/16 :goto_0

    .line 122
    :cond_5
    sget-object v2, Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;->llIIIIlllllIIllIIllI:Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;

    if-ne p1, v2, :cond_6

    .line 123
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v4, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 124
    :try_start_3
    const-string v2, ""
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-object v3, v4

    goto/16 :goto_0

    .line 126
    :cond_6
    :try_start_4
    new-instance v2, Lcom/sec/android/diagmonagent/osp/http/RestClientException;

    const-string v4, "Invalid HTTP method"

    invoke-direct {v2, v4}, Lcom/sec/android/diagmonagent/osp/http/RestClientException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 227
    :catch_0
    move-exception v2

    .line 228
    :goto_4
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 230
    if-eqz v3, :cond_7

    .line 231
    :try_start_5
    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 235
    :cond_7
    :goto_5
    new-instance v3, Lcom/sec/android/diagmonagent/osp/http/RestClientException;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "NET_0000"

    invoke-direct {v3, v4, v2, v5}, Lcom/sec/android/diagmonagent/osp/http/RestClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v3

    .line 146
    :cond_8
    :try_start_6
    sget-object v2, Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/diagmonagent/osp/http/RestClient$HttpMethod;

    if-ne p1, v2, :cond_9

    .line 147
    move-object v0, v3

    check-cast v0, Lorg/apache/http/client/methods/HttpPut;

    move-object v2, v0

    new-instance v4, Lorg/apache/http/entity/StringEntity;

    const-string v6, "UTF-8"

    move-object/from16 v0, p4

    invoke-direct {v4, v0, v6}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_6
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_1

    .line 151
    :catch_1
    move-exception v2

    .line 152
    :try_start_7
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 153
    new-instance v4, Lcom/sec/android/diagmonagent/osp/http/RestClientException;

    const-string v5, "UnsupportedEncodingException body"

    invoke-direct {v4, v5, v2}, Lcom/sec/android/diagmonagent/osp/http/RestClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 236
    :catch_2
    move-exception v2

    .line 237
    new-instance v3, Lcom/sec/android/diagmonagent/osp/http/RestClientException;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/sec/android/diagmonagent/osp/http/RestClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 149
    :cond_9
    :try_start_8
    new-instance v2, Lcom/sec/android/diagmonagent/osp/http/RestClientException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid body by HTTP method: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/sec/android/diagmonagent/osp/http/RestClientException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_8
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 181
    :cond_a
    const/16 v2, 0x50

    goto/16 :goto_2

    .line 217
    :cond_b
    :try_start_9
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Body: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 219
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Proxy Host Name: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 220
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Proxy Host Port: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 221
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Target Host: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Lorg/apache/http/HttpHost;->toURI()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 222
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Target Host Port: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LlIlllIlIlIIIllIIIIIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 225
    invoke-virtual {v5, v9, v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    move-result-object v2

    .line 240
    return-object v2

    .line 233
    :catch_3
    move-exception v3

    goto/16 :goto_5

    .line 227
    :catch_4
    move-exception v2

    move-object v3, v4

    goto/16 :goto_4
.end method
