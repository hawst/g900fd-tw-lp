.class public LIllIIlllIllIIIIIllII;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public static llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 18
    :try_start_0
    const-string v0, "create table if not exists Notification (rowid integer primary key autoincrement, appId integer, uiMode integer, sessinoId text, serverId text, opMode text, pushJobId text, serviceId text, deviceinit boolean, devserviceId text, devuimode boolean, devfotaerr text, devworkid text);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :goto_0
    return-void

    .line 20
    :catch_0
    move-exception v0

    .line 22
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Lcom/diagmondm/db/file/XDBNotiInfo;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 40
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 45
    :try_start_0
    const-string v2, "appId"

    iget v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->nAppId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 46
    const-string v2, "uiMode"

    iget v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->nUiMode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 47
    const-string v2, "sessinoId"

    iget-object v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szSessionId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v2, "serverId"

    iget-object v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szServerId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v2, "opMode"

    iget-object v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szOpmode:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v2, "pushJobId"

    iget-object v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szPushJobId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v2, "serviceId"

    iget-object v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szServiceId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v2, "deviceinit"

    iget-boolean v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDeviceInit:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 53
    const-string v2, "devserviceId"

    iget-object v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevServiceId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v2, "devuimode"

    iget-boolean v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDevUiMode:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 55
    const-string v2, "devfotaerr"

    iget-object v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevFotaErr:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v2, "devworkid"

    iget-object v3, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevWorkId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->lllIlIlIIIllIIlIllIl()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 59
    const-string v2, "Notification"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    if-eqz v1, :cond_0

    .line 68
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 63
    :try_start_1
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67
    if-eqz v1, :cond_0

    .line 68
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 68
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 94
    .line 98
    :try_start_0
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->lllIlIlIIIllIIlIllIl()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 99
    const-string v0, "Notification"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    if-eqz v1, :cond_0

    .line 108
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 103
    :try_start_1
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    if-eqz v1, :cond_0

    .line 108
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 108
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 114
    .line 118
    :try_start_0
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->lllIlIlIIIllIIlIllIl()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 119
    const-string v0, "Notification"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    if-eqz v1, :cond_0

    .line 128
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 123
    :try_start_1
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    if-eqz v1, :cond_0

    .line 128
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 128
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public static llIIIIlllllIIllIIllI()Z
    .locals 14

    .prologue
    const/4 v12, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 311
    const/16 v0, 0xd

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowId"

    aput-object v0, v3, v10

    const-string v0, "appId"

    aput-object v0, v3, v12

    const/4 v0, 0x2

    const-string v1, "uiMode"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "sessinoId"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "serverId"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "opMode"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "pushJobId"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "serviceId"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "deviceinit"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "devserviceId"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "devuimode"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "devfotaerr"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "devworkid"

    aput-object v1, v3, v0

    .line 328
    :try_start_0
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llllIIIllIlIIIIllllI()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 329
    const/4 v1, 0x1

    :try_start_1
    const-string v2, "Notification"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 331
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-lez v1, :cond_1

    move v1, v12

    .line 347
    :goto_0
    if-eqz v11, :cond_0

    .line 348
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_0
    if-eqz v0, :cond_6

    .line 351
    invoke-static {v0}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    move v0, v1

    .line 354
    :goto_1
    return v0

    :cond_1
    move v1, v10

    .line 337
    goto :goto_0

    .line 340
    :catch_0
    move-exception v0

    move-object v1, v11

    .line 343
    :goto_2
    :try_start_2
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 347
    if-eqz v11, :cond_2

    .line 348
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_2
    if-eqz v1, :cond_5

    .line 351
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    move v0, v10

    goto :goto_1

    .line 347
    :catchall_0
    move-exception v0

    move-object v1, v11

    :goto_3
    if-eqz v11, :cond_3

    .line 348
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_3
    if-eqz v1, :cond_4

    .line 351
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_4
    throw v0

    .line 347
    :catchall_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    .line 340
    :catch_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_2

    :cond_5
    move v0, v10

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public static llllIIIllIlIIIIllllI()I
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 462
    const/16 v0, 0xd

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowId"

    aput-object v0, v3, v10

    const-string v0, "appId"

    aput-object v0, v3, v1

    const/4 v0, 0x2

    const-string v1, "uiMode"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "sessinoId"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "serverId"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "opMode"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "pushJobId"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "serviceId"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "deviceinit"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "devserviceId"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "devuimode"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "devfotaerr"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "devworkid"

    aput-object v1, v3, v0

    .line 479
    :try_start_0
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llllIIIllIlIIIIllllI()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 480
    const/4 v1, 0x1

    :try_start_1
    const-string v2, "Notification"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 482
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_6

    .line 484
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    .line 494
    :goto_0
    if-eqz v11, :cond_0

    .line 495
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 497
    :cond_0
    if-eqz v0, :cond_5

    .line 498
    invoke-static {v0}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    move v0, v1

    .line 501
    :goto_1
    return v0

    .line 487
    :catch_0
    move-exception v0

    move-object v1, v11

    .line 490
    :goto_2
    :try_start_2
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 494
    if-eqz v11, :cond_1

    .line 495
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 497
    :cond_1
    if-eqz v1, :cond_4

    .line 498
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    move v0, v10

    goto :goto_1

    .line 494
    :catchall_0
    move-exception v0

    move-object v1, v11

    :goto_3
    if-eqz v11, :cond_2

    .line 495
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 497
    :cond_2
    if-eqz v1, :cond_3

    .line 498
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    throw v0

    .line 494
    :catchall_1
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    .line 487
    :catch_1
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_2

    :cond_4
    move v0, v10

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v1, v10

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 30
    :try_start_0
    const-string v0, "DROP TABLE IF EXISTS Notification"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 34
    sget-object v1, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Lcom/diagmondm/db/file/XDBNotiInfo;)V
    .locals 12

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v10, 0x0

    .line 170
    .line 172
    const/16 v0, 0xd

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowId"

    aput-object v0, v3, v2

    const-string v0, "appId"

    aput-object v0, v3, v1

    const-string v0, "uiMode"

    aput-object v0, v3, v4

    const-string v0, "sessinoId"

    aput-object v0, v3, v5

    const/4 v0, 0x4

    const-string v1, "serverId"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "opMode"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "pushJobId"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "serviceId"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "deviceinit"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "devserviceId"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "devuimode"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "devfotaerr"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "devworkid"

    aput-object v1, v3, v0

    .line 189
    :try_start_0
    invoke-static {}, Lcom/sec/android/diagmonagent/DiagMonAgentApplication;->llllIIIllIlIIIIllllI()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v0

    .line 190
    const/4 v1, 0x1

    :try_start_1
    const-string v2, "Notification"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 192
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 194
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    .line 195
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->nRowId:I

    .line 196
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->nAppId:I

    .line 197
    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->nUiMode:I

    .line 198
    const/4 v1, 0x3

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szSessionId:Ljava/lang/String;

    .line 199
    const/4 v1, 0x4

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szServerId:Ljava/lang/String;

    .line 200
    const/4 v1, 0x5

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szOpmode:Ljava/lang/String;

    .line 201
    const/4 v1, 0x6

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szPushJobId:Ljava/lang/String;

    .line 202
    const/4 v1, 0x7

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szServiceId:Ljava/lang/String;

    .line 204
    const/16 v1, 0x8

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_3

    .line 205
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDeviceInit:Z

    .line 209
    :goto_0
    const/16 v1, 0x9

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevServiceId:Ljava/lang/String;

    .line 211
    const/16 v1, 0xa

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_6

    .line 212
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDevUiMode:Z

    .line 216
    :goto_1
    const/16 v1, 0xb

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevFotaErr:Ljava/lang/String;

    .line 217
    const/16 v1, 0xc

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->szDevWorkId:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 228
    :cond_0
    if-eqz v10, :cond_1

    .line 229
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 231
    :cond_1
    if-eqz v0, :cond_2

    .line 232
    invoke-static {v0}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 234
    :cond_2
    return-void

    .line 207
    :cond_3
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDeviceInit:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 220
    :catch_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    .line 222
    :goto_2
    :try_start_3
    sget-object v2, LlIlIIlIIlIlllIIIIlll;->llIIIIlllllIIllIIllI:LlIlllIlIlIIIllIIIIIl;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LlIlllIlIlIIIllIIIIIl;->llIIllllIIlllIIIIlll(Ljava/lang/String;)V

    .line 224
    new-instance v0, Lcom/diagmondm/db/file/XDBUserDBException;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Lcom/diagmondm/db/file/XDBUserDBException;-><init>(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 228
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v10, :cond_4

    .line 229
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 231
    :cond_4
    if-eqz v1, :cond_5

    .line 232
    invoke-static {v1}, LlllIIIlllIllIlIllIIl;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v0

    .line 214
    :cond_6
    const/4 v1, 0x0

    :try_start_4
    iput-boolean v1, p0, Lcom/diagmondm/db/file/XDBNotiInfo;->bDevUiMode:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 228
    :catchall_1
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v1, v10

    goto :goto_3

    .line 220
    :catch_1
    move-exception v0

    move-object v1, v10

    goto :goto_2
.end method
