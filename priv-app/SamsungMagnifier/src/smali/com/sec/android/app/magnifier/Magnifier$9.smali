.class Lcom/sec/android/app/magnifier/Magnifier$9;
.super Ljava/lang/Object;
.source "Magnifier.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/Magnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0

    .prologue
    .line 1260
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1266
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 1267
    sparse-switch p2, :sswitch_data_0

    .line 1301
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1269
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V
    invoke-static {v0, p2, p3}, Lcom/sec/android/app/magnifier/Magnifier;->access$1300(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto :goto_0

    .line 1272
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V
    invoke-static {v0, p2, p3}, Lcom/sec/android/app/magnifier/Magnifier;->access$1400(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto :goto_0

    .line 1275
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$300(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1276
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V
    invoke-static {v0, p2, p3}, Lcom/sec/android/app/magnifier/Magnifier;->access$1300(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto :goto_0

    .line 1280
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$300(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V
    invoke-static {v0, p2, p3}, Lcom/sec/android/app/magnifier/Magnifier;->access$1400(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto :goto_0

    .line 1285
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$300(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1286
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V
    invoke-static {v0, p2, p3}, Lcom/sec/android/app/magnifier/Magnifier;->access$1300(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto :goto_0

    .line 1290
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$300(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1291
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V
    invoke-static {v0, p2, p3}, Lcom/sec/android/app/magnifier/Magnifier;->access$1400(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto :goto_0

    .line 1295
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$9;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V

    goto :goto_0

    .line 1267
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_6
        0x13 -> :sswitch_5
        0x14 -> :sswitch_4
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch
.end method
