.class Lcom/sec/android/app/magnifier/Magnifier$8;
.super Ljava/lang/Object;
.source "Magnifier.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/Magnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0

    .prologue
    .line 1209
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$8;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v3, 0x1

    .line 1221
    const/high16 v1, 0x41100000    # 9.0f

    int-to-float v2, p2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v0, v1, v2

    .line 1222
    .local v0, "zoom_scale":F
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$8;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v1, v0, v3, v3}, Lcom/sec/android/app/magnifier/Magnifier;->setFreezeZoom(FZZ)V

    .line 1223
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$8;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I
    invoke-static {v1, p2}, Lcom/sec/android/app/magnifier/Magnifier;->access$2902(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 1224
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1217
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1213
    return-void
.end method
