.class public Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "FreezeImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/FreezeImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/FreezeImageView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/magnifier/FreezeImageView;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 211
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 229
    :cond_0
    :goto_0
    return v0

    .line 215
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-gt v1, v3, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-gt v1, v3, :cond_0

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    # getter for: Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;
    invoke-static {v1}, Lcom/sec/android/app/magnifier/FreezeImageView;->access$000(Lcom/sec/android/app/magnifier/FreezeImageView;)Landroid/view/ScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    # invokes: Lcom/sec/android/app/magnifier/FreezeImageView;->checkBoundaryX(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/app/magnifier/FreezeImageView;->access$100(Lcom/sec/android/app/magnifier/FreezeImageView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F

    move-result p3

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    # invokes: Lcom/sec/android/app/magnifier/FreezeImageView;->checkBoundaryY(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    invoke-static {v0, p1, p2, p4}, Lcom/sec/android/app/magnifier/FreezeImageView;->access$200(Lcom/sec/android/app/magnifier/FreezeImageView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F

    move-result p4

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    neg-float v1, p3

    neg-float v2, p4

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/magnifier/FreezeImageView;->scrollBy(FF)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    # setter for: Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScrolling:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/magnifier/FreezeImageView;->access$302(Lcom/sec/android/app/magnifier/FreezeImageView;Z)Z

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/FreezeImageView;->invalidate()V

    .line 229
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_0
.end method
