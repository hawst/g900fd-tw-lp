.class Lcom/sec/android/app/magnifier/Magnifier$1;
.super Landroid/view/OrientationEventListener;
.source "Magnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/magnifier/Magnifier;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 562
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$1;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 565
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 566
    const-string v0, "MagnifierActivity"

    const-string v1, "onOrientationChanged: orientation - unknown orientation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    :goto_0
    return-void

    .line 569
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$1;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$1;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/MagnifierSurface;->setCameraDisplayOrientation()V

    .line 573
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$1;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-static {p1}, Lcom/sec/android/app/magnifier/Util;->roundOrientation(I)I

    move-result v1

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setLastOrientation(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->access$1700(Lcom/sec/android/app/magnifier/Magnifier;I)V

    goto :goto_0
.end method
