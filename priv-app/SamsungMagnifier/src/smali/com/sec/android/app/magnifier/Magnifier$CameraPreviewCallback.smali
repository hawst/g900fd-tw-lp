.class final Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;
.super Ljava/lang/Object;
.source "Magnifier.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/Magnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CameraPreviewCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;Lcom/sec/android/app/magnifier/Magnifier$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p2, "x1"    # Lcom/sec/android/app/magnifier/Magnifier$1;

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    return-void
.end method


# virtual methods
.method public onPreviewFrame([BLcom/sec/android/seccamera/SecCamera;)V
    .locals 10
    .param p1, "data"    # [B
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    const/4 v9, 0x0

    .line 266
    const-string v1, "MagnifierActivity"

    const-string v2, "PreviewCallback"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-virtual {p2}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v7

    .line 270
    .local v7, "parameters":Lcom/sec/android/seccamera/SecCamera$Parameters;
    invoke-virtual {v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v8

    .line 272
    .local v8, "size":Lcom/sec/android/seccamera/SecCamera$Size;
    new-instance v0, Landroid/graphics/YuvImage;

    invoke-virtual {v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewFormat()I

    move-result v2

    iget v3, v8, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v4, v8, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    const/4 v5, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/graphics/YuvImage;-><init>([BIII[I)V

    .line 273
    .local v0, "image":Landroid/graphics/YuvImage;
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 275
    .local v6, "out":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/YuvImage;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/YuvImage;->getHeight()I

    move-result v3

    invoke-direct {v1, v9, v9, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v6}, Landroid/graphics/YuvImage;->compressToJpeg(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v3

    invoke-static {v2, v9, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/sec/android/app/magnifier/Magnifier;->access$202(Lcom/sec/android/app/magnifier/Magnifier;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z
    invoke-static {v1}, Lcom/sec/android/app/magnifier/Magnifier;->access$300(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v1}, Lcom/sec/android/app/magnifier/Magnifier;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/app/magnifier/Magnifier;->access$200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/graphics/Bitmap;

    move-result-object v2

    const/16 v3, 0xb4

    invoke-static {v2, v3}, Lcom/sec/android/app/magnifier/Util;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/sec/android/app/magnifier/Magnifier;->access$202(Lcom/sec/android/app/magnifier/Magnifier;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 288
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v1}, Lcom/sec/android/app/magnifier/Magnifier;->getForcedShutterSound()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v1}, Lcom/sec/android/app/magnifier/Magnifier;->pauseAudioPlayback()V

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v9}, Lcom/sec/android/app/magnifier/Magnifier;->playCameraSound(II)V

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v1}, Lcom/sec/android/app/magnifier/Magnifier;->resumeAudioPlayback()V

    .line 294
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v1}, Lcom/sec/android/app/magnifier/Magnifier;->displayFreezeView()V

    .line 295
    return-void

    .line 284
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/app/magnifier/Magnifier;->access$200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/graphics/Bitmap;

    move-result-object v2

    const/16 v3, 0x5a

    invoke-static {v2, v3}, Lcom/sec/android/app/magnifier/Util;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/sec/android/app/magnifier/Magnifier;->access$202(Lcom/sec/android/app/magnifier/Magnifier;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_0
.end method
