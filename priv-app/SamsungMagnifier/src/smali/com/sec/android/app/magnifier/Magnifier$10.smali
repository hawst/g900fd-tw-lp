.class Lcom/sec/android/app/magnifier/Magnifier$10;
.super Landroid/os/Handler;
.source "Magnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/Magnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0

    .prologue
    .line 1836
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$10;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1840
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1859
    :goto_0
    :pswitch_0
    return-void

    .line 1842
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$10;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraOpenCheck:Z

    .line 1843
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$10;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->CannotStartCamera()V
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3000(Lcom/sec/android/app/magnifier/Magnifier;)V

    goto :goto_0

    .line 1846
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$10;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->access$102(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    goto :goto_0

    .line 1850
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$10;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/Magnifier;->hideZoomValue()V

    goto :goto_0

    .line 1853
    :pswitch_4
    const-string v0, "MagnifierActivity"

    const-string v1, "Inactivity timer is expired. finish."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1854
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$10;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V

    goto :goto_0

    .line 1840
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
