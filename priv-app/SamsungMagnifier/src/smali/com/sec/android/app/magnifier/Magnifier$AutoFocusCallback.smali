.class final Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;
.super Ljava/lang/Object;
.source "Magnifier.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/Magnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AutoFocusCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;Lcom/sec/android/app/magnifier/Magnifier$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p2, "x1"    # Lcom/sec/android/app/magnifier/Magnifier$1;

    .prologue
    .line 239
    invoke-direct {p0, p1}, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ILcom/sec/android/seccamera/SecCamera;)V
    .locals 3
    .param p1, "afMsg"    # I
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 242
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 250
    if-ne p1, v2, :cond_2

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$100(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/app/magnifier/Magnifier;->playCameraSound(II)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->access$102(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    goto :goto_0

    .line 255
    :cond_2
    if-nez p1, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$100(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/magnifier/Magnifier;->playCameraSound(II)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->access$102(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    goto :goto_0
.end method
