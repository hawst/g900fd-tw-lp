.class public final Lcom/sec/android/app/magnifier/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final button_back_land:I = 0x7f0a0005

.field public static final button_back_port:I = 0x7f0a000e

.field public static final button_focus_land:I = 0x7f0a0021

.field public static final button_focus_port:I = 0x7f0a003b

.field public static final button_freeze_land:I = 0x7f0a0029

.field public static final button_freeze_port:I = 0x7f0a0033

.field public static final button_light_land:I = 0x7f0a0025

.field public static final button_light_port:I = 0x7f0a0037

.field public static final button_save_land:I = 0x7f0a0001

.field public static final button_save_port:I = 0x7f0a0012

.field public static final freeze_seekbar_container_land:I = 0x7f0a0009

.field public static final freeze_seekbar_container_port:I = 0x7f0a0016

.field public static final freeze_seekbar_land:I = 0x7f0a000b

.field public static final freeze_seekbar_port:I = 0x7f0a0018

.field public static final freeze_view_land:I = 0x7f0a0000

.field public static final freeze_view_port:I = 0x7f0a000d

.field public static final icon_freeze_minus_land:I = 0x7f0a000c

.field public static final icon_freeze_minus_port:I = 0x7f0a0017

.field public static final icon_freeze_plus_land:I = 0x7f0a000a

.field public static final icon_freeze_plus_port:I = 0x7f0a0019

.field public static final icon_minus_land:I = 0x7f0a0030

.field public static final icon_minus_port:I = 0x7f0a0040

.field public static final icon_plus_land:I = 0x7f0a002e

.field public static final icon_plus_port:I = 0x7f0a0042

.field public static final img_back_land:I = 0x7f0a0006

.field public static final img_back_port:I = 0x7f0a000f

.field public static final img_focus_land:I = 0x7f0a0022

.field public static final img_focus_port:I = 0x7f0a003c

.field public static final img_freeze_land:I = 0x7f0a002a

.field public static final img_freeze_port:I = 0x7f0a0034

.field public static final img_light_land:I = 0x7f0a0026

.field public static final img_light_port:I = 0x7f0a0038

.field public static final img_save_land:I = 0x7f0a0002

.field public static final img_save_port:I = 0x7f0a0013

.field public static final magnifier_freeze_land:I = 0x7f0a001e

.field public static final magnifier_freeze_port:I = 0x7f0a001d

.field public static final magnifier_preview_land:I = 0x7f0a001c

.field public static final magnifier_preview_port:I = 0x7f0a001b

.field public static final main_container:I = 0x7f0a001a

.field public static final seekbar_container_land:I = 0x7f0a002d

.field public static final seekbar_container_port:I = 0x7f0a003f

.field public static final seekbar_land:I = 0x7f0a002f

.field public static final seekbar_port:I = 0x7f0a0041

.field public static final text_back_land:I = 0x7f0a0007

.field public static final text_back_port:I = 0x7f0a0010

.field public static final text_focus_land:I = 0x7f0a0023

.field public static final text_focus_port:I = 0x7f0a003d

.field public static final text_freeze_land:I = 0x7f0a002b

.field public static final text_freeze_port:I = 0x7f0a0035

.field public static final text_light_land:I = 0x7f0a0027

.field public static final text_light_port:I = 0x7f0a0039

.field public static final text_save_land:I = 0x7f0a0003

.field public static final text_save_port:I = 0x7f0a0014

.field public static final txt_back_land:I = 0x7f0a0008

.field public static final txt_back_port:I = 0x7f0a0011

.field public static final txt_focus_land:I = 0x7f0a0024

.field public static final txt_focus_port:I = 0x7f0a003e

.field public static final txt_freeze_land:I = 0x7f0a002c

.field public static final txt_freeze_port:I = 0x7f0a0036

.field public static final txt_light_land:I = 0x7f0a0028

.field public static final txt_light_port:I = 0x7f0a003a

.field public static final txt_save_land:I = 0x7f0a0004

.field public static final txt_save_port:I = 0x7f0a0015

.field public static final zoom_num_land:I = 0x7f0a0020

.field public static final zoom_num_port:I = 0x7f0a0032

.field public static final zoom_plus_land:I = 0x7f0a001f

.field public static final zoom_plus_port:I = 0x7f0a0031


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
