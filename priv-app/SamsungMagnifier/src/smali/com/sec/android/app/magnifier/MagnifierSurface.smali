.class Lcom/sec/android/app/magnifier/MagnifierSurface;
.super Landroid/view/SurfaceView;
.source "MagnifierSurface.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field public static final NORMAL_RATIO:D = 1.3333333333333333

.field public static final OPEN_RETRY_NUMBER:I = 0x0

.field public static final REAR_CAMERA_ID:I = 0x0

.field public static final TAG:Ljava/lang/String; = "MagnifierSurface"

.field public static final WIDE_SCREEN_RATIO:D = 1.7777777777777777


# instance fields
.field private mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

.field private mAspectRatio:D

.field public mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHorizontalTileSize:I

.field protected mRetry:I

.field private mVerticalTileSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x1

    .line 40
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 25
    const-wide v2, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    iput-wide v2, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    .line 27
    iput v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mHorizontalTileSize:I

    .line 28
    iput v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mVerticalTileSize:I

    .line 37
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mRetry:I

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mHandler:Landroid/os/Handler;

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mContext:Landroid/content/Context;

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/magnifier/Magnifier;

    iput-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/MagnifierSurface;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 46
    .local v0, "holder":Landroid/view/SurfaceHolder;
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 47
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/MagnifierSurface;->getCameraInstance()Lcom/sec/android/seccamera/SecCamera;

    .line 49
    return-void
.end method

.method private roundUpToTile(III)I
    .locals 1
    .param p1, "dimension"    # I
    .param p2, "tileSize"    # I
    .param p3, "maxDimension"    # I

    .prologue
    .line 225
    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, p2

    mul-int/2addr v0, p2

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getCameraInstance()Lcom/sec/android/seccamera/SecCamera;
    .locals 3

    .prologue
    .line 116
    const-string v1, "MagnifierSurface"

    const-string v2, "getCameraInstance"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v1, :cond_0

    .line 120
    :try_start_0
    invoke-static {}, Lcom/sec/android/seccamera/SecCamera;->open()Lcom/sec/android/seccamera/SecCamera;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    :goto_1
    return-object v1

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/Exception;
    iget v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mRetry:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mRetry:I

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mRetry:I

    if-gez v1, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/MagnifierSurface;->getCameraInstance()Lcom/sec/android/seccamera/SecCamera;

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v1, :cond_2

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    goto :goto_1

    .line 129
    :cond_2
    const-string v1, "MagnifierSurface"

    const-string v2, "service cannot connect. critical exception occured."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mHandler:Landroid/os/Handler;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 191
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 192
    .local v4, "widthSpecSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 194
    .local v2, "heightSpecSize":I
    move v3, v4

    .line 195
    .local v3, "width":I
    move v1, v2

    .line 197
    .local v1, "height":I
    if-lez v3, :cond_4

    if-lez v1, :cond_4

    .line 198
    int-to-float v5, v3

    int-to-float v6, v1

    div-float v0, v5, v6

    .line 199
    .local v0, "defaultRatio":F
    iget-object v5, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    .line 200
    float-to-double v6, v0

    iget-wide v8, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    cmpg-double v5, v6, v8

    if-gez v5, :cond_1

    .line 202
    int-to-double v6, v3

    iget-wide v8, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    div-double/2addr v6, v8

    double-to-int v1, v6

    .line 214
    :cond_0
    :goto_0
    iget v5, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mHorizontalTileSize:I

    invoke-direct {p0, v3, v5, v4}, Lcom/sec/android/app/magnifier/MagnifierSurface;->roundUpToTile(III)I

    move-result v3

    .line 215
    iget v5, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mVerticalTileSize:I

    invoke-direct {p0, v1, v5, v2}, Lcom/sec/android/app/magnifier/MagnifierSurface;->roundUpToTile(III)I

    move-result v1

    .line 217
    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/magnifier/MagnifierSurface;->setMeasuredDimension(II)V

    .line 222
    .end local v0    # "defaultRatio":F
    :goto_1
    return-void

    .line 203
    .restart local v0    # "defaultRatio":F
    :cond_1
    float-to-double v6, v0

    iget-wide v8, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    cmpl-double v5, v6, v8

    if-lez v5, :cond_0

    .line 204
    int-to-double v6, v1

    iget-wide v8, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    mul-double/2addr v6, v8

    double-to-int v3, v6

    goto :goto_0

    .line 207
    :cond_2
    float-to-double v6, v0

    iget-wide v8, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    cmpg-double v5, v6, v8

    if-gez v5, :cond_3

    .line 209
    int-to-double v6, v1

    iget-wide v8, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    div-double/2addr v6, v8

    double-to-int v3, v6

    goto :goto_0

    .line 210
    :cond_3
    float-to-double v6, v0

    iget-wide v8, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    cmpl-double v5, v6, v8

    if-lez v5, :cond_0

    .line 211
    int-to-double v6, v3

    iget-wide v8, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mAspectRatio:D

    mul-double/2addr v6, v8

    double-to-int v1, v6

    goto :goto_0

    .line 221
    .end local v0    # "defaultRatio":F
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onMeasure(II)V

    goto :goto_1
.end method

.method public releaseCamera()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 138
    const-string v4, "MagnifierSurface"

    const-string v5, "releaseCamera"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v4, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v4, :cond_1

    .line 142
    iget-object v4, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v1

    .line 143
    .local v1, "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    const-string v4, "off"

    invoke-virtual {v1, v4}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 144
    iget-object v4, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v4, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 146
    iget-object v4, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    iget-boolean v4, v4, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    if-nez v4, :cond_0

    .line 147
    iget-object v4, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v4}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "torch_light"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x1

    .line 148
    .local v2, "torchStatusOn":Z
    :goto_0
    if-eqz v2, :cond_0

    .line 149
    iget-object v4, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v4}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "torch_light"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 151
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.ACTION_ASSISTIVE_WIDGET_STATE_CHANGE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 152
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "from"

    const-string v5, "widget"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string v4, "value"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v3}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 158
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "torchStatusOn":Z
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v3, v6}, Lcom/sec/android/seccamera/SecCamera;->setPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V

    .line 159
    iget-object v3, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v3}, Lcom/sec/android/seccamera/SecCamera;->stopPreview()V

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v3}, Lcom/sec/android/seccamera/SecCamera;->release()V

    .line 161
    iput-object v6, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 163
    .end local v1    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_1
    return-void

    .restart local v1    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_2
    move v2, v3

    .line 147
    goto :goto_0
.end method

.method public setCameraDisplayOrientation()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v2, :cond_0

    .line 182
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-static {v2}, Lcom/sec/android/app/magnifier/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v0

    .line 174
    .local v0, "degrees":I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Util;->getCameraFacing(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 175
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Util;->getCameraOrientation(I)I

    move-result v2

    add-int/2addr v2, v0

    rem-int/lit16 v1, v2, 0x168

    .line 176
    .local v1, "result":I
    rsub-int v2, v1, 0x168

    rem-int/lit16 v1, v2, 0x168

    .line 181
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2, v1}, Lcom/sec/android/seccamera/SecCamera;->setDisplayOrientation(I)V

    goto :goto_0

    .line 178
    .end local v1    # "result":I
    :cond_1
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Util;->getCameraOrientation(I)I

    move-result v2

    sub-int/2addr v2, v0

    add-int/lit16 v2, v2, 0x168

    rem-int/lit16 v1, v2, 0x168

    .restart local v1    # "result":I
    goto :goto_1
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 10
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const-wide v8, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    .line 66
    const-string v6, "MagnifierSurface"

    const-string v7, "surfaceChanged"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v6, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v6, :cond_1

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v6}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v1

    .line 73
    .local v1, "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    if-eqz v1, :cond_0

    .line 77
    invoke-virtual {v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v5

    .line 78
    .local v5, "preview_sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    invoke-virtual {v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v3

    .line 79
    .local v3, "picture_sizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    if-eqz v5, :cond_2

    if-nez v3, :cond_3

    .line 80
    :cond_2
    const-string v6, "MagnifierSurface"

    const-string v7, "supported preview or capture size is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 84
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-static {v6, v5, v8, v9}, Lcom/sec/android/app/magnifier/Util;->getOptimalPreviewSize(Landroid/app/Activity;Ljava/util/List;D)Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v4

    .line 85
    .local v4, "preview_size":Lcom/sec/android/seccamera/SecCamera$Size;
    if-eqz v4, :cond_4

    .line 86
    iget v6, v4, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v7, v4, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewSize(II)V

    .line 89
    :cond_4
    invoke-static {v3, v8, v9}, Lcom/sec/android/app/magnifier/Util;->getOptimalPictureSize(Ljava/util/List;D)Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v2

    .line 90
    .local v2, "picture_size":Lcom/sec/android/seccamera/SecCamera$Size;
    if-eqz v2, :cond_5

    .line 91
    iget v6, v2, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v7, v2, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPictureSize(II)V

    .line 94
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v6}, Lcom/sec/android/app/magnifier/Magnifier;->getAntiBanding()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setAntibanding(Ljava/lang/String;)V

    .line 96
    const-string v6, "continuous-picture"

    invoke-virtual {v1, v6}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 102
    iget-object v6, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v6, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 105
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v6}, Lcom/sec/android/seccamera/SecCamera;->startPreview()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/MagnifierSurface;->setCameraDisplayOrientation()V

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "MagnifierSurface"

    const-string v7, "cannot start preview. critical exception occured."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v6, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mHandler:Landroid/os/Handler;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 52
    const-string v1, "MagnifierSurface"

    const-string v2, "surfaceCreated"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v1, :cond_0

    .line 63
    :goto_0
    return-void

    .line 58
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v1, p1}, Lcom/sec/android/seccamera/SecCamera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v1}, Lcom/sec/android/seccamera/SecCamera;->release()V

    .line 61
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/MagnifierSurface;->releaseCamera()V

    .line 187
    return-void
.end method
