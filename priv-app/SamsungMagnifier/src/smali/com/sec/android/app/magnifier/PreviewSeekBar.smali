.class public Lcom/sec/android/app/magnifier/PreviewSeekBar;
.super Landroid/widget/SeekBar;
.source "PreviewSeekBar.java"


# instance fields
.field isLandscape:Z

.field private lastProgress:I

.field mLocalProgress:I

.field mSeekbarContext:Landroid/content/Context;

.field private onChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, p1}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 14
    iput v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    .line 54
    iput v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mSeekbarContext:Landroid/content/Context;

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    iput v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    .line 54
    iput v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mSeekbarContext:Landroid/content/Context;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    iput v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    .line 54
    iput v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mSeekbarContext:Landroid/content/Context;

    .line 24
    return-void
.end method


# virtual methods
.method public calculatorProgress(I)I
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 139
    iget-object v2, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mSeekbarContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/Magnifier;->getZoomSensitizing()I

    move-result v1

    .line 140
    .local v1, "zoomSensitive":I
    div-int/lit8 v2, v1, 0x2

    add-int/2addr p1, v2

    .line 141
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    .line 142
    mul-int v2, v1, v0

    if-lt p1, v2, :cond_1

    add-int/lit8 v2, v0, 0x1

    mul-int/2addr v2, v1

    if-ge p1, v2, :cond_1

    .line 143
    mul-int p1, v1, v0

    .line 147
    :cond_0
    return p1

    .line 141
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onDraw(Landroid/graphics/Canvas;)V

    .line 45
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    .line 166
    const/4 v0, 0x0

    .line 167
    .local v0, "retVal":Z
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 192
    :goto_0
    return v1

    .line 171
    :cond_0
    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v1, v0

    .line 192
    goto :goto_0

    .line 174
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getHeight()I

    move-result v3

    invoke-virtual {p0, v2, v3, v1, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onSizeChanged(IIII)V

    .line 175
    const/4 v0, 0x1

    .line 176
    goto :goto_1

    .line 179
    :pswitch_2
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->isLandscape:Z

    if-nez v2, :cond_1

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getHeight()I

    move-result v3

    invoke-virtual {p0, v2, v3, v1, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onSizeChanged(IIII)V

    .line 181
    const/4 v0, 0x1

    goto :goto_1

    .line 186
    :pswitch_3
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->isLandscape:Z

    if-eqz v2, :cond_1

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getHeight()I

    move-result v3

    invoke-virtual {p0, v2, v3, v1, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onSizeChanged(IIII)V

    .line 188
    const/4 v0, 0x1

    goto :goto_1

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 152
    const/4 v0, 0x0

    .line 153
    .local v0, "retVal":Z
    packed-switch p1, :pswitch_data_0

    .line 161
    :goto_0
    :pswitch_0
    return v0

    .line 158
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected declared-synchronized onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mSeekbarContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->isLandscape:Z

    .line 39
    invoke-super {p0, p1, p2}, Landroid/widget/SeekBar;->onMeasure(II)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    monitor-exit p0

    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/SeekBar;->onSizeChanged(IIII)V

    .line 33
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 58
    const/4 v0, 0x0

    .line 60
    .local v0, "progress":I
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 135
    :goto_0
    return v1

    .line 64
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_1
    move v1, v2

    .line 135
    goto :goto_0

    .line 66
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-interface {v3, p0}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onStartTrackingTouch(Landroid/widget/SeekBar;)V

    .line 68
    iget-boolean v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->isLandscape:Z

    if-eqz v3, :cond_4

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-int v4, v4

    sub-int v0, v3, v4

    .line 74
    :goto_2
    if-gez v0, :cond_1

    .line 75
    const/4 v0, 0x0

    .line 77
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v3

    if-le v0, v3, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v0

    .line 80
    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->calculatorProgress(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    .line 81
    iget v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 82
    iget v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    if-eq v0, v3, :cond_3

    .line 83
    iput v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    .line 84
    iget-object v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-interface {v3, p0, v0, v1}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 86
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getHeight()I

    move-result v4

    invoke-virtual {p0, v3, v4, v1, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onSizeChanged(IIII)V

    .line 87
    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setPressed(Z)V

    .line 88
    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setSelected(Z)V

    goto :goto_1

    .line 71
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-int v0, v3

    goto :goto_2

    .line 91
    :pswitch_1
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 93
    iget-boolean v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->isLandscape:Z

    if-eqz v3, :cond_8

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-int v4, v4

    sub-int v0, v3, v4

    .line 99
    :goto_3
    if-gez v0, :cond_5

    .line 100
    const/4 v0, 0x0

    .line 102
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v3

    if-le v0, v3, :cond_6

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v0

    .line 106
    :cond_6
    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->calculatorProgress(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    .line 107
    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 109
    iget v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    if-eq v0, v3, :cond_7

    .line 110
    iput v0, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-interface {v3, p0, v0, v2}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 113
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getHeight()I

    move-result v4

    invoke-virtual {p0, v3, v4, v1, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onSizeChanged(IIII)V

    .line 114
    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setPressed(Z)V

    .line 115
    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setSelected(Z)V

    goto/16 :goto_1

    .line 96
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getMax()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-int v0, v3

    goto :goto_3

    .line 118
    :pswitch_2
    iget v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->calculatorProgress(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    .line 119
    iget v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 120
    iget v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    if-eq v0, v3, :cond_9

    .line 121
    iget v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    iput v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->lastProgress:I

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iget v4, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->mLocalProgress:I

    invoke-interface {v3, p0, v4, v2}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 124
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->getHeight()I

    move-result v4

    invoke-virtual {p0, v3, v4, v1, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onSizeChanged(IIII)V

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-interface {v3, p0}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onStopTrackingTouch(Landroid/widget/SeekBar;)V

    .line 126
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setPressed(Z)V

    .line 127
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setSelected(Z)V

    goto/16 :goto_1

    .line 130
    :pswitch_3
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 131
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setPressed(Z)V

    .line 132
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setSelected(Z)V

    goto/16 :goto_1

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V
    .locals 0
    .param p1, "onChangeListener"    # Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 52
    return-void
.end method
