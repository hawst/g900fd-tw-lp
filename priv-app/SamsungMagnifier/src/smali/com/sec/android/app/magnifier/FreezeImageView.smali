.class public Lcom/sec/android/app/magnifier/FreezeImageView;
.super Lcom/sec/android/app/magnifier/ImageViewTouchBase;
.source "FreezeImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;,
        Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FreezeImageView"


# instance fields
.field private mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

.field private mContext:Landroid/content/Context;

.field protected mCurrentScaleFactor:F

.field private mGestureDetector:Landroid/view/GestureDetector;

.field protected mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mIsScaleActivated:Z

.field private mIsScrolling:Z

.field protected mLastScaleFactor:F

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field protected mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field public recycler:Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    iput-boolean v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScrolling:Z

    .line 42
    iput-boolean v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScaleActivated:Z

    .line 44
    new-instance v0, Lcom/sec/android/app/magnifier/FreezeImageView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/FreezeImageView$1;-><init>(Lcom/sec/android/app/magnifier/FreezeImageView;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->recycler:Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->recycler:Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;

    invoke-super {p0, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setRecycler(Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;)V

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mContext:Landroid/content/Context;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/magnifier/Magnifier;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->initFreezeView()V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/magnifier/FreezeImageView;)Landroid/view/ScaleGestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/FreezeImageView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/magnifier/FreezeImageView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/FreezeImageView;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # Landroid/view/MotionEvent;
    .param p3, "x3"    # F

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/magnifier/FreezeImageView;->checkBoundaryX(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/magnifier/FreezeImageView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/FreezeImageView;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # Landroid/view/MotionEvent;
    .param p3, "x3"    # F

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/magnifier/FreezeImageView;->checkBoundaryY(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F

    move-result v0

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/magnifier/FreezeImageView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/FreezeImageView;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScrolling:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/magnifier/FreezeImageView;)Lcom/sec/android/app/magnifier/Magnifier;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/FreezeImageView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/magnifier/FreezeImageView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/FreezeImageView;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScaleActivated:Z

    return p1
.end method

.method private checkBoundaryX(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F

    .prologue
    const/4 v1, 0x0

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 141
    .local v0, "bitmapRect":Landroid/graphics/RectF;
    if-nez v0, :cond_1

    move p3, v1

    .line 167
    .end local p3    # "distanceX":F
    :cond_0
    :goto_0
    return p3

    .line 145
    .restart local p3    # "distanceX":F
    :cond_1
    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    move p3, v1

    .line 146
    goto :goto_0

    .line 149
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    cmpl-float v2, v2, v1

    if-lez v2, :cond_4

    .line 150
    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_3

    move p3, v1

    .line 151
    goto :goto_0

    .line 154
    :cond_3
    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, p3

    cmpl-float v1, v2, v1

    if-lez v1, :cond_0

    .line 155
    iget p3, v0, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 158
    :cond_4
    iget v2, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    move p3, v1

    .line 159
    goto :goto_0

    .line 162
    :cond_5
    iget v1, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 163
    iget v1, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result p3

    goto :goto_0
.end method

.method private checkBoundaryY(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)F
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceY"    # F

    .prologue
    const/4 v2, 0x0

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 172
    .local v0, "bitmapRect":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .line 173
    .local v1, "extraHeight":I
    if-nez v0, :cond_1

    move p3, v2

    .line 197
    .end local p3    # "distanceY":F
    :cond_0
    :goto_0
    return p3

    .line 177
    .restart local p3    # "distanceY":F
    :cond_1
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_2

    move p3, v2

    .line 178
    goto :goto_0

    .line 181
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    cmpl-float v3, v3, v2

    if-lez v3, :cond_4

    .line 182
    iget v3, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v3, v2

    if-lez v3, :cond_3

    move p3, v2

    .line 183
    goto :goto_0

    .line 185
    :cond_3
    iget v3, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, p3

    cmpl-float v2, v3, v2

    if-lez v2, :cond_0

    .line 186
    iget p3, v0, Landroid/graphics/RectF;->top:F

    goto :goto_0

    .line 189
    :cond_4
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v4, v1

    add-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    move p3, v2

    .line 190
    goto :goto_0

    .line 192
    :cond_5
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    int-to-float v3, v1

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 193
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    int-to-float v3, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result p3

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic clear()V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->clear()V

    return-void
.end method

.method public bridge synthetic easeOut(DDDD)D
    .locals 3
    .param p1, "x0"    # D
    .param p3, "x1"    # D
    .param p5, "x2"    # D
    .param p7, "x3"    # D

    .prologue
    .line 29
    invoke-super/range {p0 .. p8}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->easeOut(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method protected getGestureListener()Landroid/view/GestureDetector$OnGestureListener;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/FreezeImageView$GestureListener;-><init>(Lcom/sec/android/app/magnifier/FreezeImageView;)V

    return-object v0
.end method

.method public bridge synthetic getMaxZoom()F
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getMaxZoom()F

    move-result v0

    return v0
.end method

.method protected getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;-><init>(Lcom/sec/android/app/magnifier/FreezeImageView;)V

    return-object v0
.end method

.method public initFreezeView()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 78
    invoke-super {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->init()V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getGestureListener()Landroid/view/GestureDetector$OnGestureListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 83
    new-instance v0, Landroid/view/ScaleGestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 84
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 86
    iput v5, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mCurrentScaleFactor:F

    .line 87
    iput v5, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mLastScaleFactor:F

    .line 88
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 69
    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 70
    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 71
    iput-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 73
    invoke-super {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->clear()V

    .line 74
    invoke-super {p0, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setRecycler(Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;)V

    .line 75
    return-void
.end method

.method public bridge synthetic onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/view/KeyEvent;

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/view/KeyEvent;

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 100
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 102
    .local v0, "action":I
    iget-object v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mGestureDetector:Landroid/view/GestureDetector;

    if-nez v2, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v1

    .line 106
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 109
    and-int/lit16 v2, v0, 0xff

    packed-switch v2, :pswitch_data_0

    .line 130
    :cond_2
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 111
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->getScale()F

    move-result v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 112
    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/magnifier/FreezeImageView;->zoomTo(FF)V

    .line 115
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScrolling:Z

    if-eqz v2, :cond_4

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScrolling:Z

    .line 119
    :cond_4
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScrolling:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScaleActivated:Z

    if-nez v2, :cond_2

    .line 120
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScaleActivated:Z

    .line 121
    invoke-super {p0, p1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 125
    :pswitch_1
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScaleActivated:Z

    goto :goto_1

    .line 109
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onZoom(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->onZoom(F)V

    .line 136
    iput p1, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mCurrentScaleFactor:F

    .line 137
    return-void
.end method

.method public resetZoom()V
    .locals 1

    .prologue
    .line 201
    const/high16 v0, 0x3f800000    # 1.0f

    .line 202
    .local v0, "targetScale":F
    iput v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mCurrentScaleFactor:F

    .line 203
    iput v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView;->mLastScaleFactor:F

    .line 204
    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/FreezeImageView;->zoomTo(F)V

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/FreezeImageView;->invalidate()V

    .line 206
    return-void
.end method

.method public bridge synthetic scrollBy(FF)V
    .locals 0
    .param p1, "x0"    # F
    .param p2, "x1"    # F

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->scrollBy(FF)V

    return-void
.end method

.method public bridge synthetic setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "x0"    # Landroid/graphics/Bitmap;

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public bridge synthetic setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V
    .locals 0
    .param p1, "x0"    # Landroid/graphics/Bitmap;
    .param p2, "x1"    # Z

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public bridge synthetic setImageRotateBitmapResetBase(Lcom/sec/android/app/magnifier/RotateBitmap;Z)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/magnifier/RotateBitmap;
    .param p2, "x1"    # Z

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageRotateBitmapResetBase(Lcom/sec/android/app/magnifier/RotateBitmap;Z)V

    return-void
.end method

.method public bridge synthetic setRecycler(Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setRecycler(Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;)V

    return-void
.end method

.method public bridge synthetic zoomTo(FF)V
    .locals 0
    .param p1, "x0"    # F
    .param p2, "x1"    # F

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->zoomTo(FF)V

    return-void
.end method
