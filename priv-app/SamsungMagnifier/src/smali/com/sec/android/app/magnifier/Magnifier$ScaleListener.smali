.class public Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "Magnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/Magnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 10
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const v6, 0x7f070003

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 307
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mNumberOfPointer:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$400(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 360
    :goto_0
    return v9

    .line 311
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v4}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 312
    .local v3, "zoomVelocity":I
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsConstantGrowthRateZoomSupported:Z
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$500(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 313
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v4}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 314
    .local v2, "zoomThresholdfactor":I
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$600(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomMaxRatio:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$700(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    div-int/2addr v5, v2

    if-le v4, v5, :cond_1

    .line 315
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v4}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    mul-int v3, v4, v2

    .line 318
    .end local v2    # "zoomThresholdfactor":I
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->log10(D)D

    move-result-wide v4

    int-to-double v6, v3

    mul-double/2addr v4, v6

    double-to-int v0, v4

    .line 319
    .local v0, "currentCallbackValue":I
    const/4 v1, 0x0

    .line 321
    .local v1, "zoomInStaus":Z
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mInitialZoomValueOnScaleBegin:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$800(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$600(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    if-le v4, v5, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreviousCallbackValue:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$900(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    if-ge v4, v0, :cond_6

    .line 322
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mInitialZoomValueOnScaleBegin:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$800(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I
    invoke-static {v6}, Lcom/sec/android/app/magnifier/Magnifier;->access$600(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v6

    sub-int/2addr v5, v6

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1002(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 323
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$1100(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1000(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    if-le v4, v5, :cond_2

    .line 324
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1100(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1002(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 326
    :cond_2
    const/4 v1, 0x1

    .line 335
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mInitialZoomValueOnScaleBegin:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$800(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v6}, Lcom/sec/android/app/magnifier/Magnifier;->access$1000(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v6

    sub-int/2addr v5, v6

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I
    invoke-static {v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1202(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 336
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreviousCallbackValue:I
    invoke-static {v4, v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$902(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1000(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I
    invoke-static {v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1102(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 339
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$1200(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    if-gez v4, :cond_4

    .line 340
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I
    invoke-static {v4, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$1202(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 343
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$1200(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$600(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    if-le v4, v5, :cond_5

    .line 344
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$600(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I
    invoke-static {v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1202(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 349
    :cond_5
    if-eqz v1, :cond_9

    .line 350
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$300(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 351
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/16 v5, 0x15

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V
    invoke-static {v4, v5, v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1300(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 327
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mInitialZoomValueOnScaleBegin:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$800(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    add-int/2addr v4, v0

    if-gez v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreviousCallbackValue:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$900(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    if-le v4, v0, :cond_3

    .line 328
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mInitialZoomValueOnScaleBegin:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$800(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    add-int/2addr v5, v0

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1002(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 329
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$1100(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1000(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    if-ge v4, v5, :cond_7

    .line 330
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1100(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$1002(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 332
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 353
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/16 v5, 0x14

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V
    invoke-static {v4, v5, v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1300(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 355
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z
    invoke-static {v4}, Lcom/sec/android/app/magnifier/Magnifier;->access$300(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 356
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/16 v5, 0x16

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V
    invoke-static {v4, v5, v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1400(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 358
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/16 v5, 0x13

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V
    invoke-static {v4, v5, v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1400(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V

    goto/16 :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 367
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mNumberOfPointer:I
    invoke-static {v2}, Lcom/sec/android/app/magnifier/Magnifier;->access$400(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 377
    :goto_0
    return v0

    .line 371
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsScaleZoomWorking:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/magnifier/Magnifier;->access$1502(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    .line 372
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I
    invoke-static {v3}, Lcom/sec/android/app/magnifier/Magnifier;->access$1200(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v3

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mInitialZoomValueOnScaleBegin:I
    invoke-static {v2, v3}, Lcom/sec/android/app/magnifier/Magnifier;->access$802(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I
    invoke-static {v2, v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$1002(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 374
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I
    invoke-static {v2, v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$1102(Lcom/sec/android/app/magnifier/Magnifier;I)I

    .line 375
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mPreviousCallbackValue:I
    invoke-static {v2, v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$902(Lcom/sec/android/app/magnifier/Magnifier;I)I

    move v0, v1

    .line 377
    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mNumberOfPointer:I
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$400(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 389
    :goto_0
    return-void

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsScaleZoomWorking:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->access$1502(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    goto :goto_0
.end method
