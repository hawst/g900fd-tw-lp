.class Lcom/sec/android/app/magnifier/Magnifier$2;
.super Landroid/content/BroadcastReceiver;
.source "Magnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/Magnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0

    .prologue
    .line 586
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 588
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 590
    .local v0, "action":Ljava/lang/String;
    const-string v8, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 591
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # invokes: Lcom/sec/android/app/magnifier/Magnifier;->handleBatteryChanged(Landroid/content/Intent;)V
    invoke-static {v8, p2}, Lcom/sec/android/app/magnifier/Magnifier;->access$1800(Lcom/sec/android/app/magnifier/Magnifier;Landroid/content/Intent;)V

    .line 743
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    const-string v8, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 593
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->battLevel:I
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1900(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v8

    const/4 v9, 0x5

    if-gt v8, v9, :cond_0

    .line 594
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v8}, Lcom/sec/android/app/magnifier/Magnifier;->handleLowBattery()V

    goto :goto_0

    .line 596
    :cond_2
    const-string v8, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 597
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->battLevel:I
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1900(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v8

    const/4 v9, 0x5

    if-gt v8, v9, :cond_3

    .line 598
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v8}, Lcom/sec/android/app/magnifier/Magnifier;->handleLowBattery()V

    goto :goto_0

    .line 600
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v8, v8, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v8, v8, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 601
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v8, v8, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->dismiss()V

    .line 603
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x0

    iput-object v9, v8, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    goto :goto_0

    .line 605
    :cond_5
    const-string v8, "android.intent.action.ACTION_ASSISTIVE_WIDGET_STATE_CHANGE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 606
    const-string v8, "quicktool"

    const-string v9, "from"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 607
    const-string v8, "value"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 608
    .local v7, "torchValue":Z
    const-string v8, "MagnifierActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "torchValue - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    if-nez v7, :cond_8

    .line 612
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 613
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 614
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 616
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020003

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 617
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020003

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 618
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 619
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 621
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2402(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    .line 622
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-boolean v8, v8, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    if-nez v8, :cond_7

    .line 623
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v8, :cond_0

    .line 624
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v8}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v5

    .line 625
    .local v5, "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    const-string v8, "off"

    invoke-virtual {v5, v8}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 626
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v8, v5}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    goto/16 :goto_0

    .line 629
    .end local v5    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x0

    iput-boolean v9, v8, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    goto/16 :goto_0

    .line 631
    :cond_8
    if-eqz v7, :cond_0

    .line 633
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 634
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 635
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 637
    :cond_9
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020004

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 638
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020004

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 639
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 640
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 642
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2402(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    .line 643
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x1

    iput-boolean v9, v8, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    goto/16 :goto_0

    .line 646
    .end local v7    # "torchValue":Z
    :cond_a
    const-string v8, "android.intent.action.ACTION_ASSISTIVE_OFF"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b

    const-string v8, "android.intent.action.ASSISTIVELIGHT_OFF"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 647
    :cond_b
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_c

    .line 648
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 649
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 651
    :cond_c
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020003

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 652
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020003

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 653
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 654
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 656
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2402(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    .line 657
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-boolean v8, v8, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    if-nez v8, :cond_d

    .line 658
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v8, :cond_0

    .line 659
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v8}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v5

    .line 660
    .restart local v5    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    const-string v8, "off"

    invoke-virtual {v5, v8}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 661
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v8, v5}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    goto/16 :goto_0

    .line 664
    .end local v5    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_d
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x0

    iput-boolean v9, v8, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    goto/16 :goto_0

    .line 666
    :cond_e
    const-string v8, "android.intent.action.ASSISTIVELIGHT_ON"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 667
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_f

    .line 668
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 669
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 671
    :cond_f
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020004

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 672
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020004

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 673
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 674
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 676
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2402(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    .line 677
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x1

    iput-boolean v9, v8, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    goto/16 :goto_0

    .line 678
    :cond_10
    const-string v8, "android.intent.action.SIOP_LEVEL_CHANGED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_17

    .line 679
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 680
    .local v4, "myExtras":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 681
    .local v1, "bLimitFlash":Z
    if-eqz v4, :cond_14

    .line 682
    const-string v8, "flash_led_disable"

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 687
    :goto_1
    const-string v8, "MagnifierActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "flash_led_disable : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    if-eqz v1, :cond_16

    .line 690
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v8}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "torch_light"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-eqz v8, :cond_15

    const/4 v6, 0x1

    .line 692
    .local v6, "result":Z
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsTemperatureHighToUseFlash:Z
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2500(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v8

    if-nez v8, :cond_0

    if-nez v6, :cond_0

    .line 693
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v8, :cond_11

    .line 694
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v8}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v5

    .line 695
    .restart local v5    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    const-string v8, "off"

    invoke-virtual {v5, v8}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 696
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;

    move-result-object v8

    iget-object v8, v8, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v8, v5}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 698
    .end local v5    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_11
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_12

    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_12

    .line 699
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 700
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 702
    :cond_12
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020003

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 703
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    const v9, 0x7f020003

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 704
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 705
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 707
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2600(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/Toast;

    move-result-object v8

    if-nez v8, :cond_13

    .line 708
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v9, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v9}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f080012

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2602(Lcom/sec/android/app/magnifier/Magnifier;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 710
    :cond_13
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2600(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 711
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsTemperatureHighToUseFlash:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2502(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    .line 712
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2402(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    goto/16 :goto_0

    .line 684
    .end local v6    # "result":Z
    :cond_14
    const-string v8, "MagnifierActivity"

    const-string v9, "Extra is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 690
    :cond_15
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 715
    :cond_16
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mIsTemperatureHighToUseFlash:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2502(Lcom/sec/android/app/magnifier/Magnifier;Z)Z

    goto/16 :goto_0

    .line 717
    .end local v1    # "bLimitFlash":Z
    .end local v4    # "myExtras":Landroid/os/Bundle;
    :cond_17
    const-string v8, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_19

    .line 718
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 719
    .restart local v4    # "myExtras":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    const-string v8, "mode"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 721
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2700(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/Toast;

    move-result-object v8

    if-nez v8, :cond_18

    .line 722
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v9, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v9}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f080013

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2702(Lcom/sec/android/app/magnifier/Magnifier;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 724
    :cond_18
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2700(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 725
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v8}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V

    goto/16 :goto_0

    .line 727
    .end local v4    # "myExtras":Landroid/os/Bundle;
    :cond_19
    const-string v8, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1b

    .line 729
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2700(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/Toast;

    move-result-object v8

    if-nez v8, :cond_1a

    .line 730
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v9, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v9}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f080013

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;
    invoke-static {v8, v9}, Lcom/sec/android/app/magnifier/Magnifier;->access$2702(Lcom/sec/android/app/magnifier/Magnifier;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 732
    :cond_1a
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/android/app/magnifier/Magnifier;->access$2700(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 733
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v8}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V

    goto/16 :goto_0

    .line 734
    :cond_1b
    const-string v8, "com.samsung.cover.OPEN"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 735
    const-string v8, "coverOpen"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 736
    .local v2, "isOpen":Z
    if-nez v2, :cond_0

    .line 737
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 738
    .local v3, "launchDummyActivity":Landroid/content/Intent;
    const-string v8, "com.sec.android.app.magnifier"

    const-string v9, "com.sec.android.app.magnifier.DummyActivity"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 739
    const/high16 v8, 0x40000000    # 2.0f

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 740
    iget-object v8, p0, Lcom/sec/android/app/magnifier/Magnifier$2;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-virtual {v8, v3}, Lcom/sec/android/app/magnifier/Magnifier;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
