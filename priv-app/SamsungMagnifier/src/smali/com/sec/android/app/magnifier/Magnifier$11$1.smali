.class Lcom/sec/android/app/magnifier/Magnifier$11$1;
.super Ljava/lang/Object;
.source "Magnifier.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/magnifier/Magnifier$11;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/magnifier/Magnifier$11;


# direct methods
.method constructor <init>(Lcom/sec/android/app/magnifier/Magnifier$11;)V
    .locals 0

    .prologue
    .line 2206
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$11$1;->this$1:Lcom/sec/android/app/magnifier/Magnifier$11;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2208
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11$1;->this$1:Lcom/sec/android/app/magnifier/Magnifier$11;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->bPauseAudioPlayback:Z
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3800(Lcom/sec/android/app/magnifier/Magnifier;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2209
    const-string v0, "MagnifierActivity"

    const-string v1, "resumeAudioPlayback after focus"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2210
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11$1;->this$1:Lcom/sec/android/app/magnifier/Magnifier$11;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3900(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/media/AudioManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2211
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11$1;->this$1:Lcom/sec/android/app/magnifier/Magnifier$11;

    iget-object v1, v0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11$1;->this$1:Lcom/sec/android/app/magnifier/Magnifier$11;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    # setter for: Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1, v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3902(Lcom/sec/android/app/magnifier/Magnifier;Landroid/media/AudioManager;)Landroid/media/AudioManager;

    .line 2213
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11$1;->this$1:Lcom/sec/android/app/magnifier/Magnifier$11;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3900(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$11$1;->this$1:Lcom/sec/android/app/magnifier/Magnifier$11;

    iget-object v1, v1, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v1, v1, Lcom/sec/android/app/magnifier/Magnifier;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2215
    :cond_1
    return-void
.end method
