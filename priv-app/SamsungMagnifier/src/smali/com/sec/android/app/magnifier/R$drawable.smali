.class public final Lcom/sec/android/app/magnifier/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final focus_indicator:I = 0x7f020000

.field public static final focus_indicator_bg:I = 0x7f020001

.field public static final icon_back_sel:I = 0x7f020002

.field public static final icon_flash_off_sel:I = 0x7f020003

.field public static final icon_flash_on_sel:I = 0x7f020004

.field public static final icon_focus_sel:I = 0x7f020005

.field public static final icon_freeze_sel:I = 0x7f020006

.field public static final icon_save_sel:I = 0x7f020007

.field public static final icon_zoom_minus_land:I = 0x7f020008

.field public static final icon_zoom_minus_port:I = 0x7f020009

.field public static final icon_zoom_plus_land:I = 0x7f02000a

.field public static final icon_zoom_plus_port:I = 0x7f02000b

.field public static final magnifier_icon_back_normal:I = 0x7f02000c

.field public static final magnifier_icon_back_press:I = 0x7f02000d

.field public static final magnifier_icon_capture_normal:I = 0x7f02000e

.field public static final magnifier_icon_capture_press:I = 0x7f02000f

.field public static final magnifier_icon_focus_normal:I = 0x7f020010

.field public static final magnifier_icon_focus_press:I = 0x7f020011

.field public static final magnifier_icon_light_off:I = 0x7f020012

.field public static final magnifier_icon_light_off_press:I = 0x7f020013

.field public static final magnifier_icon_light_on:I = 0x7f020014

.field public static final magnifier_icon_out:I = 0x7f020015

.field public static final magnifier_icon_out_h:I = 0x7f020016

.field public static final magnifier_icon_out_press:I = 0x7f020017

.field public static final magnifier_icon_out_press_h:I = 0x7f020018

.field public static final magnifier_icon_save_normal:I = 0x7f020019

.field public static final magnifier_icon_save_press:I = 0x7f02001a

.field public static final magnifier_icon_zoom:I = 0x7f02001b

.field public static final magnifier_icon_zoom_h:I = 0x7f02001c

.field public static final magnifier_icon_zoom_press:I = 0x7f02001d

.field public static final magnifier_icon_zoom_press_h:I = 0x7f02001e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
