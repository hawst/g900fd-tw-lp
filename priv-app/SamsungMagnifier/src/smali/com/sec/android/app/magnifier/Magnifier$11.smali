.class Lcom/sec/android/app/magnifier/Magnifier$11;
.super Ljava/lang/Object;
.source "Magnifier.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/magnifier/Magnifier;->playCameraSound(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/Magnifier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0

    .prologue
    .line 2201
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 2203
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraStreamId:[I
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3200(Lcom/sec/android/app/magnifier/Magnifier;)[I

    move-result-object v7

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundId:I
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3300(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v8

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3700(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/media/SoundPool;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPoolId:[I
    invoke-static {v1}, Lcom/sec/android/app/magnifier/Magnifier;->access$3400(Lcom/sec/android/app/magnifier/Magnifier;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundId:I
    invoke-static {v2}, Lcom/sec/android/app/magnifier/Magnifier;->access$3300(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v2

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraStreamVolume:F
    invoke-static {v2}, Lcom/sec/android/app/magnifier/Magnifier;->access$3500(Lcom/sec/android/app/magnifier/Magnifier;)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraStreamVolume:F
    invoke-static {v3}, Lcom/sec/android/app/magnifier/Magnifier;->access$3500(Lcom/sec/android/app/magnifier/Magnifier;)F

    move-result v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundLoop:I
    invoke-static {v5}, Lcom/sec/android/app/magnifier/Magnifier;->access$3600(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v0

    aput v0, v7, v8

    .line 2205
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundId:I
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3300(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    # getter for: Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundId:I
    invoke-static {v0}, Lcom/sec/android/app/magnifier/Magnifier;->access$3300(Lcom/sec/android/app/magnifier/Magnifier;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2206
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier$11;->this$0:Lcom/sec/android/app/magnifier/Magnifier;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/magnifier/Magnifier$11$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/magnifier/Magnifier$11$1;-><init>(Lcom/sec/android/app/magnifier/Magnifier$11;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2218
    :cond_1
    return-void
.end method
