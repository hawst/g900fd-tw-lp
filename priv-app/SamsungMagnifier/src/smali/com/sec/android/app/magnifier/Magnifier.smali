.class public Lcom/sec/android/app/magnifier/Magnifier;
.super Landroid/app/Activity;
.source "Magnifier.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;,
        Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;,
        Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;
    }
.end annotation


# static fields
.field protected static final AF_FAIL_SOUND:I = 0x1

.field protected static final AF_SUCCESS_SOUND:I = 0x0

.field protected static final AF_WAIT_TIMEOUT:I = 0xbb8

.field protected static final AF_WAIT_TIMER_EXPIRED:I = 0x2

.field public static final CAMERA_ANTI_BANDING_50HZ:Ljava/lang/String; = "50hz"

.field public static final CAMERA_ANTI_BANDING_60HZ:Ljava/lang/String; = "60hz"

.field public static final CAMERA_ANTI_BANDING_AUTO:Ljava/lang/String; = "auto"

.field public static final CAMERA_ANTI_BANDING_OFF:Ljava/lang/String; = "off"

.field protected static final CHECK_CALL_DLG:I = 0x0

.field protected static final CHECK_CAMERA_DLG:I = 0x1

.field protected static final DELAY_TO_HIDE_ZOOM_VALUE:I = 0x3e8

.field protected static final DELAY_TO_HIDE_ZOOM_VALUE_AFTER_AF:I = 0xa

.field protected static final DLG_HIDE:Z = false

.field protected static final DLG_SHOW:Z = true

.field public static final ERROR_CAMERA_OPEN:I = -0x1

.field public static final FORCED_SHUTTER_SOUND_OFF:I = 0x0

.field public static final FORCED_SHUTTER_SOUND_ON:I = 0x1

.field protected static final FREEZE_SOUND:I = 0x2

.field protected static final HIDE_ZOOM_VALUE:I = 0x3

.field protected static final HIDE_ZOOM_VALUE_AFTER_AF:I = 0x4

.field protected static final INACTIVITY_TIMEOUT:I = 0x78

.field protected static final INACTIVITY_TIMER_EXPIRED:I = 0x1

.field public static final KEY_ZOOM_VALUE:Ljava/lang/String; = "key_zoom"

.field public static final MAX_FREEZE_SCALE_ZOOM:F = 10.0f

.field public static final MIN_FREEZE_SCALE_ZOOM:F = 1.0f

.field protected static final NUM_OF_DLG:I = 0x2

.field private static final NUM_POINTER_ALLOWED_FOR_PINCH:I = 0x2

.field private static final NUM_SHUTTERSOUND_CHANNELS:I = 0x3

.field public static final RES_AUTOFOCUS_CANCELED:I = 0x2

.field public static final RES_AUTOFOCUS_FAIL:I = 0x0

.field public static final RES_AUTOFOCUS_FOCUSING:I = 0x3

.field public static final RES_AUTOFOCUS_RESTART:I = 0x4

.field public static final RES_AUTOFOCUS_SUCCESS:I = 0x1

.field public static final TAG:Ljava/lang/String; = "MagnifierActivity"

.field public static TALKBACK_FLASH_OFF:Ljava/lang/String; = null

.field public static TALKBACK_FLASH_ON:Ljava/lang/String; = null

.field private static final ZOOM_LEVEL:I = 0x5

.field public static final ZOOM_PREFERENCES:Ljava/lang/String; = "com.sec.android.app.magnifier"

.field private static final mFreezeZoomMax:I = 0x64

.field private static final mFreezeZoomSensitizing:I = 0xa


# instance fields
.field private bPauseAudioPlayback:Z

.field private battLevel:I

.field private battScale:I

.field private isLandscape:Z

.field private isLightOn:Z

.field protected mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAutoFocusCallback:Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;

.field mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mButtonBackLand:Landroid/view/View;

.field private mButtonBackPort:Landroid/view/View;

.field private mButtonFocusLand:Landroid/view/View;

.field private mButtonFocusPort:Landroid/view/View;

.field private mButtonFreezeLand:Landroid/view/View;

.field private mButtonFreezePort:Landroid/view/View;

.field private mButtonLightLand:Landroid/view/View;

.field private mButtonLightPort:Landroid/view/View;

.field private mButtonSaveLand:Landroid/view/View;

.field private mButtonSavePort:Landroid/view/View;

.field private mCallbackBitmap:Landroid/graphics/Bitmap;

.field protected mCameraOpenCheck:Z

.field private mCameraSoundId:I

.field private mCameraSoundLoop:I

.field private mCameraSoundPool:Landroid/media/SoundPool;

.field private mCameraSoundPoolId:[I

.field private mCameraStreamId:[I

.field private mCameraStreamVolume:F

.field protected mCheckCalling:Z

.field protected mCheckVTCalling:Z

.field protected mCheckVoIPCalling:Z

.field private mContainer:Landroid/widget/FrameLayout;

.field private mDlgStatus:[Z

.field private mFreezeMinusIcon:Landroid/widget/ImageView;

.field private mFreezeMode:Z

.field private mFreezePlusIcon:Landroid/widget/ImageView;

.field private mFreezeSeekBarChange:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

.field private mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

.field private mFreezeZoomValue:I

.field protected mHandler:Landroid/os/Handler;

.field private mImgLightLand:Landroid/widget/ImageView;

.field private mImgLightPort:Landroid/widget/ImageView;

.field private mInitialZoomValueOnScaleBegin:I

.field private mIsConstantGrowthRateZoomSupported:Z

.field private mIsDestroying:Z

.field private mIsScaleZoomWorking:Z

.field private mIsTemperatureHighToUseFlash:Z

.field private mIsTouchAFStarted:Z

.field private mLandFreezeLayout:Landroid/widget/FrameLayout;

.field private mLandLayout:Landroid/widget/FrameLayout;

.field private mLastOrientation:I

.field private mLimitFlashToast:Landroid/widget/Toast;

.field protected mLowBatteryPopup:Landroid/app/AlertDialog;

.field private mMinusIcon:Landroid/widget/ImageView;

.field private mNumberOfPointer:I

.field mOEL:Landroid/view/OrientationEventListener;

.field private mOverValue:I

.field private mPlayCameraSoundThread:Ljava/lang/Thread;

.field private mPlusIcon:Landroid/widget/ImageView;

.field private mPortFreezeLayout:Landroid/widget/FrameLayout;

.field private mPortLayout:Landroid/widget/FrameLayout;

.field private mPreOvervalue:I

.field private mPreviousCallbackValue:I

.field private mSavedToast:Landroid/widget/Toast;

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field protected mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field private mSecurityToast:Landroid/widget/Toast;

.field private mSeekBarChange:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSeekBarContainerLand:Landroid/widget/LinearLayout;

.field private mSeekBarContainerPort:Landroid/widget/LinearLayout;

.field private mSeekBarFreezeContainerLand:Landroid/widget/LinearLayout;

.field private mSeekBarFreezeContainerPort:Landroid/widget/LinearLayout;

.field private mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

.field private mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

.field private mSeekBarKeyListener:Landroid/view/View$OnKeyListener;

.field private mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

.field private mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

.field private mSideSyncToast:Landroid/widget/Toast;

.field private mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

.field public mTorchByQuickTool:Z

.field private mTxtLightLand:Landroid/widget/TextView;

.field private mTxtLightPort:Landroid/widget/TextView;

.field private mZoomMax:I

.field private mZoomMaxRatio:I

.field private mZoomNumLand:Landroid/widget/TextView;

.field private mZoomNumPort:Landroid/widget/TextView;

.field private mZoomPlusLand:Landroid/widget/TextView;

.field private mZoomPlusPort:Landroid/widget/TextView;

.field private mZoomSensitizing:I

.field private mZoomValue:I

.field mZoom_prefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 83
    const/4 v0, 0x2

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mDlgStatus:[Z

    .line 140
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    .line 143
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    .line 144
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomSensitizing:I

    .line 145
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    .line 146
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMaxRatio:I

    .line 152
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 158
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    .line 159
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    .line 160
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    .line 161
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    .line 163
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOEL:Landroid/view/OrientationEventListener;

    .line 164
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLastOrientation:I

    .line 166
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckCalling:Z

    .line 167
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVoIPCalling:Z

    .line 168
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVTCalling:Z

    .line 169
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraOpenCheck:Z

    .line 171
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSavedToast:Landroid/widget/Toast;

    .line 172
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    .line 173
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;

    .line 174
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;

    .line 185
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPoolId:[I

    .line 186
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraStreamId:[I

    .line 192
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    .line 193
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->bPauseAudioPlayback:Z

    .line 198
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mNumberOfPointer:I

    .line 200
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsScaleZoomWorking:Z

    .line 201
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsConstantGrowthRateZoomSupported:Z

    .line 203
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPreviousCallbackValue:I

    .line 204
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I

    .line 205
    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I

    .line 223
    iput v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->battScale:I

    .line 224
    iput v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->battLevel:I

    .line 226
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsDestroying:Z

    .line 227
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    .line 228
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTemperatureHighToUseFlash:Z

    .line 231
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    .line 237
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;-><init>(Lcom/sec/android/app/magnifier/Magnifier;Lcom/sec/android/app/magnifier/Magnifier$1;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAutoFocusCallback:Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;

    .line 586
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/Magnifier$2;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1192
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/Magnifier$7;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarChange:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1209
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/Magnifier$8;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeSeekBarChange:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1260
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/Magnifier$9;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 1836
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/Magnifier$10;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    .line 2224
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/Magnifier$12;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method private CannotStartCamera()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 880
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckCalling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVoIPCalling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVTCalling:Z

    if-eqz v0, :cond_1

    .line 881
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->showDlg(I)V

    .line 890
    :goto_0
    return-void

    .line 883
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraOpenCheck:Z

    if-eqz v0, :cond_2

    .line 884
    invoke-direct {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->showDlg(I)V

    goto :goto_0

    .line 886
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->hideDlg(I)V

    .line 887
    invoke-direct {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->hideDlg(I)V

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/android/app/magnifier/Magnifier;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/magnifier/Magnifier;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOverValue:I

    return p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/magnifier/Magnifier;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/magnifier/Magnifier;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPreOvervalue:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/magnifier/Magnifier;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/magnifier/Magnifier;ILandroid/view/KeyEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V

    return-void
.end method

.method static synthetic access$1502(Lcom/sec/android/app/magnifier/Magnifier;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsScaleZoomWorking:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/magnifier/Magnifier;)Lcom/sec/android/app/magnifier/MagnifierSurface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/magnifier/Magnifier;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/app/magnifier/Magnifier;->setLastOrientation(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/magnifier/Magnifier;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/app/magnifier/Magnifier;->handleBatteryChanged(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->battLevel:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/magnifier/Magnifier;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/magnifier/Magnifier;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/magnifier/Magnifier;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTemperatureHighToUseFlash:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/magnifier/Magnifier;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTemperatureHighToUseFlash:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/magnifier/Magnifier;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/magnifier/Magnifier;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/magnifier/Magnifier;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/magnifier/Magnifier;->setCameraZoom(IZ)V

    return-void
.end method

.method static synthetic access$2902(Lcom/sec/android/app/magnifier/Magnifier;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/magnifier/Magnifier;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/magnifier/Magnifier;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->CannotStartCamera()V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/android/app/magnifier/Magnifier;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraStreamId:[I

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundId:I

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/magnifier/Magnifier;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPoolId:[I

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/magnifier/Magnifier;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraStreamVolume:F

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundLoop:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/magnifier/Magnifier;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->bPauseAudioPlayback:Z

    return v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/magnifier/Magnifier;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/magnifier/Magnifier;Landroid/media/AudioManager;)Landroid/media/AudioManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # Landroid/media/AudioManager;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mNumberOfPointer:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/magnifier/Magnifier;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsConstantGrowthRateZoomSupported:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMaxRatio:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mInitialZoomValueOnScaleBegin:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/magnifier/Magnifier;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mInitialZoomValueOnScaleBegin:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/magnifier/Magnifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPreviousCallbackValue:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/magnifier/Magnifier;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/magnifier/Magnifier;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPreviousCallbackValue:I

    return p1
.end method

.method private checkCameraStartCondition_Call()Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 988
    const-string v4, "MagnifierActivity"

    const-string v5, "checkCameraStartCondition_Call"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    iput-boolean v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckCalling:Z

    .line 992
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/magnifier/Magnifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 993
    .local v1, "mTelephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v4

    if-eq v4, v6, :cond_0

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v4

    if-ne v4, v2, :cond_1

    .line 996
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckCalling:Z

    .line 1015
    :goto_0
    return v2

    .line 1009
    :cond_1
    invoke-static {v2}, Lcom/samsung/android/telephony/MultiSimManager;->getCallState(I)I

    move-result v0

    .line 1010
    .local v0, "callState":I
    if-eq v0, v6, :cond_2

    if-ne v0, v2, :cond_3

    .line 1011
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckCalling:Z

    goto :goto_0

    :cond_3
    move v2, v3

    .line 1015
    goto :goto_0
.end method

.method private checkCameraStartCondition_Security()Z
    .locals 2

    .prologue
    .line 1070
    const-string v1, "persist.sys.camera_lock"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1071
    .local v0, "dev_camera_lock_state":Ljava/lang/String;
    const-string v1, "camera_lock.enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1072
    const/4 v1, 0x1

    .line 1074
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkCameraStartCondition_VT()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1050
    const-string v1, "MagnifierActivity"

    const-string v3, "checkCameraStartCondition_VT"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    iput-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVTCalling:Z

    .line 1054
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1055
    .local v0, "mTelephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_1

    .line 1056
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isVideoCall()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->isVTCallRunning()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVTCalling:Z

    .line 1057
    const-string v1, "MagnifierActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mCheckVTCalling = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVTCalling:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1058
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVTCalling:Z

    if-eqz v1, :cond_1

    .line 1059
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVTCalling:Z

    .line 1062
    :cond_1
    return v2

    :cond_2
    move v1, v2

    .line 1056
    goto :goto_0
.end method

.method private checkCameraStartCondition_VoIPCall()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1019
    const-string v0, "MagnifierActivity"

    const-string v1, "checkCameraStartCondition_VoIPCall"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    iput-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVoIPCalling:Z

    .line 1032
    return v2
.end method

.method private getConstantGrowthRateZoomSupported(Lcom/sec/android/seccamera/SecCamera$Parameters;)Z
    .locals 2
    .param p1, "params"    # Lcom/sec/android/seccamera/SecCamera$Parameters;

    .prologue
    .line 2287
    const-string v0, "true"

    const-string v1, "constant-growth-rate-zoom-supported"

    invoke-virtual {p1, v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private handleBatteryChanged(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2321
    const-string v1, "status"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2322
    .local v0, "battStatus":I
    const-string v1, "scale"

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->battScale:I

    .line 2323
    const-string v1, "level"

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->battScale:I

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->battLevel:I

    .line 2325
    const-string v1, "MagnifierActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleBatteryChanged : Level = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->battLevel:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->battScale:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2327
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->battLevel:I

    const/4 v2, 0x5

    if-gt v1, v2, :cond_0

    .line 2328
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->handleLowBattery()V

    .line 2330
    :cond_0
    return-void
.end method

.method private hideDlg(I)V
    .locals 3
    .param p1, "nId"    # I

    .prologue
    .line 903
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 904
    if-ne v0, p1, :cond_0

    .line 905
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mDlgStatus:[Z

    aget-boolean v1, v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 906
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mDlgStatus:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 907
    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/Magnifier;->dismissDialog(I)V

    .line 903
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 911
    :cond_1
    return-void
.end method

.method private initIntentFilter()V
    .locals 6

    .prologue
    .line 2368
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 2369
    .local v2, "intentFilterBattery":Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2370
    const-string v5, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2371
    const-string v5, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2372
    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v2}, Lcom/sec/android/app/magnifier/Magnifier;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2375
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 2376
    .local v3, "intentFilterSIOP":Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.SIOP_LEVEL_CHANGED"

    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2377
    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v3}, Lcom/sec/android/app/magnifier/Magnifier;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2382
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2383
    .local v0, "intentAssistiveLight":Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.ASSISTIVELIGHT_ON"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2384
    const-string v5, "android.intent.action.ASSISTIVELIGHT_OFF"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2385
    const-string v5, "android.intent.action.ACTION_ASSISTIVE_OFF"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2386
    const-string v5, "android.intent.action.ACTION_ASSISTIVE_WIDGET_STATE_CHANGE"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2387
    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v0}, Lcom/sec/android/app/magnifier/Magnifier;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2392
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 2393
    .local v4, "intentSideSync":Landroid/content/IntentFilter;
    const-string v5, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2394
    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v4}, Lcom/sec/android/app/magnifier/Magnifier;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2399
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 2400
    .local v1, "intentCover":Landroid/content/IntentFilter;
    const-string v5, "com.samsung.cover.OPEN"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2401
    iget-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v1}, Lcom/sec/android/app/magnifier/Magnifier;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2403
    return-void
.end method

.method private initMagnifierActivity()V
    .locals 9

    .prologue
    const v8, 0x7f080005

    const/16 v5, 0x64

    const/4 v4, 0x3

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 755
    const-string v2, "MagnifierActivity"

    const-string v3, "initMagnifierActivity()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    const v2, 0x7f040002

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->setContentView(I)V

    .line 759
    new-instance v2, Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/magnifier/MagnifierSurface;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    .line 761
    const v2, 0x7f0a001a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    .line 763
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 764
    .local v0, "paint":Landroid/graphics/Paint;
    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 765
    const/16 v2, 0x3c

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 768
    const v2, 0x7f0a003b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFocusPort:Landroid/view/View;

    .line 769
    const v2, 0x7f0a0021

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFocusLand:Landroid/view/View;

    .line 770
    const v2, 0x7f0a0037

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;

    .line 771
    const v2, 0x7f0a0025

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;

    .line 772
    const v2, 0x7f0a0033

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFreezePort:Landroid/view/View;

    .line 773
    const v2, 0x7f0a0029

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFreezeLand:Landroid/view/View;

    .line 775
    const v2, 0x7f0a0038

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    .line 776
    const v2, 0x7f0a0026

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    .line 777
    const v2, 0x7f0a003a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mTxtLightPort:Landroid/widget/TextView;

    .line 778
    const v2, 0x7f0a0028

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mTxtLightLand:Landroid/widget/TextView;

    .line 780
    const v2, 0x7f0a0031

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomPlusPort:Landroid/widget/TextView;

    .line 781
    const v2, 0x7f0a001f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomPlusLand:Landroid/widget/TextView;

    .line 783
    const v2, 0x7f0a0032

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomNumPort:Landroid/widget/TextView;

    .line 784
    const v2, 0x7f0a0020

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomNumLand:Landroid/widget/TextView;

    .line 786
    const v2, 0x7f0a0041

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    .line 787
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarChange:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 788
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 789
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setFocusable(Z)V

    .line 791
    const v2, 0x7f0a002f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    .line 792
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setMode(I)V

    .line 793
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarChange:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 794
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 795
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setFocusable(Z)V

    .line 797
    const v2, 0x7f0a003f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarContainerPort:Landroid/widget/LinearLayout;

    .line 798
    const v2, 0x7f0a002d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarContainerLand:Landroid/widget/LinearLayout;

    .line 799
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarContainerPort:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 800
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarContainerLand:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 802
    const v2, 0x7f0a001b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    .line 803
    const v2, 0x7f0a001c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    .line 806
    const v2, 0x7f0a0012

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonSavePort:Landroid/view/View;

    .line 807
    const v2, 0x7f0a0001

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonSaveLand:Landroid/view/View;

    .line 808
    const v2, 0x7f0a000e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonBackPort:Landroid/view/View;

    .line 809
    const v2, 0x7f0a0005

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonBackLand:Landroid/view/View;

    .line 811
    const v2, 0x7f0a0018

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    .line 812
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeSeekBarChange:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 813
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 814
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setFocusable(Z)V

    .line 816
    const v2, 0x7f0a000b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    .line 817
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setMode(I)V

    .line 818
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeSeekBarChange:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 819
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 820
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setFocusable(Z)V

    .line 822
    const v2, 0x7f0a0016

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeContainerPort:Landroid/widget/LinearLayout;

    .line 823
    const v2, 0x7f0a0009

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeContainerLand:Landroid/widget/LinearLayout;

    .line 824
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeContainerPort:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 825
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeContainerLand:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 827
    const v2, 0x7f0a001d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortFreezeLayout:Landroid/widget/FrameLayout;

    .line 828
    const v2, 0x7f0a001e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandFreezeLayout:Landroid/widget/FrameLayout;

    .line 830
    const v2, 0x7f0a000d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/magnifier/FreezeImageView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    .line 831
    const/high16 v2, 0x7f0a0000

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/magnifier/FreezeImageView;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    .line 834
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v2, v2, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v2, :cond_0

    .line 835
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v2, v2, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v1

    .line 836
    .local v1, "param":Lcom/sec/android/seccamera/SecCamera$Parameters;
    invoke-direct {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->getConstantGrowthRateZoomSupported(Lcom/sec/android/seccamera/SecCamera$Parameters;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsConstantGrowthRateZoomSupported:Z

    .line 838
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsConstantGrowthRateZoomSupported:Z

    if-eqz v2, :cond_1

    .line 839
    invoke-virtual {v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getMaxZoom()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    .line 840
    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    iget v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    rem-int/lit8 v3, v3, 0x5

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    .line 841
    invoke-virtual {v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getZoomRatios()Ljava/util/List;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMaxRatio:I

    .line 846
    :goto_0
    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    div-int/lit8 v2, v2, 0x5

    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomSensitizing:I

    .line 850
    .end local v1    # "param":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setMax(I)V

    .line 851
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setMax(I)V

    .line 852
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setMax(I)V

    .line 853
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setMax(I)V

    .line 856
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFocusPort:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 857
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFocusLand:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 858
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 859
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 860
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFreezePort:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 861
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFreezeLand:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 862
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonSavePort:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 863
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonSaveLand:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 864
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonBackPort:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 865
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonBackLand:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 867
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/sec/android/app/magnifier/Magnifier;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08000e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/magnifier/Magnifier;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    .line 868
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/sec/android/app/magnifier/Magnifier;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08000d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/magnifier/Magnifier;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    .line 870
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v5, 0x11

    invoke-direct {v4, v7, v7, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3, v6, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 874
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->setConfigurationLayout(I)V

    .line 875
    return-void

    .line 843
    .restart local v1    # "param":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f070000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    .line 844
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMaxRatio:I

    goto/16 :goto_0
.end method

.method private isVTCallRunning()Z
    .locals 6

    .prologue
    .line 1036
    const-string v4, "activity"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/magnifier/Magnifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1037
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const v4, 0x7fffffff

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 1038
    .local v1, "activitys":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    .line 1039
    .local v3, "isActivityFound":Z
    if-eqz v1, :cond_1

    .line 1040
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 1041
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ComponentInfo{com.android.phone/com.android.phone.InVTCallScreen}"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1042
    const/4 v3, 0x1

    .line 1040
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1046
    .end local v2    # "i":I
    :cond_1
    return v3
.end method

.method private onViewFocusVisible(I)V
    .locals 2
    .param p1, "viewID"    # I

    .prologue
    .line 1587
    const/4 v0, 0x0

    .line 1589
    .local v0, "view":Landroid/view/View;
    sparse-switch p1, :sswitch_data_0

    .line 1642
    :cond_0
    :goto_0
    return-void

    .line 1592
    :sswitch_0
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0a0021

    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1593
    if-eqz v0, :cond_0

    .line 1594
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 1592
    :cond_1
    const v1, 0x7f0a003b

    goto :goto_1

    .line 1599
    :sswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0a0025

    :goto_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1600
    if-eqz v0, :cond_0

    .line 1601
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 1599
    :cond_2
    const v1, 0x7f0a0037

    goto :goto_2

    .line 1606
    :sswitch_2
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v1, :cond_3

    const v1, 0x7f0a0029

    :goto_3
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1607
    if-eqz v0, :cond_0

    .line 1608
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 1606
    :cond_3
    const v1, 0x7f0a0033

    goto :goto_3

    .line 1613
    :sswitch_3
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v1, :cond_4

    const v1, 0x7f0a0001

    :goto_4
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1614
    if-eqz v0, :cond_0

    .line 1615
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 1613
    :cond_4
    const v1, 0x7f0a0012

    goto :goto_4

    .line 1620
    :sswitch_4
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v1, :cond_5

    const v1, 0x7f0a0005

    :goto_5
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1621
    if-eqz v0, :cond_0

    .line 1622
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 1620
    :cond_5
    const v1, 0x7f0a000e

    goto :goto_5

    .line 1627
    :sswitch_5
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v1, :cond_6

    const v1, 0x7f0a002e

    :goto_6
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1628
    if-eqz v0, :cond_0

    .line 1629
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 1627
    :cond_6
    const v1, 0x7f0a0042

    goto :goto_6

    .line 1634
    :sswitch_6
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v1, :cond_7

    const v1, 0x7f0a0030

    :goto_7
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1635
    if-eqz v0, :cond_0

    .line 1636
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    .line 1634
    :cond_7
    const v1, 0x7f0a0040

    goto :goto_7

    .line 1589
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0001 -> :sswitch_3
        0x7f0a0005 -> :sswitch_4
        0x7f0a000e -> :sswitch_4
        0x7f0a0012 -> :sswitch_3
        0x7f0a0021 -> :sswitch_0
        0x7f0a0025 -> :sswitch_1
        0x7f0a0029 -> :sswitch_2
        0x7f0a002e -> :sswitch_5
        0x7f0a0030 -> :sswitch_6
        0x7f0a0033 -> :sswitch_2
        0x7f0a0037 -> :sswitch_1
        0x7f0a003b -> :sswitch_0
        0x7f0a0040 -> :sswitch_6
        0x7f0a0042 -> :sswitch_5
    .end sparse-switch
.end method

.method private setArea(Lcom/sec/android/seccamera/SecCamera;Ljava/util/List;)V
    .locals 4
    .param p1, "camera"    # Lcom/sec/android/seccamera/SecCamera;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/seccamera/SecCamera;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/seccamera/SecCamera$Area;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1949
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Area;>;"
    invoke-virtual {p1}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v2

    .line 1951
    .local v2, "parameters":Lcom/sec/android/seccamera/SecCamera$Parameters;
    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getMaxNumFocusAreas()I

    move-result v0

    .line 1952
    .local v0, "maxNumFocusAreas":I
    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getMaxNumMeteringAreas()I

    move-result v1

    .line 1954
    .local v1, "maxNumMeteringAreas":I
    if-lez v0, :cond_0

    .line 1955
    invoke-virtual {v2, p2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 1958
    :cond_0
    if-lez v1, :cond_1

    .line 1959
    invoke-virtual {v2, p2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    .line 1969
    :cond_1
    const-string v3, "auto"

    invoke-virtual {v2, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1972
    invoke-virtual {p1, v2}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 1973
    return-void
.end method

.method private setAutoFocus(Z)V
    .locals 3
    .param p1, "mode"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1737
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v0, :cond_1

    .line 1753
    :cond_0
    :goto_0
    return-void

    .line 1741
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    if-eqz v0, :cond_0

    .line 1742
    if-ne p1, v2, :cond_2

    .line 1743
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_2

    .line 1744
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAutoFocusCallback:Lcom/sec/android/app/magnifier/Magnifier$AutoFocusCallback;

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->autoFocus(Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;)V

    .line 1748
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->sendHideZoomValueAfterAF()V

    .line 1749
    iput-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    .line 1751
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->startResetTouchAFTimer()V

    goto :goto_0
.end method

.method private setAutoFocusArea(Lcom/sec/android/seccamera/SecCamera;IIIZLandroid/graphics/Point;)V
    .locals 11
    .param p1, "camera"    # Lcom/sec/android/seccamera/SecCamera;
    .param p2, "posX"    # I
    .param p3, "posY"    # I
    .param p4, "focusRange"    # I
    .param p5, "isLandscape"    # Z
    .param p6, "point"    # Landroid/graphics/Point;

    .prologue
    .line 1888
    if-ltz p2, :cond_0

    if-gez p3, :cond_1

    .line 1889
    :cond_0
    const/4 v9, 0x0

    invoke-direct {p0, p1, v9}, Lcom/sec/android/app/magnifier/Magnifier;->setArea(Lcom/sec/android/seccamera/SecCamera;Ljava/util/List;)V

    .line 1941
    :goto_0
    return-void

    .line 1898
    :cond_1
    if-nez p5, :cond_7

    .line 1899
    move-object/from16 v0, p6

    iget v9, v0, Landroid/graphics/Point;->y:I

    shr-int/lit8 v7, v9, 0x1

    .line 1900
    .local v7, "touchPointX":I
    move-object/from16 v0, p6

    iget v9, v0, Landroid/graphics/Point;->x:I

    shr-int/lit8 v8, v9, 0x1

    .line 1902
    .local v8, "touchPointY":I
    move v6, p2

    .line 1903
    .local v6, "startFocusY":I
    move v3, p3

    .line 1912
    .local v3, "endFocusY":I
    :goto_1
    const/high16 v9, 0x447a0000    # 1000.0f

    int-to-float v10, v8

    div-float v5, v9, v10

    .line 1913
    .local v5, "startFocusX":F
    const/high16 v9, 0x447a0000    # 1000.0f

    int-to-float v10, v7

    div-float v2, v9, v10

    .line 1915
    .local v2, "endFocusX":F
    sub-int v9, v6, v8

    int-to-float v9, v9

    mul-float/2addr v9, v5

    float-to-int v9, v9

    sub-int/2addr v9, p4

    int-to-float v5, v9

    .line 1916
    sub-int v9, v3, v7

    int-to-float v9, v9

    mul-float/2addr v9, v2

    float-to-int v9, v9

    sub-int v6, v9, p4

    .line 1917
    int-to-float v9, p4

    add-float v2, v5, v9

    .line 1918
    add-int v3, v6, p4

    .line 1920
    const/high16 v9, -0x3b860000    # -1000.0f

    cmpg-float v9, v5, v9

    if-gez v9, :cond_2

    .line 1921
    const/high16 v5, -0x3b860000    # -1000.0f

    .line 1923
    :cond_2
    const/16 v9, -0x3e8

    if-ge v6, v9, :cond_3

    .line 1924
    const/16 v6, -0x3e8

    .line 1926
    :cond_3
    const/high16 v9, 0x447a0000    # 1000.0f

    cmpl-float v9, v2, v9

    if-lez v9, :cond_4

    .line 1927
    const/high16 v2, 0x447a0000    # 1000.0f

    .line 1930
    :cond_4
    const/16 v9, 0x3e8

    if-le v3, v9, :cond_5

    .line 1931
    const/16 v3, 0x3e8

    .line 1934
    :cond_5
    const/4 v1, 0x0

    .line 1935
    .local v1, "areas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/seccamera/SecCamera$Area;>;"
    float-to-int v9, v5

    float-to-int v10, v2

    if-eq v9, v10, :cond_6

    if-eq v6, v3, :cond_6

    .line 1936
    new-instance v4, Landroid/graphics/Rect;

    float-to-int v9, v5

    float-to-int v10, v2

    invoke-direct {v4, v9, v6, v10, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1937
    .local v4, "rect":Landroid/graphics/Rect;
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "areas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/seccamera/SecCamera$Area;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1938
    .restart local v1    # "areas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/seccamera/SecCamera$Area;>;"
    new-instance v9, Lcom/sec/android/seccamera/SecCamera$Area;

    const/16 v10, 0x3e8

    invoke-direct {v9, v4, v10}, Lcom/sec/android/seccamera/SecCamera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1940
    .end local v4    # "rect":Landroid/graphics/Rect;
    :cond_6
    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/magnifier/Magnifier;->setArea(Lcom/sec/android/seccamera/SecCamera;Ljava/util/List;)V

    goto :goto_0

    .line 1905
    .end local v1    # "areas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/seccamera/SecCamera$Area;>;"
    .end local v2    # "endFocusX":F
    .end local v3    # "endFocusY":I
    .end local v5    # "startFocusX":F
    .end local v6    # "startFocusY":I
    .end local v7    # "touchPointX":I
    .end local v8    # "touchPointY":I
    :cond_7
    move-object/from16 v0, p6

    iget v9, v0, Landroid/graphics/Point;->x:I

    shr-int/lit8 v7, v9, 0x1

    .line 1906
    .restart local v7    # "touchPointX":I
    move-object/from16 v0, p6

    iget v9, v0, Landroid/graphics/Point;->y:I

    shr-int/lit8 v8, v9, 0x1

    .line 1908
    .restart local v8    # "touchPointY":I
    move v6, p3

    .line 1909
    .restart local v6    # "startFocusY":I
    move-object/from16 v0, p6

    iget v9, v0, Landroid/graphics/Point;->x:I

    sub-int v3, v9, p2

    .restart local v3    # "endFocusY":I
    goto :goto_1
.end method

.method private setCameraZoom(IZ)V
    .locals 2
    .param p1, "value"    # I
    .param p2, "mode"    # Z

    .prologue
    .line 1410
    if-ltz p1, :cond_0

    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    if-le p1, v1, :cond_1

    .line 1426
    :cond_0
    :goto_0
    return-void

    .line 1414
    :cond_1
    if-eqz p2, :cond_2

    .line 1415
    invoke-virtual {p0, p1}, Lcom/sec/android/app/magnifier/Magnifier;->setZoomRect(I)V

    .line 1418
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    if-eqz v1, :cond_0

    .line 1421
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v1, v1, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v1, :cond_0

    .line 1422
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v1, v1, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v1}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v0

    .line 1423
    .local v0, "param":Lcom/sec/android/seccamera/SecCamera$Parameters;
    invoke-virtual {v0, p1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setZoom(I)V

    .line 1424
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v1, v1, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v1, v0}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    goto :goto_0
.end method

.method private setConfigurationLayout(I)V
    .locals 5
    .param p1, "orientation"    # I

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1756
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 1757
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-nez v0, :cond_1

    .line 1758
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1759
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1760
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandFreezeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1761
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortFreezeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1763
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 1764
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a002e

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPlusIcon:Landroid/widget/ImageView;

    .line 1765
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a0030

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mMinusIcon:Landroid/widget/ImageView;

    .line 1767
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1768
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFocusLand:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1823
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-nez v0, :cond_6

    .line 1824
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPlusIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1825
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mMinusIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1830
    :goto_1
    return-void

    .line 1770
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandFreezeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1771
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortFreezeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1772
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1773
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1775
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandFreezeLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a000a

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezePlusIcon:Landroid/widget/ImageView;

    .line 1776
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandFreezeLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a000c

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMinusIcon:Landroid/widget/ImageView;

    .line 1778
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    if-eqz v0, :cond_2

    .line 1779
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/magnifier/FreezeImageView;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    .line 1782
    :cond_2
    invoke-virtual {p0, v3, v2, v4}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressChange(FZZ)V

    .line 1783
    invoke-virtual {p0, v3, v2, v4}, Lcom/sec/android/app/magnifier/Magnifier;->setFreezeZoom(FZZ)V

    .line 1784
    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 1786
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1787
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonSaveLand:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 1790
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-nez v0, :cond_4

    .line 1791
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1792
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1793
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortFreezeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1794
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandFreezeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1796
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 1797
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a0042

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPlusIcon:Landroid/widget/ImageView;

    .line 1798
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a0040

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mMinusIcon:Landroid/widget/ImageView;

    .line 1800
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1801
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFreezePort:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    .line 1803
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortFreezeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1804
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandFreezeLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1805
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1806
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1808
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortFreezeLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a0019

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezePlusIcon:Landroid/widget/ImageView;

    .line 1809
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortFreezeLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0a0017

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMinusIcon:Landroid/widget/ImageView;

    .line 1811
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    if-eqz v0, :cond_5

    .line 1812
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/magnifier/FreezeImageView;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    .line 1815
    :cond_5
    invoke-virtual {p0, v3, v4, v2}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressChange(FZZ)V

    .line 1816
    invoke-virtual {p0, v3, v4, v2}, Lcom/sec/android/app/magnifier/Magnifier;->setFreezeZoom(FZZ)V

    .line 1817
    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 1819
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1820
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonBackPort:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    .line 1827
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezePlusIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1828
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMinusIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method

.method private setLastOrientation(I)V
    .locals 0
    .param p1, "lastOrientation"    # I

    .prologue
    .line 747
    iput p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLastOrientation:I

    .line 748
    return-void
.end method

.method private setProgressDown(ILandroid/view/KeyEvent;)V
    .locals 5
    .param p1, "KeyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1692
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v1, :cond_3

    .line 1693
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    add-int/lit8 v1, v1, -0xa

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 1694
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    if-gtz v1, :cond_0

    .line 1695
    iput v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 1697
    :cond_0
    const/high16 v1, 0x41100000    # 9.0f

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v0, v1, v2

    .line 1698
    .local v0, "zoom_scale":F
    invoke-virtual {p0, v0, v3, v3}, Lcom/sec/android/app/magnifier/Magnifier;->setFreezeZoom(FZZ)V

    .line 1700
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v1, :cond_2

    .line 1701
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v1, :cond_1

    .line 1702
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 1703
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 1734
    .end local v0    # "zoom_scale":F
    :cond_1
    :goto_0
    return-void

    .line 1706
    .restart local v0    # "zoom_scale":F
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v1, :cond_1

    .line 1707
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 1708
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0

    .line 1712
    .end local v0    # "zoom_scale":F
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsScaleZoomWorking:Z

    if-nez v1, :cond_5

    .line 1713
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomSensitizing:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    .line 1717
    :goto_1
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    if-gtz v1, :cond_4

    .line 1718
    iput v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    .line 1720
    :cond_4
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/app/magnifier/Magnifier;->setCameraZoom(IZ)V

    .line 1722
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v1, :cond_6

    .line 1723
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v1, :cond_1

    .line 1724
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 1725
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0

    .line 1715
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->calculatorProgress(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    goto :goto_1

    .line 1728
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v1, :cond_1

    .line 1729
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 1730
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0
.end method

.method private setProgressUp(ILandroid/view/KeyEvent;)V
    .locals 4
    .param p1, "KeyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0x64

    const/4 v3, 0x1

    .line 1645
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v1, :cond_3

    .line 1646
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    add-int/lit8 v1, v1, 0xa

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 1647
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    if-lt v1, v2, :cond_0

    .line 1648
    iput v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 1650
    :cond_0
    const/high16 v1, 0x41100000    # 9.0f

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v0, v1, v2

    .line 1651
    .local v0, "zoom_scale":F
    invoke-virtual {p0, v0, v3, v3}, Lcom/sec/android/app/magnifier/Magnifier;->setFreezeZoom(FZZ)V

    .line 1653
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v1, :cond_2

    .line 1654
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v1, :cond_1

    .line 1655
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 1656
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 1689
    .end local v0    # "zoom_scale":F
    :cond_1
    :goto_0
    return-void

    .line 1659
    .restart local v0    # "zoom_scale":F
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v1, :cond_1

    .line 1660
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 1661
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0

    .line 1665
    .end local v0    # "zoom_scale":F
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsScaleZoomWorking:Z

    if-nez v1, :cond_6

    .line 1666
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomSensitizing:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    .line 1672
    :cond_4
    :goto_1
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    if-lt v1, v2, :cond_5

    .line 1673
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomMax:I

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    .line 1675
    :cond_5
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/app/magnifier/Magnifier;->setCameraZoom(IZ)V

    .line 1677
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v1, :cond_7

    .line 1678
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v1, :cond_1

    .line 1679
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 1680
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0

    .line 1668
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v1, :cond_4

    .line 1669
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->calculatorProgress(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    goto :goto_1

    .line 1683
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v1, :cond_1

    .line 1684
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 1685
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0
.end method

.method private showDlg(I)V
    .locals 3
    .param p1, "nId"    # I

    .prologue
    .line 915
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 916
    if-ne v0, p1, :cond_0

    .line 917
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mDlgStatus:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 918
    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/Magnifier;->showDialog(I)V

    .line 915
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 921
    :cond_1
    return-void
.end method


# virtual methods
.method public checkSideSyncConnected()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2406
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sidesync_source_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 2409
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public displayFreezeView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2025
    const-string v2, "MagnifierActivity"

    const-string v3, "displayFreezeView"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2028
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    if-nez v2, :cond_0

    .line 2029
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v2, v2, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v2, :cond_0

    .line 2030
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v2, v2, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v1

    .line 2031
    .local v1, "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    const-string v2, "off"

    invoke-virtual {v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 2032
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v2, v2, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 2034
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "torch_light"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2035
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.ACTION_ASSISTIVE_WIDGET_STATE_CHANGE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2036
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "from"

    const-string v3, "widget"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2037
    const-string v2, "value"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2038
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2043
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    if-eqz v2, :cond_1

    .line 2044
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/FreezeImageView;->resetZoom()V

    .line 2045
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    if-eqz v2, :cond_2

    .line 2046
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/FreezeImageView;->resetZoom()V

    .line 2048
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v2, :cond_3

    .line 2049
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 2050
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v2, :cond_4

    .line 2051
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 2053
    :cond_4
    iput v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 2054
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    .line 2056
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->setConfigurationLayout(I)V

    .line 2057
    return-void
.end method

.method public displayPreviewView()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2060
    const-string v2, "MagnifierActivity"

    const-string v3, "displayPreviewView"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2063
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    if-nez v2, :cond_1

    .line 2064
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v2, v2, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v2, :cond_0

    .line 2065
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v2, v2, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v1

    .line 2066
    .local v1, "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    const-string v2, "torch"

    invoke-virtual {v1, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 2067
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v2, v2, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2, v1}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 2070
    .end local v1    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "torch_light"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2071
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.ACTION_ASSISTIVE_WIDGET_STATE_CHANGE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2072
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "from"

    const-string v3, "widget"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2073
    const-string v2, "value"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2074
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2078
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    .line 2080
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->setConfigurationLayout(I)V

    .line 2083
    invoke-virtual {p0, v4, v4, v4}, Lcom/sec/android/app/magnifier/Magnifier;->setAutoFocus(IIZ)V

    .line 2085
    return-void
.end method

.method public getAntiBanding()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2260
    invoke-static {}, Lcom/sec/android/app/magnifier/Util;->isCSCUsedInManyCountriesForLatin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2261
    invoke-static {}, Lcom/sec/android/app/magnifier/Util;->getAntibandingByMCCForLatin()Ljava/lang/String;

    move-result-object v1

    const-string v2, "50hz"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2262
    const-string v1, "50hz"

    .line 2281
    :goto_0
    return-object v1

    .line 2264
    :cond_0
    const-string v1, "60hz"

    goto :goto_0

    .line 2267
    :cond_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Camera_CameraFlicker"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2268
    .local v0, "flicker":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 2269
    const-string v1, "50hz"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2270
    const-string v1, "50hz"

    goto :goto_0

    .line 2271
    :cond_2
    const-string v1, "60hz"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2272
    const-string v1, "60hz"

    goto :goto_0

    .line 2273
    :cond_3
    const-string v1, "auto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2274
    const-string v1, "auto"

    goto :goto_0

    .line 2275
    :cond_4
    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2276
    const-string v1, "off"

    goto :goto_0

    .line 2278
    :cond_5
    const-string v1, "50hz"

    goto :goto_0

    .line 2281
    :cond_6
    const-string v1, "50hz"

    goto :goto_0
.end method

.method public getForcedShutterSound()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2252
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Camera_EnableCameraDuringCall"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2253
    const/4 v0, 0x0

    .line 2255
    :cond_0
    return v0
.end method

.method public getLastOrientation()I
    .locals 1

    .prologue
    .line 751
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLastOrientation:I

    return v0
.end method

.method protected getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    .locals 1

    .prologue
    .line 299
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/magnifier/Magnifier$ScaleListener;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    return-object v0
.end method

.method public getZoomSensitizing()I
    .locals 1

    .prologue
    .line 1833
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomSensitizing:I

    return v0
.end method

.method protected handleLowBattery()V
    .locals 3

    .prologue
    .line 2333
    const-string v1, "MagnifierActivity"

    const-string v2, "handleLowBattery"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2335
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2364
    :goto_0
    return-void

    .line 2339
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2340
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f080014

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2341
    const v1, 0x7f08000b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 2343
    const v1, 0x7f080003

    new-instance v2, Lcom/sec/android/app/magnifier/Magnifier$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/magnifier/Magnifier$13;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2349
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2350
    new-instance v1, Lcom/sec/android/app/magnifier/Magnifier$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/magnifier/Magnifier$14;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2362
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    .line 2363
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public hideAllDlg()V
    .locals 3

    .prologue
    .line 893
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 894
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mDlgStatus:[Z

    aget-boolean v1, v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 895
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mDlgStatus:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 896
    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/Magnifier;->dismissDialog(I)V

    .line 893
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 899
    :cond_1
    return-void
.end method

.method public hideZoomValue()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1462
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomPlusPort:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1463
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomPlusLand:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1465
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomNumPort:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1466
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomNumLand:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1467
    return-void
.end method

.method public initCameraSound()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2172
    const-string v0, "MagnifierActivity"

    const-string v1, "Initialize magnifier Sound"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2174
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x3

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v5}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    .line 2175
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPoolId:[I

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f050001

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v5

    .line 2176
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPoolId:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x7f050000

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 2177
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPoolId:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f050002

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 2178
    return-void
.end method

.method public isCalling()Z
    .locals 1

    .prologue
    .line 1066
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckCalling:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCheckVoIPCalling:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x7f020004

    const v8, 0x7f020003

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1471
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v4, v4, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v4, :cond_1

    .line 1564
    :cond_0
    :goto_0
    return-void

    .line 1475
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    if-nez v4, :cond_0

    .line 1478
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 1550
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->storeImage()Ljava/lang/String;

    move-result-object v0

    .line 1551
    .local v0, "fileName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1552
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f080010

    invoke-virtual {p0, v5}, Lcom/sec/android/app/magnifier/Magnifier;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1553
    .local v3, "saved_message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSavedToast:Landroid/widget/Toast;

    .line 1554
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSavedToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1481
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v3    # "saved_message":Ljava/lang/String;
    :sswitch_1
    iget-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v4, :cond_2

    .line 1482
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iget-object v6, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v6}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {p0, v4, v6, v5}, Lcom/sec/android/app/magnifier/Magnifier;->setAutoFocus(IIZ)V

    goto :goto_0

    .line 1484
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iget-object v6, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v6}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {p0, v4, v6, v5}, Lcom/sec/android/app/magnifier/Magnifier;->setAutoFocus(IIZ)V

    goto :goto_0

    .line 1490
    :sswitch_2
    iget-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTemperatureHighToUseFlash:Z

    if-eqz v4, :cond_3

    .line 1491
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v6, 0x7f080012

    invoke-static {v4, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;

    .line 1492
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1496
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v4, v4, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v4, :cond_0

    .line 1497
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v4, v4, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v2

    .line 1499
    .local v2, "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    iget-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    if-eqz v4, :cond_4

    .line 1500
    const-string v4, "off"

    invoke-virtual {v2, v4}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 1501
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1502
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1503
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    sget-object v7, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1504
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    sget-object v7, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1513
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v4, v4, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v4, v2}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 1515
    iget-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    if-nez v4, :cond_5

    move v4, v5

    :goto_2
    iput-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    .line 1516
    iput-boolean v6, p0, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    .line 1518
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "torch_light"

    iget-boolean v8, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    if-eqz v8, :cond_6

    :goto_3
    invoke-static {v4, v7, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1519
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.ACTION_ASSISTIVE_WIDGET_STATE_CHANGE"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1520
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "from"

    const-string v5, "widget"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1521
    const-string v4, "value"

    iget-boolean v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1522
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1506
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_4
    const-string v4, "torch"

    invoke-virtual {v2, v4}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 1507
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1508
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1509
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    sget-object v7, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1510
    iget-object v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    sget-object v7, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    move v4, v6

    .line 1515
    goto :goto_2

    :cond_6
    move v5, v6

    .line 1518
    goto :goto_3

    .line 1530
    .end local v2    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :sswitch_3
    iget-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v4, :cond_7

    .line 1531
    const/16 v4, 0x16

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 1533
    :cond_7
    const/16 v4, 0x13

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 1539
    :sswitch_4
    iget-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v4, :cond_8

    .line 1540
    const/16 v4, 0x15

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 1542
    :cond_8
    const/16 v4, 0x14

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 1546
    :sswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->runFreezeStill()V

    goto/16 :goto_0

    .line 1559
    :sswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->displayPreviewView()V

    goto/16 :goto_0

    .line 1478
    :sswitch_data_0
    .sparse-switch
        0x7f0a0001 -> :sswitch_0
        0x7f0a0005 -> :sswitch_6
        0x7f0a000a -> :sswitch_3
        0x7f0a000c -> :sswitch_4
        0x7f0a000e -> :sswitch_6
        0x7f0a0012 -> :sswitch_0
        0x7f0a0017 -> :sswitch_4
        0x7f0a0019 -> :sswitch_3
        0x7f0a0021 -> :sswitch_1
        0x7f0a0025 -> :sswitch_2
        0x7f0a0029 -> :sswitch_5
        0x7f0a002e -> :sswitch_3
        0x7f0a0030 -> :sswitch_4
        0x7f0a0033 -> :sswitch_5
        0x7f0a0037 -> :sswitch_2
        0x7f0a003b -> :sswitch_1
        0x7f0a0040 -> :sswitch_4
        0x7f0a0042 -> :sswitch_3
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1568
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1571
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    .line 1573
    const/4 v0, 0x0

    .line 1574
    .local v0, "viewID":I
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1575
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    .line 1578
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-nez v1, :cond_1

    .line 1579
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->initMagnifierActivity()V

    .line 1582
    :cond_1
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/magnifier/Magnifier;->setConfigurationLayout(I)V

    .line 1583
    invoke-direct {p0, v0}, Lcom/sec/android/app/magnifier/Magnifier;->onViewFocusVisible(I)V

    .line 1584
    return-void

    .line 1571
    .end local v0    # "viewID":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 393
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 394
    const-string v2, "MagnifierActivity"

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 397
    .local v1, "win":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 401
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 402
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 404
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsDestroying:Z

    if-eqz v2, :cond_0

    .line 405
    const-string v2, "MagnifierActivity"

    const-string v3, "onCreate mIsDestroying is true, force set to false."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsDestroying:Z

    .line 409
    :cond_0
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    .line 410
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->initCameraSound()V

    .line 411
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1, "id"    # I

    .prologue
    const v5, 0x7f080014

    const v4, 0x7f080003

    const/4 v3, 0x0

    .line 925
    packed-switch p1, :pswitch_data_0

    .line 984
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 927
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 928
    .local v0, "builder1":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f080002

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 929
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/magnifier/Magnifier$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/magnifier/Magnifier$3;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 940
    new-instance v2, Lcom/sec/android/app/magnifier/Magnifier$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/magnifier/Magnifier$4;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 953
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 955
    .end local v0    # "builder1":Landroid/app/AlertDialog$Builder;
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 956
    .local v1, "builder2":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f08000c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 957
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/magnifier/Magnifier$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/magnifier/Magnifier$5;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 968
    new-instance v2, Lcom/sec/android/app/magnifier/Magnifier$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/magnifier/Magnifier$6;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 982
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 925
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestory()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1141
    const-string v0, "MagnifierActivity"

    const-string v1, "onDestory"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsDestroying:Z

    .line 1145
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    if-eqz v0, :cond_0

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/FreezeImageView;->onDestroy()V

    .line 1147
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    if-eqz v0, :cond_1

    .line 1148
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/FreezeImageView;->onDestroy()V

    .line 1150
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 1151
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1152
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCallbackBitmap:Landroid/graphics/Bitmap;

    .line 1155
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSavedToast:Landroid/widget/Toast;

    if-eqz v0, :cond_3

    .line 1156
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSavedToast:Landroid/widget/Toast;

    .line 1159
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    if-eqz v0, :cond_4

    .line 1160
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    .line 1163
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;

    if-eqz v0, :cond_5

    .line 1164
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLimitFlashToast:Landroid/widget/Toast;

    .line 1167
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;

    if-eqz v0, :cond_6

    .line 1168
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;

    .line 1171
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_7

    .line 1172
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 1173
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    .line 1175
    :cond_7
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPoolId:[I

    .line 1177
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_8

    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1179
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1180
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    .line 1183
    :cond_8
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1184
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 1185
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 1187
    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    .line 1189
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1190
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1347
    const/4 v0, 0x0

    .line 1350
    .local v0, "retVal":Z
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    if-eqz v1, :cond_0

    .line 1351
    const/4 v1, 0x1

    .line 1404
    :goto_0
    return v1

    .line 1353
    :cond_0
    sparse-switch p1, :sswitch_data_0

    :cond_1
    :goto_1
    move v1, v0

    .line 1404
    goto :goto_0

    .line 1355
    :sswitch_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressDown(ILandroid/view/KeyEvent;)V

    .line 1356
    const/4 v0, 0x1

    .line 1357
    goto :goto_1

    .line 1359
    :sswitch_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressUp(ILandroid/view/KeyEvent;)V

    .line 1360
    const/4 v0, 0x1

    .line 1361
    goto :goto_1

    .line 1363
    :sswitch_2
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v1, :cond_2

    .line 1364
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->displayPreviewView()V

    .line 1365
    const/4 v0, 0x1

    goto :goto_1

    .line 1367
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V

    goto :goto_1

    .line 1374
    :sswitch_3
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v1, :cond_5

    .line 1375
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1376
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v1, :cond_4

    .line 1377
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonBackPort:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1378
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonBackPort:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 1385
    :cond_3
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 1381
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFreezePort:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1382
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFreezePort:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_2

    .line 1388
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1389
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v1, :cond_7

    .line 1390
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonSaveLand:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 1391
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonSaveLand:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 1398
    :cond_6
    :goto_3
    const/4 v0, 0x1

    goto :goto_1

    .line 1394
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFocusLand:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 1395
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonFocusLand:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    .line 1353
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x13 -> :sswitch_3
        0x14 -> :sswitch_3
        0x15 -> :sswitch_3
        0x16 -> :sswitch_3
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1307
    const/4 v0, 0x0

    .line 1310
    .local v0, "retVal":Z
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    if-eqz v1, :cond_0

    .line 1311
    const/4 v1, 0x1

    .line 1342
    :goto_0
    return v1

    .line 1313
    :cond_0
    packed-switch p1, :pswitch_data_0

    :goto_1
    move v1, v0

    .line 1342
    goto :goto_0

    .line 1316
    :pswitch_0
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v1, :cond_3

    .line 1317
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v1, :cond_2

    .line 1318
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v1, :cond_1

    .line 1319
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->onKeyUp(ILandroid/view/KeyEvent;)Z

    .line 1337
    :cond_1
    :goto_2
    const/4 v0, 0x1

    .line 1338
    goto :goto_1

    .line 1322
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v1, :cond_1

    .line 1323
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto :goto_2

    .line 1327
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-nez v1, :cond_4

    .line 1328
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v1, :cond_1

    .line 1329
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto :goto_2

    .line 1332
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v1, :cond_1

    .line 1333
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto :goto_2

    .line 1313
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1079
    const-string v2, "MagnifierActivity"

    const-string v3, "onPause"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1081
    iput-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraOpenCheck:Z

    .line 1082
    iput-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    .line 1084
    iget-boolean v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    if-eqz v2, :cond_0

    .line 1085
    iput-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    .line 1088
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOEL:Landroid/view/OrientationEventListener;

    if-eqz v2, :cond_1

    .line 1089
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOEL:Landroid/view/OrientationEventListener;

    invoke-virtual {v2}, Landroid/view/OrientationEventListener;->disable()V

    .line 1090
    iput-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOEL:Landroid/view/OrientationEventListener;

    .line 1093
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoom_prefs:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_2

    .line 1095
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoom_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1096
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "key_zoom"

    iget v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1097
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1100
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    if-eqz v2, :cond_3

    .line 1101
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/MagnifierSurface;->releaseCamera()V

    .line 1102
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/magnifier/MagnifierSurface;->setVisibility(I)V

    .line 1103
    iput-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    .line 1106
    :cond_3
    iput-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    .line 1107
    iput-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    .line 1108
    iput-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    .line 1109
    iput-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    .line 1111
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1112
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1113
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1114
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1115
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1118
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    .line 1119
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1120
    iput-boolean v4, p0, Lcom/sec/android/app/magnifier/Magnifier;->bPauseAudioPlayback:Z

    .line 1124
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/Magnifier;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1129
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->hideAllDlg()V

    .line 1131
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1132
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 1133
    :cond_4
    iput-object v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLowBatteryPopup:Landroid/app/AlertDialog;

    .line 1135
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->stopInactivityTimer()V

    .line 1137
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1138
    return-void

    .line 1125
    :catch_0
    move-exception v0

    .line 1126
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "MagnifierActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receiver not registered : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 14

    .prologue
    const v13, 0x7f020003

    const/4 v2, 0x0

    const v12, 0x7f080001

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 415
    const-string v0, "MagnifierActivity"

    const-string v3, "onResume()"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->checkSideSyncConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 419
    const-string v0, "MagnifierActivity"

    const-string v3, "unable launch camera while sidesync"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 421
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f080013

    invoke-static {v0, v3, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSideSyncToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 424
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V

    .line 428
    :cond_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v3, "CscFeature_Camera_SecurityMdmService"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 429
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->checkCameraStartCondition_Security()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    if-nez v0, :cond_2

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v12, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    .line 433
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 434
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V

    .line 439
    :cond_3
    const-string v0, "content://com.sec.knox.provider/RestrictionPolicy1"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 440
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 441
    .local v6, "cr":Landroid/database/Cursor;
    if-eqz v1, :cond_4

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "isCameraEnabled"

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "true"

    aput-object v5, v4, v11

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 444
    :cond_4
    if-eqz v6, :cond_7

    .line 446
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 447
    const-string v0, "isCameraEnabled"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "false"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 448
    const-string v0, "MagnifierActivity"

    const-string v3, "onResume CAMERA disable(1)"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    if-nez v0, :cond_5

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f080001

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    .line 452
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 461
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "device_policy"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/admin/DevicePolicyManager;

    .line 462
    .local v7, "mDPM":Landroid/app/admin/DevicePolicyManager;
    invoke-virtual {v7, v2}, Landroid/app/admin/DevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 463
    const-string v0, "MagnifierActivity"

    const-string v2, "onResume:CAMERA disable(2)"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    if-nez v0, :cond_8

    .line 465
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v12, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    .line 467
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSecurityToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 468
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->finish()V

    .line 471
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getWindow()Landroid/view/Window;

    move-result-object v9

    .line 472
    .local v9, "win":Landroid/view/Window;
    const/16 v0, 0x80

    invoke-virtual {v9, v0}, Landroid/view/Window;->addFlags(I)V

    .line 474
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_d

    move v0, v10

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    .line 475
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v0, :cond_a

    .line 476
    iput-boolean v11, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    .line 479
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    if-nez v0, :cond_b

    .line 481
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->initMagnifierActivity()V

    .line 484
    :cond_b
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v2, "CscFeature_Camera_EnableCameraDuringCall"

    invoke-virtual {v0, v2, v10}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 494
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->checkCameraStartCondition_VT()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 495
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->CannotStartCamera()V

    .line 496
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 584
    :goto_1
    return-void

    .line 456
    .end local v7    # "mDPM":Landroid/app/admin/DevicePolicyManager;
    .end local v9    # "win":Landroid/view/Window;
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .restart local v7    # "mDPM":Landroid/app/admin/DevicePolicyManager;
    .restart local v9    # "win":Landroid/view/Window;
    :cond_d
    move v0, v11

    .line 474
    goto :goto_0

    .line 487
    :cond_e
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->checkCameraStartCondition_Call()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->checkCameraStartCondition_VoIPCall()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 488
    :cond_f
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->CannotStartCamera()V

    .line 489
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    goto :goto_1

    .line 500
    :cond_10
    invoke-direct {p0}, Lcom/sec/android/app/magnifier/Magnifier;->initIntentFilter()V

    .line 503
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 504
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    invoke-direct {v0, v2, v3}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 506
    const-string v0, "com.sec.android.app.magnifier"

    invoke-virtual {p0, v0, v11}, Lcom/sec/android/app/magnifier/Magnifier;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoom_prefs:Landroid/content/SharedPreferences;

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoom_prefs:Landroid/content/SharedPreferences;

    const-string v2, "key_zoom"

    iget v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomSensitizing:I

    mul-int/lit8 v3, v3, 0x2

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    if-eqz v0, :cond_11

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarPort:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v0, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarLand:Lcom/sec/android/app/magnifier/PreviewSeekBar;

    iget v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-virtual {v0, v2}, Lcom/sec/android/app/magnifier/PreviewSeekBar;->setProgress(I)V

    .line 513
    :cond_11
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v0, :cond_12

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v0, v11}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v0, v11}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 519
    :cond_12
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    if-eqz v0, :cond_13

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    invoke-virtual {v0, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    invoke-virtual {v0, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_OFF:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 526
    :cond_13
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "torch_light"

    invoke-static {v0, v2, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_19

    move v8, v10

    .line 527
    .local v8, "torchStatusOn":Z
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;

    if-eqz v0, :cond_14

    .line 528
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightPort:Landroid/view/View;

    if-nez v8, :cond_1a

    move v0, v10

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 529
    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mButtonLightLand:Landroid/view/View;

    if-nez v8, :cond_1b

    move v0, v10

    :goto_4
    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 532
    :cond_14
    if-eqz v8, :cond_16

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    const v2, 0x7f020004

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    const v2, 0x7f020004

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightPort:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mImgLightLand:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/app/magnifier/Magnifier;->TALKBACK_FLASH_ON:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 539
    :cond_15
    iput-boolean v10, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLightOn:Z

    .line 540
    iput-boolean v10, p0, Lcom/sec/android/app/magnifier/Magnifier;->mTorchByQuickTool:Z

    .line 557
    :cond_16
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_17

    .line 558
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomValue:I

    invoke-direct {p0, v0, v11}, Lcom/sec/android/app/magnifier/Magnifier;->setCameraZoom(IZ)V

    .line 561
    :cond_17
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOEL:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_18

    .line 562
    new-instance v0, Lcom/sec/android/app/magnifier/Magnifier$1;

    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/magnifier/Magnifier$1;-><init>(Lcom/sec/android/app/magnifier/Magnifier;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOEL:Landroid/view/OrientationEventListener;

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mOEL:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 579
    :cond_18
    iput-boolean v11, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsScaleZoomWorking:Z

    .line 581
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->restartInactivityTimer()V

    .line 583
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    goto/16 :goto_1

    .end local v8    # "torchStatusOn":Z
    :cond_19
    move v8, v11

    .line 526
    goto :goto_2

    .restart local v8    # "torchStatusOn":Z
    :cond_1a
    move v0, v11

    .line 528
    goto :goto_3

    :cond_1b
    move v0, v11

    .line 529
    goto :goto_4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    .line 1976
    iget-boolean v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeMode:Z

    if-eqz v3, :cond_1

    .line 2014
    :cond_0
    :goto_0
    return v5

    .line 1982
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mNumberOfPointer:I

    .line 1984
    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v3, :cond_2

    .line 1986
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1990
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v3}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1995
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 1996
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1997
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1999
    .local v2, "y":F
    iget-boolean v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    if-eqz v3, :cond_3

    .line 2001
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 2003
    .local v0, "ContainerWidth":F
    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    cmpg-float v3, v1, v3

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mLandLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-gez v3, :cond_0

    .line 2004
    float-to-int v3, v1

    float-to-int v4, v2

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->setAutoFocus(IIZ)V

    goto :goto_0

    .line 2007
    .end local v0    # "ContainerWidth":F
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 2009
    .restart local v0    # "ContainerWidth":F
    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPortLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    cmpg-float v3, v2, v3

    if-gez v3, :cond_0

    .line 2010
    float-to-int v3, v1

    float-to-int v4, v2

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/magnifier/Magnifier;->setAutoFocus(IIZ)V

    goto/16 :goto_0

    .line 1987
    .end local v0    # "ContainerWidth":F
    .end local v1    # "x":F
    .end local v2    # "y":F
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public onUserInteraction()V
    .locals 0

    .prologue
    .line 2291
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->restartInactivityTimer()V

    .line 2292
    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    .line 2293
    return-void
.end method

.method public pauseAudioPlayback()V
    .locals 4

    .prologue
    .line 2239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->bPauseAudioPlayback:Z

    .line 2240
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/Magnifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    .line 2241
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 2242
    return-void
.end method

.method public playCameraSound(II)V
    .locals 4
    .param p1, "soundId"    # I
    .param p2, "loop"    # I

    .prologue
    const/4 v2, 0x1

    .line 2182
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_0

    .line 2183
    const-string v0, "MagnifierActivity"

    const-string v1, "playCameraSound - mSoundPool is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2222
    :goto_0
    return-void

    .line 2187
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->isCalling()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 2188
    const-string v0, "MagnifierActivity"

    const-string v1, "Sound shouldn\'t be occured during call"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2193
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "situation=3;device=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraStreamVolume:F

    .line 2194
    iput p1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundId:I

    .line 2195
    iput p2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundLoop:I

    .line 2196
    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundId:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mCameraSoundId:I

    if-ne v0, v2, :cond_3

    .line 2197
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 2198
    const-string v0, "MagnifierActivity"

    const-string v1, "pauseAudioPlayback as focusing"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2201
    :cond_3
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/magnifier/Magnifier$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/magnifier/Magnifier$11;-><init>(Lcom/sec/android/app/magnifier/Magnifier;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPlayCameraSoundThread:Ljava/lang/Thread;

    .line 2220
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPlayCameraSoundThread:Ljava/lang/Thread;

    const-string v1, "mPlayCameraSoundThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 2221
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mPlayCameraSoundThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public restartInactivityTimer()V
    .locals 2

    .prologue
    .line 2310
    const-string v0, "MagnifierActivity"

    const-string v1, "Restart InActivity Timer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2311
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->stopInactivityTimer()V

    .line 2312
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->startInactivityTimer()V

    .line 2313
    return-void
.end method

.method public resumeAudioPlayback()V
    .locals 2

    .prologue
    .line 2246
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/Magnifier;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    .line 2247
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->bPauseAudioPlayback:Z

    .line 2249
    return-void
.end method

.method public runFreezeStill()V
    .locals 3

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 2020
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    new-instance v1, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/magnifier/Magnifier$CameraPreviewCallback;-><init>(Lcom/sec/android/app/magnifier/Magnifier;Lcom/sec/android/app/magnifier/Magnifier$1;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera;->setOneShotPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V

    .line 2022
    :cond_0
    return-void
.end method

.method public sendHideZoomValue()V
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 1429
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1430
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1431
    return-void
.end method

.method public sendHideZoomValueAfterAF()V
    .locals 4

    .prologue
    const/4 v1, 0x4

    .line 1434
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1435
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1436
    return-void
.end method

.method public setAutoFocus(IIZ)V
    .locals 8
    .param p1, "posX"    # I
    .param p2, "posY"    # I
    .param p3, "mode"    # Z

    .prologue
    .line 1865
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v0, :cond_1

    .line 1883
    :cond_0
    :goto_0
    return-void

    .line 1869
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsTouchAFStarted:Z

    if-nez v0, :cond_0

    .line 1872
    if-nez p3, :cond_2

    .line 1873
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v7

    .line 1874
    .local v7, "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 1875
    const-string v0, "continuous-picture"

    invoke-virtual {v7, v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1876
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v0, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0, v7}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    goto :goto_0

    .line 1881
    .end local v7    # "params":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    iget-object v1, v0, Lcom/sec/android/app/magnifier/MagnifierSurface;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    const/16 v4, 0x80

    iget-boolean v5, p0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    new-instance v6, Landroid/graphics/Point;

    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/MagnifierSurface;->getWidth()I

    move-result v0

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSurface:Lcom/sec/android/app/magnifier/MagnifierSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/MagnifierSurface;->getHeight()I

    move-result v2

    invoke-direct {v6, v0, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/magnifier/Magnifier;->setAutoFocusArea(Lcom/sec/android/seccamera/SecCamera;IIIZLandroid/graphics/Point;)V

    .line 1882
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/magnifier/Magnifier;->setAutoFocus(Z)V

    goto :goto_0
.end method

.method public setFreezeZoom(FZZ)V
    .locals 2
    .param p1, "scale"    # F
    .param p2, "port"    # Z
    .param p3, "land"    # Z

    .prologue
    const/4 v1, 0x1

    .line 1228
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x41200000    # 10.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 1241
    :cond_0
    :goto_0
    return-void

    .line 1231
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    if-eqz v0, :cond_2

    if-ne p2, v1, :cond_2

    .line 1232
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/magnifier/FreezeImageView;->zoomTo(F)V

    .line 1233
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    iput p1, v0, Lcom/sec/android/app/magnifier/FreezeImageView;->mLastScaleFactor:F

    .line 1236
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    if-eqz v0, :cond_0

    if-ne p3, v1, :cond_0

    .line 1237
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/magnifier/FreezeImageView;->zoomTo(F)V

    .line 1238
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    iput p1, v0, Lcom/sec/android/app/magnifier/FreezeImageView;->mLastScaleFactor:F

    goto :goto_0
.end method

.method public setProgressChange(FZZ)V
    .locals 6
    .param p1, "scale"    # F
    .param p2, "port"    # Z
    .param p3, "land"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1244
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, p1, v1

    const v2, 0x4131c71c

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 1246
    .local v0, "progress":I
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v1, :cond_0

    if-ne p2, v5, :cond_0

    .line 1247
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 1248
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezePort:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v3}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3, v4, v4}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->onSizeChanged(IIII)V

    .line 1251
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    if-eqz v1, :cond_1

    if-ne p3, v5, :cond_1

    .line 1252
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->setProgress(I)V

    .line 1253
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/magnifier/Magnifier;->mSeekBarFreezeLand:Lcom/sec/android/app/magnifier/FreezeSeekBar;

    invoke-virtual {v3}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3, v4, v4}, Lcom/sec/android/app/magnifier/FreezeSeekBar;->onSizeChanged(IIII)V

    .line 1256
    :cond_1
    iput v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeZoomValue:I

    .line 1258
    return-void
.end method

.method public setZoomRect(I)V
    .locals 8
    .param p1, "value"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1439
    const/4 v0, 0x0

    .line 1440
    .local v0, "mZoomStep":I
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomSensitizing:I

    if-lez v1, :cond_0

    .line 1441
    iget v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomSensitizing:I

    div-int v0, p1, v1

    .line 1444
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomNumPort:Landroid/widget/TextView;

    const-string v2, "%d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1445
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomNumLand:Landroid/widget/TextView;

    const-string v2, "%d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1447
    if-lez v0, :cond_1

    .line 1448
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomPlusPort:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1449
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomPlusLand:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1455
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomNumPort:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1456
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomNumLand:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1458
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/Magnifier;->sendHideZoomValue()V

    .line 1459
    return-void

    .line 1451
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomPlusPort:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1452
    iget-object v1, p0, Lcom/sec/android/app/magnifier/Magnifier;->mZoomPlusLand:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public startInactivityTimer()V
    .locals 4

    .prologue
    .line 2296
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsDestroying:Z

    if-eqz v0, :cond_0

    .line 2300
    :goto_0
    return-void

    .line 2299
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public startResetTouchAFTimer()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 2316
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2317
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2318
    return-void
.end method

.method public stopInactivityTimer()V
    .locals 2

    .prologue
    .line 2303
    iget-boolean v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mIsDestroying:Z

    if-eqz v0, :cond_0

    .line 2307
    :goto_0
    return-void

    .line 2306
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/magnifier/Magnifier;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public storeImage()Ljava/lang/String;
    .locals 23

    .prologue
    .line 2088
    const/16 v16, 0x0

    .line 2089
    .local v16, "fullName":Ljava/lang/String;
    const/4 v11, 0x0

    .line 2090
    .local v11, "filePath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2091
    .local v2, "bSuccess":Z
    const/4 v12, 0x0

    .line 2092
    .local v12, "filecon":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .line 2094
    .local v3, "captureView":Landroid/graphics/Bitmap;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2095
    .local v4, "dateTaken":J
    invoke-static {v4, v5}, Lcom/sec/android/app/magnifier/Util;->createName(J)Ljava/lang/String;

    move-result-object v18

    .line 2096
    .local v18, "name":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ".jpg"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2098
    .local v10, "fileName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/DCIM/Magnifier"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2101
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2102
    .local v6, "dir":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v19

    if-nez v19, :cond_0

    .line 2103
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 2105
    :cond_0
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 2108
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2109
    .local v9, "f":Ljava/io/File;
    const/4 v14, 0x0

    .local v14, "filenumber":I
    move v15, v14

    .line 2110
    .end local v14    # "filenumber":I
    .local v15, "filenumber":I
    :goto_0
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 2111
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    add-int/lit8 v14, v15, 0x1

    .end local v15    # "filenumber":I
    .restart local v14    # "filenumber":I
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 2112
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ".jpg"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2113
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 2115
    new-instance v9, Ljava/io/File;

    .end local v9    # "f":Ljava/io/File;
    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v9    # "f":Ljava/io/File;
    move v15, v14

    .end local v14    # "filenumber":I
    .restart local v15    # "filenumber":I
    goto :goto_0

    .line 2118
    :cond_1
    new-instance v13, Ljava/io/FileOutputStream;

    invoke-direct {v13, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2120
    .end local v12    # "filecon":Ljava/io/FileOutputStream;
    .local v13, "filecon":Ljava/io/FileOutputStream;
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->isLandscape:Z

    move/from16 v19, v0

    if-eqz v19, :cond_7

    .line 2121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/magnifier/FreezeImageView;->buildDrawingCache()V

    .line 2122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/magnifier/FreezeImageView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/magnifier/FreezeImageView;->getBitmapDisplayedRect()Landroid/graphics/RectF;

    move-result-object v17

    .line 2124
    .local v17, "mRect":Landroid/graphics/RectF;
    if-eqz v17, :cond_2

    .line 2125
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->width()F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->height()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/magnifier/Util;->cropCenterBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2126
    :cond_2
    if-eqz v3, :cond_3

    .line 2127
    sget-object v19, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v20, 0x64

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v3, v0, v1, v13}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2129
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewLand:Lcom/sec/android/app/magnifier/FreezeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/magnifier/FreezeImageView;->destroyDrawingCache()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2142
    :goto_1
    const/4 v2, 0x1

    .line 2148
    if-eqz v13, :cond_4

    .line 2149
    :try_start_2
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    .line 2151
    :cond_4
    if-eqz v3, :cond_5

    .line 2152
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2153
    const/4 v3, 0x0

    :cond_5
    move-object v12, v13

    .line 2161
    .end local v6    # "dir":Ljava/io/File;
    .end local v9    # "f":Ljava/io/File;
    .end local v13    # "filecon":Ljava/io/FileOutputStream;
    .end local v15    # "filenumber":I
    .end local v17    # "mRect":Landroid/graphics/RectF;
    .restart local v12    # "filecon":Ljava/io/FileOutputStream;
    :cond_6
    :goto_2
    if-eqz v2, :cond_d

    .line 2163
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/magnifier/Magnifier;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    new-instance v20, Landroid/content/Intent;

    const-string v21, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v22

    invoke-direct/range {v20 .. v22}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2167
    .end local v10    # "fileName":Ljava/lang/String;
    :goto_3
    return-object v10

    .line 2131
    .end local v12    # "filecon":Ljava/io/FileOutputStream;
    .restart local v6    # "dir":Ljava/io/File;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v10    # "fileName":Ljava/lang/String;
    .restart local v13    # "filecon":Ljava/io/FileOutputStream;
    .restart local v15    # "filenumber":I
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/magnifier/FreezeImageView;->buildDrawingCache()V

    .line 2132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/magnifier/FreezeImageView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/magnifier/FreezeImageView;->getBitmapDisplayedRect()Landroid/graphics/RectF;

    move-result-object v17

    .line 2134
    .restart local v17    # "mRect":Landroid/graphics/RectF;
    if-eqz v17, :cond_8

    .line 2135
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->width()F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->height()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/magnifier/Util;->cropCenterBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2136
    :cond_8
    if-eqz v3, :cond_9

    .line 2137
    sget-object v19, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v20, 0x64

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v3, v0, v1, v13}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2139
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/magnifier/Magnifier;->mFreezeViewPort:Lcom/sec/android/app/magnifier/FreezeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/magnifier/FreezeImageView;->destroyDrawingCache()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 2143
    .end local v17    # "mRect":Landroid/graphics/RectF;
    :catch_0
    move-exception v8

    move-object v12, v13

    .line 2144
    .end local v6    # "dir":Ljava/io/File;
    .end local v9    # "f":Ljava/io/File;
    .end local v13    # "filecon":Ljava/io/FileOutputStream;
    .end local v15    # "filenumber":I
    .local v8, "ex":Ljava/lang/Exception;
    .restart local v12    # "filecon":Ljava/io/FileOutputStream;
    :goto_4
    const/4 v2, 0x0

    .line 2145
    :try_start_4
    const-string v19, "MagnifierActivity"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "storeImage(): Exception while saving the freeze image."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2148
    if-eqz v12, :cond_a

    .line 2149
    :try_start_5
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    .line 2151
    :cond_a
    if-eqz v3, :cond_6

    .line 2152
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 2153
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 2155
    .end local v8    # "ex":Ljava/lang/Exception;
    .end local v12    # "filecon":Ljava/io/FileOutputStream;
    .restart local v6    # "dir":Ljava/io/File;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v13    # "filecon":Ljava/io/FileOutputStream;
    .restart local v15    # "filenumber":I
    .restart local v17    # "mRect":Landroid/graphics/RectF;
    :catch_1
    move-exception v7

    .line 2157
    .local v7, "e":Ljava/io/IOException;
    const-string v19, "MagnifierActivity"

    move-object/from16 v0, v19

    invoke-static {v0, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v12, v13

    .line 2159
    .end local v13    # "filecon":Ljava/io/FileOutputStream;
    .restart local v12    # "filecon":Ljava/io/FileOutputStream;
    goto/16 :goto_2

    .line 2155
    .end local v6    # "dir":Ljava/io/File;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v9    # "f":Ljava/io/File;
    .end local v15    # "filenumber":I
    .end local v17    # "mRect":Landroid/graphics/RectF;
    .restart local v8    # "ex":Ljava/lang/Exception;
    :catch_2
    move-exception v7

    .line 2157
    .restart local v7    # "e":Ljava/io/IOException;
    const-string v19, "MagnifierActivity"

    move-object/from16 v0, v19

    invoke-static {v0, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 2147
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v19

    .line 2148
    :goto_5
    if-eqz v12, :cond_b

    .line 2149
    :try_start_6
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    .line 2151
    :cond_b
    if-eqz v3, :cond_c

    .line 2152
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 2153
    const/4 v3, 0x0

    .line 2158
    :cond_c
    :goto_6
    throw v19

    .line 2155
    :catch_3
    move-exception v7

    .line 2157
    .restart local v7    # "e":Ljava/io/IOException;
    const-string v20, "MagnifierActivity"

    move-object/from16 v0, v20

    invoke-static {v0, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 2167
    .end local v7    # "e":Ljava/io/IOException;
    :cond_d
    const/4 v10, 0x0

    goto/16 :goto_3

    .line 2147
    .end local v12    # "filecon":Ljava/io/FileOutputStream;
    .restart local v6    # "dir":Ljava/io/File;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v13    # "filecon":Ljava/io/FileOutputStream;
    .restart local v15    # "filenumber":I
    :catchall_1
    move-exception v19

    move-object v12, v13

    .end local v13    # "filecon":Ljava/io/FileOutputStream;
    .restart local v12    # "filecon":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 2143
    .end local v6    # "dir":Ljava/io/File;
    .end local v9    # "f":Ljava/io/File;
    .end local v15    # "filenumber":I
    :catch_4
    move-exception v8

    goto :goto_4
.end method
