.class public final Lcom/sec/android/app/magnifier/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ProgressBar:[I

.field public static final ProgressBar_android_max:I = 0x2

.field public static final ProgressBar_android_maxHeight:I = 0x1

.field public static final ProgressBar_android_maxWidth:I = 0x0

.field public static final ProgressBar_android_minHeight:I = 0x7

.field public static final ProgressBar_android_minWidth:I = 0x6

.field public static final ProgressBar_android_progress:I = 0x3

.field public static final ProgressBar_android_progressDrawable:I = 0x5

.field public static final ProgressBar_android_secondaryProgress:I = 0x4

.field public static final Theme:[I

.field public static final Theme_android_disabledAlpha:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 225
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/magnifier/R$styleable;->ProgressBar:[I

    .line 287
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010033

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/app/magnifier/R$styleable;->Theme:[I

    return-void

    .line 225
    nop

    :array_0
    .array-data 4
        0x101011f
        0x1010120
        0x1010136
        0x1010137
        0x1010138
        0x101013c
        0x101013f
        0x1010140
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
