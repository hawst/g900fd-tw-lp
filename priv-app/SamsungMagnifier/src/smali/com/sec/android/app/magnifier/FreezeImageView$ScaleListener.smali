.class public Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "FreezeImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/magnifier/FreezeImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/magnifier/FreezeImageView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/magnifier/FreezeImageView;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    iget v1, v1, Lcom/sec/android/app/magnifier/FreezeImageView;->mLastScaleFactor:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    mul-float v0, v1, v2

    .line 237
    .local v0, "targetScale":F
    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/magnifier/FreezeImageView;->getMaxZoom()F

    move-result v1

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/magnifier/FreezeImageView;->zoomTo(FFF)V

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/FreezeImageView;->getMaxZoom()F

    move-result v2

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, v1, Lcom/sec/android/app/magnifier/FreezeImageView;->mCurrentScaleFactor:F

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    # getter for: Lcom/sec/android/app/magnifier/FreezeImageView;->mActivityContext:Lcom/sec/android/app/magnifier/Magnifier;
    invoke-static {v1}, Lcom/sec/android/app/magnifier/FreezeImageView;->access$400(Lcom/sec/android/app/magnifier/FreezeImageView;)Lcom/sec/android/app/magnifier/Magnifier;

    move-result-object v1

    invoke-virtual {v1, v0, v5, v5}, Lcom/sec/android/app/magnifier/Magnifier;->setProgressChange(FZZ)V

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/magnifier/FreezeImageView;->invalidate()V

    .line 245
    const/4 v1, 0x0

    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/magnifier/FreezeImageView;->mIsScaleActivated:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/magnifier/FreezeImageView;->access$502(Lcom/sec/android/app/magnifier/FreezeImageView;Z)Z

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/FreezeImageView$ScaleListener;->this$0:Lcom/sec/android/app/magnifier/FreezeImageView;

    iget v1, v1, Lcom/sec/android/app/magnifier/FreezeImageView;->mCurrentScaleFactor:F

    iput v1, v0, Lcom/sec/android/app/magnifier/FreezeImageView;->mLastScaleFactor:F

    .line 252
    return-void
.end method
