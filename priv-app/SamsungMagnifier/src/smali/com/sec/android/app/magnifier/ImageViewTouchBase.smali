.class abstract Lcom/sec/android/app/magnifier/ImageViewTouchBase;
.super Landroid/widget/ImageView;
.source "ImageViewTouchBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;
    }
.end annotation


# static fields
.field static final SCALE_MAX_ZOOM:F = 10.0f

.field static final SCALE_RATE:F = 1.25f

.field private static final TAG:Ljava/lang/String; = "ImageViewTouchBase"


# instance fields
.field protected mBaseMatrix:Landroid/graphics/Matrix;

.field protected final mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

.field protected mBitmapRect:Landroid/graphics/RectF;

.field protected mCenterRect:Landroid/graphics/RectF;

.field private final mDisplayMatrix:Landroid/graphics/Matrix;

.field protected mHandler:Landroid/os/Handler;

.field protected mLastXTouchPos:I

.field protected mLastYTouchPos:I

.field private final mMatrixValues:[F

.field mMaxZoom:F

.field private mOnLayoutRunnable:Ljava/lang/Runnable;

.field private mRecycler:Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;

.field protected mSuppMatrix:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    .line 47
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    .line 51
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    .line 54
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMatrixValues:[F

    .line 57
    new-instance v0, Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/magnifier/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    .line 60
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    .line 61
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mCenterRect:Landroid/graphics/RectF;

    .line 117
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    .line 147
    iput-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->init()V

    .line 236
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 239
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    .line 47
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    .line 51
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    .line 54
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMatrixValues:[F

    .line 57
    new-instance v0, Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/magnifier/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    .line 60
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    .line 61
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mCenterRect:Landroid/graphics/RectF;

    .line 117
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    .line 147
    iput-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->init()V

    .line 241
    return-void
.end method

.method private getProperBaseMatrix(Lcom/sec/android/app/magnifier/RotateBitmap;Landroid/graphics/Matrix;)V
    .locals 10
    .param p1, "bitmap"    # Lcom/sec/android/app/magnifier/RotateBitmap;
    .param p2, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/high16 v8, 0x40400000    # 3.0f

    const/high16 v9, 0x40000000    # 2.0f

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v7

    int-to-float v4, v7

    .line 296
    .local v4, "viewWidth":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v7

    int-to-float v3, v7

    .line 298
    .local v3, "viewHeight":F
    invoke-virtual {p1}, Lcom/sec/android/app/magnifier/RotateBitmap;->getWidth()I

    move-result v7

    int-to-float v5, v7

    .line 299
    .local v5, "w":F
    invoke-virtual {p1}, Lcom/sec/android/app/magnifier/RotateBitmap;->getHeight()I

    move-result v7

    int-to-float v0, v7

    .line 300
    .local v0, "h":F
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 304
    div-float v7, v4, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 305
    .local v6, "widthScale":F
    div-float v7, v3, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 306
    .local v1, "heightScale":F
    invoke-static {v6, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 308
    .local v2, "scale":F
    invoke-virtual {p1}, Lcom/sec/android/app/magnifier/RotateBitmap;->getRotateMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 309
    invoke-virtual {p2, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 311
    mul-float v7, v5, v2

    sub-float v7, v4, v7

    div-float/2addr v7, v9

    mul-float v8, v0, v2

    sub-float v8, v3, v8

    div-float/2addr v8, v9

    invoke-virtual {p2, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 314
    return-void
.end method

.method private setImageBitmap(Landroid/graphics/Bitmap;I)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rotation"    # I

    .prologue
    .line 128
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 130
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 131
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 134
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 135
    .local v1, "old":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/magnifier/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/magnifier/RotateBitmap;->setRotation(I)V

    .line 138
    if-eqz v1, :cond_1

    if-eq v1, p1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mRecycler:Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;

    if-eqz v2, :cond_1

    .line 139
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mRecycler:Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;

    invoke-interface {v2, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;->recycle(Landroid/graphics/Bitmap;)V

    .line 141
    :cond_1
    return-void
.end method


# virtual methods
.method protected center(ZZ)V
    .locals 12
    .param p1, "horizontal"    # Z
    .param p2, "vertical"    # Z

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 190
    iget-object v8, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v8}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    if-nez v8, :cond_0

    .line 231
    :goto_0
    return-void

    .line 194
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 196
    .local v3, "m":Landroid/graphics/Matrix;
    new-instance v4, Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v8}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v9}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {v4, v10, v10, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 200
    .local v4, "rect":Landroid/graphics/RectF;
    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 202
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v2

    .line 203
    .local v2, "height":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 205
    .local v7, "width":F
    const/4 v0, 0x0

    .local v0, "deltaX":F
    const/4 v1, 0x0

    .line 207
    .local v1, "deltaY":F
    if-eqz p2, :cond_1

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v5

    .line 209
    .local v5, "viewHeight":I
    int-to-float v8, v5

    cmpg-float v8, v2, v8

    if-gez v8, :cond_3

    .line 210
    int-to-float v8, v5

    sub-float/2addr v8, v2

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v8, v9

    .line 218
    .end local v5    # "viewHeight":I
    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v6

    .line 220
    .local v6, "viewWidth":I
    int-to-float v8, v6

    cmpg-float v8, v7, v8

    if-gez v8, :cond_5

    .line 221
    int-to-float v8, v6

    sub-float/2addr v8, v7

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v8, v9

    .line 229
    .end local v6    # "viewWidth":I
    :cond_2
    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->postTranslate(FF)V

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 211
    .restart local v5    # "viewHeight":I
    :cond_3
    iget v8, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_4

    .line 212
    iget v8, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v8

    goto :goto_1

    .line 213
    :cond_4
    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_1

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v8, v9

    goto :goto_1

    .line 222
    .end local v5    # "viewHeight":I
    .restart local v6    # "viewWidth":I
    :cond_5
    iget v8, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_6

    .line 223
    iget v8, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v8

    goto :goto_2

    .line 224
    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v6

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    .line 225
    int-to-float v8, v6

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v8, v9

    goto :goto_2
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 144
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    .line 145
    return-void
.end method

.method public easeOut(DDDD)D
    .locals 5
    .param p1, "time"    # D
    .param p3, "start"    # D
    .param p5, "end"    # D
    .param p7, "duration"    # D

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 572
    div-double v0, p1, p7

    sub-double p1, v0, v2

    mul-double v0, p1, p1

    mul-double/2addr v0, p1

    add-double/2addr v0, v2

    mul-double/2addr v0, p5

    add-double/2addr v0, p3

    return-wide v0
.end method

.method protected getBitmapDisplayedRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 266
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    .line 267
    const/4 v1, 0x0

    .line 277
    :goto_0
    return-object v1

    .line 270
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 272
    .local v0, "m":Landroid/graphics/Matrix;
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v3}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 276
    .local v1, "rect":Landroid/graphics/RectF;
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto :goto_0
.end method

.method protected getBitmapRect()Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 256
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 257
    const/4 v2, 0x0

    .line 262
    :goto_0
    return-object v2

    .line 259
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 260
    .local v1, "m":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 261
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 262
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapRect:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method protected getCenter(ZZ)Landroid/graphics/RectF;
    .locals 12
    .param p1, "horizontal"    # Z
    .param p2, "vertical"    # Z

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 483
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_0

    .line 484
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v10, v10, v10, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 517
    :goto_0
    return-object v8

    .line 487
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 489
    .local v4, "rect":Landroid/graphics/RectF;
    if-nez v4, :cond_1

    .line 490
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v10, v10, v10, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    .line 493
    :cond_1
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 494
    .local v3, "height":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 495
    .local v7, "width":F
    const/4 v0, 0x0

    .local v0, "deltaX":F
    const/4 v1, 0x0

    .line 496
    .local v1, "deltaY":F
    if-eqz p2, :cond_2

    .line 497
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v5

    .line 498
    .local v5, "viewHeight":I
    int-to-float v8, v5

    cmpg-float v8, v3, v8

    if-gez v8, :cond_4

    .line 499
    int-to-float v8, v5

    sub-float/2addr v8, v3

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v8, v9

    .line 506
    .end local v5    # "viewHeight":I
    :cond_2
    :goto_1
    if-eqz p1, :cond_3

    .line 507
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v6

    .line 508
    .local v6, "viewWidth":I
    int-to-float v8, v6

    cmpg-float v8, v7, v8

    if-gez v8, :cond_6

    .line 509
    int-to-float v8, v6

    sub-float/2addr v8, v7

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v8, v9

    .line 516
    .end local v6    # "viewWidth":I
    :cond_3
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mCenterRect:Landroid/graphics/RectF;

    invoke-virtual {v8, v0, v1, v10, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 517
    iget-object v8, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mCenterRect:Landroid/graphics/RectF;

    goto :goto_0

    .line 500
    .restart local v5    # "viewHeight":I
    :cond_4
    iget v8, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_5

    .line 501
    iget v8, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v8

    goto :goto_1

    .line 502
    :cond_5
    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    .line 503
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v8, v9

    goto :goto_1

    .line 510
    .end local v5    # "viewHeight":I
    .restart local v6    # "viewWidth":I
    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_7

    .line 511
    iget v8, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v8

    goto :goto_2

    .line 512
    :cond_7
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v6

    cmpg-float v8, v8, v9

    if-gez v8, :cond_3

    .line 513
    int-to-float v8, v6

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v8, v9

    goto :goto_2
.end method

.method protected getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getMaxZoom()F
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMaxZoom:F

    return v0
.end method

.method protected getScale()F
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method protected getScale(Landroid/graphics/Matrix;)F
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 282
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v0

    return v0
.end method

.method protected getValue(Landroid/graphics/Matrix;I)F
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;
    .param p2, "whichValue"    # I

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMatrixValues:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMatrixValues:[F

    aget v0, v0, p2

    return v0
.end method

.method protected init()V
    .locals 1

    .prologue
    .line 244
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 245
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMaxZoom:F

    .line 246
    return-void
.end method

.method protected maxZoom()F
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v0}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 331
    const/high16 v0, 0x3f800000    # 1.0f

    .line 333
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x41200000    # 10.0f

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 95
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    .line 98
    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 105
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 110
    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->zoomTo(F)V

    .line 111
    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 80
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 83
    .local v0, "r":Ljava/lang/Runnable;
    if-eqz v0, :cond_0

    .line 84
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 85
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v1}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getProperBaseMatrix(Lcom/sec/android/app/magnifier/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 91
    :cond_1
    return-void
.end method

.method protected onZoom(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 525
    return-void
.end method

.method protected panBy(DD)V
    .locals 3
    .param p1, "dx"    # D
    .param p3, "dy"    # D

    .prologue
    .line 533
    double-to-float v0, p1

    double-to-float v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->postTranslate(FF)V

    .line 534
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 535
    return-void
.end method

.method protected panBy(FF)V
    .locals 1
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 528
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->postTranslate(FF)V

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 530
    return-void
.end method

.method protected postTranslate(FF)V
    .locals 1
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 523
    return-void
.end method

.method public scrollBy(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 538
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->panBy(FF)V

    .line 539
    return-void
.end method

.method protected scrollBy(FFD)V
    .locals 11
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F
    .param p3, "durationMs"    # D

    .prologue
    .line 542
    float-to-double v6, p1

    .line 543
    .local v6, "dx":D
    float-to-double v8, p2

    .line 544
    .local v8, "dy":D
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 545
    .local v4, "startTime":J
    iget-object v10, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/magnifier/ImageViewTouchBase$3;

    move-object v1, p0

    move-wide v2, p3

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/magnifier/ImageViewTouchBase$3;-><init>(Lcom/sec/android/app/magnifier/ImageViewTouchBase;DJDD)V

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 569
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    .line 125
    return-void
.end method

.method public setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "resetSupp"    # Z

    .prologue
    .line 153
    new-instance v0, Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-direct {v0, p1}, Lcom/sec/android/app/magnifier/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageRotateBitmapResetBase(Lcom/sec/android/app/magnifier/RotateBitmap;Z)V

    .line 154
    return-void
.end method

.method public setImageRotateBitmapResetBase(Lcom/sec/android/app/magnifier/RotateBitmap;Z)V
    .locals 3
    .param p1, "bitmap"    # Lcom/sec/android/app/magnifier/RotateBitmap;
    .param p2, "resetSupp"    # Z

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v0

    .line 160
    .local v0, "viewWidth":I
    if-gtz v0, :cond_0

    .line 161
    new-instance v1, Lcom/sec/android/app/magnifier/ImageViewTouchBase$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase$1;-><init>(Lcom/sec/android/app/magnifier/ImageViewTouchBase;Lcom/sec/android/app/magnifier/RotateBitmap;Z)V

    iput-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    .line 182
    :goto_0
    return-void

    .line 169
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getProperBaseMatrix(Lcom/sec/android/app/magnifier/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 171
    invoke-virtual {p1}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/magnifier/RotateBitmap;->getOrientation()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    .line 177
    :goto_1
    if-eqz p2, :cond_1

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 180
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->maxZoom()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMaxZoom:F

    goto :goto_0

    .line 173
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 174
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public setRecycler(Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;)V
    .locals 0
    .param p1, "r"    # Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mRecycler:Lcom/sec/android/app/magnifier/ImageViewTouchBase$Recycler;

    .line 74
    return-void
.end method

.method protected zoomIn()V
    .locals 1

    .prologue
    .line 395
    const/high16 v0, 0x3fa00000    # 1.25f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->zoomIn(F)V

    .line 396
    return-void
.end method

.method protected zoomIn(F)V
    .locals 5
    .param p1, "rate"    # F

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 403
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale()F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMaxZoom:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v4

    .line 412
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v4

    .line 414
    .local v1, "cy":F
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 415
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method protected zoomIn(FFF)V
    .locals 7
    .param p1, "rate"    # F
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x40a00000    # 5.0f

    const/high16 v4, 0x40000000    # 2.0f

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale()F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMaxZoom:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 445
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v2}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v4

    .line 450
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v4

    .line 451
    .local v1, "cy":F
    sub-float v2, v0, p2

    div-float/2addr v2, v5

    sub-float v3, v1, p3

    div-float/2addr v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->panBy(FF)V

    .line 453
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 454
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 455
    invoke-virtual {p0, v6, v6}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->center(ZZ)V

    goto :goto_0
.end method

.method protected zoomOut()V
    .locals 1

    .prologue
    .line 399
    const/high16 v0, 0x3fa00000    # 1.25f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->zoomOut(F)V

    .line 400
    return-void
.end method

.method protected zoomOut(F)V
    .locals 7
    .param p1, "rate"    # F

    .prologue
    const/4 v6, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 419
    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v3}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    .line 437
    :goto_0
    return-void

    .line 423
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v4

    .line 424
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v4

    .line 427
    .local v1, "cy":F
    new-instance v2, Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-direct {v2, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 428
    .local v2, "tmp":Landroid/graphics/Matrix;
    div-float v3, v5, p1

    div-float v4, v5, p1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 430
    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1

    .line 431
    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v5, v5, v0, v1}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 435
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 436
    invoke-virtual {p0, v6, v6}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->center(ZZ)V

    goto :goto_0

    .line 433
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v4, v5, p1

    div-float/2addr v5, p1

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_1
.end method

.method protected zoomOut(FFF)V
    .locals 8
    .param p1, "rate"    # F
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F

    .prologue
    const/4 v7, 0x1

    const/high16 v6, 0x40a00000    # 5.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 459
    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mBitmapDisplayed:Lcom/sec/android/app/magnifier/RotateBitmap;

    invoke-virtual {v3}, Lcom/sec/android/app/magnifier/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    .line 478
    :goto_0
    return-void

    .line 463
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v4

    .line 464
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v4

    .line 465
    .local v1, "cy":F
    sub-float v3, v0, p2

    div-float/2addr v3, v6

    sub-float v4, v1, p3

    div-float/2addr v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->panBy(FF)V

    .line 468
    new-instance v2, Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-direct {v2, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 469
    .local v2, "tmp":Landroid/graphics/Matrix;
    div-float v3, v5, p1

    div-float v4, v5, p1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 471
    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1

    .line 472
    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v5, v5, v0, v1}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 476
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 477
    invoke-virtual {p0, v7, v7}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->center(ZZ)V

    goto :goto_0

    .line 474
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v4, v5, p1

    div-float/2addr v5, p1

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_1
.end method

.method protected zoomTo(F)V
    .locals 4
    .param p1, "scale"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    .line 375
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    .line 377
    .local v1, "cy":F
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->zoomTo(FFF)V

    .line 378
    return-void
.end method

.method public zoomTo(FF)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "durationMs"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 381
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    .line 382
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    .line 383
    .local v1, "cy":F
    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->zoomTo(FFFF)V

    .line 384
    return-void
.end method

.method protected zoomTo(FFF)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F

    .prologue
    const/4 v3, 0x1

    .line 337
    iget v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMaxZoom:F

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    .line 338
    iget p1, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mMaxZoom:F

    .line 341
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale()F

    move-result v1

    .line 342
    .local v1, "oldScale":F
    div-float v0, p1, v1

    .line 344
    .local v0, "deltaScale":F
    iget-object v2, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 346
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->center(ZZ)V

    .line 347
    return-void
.end method

.method protected zoomTo(FFFF)V
    .locals 10
    .param p1, "scale"    # F
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F
    .param p4, "durationMs"    # F

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale()F

    move-result v0

    sub-float v0, p1, v0

    div-float v7, v0, p4

    .line 352
    .local v7, "incrementPerMs":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getScale()F

    move-result v6

    .line 353
    .local v6, "oldScale":F
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 355
    .local v4, "startTime":J
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/magnifier/ImageViewTouchBase$2;

    move-object v2, p0

    move v3, p4

    move v8, p2

    move v9, p3

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/magnifier/ImageViewTouchBase$2;-><init>(Lcom/sec/android/app/magnifier/ImageViewTouchBase;FJFFFF)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 371
    :cond_0
    return-void
.end method

.method protected zoomToPoint(FFF)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    .line 388
    .local v0, "cx":F
    invoke-virtual {p0}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    .line 390
    .local v1, "cy":F
    sub-float v2, v0, p2

    sub-float v3, v1, p3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->panBy(FF)V

    .line 391
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/magnifier/ImageViewTouchBase;->zoomTo(FFF)V

    .line 392
    return-void
.end method
