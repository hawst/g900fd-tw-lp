.class Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;
.super Ljava/lang/Object;
.source "HearingdroResult_noactionbar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->Setid()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 239
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->am:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$800(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Landroid/media/AudioManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->am:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$800(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Landroid/media/AudioManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v2

    if-nez v2, :cond_0

    .line 240
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    const v3, 0x7f07000f

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 251
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->am:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$800(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Landroid/media/AudioManager;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 244
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    const-string v3, "DHA_PREF"

    invoke-virtual {v2, v3, v6}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 245
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 246
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "page"

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 247
    const-string v2, "Pre0"

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->MakeToParam()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$900(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 248
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 249
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    const/4 v3, 0x4

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->ChangeView(I)V
    invoke-static {v2, v3}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$1000(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)V

    goto :goto_0
.end method
