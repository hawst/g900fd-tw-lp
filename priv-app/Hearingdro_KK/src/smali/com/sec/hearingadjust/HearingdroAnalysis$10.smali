.class Lcom/sec/hearingadjust/HearingdroAnalysis$10;
.super Landroid/os/Handler;
.source "HearingdroAnalysis.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/hearingadjust/HearingdroAnalysis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 384
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 425
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 426
    return-void

    .line 386
    :pswitch_0
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->tvProgress:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1000(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    .line 388
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-double v2, v1

    const-wide v4, 0x4006666666666666L    # 2.8

    div-double/2addr v2, v4

    double-to-int v1, v2

    # setter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I
    invoke-static {v0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$702(Lcom/sec/hearingadjust/HearingdroAnalysis;I)I

    .line 389
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->ivProgress:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1100(Lcom/sec/hearingadjust/HearingdroAnalysis;)[Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I
    invoke-static {v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$700(Lcom/sec/hearingadjust/HearingdroAnalysis;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 393
    :pswitch_1
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # invokes: Lcom/sec/hearingadjust/HearingdroAnalysis;->startDiagnosis()V
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1200(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    goto :goto_0

    .line 396
    :pswitch_2
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # operator++ for: Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1308(Lcom/sec/hearingadjust/HearingdroAnalysis;)I

    .line 397
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1300(Lcom/sec/hearingadjust/HearingdroAnalysis;)I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 398
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # setter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I
    invoke-static {v0, v2}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1302(Lcom/sec/hearingadjust/HearingdroAnalysis;I)I

    .line 400
    :cond_1
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->ivSignal:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1400(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "call_setting_sound_0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I
    invoke-static {v3}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1300(Lcom/sec/hearingadjust/HearingdroAnalysis;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v4}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 403
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 406
    :pswitch_3
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # invokes: Lcom/sec/hearingadjust/HearingdroAnalysis;->endAnalysis()V
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1500(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    goto/16 :goto_0

    .line 409
    :pswitch_4
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->btYes:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1600(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 410
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->btNo:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1700(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 413
    :pswitch_5
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->btYes:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1600(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 414
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->btNo:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1700(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 417
    :pswitch_6
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->tvAnalysisTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1800(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f070006

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 420
    :pswitch_7
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->tvAnalysisTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1800(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f070007

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 384
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method
