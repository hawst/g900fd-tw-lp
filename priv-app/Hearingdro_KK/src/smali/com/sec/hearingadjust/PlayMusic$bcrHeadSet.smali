.class public Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;
.super Landroid/content/BroadcastReceiver;
.source "PlayMusic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/hearingadjust/PlayMusic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "bcrHeadSet"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/PlayMusic;


# direct methods
.method public constructor <init>(Lcom/sec/hearingadjust/PlayMusic;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const v3, 0x7f07000f

    const/4 v2, 0x0

    .line 424
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 425
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->Mode:Z
    invoke-static {v1}, Lcom/sec/hearingadjust/PlayMusic;->access$1500(Lcom/sec/hearingadjust/PlayMusic;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 427
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 428
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    iget-object v2, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->Page:I
    invoke-static {v2}, Lcom/sec/hearingadjust/PlayMusic;->access$1600(Lcom/sec/hearingadjust/PlayMusic;)I

    move-result v2

    # invokes: Lcom/sec/hearingadjust/PlayMusic;->ChangeView(I)V
    invoke-static {v1, v2}, Lcom/sec/hearingadjust/PlayMusic;->access$1700(Lcom/sec/hearingadjust/PlayMusic;I)V

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 431
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->am:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/sec/hearingadjust/PlayMusic;->access$1800(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 433
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 434
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    iget-object v2, p0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->Page:I
    invoke-static {v2}, Lcom/sec/hearingadjust/PlayMusic;->access$1600(Lcom/sec/hearingadjust/PlayMusic;)I

    move-result v2

    # invokes: Lcom/sec/hearingadjust/PlayMusic;->ChangeView(I)V
    invoke-static {v1, v2}, Lcom/sec/hearingadjust/PlayMusic;->access$1700(Lcom/sec/hearingadjust/PlayMusic;I)V

    goto :goto_0
.end method
