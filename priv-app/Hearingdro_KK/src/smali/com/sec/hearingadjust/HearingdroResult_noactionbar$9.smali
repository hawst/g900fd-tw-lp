.class Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;
.super Ljava/lang/Object;
.source "HearingdroResult_noactionbar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->Setid()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 275
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_state:[Z
    invoke-static {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$1100(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)[Z

    move-result-object v5

    aget-boolean v5, v5, v8

    if-ne v5, v9, :cond_0

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectEar:I
    invoke-static {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$600(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)I

    move-result v5

    if-ne v5, v10, :cond_0

    .line 277
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    const v6, 0x7f07002c

    invoke-static {v5, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 278
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->popSelect()V
    invoke-static {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$1200(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    .line 318
    :goto_0
    return-void

    .line 281
    :cond_0
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.sec.hearingadjust.checkmusic"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 282
    .local v3, "i":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-virtual {v5, v3}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->sendBroadcast(Landroid/content/Intent;)V

    .line 284
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    const-string v6, "DHA_PREF"

    invoke-virtual {v5, v6, v8}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 285
    .local v4, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 286
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "isPreset"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 287
    .local v0, "SelectPreset":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Pre"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->MakeToParam()Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$900(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 288
    const-string v5, "CheckDone"

    invoke-interface {v2, v5, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 289
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 290
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-virtual {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_parameters"

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->MakeToParam()Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$900(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 292
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_state:[Z
    invoke-static {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$1100(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)[Z

    move-result-object v5

    aget-boolean v5, v5, v8

    if-nez v5, :cond_1

    .line 293
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # setter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectEar:I
    invoke-static {v5, v10}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$602(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)I

    .line 295
    :cond_1
    const-string v1, ""

    .line 297
    .local v1, "dhaParam":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectEar:I
    invoke-static {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$600(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 315
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->MakeToParam()Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$900(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 316
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->am:Landroid/media/AudioManager;
    invoke-static {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$800(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Landroid/media/AudioManager;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dha="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 317
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-virtual {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->finish()V

    goto/16 :goto_0

    .line 299
    :pswitch_0
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-virtual {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_revision"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 300
    const-string v1, "0,1"

    .line 301
    goto :goto_1

    .line 303
    :pswitch_1
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-virtual {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_revision"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 304
    const-string v1, "1,0"

    .line 305
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-virtual {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_direction"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    .line 308
    :pswitch_2
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-virtual {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_revision"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 309
    const-string v1, "1,1"

    .line 310
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-virtual {v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_direction"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    .line 297
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
