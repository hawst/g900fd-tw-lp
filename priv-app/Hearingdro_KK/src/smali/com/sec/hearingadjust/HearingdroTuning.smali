.class public Lcom/sec/hearingadjust/HearingdroTuning;
.super Landroid/app/Activity;
.source "HearingdroTuning.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final LEFT:I

.field private Page:I

.field private final RIGHT:I

.field private am:Landroid/media/AudioManager;

.field private backData:[I

.field private gvResult:Lcom/sec/hearingadjust/GraphView;

.field private layoutGraph:Landroid/widget/RelativeLayout;

.field private mButton:[Landroid/widget/Button;

.field private mButtonid:[I

.field private nSelectButton:I

.field private resultData:[I

.field private vTabLeft:Landroid/view/View;

.field private vTabRight:Landroid/view/View;

.field private viewManager:Lcom/sec/hearingadjust/ViewManager;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0xc

    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 21
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    .line 22
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->backData:[I

    .line 23
    iput v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->LEFT:I

    .line 24
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->RIGHT:I

    .line 25
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButtonid:[I

    .line 27
    new-array v0, v1, [Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    .line 28
    iput-object v3, p0, Lcom/sec/hearingadjust/HearingdroTuning;->layoutGraph:Landroid/widget/RelativeLayout;

    .line 30
    iput v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    .line 33
    iput v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->Page:I

    .line 36
    iput-object v3, p0, Lcom/sec/hearingadjust/HearingdroTuning;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    return-void

    .line 25
    :array_0
    .array-data 4
        0x7f0a004c
        0x7f0a004d
        0x7f0a004e
        0x7f0a004f
        0x7f0a0050
        0x7f0a0051
        0x7f0a0052
        0x7f0a0053
        0x7f0a0054
        0x7f0a0055
        0x7f0a0056
        0x7f0a0057
    .end array-data
.end method

.method private ChangeView(I)V
    .locals 1
    .param p1, "page"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    invoke-virtual {v0, p1}, Lcom/sec/hearingadjust/ViewManager;->ChageView(I)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->finish()V

    .line 54
    return-void
.end method

.method private ClickButton(I)V
    .locals 6
    .param p1, "nSelect"    # I

    .prologue
    const v5, 0x7f0a0039

    const v4, 0x7f0a0038

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 224
    sparse-switch p1, :sswitch_data_0

    .line 243
    :goto_0
    return-void

    .line 227
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->LeftGraph()V

    .line 228
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 231
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 232
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 235
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->RightGraph()V

    .line 236
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 239
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 240
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 224
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method private LeftGraph()V
    .locals 8

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->gvResult:Lcom/sec/hearingadjust/GraphView;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/4 v5, 0x3

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/4 v6, 0x4

    aget v5, v5, v6

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/4 v7, 0x5

    aget v6, v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/hearingadjust/GraphView;->setParam(IIIIII)V

    .line 215
    return-void
.end method

.method private MakeToParam()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xb

    .line 193
    const-string v0, ""

    .line 194
    .local v0, "ResultString":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 196
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 199
    return-object v0
.end method

.method private RightGraph()V
    .locals 8

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->gvResult:Lcom/sec/hearingadjust/GraphView;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/4 v3, 0x7

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/16 v4, 0x8

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/16 v5, 0x9

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/16 v6, 0xa

    aget v5, v5, v6

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    const/16 v7, 0xb

    aget v6, v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/hearingadjust/GraphView;->setParam(IIIIII)V

    .line 220
    return-void
.end method

.method private Setid()V
    .locals 3

    .prologue
    .line 67
    new-instance v1, Lcom/sec/hearingadjust/GraphView;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/GraphView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->gvResult:Lcom/sec/hearingadjust/GraphView;

    .line 69
    const v1, 0x7f0a0042

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->layoutGraph:Landroid/widget/RelativeLayout;

    .line 70
    const v1, 0x7f0a0035

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->vTabLeft:Landroid/view/View;

    .line 71
    const v1, 0x7f0a0036

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->vTabRight:Landroid/view/View;

    .line 72
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->layoutGraph:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->gvResult:Lcom/sec/hearingadjust/GraphView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    .line 76
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButtonid:[I

    aget v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    aput-object v1, v2, v0

    .line 77
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    iget v1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    invoke-direct {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->ClickButton(I)V

    .line 82
    const v1, 0x7f0a0038

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroTuning$1;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroTuning$1;-><init>(Lcom/sec/hearingadjust/HearingdroTuning;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    const v1, 0x7f0a0039

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroTuning$2;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroTuning$2;-><init>(Lcom/sec/hearingadjust/HearingdroTuning;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const/high16 v1, 0x7f0a0000

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroTuning$3;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroTuning$3;-><init>(Lcom/sec/hearingadjust/HearingdroTuning;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    const v1, 0x7f0a0001

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroTuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroTuning$4;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroTuning$4;-><init>(Lcom/sec/hearingadjust/HearingdroTuning;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    return-void
.end method

.method private XmlToResultData(Ljava/lang/String;)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 205
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 206
    .local v1, "p":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xc

    if-ge v0, v2, :cond_0

    .line 207
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 208
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->backData:[I

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    aget v3, v3, v0

    aput v3, v2, v0

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/hearingadjust/HearingdroTuning;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroTuning;

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/hearingadjust/HearingdroTuning;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroTuning;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/hearingadjust/HearingdroTuning;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroTuning;
    .param p1, "x1"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/HearingdroTuning;->ClickButton(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/hearingadjust/HearingdroTuning;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroTuning;

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->Page:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/hearingadjust/HearingdroTuning;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroTuning;
    .param p1, "x1"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/HearingdroTuning;->ChangeView(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/hearingadjust/HearingdroTuning;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroTuning;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->MakeToParam()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/hearingadjust/HearingdroTuning;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroTuning;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->am:Landroid/media/AudioManager;

    return-object v0
.end method

.method private loadPreferences()V
    .locals 4

    .prologue
    .line 176
    const-string v2, "DHA_PREF"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/hearingadjust/HearingdroTuning;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 177
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 178
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "PresetSelect"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 179
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 180
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->loadpresetxml()V

    .line 181
    return-void
.end method

.method private loadpresetxml()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 185
    const-string v2, "DHA_PREF"

    invoke-virtual {p0, v2, v4}, Lcom/sec/hearingadjust/HearingdroTuning;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 186
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    const-string v2, "Pre0"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "param":Ljava/lang/String;
    const-string v2, "page"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->Page:I

    .line 188
    invoke-direct {p0, v0}, Lcom/sec/hearingadjust/HearingdroTuning;->XmlToResultData(Ljava/lang/String;)V

    .line 189
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x28

    const/4 v7, 0x5

    .line 246
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .line 247
    .local v0, "ClickBTN":Landroid/widget/Button;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v6, 0xc

    if-ge v1, v6, :cond_0

    .line 249
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    aget-object v6, v6, v1

    if-ne v6, v0, :cond_4

    .line 251
    rem-int/lit8 v2, v1, 0x6

    .line 252
    .local v2, "nIndex":I
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    iget v8, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    add-int/2addr v8, v2

    aget v8, v6, v8

    const/4 v6, 0x6

    if-ge v1, v6, :cond_1

    const/4 v6, -0x5

    :goto_1
    add-int v5, v8, v6

    .line 253
    .local v5, "nTmp":I
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->backData:[I

    iget v8, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    add-int/2addr v8, v2

    aget v6, v6, v8

    add-int/lit8 v6, v6, 0xf

    if-le v6, v3, :cond_2

    .line 254
    .local v3, "nMaxdata":I
    :goto_2
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->backData:[I

    iget v8, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    add-int/2addr v8, v2

    aget v6, v6, v8

    add-int/lit8 v6, v6, -0xf

    if-ge v6, v7, :cond_3

    move v4, v7

    .line 255
    .local v4, "nMindata":I
    :goto_3
    if-gt v4, v5, :cond_0

    if-gt v5, v3, :cond_0

    .line 257
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->resultData:[I

    iget v7, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    add-int/2addr v7, v2

    aput v5, v6, v7

    .line 258
    iget v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    invoke-direct {p0, v6}, Lcom/sec/hearingadjust/HearingdroTuning;->ClickButton(I)V

    .line 263
    .end local v2    # "nIndex":I
    .end local v3    # "nMaxdata":I
    .end local v4    # "nMindata":I
    .end local v5    # "nTmp":I
    :cond_0
    return-void

    .restart local v2    # "nIndex":I
    :cond_1
    move v6, v7

    .line 252
    goto :goto_1

    .line 253
    .restart local v5    # "nTmp":I
    :cond_2
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->backData:[I

    iget v8, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    add-int/2addr v8, v2

    aget v6, v6, v8

    add-int/lit8 v3, v6, 0xf

    goto :goto_2

    .line 254
    .restart local v3    # "nMaxdata":I
    :cond_3
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning;->backData:[I

    iget v7, p0, Lcom/sec/hearingadjust/HearingdroTuning;->nSelectButton:I

    add-int/2addr v7, v2

    aget v6, v6, v7

    add-int/lit8 v4, v6, -0xf

    goto :goto_3

    .line 247
    .end local v2    # "nIndex":I
    .end local v3    # "nMaxdata":I
    .end local v5    # "nTmp":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 60
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroTuning;->setContentView(I)V

    .line 61
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->Setid()V

    .line 63
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 43
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroTuning;->setContentView(I)V

    .line 45
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroTuning;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->am:Landroid/media/AudioManager;

    .line 46
    new-instance v0, Lcom/sec/hearingadjust/ViewManager;

    invoke-direct {v0, p0}, Lcom/sec/hearingadjust/ViewManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroTuning;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 47
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->loadPreferences()V

    .line 48
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroTuning;->Setid()V

    .line 49
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    .line 161
    packed-switch p1, :pswitch_data_0

    .line 172
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_0
    return v2

    .line 163
    :pswitch_0
    const-string v2, "DHA_PREF"

    invoke-virtual {p0, v2, v3}, Lcom/sec/hearingadjust/HearingdroTuning;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 164
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 165
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "PresetSelect"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 166
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 167
    iget v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->Page:I

    invoke-direct {p0, v2}, Lcom/sec/hearingadjust/HearingdroTuning;->ChangeView(I)V

    .line 168
    const/4 v2, 0x1

    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x0

    .line 266
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .line 267
    .local v0, "ClickBTN":Landroid/widget/Button;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    .line 269
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_0

    .line 271
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    aget-object v2, v2, v1

    if-ne v2, v0, :cond_1

    .line 273
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    aget-object v2, v2, v1

    const v3, 0x3ecccccd    # 0.4f

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setAlpha(F)V

    .line 289
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return v4

    .line 269
    .restart local v1    # "i":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 278
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 280
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, v5, :cond_0

    .line 282
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    aget-object v2, v2, v1

    if-ne v2, v0, :cond_3

    .line 284
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroTuning;->mButton:[Landroid/widget/Button;

    aget-object v2, v2, v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setAlpha(F)V

    goto :goto_1

    .line 280
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
