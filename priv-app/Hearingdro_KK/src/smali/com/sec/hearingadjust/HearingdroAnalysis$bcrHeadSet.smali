.class public Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;
.super Landroid/content/BroadcastReceiver;
.source "HearingdroAnalysis.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/hearingadjust/HearingdroAnalysis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "bcrHeadSet"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;


# direct methods
.method public constructor <init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V
    .locals 0

    .prologue
    .line 581
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const v3, 0x7f07000f

    const/4 v2, 0x0

    .line 585
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 586
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 588
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1900(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->Mode:Z
    invoke-static {v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$2000(Lcom/sec/hearingadjust/HearingdroAnalysis;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 589
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 590
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # invokes: Lcom/sec/hearingadjust/HearingdroAnalysis;->ChangeView(I)V
    invoke-static {v1, v2}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$100(Lcom/sec/hearingadjust/HearingdroAnalysis;I)V

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 594
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$1900(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 596
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 597
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # invokes: Lcom/sec/hearingadjust/HearingdroAnalysis;->ChangeView(I)V
    invoke-static {v1, v2}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$100(Lcom/sec/hearingadjust/HearingdroAnalysis;I)V

    goto :goto_0
.end method
