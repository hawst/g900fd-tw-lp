.class public Lcom/sec/hearingadjust/HearingdroResult_actionbar;
.super Landroid/app/Activity;
.source "HearingdroResult_actionbar.java"


# instance fields
.field private ArrayString:[Ljava/lang/String;

.field private final LEFT:I

.field private final RIGHT:I

.field private SelectEar:I

.field private SelectPreset:I

.field private adapter:Landroid/widget/ArrayAdapter;

.field private am:Landroid/media/AudioManager;

.field private cb_lv_setting:Landroid/widget/ListView;

.field private cb_state:[Z

.field private cb_txt:[Ljava/lang/String;

.field private gvResult:Lcom/sec/hearingadjust/GraphView;

.field private layoutGraph:Landroid/widget/RelativeLayout;

.field private lv_preset:Landroid/widget/ListView;

.field private lv_setting:Landroid/widget/ListView;

.field private myPresetview:Lcom/sec/hearingadjust/SettingView;

.field private mySettingview:Lcom/sec/hearingadjust/SettingView;

.field private nSelectbutton:I

.field private resultData:[I

.field private scroll:Landroid/widget/ScrollView;

.field private vTabLeft:Landroid/view/View;

.field private vTabRight:Landroid/view/View;

.field private viewManager:Lcom/sec/hearingadjust/ViewManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    const/16 v0, 0xc

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    .line 26
    iput v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->LEFT:I

    .line 27
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->RIGHT:I

    .line 32
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->lv_setting:Landroid/widget/ListView;

    .line 33
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->lv_preset:Landroid/widget/ListView;

    .line 34
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    .line 35
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    .line 36
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->layoutGraph:Landroid/widget/RelativeLayout;

    .line 37
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->scroll:Landroid/widget/ScrollView;

    .line 38
    iput v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->nSelectbutton:I

    .line 40
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 44
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_lv_setting:Landroid/widget/ListView;

    .line 45
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_txt:[Ljava/lang/String;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_state:[Z

    .line 47
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->adapter:Landroid/widget/ArrayAdapter;

    return-void

    .line 46
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method private ChangeView(I)V
    .locals 1
    .param p1, "Page"    # I

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    invoke-virtual {v0, p1}, Lcom/sec/hearingadjust/ViewManager;->ChageView(I)V

    .line 350
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->finish()V

    .line 351
    return-void
.end method

.method private ClickButton(I)V
    .locals 6
    .param p1, "nSelect"    # I

    .prologue
    const v5, 0x7f0a0039

    const v4, 0x7f0a0038

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 99
    sparse-switch p1, :sswitch_data_0

    .line 118
    :goto_0
    return-void

    .line 102
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->LeftGraph()V

    .line 103
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 106
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 107
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 110
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->RightGraph()V

    .line 111
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 114
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 115
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 99
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method private LeftGraph()V
    .locals 8

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->gvResult:Lcom/sec/hearingadjust/GraphView;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/4 v5, 0x3

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/4 v6, 0x4

    aget v5, v5, v6

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/4 v7, 0x5

    aget v6, v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/hearingadjust/GraphView;->setParam(IIIIII)V

    .line 356
    return-void
.end method

.method private MakeToParam()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xb

    .line 386
    const-string v0, ""

    .line 387
    .local v0, "ResultString":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 389
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 387
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 391
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 392
    return-object v0
.end method

.method private RightGraph()V
    .locals 8

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->gvResult:Lcom/sec/hearingadjust/GraphView;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/4 v3, 0x7

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/16 v4, 0x8

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/16 v5, 0x9

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/16 v6, 0xa

    aget v5, v5, v6

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    const/16 v7, 0xb

    aget v6, v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/hearingadjust/GraphView;->setParam(IIIIII)V

    .line 361
    return-void
.end method

.method private Setid()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 181
    const v2, 0x7f0a0035

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->vTabLeft:Landroid/view/View;

    .line 182
    const v2, 0x7f0a0036

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->vTabRight:Landroid/view/View;

    .line 183
    const v2, 0x7f0a004b

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->lv_setting:Landroid/widget/ListView;

    .line 184
    const v2, 0x7f0a0049

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->lv_preset:Landroid/widget/ListView;

    .line 185
    const v2, 0x7f0a0042

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->layoutGraph:Landroid/widget/RelativeLayout;

    .line 186
    const v2, 0x7f0a0041

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->scroll:Landroid/widget/ScrollView;

    .line 190
    const v2, 0x7f0a004a

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_lv_setting:Landroid/widget/ListView;

    .line 191
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v5, 0x7f030008

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_txt:[Ljava/lang/String;

    invoke-direct {v2, p0, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->adapter:Landroid/widget/ArrayAdapter;

    .line 194
    new-instance v2, Lcom/sec/hearingadjust/GraphView;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/GraphView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->gvResult:Lcom/sec/hearingadjust/GraphView;

    .line 195
    new-instance v2, Lcom/sec/hearingadjust/SettingView;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/SettingView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    .line 196
    new-instance v2, Lcom/sec/hearingadjust/SettingView;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/SettingView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    .line 197
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->layoutGraph:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->gvResult:Lcom/sec/hearingadjust/GraphView;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 199
    iget v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->nSelectbutton:I

    invoke-direct {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->ClickButton(I)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hearing_revision"

    invoke-static {v2, v5, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 202
    .local v0, "nCallCheckTmp":I
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hearing_direction"

    invoke-static {v2, v5, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectEar:I

    .line 203
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hearing_musiccheck"

    invoke-static {v2, v5, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 205
    .local v1, "nMusicCheckTmp":I
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KYU  = (0) [CALL db : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "[MUSIC db : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 207
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    invoke-virtual {v2}, Lcom/sec/hearingadjust/SettingView;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 209
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    iget v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectEar:I

    invoke-virtual {v2, v4, v5}, Lcom/sec/hearingadjust/SettingView;->setAddList(II)V

    .line 212
    :cond_0
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    invoke-virtual {v2}, Lcom/sec/hearingadjust/SettingView;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 215
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    iget v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectPreset:I

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v3, v5}, Lcom/sec/hearingadjust/SettingView;->setAddList(II)V

    .line 219
    :cond_1
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->lv_setting:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 220
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->lv_preset:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 223
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_lv_setting:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 224
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_lv_setting:Landroid/widget/ListView;

    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 228
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_state:[Z

    if-ne v0, v3, :cond_2

    move v2, v3

    :goto_0
    aput-boolean v2, v5, v4

    .line 229
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_state:[Z

    if-ne v1, v3, :cond_3

    move v2, v3

    :goto_1
    aput-boolean v2, v5, v3

    .line 231
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KYU  = (1) [CALL db : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "[MUSIC db : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 233
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_lv_setting:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_state:[Z

    aget-boolean v5, v5, v4

    invoke-virtual {v2, v4, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 234
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_lv_setting:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_state:[Z

    aget-boolean v4, v4, v3

    invoke-virtual {v2, v3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 237
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->scrollToStart()V

    .line 238
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->scroll:Landroid/widget/ScrollView;

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->requestDisallowInterceptTouchEvent(Z)V

    .line 239
    const v2, 0x7f0a0038

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$4;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$4;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    const v2, 0x7f0a0039

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$5;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$5;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    const v2, 0x7f0a0047

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    const v2, 0x7f0a0048

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$7;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$7;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_lv_setting:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$8;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$8;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 324
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->lv_setting:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$9;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$9;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 340
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->lv_preset:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$10;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$10;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 346
    return-void

    :cond_2
    move v2, v4

    .line 228
    goto/16 :goto_0

    :cond_3
    move v2, v4

    .line 229
    goto/16 :goto_1
.end method

.method private XmlToResultData(Ljava/lang/String;)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 398
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 399
    .local v1, "p":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xc

    if-ge v0, v2, :cond_0

    .line 400
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->resultData:[I

    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 399
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 402
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->scroll:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectPreset:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/hearingadjust/HearingdroResult_actionbar;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->ChangeView(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/hearingadjust/HearingdroResult_actionbar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectPreset:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_state:[Z

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_lv_setting:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->popSelect()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->popPresetSelect()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Lcom/sec/hearingadjust/SettingView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->loadPreferences()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->nSelectbutton:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/hearingadjust/HearingdroResult_actionbar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->nSelectbutton:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/hearingadjust/HearingdroResult_actionbar;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->ClickButton(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectEar:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/hearingadjust/HearingdroResult_actionbar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectEar:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Lcom/sec/hearingadjust/SettingView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->am:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->MakeToParam()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadPreferences()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 364
    const-string v2, "DHA_PREF"

    invoke-virtual {p0, v2, v4}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 365
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    const-string v2, "isPreset"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectPreset:I

    .line 366
    const-string v2, "PresetSelect"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 367
    .local v0, "k":I
    if-nez v0, :cond_0

    .line 369
    invoke-direct {p0, v4}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->loadpresetxml(I)V

    .line 375
    :goto_0
    return-void

    .line 373
    :cond_0
    iget v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectPreset:I

    invoke-direct {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->loadpresetxml(I)V

    goto :goto_0
.end method

.method private loadpresetxml(I)V
    .locals 4
    .param p1, "nData"    # I

    .prologue
    .line 379
    const-string v2, "DHA_PREF"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 380
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pre"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 381
    .local v0, "param":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->XmlToResultData(Ljava/lang/String;)V

    .line 382
    return-void
.end method

.method private popPresetSelect()V
    .locals 4

    .prologue
    .line 130
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 131
    .local v0, "popSelector":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f070003

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 132
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->ArrayString:[Ljava/lang/String;

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectPreset:I

    add-int/lit8 v2, v2, -0x1

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$2;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$2;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 149
    const v1, 0x7f07000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 150
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 151
    return-void
.end method

.method private popSelect()V
    .locals 4

    .prologue
    .line 155
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 156
    .local v0, "popSelector":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f070029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 157
    const/high16 v1, 0x7f050000

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->SelectEar:I

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_actionbar$3;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$3;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 169
    const v1, 0x7f07000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 170
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 171
    return-void
.end method

.method private scrollToStart()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->scroll:Landroid/widget/ScrollView;

    new-instance v1, Lcom/sec/hearingadjust/HearingdroResult_actionbar$1;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar$1;-><init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 126
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 176
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->setContentView(I)V

    .line 177
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->Setid()V

    .line 178
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v7, 0x7f070000

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 53
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 57
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->setContentView(I)V

    .line 60
    new-array v0, v6, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_txt:[Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_txt:[Ljava/lang/String;

    const v1, 0x7f070028

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 65
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->cb_txt:[Ljava/lang/String;

    const v1, 0x7f07002b

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 70
    new-instance v0, Lcom/sec/hearingadjust/ViewManager;

    invoke-direct {v0, p0}, Lcom/sec/hearingadjust/ViewManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 71
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->am:Landroid/media/AudioManager;

    .line 72
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->am:Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 73
    new-array v0, v6, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->ArrayString:[Ljava/lang/String;

    .line 74
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->loadPreferences()V

    .line 75
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->Setid()V

    .line 76
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 81
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 86
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a005a

    if-ne v1, v2, :cond_0

    .line 87
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->ChangeView(I)V

    .line 94
    :goto_0
    return v0

    .line 90
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->finish()V

    goto :goto_0

    .line 94
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
