.class public Lcom/sec/hearingadjust/SoundManager;
.super Ljava/lang/Object;
.source "SoundManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/hearingadjust/SoundManager$SoundData;
    }
.end annotation


# instance fields
.field private SoundPool:Landroid/media/SoundPool;

.field private arrayItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/hearingadjust/SoundManager$SoundData;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private streamIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/hearingadjust/SoundManager;->arrayItemList:Ljava/util/ArrayList;

    .line 16
    iput-object p1, p0, Lcom/sec/hearingadjust/SoundManager;->context:Landroid/content/Context;

    .line 17
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x6

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/hearingadjust/SoundManager;->SoundPool:Landroid/media/SoundPool;

    .line 18
    return-void
.end method


# virtual methods
.method public Play(I)V
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 23
    iget-object v0, p0, Lcom/sec/hearingadjust/SoundManager;->arrayItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/hearingadjust/SoundManager$SoundData;

    iget v1, v0, Lcom/sec/hearingadjust/SoundManager$SoundData;->nSoundID:I

    .line 24
    .local v1, "soundId":I
    iget-object v0, p0, Lcom/sec/hearingadjust/SoundManager;->SoundPool:Landroid/media/SoundPool;

    const/16 v4, 0x64

    const/4 v5, -0x1

    move v3, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v0

    iput v0, p0, Lcom/sec/hearingadjust/SoundManager;->streamIndex:I

    .line 25
    return-void
.end method

.method public releaseSound()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/SoundManager;->SoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 31
    return-void
.end method

.method public setAddsound(II)V
    .locals 5
    .param p1, "Index"    # I
    .param p2, "SoundID"    # I

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/hearingadjust/SoundManager;->arrayItemList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/hearingadjust/SoundManager$SoundData;

    iget-object v2, p0, Lcom/sec/hearingadjust/SoundManager;->SoundPool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/hearingadjust/SoundManager;->context:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, p2, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    invoke-direct {v1, p1, v2}, Lcom/sec/hearingadjust/SoundManager$SoundData;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method public stopSound()V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/hearingadjust/SoundManager;->SoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/hearingadjust/SoundManager;->streamIndex:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->stop(I)V

    .line 28
    return-void
.end method
