.class Lcom/sec/hearingadjust/HearingdroTuning$4;
.super Ljava/lang/Object;
.source "HearingdroTuning.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/HearingdroTuning;->Setid()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/HearingdroTuning;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/HearingdroTuning;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v11, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 113
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    const-string v7, "DHA_PREF"

    invoke-virtual {v6, v7, v9}, Lcom/sec/hearingadjust/HearingdroTuning;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 114
    .local v5, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 115
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v6, "isPreset"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 116
    .local v1, "SelectPreset":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Pre"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    # invokes: Lcom/sec/hearingadjust/HearingdroTuning;->MakeToParam()Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/hearingadjust/HearingdroTuning;->access$400(Lcom/sec/hearingadjust/HearingdroTuning;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 117
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 120
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-virtual {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "hearing_parameters"

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    # invokes: Lcom/sec/hearingadjust/HearingdroTuning;->MakeToParam()Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/hearingadjust/HearingdroTuning;->access$400(Lcom/sec/hearingadjust/HearingdroTuning;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 121
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-virtual {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "hearing_direction"

    invoke-static {v6, v7, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 122
    .local v0, "SelectEar":I
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-virtual {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "hearing_revision"

    invoke-static {v6, v7, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 124
    .local v4, "nCallCheckTmp":I
    const-string v2, ""

    .line 125
    .local v2, "dhaParam":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 126
    const/4 v0, -0x1

    .line 131
    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 149
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    # invokes: Lcom/sec/hearingadjust/HearingdroTuning;->MakeToParam()Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/hearingadjust/HearingdroTuning;->access$400(Lcom/sec/hearingadjust/HearingdroTuning;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 150
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    # getter for: Lcom/sec/hearingadjust/HearingdroTuning;->am:Landroid/media/AudioManager;
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->access$500(Lcom/sec/hearingadjust/HearingdroTuning;)Landroid/media/AudioManager;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dha="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 152
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    # getter for: Lcom/sec/hearingadjust/HearingdroTuning;->Page:I
    invoke-static {v7}, Lcom/sec/hearingadjust/HearingdroTuning;->access$200(Lcom/sec/hearingadjust/HearingdroTuning;)I

    move-result v7

    # invokes: Lcom/sec/hearingadjust/HearingdroTuning;->ChangeView(I)V
    invoke-static {v6, v7}, Lcom/sec/hearingadjust/HearingdroTuning;->access$300(Lcom/sec/hearingadjust/HearingdroTuning;I)V

    .line 153
    return-void

    .line 127
    :cond_1
    if-ne v4, v10, :cond_0

    if-ne v0, v11, :cond_0

    .line 128
    const/4 v0, 0x1

    goto :goto_0

    .line 133
    :pswitch_0
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-virtual {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "hearing_revision"

    invoke-static {v6, v7, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 134
    const-string v2, "0,1"

    .line 135
    goto :goto_1

    .line 137
    :pswitch_1
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-virtual {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "hearing_revision"

    invoke-static {v6, v7, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 138
    const-string v2, "1,0"

    .line 139
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-virtual {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "hearing_direction"

    invoke-static {v6, v7, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    .line 142
    :pswitch_2
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-virtual {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "hearing_revision"

    invoke-static {v6, v7, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 143
    const-string v2, "1,1"

    .line 144
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroTuning$4;->this$0:Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-virtual {v6}, Lcom/sec/hearingadjust/HearingdroTuning;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "hearing_direction"

    invoke-static {v6, v7, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 131
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
