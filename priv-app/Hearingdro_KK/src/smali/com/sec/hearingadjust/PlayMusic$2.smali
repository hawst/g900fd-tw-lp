.class Lcom/sec/hearingadjust/PlayMusic$2;
.super Ljava/lang/Object;
.source "PlayMusic.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/PlayMusic;->AfterFunc()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/PlayMusic;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/PlayMusic;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/16 v11, 0x780

    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 137
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$600(Lcom/sec/hearingadjust/PlayMusic;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 138
    new-array v0, v11, [S

    .line 139
    .local v0, "Tmpinjni":[S
    new-array v1, v11, [S

    .line 140
    .local v1, "Tmpoutjni":[S
    const/4 v4, 0x0

    .line 141
    .local v4, "k":I
    const/4 v2, 0x0

    .local v2, "i":I
    move v5, v4

    .end local v4    # "k":I
    .local v5, "k":I
    :goto_1
    const v7, 0xbc278

    if-ge v2, v7, :cond_3

    .line 143
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "k":I
    .restart local v4    # "k":I
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    iget-object v8, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->buffer:[S
    invoke-static {v8}, Lcom/sec/hearingadjust/PlayMusic;->access$100(Lcom/sec/hearingadjust/PlayMusic;)[S

    move-result-object v8

    aget-short v8, v8, v2

    # invokes: Lcom/sec/hearingadjust/PlayMusic;->CheckData(SI)S
    invoke-static {v7, v8, v2}, Lcom/sec/hearingadjust/PlayMusic;->access$200(Lcom/sec/hearingadjust/PlayMusic;SI)S

    move-result v7

    aput-short v7, v0, v5

    .line 144
    if-ne v4, v11, :cond_2

    .line 146
    const/4 v4, 0x0

    .line 147
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->JNILib:Lcom/sec/hearingLib/HearingLib;
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$700(Lcom/sec/hearingadjust/PlayMusic;)Lcom/sec/hearingLib/HearingLib;

    move-result-object v7

    array-length v8, v0

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v7, v0, v1, v8}, Lcom/sec/hearingLib/HearingLib;->SamsungMusicExe([S[SI)I

    .line 148
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;

    move-result-object v7

    array-length v8, v1

    invoke-virtual {v7, v1, v9, v8}, Landroid/media/AudioTrack;->write([SII)I

    .line 149
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$600(Lcom/sec/hearingadjust/PlayMusic;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 151
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;

    move-result-object v7

    invoke-virtual {v7}, Landroid/media/AudioTrack;->stop()V

    .line 152
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;

    move-result-object v7

    invoke-virtual {v7}, Landroid/media/AudioTrack;->release()V

    .line 153
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    const v8, 0xac44

    invoke-virtual {v7, v8}, Lcom/sec/hearingadjust/PlayMusic;->InitMusic(I)V

    .line 154
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$400(Lcom/sec/hearingadjust/PlayMusic;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 173
    .end local v0    # "Tmpinjni":[S
    .end local v1    # "Tmpoutjni":[S
    .end local v2    # "i":I
    .end local v4    # "k":I
    :cond_1
    return-void

    .line 141
    .restart local v0    # "Tmpinjni":[S
    .restart local v1    # "Tmpoutjni":[S
    .restart local v2    # "i":I
    .restart local v4    # "k":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    .end local v4    # "k":I
    .restart local v5    # "k":I
    goto :goto_1

    .line 159
    :cond_3
    new-array v3, v5, [S

    .line 160
    .local v3, "injni":[S
    new-array v6, v5, [S

    .line 161
    .local v6, "outjni":[S
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_4

    .line 163
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    aget-short v8, v0, v2

    # invokes: Lcom/sec/hearingadjust/PlayMusic;->CheckData(SI)S
    invoke-static {v7, v8, v2}, Lcom/sec/hearingadjust/PlayMusic;->access$200(Lcom/sec/hearingadjust/PlayMusic;SI)S

    move-result v7

    aput-short v7, v3, v2

    .line 161
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 165
    :cond_4
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->JNILib:Lcom/sec/hearingLib/HearingLib;
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$700(Lcom/sec/hearingadjust/PlayMusic;)Lcom/sec/hearingLib/HearingLib;

    move-result-object v7

    array-length v8, v3

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v7, v3, v6, v8}, Lcom/sec/hearingLib/HearingLib;->SamsungMusicExe([S[SI)I

    .line 166
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;

    move-result-object v7

    array-length v8, v6

    invoke-virtual {v7, v6, v9, v8}, Landroid/media/AudioTrack;->write([SII)I

    .line 167
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # operator++ for: Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$508(Lcom/sec/hearingadjust/PlayMusic;)I

    .line 168
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$500(Lcom/sec/hearingadjust/PlayMusic;)I

    move-result v7

    if-ne v7, v10, :cond_0

    .line 169
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/hearingadjust/PlayMusic;->access$400(Lcom/sec/hearingadjust/PlayMusic;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 170
    iget-object v7, p0, Lcom/sec/hearingadjust/PlayMusic$2;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # setter for: Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z
    invoke-static {v7, v9}, Lcom/sec/hearingadjust/PlayMusic;->access$602(Lcom/sec/hearingadjust/PlayMusic;Z)Z

    goto/16 :goto_0
.end method
