.class public Lcom/sec/hearingadjust/HearingdroInstruct;
.super Landroid/app/Activity;
.source "HearingdroInstruct.java"


# static fields
.field public static final PREF_NAME:Ljava/lang/String; = "DHA_PREF"


# instance fields
.field private am:Landroid/media/AudioManager;

.field private tvInst1:Landroid/widget/TextView;

.field private tvInst2:Landroid/widget/TextView;

.field private tvInst3:Landroid/widget/TextView;

.field private viewManager:Lcom/sec/hearingadjust/ViewManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    return-void
.end method

.method private ChangeView(I)V
    .locals 1
    .param p1, "page"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    invoke-virtual {v0, p1}, Lcom/sec/hearingadjust/ViewManager;->ChageView(I)V

    .line 89
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->finish()V

    .line 90
    return-void
.end method

.method private Setid()V
    .locals 3

    .prologue
    .line 71
    const v0, 0x7f0a0030

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroInstruct;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst1:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0a0031

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroInstruct;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst2:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0a0032

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroInstruct;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst3:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst1:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "1. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst1:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst2:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst2:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst3:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "3. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->tvInst3:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    const v0, 0x7f0a002f

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroInstruct;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/hearingadjust/HearingdroInstruct$1;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/HearingdroInstruct$1;-><init>(Lcom/sec/hearingadjust/HearingdroInstruct;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    return-void
.end method

.method private StartAnalysis()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 93
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->am:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->am:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    const v0, 0x7f07000f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 99
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->am:Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 97
    invoke-direct {p0, v3}, Lcom/sec/hearingadjust/HearingdroInstruct;->ChangeView(I)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/hearingadjust/HearingdroInstruct;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroInstruct;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->StartAnalysis()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 66
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroInstruct;->setContentView(I)V

    .line 67
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->Setid()V

    .line 69
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v5, 0x7f030002

    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/HearingdroInstruct;->setContentView(I)V

    .line 28
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 29
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 30
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 31
    new-instance v5, Lcom/sec/hearingadjust/ViewManager;

    invoke-direct {v5, p0}, Lcom/sec/hearingadjust/ViewManager;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 32
    const-string v5, "audio"

    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/HearingdroInstruct;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/media/AudioManager;

    iput-object v5, p0, Lcom/sec/hearingadjust/HearingdroInstruct;->am:Landroid/media/AudioManager;

    .line 33
    const-string v5, "DHA_PREF"

    invoke-virtual {p0, v5, v6}, Lcom/sec/hearingadjust/HearingdroInstruct;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 34
    .local v4, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 35
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "PresetSelect"

    invoke-interface {v1, v5, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 36
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 37
    const-string v5, "isFirstRun"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 38
    .local v2, "isFirstRun":Z
    const-string v5, "CheckDone"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 39
    .local v3, "nDoneCheck":I
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "VIEW"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 41
    .local v0, "ChangeView":Z
    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 42
    if-nez v3, :cond_1

    .line 43
    const/4 v5, 0x2

    invoke-direct {p0, v5}, Lcom/sec/hearingadjust/HearingdroInstruct;->ChangeView(I)V

    .line 49
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->Setid()V

    .line 51
    return-void

    .line 46
    :cond_1
    const/4 v5, 0x3

    invoke-direct {p0, v5}, Lcom/sec/hearingadjust/HearingdroInstruct;->ChangeView(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 55
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->finish()V

    .line 57
    const/4 v0, 0x1

    .line 59
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public setLocale(Ljava/lang/String;)V
    .locals 4
    .param p1, "charicter"    # Ljava/lang/String;

    .prologue
    .line 102
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 103
    .local v1, "locale":Ljava/util/Locale;
    invoke-static {v1}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 104
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 105
    .local v0, "config":Landroid/content/res/Configuration;
    iput-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 106
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroInstruct;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 107
    return-void
.end method
