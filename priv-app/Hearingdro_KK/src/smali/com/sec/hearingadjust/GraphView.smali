.class public Lcom/sec/hearingadjust/GraphView;
.super Landroid/view/View;
.source "GraphView.java"


# instance fields
.field private coordX:[F

.field private coordY:[F

.field private density:F

.field private resultData:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, 0x7

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    .line 17
    const/16 v0, 0xa

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    .line 20
    const/16 v0, 0xc

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/hearingadjust/GraphView;->density:F

    .line 22
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 38
    const/4 v4, 0x0

    .line 39
    .local v4, "pathLeft":Landroid/graphics/Path;
    const/4 v5, 0x0

    .line 41
    .local v5, "pathRight":Landroid/graphics/Path;
    invoke-virtual {p0}, Lcom/sec/hearingadjust/GraphView;->getWidth()I

    move-result v8

    .line 42
    .local v8, "width":I
    invoke-virtual {p0}, Lcom/sec/hearingadjust/GraphView;->getHeight()I

    move-result v2

    .line 44
    .local v2, "height":I
    int-to-float v9, v8

    const/high16 v10, 0x40a00000    # 5.0f

    div-float v0, v9, v10

    .line 45
    .local v0, "averageX":F
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x0

    const/4 v11, 0x0

    aput v11, v9, v10

    .line 46
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v12, 0x0

    aget v11, v11, v12

    add-float/2addr v11, v0

    aput v11, v9, v10

    .line 47
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x2

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    add-float/2addr v11, v0

    aput v11, v9, v10

    .line 48
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x3

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v12, 0x2

    aget v11, v11, v12

    add-float/2addr v11, v0

    aput v11, v9, v10

    .line 49
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x4

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v12, 0x3

    aget v11, v11, v12

    add-float/2addr v11, v0

    aput v11, v9, v10

    .line 50
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x5

    int-to-float v11, v8

    aput v11, v9, v10

    .line 54
    int-to-float v9, v2

    const/high16 v10, 0x41100000    # 9.0f

    div-float v1, v9, v10

    .line 55
    .local v1, "averageY":F
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v10, 0x0

    const/4 v11, 0x0

    aput v11, v9, v10

    .line 56
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v12, 0x0

    aget v11, v11, v12

    add-float/2addr v11, v1

    aput v11, v9, v10

    .line 57
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v10, 0x2

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    add-float/2addr v11, v1

    aput v11, v9, v10

    .line 58
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v10, 0x3

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v12, 0x2

    aget v11, v11, v12

    add-float/2addr v11, v1

    aput v11, v9, v10

    .line 59
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v10, 0x4

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v12, 0x3

    aget v11, v11, v12

    add-float/2addr v11, v1

    aput v11, v9, v10

    .line 60
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v10, 0x5

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v12, 0x4

    aget v11, v11, v12

    add-float/2addr v11, v1

    aput v11, v9, v10

    .line 61
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v10, 0x6

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v12, 0x5

    aget v11, v11, v12

    add-float/2addr v11, v1

    aput v11, v9, v10

    .line 62
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v10, 0x7

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v12, 0x6

    aget v11, v11, v12

    add-float/2addr v11, v1

    aput v11, v9, v10

    .line 63
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/16 v10, 0x8

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/4 v12, 0x7

    aget v11, v11, v12

    add-float/2addr v11, v1

    aput v11, v9, v10

    .line 64
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    const/16 v10, 0x9

    int-to-float v11, v2

    aput v11, v9, v10

    .line 67
    new-instance v4, Landroid/graphics/Path;

    .end local v4    # "pathLeft":Landroid/graphics/Path;
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 68
    .restart local v4    # "pathLeft":Landroid/graphics/Path;
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 70
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x0

    aget v9, v9, v10

    iget-object v10, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v12, 0x0

    aget v11, v11, v12

    div-int/lit8 v11, v11, 0x5

    aget v10, v10, v11

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v12, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v13, 0x0

    aget v12, v12, v13

    div-int/lit8 v12, v12, 0x5

    add-int/lit8 v12, v12, 0x1

    aget v11, v11, v12

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 71
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    const/4 v9, 0x6

    if-ge v3, v9, :cond_0

    .line 72
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    aget v9, v9, v3

    iget-object v10, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    aget v11, v11, v3

    div-int/lit8 v11, v11, 0x5

    aget v10, v10, v11

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v12, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    aget v12, v12, v3

    div-int/lit8 v12, v12, 0x5

    add-int/lit8 v12, v12, 0x1

    aget v11, v11, v12

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 71
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 74
    :cond_0
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x5

    aget v9, v9, v10

    iget-object v10, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v12, 0x5

    aget v11, v11, v12

    div-int/lit8 v11, v11, 0x5

    aget v10, v10, v11

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v12, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v13, 0x5

    aget v12, v12, v13

    div-int/lit8 v12, v12, 0x5

    add-int/lit8 v12, v12, 0x1

    aget v11, v11, v12

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 82
    new-instance v6, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v9, Landroid/graphics/drawable/shapes/PathShape;

    int-to-float v10, v8

    int-to-float v11, v2

    invoke-direct {v9, v4, v10, v11}, Landroid/graphics/drawable/shapes/PathShape;-><init>(Landroid/graphics/Path;FF)V

    invoke-direct {v6, v9}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 83
    .local v6, "sdPathLeft":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v6}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sec/hearingadjust/GraphView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f060003

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    invoke-virtual {v6}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    new-instance v10, Landroid/graphics/CornerPathEffect;

    iget v11, p0, Lcom/sec/hearingadjust/GraphView;->density:F

    const/high16 v12, 0x41c80000    # 25.0f

    mul-float/2addr v11, v12

    invoke-direct {v10, v11}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 85
    invoke-virtual {v6}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    sget-object v10, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 86
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10, v8, v2}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 89
    new-instance v5, Landroid/graphics/Path;

    .end local v5    # "pathRight":Landroid/graphics/Path;
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 90
    .restart local v5    # "pathRight":Landroid/graphics/Path;
    invoke-virtual {v5}, Landroid/graphics/Path;->reset()V

    .line 92
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x0

    aget v9, v9, v10

    iget-object v10, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v12, 0x6

    aget v11, v11, v12

    div-int/lit8 v11, v11, 0x5

    aget v10, v10, v11

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v12, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v13, 0x6

    aget v12, v12, v13

    div-int/lit8 v12, v12, 0x5

    add-int/lit8 v12, v12, 0x1

    aget v11, v11, v12

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v5, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 93
    const/4 v3, 0x1

    :goto_1
    const/4 v9, 0x6

    if-ge v3, v9, :cond_1

    .line 94
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    aget v9, v9, v3

    iget-object v10, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    add-int/lit8 v12, v3, 0x6

    aget v11, v11, v12

    div-int/lit8 v11, v11, 0x5

    aget v10, v10, v11

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v12, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    add-int/lit8 v13, v3, 0x6

    aget v12, v12, v13

    div-int/lit8 v12, v12, 0x5

    add-int/lit8 v12, v12, 0x1

    aget v11, v11, v12

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v5, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 96
    :cond_1
    iget-object v9, p0, Lcom/sec/hearingadjust/GraphView;->coordX:[F

    const/4 v10, 0x5

    aget v9, v9, v10

    iget-object v10, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/16 v12, 0xb

    aget v11, v11, v12

    div-int/lit8 v11, v11, 0x5

    aget v10, v10, v11

    iget-object v11, p0, Lcom/sec/hearingadjust/GraphView;->coordY:[F

    iget-object v12, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/16 v13, 0xb

    aget v12, v12, v13

    div-int/lit8 v12, v12, 0x5

    add-int/lit8 v12, v12, 0x1

    aget v11, v11, v12

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v5, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 105
    new-instance v7, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v9, Landroid/graphics/drawable/shapes/PathShape;

    int-to-float v10, v8

    int-to-float v11, v2

    invoke-direct {v9, v5, v10, v11}, Landroid/graphics/drawable/shapes/PathShape;-><init>(Landroid/graphics/Path;FF)V

    invoke-direct {v7, v9}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 106
    .local v7, "sdPathRight":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sec/hearingadjust/GraphView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const/high16 v11, 0x7f060000

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    new-instance v10, Landroid/graphics/CornerPathEffect;

    iget v11, p0, Lcom/sec/hearingadjust/GraphView;->density:F

    const/high16 v12, 0x41c80000    # 25.0f

    mul-float/2addr v11, v12

    invoke-direct {v10, v11}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 108
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    sget-object v10, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 109
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10, v8, v2}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 112
    invoke-virtual {v6}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    iget v10, p0, Lcom/sec/hearingadjust/GraphView;->density:F

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 113
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    iget v10, p0, Lcom/sec/hearingadjust/GraphView;->density:F

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 114
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 115
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 153
    return-void
.end method

.method public setParam(IIIIII)V
    .locals 3
    .param p1, "HL250"    # I
    .param p2, "HL500"    # I
    .param p3, "HL1000"    # I
    .param p4, "HL2000"    # I
    .param p5, "HL4000"    # I
    .param p6, "HL8000"    # I

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v1, 0x6

    div-int/lit8 v2, p1, 0x4

    aput v2, v0, v1

    .line 26
    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v1, 0x7

    div-int/lit8 v2, p2, 0x4

    aput v2, v0, v1

    .line 27
    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v1, 0x2

    aput p3, v0, v1

    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/16 v1, 0x8

    div-int/lit8 v2, p3, 0x4

    aput v2, v0, v1

    .line 28
    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v1, 0x3

    aput p4, v0, v1

    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/16 v1, 0x9

    div-int/lit8 v2, p4, 0x4

    aput v2, v0, v1

    .line 29
    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v1, 0x4

    aput p5, v0, v1

    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/16 v1, 0xa

    div-int/lit8 v2, p5, 0x4

    aput v2, v0, v1

    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/4 v1, 0x5

    aput p6, v0, v1

    iget-object v0, p0, Lcom/sec/hearingadjust/GraphView;->resultData:[I

    const/16 v1, 0xb

    div-int/lit8 v2, p6, 0x4

    aput v2, v0, v1

    .line 31
    invoke-virtual {p0}, Lcom/sec/hearingadjust/GraphView;->invalidate()V

    .line 32
    return-void
.end method
