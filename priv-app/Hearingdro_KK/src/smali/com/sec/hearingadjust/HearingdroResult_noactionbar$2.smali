.class Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;
.super Ljava/lang/Object;
.source "HearingdroResult_noactionbar.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->popPresetSelect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x0

    .line 116
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # setter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I
    invoke-static {v2, p2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$102(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)I

    .line 117
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$200(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Lcom/sec/hearingadjust/SettingView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I
    invoke-static {v3}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$100(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)I

    move-result v3

    invoke-virtual {v2, v5, v3}, Lcom/sec/hearingadjust/SettingView;->ChangeState(II)V

    .line 118
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    const-string v3, "DHA_PREF"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 119
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 120
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "isPreset"

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I
    invoke-static {v3}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$100(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 121
    const-string v2, "PresetSelect"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 122
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 123
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$200(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Lcom/sec/hearingadjust/SettingView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/hearingadjust/SettingView;->notifyDataSetChanged()V

    .line 124
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->loadPreferences()V
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$300(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    .line 125
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # setter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->nSelectbutton:I
    invoke-static {v2, v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$402(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)I

    .line 126
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;->this$0:Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->nSelectbutton:I
    invoke-static {v3}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$400(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)I

    move-result v3

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->ClickButton(I)V
    invoke-static {v2, v3}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->access$500(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)V

    .line 127
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 128
    return-void
.end method
