.class public Lcom/sec/hearingadjust/PlayMusic;
.super Landroid/app/Activity;
.source "PlayMusic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;
    }
.end annotation


# static fields
.field private static final BOTH:I = 0x2

.field private static final LEFT:I = 0x1

.field private static final MSG_BTN_AFTER_DISABLE:I = 0x3

.field private static final MSG_BTN_AFTER_ENABLE:I = 0x2

.field private static final MSG_BTN_BEFORE_DISABLE:I = 0x1

.field private static final MSG_BTN_BEFORE_ENABLE:I

.field private static final RIGHT:I


# instance fields
.field private AfterMode:Z

.field private AfterThread:Ljava/lang/Thread;

.field private BeforeMode:Z

.field private BeforeThread:Ljava/lang/Thread;

.field private JNILib:Lcom/sec/hearingLib/HearingLib;

.field private Mode:Z

.field private MusicPlayCnt:I

.field private MusicTrack:Landroid/media/AudioTrack;

.field private Page:I

.field private ResultData:[S

.field private SelectButton:I

.field private am:Landroid/media/AudioManager;

.field private bcr:Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;

.field private buffer:[S

.field private final fileLength:I

.field private final uiHandler:Landroid/os/Handler;

.field private vTabBoth:Landroid/view/View;

.field private vTabLeft:Landroid/view/View;

.field private vTabRight:Landroid/view/View;

.field private viewManager:Lcom/sec/hearingadjust/ViewManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    const v0, 0xbc278

    iput v0, p0, Lcom/sec/hearingadjust/PlayMusic;->fileLength:I

    .line 37
    iput v1, p0, Lcom/sec/hearingadjust/PlayMusic;->Page:I

    .line 41
    const/16 v0, 0xc

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->ResultData:[S

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 51
    iput-boolean v1, p0, Lcom/sec/hearingadjust/PlayMusic;->Mode:Z

    .line 419
    new-instance v0, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;

    invoke-direct {v0, p0}, Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->bcr:Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;

    .line 467
    new-instance v0, Lcom/sec/hearingadjust/PlayMusic$8;

    invoke-direct {v0, p0}, Lcom/sec/hearingadjust/PlayMusic$8;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;

    return-void
.end method

.method private AfterFunc()V
    .locals 2

    .prologue
    .line 133
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I

    .line 134
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->JNILib:Lcom/sec/hearingLib/HearingLib;

    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->ResultData:[S

    invoke-virtual {v0, v1}, Lcom/sec/hearingLib/HearingLib;->SamsungMusicInit([S)I

    .line 135
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/hearingadjust/PlayMusic$2;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/PlayMusic$2;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterThread:Ljava/lang/Thread;

    .line 175
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 176
    return-void
.end method

.method private BeforeFunc()V
    .locals 2

    .prologue
    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I

    .line 97
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/hearingadjust/PlayMusic$1;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/PlayMusic$1;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeThread:Ljava/lang/Thread;

    .line 128
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 129
    return-void
.end method

.method private Before_After_Select()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 337
    iget-boolean v0, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z

    if-ne v0, v1, :cond_1

    .line 338
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-boolean v0, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z

    if-ne v0, v1, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private ChangeView(I)V
    .locals 1
    .param p1, "page"    # I

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    invoke-virtual {v0, p1}, Lcom/sec/hearingadjust/ViewManager;->ChageView(I)V

    .line 464
    invoke-virtual {p0}, Lcom/sec/hearingadjust/PlayMusic;->finish()V

    .line 465
    return-void
.end method

.method private CheckData(SI)S
    .locals 2
    .param p1, "src"    # S
    .param p2, "i"    # I

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->SelectButton:I

    packed-switch v0, :pswitch_data_0

    .line 91
    .end local p1    # "src":S
    :cond_0
    :goto_0
    :pswitch_0
    return p1

    .line 87
    .restart local p1    # "src":S
    :pswitch_1
    rem-int/lit8 v0, p2, 0x2

    iget v1, p0, Lcom/sec/hearingadjust/PlayMusic;->SelectButton:I

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private Setid()V
    .locals 2

    .prologue
    .line 187
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabLeft:Landroid/view/View;

    .line 188
    const v0, 0x7f0a0036

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabRight:Landroid/view/View;

    .line 189
    const v0, 0x7f0a0037

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabBoth:Landroid/view/View;

    .line 191
    invoke-direct {p0}, Lcom/sec/hearingadjust/PlayMusic;->Before_After_Select()V

    .line 193
    const v0, 0x7f0a0038

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/hearingadjust/PlayMusic$3;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/PlayMusic$3;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    const v0, 0x7f0a0039

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/hearingadjust/PlayMusic$4;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/PlayMusic$4;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/hearingadjust/PlayMusic$5;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/PlayMusic$5;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/hearingadjust/PlayMusic$6;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/PlayMusic$6;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    const v0, 0x7f0a003d

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/hearingadjust/PlayMusic$7;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/PlayMusic$7;-><init>(Lcom/sec/hearingadjust/PlayMusic;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    return-void
.end method

.method private TabSelect(I)V
    .locals 6
    .param p1, "Select"    # I

    .prologue
    const v5, 0x7f0a0039

    const v4, 0x7f0a0038

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 291
    packed-switch p1, :pswitch_data_0

    .line 334
    :goto_0
    return-void

    .line 293
    :pswitch_0
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabBoth:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 297
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 298
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 299
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 301
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 302
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 303
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 306
    :pswitch_1
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabBoth:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 310
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 311
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 312
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 314
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 315
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 316
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 319
    :pswitch_2
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->vTabBoth:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 323
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 324
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 325
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 327
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 328
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 329
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    goto/16 :goto_0

    .line 291
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/sec/hearingadjust/PlayMusic;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/hearingadjust/PlayMusic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/hearingadjust/PlayMusic;)[S
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->buffer:[S

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/hearingadjust/PlayMusic;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;
    .param p1, "x1"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/PlayMusic;->TabSelect(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/hearingadjust/PlayMusic;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/hearingadjust/PlayMusic;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/hearingadjust/PlayMusic;->AfterFunc()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/hearingadjust/PlayMusic;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/hearingadjust/PlayMusic;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/hearingadjust/PlayMusic;->BeforeFunc()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/hearingadjust/PlayMusic;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/hearingadjust/PlayMusic;->Mode:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/hearingadjust/PlayMusic;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->Page:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/hearingadjust/PlayMusic;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;
    .param p1, "x1"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/PlayMusic;->ChangeView(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->am:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/hearingadjust/PlayMusic;SI)S
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;
    .param p1, "x1"    # S
    .param p2, "x2"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/hearingadjust/PlayMusic;->CheckData(SI)S

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/hearingadjust/PlayMusic;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/hearingadjust/PlayMusic;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I

    return v0
.end method

.method static synthetic access$508(Lcom/sec/hearingadjust/PlayMusic;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/hearingadjust/PlayMusic;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/hearingadjust/PlayMusic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/hearingadjust/PlayMusic;)Lcom/sec/hearingLib/HearingLib;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->JNILib:Lcom/sec/hearingLib/HearingLib;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/hearingadjust/PlayMusic;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/hearingadjust/PlayMusic;->allstopThread()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/hearingadjust/PlayMusic;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->SelectButton:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/hearingadjust/PlayMusic;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/PlayMusic;
    .param p1, "x1"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/sec/hearingadjust/PlayMusic;->SelectButton:I

    return p1
.end method

.method private allstopThread()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 267
    iget-boolean v1, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z

    if-ne v1, v5, :cond_0

    .line 268
    iput-boolean v4, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z

    .line 269
    :goto_0
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->BeforeThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v1

    sget-object v2, Ljava/lang/Thread$State;->RUNNABLE:Ljava/lang/Thread$State;

    if-ne v1, v2, :cond_0

    .line 271
    const-wide/16 v2, 0x32

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 277
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-boolean v1, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z

    if-ne v1, v5, :cond_1

    .line 278
    iput-boolean v4, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z

    .line 279
    :goto_1
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->AfterThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v1

    sget-object v2, Ljava/lang/Thread$State;->RUNNABLE:Ljava/lang/Thread$State;

    if-ne v1, v2, :cond_1

    .line 281
    const-wide/16 v2, 0x32

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 282
    :catch_1
    move-exception v0

    .line 283
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 287
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-void
.end method

.method private loadPreferences()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 353
    const-string v4, "DHA_PREF"

    invoke-virtual {p0, v4, v6}, Lcom/sec/hearingadjust/PlayMusic;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 354
    .local v3, "progressPreference":Landroid/content/SharedPreferences;
    const-string v4, "Pre0"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 355
    .local v2, "param":Ljava/lang/String;
    const-string v4, "page"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/hearingadjust/PlayMusic;->Page:I

    .line 356
    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 358
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 359
    .local v1, "p":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v4, 0xc

    if-ge v0, v4, :cond_0

    .line 360
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic;->ResultData:[S

    aget-object v5, v1, v0

    invoke-static {v5}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v5

    aput-short v5, v4, v0

    .line 359
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 363
    .end local v0    # "i":I
    .end local v1    # "p":[Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public InitMusic(I)V
    .locals 7
    .param p1, "sampleRate"    # I

    .prologue
    const/16 v3, 0xc

    const/4 v4, 0x2

    .line 366
    invoke-static {p1, v3, v4}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v5

    .line 370
    .local v5, "bufferSize":I
    new-instance v0, Landroid/media/AudioTrack;

    const/4 v1, 0x3

    const/4 v6, 0x1

    move v2, p1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;

    .line 376
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 377
    return-void
.end method

.method public LoadFile(I)V
    .locals 8
    .param p1, "Sample"    # I

    .prologue
    const v7, 0xbc278

    .line 381
    invoke-virtual {p0}, Lcom/sec/hearingadjust/PlayMusic;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 382
    .local v3, "in":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 383
    .local v0, "bis":Ljava/io/BufferedInputStream;
    new-array v4, v7, [S

    iput-object v4, p0, Lcom/sec/hearingadjust/PlayMusic;->buffer:[S

    .line 384
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_0

    .line 386
    :try_start_0
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic;->buffer:[S

    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->read()I

    move-result v5

    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->read()I

    move-result v6

    shl-int/lit8 v6, v6, 0x8

    add-int/2addr v5, v6

    int-to-short v5, v5

    aput-short v5, v4, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 387
    :catch_0
    move-exception v1

    .line 388
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 392
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 393
    const/4 v0, 0x0

    .line 398
    :goto_2
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 399
    const/4 v3, 0x0

    .line 403
    :goto_3
    return-void

    .line 394
    :catch_1
    move-exception v1

    .line 395
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 400
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 401
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 181
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->setContentView(I)V

    .line 182
    invoke-direct {p0}, Lcom/sec/hearingadjust/PlayMusic;->Setid()V

    .line 183
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->SelectButton:I

    invoke-direct {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->TabSelect(I)V

    .line 184
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const v1, 0x7f030003

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/PlayMusic;->setContentView(I)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/hearingadjust/PlayMusic;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/hearingadjust/PlayMusic;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/hearingadjust/PlayMusic;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 62
    new-instance v1, Lcom/sec/hearingadjust/ViewManager;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/ViewManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 64
    invoke-direct {p0}, Lcom/sec/hearingadjust/PlayMusic;->loadPreferences()V

    .line 65
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/PlayMusic;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->am:Landroid/media/AudioManager;

    .line 66
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->am:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/hearingadjust/PlayMusic;->Mode:Z

    .line 67
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->bcr:Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/sec/hearingadjust/PlayMusic;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 68
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->bcr:Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/sec/hearingadjust/PlayMusic;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 70
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/hearingadjust/PlayMusic;->SelectButton:I

    .line 73
    :try_start_0
    new-instance v1, Lcom/sec/hearingLib/HearingLib;

    invoke-direct {v1}, Lcom/sec/hearingLib/HearingLib;-><init>()V

    iput-object v1, p0, Lcom/sec/hearingadjust/PlayMusic;->JNILib:Lcom/sec/hearingLib/HearingLib;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    invoke-direct {p0}, Lcom/sec/hearingadjust/PlayMusic;->Setid()V

    .line 79
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->bcr:Lcom/sec/hearingadjust/PlayMusic$bcrHeadSet;

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 416
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 417
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 443
    packed-switch p1, :pswitch_data_0

    .line 450
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 445
    :pswitch_0
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->Page:I

    invoke-direct {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->ChangeView(I)V

    .line 446
    const/4 v0, 0x1

    goto :goto_0

    .line 443
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 455
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 456
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->Page:I

    invoke-direct {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->ChangeView(I)V

    .line 457
    const/4 v0, 0x1

    .line 459
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 407
    invoke-direct {p0}, Lcom/sec/hearingadjust/PlayMusic;->allstopThread()V

    .line 408
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    .line 409
    iget-object v0, p0, Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 410
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 411
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 346
    const v0, 0xac44

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->InitMusic(I)V

    .line 347
    const v0, 0x7f040036

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->LoadFile(I)V

    .line 348
    iget v0, p0, Lcom/sec/hearingadjust/PlayMusic;->SelectButton:I

    invoke-direct {p0, v0}, Lcom/sec/hearingadjust/PlayMusic;->TabSelect(I)V

    .line 349
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 350
    return-void
.end method
