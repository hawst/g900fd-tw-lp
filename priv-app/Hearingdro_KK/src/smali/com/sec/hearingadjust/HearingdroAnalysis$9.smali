.class Lcom/sec/hearingadjust/HearingdroAnalysis$9;
.super Ljava/lang/Object;
.source "HearingdroAnalysis.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/HearingdroAnalysis;->loadSound()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 323
    const/16 v6, 0xc

    const/16 v7, 0x9

    filled-new-array {v6, v7}, [I

    move-result-object v6

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v7, v6}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    .line 324
    .local v0, "TestToneList":[[I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    const/16 v6, 0x9

    if-gt v1, v6, :cond_0

    .line 325
    const/4 v6, 0x0

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "l_250_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 326
    const/4 v6, 0x1

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "l_500_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 327
    const/4 v6, 0x2

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "l_1000_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 328
    const/4 v6, 0x3

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "l_2000_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 329
    const/4 v6, 0x4

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "l_4000_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 330
    const/4 v6, 0x5

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "l_8000_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 332
    const/4 v6, 0x6

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "r_250_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 333
    const/4 v6, 0x7

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "r_500_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 334
    const/16 v6, 0x8

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "r_1000_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 335
    const/16 v6, 0x9

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "r_2000_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 336
    const/16 v6, 0xa

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "r_4000_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 337
    const/16 v6, 0xb

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "r_8000_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "raw"

    iget-object v11, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v11}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v7

    .line 324
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 340
    :cond_0
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    new-instance v7, Lcom/sec/hearingadjust/SoundManager;

    iget-object v8, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-direct {v7, v8}, Lcom/sec/hearingadjust/SoundManager;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;
    invoke-static {v6, v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$402(Lcom/sec/hearingadjust/HearingdroAnalysis;Lcom/sec/hearingadjust/SoundManager;)Lcom/sec/hearingadjust/SoundManager;

    .line 342
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-virtual {v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getVolume()I

    move-result v7

    iput v7, v6, Lcom/sec/hearingadjust/HearingdroAnalysis;->volumeLevel:I

    .line 343
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->setVolume(I)V

    .line 345
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    const/16 v6, 0xc

    if-ge v2, v6, :cond_3

    .line 346
    const/4 v1, 0x0

    :goto_2
    const/16 v6, 0x9

    if-ge v1, v6, :cond_2

    .line 347
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$400(Lcom/sec/hearingadjust/HearingdroAnalysis;)Lcom/sec/hearingadjust/SoundManager;

    move-result-object v6

    if-nez v6, :cond_1

    .line 376
    :goto_3
    return-void

    .line 350
    :cond_1
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$400(Lcom/sec/hearingadjust/HearingdroAnalysis;)Lcom/sec/hearingadjust/SoundManager;

    move-result-object v6

    mul-int/lit8 v7, v2, 0x9

    add-int/2addr v7, v1

    aget-object v8, v0, v2

    aget v8, v8, v1

    invoke-virtual {v6, v7, v8}, Lcom/sec/hearingadjust/SoundManager;->setAddsound(II)V

    .line 351
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x0

    mul-int/lit8 v9, v2, 0x9

    add-int/2addr v9, v1

    mul-int/lit8 v9, v9, 0x64

    div-int/lit8 v9, v9, 0x6c

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 346
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 345
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 355
    :cond_3
    const/4 v1, 0x0

    :goto_4
    const/16 v6, 0xc

    if-ge v1, v6, :cond_4

    .line 356
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$500(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v6

    const/16 v7, 0x14

    aput v7, v6, v1

    .line 355
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 358
    :cond_4
    const/4 v1, 0x0

    :goto_5
    const/16 v6, 0xc

    if-ge v1, v6, :cond_5

    .line 359
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v6

    aput v1, v6, v1

    .line 360
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v6

    add-int/lit8 v7, v1, 0xc

    aput v1, v6, v7

    .line 361
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v6

    add-int/lit8 v7, v1, 0x18

    aput v1, v6, v7

    .line 358
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 363
    :cond_5
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v6

    const/16 v7, 0x24

    const/16 v8, -0xa

    aput v8, v6, v7

    .line 364
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 365
    .local v3, "rand":Ljava/util/Random;
    const/4 v1, 0x0

    :goto_6
    const/16 v6, 0x24

    if-ge v1, v6, :cond_6

    .line 366
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v6

    aget v4, v6, v1

    .line 367
    .local v4, "temp":I
    const/16 v6, 0x24

    invoke-virtual {v3, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    .line 368
    .local v5, "tempIndex":I
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v6

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I
    invoke-static {v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v7

    aget v7, v7, v5

    aput v7, v6, v1

    .line 369
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I

    move-result-object v6

    aput v4, v6, v5

    .line 365
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 371
    .end local v4    # "temp":I
    .end local v5    # "tempIndex":I
    :cond_6
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    const/4 v7, 0x0

    # setter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I
    invoke-static {v6, v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$702(Lcom/sec/hearingadjust/HearingdroAnalysis;I)I

    .line 372
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    const/16 v7, 0x24

    # setter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->indexMax:I
    invoke-static {v6, v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$802(Lcom/sec/hearingadjust/HearingdroAnalysis;I)I

    .line 373
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 374
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    # getter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 375
    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis$9;->this$0:Lcom/sec/hearingadjust/HearingdroAnalysis;

    const/4 v7, 0x0

    # setter for: Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z
    invoke-static {v6, v7}, Lcom/sec/hearingadjust/HearingdroAnalysis;->access$902(Lcom/sec/hearingadjust/HearingdroAnalysis;Z)Z

    goto/16 :goto_3
.end method
