.class Lcom/sec/hearingadjust/PlayMusic$6;
.super Ljava/lang/Object;
.source "PlayMusic.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/PlayMusic;->Setid()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/PlayMusic;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/PlayMusic;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/sec/hearingadjust/PlayMusic$6;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 223
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$6;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z
    invoke-static {v1}, Lcom/sec/hearingadjust/PlayMusic;->access$600(Lcom/sec/hearingadjust/PlayMusic;)Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 239
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$6;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # setter for: Lcom/sec/hearingadjust/PlayMusic;->AfterMode:Z
    invoke-static {v1, v2}, Lcom/sec/hearingadjust/PlayMusic;->access$602(Lcom/sec/hearingadjust/PlayMusic;Z)Z

    .line 227
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$6;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    const/4 v2, 0x0

    # setter for: Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z
    invoke-static {v1, v2}, Lcom/sec/hearingadjust/PlayMusic;->access$002(Lcom/sec/hearingadjust/PlayMusic;Z)Z

    .line 228
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$6;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/hearingadjust/PlayMusic;->access$400(Lcom/sec/hearingadjust/PlayMusic;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 229
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$6;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->BeforeThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/sec/hearingadjust/PlayMusic;->access$1100(Lcom/sec/hearingadjust/PlayMusic;)Ljava/lang/Thread;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 230
    :goto_1
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$6;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->BeforeThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/sec/hearingadjust/PlayMusic;->access$1100(Lcom/sec/hearingadjust/PlayMusic;)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v1

    sget-object v2, Ljava/lang/Thread$State;->RUNNABLE:Ljava/lang/Thread$State;

    if-ne v1, v2, :cond_1

    .line 232
    const-wide/16 v2, 0x32

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 238
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v1, p0, Lcom/sec/hearingadjust/PlayMusic$6;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # invokes: Lcom/sec/hearingadjust/PlayMusic;->AfterFunc()V
    invoke-static {v1}, Lcom/sec/hearingadjust/PlayMusic;->access$1200(Lcom/sec/hearingadjust/PlayMusic;)V

    goto :goto_0
.end method
