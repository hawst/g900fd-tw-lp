.class public Lcom/sec/hearingadjust/HearingdroResult_noactionbar;
.super Landroid/app/Activity;
.source "HearingdroResult_noactionbar.java"


# instance fields
.field private ArrayString:[Ljava/lang/String;

.field private final LEFT:I

.field private final RIGHT:I

.field private SelectEar:I

.field private SelectPreset:I

.field private adapter:Landroid/widget/ArrayAdapter;

.field private am:Landroid/media/AudioManager;

.field private cb_lv_setting:Landroid/widget/ListView;

.field private cb_state:[Z

.field private cb_txt:[Ljava/lang/String;

.field private gvResult:Lcom/sec/hearingadjust/GraphView;

.field private layoutGraph:Landroid/widget/RelativeLayout;

.field private lv_preset:Landroid/widget/ListView;

.field private lv_setting:Landroid/widget/ListView;

.field private myPresetview:Lcom/sec/hearingadjust/SettingView;

.field private mySettingview:Lcom/sec/hearingadjust/SettingView;

.field private nSelectbutton:I

.field private resultData:[I

.field private scroll:Landroid/widget/ScrollView;

.field private vTabLeft:Landroid/view/View;

.field private vTabRight:Landroid/view/View;

.field private viewManager:Lcom/sec/hearingadjust/ViewManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    const/16 v0, 0xc

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    .line 25
    iput v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->LEFT:I

    .line 26
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->RIGHT:I

    .line 27
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 32
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->lv_setting:Landroid/widget/ListView;

    .line 33
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->lv_preset:Landroid/widget/ListView;

    .line 34
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    .line 35
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    .line 36
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->layoutGraph:Landroid/widget/RelativeLayout;

    .line 37
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->scroll:Landroid/widget/ScrollView;

    .line 38
    iput v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->nSelectbutton:I

    .line 43
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_lv_setting:Landroid/widget/ListView;

    .line 44
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_txt:[Ljava/lang/String;

    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_state:[Z

    .line 46
    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->adapter:Landroid/widget/ArrayAdapter;

    return-void

    .line 45
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method private ChangeView(I)V
    .locals 1
    .param p1, "Page"    # I

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    invoke-virtual {v0, p1}, Lcom/sec/hearingadjust/ViewManager;->ChageView(I)V

    .line 379
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->finish()V

    .line 380
    return-void
.end method

.method private ClickButton(I)V
    .locals 6
    .param p1, "nSelect"    # I

    .prologue
    const v5, 0x7f0a0039

    const v4, 0x7f0a0038

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 80
    sparse-switch p1, :sswitch_data_0

    .line 99
    :goto_0
    return-void

    .line 83
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->LeftGraph()V

    .line 84
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 87
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 88
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 91
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->RightGraph()V

    .line 92
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->vTabLeft:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->vTabRight:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95
    invoke-virtual {p0, v4}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 96
    invoke-virtual {p0, v5}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 80
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method private LeftGraph()V
    .locals 8

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->gvResult:Lcom/sec/hearingadjust/GraphView;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/4 v5, 0x3

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/4 v6, 0x4

    aget v5, v5, v6

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/4 v7, 0x5

    aget v6, v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/hearingadjust/GraphView;->setParam(IIIIII)V

    .line 385
    return-void
.end method

.method private MakeToParam()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xb

    .line 415
    const-string v0, ""

    .line 416
    .local v0, "ResultString":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 418
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 416
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 420
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 421
    return-object v0
.end method

.method private RightGraph()V
    .locals 8

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->gvResult:Lcom/sec/hearingadjust/GraphView;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/4 v3, 0x7

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/16 v4, 0x8

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/16 v5, 0x9

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/16 v6, 0xa

    aget v5, v5, v6

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    const/16 v7, 0xb

    aget v6, v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/hearingadjust/GraphView;->setParam(IIIIII)V

    .line 390
    return-void
.end method

.method private Setid()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 163
    const v2, 0x7f0a0035

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->vTabLeft:Landroid/view/View;

    .line 164
    const v2, 0x7f0a0036

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->vTabRight:Landroid/view/View;

    .line 165
    const v2, 0x7f0a004b

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->lv_setting:Landroid/widget/ListView;

    .line 166
    const v2, 0x7f0a0049

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->lv_preset:Landroid/widget/ListView;

    .line 167
    const v2, 0x7f0a0042

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->layoutGraph:Landroid/widget/RelativeLayout;

    .line 168
    const v2, 0x7f0a0041

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->scroll:Landroid/widget/ScrollView;

    .line 172
    const v2, 0x7f0a004a

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_lv_setting:Landroid/widget/ListView;

    .line 175
    new-instance v2, Lcom/sec/hearingadjust/GraphView;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/GraphView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->gvResult:Lcom/sec/hearingadjust/GraphView;

    .line 176
    new-instance v2, Lcom/sec/hearingadjust/SettingView;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/SettingView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    .line 177
    new-instance v2, Lcom/sec/hearingadjust/SettingView;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/SettingView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    .line 178
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->layoutGraph:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->gvResult:Lcom/sec/hearingadjust/GraphView;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 180
    iget v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->nSelectbutton:I

    invoke-direct {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->ClickButton(I)V

    .line 182
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hearing_revision"

    invoke-static {v2, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 183
    .local v0, "nCallCheckTmp":I
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hearing_direction"

    invoke-static {v2, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectEar:I

    .line 184
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hearing_musiccheck"

    invoke-static {v2, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 186
    .local v1, "nMusicCheckTmp":I
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KYU  = (0) [CALL db : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "[MUSIC db : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 188
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    invoke-virtual {v2}, Lcom/sec/hearingadjust/SettingView;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 190
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    iget v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectEar:I

    invoke-virtual {v2, v4, v5}, Lcom/sec/hearingadjust/SettingView;->setAddList(II)V

    .line 193
    :cond_0
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    invoke-virtual {v2}, Lcom/sec/hearingadjust/SettingView;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    iget v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v3, v5}, Lcom/sec/hearingadjust/SettingView;->setAddList(II)V

    .line 200
    :cond_1
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->lv_setting:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 201
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->lv_preset:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 204
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_lv_setting:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 205
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_lv_setting:Landroid/widget/ListView;

    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 209
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_state:[Z

    if-ne v0, v3, :cond_2

    move v2, v3

    :goto_0
    aput-boolean v2, v5, v4

    .line 210
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_state:[Z

    if-ne v1, v3, :cond_3

    move v2, v3

    :goto_1
    aput-boolean v2, v5, v3

    .line 212
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KYU  = (1) [CALL db : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "[MUSIC db : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 214
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_lv_setting:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_state:[Z

    aget-boolean v5, v5, v4

    invoke-virtual {v2, v4, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 215
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_lv_setting:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_state:[Z

    aget-boolean v4, v4, v3

    invoke-virtual {v2, v3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 218
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->scrollToStart()V

    .line 219
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->scroll:Landroid/widget/ScrollView;

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->requestDisallowInterceptTouchEvent(Z)V

    .line 220
    const v2, 0x7f0a0038

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$4;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$4;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    const v2, 0x7f0a0039

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$5;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$5;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    const v2, 0x7f0a0047

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$6;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    const v2, 0x7f0a0048

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$7;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$7;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    const/high16 v2, 0x7f0a0000

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$8;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$8;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    const v2, 0x7f0a0001

    invoke-virtual {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$9;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_lv_setting:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$10;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$10;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 352
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->lv_setting:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$11;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$11;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 369
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->lv_preset:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$12;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$12;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 375
    return-void

    :cond_2
    move v2, v4

    .line 209
    goto/16 :goto_0

    :cond_3
    move v2, v4

    .line 210
    goto/16 :goto_1
.end method

.method private XmlToResultData(Ljava/lang/String;)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 427
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 428
    .local v1, "p":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xc

    if-ge v0, v2, :cond_0

    .line 429
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->resultData:[I

    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 428
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 431
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->scroll:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;
    .param p1, "x1"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->ChangeView(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_state:[Z

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->popSelect()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_lv_setting:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->popPresetSelect()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Lcom/sec/hearingadjust/SettingView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->myPresetview:Lcom/sec/hearingadjust/SettingView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->loadPreferences()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->nSelectbutton:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->nSelectbutton:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;
    .param p1, "x1"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->ClickButton(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectEar:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectEar:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Lcom/sec/hearingadjust/SettingView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->mySettingview:Lcom/sec/hearingadjust/SettingView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->am:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->MakeToParam()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadPreferences()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 393
    const-string v2, "DHA_PREF"

    invoke-virtual {p0, v2, v4}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 394
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    const-string v2, "isPreset"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I

    .line 395
    const-string v2, "PresetSelect"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 396
    .local v0, "k":I
    if-nez v0, :cond_0

    .line 398
    invoke-direct {p0, v4}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->loadpresetxml(I)V

    .line 404
    :goto_0
    return-void

    .line 402
    :cond_0
    iget v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I

    invoke-direct {p0, v2}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->loadpresetxml(I)V

    goto :goto_0
.end method

.method private loadpresetxml(I)V
    .locals 4
    .param p1, "nData"    # I

    .prologue
    .line 408
    const-string v2, "DHA_PREF"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 409
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pre"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 410
    .local v0, "param":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->XmlToResultData(Ljava/lang/String;)V

    .line 411
    return-void
.end method

.method private popPresetSelect()V
    .locals 4

    .prologue
    .line 111
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 112
    .local v0, "popSelector":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f070003

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 113
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->ArrayString:[Ljava/lang/String;

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectPreset:I

    add-int/lit8 v2, v2, -0x1

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$2;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 130
    const v1, 0x7f07000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 131
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 132
    return-void
.end method

.method private popSelect()V
    .locals 4

    .prologue
    .line 136
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 137
    .local v0, "popSelector":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f070029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 138
    const/high16 v1, 0x7f050000

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->SelectEar:I

    new-instance v3, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$3;

    invoke-direct {v3, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$3;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 150
    const v1, 0x7f07000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 151
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 152
    return-void
.end method

.method private scrollToStart()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->scroll:Landroid/widget/ScrollView;

    new-instance v1, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$1;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar$1;-><init>(Lcom/sec/hearingadjust/HearingdroResult_noactionbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 107
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 156
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 157
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->setContentView(I)V

    .line 158
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->Setid()V

    .line 159
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v7, 0x7f070000

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 55
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->setContentView(I)V

    .line 59
    new-array v0, v6, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_txt:[Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_txt:[Ljava/lang/String;

    const v1, 0x7f070028

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 64
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_txt:[Ljava/lang/String;

    const v1, 0x7f07002b

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 65
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030008

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->cb_txt:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->adapter:Landroid/widget/ArrayAdapter;

    .line 69
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->am:Landroid/media/AudioManager;

    .line 70
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->am:Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 71
    new-array v0, v6, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->ArrayString:[Ljava/lang/String;

    .line 72
    new-instance v0, Lcom/sec/hearingadjust/ViewManager;

    invoke-direct {v0, p0}, Lcom/sec/hearingadjust/ViewManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 74
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->loadPreferences()V

    .line 75
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;->Setid()V

    .line 76
    return-void
.end method
