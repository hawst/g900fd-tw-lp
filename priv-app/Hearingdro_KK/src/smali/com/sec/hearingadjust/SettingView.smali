.class public Lcom/sec/hearingadjust/SettingView;
.super Landroid/widget/BaseAdapter;
.source "SettingView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/hearingadjust/SettingView$ItemList;
    }
.end annotation


# instance fields
.field private arrItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/hearingadjust/SettingView$ItemList;",
            ">;"
        }
    .end annotation
.end field

.field private inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    .line 19
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/hearingadjust/SettingView;->inflater:Landroid/view/LayoutInflater;

    .line 20
    return-void
.end method


# virtual methods
.method public ChangeState(II)V
    .locals 1
    .param p1, "nPosition"    # I
    .param p2, "state"    # I

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/hearingadjust/SettingView$ItemList;

    invoke-virtual {v0, p2}, Lcom/sec/hearingadjust/SettingView$ItemList;->setItem(I)V

    .line 45
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 105
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getState(I)I
    .locals 1
    .param p1, "nPosition"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/hearingadjust/SettingView$ItemList;

    iget v0, v0, Lcom/sec/hearingadjust/SettingView$ItemList;->nState:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v10, 0x7f0a0059

    .line 53
    move-object v7, p2

    .line 54
    .local v7, "v":Landroid/view/View;
    const/4 v1, 0x0

    .line 55
    .local v1, "res":I
    iget-object v8, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/hearingadjust/SettingView$ItemList;

    iget v8, v8, Lcom/sec/hearingadjust/SettingView$ItemList;->nType:I

    packed-switch v8, :pswitch_data_0

    .line 64
    :goto_0
    iget-object v8, p0, Lcom/sec/hearingadjust/SettingView;->inflater:Landroid/view/LayoutInflater;

    const/4 v9, 0x0

    invoke-virtual {v8, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 65
    const v8, 0x7f0a0058

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 66
    .local v6, "txtview":Landroid/widget/TextView;
    if-eqz v6, :cond_0

    .line 68
    iget-object v8, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/hearingadjust/SettingView$ItemList;

    iget-object v8, v8, Lcom/sec/hearingadjust/SettingView$ItemList;->strTitletxt:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    :cond_0
    iget-object v8, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/hearingadjust/SettingView$ItemList;

    iget v0, v8, Lcom/sec/hearingadjust/SettingView$ItemList;->nState:I

    .line 71
    .local v0, "nTmp":I
    iget-object v8, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/hearingadjust/SettingView$ItemList;

    iget v8, v8, Lcom/sec/hearingadjust/SettingView$ItemList;->nType:I

    packed-switch v8, :pswitch_data_1

    .line 97
    :goto_1
    return-object v7

    .line 60
    .end local v0    # "nTmp":I
    .end local v6    # "txtview":Landroid/widget/TextView;
    :pswitch_0
    const v1, 0x7f030007

    goto :goto_0

    .line 75
    .restart local v0    # "nTmp":I
    .restart local v6    # "txtview":Landroid/widget/TextView;
    :pswitch_1
    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 76
    .local v5, "txtsubview":Landroid/widget/TextView;
    packed-switch v0, :pswitch_data_2

    goto :goto_1

    .line 79
    :pswitch_2
    const v8, 0x7f07002a

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 82
    :pswitch_3
    const v8, 0x7f07001c

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 85
    :pswitch_4
    const v8, 0x7f07002e

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 90
    .end local v5    # "txtsubview":Landroid/widget/TextView;
    :pswitch_5
    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 91
    .local v4, "txtpresetsubview":Landroid/widget/TextView;
    invoke-virtual {v7}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 92
    .local v2, "res1":Landroid/content/res/Resources;
    const/high16 v8, 0x7f070000

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v11, v0, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 93
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 55
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 71
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 76
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setAddList(II)V
    .locals 3
    .param p1, "Type"    # I
    .param p2, "State"    # I

    .prologue
    .line 24
    const-string v0, ""

    .line 25
    .local v0, "titletxt":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 36
    :goto_0
    iget-object v1, p0, Lcom/sec/hearingadjust/SettingView;->arrItemList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/hearingadjust/SettingView$ItemList;

    invoke-direct {v2, p1, v0, p2}, Lcom/sec/hearingadjust/SettingView$ItemList;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    return-void

    .line 29
    :pswitch_0
    iget-object v1, p0, Lcom/sec/hearingadjust/SettingView;->inflater:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070029

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 30
    goto :goto_0

    .line 32
    :pswitch_1
    iget-object v1, p0, Lcom/sec/hearingadjust/SettingView;->inflater:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
