.class public Lcom/sec/hearingadjust/ViewManager;
.super Ljava/lang/Object;
.source "ViewManager.java"


# static fields
.field public static final ANALYSIS_PAGE:I = 0x1

.field public static final FINETUINING_PAGE:I = 0x5

.field public static final INSTRUCT_PAGE:I = 0x0

.field public static final MUSIC_PAGE:I = 0x4

.field public static final RESULT_PAGE_ACTIONBAR:I = 0x3

.field public static final RESULT_PAGE_NOACTIONBAR:I = 0x2

.field public static final VIEWCHANGEMSG:Ljava/lang/String; = "VIEW"


# instance fields
.field private context:Landroid/content/Context;

.field private i:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    .line 21
    iput-object p1, p0, Lcom/sec/hearingadjust/ViewManager;->context:Landroid/content/Context;

    .line 22
    return-void
.end method


# virtual methods
.method public ChageView(I)V
    .locals 3
    .param p1, "Page"    # I

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 27
    packed-switch p1, :pswitch_data_0

    .line 49
    :goto_0
    iget-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 51
    :cond_0
    return-void

    .line 30
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/hearingadjust/ViewManager;->context:Landroid/content/Context;

    const-class v2, Lcom/sec/hearingadjust/HearingdroInstruct;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    .line 31
    iget-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    const-string v1, "VIEW"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 34
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/hearingadjust/ViewManager;->context:Landroid/content/Context;

    const-class v2, Lcom/sec/hearingadjust/HearingdroAnalysis;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    goto :goto_0

    .line 37
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/hearingadjust/ViewManager;->context:Landroid/content/Context;

    const-class v2, Lcom/sec/hearingadjust/HearingdroResult_noactionbar;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    goto :goto_0

    .line 40
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/hearingadjust/ViewManager;->context:Landroid/content/Context;

    const-class v2, Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    goto :goto_0

    .line 43
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/hearingadjust/ViewManager;->context:Landroid/content/Context;

    const-class v2, Lcom/sec/hearingadjust/HearingdroTuning;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    goto :goto_0

    .line 46
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/hearingadjust/ViewManager;->context:Landroid/content/Context;

    const-class v2, Lcom/sec/hearingadjust/PlayMusic;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/ViewManager;->i:Landroid/content/Intent;

    goto :goto_0

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
