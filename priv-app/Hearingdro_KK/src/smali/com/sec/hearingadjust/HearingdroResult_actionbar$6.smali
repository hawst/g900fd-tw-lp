.class Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;
.super Ljava/lang/Object;
.source "HearingdroResult_actionbar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/HearingdroResult_actionbar;->Setid()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 258
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_actionbar;->am:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->access$800(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Landroid/media/AudioManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_actionbar;->am:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->access$800(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Landroid/media/AudioManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v2

    if-nez v2, :cond_0

    .line 259
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    const v3, 0x7f07000f

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 270
    :goto_0
    return-void

    .line 262
    :cond_0
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    # getter for: Lcom/sec/hearingadjust/HearingdroResult_actionbar;->am:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->access$800(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Landroid/media/AudioManager;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v6, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 263
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    const-string v3, "DHA_PREF"

    invoke-virtual {v2, v3, v5}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 264
    .local v1, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 265
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "page"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 266
    const-string v2, "Pre0"

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_actionbar;->MakeToParam()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->access$900(Lcom/sec/hearingadjust/HearingdroResult_actionbar;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 267
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 268
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroResult_actionbar$6;->this$0:Lcom/sec/hearingadjust/HearingdroResult_actionbar;

    const/4 v3, 0x4

    # invokes: Lcom/sec/hearingadjust/HearingdroResult_actionbar;->ChangeView(I)V
    invoke-static {v2, v3}, Lcom/sec/hearingadjust/HearingdroResult_actionbar;->access$1000(Lcom/sec/hearingadjust/HearingdroResult_actionbar;I)V

    goto :goto_0
.end method
