.class public final Lcom/sec/hearingadjust/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/hearingadjust/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final AfterImg:I = 0x7f0a0046

.field public static final BeforeImg:I = 0x7f0a0045

.field public static final CallImg:I = 0x7f0a003c

.field public static final RestartMenu:I = 0x7f0a005a

.field public static final Scroll:I = 0x7f0a0041

.field public static final button0:I = 0x7f0a004c

.field public static final button1:I = 0x7f0a004d

.field public static final button10:I = 0x7f0a0056

.field public static final button11:I = 0x7f0a0057

.field public static final button2:I = 0x7f0a004e

.field public static final button3:I = 0x7f0a004f

.field public static final button4:I = 0x7f0a0050

.field public static final button5:I = 0x7f0a0051

.field public static final button6:I = 0x7f0a0052

.field public static final button7:I = 0x7f0a0053

.field public static final button8:I = 0x7f0a0054

.field public static final button9:I = 0x7f0a0055

.field public static final buttonAfter:I = 0x7f0a003e

.field public static final buttonAnalysisNo:I = 0x7f0a002c

.field public static final buttonAnalysisYes:I = 0x7f0a002d

.field public static final buttonBefore:I = 0x7f0a003d

.field public static final buttonCancel:I = 0x7f0a0000

.field public static final buttonDone:I = 0x7f0a0001

.field public static final buttonInstStart:I = 0x7f0a002f

.field public static final buttonMusicSound:I = 0x7f0a0047

.field public static final buttonTabBoth:I = 0x7f0a003a

.field public static final buttonTabLeft:I = 0x7f0a0038

.field public static final buttonTabRight:I = 0x7f0a0039

.field public static final buttonTuining:I = 0x7f0a0048

.field public static final cb_lvsetting:I = 0x7f0a004a

.field public static final inst1:I = 0x7f0a0030

.field public static final inst2:I = 0x7f0a0031

.field public static final inst3:I = 0x7f0a0032

.field public static final inst4:I = 0x7f0a0033

.field public static final ivProgress1:I = 0x7f0a0008

.field public static final ivProgress10:I = 0x7f0a0011

.field public static final ivProgress11:I = 0x7f0a0012

.field public static final ivProgress12:I = 0x7f0a0013

.field public static final ivProgress13:I = 0x7f0a0014

.field public static final ivProgress14:I = 0x7f0a0015

.field public static final ivProgress15:I = 0x7f0a0016

.field public static final ivProgress16:I = 0x7f0a0017

.field public static final ivProgress17:I = 0x7f0a0018

.field public static final ivProgress18:I = 0x7f0a0019

.field public static final ivProgress19:I = 0x7f0a001a

.field public static final ivProgress2:I = 0x7f0a0009

.field public static final ivProgress20:I = 0x7f0a001b

.field public static final ivProgress21:I = 0x7f0a001c

.field public static final ivProgress22:I = 0x7f0a001d

.field public static final ivProgress23:I = 0x7f0a001e

.field public static final ivProgress24:I = 0x7f0a001f

.field public static final ivProgress25:I = 0x7f0a0020

.field public static final ivProgress26:I = 0x7f0a0021

.field public static final ivProgress27:I = 0x7f0a0022

.field public static final ivProgress28:I = 0x7f0a0023

.field public static final ivProgress29:I = 0x7f0a0024

.field public static final ivProgress3:I = 0x7f0a000a

.field public static final ivProgress30:I = 0x7f0a0025

.field public static final ivProgress31:I = 0x7f0a0026

.field public static final ivProgress32:I = 0x7f0a0027

.field public static final ivProgress33:I = 0x7f0a0028

.field public static final ivProgress34:I = 0x7f0a0029

.field public static final ivProgress35:I = 0x7f0a002a

.field public static final ivProgress36:I = 0x7f0a002b

.field public static final ivProgress4:I = 0x7f0a000b

.field public static final ivProgress5:I = 0x7f0a000c

.field public static final ivProgress6:I = 0x7f0a000d

.field public static final ivProgress7:I = 0x7f0a000e

.field public static final ivProgress8:I = 0x7f0a000f

.field public static final ivProgress9:I = 0x7f0a0010

.field public static final ivSignal:I = 0x7f0a0003

.field public static final layoutInstructButton:I = 0x7f0a002e

.field public static final layoutProgress:I = 0x7f0a0006

.field public static final layoutResultGraph:I = 0x7f0a0042

.field public static final layoutTab:I = 0x7f0a0034

.field public static final lvPreset:I = 0x7f0a0049

.field public static final lvsetting:I = 0x7f0a004b

.field public static final previewtitle:I = 0x7f0a003b

.field public static final progressLoading:I = 0x7f0a0004

.field public static final scrollView1:I = 0x7f0a003f

.field public static final textExcellent:I = 0x7f0a0043

.field public static final textGood:I = 0x7f0a0044

.field public static final textProgress:I = 0x7f0a0005

.field public static final textProgressRight:I = 0x7f0a0007

.field public static final tvAnalysisTitle:I = 0x7f0a0002

.field public static final tvResultText:I = 0x7f0a0040

.field public static final tv_subtext:I = 0x7f0a0059

.field public static final tv_title:I = 0x7f0a0058

.field public static final vTabBoth:I = 0x7f0a0037

.field public static final vTabLeft:I = 0x7f0a0035

.field public static final vTabRight:I = 0x7f0a0036


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
