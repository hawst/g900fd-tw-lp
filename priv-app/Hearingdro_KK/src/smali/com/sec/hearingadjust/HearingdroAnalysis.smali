.class public Lcom/sec/hearingadjust/HearingdroAnalysis;
.super Landroid/app/Activity;
.source "HearingdroAnalysis.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;
    }
.end annotation


# static fields
.field private static final ANALYSIS_VOLUME:I = 0x9

.field private static final DELAY_MS:I = 0x4b0

.field private static final MSG_BUTTON_DISABLE:I = 0x4

.field private static final MSG_BUTTON_ENABLE:I = 0x5

.field private static final MSG_END_ANALYSIS:I = 0x3

.field private static final MSG_PROGRESS_LOAD:I = 0x0

.field private static final MSG_SIGNAL_ANIMATION:I = 0x2

.field private static final MSG_START:I = 0x1

.field private static final MSG_TXT_HEAR:I = 0x7

.field private static final MSG_TXT_LISTEN:I = 0x6

.field public static final NONPRESET:I


# instance fields
.field private AlertContinue:Landroid/app/AlertDialog;

.field private AlertPopup:Landroid/app/AlertDialog;

.field private Mode:Z

.field private am:Landroid/media/AudioManager;

.field private bcr:Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;

.field private btNo:Landroid/widget/Button;

.field private btYes:Landroid/widget/Button;

.field private diagnosisOrder:[I

.field private indexMax:I

.field private indexOrder:I

.field private isLoading:Z

.field private ivProgress:[Landroid/widget/ImageView;

.field private ivSignal:Landroid/widget/ImageView;

.field private pbLoad:Landroid/widget/ProgressBar;

.field private resultData:[I

.field private signalIndex:I

.field private smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

.field private threadLoad:Ljava/lang/Thread;

.field private tvAnalysisTitle:Landroid/widget/TextView;

.field private tvProgress:Landroid/widget/TextView;

.field private final uiHandler:Landroid/os/Handler;

.field private viewManager:Lcom/sec/hearingadjust/ViewManager;

.field public volumeLevel:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 49
    const/16 v0, 0xc

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I

    .line 50
    const/16 v0, 0x25

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I

    .line 52
    iput v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    .line 55
    const/16 v0, 0x24

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivProgress:[Landroid/widget/ImageView;

    .line 62
    iput-boolean v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->Mode:Z

    .line 64
    iput-object v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->threadLoad:Ljava/lang/Thread;

    .line 381
    new-instance v0, Lcom/sec/hearingadjust/HearingdroAnalysis$10;

    invoke-direct {v0, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$10;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    .line 580
    new-instance v0, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;

    invoke-direct {v0, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->bcr:Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;

    .line 581
    return-void
.end method

.method private ChangeView(I)V
    .locals 1
    .param p1, "Page"    # I

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    invoke-virtual {v0, p1}, Lcom/sec/hearingadjust/ViewManager;->ChageView(I)V

    .line 457
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->finish()V

    .line 458
    return-void
.end method

.method private DestroySound()V
    .locals 3

    .prologue
    .line 550
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->PlayState()Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    :goto_0
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    if-eqz v1, :cond_1

    .line 555
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->PlayState()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    invoke-virtual {v1}, Lcom/sec/hearingadjust/SoundManager;->stopSound()V

    .line 558
    :cond_0
    iget v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->volumeLevel:I

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->setVolume(I)V

    .line 559
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    .line 561
    :cond_1
    return-void

    .line 551
    :catch_0
    move-exception v0

    .line 552
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "====isSoundPlaying Exception"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private Setid()V
    .locals 6

    .prologue
    .line 209
    const v1, 0x7f0a0002

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvAnalysisTitle:Landroid/widget/TextView;

    .line 210
    const v1, 0x7f0a0004

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->pbLoad:Landroid/widget/ProgressBar;

    .line 211
    const v1, 0x7f0a0003

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivSignal:Landroid/widget/ImageView;

    .line 212
    const v1, 0x7f0a0007

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvProgress:Landroid/widget/TextView;

    .line 213
    const v1, 0x7f0a002d

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->btYes:Landroid/widget/Button;

    .line 214
    const v1, 0x7f0a002c

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->btNo:Landroid/widget/Button;

    .line 216
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x24

    if-ge v0, v1, :cond_0

    .line 217
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivProgress:[Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ivProgress"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "id"

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    aput-object v1, v2, v0

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->btYes:Landroid/widget/Button;

    new-instance v2, Lcom/sec/hearingadjust/HearingdroAnalysis$7;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$7;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->btNo:Landroid/widget/Button;

    new-instance v2, Lcom/sec/hearingadjust/HearingdroAnalysis$8;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$8;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    return-void
.end method

.method private Yes_No_Click(Z)V
    .locals 6
    .param p1, "Select"    # Z

    .prologue
    const/16 v4, -0xa

    .line 159
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    if-nez v1, :cond_0

    .line 176
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    invoke-virtual {v1}, Lcom/sec/hearingadjust/SoundManager;->stopSound()V

    .line 164
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    aget v1, v1, v2

    if-eq v1, v4, :cond_1

    .line 165
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    aget v1, v1, v2

    invoke-direct {p0, v1, p1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->setResult(IZ)V

    .line 167
    :cond_1
    iget v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexMax:I

    div-int v0, v1, v2

    .line 168
    .local v0, "progress":I
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvProgress:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " %"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivProgress:[Landroid/widget/ImageView;

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 170
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    aget v1, v1, v2

    if-ne v1, v4, :cond_2

    .line 171
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 172
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 174
    :cond_2
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->playTestTone()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/hearingadjust/HearingdroAnalysis;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->playTestTone()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/hearingadjust/HearingdroAnalysis;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->ChangeView(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvProgress:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/hearingadjust/HearingdroAnalysis;)[Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivProgress:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/hearingadjust/HearingdroAnalysis;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->startDiagnosis()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/hearingadjust/HearingdroAnalysis;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/hearingadjust/HearingdroAnalysis;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I

    return p1
.end method

.method static synthetic access$1308(Lcom/sec/hearingadjust/HearingdroAnalysis;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->signalIndex:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivSignal:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/hearingadjust/HearingdroAnalysis;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->endAnalysis()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->btYes:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->btNo:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvAnalysisTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/hearingadjust/HearingdroAnalysis;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->Yes_No_Click(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/hearingadjust/HearingdroAnalysis;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->Mode:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/hearingadjust/HearingdroAnalysis;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/hearingadjust/HearingdroAnalysis;)Lcom/sec/hearingadjust/SoundManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/hearingadjust/HearingdroAnalysis;Lcom/sec/hearingadjust/SoundManager;)Lcom/sec/hearingadjust/SoundManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;
    .param p1, "x1"    # Lcom/sec/hearingadjust/SoundManager;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/hearingadjust/HearingdroAnalysis;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/hearingadjust/HearingdroAnalysis;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/hearingadjust/HearingdroAnalysis;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    return p1
.end method

.method static synthetic access$802(Lcom/sec/hearingadjust/HearingdroAnalysis;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexMax:I

    return p1
.end method

.method static synthetic access$902(Lcom/sec/hearingadjust/HearingdroAnalysis;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/hearingadjust/HearingdroAnalysis;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    return p1
.end method

.method private endAnalysis()V
    .locals 10

    .prologue
    const/16 v7, 0xb

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 430
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I

    invoke-direct {p0, v5}, Lcom/sec/hearingadjust/HearingdroAnalysis;->savePreferences([I)V

    .line 432
    const-string v0, "0,1"

    .line 433
    .local v0, "dhaOption":Ljava/lang/String;
    const-string v1, ""

    .line 434
    .local v1, "dhaParam":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v7, :cond_0

    .line 435
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I

    aget v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 434
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 437
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 438
    iget-object v5, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dha="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 439
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_diagnosis"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 440
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_revision"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 441
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_direction"

    const/4 v7, -0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 442
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_musiccheck"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 443
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_parameters"

    invoke-static {v5, v6, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 445
    const-string v5, "DHA_PREF"

    invoke-virtual {p0, v5, v8}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 446
    .local v4, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 447
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "isFirstRun"

    invoke-interface {v2, v5, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 448
    const-string v5, "CheckDone"

    invoke-interface {v2, v5, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 449
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 451
    const v5, 0x7f070010

    invoke-static {p0, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 452
    const/4 v5, 0x2

    invoke-direct {p0, v5}, Lcom/sec/hearingadjust/HearingdroAnalysis;->ChangeView(I)V

    .line 453
    return-void
.end method

.method private loadSound()V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvAnalysisTitle:Landroid/widget/TextView;

    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivSignal:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->pbLoad:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 321
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/hearingadjust/HearingdroAnalysis$9;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$9;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->threadLoad:Ljava/lang/Thread;

    .line 378
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->threadLoad:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 379
    return-void
.end method

.method private playTestTone()V
    .locals 4

    .prologue
    .line 510
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->PlayState()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 511
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    invoke-virtual {v2}, Lcom/sec/hearingadjust/SoundManager;->stopSound()V

    .line 513
    :cond_0
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I

    iget v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    aget v0, v2, v3

    .line 514
    .local v0, "tempFreq":I
    mul-int/lit8 v2, v0, 0x9

    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I

    aget v3, v3, v0

    div-int/lit8 v3, v3, 0x5

    add-int v1, v2, v3

    .line 515
    .local v1, "toneIndex":I
    iget-object v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    invoke-virtual {v2, v1}, Lcom/sec/hearingadjust/SoundManager;->Play(I)V

    .line 516
    return-void
.end method

.method private savePreferences([I)V
    .locals 10
    .param p1, "result"    # [I

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0xb

    .line 463
    const-string v5, "DHA_PREF"

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 464
    .local v3, "progressPreference":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 465
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hearing_direction"

    const/4 v7, -0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 467
    const-string v4, ""

    .line 468
    .local v4, "savedata":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v8, :cond_0

    .line 469
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, p1, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 468
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 471
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, p1, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 472
    const-string v5, "Pre0"

    invoke-interface {v1, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 473
    const-string v5, "PresetSelect"

    invoke-interface {v1, v5, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 474
    const-string v5, "isPreset"

    invoke-interface {v3, v5, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 476
    .local v0, "PresetState":I
    if-nez v0, :cond_2

    .line 477
    const-string v5, "Pre1"

    invoke-interface {v1, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 479
    const-string v4, ""

    .line 480
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v8, :cond_1

    .line 481
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "5,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 480
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 483
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "5"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 485
    const-string v5, "Pre2"

    invoke-interface {v1, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 486
    const-string v5, "isPreset"

    const/4 v6, 0x1

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 488
    :cond_2
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 489
    return-void
.end method

.method private setResult(IZ)V
    .locals 2
    .param p1, "testFreq"    # I
    .param p2, "listen"    # Z

    .prologue
    .line 279
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I

    aget v0, v1, p1

    .line 280
    .local v0, "result":I
    sparse-switch v0, :sswitch_data_0

    .line 312
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->resultData:[I

    aput v0, v1, p1

    .line 313
    iget v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    .line 314
    return-void

    .line 282
    :sswitch_0
    if-eqz p2, :cond_1

    .line 283
    const/16 v0, 0xa

    goto :goto_0

    .line 285
    :cond_1
    const/16 v0, 0x1e

    .line 287
    goto :goto_0

    .line 289
    :sswitch_1
    if-eqz p2, :cond_2

    .line 290
    const/4 v0, 0x5

    goto :goto_0

    .line 292
    :cond_2
    const/16 v0, 0xa

    .line 294
    goto :goto_0

    .line 296
    :sswitch_2
    if-eqz p2, :cond_3

    .line 297
    add-int/lit8 v0, v0, -0x5

    goto :goto_0

    .line 299
    :cond_3
    add-int/lit8 v0, v0, 0x5

    .line 301
    goto :goto_0

    .line 303
    :sswitch_3
    if-nez p2, :cond_0

    .line 306
    add-int/lit8 v0, v0, 0x5

    .line 308
    goto :goto_0

    .line 280
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0xf -> :sswitch_3
        0x14 -> :sswitch_0
        0x19 -> :sswitch_3
        0x1e -> :sswitch_2
        0x23 -> :sswitch_3
    .end sparse-switch
.end method

.method private startDiagnosis()V
    .locals 4

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    if-nez v0, :cond_1

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->Mode:Z

    if-nez v0, :cond_0

    .line 500
    :cond_2
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 501
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 502
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->pbLoad:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 503
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivSignal:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 504
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 505
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 506
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->playTestTone()V

    goto :goto_0
.end method


# virtual methods
.method public PlayState()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getVolume()I
    .locals 2

    .prologue
    .line 571
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    .line 181
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 182
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvAnalysisTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 183
    .local v1, "Tmpstring_title":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvProgress:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 185
    .local v0, "Tmpstring_process":Ljava/lang/String;
    const v3, 0x7f030001

    invoke-virtual {p0, v3}, Lcom/sec/hearingadjust/HearingdroAnalysis;->setContentView(I)V

    .line 186
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->Setid()V

    .line 188
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvAnalysisTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->tvProgress:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->threadLoad:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v3

    sget-object v4, Ljava/lang/Thread$State;->RUNNABLE:Ljava/lang/Thread$State;

    if-ne v3, v4, :cond_0

    .line 192
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivSignal:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 193
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->pbLoad:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 194
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 195
    iget v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    add-int/lit8 v2, v3, 0x1

    .local v2, "i":I
    :goto_0
    const/16 v3, 0x24

    if-ge v2, v3, :cond_2

    .line 196
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivProgress:[Landroid/widget/ImageView;

    aget-object v3, v3, v2

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 195
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 200
    .end local v2    # "i":I
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    if-ge v2, v3, :cond_1

    .line 201
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivProgress:[Landroid/widget/ImageView;

    aget-object v3, v3, v2

    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 200
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 203
    :cond_1
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivSignal:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    iget-object v3, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->pbLoad:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 206
    :cond_2
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f070020

    const v6, 0x7f07001b

    const v5, 0x7f07000d

    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->setContentView(I)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 72
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 74
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/hearingadjust/HearingdroAnalysis;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;

    .line 75
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->Mode:Z

    .line 76
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->bcr:Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/sec/hearingadjust/HearingdroAnalysis;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 77
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->bcr:Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/sec/hearingadjust/HearingdroAnalysis;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 78
    new-instance v1, Lcom/sec/hearingadjust/ViewManager;

    invoke-direct {v1, p0}, Lcom/sec/hearingadjust/ViewManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->viewManager:Lcom/sec/hearingadjust/ViewManager;

    .line 79
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070008

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroAnalysis$3;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$3;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroAnalysis$2;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$2;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-virtual {v1, v7, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroAnalysis$1;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$1;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->AlertContinue:Landroid/app/AlertDialog;

    .line 114
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070005

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroAnalysis$6;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$6;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroAnalysis$5;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$5;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-virtual {v1, v7, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/hearingadjust/HearingdroAnalysis$4;

    invoke-direct {v2, p0}, Lcom/sec/hearingadjust/HearingdroAnalysis$4;-><init>(Lcom/sec/hearingadjust/HearingdroAnalysis;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->AlertPopup:Landroid/app/AlertDialog;

    .line 148
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->Setid()V

    .line 150
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->uiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 151
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x24

    if-ge v0, v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->ivProgress:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_0
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->loadSound()V

    .line 155
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->bcr:Lcom/sec/hearingadjust/HearingdroAnalysis$bcrHeadSet;

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 566
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->DestroySound()V

    .line 567
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 568
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 521
    sparse-switch p1, :sswitch_data_0

    .line 545
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    :sswitch_0
    return v0

    .line 526
    :sswitch_1
    iget-boolean v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    if-eqz v1, :cond_1

    .line 527
    const/4 v0, 0x0

    goto :goto_0

    .line 529
    :cond_1
    iget-boolean v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I

    iget v2, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    aget v1, v1, v2

    const/16 v2, -0xa

    if-eq v1, v2, :cond_3

    .line 530
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->PlayState()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 531
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    invoke-virtual {v1}, Lcom/sec/hearingadjust/SoundManager;->stopSound()V

    .line 533
    :cond_2
    iget-object v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->AlertPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 536
    :cond_3
    iget-boolean v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    if-nez v1, :cond_0

    goto :goto_0

    .line 521
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 245
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    if-nez v0, :cond_2

    .line 246
    iget-boolean v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->diagnosisOrder:[I

    iget v1, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->indexOrder:I

    aget v0, v0, v1

    const/16 v1, -0xa

    if-eq v0, v1, :cond_1

    .line 247
    invoke-virtual {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->PlayState()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    invoke-virtual {v0}, Lcom/sec/hearingadjust/SoundManager;->stopSound()V

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->AlertPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 252
    :cond_1
    const/4 v0, 0x1

    .line 254
    :goto_0
    return v0

    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    if-nez v0, :cond_0

    .line 272
    iget v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->volumeLevel:I

    invoke-virtual {p0, v0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->setVolume(I)V

    .line 273
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->smTonePlayer:Lcom/sec/hearingadjust/SoundManager;

    invoke-virtual {v0}, Lcom/sec/hearingadjust/SoundManager;->stopSound()V

    .line 275
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 276
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 259
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 260
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->AlertPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->AlertPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 263
    :cond_0
    iget-boolean v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->isLoading:Z

    if-nez v0, :cond_1

    .line 264
    invoke-direct {p0}, Lcom/sec/hearingadjust/HearingdroAnalysis;->playTestTone()V

    .line 265
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->AlertContinue:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 267
    :cond_1
    return-void
.end method

.method public setVolume(I)V
    .locals 3
    .param p1, "volume"    # I

    .prologue
    .line 574
    iget-object v0, p0, Lcom/sec/hearingadjust/HearingdroAnalysis;->am:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 575
    return-void
.end method
