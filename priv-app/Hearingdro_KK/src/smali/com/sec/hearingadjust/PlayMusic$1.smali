.class Lcom/sec/hearingadjust/PlayMusic$1;
.super Ljava/lang/Object;
.source "PlayMusic.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/hearingadjust/PlayMusic;->BeforeFunc()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/hearingadjust/PlayMusic;


# direct methods
.method constructor <init>(Lcom/sec/hearingadjust/PlayMusic;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/16 v7, 0x400

    const/4 v6, 0x0

    .line 99
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$000(Lcom/sec/hearingadjust/PlayMusic;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 100
    new-array v1, v7, [S

    .line 101
    .local v1, "injni":[S
    const/4 v2, 0x0

    .line 102
    .local v2, "k":I
    const/4 v0, 0x0

    .local v0, "i":I
    move v3, v2

    .end local v2    # "k":I
    .local v3, "k":I
    :goto_1
    const v4, 0xbc278

    if-ge v0, v4, :cond_3

    .line 104
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "k":I
    .restart local v2    # "k":I
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    iget-object v5, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->buffer:[S
    invoke-static {v5}, Lcom/sec/hearingadjust/PlayMusic;->access$100(Lcom/sec/hearingadjust/PlayMusic;)[S

    move-result-object v5

    aget-short v5, v5, v0

    # invokes: Lcom/sec/hearingadjust/PlayMusic;->CheckData(SI)S
    invoke-static {v4, v5, v0}, Lcom/sec/hearingadjust/PlayMusic;->access$200(Lcom/sec/hearingadjust/PlayMusic;SI)S

    move-result v4

    aput-short v4, v1, v3

    .line 105
    if-ne v2, v7, :cond_2

    .line 107
    const/4 v2, 0x0

    .line 108
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v1, v6, v5}, Landroid/media/AudioTrack;->write([SII)I

    .line 109
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$000(Lcom/sec/hearingadjust/PlayMusic;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 111
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioTrack;->stop()V

    .line 112
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioTrack;->release()V

    .line 113
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    const v5, 0xac44

    invoke-virtual {v4, v5}, Lcom/sec/hearingadjust/PlayMusic;->InitMusic(I)V

    .line 114
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$400(Lcom/sec/hearingadjust/PlayMusic;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 126
    .end local v0    # "i":I
    .end local v1    # "injni":[S
    .end local v2    # "k":I
    :cond_1
    return-void

    .line 102
    .restart local v0    # "i":I
    .restart local v1    # "injni":[S
    .restart local v2    # "k":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    .end local v2    # "k":I
    .restart local v3    # "k":I
    goto :goto_1

    .line 119
    :cond_3
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicTrack:Landroid/media/AudioTrack;
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$300(Lcom/sec/hearingadjust/PlayMusic;)Landroid/media/AudioTrack;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v1, v6, v5}, Landroid/media/AudioTrack;->write([SII)I

    .line 120
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # operator++ for: Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$508(Lcom/sec/hearingadjust/PlayMusic;)I

    .line 121
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->MusicPlayCnt:I
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$500(Lcom/sec/hearingadjust/PlayMusic;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 122
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # getter for: Lcom/sec/hearingadjust/PlayMusic;->uiHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/hearingadjust/PlayMusic;->access$400(Lcom/sec/hearingadjust/PlayMusic;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 123
    iget-object v4, p0, Lcom/sec/hearingadjust/PlayMusic$1;->this$0:Lcom/sec/hearingadjust/PlayMusic;

    # setter for: Lcom/sec/hearingadjust/PlayMusic;->BeforeMode:Z
    invoke-static {v4, v6}, Lcom/sec/hearingadjust/PlayMusic;->access$002(Lcom/sec/hearingadjust/PlayMusic;Z)Z

    goto/16 :goto_0
.end method
