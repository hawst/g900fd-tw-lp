.class public Lcom/sec/android/app/SecProductFeature_COMMON;
.super Ljava/lang/Object;
.source "SecProductFeature_COMMON.java"


# static fields
.field public static final SEC_PRODUCT_FEATURE_COMMON_BACKGROUND_DATA_TRAFFIC:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_BOARD_SOURCE_TYPE:Ljava/lang/String; = "phone"

.field public static final SEC_PRODUCT_FEATURE_COMMON_BRAND_NAME:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_COMMON_DISABLE_POPUP_UI:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_DSDS_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_DUALCP_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_DUALMODE_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_DUALRIL_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_DUAL_VIBRATION_MOTOR_CONTROL:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_DUOS_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_3DACTTOACT_FLAG:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_ABSOLUTE_ANTITHEFT:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_BAIDUMAP:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_CHANGABLE_HOME_THEME:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_DROPBOX_DEEP_INTEG:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_DYNAMIC_FPS_CONTROL:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_DYNAMIC_MULTI_SIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_DYNAMIC_SURFACE_SCALING:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_ENHANCED_FONT_CLARITY:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_EXT_DEBUG_CUSTOMER_SVC:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_GOOGLE_DRIVE_DEEP_INTEG:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_INDEXED_PALETTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_INTEGRATOR_HAPTIC:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_IOTMODE4KOR:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_MESSAGING_LABEL:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_MOUSE_HOVERING:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_POWERSTRETCH_SOLUTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_PRIVATE_CONTACT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_RESOURCE_CACHING:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_RUGGEDIZED_FEATURE:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_SECRET_BOX:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_SECURE_STORAGE_LSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_STYLUS_PEN:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_TEXTURE_COMPRESSION:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_TEXTURE_COMPRESSION_HYBRID:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_ENABLE_TORCH_LIGHT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_ETHERNET_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_FINGERPRINT:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_GALAXY_PLAYER:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_GALAXY_WATCH:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_GESTURE_ENABLE_AIR_MOVE:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_GESTURE_WITH_FINGERHOVER:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_GESTURE_WITH_IRSENSOR:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_HARDKEY:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_HW_VENDOR_CONFIG:Ljava/lang/String; = "Combination"

.field public static final SEC_PRODUCT_FEATURE_COMMON_HW_VENDOR_CONFIG_COMBINATION:Ljava/lang/String; = "Combination"

.field public static final SEC_PRODUCT_FEATURE_COMMON_HW_VENDOR_CONFIG_STRAWBERRY:Ljava/lang/String; = "Strawberry"

.field public static final SEC_PRODUCT_FEATURE_COMMON_JIT_CODE_UNCHAIN_WORKAROUND:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_KNOX_SDP_ENABLE:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_LIVEPICKER_HIDE_ACTIONBAR:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_LTE_POWER_BACKOFF:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_LTE_ROAMING_DISABLE_CANADA:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_MASS_WALLPAPERPICKER:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_MINIAPP_PLUS:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_MODEL:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_COMMON_MULTISIM_VER:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_COMMON_OPERATOR:Ljava/lang/String; = "SKT"

.field public static final SEC_PRODUCT_FEATURE_COMMON_POWER_SAVING_MODE:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_PREFFERED_TEXTURE_COMPRESSION_HYBRID_JPG:Ljava/lang/String; = "JPG"

.field public static final SEC_PRODUCT_FEATURE_COMMON_PREFFERED_TEXTURE_COMPRESSION_HYBRID_PNG:Ljava/lang/String; = "PNG"

.field public static final SEC_PRODUCT_FEATURE_COMMON_PREFFERED_TEXTURE_COMPRESSION_IPT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_PREFFERED_TEXTURE_COMPRESSION_TYPE:Ljava/lang/String; = "NONE"

.field public static final SEC_PRODUCT_FEATURE_COMMON_RC_BASE_MODEL:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_COMMON_REGION:Ljava/lang/String; = "KOR"

.field public static final SEC_PRODUCT_FEATURE_COMMON_RESET_REASON:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_RF_TYPE_CONFIG:Ljava/lang/String; = "lte"

.field public static final SEC_PRODUCT_FEATURE_COMMON_SMARTPHONE_TYPE_NOTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SMARTPHONE_TYPE_PHONEBLET:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SPC_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SPRINT_EXTENSION:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_DIRTYRECT:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_DORMANT_MODE:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_EASY_ONE_HAND_OPERATION:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_FLASH_NOTIFICATION:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_FRAGMENT_LISTVIEW:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_ITSON:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_ONE_HAND_OPERATION:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_POGO:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_SAFETYCARE:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_VIDEO_EDITOR:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_VIDEO_EDITOR_VEDITOR:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_SUPPORT_VIDEO_EDITOR_WITH_ADMIN:Z = true

.field public static final SEC_PRODUCT_FEATURE_COMMON_TC_JAVAHEAP_PATCH_ENABLE:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_TEXTURE_COMPRESSION_APK_CONVERTER:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_TEXTURE_COMPRESSION_HYBRID_PNG:Ljava/lang/String; = "PNG"

.field public static final SEC_PRODUCT_FEATURE_COMMON_TEXTURE_COMPRESSION_HYBRID_QMG_OPTION:Ljava/lang/String; = "91"

.field public static final SEC_PRODUCT_FEATURE_COMMON_TEXTURE_COMPRESSION_HYBRID_SPI_OPTION:Ljava/lang/String; = "15 2 0"

.field public static final SEC_PRODUCT_FEATURE_COMMON_TRANSPARENCY_GRADATION_IND:Z = false

.field public static final SEC_PRODUCT_FEATURE_COMMON_UPGRADE_OS_FROM:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_COMMON_USE_ACRODEA_AVATAR_MAKER:Z

.field public static final SEC_PRODUCT_FEATURE_COMMON_USE_HOLO_7INCH_THEME:Z

.field public static final SEC_PRODUCT_FEATURE_COMMON_USE_MULTISIM:Z

.field public static final SEC_PRODUCT_FEATURE_COMMON_USE_MULTISIM_SIM_PROFILE:Z

.field public static final SEC_PRODUCT_FEATURE_COMMON_WAIT_FOR_CP_UPGRADE:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
