.class public Lcom/fmm/dm/XDMUpdateURLReceiver;
.super Landroid/content/BroadcastReceiver;
.source "XDMUpdateURLReceiver.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XCommonInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Lcom/fmm/dm/interfaces/XNOTIInterface;


# static fields
.field public static final XCOMMON_INTENT_CHANGE_SERVER_INFO:Ljava/lang/String; = "android.intent.action.pcw.UPDATE_URL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 26
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.pcw.UPDATE_URL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 28
    const-string v1, "android.intent.action.pcw.UPDATE_URL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 30
    sget-boolean v1, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    const/4 v1, 0x1

    sput-boolean v1, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    .line 33
    const-string v1, "FMMDM SERVICE START Set!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 34
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/fmm/dm/XDMService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 38
    .end local v0    # "cn":Landroid/content/ComponentName;
    :cond_0
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_1

    .line 40
    invoke-static {p2}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSetInitIntent(Landroid/content/Intent;)V

    .line 41
    const/4 v1, 0x7

    invoke-static {v1}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    .line 50
    :goto_0
    return-void

    .line 44
    :cond_1
    invoke-static {p2}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSetUpdateURL(Landroid/content/Intent;)V

    goto :goto_0

    .line 48
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method
