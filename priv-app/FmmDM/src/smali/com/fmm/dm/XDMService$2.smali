.class Lcom/fmm/dm/XDMService$2;
.super Landroid/os/Handler;
.source "XDMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fmm/dm/XDMService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/XDMService;


# direct methods
.method constructor <init>(Lcom/fmm/dm/XDMService;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/fmm/dm/XDMService$2;->this$0:Lcom/fmm/dm/XDMService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x5

    const/4 v1, 0x3

    .line 348
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 350
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 466
    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device failed - unknown operation requested"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 470
    :goto_0
    sget-object v0, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 471
    return-void

    .line 354
    :pswitch_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device  succeeded - operation to be performed is full lock"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v0, "XLAWMO_OPERATION_FULLYLOCK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 357
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSyncOerationFullyLock()Z

    goto :goto_0

    .line 362
    :pswitch_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device  succeeded - operation to be performed is partial lock"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v0, "XLAWMO_OPERATION_PARTIALLYLOCK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 365
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpOerationPartiallyLock()Z

    goto :goto_0

    .line 370
    :pswitch_2
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device  succeeded - operation to be performed is unlock"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string v0, "XLAWMO_OPERATION_UNLOCK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 373
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpOerationUnLock()Z

    goto :goto_0

    .line 378
    :pswitch_3
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device  succeeded - operation to be performed is wipe"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v0, "XLAWMO_OPERATION_WIPE"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 381
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpOerationWipe()Z

    goto :goto_0

    .line 385
    :pswitch_4
    const-string v0, "XLAWMO_OPERATION_EMEREGNCY"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 386
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpOerationEmergencyMode()Z

    .line 387
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    goto :goto_0

    .line 391
    :pswitch_5
    const-string v0, "XLAWMO_OPERATION_REACTIVATIONLOCK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 392
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpOerationReactivationLock()Z

    goto :goto_0

    .line 397
    :pswitch_6
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is factory reset"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 399
    const-string v0, "XLAWMO_OPERATION_FACTORYRESET"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 400
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpOerationFactoryReset()Z

    goto/16 :goto_0

    .line 405
    :pswitch_7
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is phone reset"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v0, "XLAWMO_OPERATION_RESET"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 412
    :pswitch_8
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is change lock message"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpLockMyPhoneOperationChangeMsg()Z

    .line 415
    const-string v0, "XLAWMO_LOCKMYPHONE_OPERATION_CHANGEMSG"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 420
    :pswitch_9
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is block calls"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 422
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpCallrestrictionOperationsRestriction()Z

    .line 423
    const-string v0, "XLAWMO_CALLRESTRICTION_OPERATIONS_RESTRICTION"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 428
    :pswitch_a
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is unblock calls"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 430
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpCallrestrictionOperationsStop()Z

    .line 431
    const-string v0, "XLAWMO_CALLRESTRICTION_OPERATIONS_STOP"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 436
    :pswitch_b
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is phone ringing"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSyncRingMyPhoneOperationsStart()Z

    .line 440
    const-string v0, "XLAWMO_RINGMYPHONE_OPERATIONS_START"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 445
    :pswitch_c
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is divert request"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpForwardingOperationsDivertRequest()Z

    .line 448
    const-string v0, "XLAWMO_FORWARDING_OPERATIONS_DIVERT"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 453
    :pswitch_d
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is release master-key lock"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 455
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSyncMasterKeyOperationsLockRelease()V

    .line 456
    const-string v0, "XLAWMO_MASTERKEY_OPERATIONS_LOCKRELEASE"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 460
    :pswitch_e
    const-string v0, "XLAWMO_OPERATION_CONNECTION_CHECK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 461
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpOerationConnectionCheck()Z

    goto/16 :goto_0

    .line 350
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
