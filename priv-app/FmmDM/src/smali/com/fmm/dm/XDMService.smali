.class public Lcom/fmm/dm/XDMService;
.super Landroid/app/Service;
.source "XDMService.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XCommonInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XEventInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Lcom/fmm/dm/interfaces/XUIInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;,
        Lcom/fmm/dm/XDMService$XDMPhoneStateListener3;,
        Lcom/fmm/dm/XDMService$XDMPhoneStateListener2;,
        Lcom/fmm/dm/XDMService$XDMPhoneStateListener;,
        Lcom/fmm/dm/XDMService$XDMLocalBinder;
    }
.end annotation


# static fields
.field public static final XDM_CONTEXT_TELEPHONY_SERVICE1:Ljava/lang/String; = "phone1"

.field public static final XDM_CONTEXT_TELEPHONY_SERVICE2:Ljava/lang/String; = "phone2"

.field private static final XDM_SS_ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"

.field public static g_Task:Lcom/fmm/dm/agent/XDMTask;

.field public static g_UITask:Lcom/fmm/dm/agent/XDMUITask;

.field public static g_bIsInitializing:Z

.field public static g_bRestoreDataRoaming:Z

.field public static g_hAMTHandler:Landroid/os/Handler;

.field public static g_hDmHandler:Landroid/os/Handler;

.field public static g_hLawmoHandler:Landroid/os/Handler;

.field public static g_nResumeStatus:I

.field public static mDataState:I

.field public static mDataState2:I

.field private static m_Context:Landroid/content/Context;

.field private static m_ToastAlreadyAdded:Landroid/widget/Toast;

.field private static m_WakeLock:Landroid/os/PowerManager$WakeLock;

.field private static m_bIsRoaming:Z

.field private static m_bPinLockState:Z

.field public static nServiceType:I

.field public static nServiceType2:I

.field public static nSimStatus:I

.field public static nSimStatus2:I

.field public static phoneListener:Lcom/fmm/dm/XDMService$XDMPhoneStateListener;

.field public static phoneListener2:Lcom/fmm/dm/XDMService$XDMPhoneStateListener2;

.field public static phoneListener3:Lcom/fmm/dm/XDMService$XDMPhoneStateListener3;

.field public static phoneListener4:Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;

.field public static phoneListener5:Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;

.field public static serviceState:Landroid/telephony/ServiceState;

.field public static serviceState2:Landroid/telephony/ServiceState;

.field public static serviceStateDual:[Landroid/telephony/ServiceState;

.field public static telephonyManager:Landroid/telephony/TelephonyManager;

.field public static telephonyManager2:Landroid/telephony/TelephonyManager;

.field public static telephonyManagerDual:[Landroid/telephony/TelephonyManager;


# instance fields
.field private final m_Binder:Landroid/os/IBinder;

.field private m_DataReceiver:Landroid/content/BroadcastReceiver;

.field private m_WifiReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    sput-object v1, Lcom/fmm/dm/XDMService;->g_Task:Lcom/fmm/dm/agent/XDMTask;

    .line 73
    sput-object v1, Lcom/fmm/dm/XDMService;->g_UITask:Lcom/fmm/dm/agent/XDMUITask;

    .line 74
    sput-object v1, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    .line 75
    sput-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    .line 76
    sput-object v1, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    .line 80
    sput-object v1, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    .line 82
    sput v0, Lcom/fmm/dm/XDMService;->nSimStatus:I

    .line 83
    sput v2, Lcom/fmm/dm/XDMService;->mDataState:I

    .line 88
    sput-object v1, Lcom/fmm/dm/XDMService;->serviceState2:Landroid/telephony/ServiceState;

    .line 90
    sput v0, Lcom/fmm/dm/XDMService;->nSimStatus2:I

    .line 91
    sput v2, Lcom/fmm/dm/XDMService;->mDataState2:I

    .line 94
    new-array v0, v3, [Landroid/telephony/TelephonyManager;

    sput-object v0, Lcom/fmm/dm/XDMService;->telephonyManagerDual:[Landroid/telephony/TelephonyManager;

    .line 95
    new-array v0, v3, [Landroid/telephony/ServiceState;

    sput-object v0, Lcom/fmm/dm/XDMService;->serviceStateDual:[Landroid/telephony/ServiceState;

    .line 104
    sput v2, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    .line 105
    sput-boolean v2, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    .line 106
    sput-boolean v2, Lcom/fmm/dm/XDMService;->g_bRestoreDataRoaming:Z

    .line 111
    sput-object v1, Lcom/fmm/dm/XDMService;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 120
    sput-boolean v2, Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z

    .line 121
    sput-boolean v2, Lcom/fmm/dm/XDMService;->m_bPinLockState:Z

    .line 123
    sput-object v1, Lcom/fmm/dm/XDMService;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 118
    new-instance v0, Lcom/fmm/dm/XDMService$XDMLocalBinder;

    invoke-direct {v0, p0}, Lcom/fmm/dm/XDMService$XDMLocalBinder;-><init>(Lcom/fmm/dm/XDMService;)V

    iput-object v0, p0, Lcom/fmm/dm/XDMService;->m_Binder:Landroid/os/IBinder;

    .line 1182
    return-void
.end method

.method static synthetic access$000(Lcom/fmm/dm/XDMService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/XDMService;
    .param p1, "x1"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/fmm/dm/XDMService;->xdmCallUiDialogActivity(I)V

    return-void
.end method

.method static synthetic access$100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 70
    sget-boolean v0, Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 70
    sput-boolean p0, Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z

    return p0
.end method

.method private xdmCallUiDialogActivity(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 1343
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1344
    new-instance v0, Landroid/content/Intent;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    const-class v4, Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1345
    .local v0, "DialIntent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1346
    invoke-virtual {p0, v0}, Lcom/fmm/dm/XDMService;->startActivity(Landroid/content/Intent;)V

    .line 1347
    return-void
.end method

.method public static xdmCheckIdleScreen()Z
    .locals 6

    .prologue
    .line 786
    const/4 v0, 0x0

    .line 790
    .local v0, "bRet":Z
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetTopPackageName()Ljava/lang/String;

    move-result-object v3

    .line 791
    .local v3, "szTopPackage":Ljava/lang/String;
    const-string v4, "android"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "settings"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "fotaclient"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    .line 793
    :cond_0
    const/4 v0, 0x1

    .line 817
    .end local v3    # "szTopPackage":Ljava/lang/String;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Idle Screen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 819
    return v0

    .line 795
    .restart local v3    # "szTopPackage":Ljava/lang/String;
    :cond_1
    :try_start_1
    sget-object v4, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 797
    const/4 v0, 0x1

    goto :goto_0

    .line 801
    :cond_2
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetHomeScreenList()Ljava/util/ArrayList;

    move-result-object v2

    .line 802
    .local v2, "homeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    if-eqz v4, :cond_3

    .line 804
    const/4 v0, 0x1

    goto :goto_0

    .line 808
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 812
    .end local v2    # "homeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "szTopPackage":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 814
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetAccountRegistration(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1495
    const/4 v2, 0x0

    .line 1496
    .local v2, "bRegistration":Z
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1497
    .local v1, "am":Landroid/accounts/AccountManager;
    if-eqz v1, :cond_2

    .line 1499
    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    .line 1501
    .local v0, "account":[Landroid/accounts/Account;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_0

    .line 1503
    const-string v4, "com.osp.app.signin"

    aget-object v5, v0, v3

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1505
    const-string v4, "Samsung Account Exist !!!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1506
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetRegistration(Z)V

    .line 1507
    const/4 v2, 0x1

    .line 1516
    .end local v0    # "account":[Landroid/accounts/Account;
    .end local v3    # "i":I
    :cond_0
    :goto_1
    return v2

    .line 1501
    .restart local v0    # "account":[Landroid/accounts/Account;
    .restart local v3    # "i":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1514
    .end local v0    # "account":[Landroid/accounts/Account;
    .end local v3    # "i":I
    :cond_2
    const-string v4, "Account Manager is null!!!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmGetDataRoamingEnabled()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1413
    :try_start_0
    sget-object v2, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "data_roaming"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x1

    .line 1417
    .local v0, "snfe":Landroid/provider/Settings$SettingNotFoundException;
    :cond_0
    :goto_0
    return v1

    .line 1415
    .end local v0    # "snfe":Landroid/provider/Settings$SettingNotFoundException;
    :catch_0
    move-exception v0

    .line 1417
    .restart local v0    # "snfe":Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_0
.end method

.method public static xdmGetDataState()I
    .locals 1

    .prologue
    .line 674
    sget v0, Lcom/fmm/dm/XDMService;->mDataState:I

    return v0
.end method

.method public static xdmGetDataState2()I
    .locals 1

    .prologue
    .line 685
    sget v0, Lcom/fmm/dm/XDMService;->mDataState2:I

    return v0
.end method

.method public static xdmGetHomeScreenList()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 760
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 764
    .local v3, "homeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    sget-object v7, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 765
    .local v6, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 766
    .local v2, "homeIntent":Landroid/content/Intent;
    const-string v7, "android.intent.category.HOME"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 768
    const/4 v7, 0x1

    invoke-virtual {v6, v2, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 770
    .local v1, "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 772
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 773
    .local v5, "info":Landroid/content/pm/ResolveInfo;
    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 770
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 776
    .end local v1    # "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v2    # "homeIntent":Landroid/content/Intent;
    .end local v4    # "i":I
    .end local v5    # "info":Landroid/content/pm/ResolveInfo;
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 778
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 781
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-object v3
.end method

.method public static xdmGetPinLockState()Z
    .locals 2

    .prologue
    .line 640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_bPinLockState ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/XDMService;->m_bPinLockState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 641
    sget-boolean v0, Lcom/fmm/dm/XDMService;->m_bPinLockState:Z

    return v0
.end method

.method public static xdmGetProxyData()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1352
    new-instance v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    sget-object v3, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;-><init>(Landroid/content/Context;)V

    .line 1353
    .local v2, "netInfo":Lcom/fmm/dm/agent/XDMAppProtoNetInfo;
    iget-object v3, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    if-nez v3, :cond_0

    .line 1354
    new-instance v3, Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-direct {v3}, Lcom/fmm/dm/agent/XDMAppConnectSetting;-><init>()V

    iput-object v3, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    .line 1355
    :cond_0
    iget-object v3, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    if-nez v3, :cond_1

    .line 1356
    new-instance v3, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    invoke-direct {v3}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;-><init>()V

    iput-object v3, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    .line 1358
    :cond_1
    sget-boolean v3, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-nez v3, :cond_2

    .line 1360
    invoke-static {v2}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpNetGetProfileData(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)V

    .line 1361
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetConRef()Lcom/fmm/dm/db/file/XDBInfoConRef;

    move-result-object v0

    .line 1365
    .local v0, "conref":Lcom/fmm/dm/db/file/XDBInfoConRef;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppGetExistApn()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1367
    invoke-static {}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbResetNetworkTable()V

    .line 1390
    .end local v0    # "conref":Lcom/fmm/dm/db/file/XDBInfoConRef;
    :cond_2
    :goto_0
    return-object v2

    .line 1370
    .restart local v0    # "conref":Lcom/fmm/dm/db/file/XDBInfoConRef;
    :cond_3
    invoke-static {}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppGetExistApn()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1372
    if-eqz v0, :cond_2

    .line 1374
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szApn:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    .line 1375
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->szAccountName:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    .line 1376
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v4, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 1377
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v4, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget v4, v4, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    iput v4, v3, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    .line 1378
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szId:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    .line 1379
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    .line 1381
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetConRef(Lcom/fmm/dm/db/file/XDBInfoConRef;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1385
    :catch_0
    move-exception v1

    .line 1387
    .local v1, "ex":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetRoamingState()Z
    .locals 1

    .prologue
    .line 669
    sget-boolean v0, Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z

    return v0
.end method

.method public static xdmGetServerURLFromDSMProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1565
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetServerUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p0, "szServiceName"    # Ljava/lang/String;

    .prologue
    .line 1531
    const/4 v2, 0x0

    .line 1534
    .local v2, "manager":Ljava/lang/Object;
    :try_start_0
    sget-object v3, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 1535
    if-nez v2, :cond_0

    .line 1537
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0xa

    if-ge v1, v3, :cond_0

    .line 1541
    const-wide/16 v4, 0x3e8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1548
    :goto_1
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is null, retry..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1549
    sget-object v3, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 1550
    if-eqz v2, :cond_1

    .line 1560
    .end local v1    # "i":I
    .end local v2    # "manager":Ljava/lang/Object;
    :cond_0
    :goto_2
    return-object v2

    .line 1543
    .restart local v1    # "i":I
    .restart local v2    # "manager":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 1545
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 1555
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    .end local v2    # "manager":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 1557
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 1537
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    .restart local v2    # "manager":Ljava/lang/Object;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xdmGetServiceType()I
    .locals 1

    .prologue
    .line 591
    sget v0, Lcom/fmm/dm/XDMService;->nServiceType:I

    return v0
.end method

.method public static xdmGetServiceType2()I
    .locals 1

    .prologue
    .line 611
    sget v0, Lcom/fmm/dm/XDMService;->nServiceType2:I

    return v0
.end method

.method public static xdmGetSimState()I
    .locals 1

    .prologue
    .line 646
    sget v0, Lcom/fmm/dm/XDMService;->nSimStatus:I

    return v0
.end method

.method public static xdmGetSimState2()I
    .locals 1

    .prologue
    .line 657
    sget v0, Lcom/fmm/dm/XDMService;->nSimStatus2:I

    return v0
.end method

.method public static xdmGetTopPackageName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 726
    const-string v4, ""

    .line 730
    .local v4, "szPackageName":Ljava/lang/String;
    :try_start_0
    const-string v5, "activity"

    invoke-static {v5}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 731
    .local v0, "activityManager":Landroid/app/ActivityManager;
    if-nez v0, :cond_0

    .line 733
    const-string v5, "activityManager is null!!"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 734
    const-string v4, "NoPackage"

    .line 755
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    :goto_0
    return-object v4

    .line 738
    .restart local v0    # "activityManager":Landroid/app/ActivityManager;
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 739
    .local v3, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v3, :cond_1

    .line 741
    const-string v5, "RunningTasks is null!!"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 742
    const-string v4, "NoPackage"

    .line 743
    goto :goto_0

    .line 746
    :cond_1
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 747
    .local v1, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 749
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    .end local v1    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :catch_0
    move-exception v2

    .line 751
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 752
    const-string v4, "NoPackage"

    goto :goto_0
.end method

.method public static xdmProtoIsMobileDataConnected()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1453
    const/4 v0, 0x0

    .line 1456
    .local v0, "bRet":Z
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1457
    .local v1, "cm":Landroid/net/ConnectivityManager;
    if-nez v1, :cond_0

    .line 1459
    const-string v5, "ConnectivityManager is null!!"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1483
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mobile Data Connected : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1489
    .end local v0    # "bRet":Z
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    :goto_1
    return v0

    .line 1463
    .restart local v0    # "bRet":Z
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1465
    .local v3, "mobileInfo":Landroid/net/NetworkInfo;
    if-nez v3, :cond_1

    .line 1467
    const-string v5, "moblie data info is null!!"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1486
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    .end local v3    # "mobileInfo":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v2

    .line 1488
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v4

    .line 1489
    goto :goto_1

    .line 1471
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "mobileInfo":Landroid/net/NetworkInfo;
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1473
    const-string v5, "Mobile Data Connected"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1474
    const/4 v0, 0x1

    goto :goto_0

    .line 1478
    :cond_2
    const-string v5, "Mobile Data DisConnected"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static xdmProtoIsWIFIConnected()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1423
    const-string v4, "connectivity"

    invoke-static {v4}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1425
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_0

    .line 1427
    const-string v3, "connectivityManager is null!!"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1447
    :goto_0
    return v2

    .line 1431
    :cond_0
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1433
    .local v1, "wifiInfo":Landroid/net/NetworkInfo;
    if-nez v1, :cond_1

    .line 1435
    const-string v3, "wifiInfo is null!!"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1439
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1441
    const-string v2, "WiFi Connected"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move v2, v3

    .line 1442
    goto :goto_0

    .line 1446
    :cond_2
    const-string v3, "WiFi DisConnected"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xdmResumeOperation()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 700
    const-string v3, ""

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 702
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmProtoIsWIFIConnected()Z

    move-result v0

    .line 704
    .local v0, "bWificonnected":Z
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpGetNetStatus()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 706
    const-string v2, "already connect to network, return"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 721
    :goto_0
    return v1

    .line 709
    :cond_0
    sget-boolean v3, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    .line 711
    const-string v2, "WIFI_ONLY_MODEL or WIFI_ONLY_BLOCK_3G_MODEL. but Wi-Fi Disconnected, return"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 715
    :cond_1
    sget v3, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-ne v3, v2, :cond_2

    .line 717
    invoke-static {v1}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpResumeNotiAction(I)V

    .line 719
    :cond_2
    sput v1, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    move v1, v2

    .line 721
    goto :goto_0
.end method

.method public static xdmSetDataRoamingEnabled(Z)V
    .locals 2
    .param p0, "enabled"    # Z

    .prologue
    .line 1395
    sget-boolean v1, Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z

    if-eqz v1, :cond_0

    .line 1397
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataRoamingEnabled()Z

    move-result v1

    if-eq v1, p0, :cond_0

    .line 1399
    const-string v1, "Data Roaming Off <-> On"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1400
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DATA_ROAMING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1401
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "Enabled"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1402
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1403
    sget-object v1, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1404
    sput-boolean p0, Lcom/fmm/dm/XDMService;->g_bRestoreDataRoaming:Z

    .line 1407
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static xdmSetDataState(I)V
    .locals 0
    .param p0, "state"    # I

    .prologue
    .line 679
    sput p0, Lcom/fmm/dm/XDMService;->mDataState:I

    .line 680
    return-void
.end method

.method public static xdmSetDataState2(I)V
    .locals 0
    .param p0, "state"    # I

    .prologue
    .line 693
    sput p0, Lcom/fmm/dm/XDMService;->mDataState2:I

    .line 694
    return-void
.end method

.method public static xdmSetPinLockState(Z)V
    .locals 0
    .param p0, "state"    # Z

    .prologue
    .line 635
    sput-boolean p0, Lcom/fmm/dm/XDMService;->m_bPinLockState:Z

    .line 636
    return-void
.end method

.method public static xdmSetServiceType(I)I
    .locals 2
    .param p0, "Type"    # I

    .prologue
    .line 596
    const/4 v0, 0x0

    .line 597
    .local v0, "ret":I
    sget v1, Lcom/fmm/dm/XDMService;->nServiceType:I

    if-eq v1, p0, :cond_0

    .line 599
    sput p0, Lcom/fmm/dm/XDMService;->nServiceType:I

    .line 605
    :goto_0
    return v0

    .line 603
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xdmSetServiceType2(I)I
    .locals 2
    .param p0, "Type"    # I

    .prologue
    .line 619
    const/4 v0, 0x0

    .line 620
    .local v0, "ret":I
    sget v1, Lcom/fmm/dm/XDMService;->nServiceType2:I

    if-eq v1, p0, :cond_0

    .line 622
    sput p0, Lcom/fmm/dm/XDMService;->nServiceType2:I

    .line 628
    :goto_0
    return v0

    .line 626
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xdmSetSimState(I)V
    .locals 0
    .param p0, "state"    # I

    .prologue
    .line 651
    sput p0, Lcom/fmm/dm/XDMService;->nSimStatus:I

    .line 652
    return-void
.end method

.method public static xdmSetSimState2(I)V
    .locals 0
    .param p0, "state"    # I

    .prologue
    .line 662
    sput p0, Lcom/fmm/dm/XDMService;->nSimStatus2:I

    .line 663
    return-void
.end method

.method public static xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "szText"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 1521
    sget-object v0, Lcom/fmm/dm/XDMService;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1523
    const/4 v0, 0x0

    invoke-static {p0, v0, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/XDMService;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    .line 1525
    :cond_0
    sget-object v0, Lcom/fmm/dm/XDMService;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 1526
    sget-object v0, Lcom/fmm/dm/XDMService;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1527
    return-void
.end method

.method public static xdmWakeLockAcquire()V
    .locals 4

    .prologue
    .line 1570
    const-string v2, ""

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1573
    :try_start_0
    sget-object v2, Lcom/fmm/dm/XDMService;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v2, :cond_0

    .line 1575
    const-string v2, "m_WakeLock is acquire!!"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1576
    const-string v2, "power"

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 1577
    .local v1, "pm":Landroid/os/PowerManager;
    if-nez v1, :cond_1

    .line 1579
    const-string v2, "PowerManager is null!!"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1592
    :cond_0
    :goto_0
    return-void

    .line 1583
    :cond_1
    const/4 v2, 0x1

    const-string v3, "wakeLock"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    sput-object v2, Lcom/fmm/dm/XDMService;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1584
    sget-object v2, Lcom/fmm/dm/XDMService;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 1585
    sget-object v2, Lcom/fmm/dm/XDMService;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1588
    :catch_0
    move-exception v0

    .line 1590
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmWakeLockRelease()V
    .locals 2

    .prologue
    .line 1596
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1599
    :try_start_0
    sget-object v1, Lcom/fmm/dm/XDMService;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 1601
    const-string v1, "m_WakeLock is release!!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1602
    sget-object v1, Lcom/fmm/dm/XDMService;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1603
    const/4 v1, 0x0

    sput-object v1, Lcom/fmm/dm/XDMService;->m_WakeLock:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1610
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 1606
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 1608
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 158
    invoke-super {p0, p1}, Landroid/app/Service;->attachBaseContext(Landroid/content/Context;)V

    .line 159
    sput-object p1, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    .line 160
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/fmm/dm/XDMService;->m_Binder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 204
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Service| OnCreate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 207
    const-string v1, "Device ID is null!!, service not start !"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 208
    sput-boolean v3, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    .line 587
    :goto_0
    return-void

    .line 211
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 213
    sput-object p0, Lcom/fmm/dm/XDMService;->m_Context:Landroid/content/Context;

    .line 214
    new-instance v1, Lcom/fmm/dm/agent/XDMTask;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMTask;-><init>()V

    sput-object v1, Lcom/fmm/dm/XDMService;->g_Task:Lcom/fmm/dm/agent/XDMTask;

    .line 215
    new-instance v1, Lcom/fmm/dm/agent/XDMUITask;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMUITask;-><init>()V

    sput-object v1, Lcom/fmm/dm/XDMService;->g_UITask:Lcom/fmm/dm/agent/XDMUITask;

    .line 218
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmWakeLockAcquire()V

    .line 219
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 220
    invoke-static {p0}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiNotiInitialize(Landroid/content/Context;)V

    .line 224
    invoke-static {p0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSetContext(Landroid/content/Context;)V

    .line 228
    invoke-static {p0}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetContext(Landroid/content/Context;)V

    .line 284
    const-string v1, "phone"

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    sput-object v1, Lcom/fmm/dm/XDMService;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 286
    sget-object v1, Lcom/fmm/dm/XDMService;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v1, :cond_1

    .line 288
    const-string v1, "TelephonyManager is null!!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmWakeLockRelease()V

    goto :goto_0

    .line 292
    :cond_1
    :try_start_1
    new-instance v1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener;

    invoke-direct {v1}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener;-><init>()V

    sput-object v1, Lcom/fmm/dm/XDMService;->phoneListener:Lcom/fmm/dm/XDMService$XDMPhoneStateListener;

    .line 293
    sget-object v1, Lcom/fmm/dm/XDMService;->telephonyManager:Landroid/telephony/TelephonyManager;

    sget-object v2, Lcom/fmm/dm/XDMService;->phoneListener:Lcom/fmm/dm/XDMService$XDMPhoneStateListener;

    const/16 v3, 0x1f1

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 296
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetServiceType()I

    move-result v1

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetServiceType(I)I

    .line 316
    new-instance v1, Lcom/fmm/dm/XDMService$1;

    invoke-direct {v1, p0}, Lcom/fmm/dm/XDMService$1;-><init>(Lcom/fmm/dm/XDMService;)V

    sput-object v1, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    .line 343
    new-instance v1, Lcom/fmm/dm/XDMService$2;

    invoke-direct {v1, p0}, Lcom/fmm/dm/XDMService$2;-><init>(Lcom/fmm/dm/XDMService;)V

    sput-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    .line 474
    new-instance v1, Lcom/fmm/dm/XDMService$3;

    invoke-direct {v1, p0}, Lcom/fmm/dm/XDMService$3;-><init>(Lcom/fmm/dm/XDMService;)V

    sput-object v1, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    .line 557
    new-instance v1, Lcom/fmm/dm/XDMService$5;

    invoke-direct {v1, p0}, Lcom/fmm/dm/XDMService$5;-><init>(Lcom/fmm/dm/XDMService;)V

    iput-object v1, p0, Lcom/fmm/dm/XDMService;->m_WifiReceiver:Landroid/content/BroadcastReceiver;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmWakeLockRelease()V

    goto :goto_0

    .line 579
    :catch_0
    move-exception v0

    .line 581
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 585
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmWakeLockRelease()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmWakeLockRelease()V

    throw v1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 165
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 168
    :try_start_0
    iget-object v1, p0, Lcom/fmm/dm/XDMService;->m_WifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/fmm/dm/XDMService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 174
    sget-object v1, Lcom/fmm/dm/XDMService;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/fmm/dm/XDMService;->phoneListener:Lcom/fmm/dm/XDMService$XDMPhoneStateListener;

    if-eqz v1, :cond_0

    .line 175
    sget-object v1, Lcom/fmm/dm/XDMService;->telephonyManager:Landroid/telephony/TelephonyManager;

    sget-object v2, Lcom/fmm/dm/XDMService;->phoneListener:Lcom/fmm/dm/XDMService$XDMPhoneStateListener;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 195
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 147
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 148
    iget-object v0, p0, Lcom/fmm/dm/XDMService;->m_WifiReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/fmm/dm/XDMService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 153
    return-void
.end method
