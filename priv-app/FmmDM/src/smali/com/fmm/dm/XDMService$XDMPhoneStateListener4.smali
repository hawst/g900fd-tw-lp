.class public Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;
.super Landroid/telephony/PhoneStateListener;
.source "XDMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/XDMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XDMPhoneStateListener4"
.end annotation


# static fields
.field private static nSlot:I


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "nSlotNum"    # I

    .prologue
    .line 1187
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 1188
    sput p1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    .line 1189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nSlot "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1190
    return-void
.end method


# virtual methods
.method public notifyListeners()V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1277
    sget-object v1, Lcom/fmm/dm/XDMService;->telephonyManagerDual:[Landroid/telephony/TelephonyManager;

    sget v2, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    aget-object v1, v1, v2

    sget-object v2, Lcom/fmm/dm/XDMService;->serviceStateDual:[Landroid/telephony/ServiceState;

    aget-object v2, v2, v7

    sget-object v3, Lcom/fmm/dm/XDMService;->serviceStateDual:[Landroid/telephony/ServiceState;

    aget-object v3, v3, v5

    sget v4, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    invoke-static {v1, v2, v3, v4}, Lcom/fmm/dm/adapter/XDMTelephonyData;->getInstance(Landroid/telephony/TelephonyManager;Landroid/telephony/ServiceState;Landroid/telephony/ServiceState;I)Lcom/fmm/dm/adapter/XDMTelephonyData;

    move-result-object v0

    .line 1279
    .local v0, "telephony":Lcom/fmm/dm/adapter/XDMTelephonyData;
    if-eqz v0, :cond_1

    .line 1281
    sget v1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    if-ne v1, v5, :cond_3

    .line 1283
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetServiceType2(I)I

    .line 1284
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetSimState2(I)V

    .line 1286
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-eqz v1, :cond_0

    .line 1288
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    if-nez v1, :cond_0

    .line 1290
    sput-boolean v5, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    .line 1291
    invoke-static {v8, v6, v6}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 1296
    :cond_0
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-eqz v1, :cond_2

    .line 1298
    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v5}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 1300
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "going to Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1338
    :cond_1
    :goto_0
    return-void

    .line 1302
    :cond_2
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-nez v1, :cond_1

    .line 1304
    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v7}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 1306
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exit from Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0

    .line 1311
    :cond_3
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetServiceType(I)I

    .line 1312
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetSimState(I)V

    .line 1314
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-eqz v1, :cond_4

    .line 1316
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v1, :cond_4

    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_4

    sget-boolean v1, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    if-nez v1, :cond_4

    .line 1318
    sput-boolean v5, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    .line 1319
    invoke-static {v8, v6, v6}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 1324
    :cond_4
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v1

    if-nez v1, :cond_5

    iget-boolean v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-eqz v1, :cond_5

    .line 1326
    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v5}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 1328
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "going to Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0

    .line 1330
    :cond_5
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-nez v1, :cond_1

    .line 1332
    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v7}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 1334
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exit from Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "szIncomingNumber"    # Ljava/lang/String;

    .prologue
    .line 1195
    sget-object v0, Lcom/fmm/dm/XDMService;->telephonyManagerDual:[Landroid/telephony/TelephonyManager;

    sget v1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    .line 1217
    :cond_0
    :goto_0
    return-void

    .line 1198
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ">>>>>>>>>>> onCallStateChanged nSlot : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1200
    if-nez p1, :cond_3

    .line 1202
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_2

    .line 1204
    const-string v0, "Run Resume Operation, Resume call CallStateChanged)"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1205
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    .line 1207
    :cond_2
    const-string v0, ">>>>>>>>>>> CALL_STATE_IDLE"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 1209
    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 1211
    const-string v0, ">>>>>>>>>>> CALL_STATE_RINGING"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 1213
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1215
    const-string v0, ">>>>>>>>>>> CALL_STATE_OFFHOOK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDataActivity(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 1273
    return-void
.end method

.method public onDataConnectionStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 1239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Data Connection State Changed : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1241
    sget v0, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1243
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataState2()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1245
    invoke-static {p1}, Lcom/fmm/dm/XDMService;->xdmSetDataState2(I)V

    .line 1246
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->notifyListeners()V

    .line 1260
    :cond_0
    :goto_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 1262
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_1

    .line 1264
    const-string v0, "Run Resume Operation, Resume call DataConnectionState)"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1265
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    .line 1268
    :cond_1
    return-void

    .line 1253
    :cond_2
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataState()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1255
    invoke-static {p1}, Lcom/fmm/dm/XDMService;->xdmSetDataState(I)V

    .line 1256
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->notifyListeners()V

    goto :goto_0
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 2
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 1229
    sget-object v0, Lcom/fmm/dm/XDMService;->serviceStateDual:[Landroid/telephony/ServiceState;

    sget v1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    aget-object v0, v0, v1

    if-eq v0, p1, :cond_0

    .line 1231
    sget-object v0, Lcom/fmm/dm/XDMService;->serviceStateDual:[Landroid/telephony/ServiceState;

    sget v1, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->nSlot:I

    aput-object p1, v0, v1

    .line 1232
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener4;->notifyListeners()V

    .line 1234
    :cond_0
    return-void
.end method

.method public onSignalStrengthChanged(I)V
    .locals 0
    .param p1, "asu"    # I

    .prologue
    .line 1224
    return-void
.end method
