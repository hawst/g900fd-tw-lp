.class public Lcom/fmm/dm/adapter/XDMTelephonyData;
.super Ljava/lang/Object;
.source "XDMTelephonyData.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XTPInterface;


# instance fields
.field XDMLocationListener:Landroid/location/LocationListener;

.field public cellId:I

.field public homeMobileCountryCode:I

.field public homeMobileNetworkCode:I

.field public isNetworkRoaming:Z

.field public locationAreaCode:I

.field mAccuracy:F

.field mAltitude:D

.field mBearing:F

.field mLatitude:D

.field mLocMgr:Landroid/location/LocationManager;

.field mLongitude:D

.field mSpeed:F

.field mTime:J

.field public m_szCarrierName:Ljava/lang/String;

.field public m_szMsisdn:Ljava/lang/String;

.field public mobileCountryCode:I

.field public mobileNetworkCode:I

.field public nSimState:I

.field public networkType:I

.field public radioType:I

.field public signalStrength:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->cellId:I

    .line 21
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->locationAreaCode:I

    .line 22
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->signalStrength:I

    .line 23
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mobileCountryCode:I

    .line 24
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mobileNetworkCode:I

    .line 25
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->homeMobileCountryCode:I

    .line 26
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->homeMobileNetworkCode:I

    .line 27
    iput v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->radioType:I

    .line 28
    iput-boolean v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    .line 29
    iput v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->m_szMsisdn:Ljava/lang/String;

    .line 32
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    .line 37
    iput-wide v2, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLatitude:D

    .line 38
    iput-wide v2, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLongitude:D

    .line 39
    iput-wide v2, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mAltitude:D

    .line 40
    iput v4, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mBearing:F

    .line 41
    iput v4, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mSpeed:F

    .line 42
    iput v4, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mAccuracy:F

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mTime:J

    .line 49
    new-instance v0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;

    invoke-direct {v0, p0}, Lcom/fmm/dm/adapter/XDMTelephonyData$1;-><init>(Lcom/fmm/dm/adapter/XDMTelephonyData;)V

    iput-object v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->XDMLocationListener:Landroid/location/LocationListener;

    .line 47
    return-void
.end method

.method public static getInstance(Landroid/telephony/TelephonyManager;Landroid/telephony/ServiceState;Landroid/telephony/ServiceState;I)Lcom/fmm/dm/adapter/XDMTelephonyData;
    .locals 8
    .param p0, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p1, "serviceState1"    # Landroid/telephony/ServiceState;
    .param p2, "serviceState2"    # Landroid/telephony/ServiceState;
    .param p3, "nSlot"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 202
    if-nez p0, :cond_0

    .line 204
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "telephonyManager is null, return."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 205
    const/4 v2, 0x0

    .line 253
    :goto_0
    return-object v2

    .line 208
    :cond_0
    new-instance v2, Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-direct {v2}, Lcom/fmm/dm/adapter/XDMTelephonyData;-><init>()V

    .line 211
    .local v2, "wstelephony":Lcom/fmm/dm/adapter/XDMTelephonyData;
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 212
    .local v1, "operator":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    .line 213
    .local v0, "SimState":I
    iput v0, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    .line 215
    if-ne p3, v4, :cond_3

    if-eqz p2, :cond_3

    .line 218
    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->m_szCarrierName:Ljava/lang/String;

    .line 221
    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v1

    .line 222
    invoke-virtual {v2, v1, v5}, Lcom/fmm/dm/adapter/XDMTelephonyData;->xdmSetMobileCodes(Ljava/lang/String;Z)V

    .line 238
    :cond_1
    :goto_1
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v3

    iput v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    .line 240
    iget v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-lt v3, v7, :cond_4

    .line 242
    iput v7, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->radioType:I

    .line 252
    :cond_2
    :goto_2
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v3

    iput-boolean v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    goto :goto_0

    .line 224
    :cond_3
    if-nez p3, :cond_1

    if-eqz p1, :cond_1

    .line 227
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->m_szCarrierName:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v1

    .line 231
    invoke-virtual {v2, v1, v5}, Lcom/fmm/dm/adapter/XDMTelephonyData;->xdmSetMobileCodes(Ljava/lang/String;Z)V

    goto :goto_1

    .line 244
    :cond_4
    iget v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-ne v3, v4, :cond_5

    .line 246
    iput v4, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->radioType:I

    goto :goto_2

    .line 248
    :cond_5
    iget v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-ne v3, v6, :cond_2

    .line 250
    iput v6, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->radioType:I

    goto :goto_2
.end method

.method public static xdmGetInstance(Landroid/telephony/TelephonyManager;Landroid/telephony/ServiceState;)Lcom/fmm/dm/adapter/XDMTelephonyData;
    .locals 7
    .param p0, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 139
    if-nez p0, :cond_0

    .line 141
    const-string v3, "telephonyManager is null, return."

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 142
    const/4 v2, 0x0

    .line 196
    :goto_0
    return-object v2

    .line 145
    :cond_0
    new-instance v2, Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-direct {v2}, Lcom/fmm/dm/adapter/XDMTelephonyData;-><init>()V

    .line 148
    .local v2, "wstelephony":Lcom/fmm/dm/adapter/XDMTelephonyData;
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "szOperator":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    .line 151
    .local v0, "SimState":I
    iput v0, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    .line 153
    if-eqz p1, :cond_1

    .line 156
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->m_szCarrierName:Ljava/lang/String;

    .line 159
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v1

    .line 160
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/fmm/dm/adapter/XDMTelephonyData;->xdmSetMobileCodes(Ljava/lang/String;Z)V

    .line 164
    :cond_1
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v3

    iput v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    .line 166
    iget v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-ne v3, v6, :cond_3

    .line 168
    iput v6, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->radioType:I

    .line 179
    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v3

    iput-boolean v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    .line 180
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->m_szMsisdn:Ljava/lang/String;

    goto :goto_0

    .line 170
    :cond_3
    iget v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-ne v3, v4, :cond_4

    .line 172
    iput v4, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->radioType:I

    goto :goto_1

    .line 174
    :cond_4
    iget v3, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-ne v3, v5, :cond_2

    .line 176
    iput v5, v2, Lcom/fmm/dm/adapter/XDMTelephonyData;->radioType:I

    goto :goto_1
.end method


# virtual methods
.method public xdmInitNetworkProvider()V
    .locals 1

    .prologue
    .line 98
    const-string v0, "location"

    invoke-static {v0}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLocMgr:Landroid/location/LocationManager;

    .line 99
    iget-object v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLocMgr:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 101
    const-string v0, "LocationManager is null!!"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 104
    :cond_0
    return-void
.end method

.method public xdmSetMobileCodes(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "szCodes"    # Ljava/lang/String;
    .param p2, "homeValues"    # Z

    .prologue
    .line 263
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 269
    const/4 v3, 0x0

    const/4 v4, 0x3

    :try_start_0
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 270
    .local v1, "mcc":I
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 271
    .local v2, "mnc":I
    if-eqz p2, :cond_1

    .line 273
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->homeMobileCountryCode:I

    .line 274
    iput v2, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->homeMobileNetworkCode:I

    .line 291
    .end local v1    # "mcc":I
    .end local v2    # "mnc":I
    :cond_0
    :goto_0
    return-void

    .line 278
    .restart local v1    # "mcc":I
    .restart local v2    # "mnc":I
    :cond_1
    iput v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mobileCountryCode:I

    .line 279
    iput v2, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mobileNetworkCode:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 282
    .end local v1    # "mcc":I
    .end local v2    # "mnc":I
    :catch_0
    move-exception v0

    .line 284
    .local v0, "ex":Ljava/lang/IndexOutOfBoundsException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMobileCodes: Invalid operator numeric data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 286
    .end local v0    # "ex":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 288
    .local v0, "ex":Ljava/lang/NumberFormatException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMobileCodes: Operator numeric format error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xdmStartNetworkProvider()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 108
    const-string v0, "Starting Network Provider."

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 111
    iput-wide v2, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLongitude:D

    .line 112
    iput-wide v2, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLatitude:D

    .line 113
    iput-wide v2, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mAltitude:D

    .line 114
    iput v4, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mBearing:F

    .line 115
    iput v4, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mSpeed:F

    .line 116
    iput v4, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mAccuracy:F

    .line 117
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mTime:J

    .line 119
    iget-object v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLocMgr:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x3e8

    iget-object v5, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->XDMLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 120
    return-void
.end method

.method public xdmStopNetwokProvider()V
    .locals 2

    .prologue
    .line 124
    const-string v0, "Stopping Network Provider."

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLocMgr:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData;->XDMLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 127
    return-void
.end method
