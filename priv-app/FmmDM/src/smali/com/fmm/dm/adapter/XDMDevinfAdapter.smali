.class public Lcom/fmm/dm/adapter/XDMDevinfAdapter;
.super Ljava/lang/Object;
.source "XDMDevinfAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;


# static fields
.field private static final DEFAULT_DEVID_VALUE:Ljava/lang/String; = "Default"

.field private static final DEFAULT_MANUFACTURER:Ljava/lang/String; = "SAMSUNG"

.field private static final DEFAULT_MODEL_NAME:Ljava/lang/String; = "GT-I9100"

.field private static final DEFAULT_NULL_DEVID_VALUE:Ljava/lang/String; = "000000000000000"

.field private static final DEFAULT_NULL_DEVID_VALUE2:Ljava/lang/String; = "B0000000"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmDevAdpGetDeviceID()Ljava/lang/String;
    .locals 5

    .prologue
    .line 28
    const-string v1, ""

    .line 29
    .local v1, "szDeviceId":Ljava/lang/String;
    const-string v2, ""

    .line 34
    .local v2, "szTmpDevID":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWDeviceID()Ljava/lang/String;

    move-result-object v2

    .line 35
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 37
    const-string v3, "DeviceID read fail!!!!!!!!"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 39
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmGetEnableLiveDemoFromCSCFeature()Z

    move-result v3

    if-nez v3, :cond_0

    sget-boolean v3, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v3, :cond_3

    .line 41
    :cond_0
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetTWID()Ljava/lang/String;

    move-result-object v1

    .line 48
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "Default"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "000000000000000"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "B0000000"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 49
    :cond_1
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpRetryGetDeviceID()Ljava/lang/String;

    move-result-object v1

    .line 57
    :cond_2
    :goto_1
    return-object v1

    .line 45
    :cond_3
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetDevID()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 53
    :cond_4
    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 54
    .local v0, "nindex":I
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static xdmDevAdpGetFirmwareVersion()Ljava/lang/String;
    .locals 5

    .prologue
    .line 166
    const-string v0, ""

    .line 169
    .local v0, "szFirmwareVer":Ljava/lang/String;
    const-string v1, "%s/%s/%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetFwV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetCscV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetPhoneV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 176
    :goto_0
    return-object v0

    .line 174
    :cond_0
    const-string v0, "I9300XX"

    goto :goto_0
.end method

.method public static xdmDevAdpGetFullDeviceID()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 96
    const-string v1, ""

    .line 98
    .local v1, "szDeviceID":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWDeviceID()Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 101
    const-string v2, "DeviceID read fail!!!!!!!!"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 104
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmGetEnableLiveDemoFromCSCFeature()Z

    move-result v2

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v2, :cond_2

    .line 106
    :cond_0
    const-string v2, "TWID:%s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 128
    :cond_1
    :goto_0
    return-object v1

    .line 110
    :cond_2
    const-string v0, ""

    .line 111
    .local v0, "preId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 113
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetPhoneType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 120
    const-string v0, "IMEI"

    .line 124
    :cond_3
    :goto_1
    const-string v2, "%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 116
    :pswitch_0
    const-string v0, "MEID"

    .line 117
    goto :goto_1

    .line 113
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmDevAdpGetHardwareVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 196
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetHwV()Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "szHardwareVer":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 199
    :goto_0
    return-object v0

    .line 198
    :cond_0
    const-string v0, "REV0.4"

    goto :goto_0
.end method

.method public static xdmDevAdpGetHttpUserAgent()Ljava/lang/String;
    .locals 7

    .prologue
    .line 213
    const-string v2, ""

    .line 214
    .local v2, "szUserAgent":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetManufacturer()Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "szManufacturer":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetModel()Ljava/lang/String;

    move-result-object v1

    .line 217
    .local v1, "szModelName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 222
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 227
    :goto_1
    const-string v3, "%s %s %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v5, 0x2

    const-string v6, "SyncML_DM Client"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 228
    return-object v2

    .line 219
    :cond_0
    const-string v0, "SAMSUNG"

    goto :goto_0

    .line 224
    :cond_1
    const-string v1, "GT-I9100"

    goto :goto_1
.end method

.method public static xdmDevAdpGetIMSIFromSIM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetIMSIFromSIM()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmDevAdpGetIMSIFromSIM1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetIMSIFromSIM1()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmDevAdpGetIMSIFromSIM2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetIMSIFromSIM2()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmDevAdpGetLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 150
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetLanguage()Ljava/lang/String;

    move-result-object v0

    .line 151
    .local v0, "szLanguage":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 153
    :goto_0
    return-object v0

    .line 152
    :cond_0
    const-string v0, "en-us"

    goto :goto_0
.end method

.method public static xdmDevAdpGetManufacturer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetManufacturer()Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "szManufacturer":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    :goto_0
    return-object v0

    .line 135
    :cond_0
    const-string v0, "Samsung"

    goto :goto_0
.end method

.method public static xdmDevAdpGetModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 141
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "szModel":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    :goto_0
    return-object v0

    .line 143
    :cond_0
    const-string v0, "GT-I9070"

    goto :goto_0
.end method

.method public static xdmDevAdpGetOEMName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 158
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetOEM()Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "szOem":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 161
    :goto_0
    return-object v0

    .line 160
    :cond_0
    const-string v0, "Samsung Electronics."

    goto :goto_0
.end method

.method public static xdmDevAdpGetSIMSlot()I
    .locals 1

    .prologue
    .line 233
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDsdsAdapter;->xdmDsdsGetSimSlot()I

    move-result v0

    return v0
.end method

.method public static xdmDevAdpGetSalesCode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 204
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetSalesCode()Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "szSalesCode":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSalesCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 206
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    :goto_0
    return-object v0

    .line 207
    :cond_0
    const-string v0, "XEU"

    goto :goto_0
.end method

.method public static xdmDevAdpGetSoftwareVersion()Ljava/lang/String;
    .locals 5

    .prologue
    .line 181
    const-string v0, ""

    .line 184
    .local v0, "szSoftwareVer":Ljava/lang/String;
    const-string v1, "%s/%s/%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetSwV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetCscV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetPhoneV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 188
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    :goto_0
    return-object v0

    .line 189
    :cond_0
    const-string v0, "I9300XX"

    goto :goto_0
.end method

.method public static xdmDevAdpRetryGetDeviceID()Ljava/lang/String;
    .locals 6

    .prologue
    .line 62
    const-string v2, ""

    .line 64
    .local v2, "szDeviceId":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0xa

    if-ge v1, v3, :cond_3

    .line 68
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_1
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmGetEnableLiveDemoFromCSCFeature()Z

    move-result v3

    if-nez v3, :cond_0

    sget-boolean v3, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v3, :cond_2

    .line 78
    :cond_0
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetTWID()Ljava/lang/String;

    move-result-object v2

    .line 85
    :goto_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "Default"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "000000000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "B0000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 86
    :cond_1
    const-string v2, "000000000000000"

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 82
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetDevID()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 91
    :cond_3
    return-object v2
.end method

.method public static xdmDevAdpVerifyDevID()Z
    .locals 2

    .prologue
    .line 350
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v0

    .line 352
    .local v0, "szDeviceId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "000000000000000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "B0000000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 353
    :cond_0
    const/4 v1, 0x0

    .line 355
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xdmDevAdpVerifySubscId()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 256
    const/4 v1, 0x0

    .line 257
    .local v1, "nRet":Z
    new-instance v0, Lcom/fmm/dm/db/file/XDBSimInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBSimInfo;-><init>()V

    .line 258
    .local v0, "nIMSI":Lcom/fmm/dm/db/file/XDBSimInfo;
    const/4 v2, 0x0

    .line 260
    .local v2, "oIMSI":Lcom/fmm/dm/db/file/XDBSimInfo;
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v3

    if-le v3, v4, :cond_6

    .line 278
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v3

    if-le v3, v4, :cond_1

    .line 279
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetIMSIFromSIM1()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    .line 283
    :goto_0
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetIMSIFromSIM2()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    .line 286
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 288
    const-string v3, "IMSI is NULL!!!!!"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 345
    :cond_0
    :goto_1
    return v1

    .line 281
    :cond_1
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetIMSIFromSIM()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    goto :goto_0

    .line 292
    :cond_2
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSimIMSI()Lcom/fmm/dm/db/file/XDBSimInfo;

    move-result-object v2

    .line 297
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 299
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 301
    const-string v3, "SIM Swap"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 302
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetSimIMSI(Lcom/fmm/dm/db/file/XDBSimInfo;)V

    .line 303
    const/4 v1, 0x1

    .line 324
    :cond_3
    :goto_2
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 325
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetSimIMSI(Lcom/fmm/dm/db/file/XDBSimInfo;)V

    goto :goto_1

    .line 306
    :cond_4
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 308
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 310
    const-string v3, "SIM Swap"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 311
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetSimIMSI(Lcom/fmm/dm/db/file/XDBSimInfo;)V

    .line 312
    const/4 v1, 0x1

    goto :goto_2

    .line 315
    :cond_5
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 317
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 319
    const-string v3, "SIM Swap"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 320
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetSimIMSI(Lcom/fmm/dm/db/file/XDBSimInfo;)V

    .line 321
    const/4 v1, 0x1

    goto :goto_2

    .line 329
    :cond_6
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetIMSIFromSIM()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    .line 330
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 333
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSimIMSI()Lcom/fmm/dm/db/file/XDBSimInfo;

    move-result-object v2

    .line 335
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 337
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    iget-object v4, v2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 339
    const-string v3, "SIM Swap"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 340
    const/4 v1, 0x1

    .line 343
    :cond_7
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetSimIMSI(Lcom/fmm/dm/db/file/XDBSimInfo;)V

    goto/16 :goto_1
.end method

.method public static xdmGetEnableLiveDemoFromCSCFeature()Z
    .locals 3

    .prologue
    .line 362
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Common_EnableLiveDemo"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    .line 363
    .local v0, "bEnable":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enable LiveDemo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 364
    return v0
.end method
