.class Lcom/fmm/dm/adapter/XDMTelephonyData$1;
.super Ljava/lang/Object;
.source "XDMTelephonyData.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/adapter/XDMTelephonyData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;


# direct methods
.method constructor <init>(Lcom/fmm/dm/adapter/XDMTelephonyData;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 54
    .local v0, "szBuf":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLatitude:D

    .line 55
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLongitude:D

    .line 56
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mAltitude:D

    .line 57
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    iput v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mBearing:F

    .line 58
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    iput v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mSpeed:F

    .line 59
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    iput v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mAccuracy:F

    .line 60
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mTime:J

    .line 62
    const-string v1, "location changed: latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 63
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    iget-wide v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLatitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 64
    const-string v1, ", longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 65
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    iget-wide v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mLongitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 66
    const-string v1, ", alt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 67
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    iget-wide v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mAltitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 68
    const-string v1, ", bearing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 69
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    iget v1, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mBearing:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 70
    const-string v1, ", speed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 71
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    iget v1, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mSpeed:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 72
    const-string v1, ", accuracy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    iget v1, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->mAccuracy:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 75
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lcom/fmm/dm/adapter/XDMTelephonyData$1;->this$0:Lcom/fmm/dm/adapter/XDMTelephonyData;

    invoke-virtual {v1}, Lcom/fmm/dm/adapter/XDMTelephonyData;->xdmStopNetwokProvider()V

    .line 78
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "szArg"    # Ljava/lang/String;

    .prologue
    .line 83
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "szProvider"    # Ljava/lang/String;

    .prologue
    .line 88
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "szProvider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 93
    return-void
.end method
