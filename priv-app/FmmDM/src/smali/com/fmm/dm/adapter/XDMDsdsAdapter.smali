.class public Lcom/fmm/dm/adapter/XDMDsdsAdapter;
.super Ljava/lang/Object;
.source "XDMDsdsAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XTPInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmDsdsCheckService()Z
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    return v0
.end method

.method public static xdmDsdsGetSimSlot()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public static xdmDsdsGetTargetDevID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, ""

    return-object v0
.end method

.method public static xdmDsdsGetTargetIMSIFromSIM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, ""

    return-object v0
.end method

.method public static xdmDsdsGetTargetIMSIFromSIM2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, ""

    return-object v0
.end method

.method public static xdmDsdsGetTargetSIMState()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public static xdmDsdsGetTargetSIMState2()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public static xdmDsdsGetTelephonyData1(Landroid/telephony/ServiceState;)Lcom/fmm/dm/adapter/XDMTelephonyData;
    .locals 1
    .param p0, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public static xdmDsdsGetTelephonyData2(Landroid/telephony/ServiceState;)Lcom/fmm/dm/adapter/XDMTelephonyData;
    .locals 1
    .param p0, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public static xdmDsdsIsDualSIM()Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public static xdmDsdsSetListener(Lcom/fmm/dm/XDMService$XDMPhoneStateListener3;)Z
    .locals 1
    .param p0, "phoneListener3"    # Lcom/fmm/dm/XDMService$XDMPhoneStateListener3;

    .prologue
    .line 17
    const/4 v0, 0x0

    return v0
.end method

.method public static xdmDsdsUnsetListener()V
    .locals 0

    .prologue
    .line 23
    return-void
.end method
