.class public Lcom/fmm/dm/adapter/XDMInitAdapter;
.super Ljava/lang/Object;
.source "XDMInitAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XCommonInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Lcom/fmm/dm/interfaces/XUIInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmGetDataStateString(I)Ljava/lang/String;
    .locals 1
    .param p0, "nstate"    # I

    .prologue
    .line 227
    packed-switch p0, :pswitch_data_0

    .line 238
    const-string v0, "Network unknown"

    :goto_0
    return-object v0

    .line 230
    :pswitch_0
    const-string v0, "Network Status is DATA CONNECTED!"

    goto :goto_0

    .line 232
    :pswitch_1
    const-string v0, "Network Status is DATA CONNECTING!"

    goto :goto_0

    .line 234
    :pswitch_2
    const-string v0, "Network Status is DATA DISCONNECTED!"

    goto :goto_0

    .line 236
    :pswitch_3
    const-string v0, "Network Status is DATA SUSPENDED!"

    goto :goto_0

    .line 227
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static xdmInitAdpCheckNetworkReady()I
    .locals 2

    .prologue
    .line 154
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "XDM_NETWORK_STATE_SYNCML_USE"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 157
    const/4 v0, 0x2

    .line 165
    :goto_0
    return v0

    .line 160
    :cond_0
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpGetNetStatus()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 162
    const/4 v0, 0x1

    goto :goto_0

    .line 165
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmInitAdpEXTInit()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 170
    invoke-static {v4}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentTaskSetDmInitStatus(Z)V

    .line 173
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetServerURLFromDSMProvider()Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "szUrl":Ljava/lang/String;
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbChangeLawmoServerInfo(Ljava/lang/String;)V

    .line 178
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetDbLawmoOperation()I

    move-result v2

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 179
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetDBOperation()I

    move-result v2

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 182
    sget-boolean v2, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    if-eqz v2, :cond_1

    .line 184
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DmAgentType ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 185
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpGetSIMLockState()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v2

    if-nez v2, :cond_1

    .line 187
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiSimChangeAlertReport()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v4

    .line 194
    :cond_1
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetDBOperation()I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 196
    const-string v2, "Resume Get Location"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 197
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpOerationGetLocation()Z

    goto :goto_0

    .line 204
    :cond_2
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v0

    .line 205
    .local v0, "nOperation":I
    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    .line 207
    const-string v2, "Report Wipe Operation Result"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 208
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpResumeWipeResult()V

    goto :goto_0

    .line 211
    :cond_3
    const/16 v2, 0xb

    if-ne v0, v2, :cond_4

    .line 213
    const-string v2, "Report Forwarding Operation Result"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 214
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpResumeForwardingResult()V

    goto :goto_0

    .line 219
    :cond_4
    invoke-static {}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentGetNotiResumeCase()I

    move-result v2

    if-nez v2, :cond_0

    .line 220
    const/4 v2, 0x0

    const/16 v3, 0x92

    invoke-static {v2, v3}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public static xdmInitAdpGetNetStatus()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 29
    const/4 v0, 0x0

    .line 32
    .local v0, "nret":I
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmProtoIsWIFIConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 37
    :cond_1
    sget-boolean v3, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-nez v3, :cond_3

    .line 39
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    .line 42
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataState()I

    move-result v1

    .line 43
    .local v1, "state":I
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataState2()I

    move-result v2

    .line 44
    .local v2, "state2":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM1 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmGetDataStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 45
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM2 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmGetDataStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 46
    if-eq v1, v5, :cond_0

    if-eq v2, v5, :cond_0

    .line 48
    const/4 v0, -0x1

    goto :goto_0

    .line 53
    .end local v1    # "state":I
    .end local v2    # "state2":I
    :cond_2
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataState()I

    move-result v1

    .line 54
    .restart local v1    # "state":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmGetDataStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 55
    if-eq v1, v5, :cond_0

    .line 57
    const/4 v0, -0x1

    goto :goto_0

    .line 63
    .end local v1    # "state":I
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xdmInitAdpGetSIMLockState()Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x5

    .line 70
    const/4 v2, 0x0

    .line 72
    .local v2, "bret":Z
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v3

    if-le v3, v6, :cond_7

    .line 75
    const/4 v0, 0x0

    .line 76
    .local v0, "SIMState":I
    const/4 v1, 0x0

    .line 93
    .local v1, "SIMState2":I
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v3

    if-le v3, v6, :cond_6

    .line 94
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetSIMState1()I

    move-result v0

    .line 98
    :goto_0
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetSIMState2()I

    move-result v1

    .line 101
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIMState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 102
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xdmGetSimState() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 103
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIMState2 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 104
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xdmGetSimState2() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState2()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 106
    if-eq v0, v5, :cond_0

    if-ne v1, v5, :cond_1

    .line 108
    :cond_0
    const/4 v2, 0x1

    .line 111
    :cond_1
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState()I

    move-result v3

    if-eq v3, v5, :cond_5

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState2()I

    move-result v3

    if-eq v3, v5, :cond_5

    .line 114
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState()I

    move-result v3

    if-eq v3, v7, :cond_2

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState()I

    move-result v3

    if-ne v3, v8, :cond_4

    :cond_2
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState2()I

    move-result v3

    if-eq v3, v7, :cond_3

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState2()I

    move-result v3

    if-ne v3, v8, :cond_4

    .line 117
    :cond_3
    invoke-static {v6}, Lcom/fmm/dm/XDMService;->xdmSetPinLockState(Z)V

    .line 120
    :cond_4
    const-string v3, "SIM Status is not WORKING"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 121
    const/4 v2, 0x0

    .line 148
    .end local v1    # "SIMState2":I
    :cond_5
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bret : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 149
    return v2

    .line 96
    .restart local v1    # "SIMState2":I
    :cond_6
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetSIMState()I

    move-result v0

    goto/16 :goto_0

    .line 126
    .end local v0    # "SIMState":I
    .end local v1    # "SIMState2":I
    :cond_7
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetSIMState()I

    move-result v0

    .line 128
    .restart local v0    # "SIMState":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIMState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xdmGetSimState() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 131
    if-ne v0, v5, :cond_8

    .line 133
    const/4 v2, 0x1

    .line 136
    :cond_8
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState()I

    move-result v3

    if-eq v3, v5, :cond_5

    .line 138
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState()I

    move-result v3

    if-eq v3, v7, :cond_9

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetSimState()I

    move-result v3

    if-ne v3, v8, :cond_a

    .line 140
    :cond_9
    invoke-static {v6}, Lcom/fmm/dm/XDMService;->xdmSetPinLockState(Z)V

    .line 143
    :cond_a
    const-string v3, "SIM Status is not WORKING"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 144
    const/4 v2, 0x0

    goto :goto_1
.end method
