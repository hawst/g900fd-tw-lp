.class public Lcom/fmm/dm/adapter/XDMTargetAdapter;
.super Ljava/lang/Object;
.source "XDMTargetAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;


# static fields
.field private static final FILE_PATH_CSC_VERSION:Ljava/lang/String; = "system/CSCVersion.txt"

.field private static final PROPERTY_SIM_OPERATOR_ALPHA:Ljava/lang/String; = "gsm.sim.operator.alpha"

.field private static final PROP_ACTIVATION_LOCK:Ljava/lang/String; = "persist.sys.setupwizard"

.field private static final PROP_BUILD_CSC_VERSION:Ljava/lang/String; = "ril.official_cscver"

.field private static final PROP_BUILD_HW_VERSION:Ljava/lang/String; = "ro.build.HW"

.field private static final PROP_BUILD_PDA_VERSION:Ljava/lang/String; = "ro.build.PDA"

.field private static final PROP_BUILD_PHONE_VERSION:Ljava/lang/String; = "ril.sw_ver"

.field private static final PROP_CSC_SALES_CODE:Ljava/lang/String; = "ro.csc.sales_code"

.field private static final PROP_CSC_SALES_CODE2:Ljava/lang/String; = "ril.sales_code"

.field private static final PROP_PRODUCT_MANUFACTURER:Ljava/lang/String; = "ro.product.manufacturer"

.field private static final PROP_PRODUCT_MODEL:Ljava/lang/String; = "ro.product.model"

.field private static final PROP_PRODUCT_OEM:Ljava/lang/String; = "ro.product.OEM"

.field public static final PROP_SIM_STATE:Ljava/lang/String; = "gsm.sim.state"

.field public static final PROP_SIM_STATE2:Ljava/lang/String; = "gsm.sim.state_1"

.field private static final PROP_TWID:Ljava/lang/String; = "ro.serialno"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmCheckActivationLock()Z
    .locals 4

    .prologue
    .line 208
    const/4 v0, 0x1

    .line 209
    .local v0, "bActivationLock":Z
    const-string v2, "persist.sys.setupwizard"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "szActivationLock":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "FINISH"

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 212
    const/4 v0, 0x0

    .line 214
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bActivationLock : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 215
    return v0
.end method

.method private static xdmGetCharToHex(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 490
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 491
    add-int/lit8 v0, p0, -0x30

    int-to-char v0, v0

    .line 497
    :goto_0
    return v0

    .line 492
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 493
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    int-to-char v0, v0

    goto :goto_0

    .line 494
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 495
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    int-to-char v0, v0

    goto :goto_0

    .line 497
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmGetCheckWifiOnlyModel()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 549
    const/4 v0, 0x0

    .line 552
    .local v0, "bRet":Z
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 553
    .local v1, "cm":Landroid/net/ConnectivityManager;
    if-nez v1, :cond_0

    .line 555
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/16 v5, 0xa

    if-ge v3, v5, :cond_0

    .line 559
    const-wide/16 v6, 0x3e8

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 566
    :goto_1
    :try_start_2
    const-string v5, "connectivity is null, retry..."

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 567
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    check-cast v1, Landroid/net/ConnectivityManager;

    .line 568
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_2

    .line 573
    .end local v3    # "i":I
    :cond_0
    if-eqz v1, :cond_1

    .line 575
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 576
    const/4 v0, 0x1

    .line 581
    :cond_1
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isWifiOnly : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 587
    .end local v0    # "bRet":Z
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    :goto_3
    return v0

    .line 561
    .restart local v0    # "bRet":Z
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "i":I
    :catch_0
    move-exception v2

    .line 563
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 584
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v3    # "i":I
    :catch_1
    move-exception v2

    .line 586
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v4

    .line 587
    goto :goto_3

    .line 555
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "i":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 578
    .end local v3    # "i":I
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static xdmGetNetPinFormIMSI()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 436
    const-string v7, ""

    .line 437
    .local v7, "szIMSI":Ljava/lang/String;
    const-string v9, ""

    .line 438
    .local v9, "szPadIMSI":Ljava/lang/String;
    const/4 v3, 0x0

    .line 439
    .local v3, "nPadIMSILen":I
    const/4 v2, 0x0

    .line 441
    .local v2, "i":I
    const/4 v6, 0x0

    .line 442
    .local v6, "pNetPin":[C
    const/4 v8, 0x0

    .line 444
    .local v8, "szNetPin":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetIMSIFromSIM()Ljava/lang/String;

    move-result-object v7

    .line 445
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 485
    :cond_0
    :goto_0
    return-object v10

    .line 451
    :cond_1
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v3

    .line 452
    rem-int/lit8 v11, v3, 0x2

    if-eqz v11, :cond_2

    .line 454
    add-int/lit8 v3, v3, 0x1

    .line 455
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "9"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 463
    :goto_1
    div-int/lit8 v11, v3, 0x2

    new-array v6, v11, [C

    .line 464
    if-eqz v6, :cond_0

    .line 470
    const/4 v2, 0x0

    :goto_2
    div-int/lit8 v10, v3, 0x2

    if-ge v2, v10, :cond_3

    .line 472
    mul-int/lit8 v10, v2, 0x2

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 473
    .local v4, "nTemp1":C
    mul-int/lit8 v10, v2, 0x2

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 475
    .local v5, "nTemp2":C
    invoke-static {v4}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetCharToHex(C)C

    move-result v10

    shl-int/lit8 v10, v10, 0x4

    int-to-char v0, v10

    .line 476
    .local v0, "a":C
    invoke-static {v5}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetCharToHex(C)C

    move-result v1

    .line 477
    .local v1, "b":C
    or-int v10, v0, v1

    int-to-char v10, v10

    aput-char v10, v6, v2

    .line 480
    const/4 v4, 0x0

    .line 481
    const/4 v5, 0x0

    .line 470
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 459
    .end local v0    # "a":C
    .end local v1    # "b":C
    .end local v4    # "nTemp1":C
    .end local v5    # "nTemp2":C
    :cond_2
    add-int/lit8 v3, v3, 0x2

    .line 460
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "1"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "F"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 484
    :cond_3
    invoke-static {v6}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    move-object v10, v8

    .line 485
    goto :goto_0
.end method

.method public static xdmGetPhoneType()I
    .locals 4

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 89
    .local v0, "nRet":I
    const-string v2, "phone"

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 90
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 92
    const-string v2, "tm == null"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 93
    const/4 v0, 0x0

    .line 116
    .end local v0    # "nRet":I
    :goto_0
    return v0

    .line 96
    .restart local v0    # "nRet":I
    :cond_0
    const-string v2, "KOR"

    const-string v3, "KOR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 98
    const-string v2, "SKT"

    const-string v3, "LG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 100
    const/4 v0, 0x2

    goto :goto_0

    .line 102
    :cond_1
    const-string v2, "SKT"

    const-string v3, "LTELG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 104
    const/4 v0, 0x1

    goto :goto_0

    .line 108
    :cond_2
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    goto :goto_0

    .line 113
    :cond_3
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    goto :goto_0
.end method

.method public static xdmGetPropTargetSIMState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    const-string v0, "gsm.sim.state"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetPropTargetSIMState2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    const-string v0, "gsm.sim.state_1"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetSystemCscFile()Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 502
    const/4 v2, 0x0

    .line 503
    .local v2, "f":Ljava/io/RandomAccessFile;
    new-instance v4, Ljava/io/File;

    const-string v8, "system/CSCVersion.txt"

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 504
    .local v4, "file":Ljava/io/File;
    const/4 v0, 0x0

    .line 505
    .local v0, "buffer":[B
    const-string v6, ""

    .line 509
    .local v6, "szRet":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v8, "r"

    invoke-direct {v3, v4, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 511
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .local v3, "f":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    cmp-long v8, v8, v10

    if-lez v8, :cond_0

    .line 513
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    long-to-int v8, v8

    add-int/lit8 v8, v8, -0x1

    new-array v0, v8, [B

    .line 514
    const-wide/16 v8, 0x0

    invoke-virtual {v3, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 515
    invoke-virtual {v3, v0}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v5

    .line 516
    .local v5, "result":I
    if-lez v5, :cond_3

    .line 518
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v6    # "szRet":Ljava/lang/String;
    .local v7, "szRet":Ljava/lang/String;
    move-object v6, v7

    .line 535
    .end local v5    # "result":I
    .end local v7    # "szRet":Ljava/lang/String;
    .restart local v6    # "szRet":Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 536
    :try_start_2
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    move-object v2, v3

    .line 544
    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    :cond_2
    :goto_1
    return-object v6

    .line 522
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v5    # "result":I
    :cond_3
    :try_start_3
    const-string v8, "read failed"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 527
    .end local v5    # "result":I
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 529
    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 535
    if-eqz v2, :cond_2

    .line 536
    :try_start_5
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 538
    :catch_1
    move-exception v1

    .line 540
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 538
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    :catch_2
    move-exception v1

    .line 540
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v2, v3

    .line 542
    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    goto :goto_1

    .line 533
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 535
    :goto_3
    if-eqz v2, :cond_4

    .line 536
    :try_start_6
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 541
    :cond_4
    :goto_4
    throw v8

    .line 538
    :catch_3
    move-exception v1

    .line 540
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 533
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 527
    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method public static xdmGetTargetCscV()Ljava/lang/String;
    .locals 3

    .prologue
    .line 185
    const-string v1, "ril.official_cscver"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "szCsc":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read csc: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 187
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetSystemCscFile()Ljava/lang/String;

    move-result-object v0

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read file csc: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 192
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    const-string v0, ""

    .line 195
    const-string v1, "csc file is Unknown"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 198
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetDevID()Ljava/lang/String;
    .locals 4

    .prologue
    .line 50
    const-string v0, ""

    .line 64
    .local v0, "szDeviceId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 65
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 66
    invoke-static {}, Landroid/telephony/TelephonyManager;->getFirst()Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 70
    :goto_0
    if-eqz v1, :cond_1

    .line 72
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 74
    :cond_0
    const-string v0, ""

    .line 78
    :cond_1
    return-object v0

    .line 68
    :cond_2
    const-string v2, "phone"

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "tm":Landroid/telephony/TelephonyManager;
    check-cast v1, Landroid/telephony/TelephonyManager;

    .restart local v1    # "tm":Landroid/telephony/TelephonyManager;
    goto :goto_0
.end method

.method public static xdmGetTargetFwV()Ljava/lang/String;
    .locals 3

    .prologue
    .line 174
    const-string v1, "ro.build.PDA"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "szFwV":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read FwV: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 176
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    const-string v0, ""

    .line 180
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetHwV()Ljava/lang/String;
    .locals 3

    .prologue
    .line 163
    const-string v1, "ro.build.HW"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "szHwV":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read FwV: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 165
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const-string v0, ""

    .line 169
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetIMSIFromSIM()Ljava/lang/String;
    .locals 3

    .prologue
    .line 342
    const-string v0, ""

    .line 356
    .local v0, "szBuff":Ljava/lang/String;
    const-string v2, "phone"

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 358
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_1

    .line 359
    const/4 v0, 0x0

    .line 369
    .end local v0    # "szBuff":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 361
    .restart local v0    # "szBuff":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 365
    const-string v0, ""

    goto :goto_0
.end method

.method public static xdmGetTargetIMSIFromSIM1()Ljava/lang/String;
    .locals 3

    .prologue
    .line 375
    const-string v0, ""

    .line 376
    .local v0, "szBuff":Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getFirst()Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 378
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_1

    .line 379
    const/4 v0, 0x0

    .line 388
    .end local v0    # "szBuff":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 381
    .restart local v0    # "szBuff":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    .line 383
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    const-string v0, ""

    goto :goto_0
.end method

.method public static xdmGetTargetIMSIFromSIM2()Ljava/lang/String;
    .locals 3

    .prologue
    .line 393
    const-string v0, ""

    .line 407
    .local v0, "szBuff":Ljava/lang/String;
    const-string v2, "phone2"

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 409
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_1

    .line 410
    const/4 v0, 0x0

    .line 420
    .end local v0    # "szBuff":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 412
    .restart local v0    # "szBuff":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    .line 414
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 416
    const-string v0, ""

    goto :goto_0
.end method

.method public static xdmGetTargetLanguage()Ljava/lang/String;
    .locals 7

    .prologue
    .line 136
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 137
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget-object v4, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "szLanguage":Ljava/lang/String;
    iget-object v4, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "szCountry":Ljava/lang/String;
    const-string v4, "%s-%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 140
    .local v3, "szRet":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "language : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 142
    return-object v3
.end method

.method public static xdmGetTargetMSISDN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 427
    const-string v1, "phone"

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 428
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    .line 429
    const/4 v1, 0x0

    .line 431
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static xdmGetTargetManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const-string v0, "ro.product.manufacturer"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetModel()Ljava/lang/String;
    .locals 3

    .prologue
    .line 126
    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "szModel":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    const/4 v1, 0x0

    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 131
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetOEM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const-string v0, "ro.product.OEM"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetOperatorAlphaFromSIM()Ljava/lang/String;
    .locals 3

    .prologue
    .line 327
    const-string v0, ""

    .line 329
    .local v0, "szBuff":Ljava/lang/String;
    const-string v1, "gsm.sim.operator.alpha"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pszBuff: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 332
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 334
    const/4 v0, 0x0

    .line 337
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetPhoneV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const-string v0, "ril.sw_ver"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetSIMOperator()Ljava/lang/String;
    .locals 4

    .prologue
    .line 307
    const-string v0, ""

    .line 308
    .local v0, "szBuff":Ljava/lang/String;
    const-string v2, "phone"

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 309
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 311
    const-string v2, "TelephonyManager is null!!"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 312
    const/4 v0, 0x0

    .line 322
    .end local v0    # "szBuff":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 315
    .restart local v0    # "szBuff":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 316
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 318
    const-string v0, ""

    .line 321
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operator = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetTargetSIMState()I
    .locals 3

    .prologue
    .line 220
    const/4 v0, 0x0

    .line 235
    .local v0, "nSimState":I
    const-string v2, "phone"

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 236
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 242
    :goto_0
    return v0

    .line 239
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    goto :goto_0
.end method

.method public static xdmGetTargetSIMState1()I
    .locals 2

    .prologue
    .line 248
    invoke-static {}, Landroid/telephony/TelephonyManager;->getFirst()Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 249
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    .line 250
    const/4 v1, 0x0

    .line 252
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    goto :goto_0
.end method

.method public static xdmGetTargetSIMState2()I
    .locals 3

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 272
    .local v0, "nSimState":I
    const-string v2, "phone2"

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 273
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 274
    const/4 v0, 0x0

    .line 279
    :goto_0
    return v0

    .line 276
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    goto :goto_0
.end method

.method public static xdmGetTargetSalesCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 298
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 299
    .local v0, "szSalesCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    const-string v1, "ril.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 302
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetSwV()Ljava/lang/String;
    .locals 3

    .prologue
    .line 152
    const-string v1, "ro.build.PDA"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "szSwV":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read SwV: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 154
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    const-string v0, ""

    .line 158
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetTWID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    const-string v0, "ro.serialno"

    const-string v1, "Default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
