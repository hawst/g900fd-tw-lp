.class public Lcom/fmm/dm/adapter/XDMFeature;
.super Ljava/lang/Object;
.source "XDMFeature.java"


# static fields
.field public static XDM_FEATURE_COMMON_DSDS_SUPPORT:Z

.field public static XDM_FEATURE_SIM_CHANGE:Z

.field public static XDM_FEATURE_WIFI_ONLY_MODEL:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    const/4 v0, 0x1

    sput-boolean v0, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    .line 9
    sput-boolean v1, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    .line 10
    sput-boolean v1, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_COMMON_DSDS_SUPPORT:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmInitialize()V
    .locals 2

    .prologue
    .line 14
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetCheckWifiOnlyModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x1

    sput-boolean v0, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    .line 17
    const/4 v0, 0x0

    sput-boolean v0, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XDM_FEATURE_WIFI_ONLY_MODEL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XDM_FEATURE_COMMON_DSDS_SUPPORT : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_COMMON_DSDS_SUPPORT:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XDM_FEATURE_SIM_CHANGE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 30
    return-void
.end method
