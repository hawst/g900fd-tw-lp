.class public Lcom/fmm/dm/XDMApplication;
.super Landroid/app/Application;
.source "XDMApplication.java"


# static fields
.field public static g_ServiceStartSte:Z

.field private static m_Context:Landroid/content/Context;

.field private static m_szCurrentReleaseVer:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, ""

    sput-object v0, Lcom/fmm/dm/XDMApplication;->m_szCurrentReleaseVer:Ljava/lang/String;

    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 5

    .prologue
    .line 58
    const/4 v2, 0x0

    .line 59
    .local v2, "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    const/4 v0, 0x0

    .line 62
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v3, Lcom/fmm/dm/db/sql/XDMDbHelper;

    sget-object v4, Lcom/fmm/dm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/fmm/dm/db/sql/XDMDbHelper;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .end local v2    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    .local v3, "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    :try_start_1
    invoke-virtual {v3}, Lcom/fmm/dm/db/sql/XDMDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v2, v3

    .line 70
    .end local v3    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    .restart local v2    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    :goto_0
    return-object v0

    .line 65
    :catch_0
    move-exception v1

    .line 67
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 65
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    .restart local v3    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    .restart local v2    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    goto :goto_1
.end method

.method public static xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 5

    .prologue
    .line 75
    const/4 v2, 0x0

    .line 76
    .local v2, "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    const/4 v0, 0x0

    .line 79
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v3, Lcom/fmm/dm/db/sql/XDMDbHelper;

    sget-object v4, Lcom/fmm/dm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/fmm/dm/db/sql/XDMDbHelper;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    .end local v2    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    .local v3, "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    :try_start_1
    invoke-virtual {v3}, Lcom/fmm/dm/db/sql/XDMDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v2, v3

    .line 87
    .end local v3    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    .restart local v2    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    :goto_0
    return-object v0

    .line 82
    :catch_0
    move-exception v1

    .line 84
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 82
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    .restart local v3    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    .restart local v2    # "mDatabaseHelper":Lcom/fmm/dm/db/sql/XDMDbHelper;
    goto :goto_1
.end method

.method public static xdmGetContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/fmm/dm/XDMApplication;->m_Context:Landroid/content/Context;

    return-object v0
.end method

.method public static xdmGetReleaseVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/fmm/dm/XDMApplication;->m_szCurrentReleaseVer:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmSetReleaseVer()V

    .line 115
    :cond_0
    sget-object v0, Lcom/fmm/dm/XDMApplication;->m_szCurrentReleaseVer:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmSetReleaseVer()V
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/fmm/dm/XDMApplication;->m_Context:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 93
    sget-object v0, Lcom/fmm/dm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/dm/XDMApplication;->xdmSetReleaseVer(Landroid/content/Context;)V

    .line 94
    :cond_0
    return-void
.end method

.method public static xdmSetReleaseVer(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 101
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v2, Lcom/fmm/dm/XDMApplication;->m_szCurrentReleaseVer:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 48
    sput-object p1, Lcom/fmm/dm/XDMApplication;->m_Context:Landroid/content/Context;

    .line 49
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 26
    sput-object p0, Lcom/fmm/dm/XDMApplication;->m_Context:Landroid/content/Context;

    .line 27
    sget-object v0, Lcom/fmm/dm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/dm/XDMApplication;->xdmSetReleaseVer(Landroid/content/Context;)V

    .line 28
    invoke-static {}, Lcom/fmm/dm/adapter/XDMFeature;->xdmInitialize()V

    .line 30
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWDeviceID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    const-string v0, "Device ID exist!!"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 33
    const-string v0, "FMMApplication Start !"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 34
    const/4 v0, 0x1

    sput-boolean v0, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    .line 35
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/fmm/dm/XDMService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/fmm/dm/XDMApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 42
    :goto_0
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    .line 40
    const-string v0, "FMMApplication NOT Start !"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method
