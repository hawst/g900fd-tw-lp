.class final Lcom/fmm/dm/noti/XNOTIAdapter$xnotiBackgroundQueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "XNOTIAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/noti/XNOTIAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "xnotiBackgroundQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/noti/XNOTIAdapter;


# direct methods
.method public constructor <init>(Lcom/fmm/dm/noti/XNOTIAdapter;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/fmm/dm/noti/XNOTIAdapter$xnotiBackgroundQueryHandler;->this$0:Lcom/fmm/dm/noti/XNOTIAdapter;

    .line 216
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 217
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 222
    packed-switch p1, :pswitch_data_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 225
    :pswitch_0
    if-eqz p3, :cond_2

    .line 227
    const-string v6, "body"

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 228
    .local v0, "bodyColumn":I
    const-string v6, "date"

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 230
    .local v1, "dateColumn":I
    const/4 v3, 0x0

    .line 231
    .local v3, "ret":Z
    const/4 v2, 0x0

    .line 232
    .local v2, "msgBody":[B
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 233
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 234
    .local v5, "szMessageBody":Ljava/lang/String;
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 235
    .local v4, "szDate":Ljava/lang/String;
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibHexStringToBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 237
    if-eqz v2, :cond_1

    .line 239
    iget-object v6, p0, Lcom/fmm/dm/noti/XNOTIAdapter$xnotiBackgroundQueryHandler;->this$0:Lcom/fmm/dm/noti/XNOTIAdapter;

    array-length v7, v2

    invoke-virtual {v6, v2, v7, v4}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpReceiveMsg([BILjava/lang/String;)Z

    move-result v3

    .line 240
    if-nez v3, :cond_0

    .line 242
    const-string v6, "xnotiPushAdpReceiveMsg false"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 247
    :cond_1
    const-string v6, "messageBody is null"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 252
    .end local v0    # "bodyColumn":I
    .end local v1    # "dateColumn":I
    .end local v2    # "msgBody":[B
    .end local v3    # "ret":Z
    .end local v4    # "szDate":Ljava/lang/String;
    .end local v5    # "szMessageBody":Ljava/lang/String;
    :cond_2
    const-string v6, "onquerycomplete..cursor == null"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 222
    :pswitch_data_0
    .packed-switch 0x2537
        :pswitch_0
    .end packed-switch
.end method
