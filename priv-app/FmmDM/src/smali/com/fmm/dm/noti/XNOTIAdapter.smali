.class public Lcom/fmm/dm/noti/XNOTIAdapter;
.super Lcom/fmm/dm/agent/XDMAgent;
.source "XNOTIAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XNOTIInterface;
.implements Lcom/fmm/dm/interfaces/XUIInterface;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/noti/XNOTIAdapter$xnotiBackgroundQueryHandler;
    }
.end annotation


# static fields
.field private static final MESSAGE_LIST_QUERY_TOKEN:I = 0x2537

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final XNOTI_RETRY_COUNT_MAX:I = 0x5

.field private static m_BootStrapUri:Landroid/net/Uri; = null

.field private static m_NotiProcessing:Z = false

.field private static m_PtWapPush:Lcom/fmm/dm/noti/XNOTIWapPush; = null

.field private static m_PushDataQueue:Ljava/util/LinkedList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/fmm/dm/noti/XNOTIData;",
            ">;"
        }
    .end annotation
.end field

.field private static m_nAuthCount:I = 0x0

.field private static m_szPushDate:Ljava/lang/String; = null

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 31
    sput-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PtWapPush:Lcom/fmm/dm/noti/XNOTIWapPush;

    .line 32
    sput-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_szPushDate:Ljava/lang/String;

    .line 33
    sput-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_BootStrapUri:Landroid/net/Uri;

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    .line 35
    sput-boolean v2, Lcom/fmm/dm/noti/XNOTIAdapter;->m_NotiProcessing:Z

    .line 39
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "address"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "body"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "read"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "status"

    aput-object v2, v0, v1

    sput-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 48
    return-void
.end method

.method public static xnotiAddPushDataQueue(I[B)V
    .locals 4
    .param p0, "type"    # I
    .param p1, "pushData"    # [B

    .prologue
    const/4 v3, 0x0

    .line 292
    if-nez p1, :cond_0

    .line 294
    const-string v2, "gPushData  Uri is null"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 308
    :goto_0
    return-void

    .line 298
    :cond_0
    array-length v2, p1

    new-array v0, v2, [B

    .line 299
    .local v0, "pushDataCopy":[B
    array-length v2, p1

    invoke-static {p1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 301
    new-instance v1, Lcom/fmm/dm/noti/XNOTIData;

    invoke-direct {v1, p0, v0}, Lcom/fmm/dm/noti/XNOTIData;-><init>(I[B)V

    .line 303
    .local v1, "wssPushData":Lcom/fmm/dm/noti/XNOTIData;
    sget-object v3, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    monitor-enter v3

    .line 305
    :try_start_0
    sget-object v2, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 306
    const-string v2, "mPushDataQueue add"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 307
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static xnotiGetBootstrapUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_BootStrapUri:Landroid/net/Uri;

    return-object v0
.end method

.method public static xnotiGetCurAuthCnt()I
    .locals 1

    .prologue
    .line 612
    sget v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_nAuthCount:I

    return v0
.end method

.method public static xnotiGetNotiProcessing()Z
    .locals 1

    .prologue
    .line 281
    sget-boolean v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_NotiProcessing:Z

    return v0
.end method

.method public static xnotiIPPushDataReceive([B)V
    .locals 2
    .param p0, "pushData"    # [B

    .prologue
    .line 361
    if-nez p0, :cond_0

    .line 363
    const-string v1, "pushData is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 371
    :goto_0
    return-void

    .line 367
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiSetNotiProcessing(Z)V

    .line 368
    new-instance v0, Lcom/fmm/dm/noti/XNOTIAdapter;

    invoke-direct {v0}, Lcom/fmm/dm/noti/XNOTIAdapter;-><init>()V

    .line 369
    .local v0, "pushAdp":Lcom/fmm/dm/noti/XNOTIAdapter;
    array-length v1, p0

    invoke-virtual {v0, p0, v1}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiIpPushAdpReceiveMsg([BI)Z

    goto :goto_0
.end method

.method public static xnotiPushAdpClearSessionStatus(I)V
    .locals 2
    .param p0, "nAppId"    # I

    .prologue
    .line 379
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 383
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpResetSessionSaveState(I)V

    .line 384
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 385
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbSetBackUpServerUrl()V

    .line 386
    const/4 v1, 0x0

    sput v1, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :goto_0
    return-void

    .line 388
    :catch_0
    move-exception v0

    .line 390
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xnotiPushAdpExcuteResumeNoti(I)I
    .locals 7
    .param p0, "nAppId"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 475
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNofiInfo()Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    move-result-object v1

    .line 476
    .local v1, "pSessionSaveInfo":Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    if-nez v1, :cond_0

    .line 478
    const-string v2, "Get Noti Info File Read Error"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 479
    const/4 v2, -0x1

    .line 533
    :goto_0
    return v2

    .line 482
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nSessionSaveState:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 483
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nNotiUiEvent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 484
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nNotiRetryCount:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 486
    iget v3, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    iget v3, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    if-ne v3, v5, :cond_6

    .line 488
    :cond_1
    iget v3, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    const/4 v4, 0x5

    if-lt v3, v4, :cond_2

    .line 490
    const-string v3, "Noti Retry Count MAX. All Clear"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 491
    invoke-static {v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus(I)V

    .line 492
    const/16 v3, 0x91

    invoke-static {v6, v3}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 494
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    goto :goto_0

    .line 498
    :cond_2
    const-string v3, "FMM always background mode!!"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 500
    iget v3, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    .line 502
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 504
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 521
    :cond_3
    :goto_1
    iget v3, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    iget v4, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    iget v5, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/fmm/dm/db/file/XDB;->xdbSetSessionSaveStatus(III)Z

    goto/16 :goto_0

    .line 507
    :cond_4
    iget v3, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_5

    .line 509
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 511
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_1

    .line 516
    :cond_5
    iget v3, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 517
    iget v3, v1, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v3}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpSelectNotiEvt(I)I

    move-result v0

    .line 518
    .local v0, "nMessage":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SetEvent. Event is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 519
    invoke-static {v6, v0}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_1

    .line 526
    .end local v0    # "nMessage":I
    :cond_6
    const-string v3, "Current NOTI NOT SAVED State. EXIT. EXIT."

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 528
    invoke-static {v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus(I)V

    .line 530
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    goto/16 :goto_0
.end method

.method public static xnotiPushAdpFreePushData()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PtWapPush:Lcom/fmm/dm/noti/XNOTIWapPush;

    .line 176
    return-void
.end method

.method public static xnotiPushAdpHandleNotiQueue()V
    .locals 4

    .prologue
    .line 443
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiSessionID()Ljava/lang/String;

    move-result-object v1

    .line 445
    .local v1, "szSessionID":Ljava/lang/String;
    sget-boolean v2, Lcom/fmm/dm/XDMService;->g_bRestoreDataRoaming:Z

    if-eqz v2, :cond_0

    .line 447
    const-string v2, "g_bRestoreDataRoaming true...."

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 448
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmSetDataRoamingEnabled(Z)V

    .line 451
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 453
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Delete Noti Msg sessionId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 454
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBNoti;->xdbNotiDeleteSessionId(Ljava/lang/String;)V

    .line 457
    :cond_1
    invoke-static {}, Lcom/fmm/dm/db/file/XDBNoti;->xdbNotiExistInfo()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 460
    const-string v2, "Next Noti Msg Execute"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 462
    invoke-static {}, Lcom/fmm/dm/db/file/XDBNoti;->xdbNotiGetInfo()Lcom/fmm/dm/db/file/XDBNotiInfo;

    move-result-object v0

    .line 463
    .local v0, "notiInfo":Lcom/fmm/dm/db/file/XDBNotiInfo;
    const/16 v2, 0x34

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 465
    .end local v0    # "notiInfo":Lcom/fmm/dm/db/file/XDBNotiInfo;
    :cond_2
    return-void
.end method

.method public static xnotiPushAdpProcessNotiMessage(I)V
    .locals 5
    .param p0, "nNotiUiEvent"    # I

    .prologue
    const/4 v4, 0x0

    .line 557
    const/4 v1, 0x0

    .line 558
    .local v1, "nNetworkState":I
    const/16 v0, 0x8d

    .line 560
    .local v0, "nMessage":I
    const/4 v2, 0x1

    invoke-static {v2, p0, v4}, Lcom/fmm/dm/db/file/XDB;->xdbSetSessionSaveStatus(III)Z

    .line 561
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    .line 563
    if-nez v1, :cond_0

    .line 565
    invoke-static {p0}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpSelectNotiEvt(I)I

    move-result v0

    .line 566
    const/4 v2, 0x0

    invoke-static {v2, v0}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 574
    :goto_0
    return-void

    .line 571
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nNetworkState is Used."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 572
    invoke-static {v4, p0}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpSuspendNotiAction(II)V

    goto :goto_0
.end method

.method public static xnotiPushAdpProcessWapPushMsg(I)V
    .locals 5
    .param p0, "nId"    # I

    .prologue
    .line 185
    const/4 v0, 0x0

    .line 186
    .local v0, "nID":I
    const/4 v2, 0x0

    .line 187
    .local v2, "ptWapPush":Lcom/fmm/dm/noti/XNOTIWapPush;
    const/4 v1, 0x0

    .line 188
    .local v1, "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    move v0, p0

    .line 190
    invoke-static {v0}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpReadPushData(I)Lcom/fmm/dm/noti/XNOTIWapPush;

    move-result-object v2

    .line 191
    if-nez v2, :cond_0

    .line 210
    :goto_0
    return-void

    .line 196
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 207
    const-string v3, "Not Support Content Type"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 199
    :pswitch_0
    new-instance v1, Lcom/fmm/dm/noti/XNOTIMessage;

    .end local v1    # "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    invoke-direct {v1}, Lcom/fmm/dm/noti/XNOTIMessage;-><init>()V

    .line 200
    .restart local v1    # "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    const/4 v3, 0x1

    iput v3, v1, Lcom/fmm/dm/noti/XNOTIMessage;->push_type:I

    .line 201
    iget-object v3, v2, Lcom/fmm/dm/noti/XNOTIWapPush;->pBody:[B

    iput-object v3, v1, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    .line 202
    iget-object v3, v2, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    iget v3, v3, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nBodyLen:I

    iput v3, v1, Lcom/fmm/dm/noti/XNOTIMessage;->dataSize:I

    .line 203
    const/16 v3, 0x33

    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_0
    .end packed-switch
.end method

.method public static xnotiPushAdpReadPushData(I)Lcom/fmm/dm/noti/XNOTIWapPush;
    .locals 1
    .param p0, "nID"    # I

    .prologue
    .line 149
    sget-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PtWapPush:Lcom/fmm/dm/noti/XNOTIWapPush;

    return-object v0
.end method

.method public static xnotiPushAdpResetSessionSaveState(I)V
    .locals 3
    .param p0, "nAppId"    # I

    .prologue
    const/4 v2, 0x0

    .line 542
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSessionSaveStatus()Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    move-result-object v0

    .line 544
    .local v0, "pSessionSaveInfo":Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    if-eqz v1, :cond_0

    .line 546
    invoke-static {v2, v2, v2}, Lcom/fmm/dm/db/file/XDB;->xdbSetSessionSaveStatus(III)Z

    .line 548
    :cond_0
    const/4 v0, 0x0

    .line 549
    return-void
.end method

.method public static xnotiPushAdpResumeNotiAction(I)V
    .locals 3
    .param p0, "nAppId"    # I

    .prologue
    .line 582
    const/4 v1, 0x0

    .line 583
    .local v1, "nNetworkState":I
    const/4 v0, 0x0

    .line 585
    .local v0, "nEvent":I
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    .line 586
    if-eqz v1, :cond_0

    .line 588
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbGetNotiEvent(I)I

    move-result v0

    .line 589
    packed-switch v0, :pswitch_data_0

    .line 598
    const-string v2, "CAN NOT EXCUTE Noti Resume. EXIT"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 604
    :goto_0
    :pswitch_0
    return-void

    .line 603
    :cond_0
    invoke-static {p0}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpExcuteResumeNoti(I)I

    goto :goto_0

    .line 589
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static xnotiPushAdpSelectNotiEvt(I)I
    .locals 1
    .param p0, "nEvent"    # I

    .prologue
    .line 401
    const/4 v0, 0x0

    .line 402
    .local v0, "nMessage":I
    packed-switch p0, :pswitch_data_0

    .line 417
    const/16 v0, 0x8d

    .line 420
    :goto_0
    return v0

    .line 405
    :pswitch_0
    const/16 v0, 0x8d

    .line 406
    goto :goto_0

    .line 408
    :pswitch_1
    const/16 v0, 0x8e

    .line 409
    goto :goto_0

    .line 411
    :pswitch_2
    const/16 v0, 0x8f

    .line 412
    goto :goto_0

    .line 414
    :pswitch_3
    const/16 v0, 0x90

    .line 415
    goto :goto_0

    .line 402
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static xnotiPushAdpSuspendNotiAction(II)V
    .locals 2
    .param p0, "nAppId"    # I
    .param p1, "nNotiUiEvent"    # I

    .prologue
    const/4 v1, 0x0

    .line 430
    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 431
    const/4 v0, 0x1

    invoke-static {v0, p1, v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetSessionSaveStatus(III)Z

    .line 432
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 433
    return-void
.end method

.method public static xnotiPushAdpWritePushData(Lcom/fmm/dm/noti/XNOTIWapPush;I)I
    .locals 1
    .param p0, "pWapPush"    # Lcom/fmm/dm/noti/XNOTIWapPush;
    .param p1, "nID"    # I

    .prologue
    .line 134
    if-nez p0, :cond_0

    .line 135
    const/4 v0, 0x1

    .line 139
    :goto_0
    return v0

    .line 137
    :cond_0
    sput-object p0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PtWapPush:Lcom/fmm/dm/noti/XNOTIWapPush;

    .line 139
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xnotiPushDataHandling()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 312
    const/4 v1, 0x0

    .line 313
    .local v1, "wssPushData":Lcom/fmm/dm/noti/XNOTIData;
    sget-object v3, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    monitor-enter v3

    .line 315
    :try_start_0
    sget-object v2, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 317
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiSetNotiProcessing(Z)V

    .line 318
    const-string v2, "mPushDataQueue is empty. return"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 319
    monitor-exit v3

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    sget-object v2, Lcom/fmm/dm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/fmm/dm/noti/XNOTIData;

    move-object v1, v0

    .line 323
    const-string v2, "mPushDataQueue poll"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 324
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    if-eqz v1, :cond_0

    .line 328
    iget v2, v1, Lcom/fmm/dm/noti/XNOTIData;->type:I

    packed-switch v2, :pswitch_data_0

    .line 339
    invoke-static {v4}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiSetNotiProcessing(Z)V

    .line 340
    const-string v2, "PUSH_TYPE is not exist"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 324
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 331
    :pswitch_0
    iget-object v2, v1, Lcom/fmm/dm/noti/XNOTIData;->pushData:[B

    invoke-static {v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataReceive([B)V

    goto :goto_0

    .line 335
    :pswitch_1
    iget-object v2, v1, Lcom/fmm/dm/noti/XNOTIData;->pushData:[B

    invoke-static {v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiIPPushDataReceive([B)V

    goto :goto_0

    .line 328
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xnotiPushDataReceive([B)V
    .locals 3
    .param p0, "pushData"    # [B

    .prologue
    .line 348
    if-nez p0, :cond_0

    .line 350
    const-string v1, "pushData is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 357
    :goto_0
    return-void

    .line 354
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiSetNotiProcessing(Z)V

    .line 355
    new-instance v0, Lcom/fmm/dm/noti/XNOTIAdapter;

    invoke-direct {v0}, Lcom/fmm/dm/noti/XNOTIAdapter;-><init>()V

    .line 356
    .local v0, "pushAdp":Lcom/fmm/dm/noti/XNOTIAdapter;
    array-length v1, p0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpReceiveMsg([BILjava/lang/String;)Z

    goto :goto_0
.end method

.method public static xnotiPushGetDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    sget-object v0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_szPushDate:Ljava/lang/String;

    return-object v0
.end method

.method public static xnotiPushSetDate(Ljava/lang/String;)V
    .locals 0
    .param p0, "szDate"    # Ljava/lang/String;

    .prologue
    .line 158
    sput-object p0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_szPushDate:Ljava/lang/String;

    .line 159
    return-void
.end method

.method public static xnotiSetBootstrapUri(Landroid/net/Uri;)V
    .locals 0
    .param p0, "curUri"    # Landroid/net/Uri;

    .prologue
    .line 276
    sput-object p0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_BootStrapUri:Landroid/net/Uri;

    .line 277
    return-void
.end method

.method public static xnotiSetCurAuthCnt(I)V
    .locals 0
    .param p0, "nCnt"    # I

    .prologue
    .line 621
    sput p0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_nAuthCount:I

    .line 622
    return-void
.end method

.method public static xnotiSetNotiProcessing(Z)V
    .locals 2
    .param p0, "notiProcessing"    # Z

    .prologue
    .line 286
    sput-boolean p0, Lcom/fmm/dm/noti/XNOTIAdapter;->m_NotiProcessing:Z

    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notiProcessing : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 288
    return-void
.end method


# virtual methods
.method public xnotiIpPushAdpReceiveMsg([BI)Z
    .locals 4
    .param p1, "pBody"    # [B
    .param p2, "nBodySize"    # I

    .prologue
    const/4 v3, 0x1

    .line 115
    const/4 v0, 0x0

    .line 117
    .local v0, "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    new-instance v0, Lcom/fmm/dm/noti/XNOTIMessage;

    .end local v0    # "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    invoke-direct {v0}, Lcom/fmm/dm/noti/XNOTIMessage;-><init>()V

    .line 118
    .restart local v0    # "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    iput v3, v0, Lcom/fmm/dm/noti/XNOTIMessage;->push_type:I

    .line 119
    iput-object p1, v0, Lcom/fmm/dm/noti/XNOTIMessage;->pBody:[B

    .line 120
    iput p2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->bodySize:I

    .line 122
    const/16 v1, 0x33

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 123
    return v3
.end method

.method public xnotiPushAdpReceiveMsg(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dmuri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 52
    const/4 v8, 0x1

    .line 54
    .local v8, "bRet":Z
    new-instance v0, Lcom/fmm/dm/noti/XNOTIAdapter$xnotiBackgroundQueryHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/fmm/dm/noti/XNOTIAdapter$xnotiBackgroundQueryHandler;-><init>(Lcom/fmm/dm/noti/XNOTIAdapter;Landroid/content/ContentResolver;)V

    .line 55
    .local v0, "queryHandler":Lcom/fmm/dm/noti/XNOTIAdapter$xnotiBackgroundQueryHandler;
    const/16 v1, 0x2537

    sget-object v4, Lcom/fmm/dm/noti/XNOTIAdapter;->PROJECTION:[Ljava/lang/String;

    move-object v3, p2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/fmm/dm/noti/XNOTIAdapter$xnotiBackgroundQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-static {p2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiSetBootstrapUri(Landroid/net/Uri;)V

    .line 58
    return v8
.end method

.method public xnotiPushAdpReceiveMsg([BILjava/lang/String;)Z
    .locals 12
    .param p1, "pMsg"    # [B
    .param p2, "nMsgLen"    # I
    .param p3, "szDate"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 63
    const/4 v5, 0x0

    .line 64
    .local v5, "ptWapPush":Lcom/fmm/dm/noti/XNOTIWapPush;
    const/4 v4, 0x0

    .line 65
    .local v4, "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    const/4 v1, 0x0

    .line 66
    .local v1, "eRet":I
    const/16 v2, 0x204

    .line 68
    .local v2, "nID":I
    const/4 v0, 0x1

    .line 70
    .local v0, "bRet":Z
    invoke-static {p1, p2}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdlrParsingWSPHeader([BI)Lcom/fmm/dm/noti/XNOTIWapPush;

    move-result-object v5

    .line 71
    if-nez v5, :cond_0

    .line 73
    const-string v7, "xnotiPushHdlrParsingWSPHeader Error"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 110
    :goto_0
    return v6

    .line 77
    :cond_0
    iget-object v8, v5, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    iget v8, v8, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    packed-switch v8, :pswitch_data_0

    .line 90
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Not Support Content Type :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "0x%x"

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v10, v5, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    iget v10, v10, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v6

    invoke-static {v9, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 94
    const/16 v6, 0x204

    if-ne v2, v6, :cond_1

    .line 96
    const/4 v0, 0x0

    move v6, v0

    .line 97
    goto :goto_0

    .line 80
    :pswitch_0
    const/16 v2, 0x201

    .line 81
    new-instance v4, Lcom/fmm/dm/noti/XNOTIMessage;

    .end local v4    # "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    invoke-direct {v4}, Lcom/fmm/dm/noti/XNOTIMessage;-><init>()V

    .line 82
    .restart local v4    # "ptNoti":Lcom/fmm/dm/noti/XNOTIMessage;
    iput v7, v4, Lcom/fmm/dm/noti/XNOTIMessage;->push_type:I

    .line 83
    new-array v6, p2, [B

    iput-object v6, v4, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    .line 84
    iput p2, v4, Lcom/fmm/dm/noti/XNOTIMessage;->dataSize:I

    .line 85
    iput-object p1, v4, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    .line 86
    const/16 v6, 0x33

    invoke-static {v6, v4, v11}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v6, v7

    .line 87
    goto :goto_0

    .line 100
    :cond_1
    invoke-static {p3}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushSetDate(Ljava/lang/String;)V

    .line 101
    invoke-static {v5, v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpWritePushData(Lcom/fmm/dm/noti/XNOTIWapPush;I)I

    move-result v1

    .line 102
    if-eqz v1, :cond_2

    .line 104
    const/4 v0, 0x0

    move v6, v0

    .line 105
    goto :goto_0

    .line 108
    :cond_2
    move v3, v2

    .line 109
    .local v3, "pSendId":I
    const/16 v6, 0x39

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6, v7, v11}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v6, v0

    .line 110
    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x44
        :pswitch_0
    .end packed-switch
.end method
