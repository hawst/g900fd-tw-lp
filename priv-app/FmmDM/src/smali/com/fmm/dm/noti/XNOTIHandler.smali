.class public Lcom/fmm/dm/noti/XNOTIHandler;
.super Ljava/lang/Object;
.source "XNOTIHandler.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XNOTIInterface;


# static fields
.field public static g_tNotiMsg:[Lcom/fmm/dm/noti/XNOTI;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-array v1, v3, [Lcom/fmm/dm/noti/XNOTI;

    sput-object v1, Lcom/fmm/dm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/fmm/dm/noti/XNOTI;

    .line 28
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 30
    sget-object v1, Lcom/fmm/dm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/fmm/dm/noti/XNOTI;

    new-instance v2, Lcom/fmm/dm/noti/XNOTI;

    invoke-direct {v2}, Lcom/fmm/dm/noti/XNOTI;-><init>()V

    aput-object v2, v1, v0

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_0
    return-void
.end method

.method public static final XDM_TO_HEX(I)B
    .locals 2
    .param p0, "val"    # I

    .prologue
    .line 18
    const/16 v1, 0x9

    if-le p0, v1, :cond_0

    add-int/lit8 v1, p0, -0xa

    add-int/lit8 v1, v1, 0x41

    :goto_0
    int-to-byte v0, v1

    .line 19
    .local v0, "ret":B
    return v0

    .line 18
    .end local v0    # "ret":B
    :cond_0
    add-int/lit8 v1, p0, 0x30

    goto :goto_0
.end method

.method public static xnotiPushHdleCompareNotiDigest([BILcom/fmm/dm/noti/XNOTI;)I
    .locals 8
    .param p0, "pBody"    # [B
    .param p1, "nBodyLen"    # I
    .param p2, "pNotiMsg"    # Lcom/fmm/dm/noti/XNOTI;

    .prologue
    const/4 v5, -0x1

    .line 138
    const/4 v3, 0x0

    .line 140
    .local v3, "szDigest":Ljava/lang/String;
    iget-object v6, p2, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    if-nez v6, :cond_0

    .line 142
    const-string v6, "triggerHeader is NULL"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 176
    :goto_0
    return v5

    .line 146
    :cond_0
    iget-object v6, p2, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget-object v6, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 148
    const-string v6, "pServerID is NULL"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_1
    add-int/lit8 v1, p1, -0x10

    .line 153
    .local v1, "len":I
    new-array v2, v1, [B

    .line 154
    .local v2, "packetbody":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 156
    aget-byte v6, p0, v0

    aput-byte v6, v2, v0

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 159
    :cond_2
    iget-object v6, p2, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget-object v6, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    const/4 v7, 0x3

    invoke-static {v6, v7, v2, v1}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiDigest(Ljava/lang/String;I[BI)Ljava/lang/String;

    move-result-object v3

    .line 161
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 163
    const-string v6, "pDigest is NULL "

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_3
    new-instance v4, Ljava/lang/String;

    iget-object v6, p2, Lcom/fmm/dm/noti/XNOTI;->digestdata:[B

    invoke-direct {v4, v6}, Ljava/lang/String;-><init>([B)V

    .line 168
    .local v4, "szDigestdata":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 170
    const-string v6, "Compare Fail"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_4
    const-string v5, "Compare Success"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 176
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static xnotiPushHdleInitNotiMsg(Lcom/fmm/dm/noti/XNOTI;I)V
    .locals 0
    .param p0, "pNotiMsg"    # Lcom/fmm/dm/noti/XNOTI;
    .param p1, "appId"    # I

    .prologue
    .line 106
    iput p1, p0, Lcom/fmm/dm/noti/XNOTI;->appId:I

    .line 107
    return-void
.end method

.method public static xnotiPushHdleMessageFree(Lcom/fmm/dm/noti/XNOTIMessage;)V
    .locals 1
    .param p0, "pObj"    # Lcom/fmm/dm/noti/XNOTIMessage;

    .prologue
    const/4 v0, 0x0

    .line 36
    if-nez p0, :cond_0

    .line 43
    :goto_0
    return-void

    .line 39
    :cond_0
    iput-object v0, p0, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    .line 40
    iput-object v0, p0, Lcom/fmm/dm/noti/XNOTIMessage;->pHeader:[B

    .line 41
    iput-object v0, p0, Lcom/fmm/dm/noti/XNOTIMessage;->pBody:[B

    .line 42
    const/4 p0, 0x0

    .line 43
    goto :goto_0
.end method

.method public static xnotiPushHdleParsingSyncNoti([BII)I
    .locals 8
    .param p0, "pPushBody"    # [B
    .param p1, "bodySize"    # I
    .param p2, "nAppId"    # I

    .prologue
    const/4 v5, -0x1

    .line 294
    const/4 v3, 0x0

    .line 295
    .local v3, "pNotiMsg":Lcom/fmm/dm/noti/XNOTI;
    const/4 v2, 0x0

    .line 296
    .local v2, "notiHeaderLen":I
    const/4 v1, 0x0

    .line 297
    .local v1, "nRet":I
    const-string v6, ""

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 299
    sget-object v6, Lcom/fmm/dm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/fmm/dm/noti/XNOTI;

    aget-object v3, v6, p2

    .line 300
    invoke-static {p0}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleParsingSyncNotiDigest([B)[B

    move-result-object v6

    iput-object v6, v3, Lcom/fmm/dm/noti/XNOTI;->digestdata:[B

    .line 302
    array-length v6, p0

    add-int/lit8 v6, v6, -0x10

    new-array v4, v6, [B

    .line 303
    .local v4, "tmp":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, p0

    add-int/lit8 v6, v6, -0x10

    if-ge v0, v6, :cond_0

    .line 305
    add-int/lit8 v6, v0, 0x10

    aget-byte v6, p0, v6

    aput-byte v6, v4, v0

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 308
    :cond_0
    invoke-static {v4, v3}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleParsingSyncNotiHeader([BLcom/fmm/dm/noti/XNOTI;)I

    move-result v2

    .line 309
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " bodySize["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], notiHeaderLen["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] DigestLength["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 310
    if-nez v2, :cond_2

    .line 312
    const-string v6, "notiHeaderLen is 0"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 329
    :cond_1
    :goto_1
    return v5

    .line 315
    :cond_2
    invoke-static {p0, p1, v3}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleCompareNotiDigest([BILcom/fmm/dm/noti/XNOTI;)I

    move-result v1

    .line 319
    if-eqz v1, :cond_3

    .line 321
    invoke-static {p0, p1, v3}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiReSyncCompare([BILcom/fmm/dm/noti/XNOTI;)I

    move-result v1

    .line 322
    if-nez v1, :cond_1

    .line 329
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private xnotiPushHdleParsingSyncNotiBody([BILcom/fmm/dm/noti/XNOTI;)V
    .locals 10
    .param p1, "pPushBody"    # [B
    .param p2, "notiBodyLen"    # I
    .param p3, "pNotiMsg"    # Lcom/fmm/dm/noti/XNOTI;

    .prologue
    const/4 v9, 0x0

    .line 253
    new-instance v2, Lcom/fmm/dm/noti/XNOTITriggerbody;

    invoke-direct {v2}, Lcom/fmm/dm/noti/XNOTITriggerbody;-><init>()V

    .line 254
    .local v2, "body":Lcom/fmm/dm/noti/XNOTITriggerbody;
    const/4 v0, 0x5

    .line 255
    .local v0, "Gap":I
    const/4 v1, 0x0

    .line 256
    .local v1, "Pos":I
    const/4 v5, 0x0

    .line 258
    .local v5, "venderLen":I
    const-string v6, ""

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 259
    if-eqz p1, :cond_1

    .line 261
    iget-object v2, p3, Lcom/fmm/dm/noti/XNOTI;->triggerBody:Lcom/fmm/dm/noti/XNOTITriggerbody;

    .line 262
    aget-byte v6, p1, v9

    shr-int/lit8 v6, v6, 0x4

    iput v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->number_Of_Syncs:I

    .line 264
    iget v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->number_Of_Syncs:I

    new-array v6, v6, [Lcom/fmm/dm/noti/XNOTISyncInfo;

    iput-object v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    .line 266
    const/4 v3, 0x0

    .local v3, "nCount":I
    :goto_0
    iget v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->number_Of_Syncs:I

    if-ge v3, v6, :cond_0

    .line 269
    iget-object v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    new-instance v7, Lcom/fmm/dm/noti/XNOTISyncInfo;

    invoke-direct {v7}, Lcom/fmm/dm/noti/XNOTISyncInfo;-><init>()V

    aput-object v7, v6, v3

    .line 270
    iget-object v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    aget-object v6, v6, v3

    add-int/lit8 v7, v1, 0x1

    aget-byte v7, p1, v7

    shr-int/lit8 v7, v7, 0x4

    or-int/lit16 v7, v7, 0xc8

    iput v7, v6, Lcom/fmm/dm/noti/XNOTISyncInfo;->syncType:I

    .line 271
    iget-object v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    aget-object v6, v6, v3

    add-int/lit8 v7, v1, 0x1

    aget-byte v7, p1, v7

    and-int/lit16 v7, v7, 0xf0

    iput v7, v6, Lcom/fmm/dm/noti/XNOTISyncInfo;->future:I

    .line 272
    iget-object v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    aget-object v6, v6, v3

    add-int/lit8 v7, v1, 0x2

    aget-byte v7, p1, v7

    shl-int/lit8 v7, v7, 0x10

    add-int/lit8 v8, v1, 0x3

    aget-byte v8, p1, v8

    shl-int/lit8 v8, v8, 0x8

    or-int/2addr v7, v8

    add-int/lit8 v8, v1, 0x4

    aget-byte v8, p1, v8

    or-int/2addr v7, v8

    iput v7, v6, Lcom/fmm/dm/noti/XNOTISyncInfo;->contentType:I

    .line 273
    iget-object v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    aget-object v6, v6, v3

    add-int/lit8 v7, v1, 0x5

    aget-byte v7, p1, v7

    iput v7, v6, Lcom/fmm/dm/noti/XNOTISyncInfo;->serverUriLen:I

    .line 275
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([B)V

    .line 276
    .local v4, "szBody":Ljava/lang/String;
    iget-object v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    aget-object v6, v6, v3

    add-int/lit8 v7, v1, 0x6

    iget-object v8, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    aget-object v8, v8, v3

    iget v8, v8, Lcom/fmm/dm/noti/XNOTISyncInfo;->serverUriLen:I

    add-int/2addr v8, v1

    add-int/lit8 v8, v8, 0x6

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/fmm/dm/noti/XNOTISyncInfo;->m_szServerUri:Ljava/lang/String;

    .line 277
    add-int v6, v1, v0

    iget-object v7, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    aget-object v7, v7, v3

    iget v7, v7, Lcom/fmm/dm/noti/XNOTISyncInfo;->serverUriLen:I

    add-int v1, v6, v7

    .line 266
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 280
    .end local v4    # "szBody":Ljava/lang/String;
    :cond_0
    sub-int v5, p2, v1

    .line 281
    iput v9, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->vendor:I

    .line 283
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_1

    .line 285
    iget v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->vendor:I

    shl-int/lit8 v6, v6, 0x8

    iput v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->vendor:I

    .line 286
    iget v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->vendor:I

    add-int/lit8 v7, v1, 0x1

    aget-byte v7, p1, v7

    or-int/2addr v6, v7

    iput v6, v2, Lcom/fmm/dm/noti/XNOTITriggerbody;->vendor:I

    .line 287
    add-int/lit8 v1, v1, 0x1

    .line 283
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 290
    .end local v3    # "nCount":I
    :cond_1
    return-void
.end method

.method public static xnotiPushHdleParsingSyncNotiDigest([B)[B
    .locals 5
    .param p0, "pPushBody"    # [B

    .prologue
    const/16 v4, 0x10

    .line 182
    const/4 v1, 0x0

    .line 183
    .local v1, "szDigest":[B
    if-eqz p0, :cond_1

    .line 186
    new-array v1, v4, [B

    .line 187
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 189
    aget-byte v3, p0, v0

    aput-byte v3, v1, v0

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v2, v1

    .line 194
    .end local v0    # "i":I
    .end local v1    # "szDigest":[B
    .local v2, "szDigest":[B
    :goto_1
    return-object v2

    .end local v2    # "szDigest":[B
    .restart local v1    # "szDigest":[B
    :cond_1
    move-object v2, v1

    .end local v1    # "szDigest":[B
    .restart local v2    # "szDigest":[B
    goto :goto_1
.end method

.method public static xnotiPushHdleParsingSyncNotiHeader([BLcom/fmm/dm/noti/XNOTI;)I
    .locals 14
    .param p0, "pPushBody2"    # [B
    .param p1, "pNotiMsg"    # Lcom/fmm/dm/noti/XNOTI;

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v6, 0x0

    const/4 v11, 0x2

    const/4 v7, 0x1

    .line 199
    const/4 v3, 0x0

    .line 200
    .local v3, "len_noti_header":I
    array-length v8, p0

    new-array v4, v8, [B

    .line 201
    .local v4, "pPushBody":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v8, p0

    if-ge v0, v8, :cond_0

    .line 203
    aget-byte v8, p0, v0

    aput-byte v8, v4, v0

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    if-eqz v4, :cond_3

    .line 208
    iget-object v8, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    aget-byte v9, v4, v6

    shl-int/lit8 v9, v9, 0x8

    aget-byte v10, v4, v7

    and-int/lit16 v10, v10, 0xc0

    or-int/2addr v9, v10

    shr-int/lit8 v9, v9, 0x6

    iput v9, v8, Lcom/fmm/dm/noti/XNOTITriggerheader;->version:I

    .line 209
    aget-byte v8, v4, v7

    and-int/lit8 v8, v8, 0x30

    shr-int/lit8 v8, v8, 0x4

    packed-switch v8, :pswitch_data_0

    .line 221
    iget-object v8, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iput v13, v8, Lcom/fmm/dm/noti/XNOTITriggerheader;->uiMode:I

    .line 225
    :goto_1
    aget-byte v8, v4, v7

    and-int/lit8 v8, v8, 0x8

    shr-int/lit8 v1, v8, 0x3

    .line 226
    .local v1, "init":I
    iget-object v8, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    if-lez v1, :cond_1

    move v6, v7

    :cond_1
    iput v6, v8, Lcom/fmm/dm/noti/XNOTITriggerheader;->initiator:I

    .line 227
    iget-object v6, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    aget-byte v7, v4, v7

    and-int/lit8 v7, v7, 0x7

    aget-byte v8, v4, v11

    or-int/2addr v7, v8

    aget-byte v8, v4, v12

    or-int/2addr v7, v8

    aget-byte v8, v4, v13

    or-int/2addr v7, v8

    iput v7, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->future:I

    .line 228
    iget-object v6, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    const/4 v7, 0x5

    invoke-static {v4, v7, v11}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibToHexString([BII)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szSessionID:Ljava/lang/String;

    .line 229
    iget-object v6, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    const/4 v7, 0x7

    aget-byte v7, v4, v7

    iput v7, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->lenServerID:I

    .line 231
    add-int/lit8 v3, v3, 0x8

    .line 234
    iget-object v6, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget v6, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->lenServerID:I

    new-array v5, v6, [B

    .line 235
    .local v5, "tmp":[B
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    iget-object v6, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget v6, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->lenServerID:I

    if-ge v2, v6, :cond_2

    .line 237
    add-int/lit8 v6, v2, 0x8

    aget-byte v6, p0, v6

    aput-byte v6, v5, v2

    .line 235
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 212
    .end local v1    # "init":I
    .end local v2    # "j":I
    .end local v5    # "tmp":[B
    :pswitch_0
    iget-object v8, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iput v7, v8, Lcom/fmm/dm/noti/XNOTITriggerheader;->uiMode:I

    goto :goto_1

    .line 215
    :pswitch_1
    iget-object v8, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iput v11, v8, Lcom/fmm/dm/noti/XNOTITriggerheader;->uiMode:I

    goto :goto_1

    .line 218
    :pswitch_2
    iget-object v8, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iput v12, v8, Lcom/fmm/dm/noti/XNOTITriggerheader;->uiMode:I

    goto :goto_1

    .line 239
    .restart local v1    # "init":I
    .restart local v2    # "j":I
    .restart local v5    # "tmp":[B
    :cond_2
    iget-object v6, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v5}, Ljava/lang/String;-><init>([B)V

    iput-object v7, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    .line 240
    iget-object v6, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget v6, v6, Lcom/fmm/dm/noti/XNOTITriggerheader;->lenServerID:I

    add-int/lit8 v3, v6, 0x8

    .line 242
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SessionID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget-object v7, v7, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szSessionID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 243
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "UI Mode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget v7, v7, Lcom/fmm/dm/noti/XNOTITriggerheader;->uiMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 246
    .end local v1    # "init":I
    .end local v2    # "j":I
    .end local v5    # "tmp":[B
    :cond_3
    return v3

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private xnotiPushHdleSDMMakeToHex([B[BI)V
    .locals 4
    .param p1, "pMacData"    # [B
    .param p2, "Mac_code"    # [B
    .param p3, "length"    # I

    .prologue
    .line 335
    const/4 v0, 0x0

    .line 336
    .local v0, "index":I
    new-array v2, p3, [B

    .line 337
    .local v2, "pBuf":[B
    move-object v2, p1

    .line 338
    const/4 v0, 0x0

    move v1, v0

    .end local v0    # "index":I
    .local v1, "index":I
    :goto_0
    if-ge v1, p3, :cond_0

    .line 340
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "index":I
    .restart local v0    # "index":I
    aget-byte v3, p2, v0

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v3, v3, 0xf

    invoke-static {v3}, Lcom/fmm/dm/noti/XNOTIHandler;->XDM_TO_HEX(I)B

    move-result v3

    aput-byte v3, v2, v1

    .line 341
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .restart local v1    # "index":I
    aget-byte v3, p2, v1

    and-int/lit8 v3, v3, 0xf

    invoke-static {v3}, Lcom/fmm/dm/noti/XNOTIHandler;->XDM_TO_HEX(I)B

    move-result v3

    aput-byte v3, v2, v0

    .line 338
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "index":I
    .restart local v0    # "index":I
    move v1, v0

    .end local v0    # "index":I
    .restart local v1    # "index":I
    goto :goto_0

    .line 343
    :cond_0
    return-void
.end method

.method public static xnotiPushHdleWSPHeader([BI)I
    .locals 4
    .param p0, "buf"    # [B
    .param p1, "len"    # I

    .prologue
    .line 88
    const/4 v1, 0x0

    .line 89
    .local v1, "lenWSP":I
    const/4 v0, 0x0

    .line 91
    .local v0, "LenHeader":I
    const/4 v2, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    .line 93
    const/4 v2, 0x2

    aget-byte v1, p0, v2

    .line 94
    add-int/lit8 v0, v1, 0x3

    .line 96
    if-ge v0, p1, :cond_0

    move v2, v0

    .line 101
    :goto_0
    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static xnotiPushHdleWSPHeader([BII)Z
    .locals 4
    .param p0, "buf"    # [B
    .param p1, "len"    # I
    .param p2, "LenHeader"    # I

    .prologue
    const/4 v1, 0x1

    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, "lenWSP":I
    aget-byte v2, p0, v1

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    .line 75
    const/4 v2, 0x2

    aget-byte v0, p0, v2

    .line 76
    add-int/lit8 p2, v0, 0x3

    .line 78
    if-ge p2, p1, :cond_0

    .line 83
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static xnotiPushHdlrParsingWSPHeader([BI)Lcom/fmm/dm/noti/XNOTIWapPush;
    .locals 24
    .param p0, "szMsg"    # [B
    .param p1, "nMsgLen"    # I

    .prologue
    .line 435
    const/4 v14, 0x0

    .line 436
    .local v14, "ptMsg":Lcom/fmm/dm/noti/XNOTIWapPush;
    const/4 v11, 0x0

    .line 437
    .local v11, "nLoc":I
    const/16 v20, 0x0

    .line 438
    .local v20, "value":I
    if-eqz p0, :cond_0

    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v21, v0

    if-nez v21, :cond_1

    .line 440
    :cond_0
    const-string v21, "pData is NULL"

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 441
    const/16 v21, 0x0

    .line 622
    :goto_0
    return-object v21

    .line 444
    :cond_1
    const/4 v11, 0x1

    .line 445
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_2

    .line 447
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Not Support PDU Type="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 448
    const/16 v21, 0x0

    goto :goto_0

    .line 451
    :cond_2
    new-instance v14, Lcom/fmm/dm/noti/XNOTIWapPush;

    .end local v14    # "ptMsg":Lcom/fmm/dm/noti/XNOTIWapPush;
    invoke-direct {v14}, Lcom/fmm/dm/noti/XNOTIWapPush;-><init>()V

    .line 452
    .restart local v14    # "ptMsg":Lcom/fmm/dm/noti/XNOTIWapPush;
    const/4 v11, 0x2

    .line 453
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nHeaderLen:I

    .line 454
    add-int/lit8 v11, v11, 0x1

    .line 455
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x1f

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 457
    add-int/lit8 v11, v11, 0x2

    .line 460
    :cond_3
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x20

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_b

    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x80

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_b

    .line 463
    const/4 v7, 0x0

    .local v7, "i":I
    const/4 v2, 0x0

    .local v2, "Loc":I
    const/4 v8, 0x0

    .line 464
    .local v8, "j":I
    move v2, v11

    .line 465
    :goto_1
    aget-byte v21, p0, v11

    if-eqz v21, :cond_4

    .line 467
    add-int/lit8 v7, v7, 0x1

    .line 468
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 470
    :cond_4
    new-array v5, v7, [B

    .line 471
    .local v5, "Mime":[B
    :goto_2
    aget-byte v19, p0, v2

    .local v19, "txt":B
    if-eqz v19, :cond_5

    .line 473
    aput-byte v19, v5, v8

    .line 474
    add-int/lit8 v8, v8, 0x1

    .line 475
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 477
    :cond_5
    new-instance v18, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([B)V

    .line 478
    .local v18, "szWbxml":Ljava/lang/String;
    const-string v21, "application/vnd.syncml.notification"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 480
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x36

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 499
    :goto_3
    add-int/lit8 v11, v11, 0x1

    .line 534
    .end local v2    # "Loc":I
    .end local v5    # "Mime":[B
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v18    # "szWbxml":Ljava/lang/String;
    .end local v19    # "txt":B
    :goto_4
    aget-byte v21, p0, v11

    if-nez v21, :cond_6

    .line 536
    add-int/lit8 v11, v11, 0x1

    .line 539
    :cond_6
    const-string v21, "SEC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    new-array v6, v0, [B

    .line 540
    .local v6, "a":[B
    const-string v21, "SEC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v15

    .line 541
    .local v15, "strLen":I
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_5
    if-ge v7, v15, :cond_10

    .line 543
    add-int v21, v11, v7

    aget-byte v21, p0, v21

    aput-byte v21, v6, v7

    .line 541
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 482
    .end local v6    # "a":[B
    .end local v15    # "strLen":I
    .restart local v2    # "Loc":I
    .restart local v5    # "Mime":[B
    .restart local v8    # "j":I
    .restart local v18    # "szWbxml":Ljava/lang/String;
    .restart local v19    # "txt":B
    :cond_7
    const-string v21, "application/vnd.wap.connectivity-xml"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 484
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x35

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    goto :goto_3

    .line 486
    :cond_8
    const-string v21, "application/vnd.syncml.notification"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 488
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x44

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    goto :goto_3

    .line 490
    :cond_9
    const-string v21, "application/vnd.syncml.dm+wbxml"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 492
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x42

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    goto :goto_3

    .line 496
    :cond_a
    const-string v21, "Not Support Content Type"

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 497
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 503
    .end local v2    # "Loc":I
    .end local v5    # "Mime":[B
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v18    # "szWbxml":Ljava/lang/String;
    .end local v19    # "txt":B
    :cond_b
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Content Value:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    aget-byte v22, p0, v11

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 504
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Mask Value:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const/16 v22, 0x3

    aget-byte v22, p0, v22

    and-int/lit8 v22, v22, 0x7f

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 506
    aget-byte v21, p0, v11

    and-int/lit8 v21, v21, 0x7f

    const/16 v22, 0x36

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    .line 508
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0x7f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 509
    const-string v21, "Content Type: XNOTI_MIMETYPE_WAP_CONNECTIVITY_WBXML"

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 531
    :goto_6
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 511
    :cond_c
    aget-byte v21, p0, v11

    and-int/lit8 v21, v21, 0x7f

    const/16 v22, 0x35

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    .line 513
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0x7f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 514
    const-string v21, "Content Type: XNOTI_MIMETYPE_WAP_CONNECTIVITY_XML"

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_6

    .line 516
    :cond_d
    aget-byte v21, p0, v11

    and-int/lit8 v21, v21, 0x7f

    const/16 v22, 0x44

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 518
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0x7f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 519
    const-string v21, "Content Type: XNOTI_MIMETYPE_SYNCML_DM_NOTI"

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_6

    .line 521
    :cond_e
    aget-byte v21, p0, v11

    and-int/lit8 v21, v21, 0x7f

    const/16 v22, 0x42

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 523
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0x7f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 524
    const-string v21, "Content Type: XNOTI_MIMETYPE_SYNCML_DM_WBXML"

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_6

    .line 528
    :cond_f
    const-string v21, "Not Support Content Type"

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 529
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 545
    .restart local v6    # "a":[B
    .restart local v7    # "i":I
    .restart local v15    # "strLen":I
    :cond_10
    new-instance v17, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>([B)V

    .line 547
    .local v17, "szSec":Ljava/lang/String;
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x91

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_13

    .line 549
    add-int/lit8 v11, v11, 0x1

    .line 550
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nSEC:I

    .line 551
    add-int/lit8 v11, v11, 0x1

    .line 560
    :cond_11
    :goto_7
    aget-byte v21, p0, v11

    if-nez v21, :cond_12

    .line 562
    add-int/lit8 v11, v11, 0x1

    .line 564
    :cond_12
    const-string v21, "MAC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    new-array v13, v0, [B

    .line 565
    .local v13, "nMac":[B
    const-string v21, "MAC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v15

    .line 566
    const/4 v7, 0x0

    :goto_8
    if-ge v7, v15, :cond_14

    .line 568
    add-int v21, v11, v7

    aget-byte v21, p0, v21

    aput-byte v21, v13, v7

    .line 566
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 553
    .end local v13    # "nMac":[B
    :cond_13
    const-string v21, "SEC"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 555
    const-string v21, "SEC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v21, v21, v11

    add-int/lit8 v11, v21, 0x1

    .line 556
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0xf

    move/from16 v0, v22

    or-int/lit16 v0, v0, 0x80

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nSEC:I

    .line 557
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 570
    .restart local v13    # "nMac":[B
    :cond_14
    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Ljava/lang/String;-><init>([B)V

    .line 571
    .local v16, "szMac":Ljava/lang/String;
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x92

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_18

    .line 574
    const/4 v7, 0x0

    const/4 v2, 0x0

    .restart local v2    # "Loc":I
    const/4 v8, 0x0

    .line 575
    .restart local v8    # "j":I
    add-int/lit8 v11, v11, 0x1

    .line 576
    move v2, v11

    .line 577
    :goto_9
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "nLoc":I
    .local v12, "nLoc":I
    aget-byte v21, p0, v11

    if-eqz v21, :cond_15

    .line 579
    add-int/lit8 v7, v7, 0x1

    move v11, v12

    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    goto :goto_9

    .line 581
    .end local v11    # "nLoc":I
    .restart local v12    # "nLoc":I
    :cond_15
    new-array v4, v7, [B

    .line 582
    .local v4, "MacValue":[B
    :goto_a
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "Loc":I
    .local v3, "Loc":I
    aget-byte v9, p0, v2

    .local v9, "msg":B
    if-eqz v9, :cond_16

    .line 584
    aput-byte v9, v4, v8

    .line 585
    add-int/lit8 v8, v8, 0x1

    move v2, v3

    .end local v3    # "Loc":I
    .restart local v2    # "Loc":I
    goto :goto_a

    .line 588
    .end local v2    # "Loc":I
    .restart local v3    # "Loc":I
    :cond_16
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    array-length v0, v4

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nMACLen:I

    .line 589
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->szMAC:[B

    .line 590
    add-int/lit8 v11, v12, 0x1

    .line 615
    .end local v3    # "Loc":I
    .end local v4    # "MacValue":[B
    .end local v8    # "j":I
    .end local v9    # "msg":B
    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    :cond_17
    :goto_b
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    add-int/lit8 v22, p1, -0x2

    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nHeaderLen:I

    move/from16 v23, v0

    sub-int v22, v22, v23

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nBodyLen:I

    .line 616
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nHeaderLen:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x2

    add-int/lit8 v11, v21, 0x1

    .line 617
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nBodyLen:I

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v0, v0, [B

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->pBody:[B

    .line 618
    const/4 v10, 0x0

    .local v10, "n":I
    :goto_c
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nBodyLen:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v10, v0, :cond_1b

    .line 620
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->pBody:[B

    move-object/from16 v21, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "nLoc":I
    .restart local v12    # "nLoc":I
    aget-byte v22, p0, v11

    aput-byte v22, v21, v10

    .line 618
    add-int/lit8 v10, v10, 0x1

    move v11, v12

    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    goto :goto_c

    .line 592
    .end local v10    # "n":I
    :cond_18
    const-string v21, "MAC"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_17

    .line 595
    const/4 v7, 0x0

    const/4 v2, 0x0

    .restart local v2    # "Loc":I
    const/4 v8, 0x0

    .line 596
    .restart local v8    # "j":I
    const-string v21, "MAC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v21, v21, v11

    add-int/lit8 v11, v21, 0x1

    .line 598
    move v2, v11

    .line 599
    :goto_d
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "nLoc":I
    .restart local v12    # "nLoc":I
    aget-byte v21, p0, v11

    if-eqz v21, :cond_19

    .line 601
    add-int/lit8 v7, v7, 0x1

    move v11, v12

    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    goto :goto_d

    .line 603
    .end local v11    # "nLoc":I
    .restart local v12    # "nLoc":I
    :cond_19
    new-array v4, v7, [B

    .line 604
    .restart local v4    # "MacValue":[B
    :goto_e
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "Loc":I
    .restart local v3    # "Loc":I
    aget-byte v9, p0, v2

    .restart local v9    # "msg":B
    if-eqz v9, :cond_1a

    .line 606
    aput-byte v9, v4, v8

    .line 607
    add-int/lit8 v8, v8, 0x1

    move v2, v3

    .end local v3    # "Loc":I
    .restart local v2    # "Loc":I
    goto :goto_e

    .line 610
    .end local v2    # "Loc":I
    .restart local v3    # "Loc":I
    :cond_1a
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    array-length v0, v4

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->nMACLen:I

    .line 611
    iget-object v0, v14, Lcom/fmm/dm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/fmm/dm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/fmm/dm/noti/XNOTIWapPushInfo;->szMAC:[B

    .line 612
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    goto/16 :goto_b

    .end local v3    # "Loc":I
    .end local v4    # "MacValue":[B
    .end local v8    # "j":I
    .end local v9    # "msg":B
    .restart local v10    # "n":I
    :cond_1b
    move-object/from16 v21, v14

    .line 622
    goto/16 :goto_0
.end method

.method public static xnotiReSyncCompare([BILcom/fmm/dm/noti/XNOTI;)I
    .locals 13
    .param p0, "pBody"    # [B
    .param p1, "nBodyLen"    # I
    .param p2, "msg"    # Lcom/fmm/dm/noti/XNOTI;

    .prologue
    const/4 v12, -0x1

    .line 627
    const/4 v8, 0x0

    .line 628
    .local v8, "nRet":I
    move-object v9, p2

    .line 629
    .local v9, "pNotiMsg":Lcom/fmm/dm/noti/XNOTI;
    const/4 v10, 0x0

    .line 630
    .local v10, "pNvInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    const/4 v11, 0x0

    .line 631
    .local v11, "szDigest":Ljava/lang/String;
    const/4 v0, 0x4

    new-array v3, v0, [B

    fill-array-data v3, :array_0

    .line 633
    .local v3, "szReSyncNonce":[B
    const/4 v7, 0x0

    .line 635
    .local v7, "bResyncModeRet":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNonceResync()Z

    move-result v0

    if-nez v0, :cond_0

    .line 637
    const-string v0, "nonce resync SKIP!!!"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 638
    const/4 v0, 0x0

    .line 683
    :goto_0
    return v0

    .line 641
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    if-nez v0, :cond_2

    :cond_1
    move v0, v12

    .line 643
    goto :goto_0

    .line 645
    :cond_2
    iget-object v0, p2, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget-object v0, v0, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v12

    .line 647
    goto :goto_0

    .line 651
    :cond_3
    iget-object v0, v9, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iget-object v0, v0, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetActiveProfileIndexByServerID(Ljava/lang/String;)I

    .line 652
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileInfo()Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v10

    .line 653
    if-nez v10, :cond_4

    .line 655
    const-string v0, "DM Info is NULL "

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v12

    .line 656
    goto :goto_0

    .line 658
    :cond_4
    const/4 v0, 0x3

    iget-object v1, v10, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    iget-object v2, v10, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    array-length v4, v3

    add-int/lit8 v6, p1, -0x10

    move-object v5, p0

    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v11

    .line 660
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 662
    const-string v0, "Digest is NULL "

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v12

    .line 663
    goto :goto_0

    .line 666
    :cond_5
    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 667
    new-instance v0, Ljava/lang/String;

    iget-object v1, v9, Lcom/fmm/dm/noti/XNOTI;->digestdata:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v11, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    .line 669
    const-string v0, "Fail"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 670
    const/4 v8, -0x1

    :cond_6
    :goto_1
    move v0, v8

    .line 683
    goto :goto_0

    .line 674
    :cond_7
    const-string v0, "xnotiReSyncCompare Success"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 675
    const/4 v8, 0x0

    .line 676
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiReSyncMode(I)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 677
    if-nez v7, :cond_6

    .line 679
    const-string v0, "Fail"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 680
    const/4 v8, -0x1

    goto :goto_1

    .line 631
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public xnotiPushHdleFreeNotiMsg(Lcom/fmm/dm/noti/XNOTI;)V
    .locals 3
    .param p1, "pNotiMsg"    # Lcom/fmm/dm/noti/XNOTI;

    .prologue
    const/4 v2, 0x0

    .line 111
    if-nez p1, :cond_0

    .line 113
    const-string v1, "pNotiMsg is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 134
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v1, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    iput-object v2, v1, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    .line 120
    iput-object v2, p1, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    .line 123
    :cond_1
    iget-object v1, p1, Lcom/fmm/dm/noti/XNOTI;->triggerBody:Lcom/fmm/dm/noti/XNOTITriggerbody;

    if-eqz v1, :cond_3

    .line 127
    const/4 v0, 0x0

    .local v0, "nCount":I
    :goto_1
    iget-object v1, p1, Lcom/fmm/dm/noti/XNOTI;->triggerBody:Lcom/fmm/dm/noti/XNOTITriggerbody;

    iget v1, v1, Lcom/fmm/dm/noti/XNOTITriggerbody;->number_Of_Syncs:I

    if-ge v0, v1, :cond_2

    .line 129
    iget-object v1, p1, Lcom/fmm/dm/noti/XNOTI;->triggerBody:Lcom/fmm/dm/noti/XNOTITriggerbody;

    iget-object v1, v1, Lcom/fmm/dm/noti/XNOTITriggerbody;->syncInfo:[Lcom/fmm/dm/noti/XNOTISyncInfo;

    aget-object v1, v1, v0

    iput-object v2, v1, Lcom/fmm/dm/noti/XNOTISyncInfo;->m_szServerUri:Ljava/lang/String;

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 131
    :cond_2
    iput-object v2, p1, Lcom/fmm/dm/noti/XNOTI;->triggerBody:Lcom/fmm/dm/noti/XNOTITriggerbody;

    .line 133
    .end local v0    # "nCount":I
    :cond_3
    const/4 p1, 0x0

    .line 134
    goto :goto_0
.end method

.method public xnotiPushHdleMessageCopy(Ljava/lang/Object;)Lcom/fmm/dm/noti/XNOTIMessage;
    .locals 3
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 47
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/noti/XNOTIMessage;

    .line 48
    .local v1, "pSrc":Lcom/fmm/dm/noti/XNOTIMessage;
    new-instance v0, Lcom/fmm/dm/noti/XNOTIMessage;

    invoke-direct {v0}, Lcom/fmm/dm/noti/XNOTIMessage;-><init>()V

    .line 50
    .local v0, "pDst":Lcom/fmm/dm/noti/XNOTIMessage;
    iget-object v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    if-eqz v2, :cond_1

    .line 52
    iget-object v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    iput-object v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    .line 53
    iget v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->dataSize:I

    iput v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->dataSize:I

    .line 61
    :cond_0
    :goto_0
    iget v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->bodySize:I

    iput v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->bodySize:I

    .line 62
    iget v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->mime_type:I

    iput v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->mime_type:I

    .line 63
    iget v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->push_type:I

    iput v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->push_type:I

    .line 64
    iget v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->push_status:I

    iput v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->push_status:I

    .line 65
    iget v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->appId:I

    iput v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->appId:I

    .line 67
    return-object v0

    .line 55
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->pBody:[B

    if-eqz v2, :cond_0

    .line 57
    iget v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->bodySize:I

    new-array v2, v2, [B

    iput-object v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    .line 58
    iget-object v2, v1, Lcom/fmm/dm/noti/XNOTIMessage;->pBody:[B

    iput-object v2, v0, Lcom/fmm/dm/noti/XNOTIMessage;->pBody:[B

    goto :goto_0
.end method

.method public xnotiPushHdleMsgHandler(Lcom/fmm/dm/noti/XNOTIMessage;)Lcom/fmm/dm/noti/XNOTI;
    .locals 4
    .param p1, "pPushMsg"    # Lcom/fmm/dm/noti/XNOTIMessage;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 405
    new-instance v0, Lcom/fmm/dm/noti/XNOTI;

    invoke-direct {v0}, Lcom/fmm/dm/noti/XNOTI;-><init>()V

    .line 407
    .local v0, "ptMsg":Lcom/fmm/dm/noti/XNOTI;
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 408
    iget v1, p1, Lcom/fmm/dm/noti/XNOTIMessage;->push_type:I

    packed-switch v1, :pswitch_data_0

    .line 424
    :pswitch_0
    const-string v1, "Not Support Push Type"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 428
    :goto_0
    invoke-static {p1}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleMessageFree(Lcom/fmm/dm/noti/XNOTIMessage;)V

    .line 430
    return-object v0

    .line 411
    :pswitch_1
    iput v2, p1, Lcom/fmm/dm/noti/XNOTIMessage;->appId:I

    .line 412
    iput v3, p1, Lcom/fmm/dm/noti/XNOTIMessage;->mime_type:I

    .line 413
    const-string v1, "XNOTI_MIME_CONTENT_TYPE_DM "

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 414
    invoke-virtual {p0, p1}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleNotiMsgHandler(Lcom/fmm/dm/noti/XNOTIMessage;)Lcom/fmm/dm/noti/XNOTI;

    move-result-object v0

    .line 415
    goto :goto_0

    .line 418
    :pswitch_2
    iput v2, p1, Lcom/fmm/dm/noti/XNOTIMessage;->appId:I

    .line 419
    iput v3, p1, Lcom/fmm/dm/noti/XNOTIMessage;->mime_type:I

    .line 420
    const-string v1, "XNOTI_MIME_CONTENT_TYPE_DM "

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 408
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public xnotiPushHdleNotiMsgHandler(Lcom/fmm/dm/noti/XNOTIMessage;)Lcom/fmm/dm/noti/XNOTI;
    .locals 12
    .param p1, "pPushMsg"    # Lcom/fmm/dm/noti/XNOTIMessage;

    .prologue
    const/4 v9, 0x0

    .line 347
    const/4 v2, 0x0

    .line 348
    .local v2, "appId":I
    const/4 v6, 0x0

    .line 350
    .local v6, "nRet":I
    if-nez p1, :cond_0

    .line 352
    const-string v10, "pPushMsg is NULL"

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 399
    :goto_0
    return-object v9

    .line 356
    :cond_0
    iget v2, p1, Lcom/fmm/dm/noti/XNOTIMessage;->appId:I

    .line 357
    sget-object v10, Lcom/fmm/dm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/fmm/dm/noti/XNOTI;

    aget-object v10, v10, v2

    invoke-static {v10, v2}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleInitNotiMsg(Lcom/fmm/dm/noti/XNOTI;I)V

    .line 358
    sget-object v10, Lcom/fmm/dm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/fmm/dm/noti/XNOTI;

    aget-object v10, v10, v2

    iget v11, p1, Lcom/fmm/dm/noti/XNOTIMessage;->mime_type:I

    iput v11, v10, Lcom/fmm/dm/noti/XNOTI;->mimeType:I

    .line 360
    iget-object v10, p1, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    if-eqz v10, :cond_2

    .line 362
    iget-object v10, p1, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    const/4 v11, 0x0

    aget-byte v3, v10, v11

    .line 363
    .local v3, "data":B
    if-eqz v3, :cond_2

    .line 365
    iget v10, p1, Lcom/fmm/dm/noti/XNOTIMessage;->dataSize:I

    new-array v8, v10, [B

    .line 366
    .local v8, "ptrPushData":[B
    const/4 v1, 0x0

    .line 367
    .local v1, "PushDataLength":I
    const/4 v0, 0x0

    .line 368
    .local v0, "LenHeader":I
    const-string v10, "header and body"

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 370
    iget-object v8, p1, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    .line 371
    iget v1, p1, Lcom/fmm/dm/noti/XNOTIMessage;->dataSize:I

    .line 373
    invoke-static {v8, v1}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleWSPHeader([BI)I

    move-result v0

    .line 374
    const/4 v10, -0x1

    if-eq v0, v10, :cond_2

    .line 376
    sub-int v5, v1, v0

    .line 377
    .local v5, "len":I
    new-array v7, v5, [B

    .line 378
    .local v7, "ptrPushBody":[B
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v5, :cond_1

    .line 380
    add-int v10, v0, v4

    aget-byte v10, v8, v10

    aput-byte v10, v7, v4

    .line 378
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 383
    :cond_1
    iput-object v9, p1, Lcom/fmm/dm/noti/XNOTIMessage;->pBody:[B

    .line 384
    iput v5, p1, Lcom/fmm/dm/noti/XNOTIMessage;->bodySize:I

    .line 385
    iput-object v7, p1, Lcom/fmm/dm/noti/XNOTIMessage;->pBody:[B

    .line 386
    iput-object v9, p1, Lcom/fmm/dm/noti/XNOTIMessage;->pData:[B

    .line 391
    .end local v0    # "LenHeader":I
    .end local v1    # "PushDataLength":I
    .end local v3    # "data":B
    .end local v4    # "i":I
    .end local v5    # "len":I
    .end local v7    # "ptrPushBody":[B
    .end local v8    # "ptrPushData":[B
    :cond_2
    iget-object v10, p1, Lcom/fmm/dm/noti/XNOTIMessage;->pBody:[B

    iget v11, p1, Lcom/fmm/dm/noti/XNOTIMessage;->bodySize:I

    invoke-static {v10, v11, v2}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleParsingSyncNoti([BII)I

    move-result v6

    .line 392
    if-nez v6, :cond_3

    .line 394
    sget-object v9, Lcom/fmm/dm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/fmm/dm/noti/XNOTI;

    aget-object v9, v9, v2

    goto :goto_0

    .line 398
    :cond_3
    sget-object v10, Lcom/fmm/dm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/fmm/dm/noti/XNOTI;

    aget-object v10, v10, v2

    invoke-virtual {p0, v10}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleFreeNotiMsg(Lcom/fmm/dm/noti/XNOTI;)V

    goto :goto_0
.end method
