.class public Lcom/fmm/dm/eng/parser/XDMParserResults;
.super Ljava/lang/Object;
.source "XDMParserResults.java"

# interfaces
.implements Lcom/fmm/dm/eng/core/XDMWbxml;
.implements Lcom/fmm/dm/interfaces/XDMInterface;


# instance fields
.field public cmdid:I

.field public itemlist:Lcom/fmm/dm/eng/core/XDMList;

.field public m_szCmdRef:Ljava/lang/String;

.field public m_szMsgRef:Ljava/lang/String;

.field public m_szSourceRef:Ljava/lang/String;

.field public m_szTargetRef:Ljava/lang/String;

.field public meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 13
    return-void
.end method


# virtual methods
.method public xdmBuildResultsCommand(Lcom/fmm/dm/adapter/XDMDevinfAdapter;)Lcom/fmm/dm/eng/parser/XDMParserResults;
    .locals 10
    .param p1, "dev"    # Lcom/fmm/dm/adapter/XDMDevinfAdapter;

    .prologue
    .line 128
    new-instance v4, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v4}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 129
    .local v4, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    new-instance v2, Lcom/fmm/dm/eng/core/XDMEncoder;

    invoke-direct {v2}, Lcom/fmm/dm/eng/core/XDMEncoder;-><init>()V

    .line 130
    .local v2, "encoder":Lcom/fmm/dm/eng/core/XDMEncoder;
    const/16 v8, 0x800

    new-array v0, v8, [B

    .line 131
    .local v0, "buffer":[B
    const/4 v8, 0x2

    new-array v5, v8, [I

    .line 133
    .local v5, "odataSize":[I
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 135
    .local v6, "out":Ljava/io/ByteArrayOutputStream;
    new-instance v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v7}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    .line 137
    .local v7, "pcdata":Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    invoke-virtual {v2, v6, p1, v5}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncDevinf2Opaque(Ljava/io/ByteArrayOutputStream;Lcom/fmm/dm/adapter/XDMDevinfAdapter;[I)[B

    move-result-object v0

    .line 139
    const/4 v8, 0x1

    iput v8, v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    .line 140
    const/4 v8, 0x0

    aget v8, v5, v8

    iput v8, v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 141
    array-length v8, v0

    new-array v8, v8, [C

    iput-object v8, v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 143
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v8, v0

    if-lt v3, v8, :cond_1

    .line 146
    iput-object v7, v4, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 148
    new-instance v8, Lcom/fmm/dm/eng/core/XDMList;

    invoke-direct {v8}, Lcom/fmm/dm/eng/core/XDMList;-><init>()V

    iput-object v8, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 152
    if-eqz v6, :cond_0

    .line 154
    :try_start_0
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :cond_0
    :goto_1
    return-object p0

    .line 144
    :cond_1
    iget-object v8, v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    aget-byte v9, v0, v3

    int-to-char v9, v9

    aput-char v9, v8, v3

    .line 143
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 157
    :catch_0
    move-exception v1

    .line 159
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public xdmParParseResults(Lcom/fmm/dm/eng/parser/XDMParser;)I
    .locals 5
    .param p1, "p"    # Lcom/fmm/dm/eng/parser/XDMParser;

    .prologue
    const/4 v3, 0x0

    .line 30
    const/4 v1, -0x1

    .line 31
    .local v1, "id":I
    const/4 v2, 0x0

    .line 33
    .local v2, "res":I
    const/16 v4, 0x22

    invoke-virtual {p1, v4}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 35
    if-eqz v2, :cond_1

    move v3, v2

    .line 123
    :cond_0
    :goto_0
    return v3

    .line 40
    :cond_1
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 41
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 45
    if-eqz v2, :cond_2

    .line 47
    const-string v3, "not WBXML_ERR_OK"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 48
    goto :goto_0

    .line 55
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 62
    :goto_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_3

    .line 64
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 65
    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 68
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v1, :sswitch_data_0

    .line 113
    const/4 v2, 0x2

    .line 117
    :goto_2
    if-eqz v2, :cond_2

    move v3, v2

    .line 119
    goto :goto_0

    .line 71
    :sswitch_0
    invoke-virtual {p1, v1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 72
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->cmdid:I

    goto :goto_2

    .line 76
    :sswitch_1
    invoke-virtual {p1, v1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 77
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    goto :goto_2

    .line 81
    :sswitch_2
    invoke-virtual {p1, v1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 82
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    goto :goto_2

    .line 86
    :sswitch_3
    new-instance v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v4}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 87
    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-virtual {v4, p1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v2

    .line 88
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->Meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 92
    :sswitch_4
    invoke-virtual {p1, v1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 93
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    goto :goto_2

    .line 97
    :sswitch_5
    invoke-virtual {p1, v1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 98
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    goto :goto_2

    .line 102
    :sswitch_6
    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-virtual {p1, v4}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_2

    .line 106
    :sswitch_7
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 107
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 109
    iput v1, p1, Lcom/fmm/dm/eng/parser/XDMParser;->codePage:I

    goto :goto_2

    .line 68
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_7
        0xb -> :sswitch_0
        0xc -> :sswitch_2
        0x14 -> :sswitch_6
        0x1a -> :sswitch_3
        0x1c -> :sswitch_1
        0x28 -> :sswitch_5
        0x2f -> :sswitch_4
    .end sparse-switch
.end method
