.class public Lcom/fmm/dm/eng/core/XDMMem;
.super Ljava/lang/Object;
.source "XDMMem.java"


# static fields
.field private static final HEX_DIGITS:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/fmm/dm/eng/core/XDMMem;->HEX_DIGITS:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmLibBytesToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    .line 305
    if-nez p0, :cond_0

    .line 306
    const/4 v3, 0x0

    .line 323
    :goto_0
    return-object v3

    .line 308
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 310
    .local v2, "ret":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p0

    if-lt v1, v3, :cond_1

    .line 323
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 314
    :cond_1
    aget-byte v3, p0, v1

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v0, v3, 0xf

    .line 316
    .local v0, "b":I
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 318
    aget-byte v3, p0, v1

    and-int/lit8 v0, v3, 0xf

    .line 320
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 310
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static xdmLibCharToString([C)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # [C

    .prologue
    .line 210
    const/4 v1, 0x0

    .line 211
    .local v1, "i":I
    const/4 v0, 0x0

    .line 212
    .local v0, "buf":[C
    array-length v3, p0

    if-gtz v3, :cond_1

    .line 213
    const/4 v3, 0x0

    .line 226
    :goto_0
    return-object v3

    .line 216
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 214
    :cond_1
    aget-char v3, p0, v1

    if-eqz v3, :cond_2

    array-length v3, p0

    if-gt v3, v1, :cond_0

    .line 219
    :cond_2
    new-array v0, v1, [C

    .line 221
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_1
    if-lt v2, v1, :cond_3

    .line 226
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 223
    :cond_3
    aget-char v3, p0, v2

    aput-char v3, v0, v2

    .line 221
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method static xdmLibHexCharToInt(C)I
    .locals 3
    .param p0, "c"    # C

    .prologue
    .line 287
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 288
    add-int/lit8 v0, p0, -0x30

    .line 292
    :goto_0
    return v0

    .line 289
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 290
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 291
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 292
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 294
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid hex char \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static xdmLibHexStringToBytes(Ljava/lang/String;)[B
    .locals 6
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 270
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 271
    const/4 v1, 0x0

    .line 282
    :cond_0
    return-object v1

    .line 273
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 275
    .local v2, "sz":I
    div-int/lit8 v3, v2, 0x2

    new-array v1, v3, [B

    .line 277
    .local v1, "ret":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 279
    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibHexCharToInt(C)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibHexCharToInt(C)I

    move-result v5

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 277
    add-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method public static xdmLibHexToChar(I)I
    .locals 1
    .param p0, "nHex"    # I

    .prologue
    .line 175
    if-ltz p0, :cond_0

    const/16 v0, 0x9

    if-gt p0, v0, :cond_0

    .line 176
    add-int/lit8 v0, p0, 0x30

    .line 180
    :goto_0
    return v0

    .line 177
    :cond_0
    const/16 v0, 0xa

    if-gt v0, p0, :cond_1

    const/16 v0, 0xf

    if-gt p0, v0, :cond_1

    .line 178
    add-int/lit8 v0, p0, 0x41

    add-int/lit8 v0, v0, -0xa

    goto :goto_0

    .line 180
    :cond_1
    const/16 v0, 0x3f

    goto :goto_0
.end method

.method public static xdmLibIsAlpha(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 246
    invoke-static {p0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v0

    return v0
.end method

.method public static xdmLibIsAlphaNum(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 256
    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    return v0
.end method

.method public static xdmLibIsNum(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 236
    invoke-static {p0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    return v0
.end method

.method public static xdmLibIsSpace(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 170
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmLibMemcpy(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p0, "szDest"    # Ljava/lang/String;
    .param p1, "szSrc"    # Ljava/lang/String;
    .param p2, "len"    # I

    .prologue
    .line 97
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 98
    .local v0, "buf1":[C
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 101
    .local v1, "buf2":[C
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-static {v1, v3, v0, v4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v2

    .line 105
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmLibMemcpy([B[BI)V
    .locals 5
    .param p0, "dest"    # [B
    .param p1, "src"    # [B
    .param p2, "len"    # I

    .prologue
    .line 111
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 112
    .local v0, "buf1":[C
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 115
    .local v1, "buf2":[C
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-static {v1, v3, v0, v4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-void

    .line 117
    :catch_0
    move-exception v2

    .line 119
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmLibReplaceString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "szSrc"    # Ljava/lang/String;
    .param p1, "szBefore"    # Ljava/lang/String;
    .param p2, "szAfter"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 371
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 393
    .end local p0    # "szSrc":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 376
    .restart local p0    # "szSrc":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 378
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 379
    .local v1, "index":I
    const/4 v0, 0x0

    .line 380
    .local v0, "begine":I
    :goto_1
    if-ne v1, v4, :cond_3

    .line 388
    if-ne v1, v4, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v0, v3, :cond_2

    .line 390
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 382
    :cond_3
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int v0, v1, v3

    .line 385
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    goto :goto_1
.end method

.method public static xdmLibStrchr(Ljava/lang/String;C)Ljava/lang/String;
    .locals 3
    .param p0, "szSource"    # Ljava/lang/String;
    .param p1, "c"    # C

    .prologue
    const/4 v1, 0x0

    .line 76
    const/4 v0, 0x0

    .line 78
    .local v0, "index":I
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-object v1

    .line 81
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 83
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 86
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static xdmLibStrncmp(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 3
    .param p0, "szData1"    # Ljava/lang/String;
    .param p1, "szData2"    # Ljava/lang/String;
    .param p2, "size"    # I

    .prologue
    const/4 v1, 0x0

    .line 134
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge p2, v2, :cond_0

    .line 135
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "szTmp":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 144
    :goto_1
    return v1

    .line 137
    .end local v0    # "szTmp":Ljava/lang/String;
    :cond_0
    move-object v0, p0

    .restart local v0    # "szTmp":Ljava/lang/String;
    goto :goto_0

    .line 144
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static xdmLibStrrchr(Ljava/lang/String;C)Ljava/lang/String;
    .locals 2
    .param p0, "szSource"    # Ljava/lang/String;
    .param p1, "c"    # C

    .prologue
    .line 155
    const/4 v0, 0x0

    .line 157
    .local v0, "index":I
    invoke-virtual {p0, p1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 159
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static xdmLibStrsplit([CC[C)Ljava/lang/String;
    .locals 5
    .param p0, "str"    # [C
    .param p1, "delim"    # C
    .param p2, "out"    # [C

    .prologue
    const/4 v3, 0x0

    .line 20
    const/4 v0, 0x0

    .line 22
    .local v0, "i":I
    if-nez p0, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-object v3

    .line 26
    :cond_1
    array-length v4, p0

    if-eqz v4, :cond_0

    .line 31
    :goto_1
    array-length v4, p0

    if-ge v0, v4, :cond_0

    .line 33
    aget-char v4, p0, v0

    if-ne v4, p1, :cond_3

    .line 35
    const/4 v3, 0x0

    aput-char v3, p2, v0

    .line 36
    array-length v3, p0

    add-int/lit8 v4, v0, 0x1

    sub-int/2addr v3, v4

    new-array v2, v3, [C

    .line 37
    .local v2, "t":[C
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_2
    array-length v3, p0

    add-int/lit8 v4, v0, 0x1

    sub-int/2addr v3, v4

    if-lt v1, v3, :cond_2

    .line 39
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 38
    :cond_2
    add-int v3, v1, v0

    add-int/lit8 v3, v3, 0x1

    aget-char v3, p0, v3

    aput-char v3, v2, v1

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 41
    .end local v1    # "n":I
    .end local v2    # "t":[C
    :cond_3
    aget-char v4, p0, v0

    aput-char v4, p2, v0

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "szSource"    # Ljava/lang/String;
    .param p1, "szToken"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 55
    const/4 v0, 0x0

    .line 57
    .local v0, "index":I
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-object v1

    .line 60
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 62
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 65
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static xdmLibToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0, "buf"    # [B

    .prologue
    .line 328
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 329
    .local v1, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-lt v0, v2, :cond_0

    .line 333
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 331
    :cond_0
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    add-int/lit16 v2, v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static xdmLibToHexString([BII)Ljava/lang/String;
    .locals 8
    .param p0, "array"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 338
    mul-int/lit8 v6, p2, 0x2

    new-array v1, v6, [C

    .line 340
    .local v1, "buf":[C
    const/4 v2, 0x0

    .local v2, "bufIndex":I
    const/4 v4, 0x0

    .line 341
    .local v4, "i":I
    const/4 v5, 0x0

    .line 343
    .local v5, "szBuf":Ljava/lang/String;
    move v4, p1

    move v3, v2

    .end local v2    # "bufIndex":I
    .local v3, "bufIndex":I
    :goto_0
    add-int v6, p1, p2

    if-lt v4, v6, :cond_1

    .line 351
    const/4 v4, 0x0

    :goto_1
    array-length v6, v1

    if-lt v4, v6, :cond_2

    .line 358
    :cond_0
    array-length v6, v1

    if-ne v4, v6, :cond_3

    .line 360
    new-instance v5, Ljava/lang/String;

    .end local v5    # "szBuf":Ljava/lang/String;
    const-string v6, "0"

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 366
    .restart local v5    # "szBuf":Ljava/lang/String;
    :goto_2
    return-object v5

    .line 345
    :cond_1
    aget-byte v0, p0, v4

    .line 346
    .local v0, "b":B
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "bufIndex":I
    .restart local v2    # "bufIndex":I
    sget-object v6, Lcom/fmm/dm/eng/core/XDMMem;->HEX_DIGITS:[C

    ushr-int/lit8 v7, v0, 0x4

    and-int/lit8 v7, v7, 0xf

    aget-char v6, v6, v7

    aput-char v6, v1, v3

    .line 347
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "bufIndex":I
    .restart local v3    # "bufIndex":I
    sget-object v6, Lcom/fmm/dm/eng/core/XDMMem;->HEX_DIGITS:[C

    and-int/lit8 v7, v0, 0xf

    aget-char v6, v6, v7

    aput-char v6, v1, v2

    .line 343
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 353
    .end local v0    # "b":B
    :cond_2
    aget-char v6, v1, v4

    const/16 v7, 0x30

    if-gt v6, v7, :cond_0

    .line 351
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 364
    :cond_3
    array-length v6, v1

    sub-int/2addr v6, v4

    invoke-static {v1, v4, v6}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method public static xdmLibToLower(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 200
    invoke-static {p0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    return v0
.end method

.method public static xdmLibToUpper(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 190
    invoke-static {p0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    return v0
.end method
