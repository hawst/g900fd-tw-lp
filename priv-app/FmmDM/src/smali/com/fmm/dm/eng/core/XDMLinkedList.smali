.class public Lcom/fmm/dm/eng/core/XDMLinkedList;
.super Ljava/lang/Object;
.source "XDMLinkedList.java"


# instance fields
.field public count:I

.field public cur:Lcom/fmm/dm/eng/core/XDMNode;

.field public top:Lcom/fmm/dm/eng/core/XDMNode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmListAddObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V
    .locals 3
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 104
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 105
    .local v1, "top":Lcom/fmm/dm/eng/core/XDMNode;
    new-instance v0, Lcom/fmm/dm/eng/core/XDMNode;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMNode;-><init>()V

    .line 107
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMNode;
    invoke-static {v0, p1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListBindObjectToNode(Lcom/fmm/dm/eng/core/XDMNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 110
    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 111
    iput-object v0, v1, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 112
    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v0, v2, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 114
    iget v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    .line 115
    return-void
.end method

.method public static xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V
    .locals 3
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 124
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 125
    .local v1, "top":Lcom/fmm/dm/eng/core/XDMNode;
    new-instance v0, Lcom/fmm/dm/eng/core/XDMNode;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMNode;-><init>()V

    .line 127
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMNode;
    invoke-static {v0, p1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListBindObjectToNode(Lcom/fmm/dm/eng/core/XDMNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    if-eqz v0, :cond_0

    .line 131
    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 132
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 133
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v0, v2, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 134
    iput-object v0, v1, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 136
    iget v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    .line 138
    :cond_0
    return-void
.end method

.method public static xdmListBindObjectToNode(Lcom/fmm/dm/eng/core/XDMNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "node"    # Lcom/fmm/dm/eng/core/XDMNode;
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 245
    const/4 v0, 0x0

    .line 247
    .local v0, "ret":Ljava/lang/Object;
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 249
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    .line 250
    iput-object p1, p0, Lcom/fmm/dm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    .line 253
    .end local v0    # "ret":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public static xdmListClearLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V
    .locals 4
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 66
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 67
    .local v2, "top":Lcom/fmm/dm/eng/core/XDMNode;
    iget-object v0, v2, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 71
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMNode;
    :goto_0
    if-ne v0, v2, :cond_0

    .line 78
    iput-object v2, v2, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 79
    iput-object v2, v2, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 80
    iput-object v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 82
    const/4 v3, 0x0

    iput v3, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    .line 83
    return-void

    .line 73
    :cond_0
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 74
    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/fmm/dm/eng/core/XDMNode;)Ljava/lang/Object;

    move-result-object v1

    .line 75
    .local v1, "obj":Ljava/lang/Object;
    goto :goto_0
.end method

.method public static xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x0

    .line 18
    .local v0, "list":Lcom/fmm/dm/eng/core/XDMLinkedList;
    const/4 v1, 0x0

    .line 20
    .local v1, "node":Lcom/fmm/dm/eng/core/XDMNode;
    new-instance v0, Lcom/fmm/dm/eng/core/XDMLinkedList;

    .end local v0    # "list":Lcom/fmm/dm/eng/core/XDMLinkedList;
    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;-><init>()V

    .line 22
    .restart local v0    # "list":Lcom/fmm/dm/eng/core/XDMLinkedList;
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateNodeFromMemory()Lcom/fmm/dm/eng/core/XDMNode;

    move-result-object v1

    .line 23
    if-nez v1, :cond_0

    .line 25
    const-string v2, "Create node memory alloc failed"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 26
    const/4 v0, 0x0

    .line 27
    const/4 v0, 0x0

    .line 36
    .end local v0    # "list":Lcom/fmm/dm/eng/core/XDMLinkedList;
    :goto_0
    return-object v0

    .line 30
    .restart local v0    # "list":Lcom/fmm/dm/eng/core/XDMLinkedList;
    :cond_0
    iput-object v1, v1, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 31
    iput-object v1, v1, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 33
    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 34
    const/4 v2, 0x0

    iput v2, v0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    goto :goto_0
.end method

.method public static xdmListCreateNodeFromMemory()Lcom/fmm/dm/eng/core/XDMNode;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 92
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMNode;
    new-instance v0, Lcom/fmm/dm/eng/core/XDMNode;

    .end local v0    # "node":Lcom/fmm/dm/eng/core/XDMNode;
    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMNode;-><init>()V

    .line 94
    .restart local v0    # "node":Lcom/fmm/dm/eng/core/XDMNode;
    return-object v0
.end method

.method public static xdmListFreeLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V
    .locals 3
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 45
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 46
    .local v2, "top":Lcom/fmm/dm/eng/core/XDMNode;
    iget-object v0, v2, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 50
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMNode;
    :goto_0
    if-ne v0, v2, :cond_0

    .line 55
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/fmm/dm/eng/core/XDMNode;)Ljava/lang/Object;

    move-result-object v1

    .line 56
    .local v1, "pObj":Ljava/lang/Object;
    const/4 v1, 0x0

    .line 57
    const/4 p0, 0x0

    .line 58
    return-void

    .line 52
    .end local v1    # "pObj":Ljava/lang/Object;
    :cond_0
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    goto :goto_0
.end method

.method public static xdmListFreeNodeFromMemory(Lcom/fmm/dm/eng/core/XDMNode;)Ljava/lang/Object;
    .locals 1
    .param p0, "node"    # Lcom/fmm/dm/eng/core/XDMNode;

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    .local v0, "ret":Ljava/lang/Object;
    if-eqz p0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    .line 267
    const/4 p0, 0x0

    .line 269
    .end local v0    # "ret":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public static xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;
    .locals 2
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->cur:Lcom/fmm/dm/eng/core/XDMNode;

    .line 300
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMNode;
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    if-ne v0, v1, :cond_0

    .line 302
    const/4 v1, 0x0

    .line 309
    :goto_0
    return-object v1

    .line 306
    :cond_0
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 308
    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->cur:Lcom/fmm/dm/eng/core/XDMNode;

    .line 309
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    goto :goto_0
.end method

.method public static xdmListGetObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)Ljava/lang/Object;
    .locals 4
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;
    .param p1, "idx"    # I

    .prologue
    .line 148
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 149
    .local v2, "top":Lcom/fmm/dm/eng/core/XDMNode;
    move-object v0, v2

    .line 151
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMNode;
    iget v3, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    if-ge p1, v3, :cond_0

    if-gez p1, :cond_2

    .line 153
    :cond_0
    const/4 v3, 0x0

    .line 161
    :goto_0
    return-object v3

    .line 158
    :cond_1
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    move v1, p1

    .line 156
    .end local p1    # "idx":I
    .local v1, "idx":I
    :goto_1
    add-int/lit8 p1, v1, -0x1

    .end local v1    # "idx":I
    .restart local p1    # "idx":I
    if-gez v1, :cond_1

    .line 161
    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    move v1, p1

    .end local p1    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_1
.end method

.method public static xdmListRemoveObjAt(Lcom/fmm/dm/eng/core/XDMLinkedList;I)Ljava/lang/Object;
    .locals 6
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;
    .param p1, "idx"    # I

    .prologue
    .line 204
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 205
    .local v3, "top":Lcom/fmm/dm/eng/core/XDMNode;
    move-object v0, v3

    .line 208
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMNode;
    iget v4, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    if-ge p1, v4, :cond_0

    if-gez p1, :cond_2

    .line 210
    :cond_0
    const/4 v2, 0x0

    .line 224
    :goto_0
    return-object v2

    .line 215
    :cond_1
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    move v1, p1

    .line 213
    .end local p1    # "idx":I
    .local v1, "idx":I
    :goto_1
    add-int/lit8 p1, v1, -0x1

    .end local v1    # "idx":I
    .restart local p1    # "idx":I
    if-gez v1, :cond_1

    .line 218
    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 219
    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 221
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/fmm/dm/eng/core/XDMNode;)Ljava/lang/Object;

    move-result-object v2

    .line 222
    .local v2, "obj":Ljava/lang/Object;
    iget v4, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    goto :goto_0

    .end local v2    # "obj":Ljava/lang/Object;
    :cond_2
    move v1, p1

    .end local p1    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_1
.end method

.method public static xdmListRemoveObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;
    .locals 1
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 234
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemoveObjAt(Lcom/fmm/dm/eng/core/XDMLinkedList;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static xdmListRemovePreviousObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;
    .locals 3
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->cur:Lcom/fmm/dm/eng/core/XDMNode;

    .line 320
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMNode;
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    if-eq v1, v2, :cond_0

    .line 322
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 324
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 325
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 327
    iget v1, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    .line 328
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/fmm/dm/eng/core/XDMNode;)Ljava/lang/Object;

    move-result-object v1

    .line 331
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V
    .locals 3
    .param p0, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;
    .param p1, "idx"    # I

    .prologue
    .line 279
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 280
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMNode;
    if-ltz p1, :cond_0

    iget v2, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    if-ge p1, v2, :cond_0

    move v1, p1

    .line 282
    .end local p1    # "idx":I
    .local v1, "idx":I
    :goto_0
    add-int/lit8 p1, v1, -0x1

    .end local v1    # "idx":I
    .restart local p1    # "idx":I
    if-gez v1, :cond_1

    .line 288
    :cond_0
    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMLinkedList;->cur:Lcom/fmm/dm/eng/core/XDMNode;

    .line 289
    return-void

    .line 284
    :cond_1
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    move v1, p1

    .end local p1    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_0
.end method


# virtual methods
.method public xdmListRemoveObj(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 5
    .param p1, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "size"    # I

    .prologue
    .line 173
    iget-object v2, p1, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 174
    .local v2, "top":Lcom/fmm/dm/eng/core/XDMNode;
    iget-object v0, v2, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 175
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMNode;
    const/4 v1, 0x0

    .line 177
    .local v1, "ret":Ljava/lang/Object;
    :goto_0
    if-ne v0, v2, :cond_0

    .line 193
    .end local v1    # "ret":Ljava/lang/Object;
    :goto_1
    return-object v1

    .line 179
    .restart local v1    # "ret":Ljava/lang/Object;
    :cond_0
    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 181
    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v4, v3, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 182
    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v4, v3, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 183
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/fmm/dm/eng/core/XDMNode;)Ljava/lang/Object;

    move-result-object v1

    .line 184
    iget v3, p1, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p1, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    goto :goto_1

    .line 189
    :cond_1
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    goto :goto_0
.end method
