.class public Lcom/fmm/dm/eng/parser/XDMParserAtomic;
.super Lcom/fmm/dm/agent/XDMHandleCmd;
.source "XDMParserAtomic.java"

# interfaces
.implements Lcom/fmm/dm/eng/core/XDMWbxml;


# instance fields
.field public cmdid:I

.field public is_noresp:I

.field public itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

.field public meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMHandleCmd;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParParseAtomic(Lcom/fmm/dm/eng/parser/XDMParser;)I
    .locals 14
    .param p1, "p"    # Lcom/fmm/dm/eng/parser/XDMParser;

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x0

    .line 24
    const/4 v6, -0x1

    .line 25
    .local v6, "id":I
    const/4 v9, 0x0

    .line 26
    .local v9, "res":I
    const/4 v1, 0x1

    .line 28
    .local v1, "call_start_atomic":Z
    invoke-virtual {p1, v13}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v9

    .line 29
    if-eqz v9, :cond_1

    move v12, v9

    .line 195
    :cond_0
    :goto_0
    return v12

    .line 34
    :cond_1
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v9

    .line 35
    if-eq v9, v13, :cond_0

    .line 39
    if-eqz v9, :cond_2

    .line 41
    const-string v12, "not WBXML_ERR_OK"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v12, v9

    .line 42
    goto :goto_0

    .line 49
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 56
    :goto_1
    const/4 v13, 0x1

    if-ne v6, v13, :cond_4

    .line 58
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    .line 188
    if-eqz v1, :cond_3

    .line 190
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 191
    const/4 v1, 0x0

    .line 194
    :cond_3
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicEnd(Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :catch_0
    move-exception v4

    .line 53
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 62
    .end local v4    # "e":Ljava/io/IOException;
    :cond_4
    sparse-switch v6, :sswitch_data_0

    .line 178
    const/4 v9, 0x2

    .line 182
    :goto_2
    if-eqz v9, :cond_2

    move v12, v9

    .line 184
    goto :goto_0

    .line 66
    :sswitch_0
    invoke-virtual {p1, v6}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v9

    .line 67
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    iput v13, p0, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->cmdid:I

    goto :goto_2

    .line 71
    :sswitch_1
    invoke-virtual {p1, v6}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v13

    iput v13, p0, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->is_noresp:I

    goto :goto_2

    .line 75
    :sswitch_2
    new-instance v13, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v13}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v13, p0, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 76
    iget-object v13, p0, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-virtual {v13, p1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 77
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->Meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v13, p0, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 81
    :sswitch_3
    if-eqz v1, :cond_5

    .line 83
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 84
    const/4 v1, 0x0

    .line 87
    :cond_5
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserAdd;-><init>()V

    .line 88
    .local v0, "add":Lcom/fmm/dm/eng/parser/XDMParserAdd;
    invoke-virtual {v0, p1}, Lcom/fmm/dm/eng/parser/XDMParserAdd;->xdmParParseAdd(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 89
    goto :goto_2

    .line 91
    .end local v0    # "add":Lcom/fmm/dm/eng/parser/XDMParserAdd;
    :sswitch_4
    if-eqz v1, :cond_6

    .line 93
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 94
    const/4 v1, 0x0

    .line 97
    :cond_6
    new-instance v3, Lcom/fmm/dm/eng/parser/XDMParserDelete;

    invoke-direct {v3}, Lcom/fmm/dm/eng/parser/XDMParserDelete;-><init>()V

    .line 98
    .local v3, "delete":Lcom/fmm/dm/eng/parser/XDMParserDelete;
    invoke-virtual {v3, p1}, Lcom/fmm/dm/eng/parser/XDMParserDelete;->xdmParParseDelete(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 99
    goto :goto_2

    .line 101
    .end local v3    # "delete":Lcom/fmm/dm/eng/parser/XDMParserDelete;
    :sswitch_5
    if-eqz v1, :cond_7

    .line 103
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 104
    const/4 v1, 0x0

    .line 106
    :cond_7
    new-instance v5, Lcom/fmm/dm/eng/parser/XDMParserExec;

    invoke-direct {v5}, Lcom/fmm/dm/eng/parser/XDMParserExec;-><init>()V

    .line 107
    .local v5, "exec":Lcom/fmm/dm/eng/parser/XDMParserExec;
    invoke-virtual {v5, p1}, Lcom/fmm/dm/eng/parser/XDMParserExec;->xdmParParseExec(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 108
    goto :goto_2

    .line 110
    .end local v5    # "exec":Lcom/fmm/dm/eng/parser/XDMParserExec;
    :sswitch_6
    if-eqz v1, :cond_8

    .line 112
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 113
    const/4 v1, 0x0

    .line 115
    :cond_8
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserCopy;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserCopy;-><init>()V

    .line 116
    .local v2, "copy":Lcom/fmm/dm/eng/parser/XDMParserCopy;
    invoke-virtual {v2, p1}, Lcom/fmm/dm/eng/parser/XDMParserCopy;->xdmParParseCopy(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 117
    goto :goto_2

    .line 119
    .end local v2    # "copy":Lcom/fmm/dm/eng/parser/XDMParserCopy;
    :sswitch_7
    if-eqz v1, :cond_9

    .line 121
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 122
    const/4 v1, 0x0

    .line 125
    :cond_9
    invoke-virtual {p0, p1}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmParParseAtomic(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 126
    goto :goto_2

    .line 128
    :sswitch_8
    if-eqz v1, :cond_a

    .line 130
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 131
    const/4 v1, 0x0

    .line 134
    :cond_a
    new-instance v7, Lcom/fmm/dm/eng/parser/XDMParserMap;

    invoke-direct {v7}, Lcom/fmm/dm/eng/parser/XDMParserMap;-><init>()V

    .line 135
    .local v7, "map":Lcom/fmm/dm/eng/parser/XDMParserMap;
    invoke-virtual {v7, p1}, Lcom/fmm/dm/eng/parser/XDMParserMap;->xdmParParseMap(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 136
    goto/16 :goto_2

    .line 139
    .end local v7    # "map":Lcom/fmm/dm/eng/parser/XDMParserMap;
    :sswitch_9
    if-eqz v1, :cond_b

    .line 141
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 142
    const/4 v1, 0x0

    .line 145
    :cond_b
    new-instance v8, Lcom/fmm/dm/eng/parser/XDMParserReplace;

    invoke-direct {v8}, Lcom/fmm/dm/eng/parser/XDMParserReplace;-><init>()V

    .line 146
    .local v8, "replace":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    invoke-virtual {v8, p1}, Lcom/fmm/dm/eng/parser/XDMParserReplace;->xdmParParseReplace(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 147
    goto/16 :goto_2

    .line 150
    .end local v8    # "replace":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    :sswitch_a
    if-eqz v1, :cond_c

    .line 152
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 153
    const/4 v1, 0x0

    .line 155
    :cond_c
    new-instance v10, Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-direct {v10}, Lcom/fmm/dm/eng/parser/XDMParserSequence;-><init>()V

    .line 156
    .local v10, "sequence":Lcom/fmm/dm/eng/parser/XDMParserSequence;
    invoke-virtual {v10, p1}, Lcom/fmm/dm/eng/parser/XDMParserSequence;->xdmParParseSequence(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 157
    goto/16 :goto_2

    .line 160
    .end local v10    # "sequence":Lcom/fmm/dm/eng/parser/XDMParserSequence;
    :sswitch_b
    if-eqz v1, :cond_d

    .line 162
    iget-object v13, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v13, p0}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 163
    const/4 v1, 0x0

    .line 166
    :cond_d
    new-instance v11, Lcom/fmm/dm/eng/parser/XDMParserSync;

    invoke-direct {v11}, Lcom/fmm/dm/eng/parser/XDMParserSync;-><init>()V

    .line 167
    .local v11, "sync":Lcom/fmm/dm/eng/parser/XDMParserSync;
    invoke-virtual {v11, p1}, Lcom/fmm/dm/eng/parser/XDMParserSync;->xdmParParseSync(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v9

    .line 168
    goto/16 :goto_2

    .line 171
    .end local v11    # "sync":Lcom/fmm/dm/eng/parser/XDMParserSync;
    :sswitch_c
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v6

    .line 172
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v6

    .line 174
    iput v6, p1, Lcom/fmm/dm/eng/parser/XDMParser;->codePage:I

    goto/16 :goto_2

    .line 62
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_c
        0x5 -> :sswitch_3
        0x8 -> :sswitch_7
        0xb -> :sswitch_0
        0xd -> :sswitch_6
        0x10 -> :sswitch_4
        0x11 -> :sswitch_5
        0x18 -> :sswitch_8
        0x1a -> :sswitch_2
        0x1d -> :sswitch_1
        0x20 -> :sswitch_9
        0x24 -> :sswitch_a
        0x2a -> :sswitch_b
    .end sparse-switch
.end method
