.class public Lcom/fmm/dm/eng/core/XDMOmList;
.super Ljava/lang/Object;
.source "XDMOmList.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMInterface;


# instance fields
.field public data:Ljava/lang/Object;

.field public next:Lcom/fmm/dm/eng/core/XDMOmList;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmOmDeleteAclList(Lcom/fmm/dm/eng/core/XDMOmList;)V
    .locals 3
    .param p0, "h"    # Lcom/fmm/dm/eng/core/XDMOmList;

    .prologue
    .line 17
    move-object v1, p0

    .line 19
    .local v1, "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    const/4 v0, 0x0

    .line 21
    .local v0, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    :goto_0
    if-nez v1, :cond_0

    .line 30
    return-void

    .line 23
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 24
    .local v2, "next":Lcom/fmm/dm/eng/core/XDMOmList;
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 26
    .restart local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    const/4 v0, 0x0

    .line 27
    const/4 v1, 0x0

    .line 28
    move-object v1, v2

    goto :goto_0
.end method

.method public static xdmOmGetFormatFromString(Ljava/lang/String;)I
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v0, "b64"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x1

    .line 136
    :goto_0
    return v0

    .line 114
    :cond_0
    const-string v0, "bin"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 115
    const/4 v0, 0x2

    goto :goto_0

    .line 116
    :cond_1
    const-string v0, "bool"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 117
    const/4 v0, 0x3

    goto :goto_0

    .line 118
    :cond_2
    const-string v0, "chr"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 119
    const/4 v0, 0x4

    goto :goto_0

    .line 120
    :cond_3
    const-string v0, "int"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 121
    const/4 v0, 0x5

    goto :goto_0

    .line 122
    :cond_4
    const-string v0, "node"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 123
    const/4 v0, 0x6

    goto :goto_0

    .line 124
    :cond_5
    const-string v0, "null"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 125
    const/4 v0, 0x7

    goto :goto_0

    .line 126
    :cond_6
    const-string v0, "xml"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    .line 127
    const/16 v0, 0x8

    goto :goto_0

    .line 128
    :cond_7
    const-string v0, "float"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 129
    const/16 v0, 0x9

    goto :goto_0

    .line 130
    :cond_8
    const-string v0, "time"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 131
    const/16 v0, 0xb

    goto :goto_0

    .line 132
    :cond_9
    const-string v0, "date"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    .line 133
    const/16 v0, 0xa

    goto :goto_0

    .line 136
    :cond_a
    const/16 v0, 0xc

    goto :goto_0
.end method

.method public static xdmOmGetFormatString(I)Ljava/lang/String;
    .locals 1
    .param p0, "szFormat"    # I

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 62
    .local v0, "szOutbuf":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 102
    :goto_0
    return-object v0

    .line 65
    :pswitch_0
    const-string v0, "b64"

    .line 66
    goto :goto_0

    .line 68
    :pswitch_1
    const-string v0, "bin"

    .line 69
    goto :goto_0

    .line 71
    :pswitch_2
    const-string v0, "bool"

    .line 72
    goto :goto_0

    .line 74
    :pswitch_3
    const-string v0, "chr"

    .line 75
    goto :goto_0

    .line 77
    :pswitch_4
    const-string v0, "int"

    .line 78
    goto :goto_0

    .line 80
    :pswitch_5
    const-string v0, "node"

    .line 81
    goto :goto_0

    .line 83
    :pswitch_6
    const-string v0, "null"

    .line 84
    goto :goto_0

    .line 86
    :pswitch_7
    const-string v0, "xml"

    .line 87
    goto :goto_0

    .line 89
    :pswitch_8
    const-string v0, "float"

    .line 90
    goto :goto_0

    .line 92
    :pswitch_9
    const-string v0, "time"

    .line 93
    goto :goto_0

    .line 95
    :pswitch_a
    const-string v0, "date"

    .line 96
    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public xdmOmDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V
    .locals 3
    .param p1, "h"    # Lcom/fmm/dm/eng/core/XDMOmList;

    .prologue
    .line 41
    move-object v0, p1

    .line 43
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    :goto_0
    if-nez v0, :cond_0

    .line 51
    return-void

    .line 45
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 47
    .local v1, "next":Lcom/fmm/dm/eng/core/XDMOmList;
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 48
    const/4 v0, 0x0

    .line 49
    move-object v0, v1

    goto :goto_0
.end method
