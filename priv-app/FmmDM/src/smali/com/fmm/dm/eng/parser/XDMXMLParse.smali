.class public Lcom/fmm/dm/eng/parser/XDMXMLParse;
.super Ljava/lang/Object;
.source "XDMXMLParse.java"


# direct methods
.method public constructor <init>(Lorg/xml/sax/helpers/DefaultHandler;Ljava/lang/String;)V
    .locals 7
    .param p1, "handler"    # Lorg/xml/sax/helpers/DefaultHandler;
    .param p2, "szData"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v2, 0x0

    .line 21
    .local v2, "inputSt":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    .line 22
    .local v1, "factory":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v4

    .line 23
    .local v4, "parser":Ljavax/xml/parsers/SAXParser;
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljavax/xml/parsers/SAXParserFactory;->setValidating(Z)V

    .line 25
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    .end local v2    # "inputSt":Ljava/io/InputStream;
    .local v3, "inputSt":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v4, v3, p1}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 37
    if-eqz v3, :cond_2

    .line 40
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v2, v3

    .line 47
    .end local v1    # "factory":Ljavax/xml/parsers/SAXParserFactory;
    .end local v3    # "inputSt":Ljava/io/InputStream;
    .end local v4    # "parser":Ljavax/xml/parsers/SAXParser;
    .restart local v2    # "inputSt":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    .line 32
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 37
    if-eqz v2, :cond_0

    .line 40
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 42
    :catch_1
    move-exception v0

    .line 44
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 35
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 37
    :goto_2
    if-eqz v2, :cond_1

    .line 40
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 46
    :cond_1
    :goto_3
    throw v5

    .line 42
    :catch_2
    move-exception v0

    .line 44
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 42
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "inputSt":Ljava/io/InputStream;
    .restart local v1    # "factory":Ljavax/xml/parsers/SAXParserFactory;
    .restart local v3    # "inputSt":Ljava/io/InputStream;
    .restart local v4    # "parser":Ljavax/xml/parsers/SAXParser;
    :catch_3
    move-exception v0

    .line 44
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    move-object v2, v3

    .end local v3    # "inputSt":Ljava/io/InputStream;
    .restart local v2    # "inputSt":Ljava/io/InputStream;
    goto :goto_0

    .line 35
    .end local v2    # "inputSt":Ljava/io/InputStream;
    .restart local v3    # "inputSt":Ljava/io/InputStream;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "inputSt":Ljava/io/InputStream;
    .restart local v2    # "inputSt":Ljava/io/InputStream;
    goto :goto_2

    .line 30
    .end local v2    # "inputSt":Ljava/io/InputStream;
    .restart local v3    # "inputSt":Ljava/io/InputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "inputSt":Ljava/io/InputStream;
    .restart local v2    # "inputSt":Ljava/io/InputStream;
    goto :goto_1
.end method
