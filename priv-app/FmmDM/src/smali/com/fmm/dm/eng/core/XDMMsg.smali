.class public Lcom/fmm/dm/eng/core/XDMMsg;
.super Ljava/lang/Object;
.source "XDMMsg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;,
        Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;
    }
.end annotation


# static fields
.field private static SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

.field private static UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

.field private static final syncMsgQueueObj:Ljava/lang/Object;

.field private static final syncUIMsgQueueObj:Ljava/lang/Object;


# instance fields
.field public msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    sput-object v0, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 14
    sput-object v0, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/fmm/dm/eng/core/XDMMsg;->syncMsgQueueObj:Ljava/lang/Object;

    .line 17
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/fmm/dm/eng/core/XDMMsg;->syncUIMsgQueueObj:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    .line 22
    return-void
.end method

.method public static xdmCreateAbortMessage(IZ)Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    .locals 1
    .param p0, "abortCode"    # I
    .param p1, "userReq"    # Z

    .prologue
    .line 333
    const/4 v0, 0x0

    .line 335
    .local v0, "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    new-instance v0, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;

    .end local v0    # "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;-><init>()V

    .line 336
    .restart local v0    # "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    iput p0, v0, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;->abortCode:I

    .line 337
    iput-boolean p1, v0, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;->userReq:Z

    .line 338
    return-object v0
.end method

.method public static xdmCreateMessage(ILcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;)Lcom/fmm/dm/eng/core/XDMMsg;
    .locals 2
    .param p0, "type"    # I
    .param p1, "param"    # Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    .prologue
    .line 57
    new-instance v0, Lcom/fmm/dm/eng/core/XDMMsg;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMMsg;-><init>()V

    .line 59
    .local v0, "msg":Lcom/fmm/dm/eng/core/XDMMsg;
    if-nez v0, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 67
    .end local v0    # "msg":Lcom/fmm/dm/eng/core/XDMMsg;
    :goto_0
    return-object v0

    .line 62
    .restart local v0    # "msg":Lcom/fmm/dm/eng/core/XDMMsg;
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iput p0, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    .line 63
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iput-object p1, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    goto :goto_0
.end method

.method public static xdmFreeMessage(Lcom/fmm/dm/eng/core/XDMMsg;)V
    .locals 3
    .param p0, "msg"    # Lcom/fmm/dm/eng/core/XDMMsg;

    .prologue
    const/4 v2, 0x0

    .line 77
    const/4 v0, 0x0

    .line 78
    .local v0, "paramFree":Ljava/lang/Object;
    if-nez p0, :cond_0

    .line 103
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    if-eqz v1, :cond_3

    .line 85
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    if-eqz v1, :cond_2

    .line 87
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 89
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    .line 102
    :cond_3
    const/4 p0, 0x0

    .line 103
    goto :goto_0
.end method

.method public static xdmGetMessage()Lcom/fmm/dm/eng/core/XDMMsg;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 111
    const/4 v1, 0x0

    .line 113
    .local v1, "obj":Lcom/fmm/dm/eng/core/XDMMsg;
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v2, :cond_1

    .line 115
    sget-object v4, Lcom/fmm/dm/eng/core/XDMMsg;->syncMsgQueueObj:Ljava/lang/Object;

    monitor-enter v4

    .line 117
    :try_start_0
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    const/4 v5, 0x0

    invoke-static {v2, v5}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 118
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/fmm/dm/eng/core/XDMMsg;

    move-object v1, v0

    .line 119
    :goto_0
    if-nez v1, :cond_3

    .line 129
    :goto_1
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    iget v2, v2, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    if-nez v2, :cond_0

    .line 131
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListFreeLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 132
    const/4 v2, 0x0

    sput-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 115
    :cond_0
    monitor-exit v4

    .line 137
    :cond_1
    if-nez v1, :cond_2

    move-object v1, v3

    .line 140
    .end local v1    # "obj":Lcom/fmm/dm/eng/core/XDMMsg;
    :cond_2
    return-object v1

    .line 121
    .restart local v1    # "obj":Lcom/fmm/dm/eng/core/XDMMsg;
    :cond_3
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iget v2, v2, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    if-ltz v2, :cond_4

    .line 123
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemovePreviousObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    goto :goto_1

    .line 115
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 126
    :cond_4
    :try_start_1
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->SyncMLMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/fmm/dm/eng/core/XDMMsg;

    move-object v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static xdmGetUIMessage()Lcom/fmm/dm/eng/core/XDMMsg;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 149
    const/4 v1, 0x0

    .line 151
    .local v1, "obj":Lcom/fmm/dm/eng/core/XDMMsg;
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v2, :cond_1

    .line 153
    sget-object v4, Lcom/fmm/dm/eng/core/XDMMsg;->syncUIMsgQueueObj:Ljava/lang/Object;

    monitor-enter v4

    .line 155
    :try_start_0
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    const/4 v5, 0x0

    invoke-static {v2, v5}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 156
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/fmm/dm/eng/core/XDMMsg;

    move-object v1, v0

    .line 157
    :goto_0
    if-nez v1, :cond_3

    .line 167
    :goto_1
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    iget v2, v2, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    if-nez v2, :cond_0

    .line 169
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListFreeLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 170
    const/4 v2, 0x0

    sput-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 153
    :cond_0
    monitor-exit v4

    .line 175
    :cond_1
    if-nez v1, :cond_2

    move-object v1, v3

    .line 178
    .end local v1    # "obj":Lcom/fmm/dm/eng/core/XDMMsg;
    :cond_2
    return-object v1

    .line 159
    .restart local v1    # "obj":Lcom/fmm/dm/eng/core/XDMMsg;
    :cond_3
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMMsg;->msgItem:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    iget v2, v2, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    if-ltz v2, :cond_4

    .line 161
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemovePreviousObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    goto :goto_1

    .line 153
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 164
    :cond_4
    :try_start_1
    sget-object v2, Lcom/fmm/dm/eng/core/XDMMsg;->UIMsgQueue:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/fmm/dm/eng/core/XDMMsg;

    move-object v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10
    .param p0, "type"    # I
    .param p1, "param"    # Ljava/lang/Object;
    .param p2, "paramFree"    # Ljava/lang/Object;

    .prologue
    .line 189
    const/4 v5, 0x0

    .line 190
    .local v5, "msgParam":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;
    const/4 v3, 0x0

    .line 192
    .local v3, "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    if-eqz p1, :cond_0

    .line 194
    new-instance v5, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    .end local v5    # "msgParam":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;
    invoke-direct {v5}, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;-><init>()V

    .line 195
    .restart local v5    # "msgParam":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;
    iput-object p1, v5, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    .line 197
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 198
    if-eqz p2, :cond_0

    .line 200
    iput-object p2, v5, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 204
    :cond_0
    sget-object v7, Lcom/fmm/dm/eng/core/XDMMsg;->syncMsgQueueObj:Ljava/lang/Object;

    monitor-enter v7

    .line 206
    :try_start_0
    new-instance v4, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    invoke-direct {v4}, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 208
    .end local v3    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    .local v4, "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    :try_start_1
    sget-object v6, Lcom/fmm/dm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_1

    .line 211
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x5

    if-lt v1, v6, :cond_2

    .line 232
    .end local v1    # "i":I
    :cond_1
    if-eqz v4, :cond_3

    .line 234
    :try_start_2
    iput p0, v4, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    .line 235
    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    .line 237
    sget-object v6, Lcom/fmm/dm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 238
    .local v2, "msg":Landroid/os/Message;
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 239
    sget-object v6, Lcom/fmm/dm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 204
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 251
    return-void

    .line 215
    .restart local v1    # "i":I
    :cond_2
    :try_start_4
    const-string v6, "waiting for DM_TaskHandler create"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 216
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 223
    :goto_2
    :try_start_5
    sget-object v6, Lcom/fmm/dm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;

    if-nez v6, :cond_1

    .line 211
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 204
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    :catchall_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    :goto_3
    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 243
    .end local v3    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v4    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    :cond_3
    :try_start_7
    const-string v6, "Can\'t send message"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 246
    :catch_1
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v6, "Can\'t send message"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 204
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    :catchall_1
    move-exception v6

    goto :goto_3
.end method

.method public static xdmSendUIMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10
    .param p0, "type"    # I
    .param p1, "param"    # Ljava/lang/Object;
    .param p2, "paramFree"    # Ljava/lang/Object;

    .prologue
    .line 261
    const/4 v5, 0x0

    .line 262
    .local v5, "msgParam":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;
    const/4 v3, 0x0

    .line 264
    .local v3, "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    if-eqz p1, :cond_0

    .line 266
    new-instance v5, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    .end local v5    # "msgParam":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;
    invoke-direct {v5}, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;-><init>()V

    .line 267
    .restart local v5    # "msgParam":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;
    iput-object p1, v5, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    .line 269
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 270
    if-eqz p2, :cond_0

    .line 272
    iput-object p2, v5, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 276
    :cond_0
    sget-object v7, Lcom/fmm/dm/eng/core/XDMMsg;->syncUIMsgQueueObj:Ljava/lang/Object;

    monitor-enter v7

    .line 278
    :try_start_0
    new-instance v4, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    invoke-direct {v4}, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 280
    .end local v3    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    .local v4, "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    :try_start_1
    sget-object v6, Lcom/fmm/dm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_1

    .line 283
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x5

    if-lt v1, v6, :cond_2

    .line 304
    .end local v1    # "i":I
    :cond_1
    if-eqz v4, :cond_3

    .line 306
    :try_start_2
    iput p0, v4, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    .line 307
    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    .line 309
    sget-object v6, Lcom/fmm/dm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 310
    .local v2, "msg":Landroid/os/Message;
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 311
    sget-object v6, Lcom/fmm/dm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 276
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 323
    return-void

    .line 287
    .restart local v1    # "i":I
    :cond_2
    :try_start_4
    const-string v6, "waiting for hUITask create"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 288
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 295
    :goto_2
    :try_start_5
    sget-object v6, Lcom/fmm/dm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;

    if-nez v6, :cond_1

    .line 283
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 290
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 276
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    :catchall_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    :goto_3
    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 315
    .end local v3    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v4    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    :cond_3
    :try_start_7
    const-string v6, "Can\'t send UI message"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 318
    :catch_1
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v6, "Can\'t send UI message"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 276
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    :catchall_1
    move-exception v6

    goto :goto_3
.end method

.method public static xdmStatus(ILjava/lang/Object;)I
    .locals 1
    .param p0, "type"    # I
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method
