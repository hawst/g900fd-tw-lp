.class public Lcom/fmm/dm/eng/parser/XDMParserItem;
.super Ljava/lang/Object;
.source "XDMParserItem.java"

# interfaces
.implements Lcom/fmm/dm/eng/core/XDMWbxml;


# instance fields
.field public data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

.field public m_szSource:Ljava/lang/String;

.field public m_szTarget:Ljava/lang/String;

.field public meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

.field public moredata:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParParseItem(Lcom/fmm/dm/eng/parser/XDMParser;Lcom/fmm/dm/eng/parser/XDMParserItem;)I
    .locals 5
    .param p1, "p"    # Lcom/fmm/dm/eng/parser/XDMParser;
    .param p2, "item"    # Lcom/fmm/dm/eng/parser/XDMParserItem;

    .prologue
    .line 24
    const/4 v1, -0x1

    .line 25
    .local v1, "id":I
    const/4 v2, 0x0

    .line 27
    .local v2, "res":I
    const/16 v3, 0x14

    invoke-virtual {p1, v3}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 28
    if-eqz v2, :cond_0

    .line 30
    const-string v3, "not WBXML_ERR_OK"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 111
    :goto_0
    return v3

    .line 34
    :cond_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 35
    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 37
    const/4 v3, 0x0

    goto :goto_0

    .line 39
    :cond_1
    if-eqz v2, :cond_2

    .line 41
    const-string v3, "not WBXML_ERR_OK"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 42
    goto :goto_0

    .line 49
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 56
    :goto_1
    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    .line 58
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    .line 109
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "xdmParParseItem res  = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move v3, v2

    .line 111
    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 62
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v1, :sswitch_data_0

    .line 98
    const/4 v2, 0x2

    .line 101
    :goto_2
    if-eqz v2, :cond_2

    move v3, v2

    .line 103
    goto :goto_0

    .line 65
    :sswitch_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseTarget()I

    move-result v2

    .line 66
    iget-object v3, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szTarget:Ljava/lang/String;

    iput-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    goto :goto_2

    .line 70
    :sswitch_1
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseSource()I

    move-result v2

    .line 71
    iget-object v3, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szSource:Ljava/lang/String;

    iput-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    goto :goto_2

    .line 75
    :sswitch_2
    new-instance v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v3}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 76
    iget-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-virtual {v3, p1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v2

    .line 77
    iget-object v3, p1, Lcom/fmm/dm/eng/parser/XDMParser;->Meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 81
    :sswitch_3
    new-instance v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v3}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 82
    iget-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v3, p1, v1}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->xdmParParsePcdata(Lcom/fmm/dm/eng/parser/XDMParser;I)I

    move-result v2

    .line 83
    goto :goto_2

    .line 86
    :sswitch_4
    invoke-virtual {p1, v1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v3

    iput v3, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    goto :goto_2

    .line 90
    :sswitch_5
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 91
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 93
    iput v1, p1, Lcom/fmm/dm/eng/parser/XDMParser;->codePage:I

    goto :goto_2

    .line 62
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0xf -> :sswitch_3
        0x1a -> :sswitch_2
        0x27 -> :sswitch_1
        0x2e -> :sswitch_0
        0x34 -> :sswitch_4
    .end sparse-switch
.end method
