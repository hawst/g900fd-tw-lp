.class public Lcom/fmm/dm/eng/parser/XDMParserMapItem;
.super Ljava/lang/Object;
.source "XDMParserMapItem.java"

# interfaces
.implements Lcom/fmm/dm/eng/core/XDMWbxml;


# instance fields
.field public m_szSource:Ljava/lang/String;

.field public m_szTarget:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParParseMapitem(Lcom/fmm/dm/eng/parser/XDMParser;Lcom/fmm/dm/eng/parser/XDMParserMapItem;)I
    .locals 4
    .param p1, "p"    # Lcom/fmm/dm/eng/parser/XDMParser;
    .param p2, "mapitem"    # Lcom/fmm/dm/eng/parser/XDMParserMapItem;

    .prologue
    .line 21
    const/4 v1, -0x1

    .line 22
    .local v1, "id":I
    const/4 v2, 0x0

    .line 24
    .local v2, "res":I
    const/16 v3, 0x19

    invoke-virtual {p1, v3}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 25
    if-eqz v2, :cond_0

    move v3, v2

    .line 80
    :goto_0
    return v3

    .line 30
    :cond_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 31
    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 33
    const/4 v3, 0x0

    goto :goto_0

    .line 35
    :cond_1
    if-eqz v2, :cond_2

    move v3, v2

    .line 37
    goto :goto_0

    .line 44
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 51
    :goto_1
    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    .line 53
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    move v3, v2

    .line 80
    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 57
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v1, :sswitch_data_0

    .line 71
    const/4 v2, 0x2

    .line 74
    :goto_2
    if-eqz v2, :cond_2

    move v3, v2

    .line 76
    goto :goto_0

    .line 60
    :sswitch_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseTarget()I

    move-result v2

    .line 61
    iget-object v3, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szTarget:Ljava/lang/String;

    iput-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserMapItem;->m_szTarget:Ljava/lang/String;

    goto :goto_2

    .line 65
    :sswitch_1
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseSource()I

    move-result v2

    .line 66
    iget-object v3, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szSource:Ljava/lang/String;

    iput-object v3, p0, Lcom/fmm/dm/eng/parser/XDMParserMapItem;->m_szSource:Ljava/lang/String;

    goto :goto_2

    .line 57
    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_1
        0x2e -> :sswitch_0
    .end sparse-switch
.end method
