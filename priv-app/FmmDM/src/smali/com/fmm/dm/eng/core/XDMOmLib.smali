.class public Lcom/fmm/dm/eng/core/XDMOmLib;
.super Lcom/fmm/dm/eng/core/XDMOmVfs;
.source "XDMOmLib.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;-><init>()V

    return-void
.end method

.method public static xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z
    .locals 3
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p2, "nAcl"    # I

    .prologue
    const/4 v1, 0x0

    .line 230
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->m_szServerId:Ljava/lang/String;

    .line 232
    .local v0, "szServerId":Ljava/lang/String;
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v1

    .line 235
    :cond_1
    invoke-static {p1, p2, v0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckNodeAcl(Lcom/fmm/dm/eng/core/XDMVnode;ILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 240
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xdmOmCheckAclCurrentNode(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I)Z
    .locals 11
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "action"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 338
    const/4 v5, 0x0

    .line 339
    .local v5, "szPtr":Ljava/lang/String;
    const/4 v4, 0x0

    .line 341
    .local v4, "szNodeName":Ljava/lang/String;
    iget-object v9, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v0, v9, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 342
    .local v0, "basenode":Lcom/fmm/dm/eng/core/XDMVnode;
    iget-object v6, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->m_szServerId:Ljava/lang/String;

    .line 343
    .local v6, "szServerId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 345
    .local v2, "rootcheck":Z
    const/4 v3, 0x0

    .line 347
    .local v3, "searchSlash":I
    const-string v9, "/"

    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 348
    if-ltz v3, :cond_0

    .line 350
    invoke-virtual {p1, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 351
    add-int/lit8 v9, v3, 0x1

    invoke-virtual {p1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 354
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "strnodename :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", ptr :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 356
    :cond_1
    :goto_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 417
    :cond_2
    :goto_1
    return v7

    .line 358
    :cond_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 360
    if-nez v2, :cond_8

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x2f

    if-eq v9, v10, :cond_4

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x2e

    if-ne v9, v10, :cond_8

    .line 362
    :cond_4
    const/4 v2, 0x1

    .line 363
    iget-object v9, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v1, v9, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 378
    :cond_5
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    if-eq p2, v7, :cond_2

    .line 383
    :cond_6
    const-string v9, "."

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 385
    iget-object v9, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v1, v9, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 396
    .local v1, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_7
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 398
    invoke-static {v1, p2, v6}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckNodeAcl(Lcom/fmm/dm/eng/core/XDMVnode;ILjava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_a

    move v7, v8

    .line 400
    goto :goto_1

    .line 367
    .end local v1    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_8
    const-string v9, "/"

    invoke-virtual {v5, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 368
    if-ltz v3, :cond_1

    .line 370
    invoke-virtual {v5, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 371
    add-int/lit8 v9, v3, 0x1

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 372
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "strnodename :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", ptr :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 389
    :cond_9
    iget-object v9, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v9, v4, v0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 390
    .restart local v1    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v1, :cond_7

    move v7, v8

    .line 392
    goto :goto_1

    .line 404
    :cond_a
    move-object v0, v1

    .line 405
    const-string v9, "/"

    invoke-virtual {v5, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 406
    if-ltz v3, :cond_2

    .line 408
    invoke-virtual {v5, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 409
    add-int/lit8 v9, v3, 0x1

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 410
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "strnodename :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", ptr :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static xdmOmCheckNodeAcl(Lcom/fmm/dm/eng/core/XDMVnode;ILjava/lang/String;)Z
    .locals 5
    .param p0, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p1, "nAcl"    # I
    .param p2, "szServerId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 252
    const/4 v1, 0x0

    .line 253
    .local v1, "ptList":Lcom/fmm/dm/eng/core/XDMOmList;
    const/4 v0, 0x0

    .line 255
    .local v0, "ptAcl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 257
    if-nez v1, :cond_4

    .line 284
    :goto_0
    return v2

    .line 264
    :cond_0
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .end local v0    # "ptAcl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 266
    .restart local v0    # "ptAcl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    const-string v4, "*"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    .line 268
    :cond_1
    iget v3, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    if-nez v3, :cond_2

    .line 270
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMVnode;->ptParentNode:Lcom/fmm/dm/eng/core/XDMVnode;

    invoke-static {v2, p1, p2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckNodeAcl(Lcom/fmm/dm/eng/core/XDMVnode;ILjava/lang/String;)Z

    move-result v2

    goto :goto_0

    .line 274
    :cond_2
    iget v3, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    and-int/2addr v3, p1

    if-ne v3, p1, :cond_3

    .line 276
    const/4 v2, 0x1

    goto :goto_0

    .line 281
    :cond_3
    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 262
    :cond_4
    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public static xdmOmCheckNodePathDepth(Ljava/lang/String;)Z
    .locals 5
    .param p0, "szNodePath"    # Ljava/lang/String;

    .prologue
    .line 508
    const/4 v1, 0x0

    .line 509
    .local v1, "nCount":I
    const/4 v0, 0x0

    .line 510
    .local v0, "index":I
    move-object v2, p0

    .line 512
    .local v2, "szPath":Ljava/lang/String;
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 521
    const/16 v3, 0xf

    if-le v1, v3, :cond_2

    .line 523
    const/4 v3, 0x0

    .line 526
    :goto_1
    return v3

    .line 514
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2f

    if-ne v3, v4, :cond_1

    .line 516
    add-int/lit8 v1, v1, 0x1

    .line 512
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 526
    :cond_2
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static xdmOmDefaultACL(Ljava/lang/Object;Ljava/lang/String;II)V
    .locals 5
    .param p0, "pOM"    # Ljava/lang/Object;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "aclValue"    # I
    .param p3, "scope"    # I

    .prologue
    .line 538
    move-object v3, p0

    check-cast v3, Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 543
    .local v3, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    invoke-static {v3, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v2

    .line 544
    .local v2, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v2, :cond_0

    .line 546
    iget-object v1, v2, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 547
    .local v1, "item":Lcom/fmm/dm/eng/core/XDMOmList;
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 548
    .local v0, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    iput p2, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    .line 549
    iput p3, v2, Lcom/fmm/dm/eng/core/XDMVnode;->scope:I

    .line 555
    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    .end local v1    # "item":Lcom/fmm/dm/eng/core/XDMOmList;
    :goto_0
    return-void

    .line 553
    :cond_0
    const-string v4, "Not Exist"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmOmDelete(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Z)I
    .locals 4
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "deletechild"    # Z

    .prologue
    const/4 v2, -0x3

    .line 201
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v3, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsPath2Node(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    .line 202
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return v2

    .line 207
    :cond_1
    const/4 v3, 0x2

    invoke-static {p0, v0, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 209
    const/4 v2, -0x5

    goto :goto_0

    .line 212
    :cond_2
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v3, v0, p2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsRemoveNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Z)I

    move-result v1

    .line 213
    .local v1, "ret":I
    if-nez v1, :cond_0

    .line 218
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xdmOmDeleteImplicit(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Z)I
    .locals 4
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "deletechild"    # Z

    .prologue
    const/4 v2, -0x3

    .line 569
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v3, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsPath2Node(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    .line 570
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v0, :cond_1

    .line 581
    :cond_0
    :goto_0
    return v2

    .line 575
    :cond_1
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v3, v0, p2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsRemoveNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Z)I

    move-result v1

    .line 576
    .local v1, "ret":I
    if-nez v1, :cond_0

    .line 581
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xdmOmEnd(Lcom/fmm/dm/eng/core/XDMOmTree;)I
    .locals 2
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;

    .prologue
    .line 34
    if-nez p0, :cond_0

    .line 36
    const/4 v1, -0x3

    .line 47
    :goto_0
    return v1

    .line 41
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsSaveFs(Lcom/fmm/dm/eng/core/XDMOmVfs;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmOmGetChild(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;[Ljava/lang/String;I)I
    .locals 4
    .param p0, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "bufs"    # [Ljava/lang/String;
    .param p3, "maxnum"    # I

    .prologue
    .line 299
    const/4 v1, 0x0

    .line 301
    .local v1, "i":I
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v3, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsPath2Node(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v2

    .line 302
    .local v2, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v2, :cond_1

    .line 304
    const/4 p3, -0x6

    .line 326
    .end local p3    # "maxnum":I
    :cond_0
    :goto_0
    return p3

    .line 307
    .restart local p3    # "maxnum":I
    :cond_1
    iget-object v0, v2, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 309
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    :goto_1
    if-nez v0, :cond_2

    move p3, v1

    .line 326
    goto :goto_0

    .line 311
    :cond_2
    if-ge v1, p3, :cond_0

    .line 316
    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 318
    const/4 p3, -0x3

    goto :goto_0

    .line 320
    :cond_3
    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    aput-object v3, p2, v1

    .line 322
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 323
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;
    .locals 1
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsPath2Node(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    return-object v0
.end method

.method public static xdmOmInit(Lcom/fmm/dm/eng/core/XDMOmTree;)I
    .locals 2
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;

    .prologue
    .line 18
    const/4 v0, 0x0

    .line 20
    .local v0, "ret":I
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsInit(Lcom/fmm/dm/eng/core/XDMOmVfs;)I

    move-result v0

    .line 21
    if-nez v0, :cond_0

    .line 22
    const/4 v1, 0x0

    .line 24
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x3

    goto :goto_0
.end method

.method public static xdmOmMakeParentPath(Ljava/lang/String;[C)V
    .locals 6
    .param p0, "szIn"    # Ljava/lang/String;
    .param p1, "out"    # [C

    .prologue
    const/4 v5, 0x0

    .line 428
    const/4 v2, -0x1

    .line 430
    .local v2, "pos":I
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 453
    :goto_0
    return-void

    .line 432
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 433
    .local v1, "len":I
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_1

    .line 442
    :goto_2
    if-gez v2, :cond_3

    .line 444
    aput-char v5, p1, v5

    goto :goto_0

    .line 435
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2f

    if-ne v3, v4, :cond_2

    .line 437
    move v2, v0

    .line 438
    goto :goto_2

    .line 433
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 448
    :cond_3
    const/4 v0, 0x0

    :goto_3
    if-lt v0, v2, :cond_4

    .line 452
    aput-char v5, p1, v0

    goto :goto_0

    .line 450
    :cond_4
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aput-char v3, p1, v0

    .line 448
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public static xdmOmProcessCmdImplicitAdd(Ljava/lang/Object;Ljava/lang/String;II)Z
    .locals 9
    .param p0, "pOM"    # Ljava/lang/Object;
    .param p1, "szNodeName"    # Ljava/lang/String;
    .param p2, "aclValue"    # I
    .param p3, "bStart"    # I

    .prologue
    const/4 v2, 0x0

    .line 465
    const/4 v7, 0x0

    .line 466
    .local v7, "parentNode":[C
    const/4 v8, 0x0

    .line 467
    .local v8, "szParentNodeName":Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    move-object v0, p0

    .line 468
    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 470
    .local v0, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 472
    const-string v1, "ROOT NODE not found"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 498
    :cond_0
    :goto_0
    return v2

    .line 476
    :cond_1
    if-lez p3, :cond_2

    .line 478
    invoke-static {p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckNodePathDepth(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484
    :cond_2
    const/16 v1, 0x100

    new-array v7, v1, [C

    .line 486
    invoke-static {p1, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 487
    invoke-static {v7}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v8

    .line 488
    invoke-static {v0, v8}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v6

    .line 490
    if-nez v6, :cond_3

    .line 492
    invoke-static {v0, v8, p2, v2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmProcessCmdImplicitAdd(Ljava/lang/Object;Ljava/lang/String;II)Z

    .line 495
    :cond_3
    const-string v4, ""

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 496
    const/4 v1, 0x2

    invoke-static {v0, p1, p2, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmDefaultACL(Ljava/lang/Object;Ljava/lang/String;II)V

    .line 498
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I
    .locals 2
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "nOffset"    # I
    .param p3, "databuf"    # [C
    .param p4, "bufsize"    # I

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "ret":I
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v1, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmvfsCheckPath(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    const/4 v1, -0x1

    .line 126
    :goto_0
    return v1

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v1, p1, p2, p3, p4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmvfsReadObj(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;I[CI)I

    move-result v0

    .line 121
    if-gez v0, :cond_1

    .line 123
    const/4 v1, -0x3

    goto :goto_0

    :cond_1
    move v1, v0

    .line 126
    goto :goto_0
.end method

.method public static xdmOmSetServerId(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)I
    .locals 3
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szServerId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x13

    if-le v1, v2, :cond_1

    .line 60
    :cond_0
    const/4 v0, -0x3

    .line 64
    :goto_0
    return v0

    .line 63
    :cond_1
    iput-object p1, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->m_szServerId:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I
    .locals 7
    .param p0, "ptOmt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "nTotalSize"    # I
    .param p3, "nOffset"    # I
    .param p4, "pData"    # Ljava/lang/Object;
    .param p5, "nSize"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsCreatePath(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)I

    move-result v6

    .line 92
    .local v6, "ret":I
    if-eqz p4, :cond_0

    if-lez p5, :cond_0

    .line 94
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsWriteObj(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v6

    .line 95
    if-gez v6, :cond_0

    .line 97
    const/4 p5, -0x3

    .line 101
    .end local p5    # "nSize":I
    :cond_0
    return p5
.end method

.method public static xdmOmvfsCheckPath(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Z
    .locals 2
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    .line 139
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsPath2Node(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    .line 140
    .local v0, "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v0, :cond_0

    .line 142
    const/4 v1, 0x0

    .line 145
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xdmOmvfsReadObj(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;I[CI)I
    .locals 5
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "nOffset"    # I
    .param p3, "pBuff"    # [C
    .param p4, "nBuffSize"    # I

    .prologue
    const/4 v3, -0x4

    .line 163
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsPath2Node(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 164
    .local v1, "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v1, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v3

    .line 168
    :cond_1
    iget v4, v1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-lez v4, :cond_0

    iget v4, v1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    if-ltz v4, :cond_0

    .line 170
    add-int v0, p4, p2

    .line 171
    .local v0, "blocksize":I
    iget v4, v1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-le v0, v4, :cond_2

    .line 173
    iget v4, v1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    sub-int v4, v0, v4

    sub-int/2addr p4, v4

    .line 175
    :cond_2
    iget v4, v1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    add-int/2addr v4, p2

    invoke-static {p0, v4, p3, p4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsLoadFsData(Lcom/fmm/dm/eng/core/XDMOmVfs;I[CI)I

    move-result v2

    .line 176
    .local v2, "ret":I
    if-nez v2, :cond_0

    move v3, p4

    .line 186
    goto :goto_0
.end method
