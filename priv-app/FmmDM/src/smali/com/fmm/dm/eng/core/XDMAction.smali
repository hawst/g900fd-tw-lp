.class public Lcom/fmm/dm/eng/core/XDMAction;
.super Ljava/lang/Object;
.source "XDMAction.java"


# instance fields
.field public CmdID:I

.field public MsgID:I

.field public sourceList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public szCmdName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMAction;->sourceList:Ljava/util/LinkedList;

    .line 7
    return-void
.end method


# virtual methods
.method public xdmCreateAction(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "szCmd"    # Ljava/lang/String;
    .param p2, "createItemList"    # Z

    .prologue
    .line 21
    iput-object p1, p0, Lcom/fmm/dm/eng/core/XDMAction;->szCmdName:Ljava/lang/String;

    .line 22
    if-nez p2, :cond_0

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMAction;->sourceList:Ljava/util/LinkedList;

    .line 26
    :cond_0
    return-void
.end method

.method public xdmFindAction(IILjava/lang/String;)Lcom/fmm/dm/eng/core/XDMAction;
    .locals 5
    .param p1, "msgRef"    # I
    .param p2, "cmdRef"    # I
    .param p3, "szCmd"    # Ljava/lang/String;

    .prologue
    .line 37
    new-instance v0, Lcom/fmm/dm/eng/core/XDMAction;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMAction;-><init>()V

    .line 38
    .local v0, "action":Lcom/fmm/dm/eng/core/XDMAction;
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .line 39
    .local v2, "size":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "wsFindAction actionList size : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 40
    :goto_0
    if-ge v1, v2, :cond_0

    if-gtz v2, :cond_2

    .line 49
    :cond_0
    const/4 v0, 0x0

    .end local v0    # "action":Lcom/fmm/dm/eng/core/XDMAction;
    :cond_1
    return-object v0

    .line 42
    .restart local v0    # "action":Lcom/fmm/dm/eng/core/XDMAction;
    :cond_2
    iget v3, v0, Lcom/fmm/dm/eng/core/XDMAction;->MsgID:I

    if-ne v3, p1, :cond_3

    iget v3, v0, Lcom/fmm/dm/eng/core/XDMAction;->CmdID:I

    if-eq v3, p2, :cond_1

    .line 46
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
