.class public Lcom/fmm/dm/eng/core/XDMOmVfs;
.super Ljava/lang/Object;
.source "XDMOmVfs.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMInterface;


# static fields
.field private static final OM_MAX_LEN:I = 0x200

.field private static index:I


# instance fields
.field public root:Lcom/fmm/dm/eng/core/XDMVnode;

.field public stdobj_space:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 27
    const v0, 0xa000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->stdobj_space:[B

    .line 28
    return-void
.end method

.method public static xdmOmVfsAppendList(Lcom/fmm/dm/eng/core/XDMOmList;Lcom/fmm/dm/eng/core/XDMOmList;)Lcom/fmm/dm/eng/core/XDMOmList;
    .locals 3
    .param p0, "h"    # Lcom/fmm/dm/eng/core/XDMOmList;
    .param p1, "node"    # Lcom/fmm/dm/eng/core/XDMOmList;

    .prologue
    const/4 v2, 0x0

    .line 527
    if-nez p0, :cond_0

    .line 529
    move-object p0, p1

    .line 530
    iput-object v2, p0, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 543
    :goto_0
    return-object p0

    .line 534
    :cond_0
    move-object v0, p0

    .line 535
    .local v0, "tmp":Lcom/fmm/dm/eng/core/XDMOmList;
    :goto_1
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    if-nez v1, :cond_1

    .line 540
    iput-object v2, p1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 541
    iput-object p1, v0, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    goto :goto_0

    .line 537
    :cond_1
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    goto :goto_1
.end method

.method public static xdmOmVfsAppendNode(Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)I
    .locals 3
    .param p0, "ptParent"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p1, "ptChild"    # Lcom/fmm/dm/eng/core/XDMVnode;

    .prologue
    const/4 v1, 0x0

    .line 550
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsHaveThisChild(Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 552
    const/4 v1, -0x2

    .line 574
    :goto_0
    return v1

    .line 555
    :cond_0
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    if-nez v2, :cond_1

    .line 557
    iput-object p1, p0, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 560
    iput-object p0, p1, Lcom/fmm/dm/eng/core/XDMVnode;->ptParentNode:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_0

    .line 564
    :cond_1
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 565
    .local v0, "last":Lcom/fmm/dm/eng/core/XDMVnode;
    :goto_1
    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    if-nez v2, :cond_2

    .line 569
    iput-object p1, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 573
    iput-object p0, p1, Lcom/fmm/dm/eng/core/XDMVnode;->ptParentNode:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_0

    .line 567
    :cond_2
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_1
.end method

.method public static xdmOmVfsCreateNewNode(Ljava/lang/String;Z)Lcom/fmm/dm/eng/core/XDMVnode;
    .locals 5
    .param p0, "szName"    # Ljava/lang/String;
    .param p1, "defaultacl"    # Z

    .prologue
    const/4 v4, 0x0

    .line 66
    new-instance v2, Lcom/fmm/dm/eng/core/XDMVnode;

    invoke-direct {v2}, Lcom/fmm/dm/eng/core/XDMVnode;-><init>()V

    .line 68
    .local v2, "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz p1, :cond_0

    .line 70
    new-instance v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMOmAcl;-><init>()V

    .line 71
    .local v0, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    const-string v3, "*"

    iput-object v3, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    .line 72
    const/16 v3, 0x1b

    iput v3, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    .line 74
    new-instance v1, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v1}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 75
    .local v1, "item":Lcom/fmm/dm/eng/core/XDMOmList;
    iput-object v0, v1, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 76
    const/4 v3, 0x0

    iput-object v3, v1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 78
    iput-object v1, v2, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 81
    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    .end local v1    # "item":Lcom/fmm/dm/eng/core/XDMOmList;
    :cond_0
    iput-object p0, v2, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 82
    const/4 v3, 0x6

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 83
    iput v4, v2, Lcom/fmm/dm/eng/core/XDMVnode;->verno:I

    .line 84
    iput v4, v2, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 85
    const/4 v3, -0x1

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    .line 86
    const/4 v3, 0x2

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMVnode;->scope:I

    .line 88
    return-object v2
.end method

.method public static xdmOmVfsCreatePath(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)I
    .locals 11
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 665
    const-string v6, ""

    .line 666
    .local v6, "szNodeName":Ljava/lang/String;
    move-object v7, p1

    .line 668
    .local v7, "szTempPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 669
    .local v1, "index":I
    const/4 v0, 0x0

    .line 670
    .local v0, "i":I
    iget-object v4, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 672
    .local v4, "ptBaseNode":Lcom/fmm/dm/eng/core/XDMVnode;
    const-string v9, "/"

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 673
    .local v3, "nodenamesplit":[Ljava/lang/String;
    array-length v2, v3

    .line 675
    .local v2, "l":I
    aget-object v9, v3, v8

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_0

    .line 676
    const/4 v1, 0x1

    .line 680
    :goto_0
    move v0, v1

    :goto_1
    if-lt v0, v2, :cond_1

    .line 702
    :goto_2
    return v8

    .line 678
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 682
    :cond_1
    aget-object v6, v3, v0

    .line 683
    add-int/lit8 v9, v0, 0x1

    if-ne v9, v2, :cond_3

    .line 685
    invoke-static {p0, v6, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 687
    const/4 v8, -0x2

    goto :goto_2

    .line 690
    :cond_2
    const/4 v9, 0x1

    invoke-static {v6, v9}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsCreateNewNode(Ljava/lang/String;Z)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 691
    .local v5, "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-static {v4, v5}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsAppendNode(Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)I

    goto :goto_2

    .line 694
    .end local v5    # "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_3
    invoke-static {p0, v6, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 695
    .restart local v5    # "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v5, :cond_4

    .line 697
    const/4 v8, -0x3

    goto :goto_2

    .line 700
    :cond_4
    move-object v4, v5

    .line 680
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static xdmOmVfsDeleteAclList(Lcom/fmm/dm/eng/core/XDMOmList;)V
    .locals 3
    .param p0, "h"    # Lcom/fmm/dm/eng/core/XDMOmList;

    .prologue
    .line 965
    move-object v1, p0

    .line 967
    .local v1, "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    const/4 v0, 0x0

    .line 969
    .local v0, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    :goto_0
    if-nez v1, :cond_0

    .line 979
    return-void

    .line 971
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 972
    .local v2, "next":Lcom/fmm/dm/eng/core/XDMOmList;
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 974
    .restart local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    const/4 v0, 0x0

    .line 975
    const/4 v1, 0x0

    .line 977
    move-object v1, v2

    goto :goto_0
.end method

.method public static xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V
    .locals 3
    .param p0, "h"    # Lcom/fmm/dm/eng/core/XDMOmList;

    .prologue
    .line 986
    move-object v0, p0

    .line 988
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    :goto_0
    if-nez v0, :cond_0

    .line 997
    return-void

    .line 990
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 992
    .local v1, "next":Lcom/fmm/dm/eng/core/XDMOmList;
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 993
    const/4 v0, 0x0

    .line 995
    move-object v0, v1

    goto :goto_0
.end method

.method private static xdmOmVfsDeleteOmFile()V
    .locals 1

    .prologue
    .line 1074
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdObjectTreeInfo()I

    move-result v0

    .line 1075
    .local v0, "nFileId":I
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbDeleteFile(I)I

    .line 1077
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdObjectData()I

    move-result v0

    .line 1078
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbDeleteFile(I)I

    .line 1079
    return-void
.end method

.method public static xdmOmVfsDeleteVfs(Lcom/fmm/dm/eng/core/XDMVnode;)V
    .locals 4
    .param p0, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;

    .prologue
    const/4 v3, 0x0

    .line 1045
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 1048
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    :goto_0
    if-nez v0, :cond_2

    .line 1055
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v2, :cond_0

    .line 1057
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsDeleteAclList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 1059
    :cond_0
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v2, :cond_1

    .line 1061
    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 1063
    :cond_1
    iput-object v3, p0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 1064
    iput-object v3, p0, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 1065
    iput-object v3, p0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szTstamp:Ljava/lang/String;

    .line 1066
    iput-object v3, p0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szDdfName:Ljava/lang/String;

    .line 1067
    const/4 p0, 0x0

    .line 1068
    return-void

    .line 1050
    :cond_2
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 1051
    .local v1, "tmp":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsDeleteVfs(Lcom/fmm/dm/eng/core/XDMVnode;)V

    .line 1052
    move-object v0, v1

    goto :goto_0
.end method

.method public static xdmOmVfsEnd(Lcom/fmm/dm/eng/core/XDMOmVfs;)V
    .locals 1
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsDeleteVfs(Lcom/fmm/dm/eng/core/XDMVnode;)V

    .line 1041
    return-void
.end method

.method public static xdmOmVfsFindVaddr(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVfspace;)V
    .locals 5
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p2, "pSpace"    # Lcom/fmm/dm/eng/core/XDMVfspace;

    .prologue
    .line 814
    iget-object v0, p1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 816
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    :goto_0
    if-nez v0, :cond_1

    .line 822
    iget v1, p1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    if-ltz v1, :cond_0

    iget v1, p1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-lez v1, :cond_0

    .line 824
    iget-object v1, p2, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    iget v2, p2, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    iget v3, p1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    aput v3, v1, v2

    .line 825
    iget-object v1, p2, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    iget v2, p2, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    iget v3, p1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    iget v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    .line 826
    iget v1, p2, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p2, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    .line 828
    :cond_0
    return-void

    .line 818
    :cond_1
    invoke-static {p0, v0, p2}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsFindVaddr(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVfspace;)V

    .line 819
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_0
.end method

.method public static xdmOmVfsGetData(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;[C)I
    .locals 4
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p2, "pBuff"    # [C

    .prologue
    const/4 v1, -0x4

    .line 1003
    iget v2, p1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-lez v2, :cond_0

    iget v2, p1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    if-ltz v2, :cond_0

    .line 1005
    iget v2, p1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    iget v3, p1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {p0, v2, p2, v3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsLoadFsData(Lcom/fmm/dm/eng/core/XDMOmVfs;I[CI)I

    move-result v0

    .line 1006
    .local v0, "ret":I
    if-eqz v0, :cond_1

    .line 1012
    .end local v0    # "ret":I
    :cond_0
    :goto_0
    return v1

    .line 1010
    .restart local v0    # "ret":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static xdmOmVfsGetFreeVaddr(Lcom/fmm/dm/eng/core/XDMOmVfs;I)I
    .locals 12
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "nSize"    # I

    .prologue
    const/4 v11, 0x0

    .line 750
    const/4 v3, 0x0

    .line 755
    .local v3, "pSpace":Lcom/fmm/dm/eng/core/XDMVfspace;
    new-instance v3, Lcom/fmm/dm/eng/core/XDMVfspace;

    .end local v3    # "pSpace":Lcom/fmm/dm/eng/core/XDMVfspace;
    invoke-direct {v3}, Lcom/fmm/dm/eng/core/XDMVfspace;-><init>()V

    .line 756
    .restart local v3    # "pSpace":Lcom/fmm/dm/eng/core/XDMVfspace;
    iget-object v8, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    invoke-static {p0, v8, v3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsFindVaddr(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVfspace;)V

    .line 758
    iget v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    if-nez v8, :cond_0

    .line 760
    const/4 v4, 0x0

    .line 761
    .local v4, "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 809
    .end local v4    # "ret":I
    .local v5, "ret":I
    :goto_0
    return v5

    .line 765
    .end local v5    # "ret":I
    :cond_0
    iget v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v1, v8, -0x1

    .local v1, "i":I
    :goto_1
    const/4 v8, 0x1

    if-ge v1, v8, :cond_1

    .line 781
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    aget v8, v8, v11

    if-lez v8, :cond_4

    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    aget v8, v8, v11

    add-int/lit8 v8, v8, 0x1

    if-lt v8, p1, :cond_4

    .line 783
    const/4 v4, 0x0

    .line 784
    .restart local v4    # "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 785
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto :goto_0

    .line 767
    .end local v5    # "ret":I
    :cond_1
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_2
    add-int/lit8 v8, v1, -0x1

    if-le v2, v8, :cond_2

    .line 765
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 769
    :cond_2
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    add-int/lit8 v9, v2, 0x1

    aget v8, v8, v9

    iget-object v9, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    aget v9, v9, v2

    if-ge v8, v9, :cond_3

    .line 771
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    add-int/lit8 v9, v2, 0x1

    aget v7, v8, v9

    .line 772
    .local v7, "start":I
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    add-int/lit8 v9, v2, 0x1

    aget v0, v8, v9

    .line 773
    .local v0, "end":I
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    add-int/lit8 v9, v2, 0x1

    iget-object v10, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    aget v10, v10, v2

    aput v10, v8, v9

    .line 774
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    add-int/lit8 v9, v2, 0x1

    iget-object v10, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    aget v10, v10, v2

    aput v10, v8, v9

    .line 775
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    aput v7, v8, v2

    .line 776
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    aput v0, v8, v2

    .line 767
    .end local v0    # "end":I
    .end local v7    # "start":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 788
    .end local v2    # "k":I
    :cond_4
    const/4 v1, 0x0

    :goto_3
    iget v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v8, v8, -0x1

    if-lt v1, v8, :cond_5

    .line 799
    const-wide/32 v8, 0xa000

    iget-object v10, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    iget v11, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v11, v11, -0x1

    aget v10, v10, v11

    int-to-long v10, v10

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    int-to-long v10, p1

    cmp-long v8, v8, v10

    if-ltz v8, :cond_7

    .line 801
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    iget v9, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v9, v9, -0x1

    aget v4, v8, v9

    .line 802
    .restart local v4    # "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 803
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto :goto_0

    .line 790
    .end local v5    # "ret":I
    :cond_5
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->start:[I

    add-int/lit8 v9, v1, 0x1

    aget v8, v8, v9

    iget-object v9, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    aget v9, v9, v1

    sub-int/2addr v8, v9

    add-int/lit8 v6, v8, -0x1

    .line 791
    .local v6, "s":I
    if-lt v6, p1, :cond_6

    .line 793
    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMVfspace;->end:[I

    aget v4, v8, v1

    .line 794
    .restart local v4    # "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 795
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto/16 :goto_0

    .line 788
    .end local v5    # "ret":I
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 806
    .end local v6    # "s":I
    :cond_7
    const/4 v4, -0x5

    .line 808
    .restart local v4    # "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 809
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto/16 :goto_0
.end method

.method public static xdmOmVfsGetNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;
    .locals 3
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "szNodeName"    # Ljava/lang/String;
    .param p2, "ptBaseNode"    # Lcom/fmm/dm/eng/core/XDMVnode;

    .prologue
    const/4 v1, 0x0

    .line 636
    const/4 v0, 0x0

    .line 638
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz p2, :cond_0

    .line 640
    iget-object v0, p2, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 643
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 659
    :goto_0
    return-object v1

    .line 646
    :cond_1
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-nez v2, :cond_5

    .line 648
    :cond_2
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_0

    .line 653
    :cond_3
    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v1, v0

    .line 655
    goto :goto_0

    .line 657
    :cond_4
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 651
    :cond_5
    if-nez v0, :cond_3

    goto :goto_0
.end method

.method public static xdmOmVfsGetParent(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;
    .locals 3
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "ptBaseNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p2, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;

    .prologue
    .line 938
    move-object v1, p1

    .line 939
    .local v1, "ptParent":Lcom/fmm/dm/eng/core/XDMVnode;
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 941
    .local v0, "ptChild":Lcom/fmm/dm/eng/core/XDMVnode;
    :goto_0
    if-nez v0, :cond_1

    .line 948
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 949
    :goto_1
    if-nez v0, :cond_2

    .line 959
    const/4 v1, 0x0

    .end local v1    # "ptParent":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_0
    :goto_2
    return-object v1

    .line 943
    .restart local v1    # "ptParent":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_1
    if-eq v0, p2, :cond_0

    .line 945
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_0

    .line 951
    :cond_2
    invoke-static {p0, v0, p2}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetParent(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v2

    .line 952
    .local v2, "tmp":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v2, :cond_3

    move-object v1, v2

    .line 954
    goto :goto_2

    .line 956
    :cond_3
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_1
.end method

.method public static xdmOmVfsHaveThisChild(Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)Z
    .locals 3
    .param p0, "ptParent"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p1, "ptChild"    # Lcom/fmm/dm/eng/core/XDMVnode;

    .prologue
    .line 579
    const/4 v0, 0x0

    .line 581
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz p0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 586
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 595
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 588
    :cond_1
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    iget-object v2, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 590
    const/4 v1, 0x1

    goto :goto_1

    .line 592
    :cond_2
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_0
.end method

.method public static xdmOmVfsInit(Lcom/fmm/dm/eng/core/XDMOmVfs;)I
    .locals 3
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 45
    .local v0, "nRet":I
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    if-nez v1, :cond_0

    .line 47
    const-string v1, "/"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsCreateNewNode(Ljava/lang/String;Z)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 50
    :cond_0
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsLoadFs(Lcom/fmm/dm/eng/core/XDMOmVfs;)I

    move-result v0

    .line 51
    return v0
.end method

.method public static xdmOmVfsLoadFs(Lcom/fmm/dm/eng/core/XDMOmVfs;)I
    .locals 11
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;

    .prologue
    const/4 v10, 0x0

    .line 98
    const/4 v8, 0x0

    .line 99
    .local v8, "nFileId":I
    const/4 v1, 0x0

    .line 100
    .local v1, "Input":Ljava/io/DataInputStream;
    const/4 v2, 0x0

    .line 101
    .local v2, "pBuff":I
    const/4 v5, 0x0

    .line 102
    .local v5, "nSize":I
    const/4 v9, 0x0

    .line 104
    .local v9, "szFileName":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdObjectTreeInfo()I

    move-result v8

    .line 105
    invoke-static {v8}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v9

    .line 106
    invoke-static {v8}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileSize(I)I

    move-result v5

    .line 107
    if-gtz v5, :cond_0

    move v0, v10

    .line 172
    :goto_0
    return v0

    .line 112
    :cond_0
    new-array v4, v5, [B

    .line 113
    .local v4, "tmp":[B
    invoke-static {v8, v10, v5, v4}, Lcom/fmm/dm/db/file/XDB;->xdbReadFile(III[B)Z

    .line 116
    :try_start_0
    new-instance v6, Ljava/io/DataInputStream;

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "Input":Ljava/io/DataInputStream;
    .local v6, "Input":Ljava/io/DataInputStream;
    move-object v1, v6

    .line 123
    .end local v6    # "Input":Ljava/io/DataInputStream;
    .restart local v1    # "Input":Ljava/io/DataInputStream;
    :goto_1
    sget v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    aget-byte v2, v4, v0

    .line 124
    :cond_1
    const/16 v0, 0x42

    if-eq v2, v0, :cond_3

    .line 158
    if-eqz v1, :cond_2

    .line 159
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 166
    :cond_2
    :goto_2
    const/4 v4, 0x0

    .line 167
    sput v10, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 168
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdObjectData()I

    move-result v8

    .line 169
    invoke-static {v8}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileSize(I)I

    move-result v5

    .line 170
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->stdobj_space:[B

    invoke-static {v8, v10, v5, v0}, Lcom/fmm/dm/db/file/XDB;->xdbReadFile(III[B)Z

    move v0, v10

    .line 172
    goto :goto_0

    .line 118
    :catch_0
    move-exception v7

    .line 120
    .local v7, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 128
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackFsNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/io/DataInputStream;ILcom/fmm/dm/eng/core/XDMVnode;[BI)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v2

    .line 139
    if-nez v2, :cond_1

    .line 143
    if-eqz v1, :cond_4

    .line 144
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 150
    :cond_4
    :goto_3
    const/4 v4, 0x0

    .line 151
    sput v10, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 152
    const/4 v0, -0x4

    goto :goto_0

    .line 130
    :catch_1
    move-exception v7

    .line 132
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 135
    invoke-direct {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsResetStdobj()V

    .line 136
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsDeleteOmFile()V

    move v0, v10

    .line 137
    goto :goto_0

    .line 146
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v7

    .line 148
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 161
    .end local v7    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    .line 163
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static xdmOmVfsLoadFsData(Lcom/fmm/dm/eng/core/XDMOmVfs;I[CI)I
    .locals 3
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "addr"    # I
    .param p2, "pBuff"    # [C
    .param p3, "nSize"    # I

    .prologue
    .line 846
    const/4 v0, 0x0

    .line 847
    .local v0, "i":I
    const/4 v0, 0x0

    :goto_0
    if-lt v0, p3, :cond_0

    .line 851
    const/4 v1, 0x0

    return v1

    .line 849
    :cond_0
    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->stdobj_space:[B

    add-int v2, p1, v0

    aget-byte v1, v1, v2

    int-to-char v1, v1

    aput-char v1, p2, v0

    .line 847
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static xdmOmVfsPackByte(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;
    .locals 0
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 328
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 329
    return-object p0
.end method

.method public static xdmOmVfsPackEnd(Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;
    .locals 1
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 322
    const/16 v0, 0x44

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 323
    return-object p0
.end method

.method public static xdmOmVfsPackFsNode(Ljava/io/DataOutputStream;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;
    .locals 1
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 222
    const/4 v0, 0x0

    .line 223
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez p1, :cond_0

    .line 240
    :goto_0
    return-object p0

    .line 228
    :cond_0
    iget-object v0, p1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 230
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackStart(Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 231
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackNode(Ljava/io/DataOutputStream;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 233
    :goto_1
    if-nez v0, :cond_1

    .line 239
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackEnd(Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 240
    goto :goto_0

    .line 235
    :cond_1
    invoke-static {p0, v0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackFsNode(Ljava/io/DataOutputStream;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 236
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_1
.end method

.method public static xdmOmVfsPackInt16(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;
    .locals 0
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 369
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 370
    return-object p0
.end method

.method public static xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;
    .locals 0
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 364
    return-object p0
.end method

.method public static xdmOmVfsPackNode(Ljava/io/DataOutputStream;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;
    .locals 5
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    const/4 v2, 0x0

    .line 257
    .local v2, "num":I
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 258
    .local v1, "item":Lcom/fmm/dm/eng/core/XDMOmList;
    :goto_0
    if-nez v1, :cond_1

    .line 264
    invoke-static {p0, v2}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackByte(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 266
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 267
    :goto_1
    if-nez v1, :cond_2

    .line 275
    iget v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 276
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 277
    iget v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 278
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 279
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szTstamp:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 280
    iget v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->scope:I

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 282
    const/4 v2, 0x0

    .line 283
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 284
    :goto_2
    if-nez v1, :cond_3

    .line 289
    invoke-static {p0, v2}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackByte(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 290
    if-lez v2, :cond_0

    .line 292
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 293
    :goto_3
    if-nez v1, :cond_4

    .line 301
    :cond_0
    iget v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->verno:I

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackInt16(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 302
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szDdfName:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 303
    iget v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 305
    return-object p0

    .line 260
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 261
    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    goto :goto_0

    .line 269
    :cond_2
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 270
    .local v0, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 271
    iget v4, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    invoke-static {p0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackByte(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 272
    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    goto :goto_1

    .line 286
    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 287
    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    goto :goto_2

    .line 295
    :cond_4
    iget-object v3, v1, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 296
    .local v3, "szData":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 297
    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    goto :goto_3
.end method

.method public static xdmOmVfsPackStart(Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;
    .locals 1
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    const/16 v0, 0x42

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 317
    return-object p0
.end method

.method public static xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;
    .locals 3
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "szData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 334
    const/4 v1, 0x0

    .line 336
    .local v1, "len":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 338
    const/4 v1, 0x0

    .line 344
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 346
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 350
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :cond_0
    :goto_1
    return-object p0

    .line 342
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0

    .line 352
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmOmVfsPath2Node(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;
    .locals 9
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 602
    const-string v5, ""

    .line 603
    .local v5, "szNodeName":Ljava/lang/String;
    move-object v6, p1

    .line 604
    .local v6, "szTempPath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 606
    .local v4, "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 608
    .local v3, "ptBaseNode":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 631
    :cond_0
    :goto_0
    return-object v7

    .line 611
    :cond_1
    const-string v8, "."

    invoke-virtual {p1, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "./"

    invoke-virtual {p1, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_3

    .line 613
    :cond_2
    iget-object v7, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_0

    .line 616
    :cond_3
    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 617
    .local v2, "nodenamesplit":[Ljava/lang/String;
    array-length v1, v2

    .line 618
    .local v1, "l":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_4

    move-object v7, v4

    .line 631
    goto :goto_0

    .line 620
    :cond_4
    aget-object v5, v2, v0

    .line 622
    invoke-static {p0, v5, v3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v4

    .line 623
    if-eqz v4, :cond_0

    .line 628
    move-object v3, v4

    .line 618
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static xdmOmVfsRemoveNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Z)I
    .locals 7
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p2, "deletechilds"    # Z

    .prologue
    const/4 v4, -0x4

    const/4 v6, 0x0

    .line 856
    iget-object v0, p1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 860
    .local v0, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v0, :cond_3

    .line 862
    if-nez p2, :cond_2

    move v2, v4

    .line 930
    :cond_0
    :goto_0
    return v2

    .line 869
    :cond_1
    const/4 v5, 0x1

    invoke-static {p0, v0, v5}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsRemoveNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Z)I

    move-result v2

    .line 870
    .local v2, "ret":I
    if-nez v2, :cond_0

    .line 875
    iget-object v5, p1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    if-nez v5, :cond_4

    .line 877
    const/4 v0, 0x0

    .line 867
    .end local v2    # "ret":I
    :cond_2
    :goto_1
    if-nez v0, :cond_1

    .line 886
    :cond_3
    iget-object v5, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    invoke-static {p0, v5, p1}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetParent(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 887
    .local v1, "ptParent":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v1, :cond_5

    move v2, v4

    .line 889
    goto :goto_0

    .line 881
    .end local v1    # "ptParent":Lcom/fmm/dm/eng/core/XDMVnode;
    .restart local v2    # "ret":I
    :cond_4
    iget-object v0, p1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_1

    .line 891
    .end local v2    # "ret":I
    .restart local v1    # "ptParent":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_5
    iget-object v4, v1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    if-ne v4, p1, :cond_9

    .line 893
    iget-object v3, p1, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 894
    .local v3, "tmp":Lcom/fmm/dm/eng/core/XDMVnode;
    iput-object v3, v1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 910
    .end local v3    # "tmp":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_6
    :goto_2
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v4, :cond_7

    .line 912
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsDeleteAclList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 914
    :cond_7
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v4, :cond_8

    .line 916
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 919
    :cond_8
    iput-object v6, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 920
    iput-object v6, p1, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 921
    iput-object v6, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szTstamp:Ljava/lang/String;

    .line 922
    iput-object v6, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szDdfName:Ljava/lang/String;

    .line 924
    iput-object v6, p1, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 927
    iput-object v6, p1, Lcom/fmm/dm/eng/core/XDMVnode;->ptParentNode:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 928
    const/4 p1, 0x0

    .line 930
    const/4 v2, 0x0

    goto :goto_0

    .line 898
    :cond_9
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 899
    :goto_3
    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    if-eqz v4, :cond_6

    .line 901
    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    if-ne v4, p1, :cond_a

    .line 903
    iget-object v4, p1, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    iput-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_2

    .line 906
    :cond_a
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_3
.end method

.method private xdmOmVfsResetStdobj()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->stdobj_space:[B

    .line 34
    const v0, 0xa000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->stdobj_space:[B

    .line 35
    return-void
.end method

.method public static xdmOmVfsSaveFs(Lcom/fmm/dm/eng/core/XDMOmVfs;)I
    .locals 8
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    const/4 v0, 0x0

    .line 184
    .local v0, "Data":Ljava/io/DataOutputStream;
    const/4 v3, 0x0

    .line 186
    .local v3, "nFileId":I
    const/4 v5, 0x0

    .line 187
    .local v5, "szFileName":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdObjectTreeInfo()I

    move-result v3

    .line 188
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v5

    .line 192
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .local v1, "Data":Ljava/io/DataOutputStream;
    :try_start_1
    iget-object v6, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    iget-object v4, v6, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .local v4, "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    move-object v0, v1

    .line 195
    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    :goto_0
    if-nez v4, :cond_0

    .line 200
    :try_start_2
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 207
    .end local v4    # "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    :goto_1
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdObjectData()I

    move-result v3

    .line 208
    const v6, 0xa000

    iget-object v7, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->stdobj_space:[B

    invoke-static {v3, v6, v7}, Lcom/fmm/dm/db/file/XDB;->xdbWriteFile(IILjava/lang/Object;)Z

    .line 210
    const/4 v6, 0x0

    return v6

    .line 197
    .restart local v4    # "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_0
    :try_start_3
    invoke-static {v0, v4}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPackFsNode(Ljava/io/DataOutputStream;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;

    move-result-object v0

    .line 198
    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 202
    .end local v4    # "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    :catch_0
    move-exception v2

    .line 204
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 202
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    goto :goto_2
.end method

.method public static xdmOmVfsSaveFsData(Lcom/fmm/dm/eng/core/XDMOmVfs;ILjava/lang/Object;I)I
    .locals 6
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "l"    # I
    .param p2, "pBuff"    # Ljava/lang/Object;
    .param p3, "nSize"    # I

    .prologue
    .line 832
    const/4 v1, 0x0

    .line 833
    .local v1, "i":I
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 834
    .local v2, "szTmp":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 835
    .local v0, "data":[B
    const/4 v1, 0x0

    :goto_0
    if-lt v1, p3, :cond_1

    .line 841
    :cond_0
    const/4 v3, 0x0

    return v3

    .line 837
    :cond_1
    array-length v3, v0

    if-le v3, v1, :cond_0

    .line 839
    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMOmVfs;->stdobj_space:[B

    add-int v4, p1, v1

    aget-byte v5, v0, v1

    aput-byte v5, v3, v4

    .line 835
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xdmOmVfsSetData(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Ljava/lang/Object;I)I
    .locals 2
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p2, "pBuff"    # Ljava/lang/Object;
    .param p3, "nBuffSize"    # I

    .prologue
    .line 1020
    invoke-static {p0, p3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetFreeVaddr(Lcom/fmm/dm/eng/core/XDMOmVfs;I)I

    move-result v0

    .line 1021
    .local v0, "addr":I
    if-gez v0, :cond_0

    .line 1035
    .end local v0    # "addr":I
    :goto_0
    return v0

    .line 1026
    .restart local v0    # "addr":I
    :cond_0
    iput v0, p1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    .line 1027
    iput p3, p1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 1029
    invoke-static {p0, v0, p2, p3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsSaveFsData(Lcom/fmm/dm/eng/core/XDMOmVfs;ILjava/lang/Object;I)I

    move-result v1

    .line 1030
    .local v1, "ret":I
    if-eqz v1, :cond_1

    .line 1032
    const/4 v0, -0x4

    goto :goto_0

    .line 1035
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmOmVfsUnpackFsNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/io/DataInputStream;ILcom/fmm/dm/eng/core/XDMVnode;[BI)I
    .locals 9
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "in"    # Ljava/io/DataInputStream;
    .param p2, "pBuff"    # I
    .param p3, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p4, "buf"    # [B
    .param p5, "nSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v8, 0x42

    const/4 v6, 0x0

    const/16 v7, 0x44

    .line 376
    move v2, p2

    .line 378
    .local v2, "ptr":I
    if-nez p1, :cond_2

    move v0, v6

    .line 414
    :goto_0
    return v0

    .line 383
    :cond_0
    if-ne v2, v8, :cond_5

    .line 385
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 386
    sget v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 387
    new-instance v3, Lcom/fmm/dm/eng/core/XDMVnode;

    invoke-direct {v3}, Lcom/fmm/dm/eng/core/XDMVnode;-><init>()V

    .line 388
    .local v3, "ptChild":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-static {p1, v3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackNode(Ljava/io/DataInputStream;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/io/DataInputStream;

    move-result-object p1

    .line 389
    invoke-static {p3, v3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsAppendNode(Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)I

    .line 391
    sget v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    aget-byte v2, p4, v0

    .line 392
    :cond_1
    :goto_1
    if-ne v2, v7, :cond_3

    .line 381
    .end local v3    # "ptChild":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_2
    if-ne v2, v7, :cond_0

    .line 409
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 410
    sget v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 411
    sget v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    if-ne v0, p5, :cond_6

    move v0, v7

    .line 412
    goto :goto_0

    .line 394
    .restart local v3    # "ptChild":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_3
    if-ne v2, v8, :cond_4

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    move v5, p5

    .line 396
    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackFsNode(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/io/DataInputStream;ILcom/fmm/dm/eng/core/XDMVnode;[BI)I

    move-result v2

    .line 397
    goto :goto_1

    .line 398
    :cond_4
    if-eq v2, v7, :cond_1

    move v0, v6

    .line 400
    goto :goto_0

    .line 404
    .end local v3    # "ptChild":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_5
    if-eq v2, v7, :cond_2

    move v0, v6

    .line 406
    goto :goto_0

    .line 414
    :cond_6
    sget v0, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    aget-byte v0, p4, v0

    goto :goto_0
.end method

.method public static xdmOmVfsUnpackNode(Ljava/io/DataInputStream;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/io/DataInputStream;
    .locals 8
    .param p0, "in"    # Ljava/io/DataInputStream;
    .param p1, "ptNode"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 419
    const/4 v4, 0x0

    .line 422
    .local v4, "num":I
    const/4 v6, 0x0

    .line 424
    .local v6, "szTmp":Ljava/lang/String;
    const-string v5, ""

    .line 426
    .local v5, "szData":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 427
    sget v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 428
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_1

    .line 441
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 442
    sget v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 443
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 444
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 445
    sget v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 446
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 447
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szTstamp:Ljava/lang/String;

    .line 448
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->scope:I

    .line 449
    sget v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 450
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 451
    sget v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 452
    if-lez v4, :cond_0

    .line 454
    const/4 v2, 0x0

    :goto_1
    if-lt v2, v4, :cond_2

    .line 463
    :cond_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->verno:I

    .line 464
    sget v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 465
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szDdfName:Ljava/lang/String;

    .line 466
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    .line 467
    sget v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 469
    return-object p0

    .line 430
    :cond_1
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStr(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v6

    .line 431
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 432
    .local v0, "ac":I
    sget v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 434
    new-instance v1, Lcom/fmm/dm/eng/core/XDMOmAcl;

    invoke-direct {v1}, Lcom/fmm/dm/eng/core/XDMOmAcl;-><init>()V

    .line 435
    .local v1, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    iput-object v6, v1, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    .line 436
    iput v0, v1, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    .line 437
    new-instance v3, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v3}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 438
    .local v3, "item":Lcom/fmm/dm/eng/core/XDMOmList;
    iput-object v1, v3, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 439
    iget-object v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v7, v3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsAppendList(Lcom/fmm/dm/eng/core/XDMOmList;Lcom/fmm/dm/eng/core/XDMOmList;)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object v7

    iput-object v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 428
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 456
    .end local v0    # "ac":I
    .end local v1    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    .end local v3    # "item":Lcom/fmm/dm/eng/core/XDMOmList;
    :cond_2
    invoke-static {p0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStr(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v6

    .line 457
    move-object v5, v6

    .line 458
    new-instance v3, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v3}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 459
    .restart local v3    # "item":Lcom/fmm/dm/eng/core/XDMOmList;
    iput-object v5, v3, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 460
    iget-object v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v7, v3}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsAppendList(Lcom/fmm/dm/eng/core/XDMOmList;Lcom/fmm/dm/eng/core/XDMOmList;)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object v7

    iput-object v7, p1, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 454
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static xdmOmVfsUnpackStr(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 5
    .param p0, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 474
    const/4 v0, 0x0

    .line 475
    .local v0, "len":I
    const/4 v2, 0x0

    .line 476
    .local v2, "tmp":[B
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 477
    sget v3, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v3, v3, 0x4

    sput v3, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 479
    if-nez v0, :cond_0

    .line 481
    const/4 v1, 0x0

    .line 494
    :goto_0
    return-object v1

    .line 483
    :cond_0
    const/16 v3, 0x200

    if-le v0, v3, :cond_1

    .line 485
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "OM_MAX_LEN over"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 488
    :cond_1
    new-array v2, v0, [B

    .line 489
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v0}, Ljava/io/DataInputStream;->read([BII)I

    .line 491
    sget v3, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/2addr v3, v0

    sput v3, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 493
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 494
    .local v1, "szRet":Ljava/lang/String;
    goto :goto_0
.end method

.method public static xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 5
    .param p0, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 500
    const/4 v2, 0x0

    .line 501
    .local v2, "tmp":[B
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 502
    .local v0, "len":I
    sget v3, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v3, v3, 0x4

    sput v3, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 504
    if-nez v0, :cond_0

    .line 506
    const/4 v1, 0x0

    .line 520
    :goto_0
    return-object v1

    .line 508
    :cond_0
    const/16 v3, 0x200

    if-le v0, v3, :cond_1

    .line 510
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "OM_MAX_LEN over"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 513
    :cond_1
    new-array v2, v0, [B

    .line 514
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v0}, Ljava/io/DataInputStream;->read([BII)I

    .line 516
    sget v3, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    add-int/2addr v3, v0

    sput v3, Lcom/fmm/dm/eng/core/XDMOmVfs;->index:I

    .line 518
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 520
    .local v1, "szRet":Ljava/lang/String;
    goto :goto_0
.end method

.method public static xdmOmVfsWriteObj(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;IILjava/lang/Object;I)I
    .locals 5
    .param p0, "pVfs"    # Lcom/fmm/dm/eng/core/XDMOmVfs;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "nTotalSize"    # I
    .param p3, "nOffset"    # I
    .param p4, "pBuff"    # Ljava/lang/Object;
    .param p5, "nBuffSize"    # I

    .prologue
    .line 708
    const/4 v0, 0x0

    .line 712
    .local v0, "addr":I
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsPath2Node(Lcom/fmm/dm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v2

    .line 713
    .local v2, "ptNode":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v2, :cond_0

    .line 715
    const/4 v4, -0x1

    .line 744
    :goto_0
    return v4

    .line 717
    :cond_0
    if-nez p3, :cond_3

    .line 720
    invoke-static {p0, p2}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetFreeVaddr(Lcom/fmm/dm/eng/core/XDMOmVfs;I)I

    move-result v0

    .line 721
    if-gez v0, :cond_1

    move v4, v0

    .line 723
    goto :goto_0

    .line 725
    :cond_1
    iput v0, v2, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    .line 726
    iput p2, v2, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 733
    :goto_1
    add-int v1, p3, p5

    .line 734
    .local v1, "blocksize":I
    if-le v1, p2, :cond_2

    .line 736
    sub-int v4, v1, p2

    sub-int/2addr p5, v4

    .line 739
    :cond_2
    add-int v4, v0, p3

    invoke-static {p0, v4, p4, p5}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsSaveFsData(Lcom/fmm/dm/eng/core/XDMOmVfs;ILjava/lang/Object;I)I

    move-result v3

    .line 740
    .local v3, "ret":I
    if-eqz v3, :cond_4

    .line 742
    const/4 v4, -0x4

    goto :goto_0

    .line 730
    .end local v1    # "blocksize":I
    .end local v3    # "ret":I
    :cond_3
    iget v0, v2, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    goto :goto_1

    .restart local v1    # "blocksize":I
    .restart local v3    # "ret":I
    :cond_4
    move v4, p5

    .line 744
    goto :goto_0
.end method
