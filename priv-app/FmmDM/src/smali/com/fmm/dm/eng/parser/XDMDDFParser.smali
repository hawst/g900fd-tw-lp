.class public Lcom/fmm/dm/eng/parser/XDMDDFParser;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "XDMDDFParser.java"

# interfaces
.implements Lcom/fmm/dm/eng/core/XDMWbxml;
.implements Lcom/fmm/dm/eng/core/XDMXml;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;


# static fields
.field public static CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

.field public static XmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

.field public static bNodeChangeMode:Z

.field public static gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

.field public static gTndsWbxmlHeaderInfo:[C

.field public static g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

.field public static g_szDmManagementObjectIdPath:[Ljava/lang/String;

.field public static g_szDmManagementObjectIdType:[Ljava/lang/String;

.field public static g_szDmXmlOmaTags:[Ljava/lang/String;

.field public static g_szDmXmlTagString:[Ljava/lang/String;

.field public static g_szNewAccPath:Ljava/lang/String;

.field public static g_szTndsTokenStr:[Ljava/lang/String;

.field public static gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;


# instance fields
.field public gTagCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/16 v3, 0x2f

    .line 99
    const/16 v0, 0x3d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 100
    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 101
    const-string v2, ""

    aput-object v2, v0, v1

    .line 102
    const-string v1, ""

    aput-object v1, v0, v4

    .line 103
    const-string v1, ""

    aput-object v1, v0, v5

    .line 104
    const-string v1, ""

    aput-object v1, v0, v6

    .line 105
    const-string v1, "AccessType"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 106
    const-string v2, "ACL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 107
    const-string v2, "Add"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 108
    const-string v2, "b64"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 109
    const-string v2, "bin"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 110
    const-string v2, "bool"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 111
    const-string v2, "chr"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 112
    const-string v2, "CaseSense"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 113
    const-string v2, "CIS"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 114
    const-string v2, "Copy"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 115
    const-string v2, "CS"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 116
    const-string v2, "data"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 117
    const-string v2, "DDFName"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 118
    const-string v2, "DefaultValue"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 119
    const-string v2, "Delete"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 120
    const-string v2, "Description"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 121
    const-string v2, "DFFormat"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 122
    const-string v2, "DFProperties"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 123
    const-string v2, "DFTitle"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 124
    const-string v2, "DFType"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 125
    const-string v2, "Dynamic"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 126
    const-string v2, "Exec"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 127
    const-string v2, "float"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 128
    const-string v2, "Format"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 129
    const-string v2, "Get"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 130
    const-string v2, "int"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 131
    const-string v2, "Man"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 132
    const-string v2, "MgmtTree"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 133
    const-string v2, "MIME"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 134
    const-string v2, "Mod"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 135
    const-string v2, "Name"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 136
    const-string v2, "Node"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 137
    const-string v2, "node"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 138
    const-string v2, "NodeName"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 139
    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 140
    const-string v2, "Occurrence"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 141
    const-string v2, "One"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 142
    const-string v2, "OneOrMore"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 143
    const-string v2, "OneOrN"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 144
    const-string v2, "Path"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 145
    const-string v2, "Permanent"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 146
    const-string v2, "Replace"

    aput-object v2, v0, v1

    .line 147
    const-string v1, "RTProperties"

    aput-object v1, v0, v3

    const/16 v1, 0x30

    .line 148
    const-string v2, "Scope"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    .line 149
    const-string v2, "Size"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    .line 150
    const-string v2, "time"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 151
    const-string v2, "Title"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    .line 152
    const-string v2, "TStamp"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    .line 153
    const-string v2, "Type"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 154
    const-string v2, "Value"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 155
    const-string v2, "VerDTD"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    .line 156
    const-string v2, "VerNo"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    .line 157
    const-string v2, "xml"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    .line 158
    const-string v2, "ZeroOrMore"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    .line 159
    const-string v2, "ZeroOrN"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    .line 160
    const-string v2, "ZeroOrOne"

    aput-object v2, v0, v1

    .line 99
    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    .line 162
    const/16 v0, 0x2a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 163
    const-string v2, "<MgmtTree>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 164
    const-string v2, "</MgmtTree>\n"

    aput-object v2, v0, v1

    .line 165
    const-string v1, "<VerDTD>"

    aput-object v1, v0, v4

    .line 166
    const-string v1, "</VerDTD>\n"

    aput-object v1, v0, v5

    .line 167
    const-string v1, "<Node>"

    aput-object v1, v0, v6

    .line 168
    const-string v1, "</Node>\n"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 169
    const-string v2, "<NodeName>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 170
    const-string v2, "</NodeName>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 171
    const-string v2, "<Path>"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 172
    const-string v2, "</Path>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 173
    const-string v2, "<Value>"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 174
    const-string v2, "</Value>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 175
    const-string v2, "<RTProperties>"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 176
    const-string v2, "</RTProperties>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 177
    const-string v2, "<ACL>"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 178
    const-string v2, "</ACL>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 179
    const-string v2, "<Format>"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 180
    const-string v2, "</Format>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 181
    const-string v2, "<Type>"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 182
    const-string v2, "</Type>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 183
    const-string v2, "<Add>"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 184
    const-string v2, "</Add>"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 185
    const-string v2, "<Get>"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 186
    const-string v2, "</Get>"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 187
    const-string v2, "<Replace>"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 188
    const-string v2, "</Replace>"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 189
    const-string v2, "<Delete>"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 190
    const-string v2, "</Delete>"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 191
    const-string v2, "<Exec>"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 192
    const-string v2, "</Exec>"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 193
    const-string v2, "<AccessType>"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 194
    const-string v2, "</AccessType>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 195
    const-string v2, "<![CDATA["

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 196
    const-string v2, "]]>"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 197
    const-string v2, "<SyncML>"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 198
    const-string v2, "</SyncML>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 199
    const-string v2, "<ResultCode>"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 200
    const-string v2, "</ResultCode>"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 201
    const-string v2, "<Identifier>"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 202
    const-string v2, "</Identifier>"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 203
    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 204
    const-string v2, ""

    aput-object v2, v0, v1

    .line 162
    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    .line 206
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x1

    .line 208
    const-string v2, "./SyncML/DMAcc"

    aput-object v2, v0, v1

    .line 209
    const-string v1, "./SyncML/DMAcc"

    aput-object v1, v0, v4

    .line 210
    const-string v1, "./SyncML/DMAcc"

    aput-object v1, v0, v5

    .line 211
    const-string v1, "./SyncML/DMAcc"

    aput-object v1, v0, v6

    .line 212
    const-string v1, "./SyncML/DMAcc"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 213
    const-string v2, "./SyncML/DMAcc"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 214
    const-string v2, "./SyncML/DMAcc"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 215
    const-string v2, "./DevInfo"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 216
    const-string v2, "./DevDetail"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 217
    const-string v2, "./Inbox"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 218
    const-string v2, "./FUMO"

    aput-object v2, v0, v1

    .line 206
    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmManagementObjectIdPath:[Ljava/lang/String;

    .line 221
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x1

    .line 223
    const-string v2, "org.openmobilealliance/1.0/w1"

    aput-object v2, v0, v1

    .line 224
    const-string v1, "org.openmobilealliance/1.0/w2"

    aput-object v1, v0, v4

    .line 225
    const-string v1, "org.openmobilealliance/1.0/w3"

    aput-object v1, v0, v5

    .line 226
    const-string v1, "org.openmobilealliance/1.0/w4"

    aput-object v1, v0, v6

    .line 227
    const-string v1, "org.openmobilealliance/1.0/w5"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 228
    const-string v2, "org.openmobilealliance/1.0/w6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 229
    const-string v2, "org.openmobilealliance/1.0/w7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 230
    const-string v2, "org.openmobilealliance.dm/1.0/DevInfo"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 231
    const-string v2, "org.openmobilealliance.dm/1.0/DevDetail"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 232
    const-string v2, "org.openmobilealliance.dm/1.0/Inbox"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 233
    const-string v2, "org.openmobilealliance/1.0/FirmwareUpdateManagementObject"

    aput-object v2, v0, v1

    .line 221
    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmManagementObjectIdType:[Ljava/lang/String;

    .line 236
    const/16 v0, 0x38

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 237
    const-string v2, "AccessType"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 238
    const-string v2, "ACL"

    aput-object v2, v0, v1

    .line 239
    const-string v1, "Add"

    aput-object v1, v0, v4

    .line 240
    const-string v1, "b64"

    aput-object v1, v0, v5

    .line 241
    const-string v1, "bin"

    aput-object v1, v0, v6

    .line 242
    const-string v1, "bool"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 243
    const-string v2, "chr"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 244
    const-string v2, "CaseSense"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 245
    const-string v2, "CIS"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 246
    const-string v2, "Copy"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 247
    const-string v2, "CS"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 248
    const-string v2, "date"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 249
    const-string v2, "DDFName"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 250
    const-string v2, "DefaultValue"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 251
    const-string v2, "Delete"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 252
    const-string v2, "Description"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 253
    const-string v2, "DFFormat"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 254
    const-string v2, "DFProperties"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 255
    const-string v2, "DFTitle"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 256
    const-string v2, "DFType"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 257
    const-string v2, "Dynamic"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 258
    const-string v2, "Exec"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 259
    const-string v2, "float"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 260
    const-string v2, "Format"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 261
    const-string v2, "Get"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 262
    const-string v2, "int"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 263
    const-string v2, "Man"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 264
    const-string v2, "MgmtTree"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 265
    const-string v2, "MIME"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 266
    const-string v2, "Mod"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 267
    const-string v2, "Name"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 268
    const-string v2, "Node"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 269
    const-string v2, "node"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 270
    const-string v2, "NodeName"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 271
    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 272
    const-string v2, "Occurrence"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 273
    const-string v2, "One"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 274
    const-string v2, "OneOrMore"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 275
    const-string v2, "OneOrN"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 276
    const-string v2, "Path"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 277
    const-string v2, "Permanent"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 278
    const-string v2, "Replace"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 279
    const-string v2, "RTProperties"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 280
    const-string v2, "Scope"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 281
    const-string v2, "Size"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 282
    const-string v2, "time"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 283
    const-string v2, "Title"

    aput-object v2, v0, v1

    .line 284
    const-string v1, "TStamp"

    aput-object v1, v0, v3

    const/16 v1, 0x30

    .line 285
    const-string v2, "Type"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    .line 286
    const-string v2, "Value"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    .line 287
    const-string v2, "VerDTD"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 288
    const-string v2, "VerNo"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    .line 289
    const-string v2, "xml"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    .line 290
    const-string v2, "ZerOrMore"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 291
    const-string v2, "ZeroOrN"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 292
    const-string v2, "ZeroOrOne"

    aput-object v2, v0, v1

    .line 236
    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szTndsTokenStr:[Ljava/lang/String;

    .line 294
    const/16 v0, 0x1f

    new-array v0, v0, [C

    const/4 v1, 0x0

    .line 295
    aput-char v4, v0, v1

    .line 298
    const/16 v1, 0x6a

    aput-char v1, v0, v5

    .line 299
    const/16 v1, 0x1a

    aput-char v1, v0, v6

    .line 300
    const/16 v1, 0x2d

    aput-char v1, v0, v7

    const/4 v1, 0x6

    .line 301
    aput-char v3, v0, v1

    const/4 v1, 0x7

    .line 302
    aput-char v3, v0, v1

    const/16 v1, 0x8

    .line 303
    const/16 v2, 0x4f

    aput-char v2, v0, v1

    const/16 v1, 0x9

    .line 304
    const/16 v2, 0x4d

    aput-char v2, v0, v1

    const/16 v1, 0xa

    .line 305
    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/16 v1, 0xb

    .line 306
    aput-char v3, v0, v1

    const/16 v1, 0xc

    .line 307
    aput-char v3, v0, v1

    const/16 v1, 0xd

    .line 308
    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0xe

    .line 309
    const/16 v2, 0x54

    aput-char v2, v0, v1

    const/16 v1, 0xf

    .line 310
    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0x10

    .line 311
    const/16 v2, 0x2d

    aput-char v2, v0, v1

    const/16 v1, 0x11

    .line 312
    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0x12

    .line 313
    const/16 v2, 0x4d

    aput-char v2, v0, v1

    const/16 v1, 0x13

    .line 314
    const/16 v2, 0x2d

    aput-char v2, v0, v1

    const/16 v1, 0x14

    .line 315
    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0x15

    .line 316
    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0x16

    .line 317
    const/16 v2, 0x46

    aput-char v2, v0, v1

    const/16 v1, 0x17

    .line 318
    const/16 v2, 0x20

    aput-char v2, v0, v1

    const/16 v1, 0x18

    .line 319
    const/16 v2, 0x31

    aput-char v2, v0, v1

    const/16 v1, 0x19

    .line 320
    const/16 v2, 0x2e

    aput-char v2, v0, v1

    const/16 v1, 0x1a

    .line 321
    const/16 v2, 0x32

    aput-char v2, v0, v1

    const/16 v1, 0x1b

    .line 322
    aput-char v3, v0, v1

    const/16 v1, 0x1c

    .line 323
    aput-char v3, v0, v1

    const/16 v1, 0x1d

    .line 324
    const/16 v2, 0x45

    aput-char v2, v0, v1

    const/16 v1, 0x1e

    .line 325
    const/16 v2, 0x4e

    aput-char v2, v0, v1

    .line 294
    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsWbxmlHeaderInfo:[C

    .line 326
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 92
    sput-boolean v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->bNodeChangeMode:Z

    .line 93
    new-instance v0, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;-><init>()V

    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .line 94
    iput v1, p0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTagCode:I

    .line 95
    new-instance v0, Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMOmTree;-><init>()V

    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 96
    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szNewAccPath:Ljava/lang/String;

    .line 97
    return-void
.end method

.method private static OMSETPATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V
    .locals 6
    .param p0, "g_om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "aclValue"    # I
    .param p3, "scope"    # I

    .prologue
    const/4 v2, 0x0

    .line 330
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 332
    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 333
    invoke-static {p0, p1, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 335
    :cond_0
    return-void
.end method

.method public static xdmAgentVerifyNewAccount(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "omt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szAccName"    # Ljava/lang/String;

    .prologue
    .line 912
    const-string v2, ""

    .line 913
    .local v2, "szTmpAccPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 914
    .local v1, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    const/4 v0, 0x0

    .line 916
    .local v0, "bRet":Z
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 918
    move-object v2, p1

    .line 919
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 920
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 921
    invoke-virtual {v2, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 923
    :cond_0
    invoke-static {p0, v2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 924
    if-nez v1, :cond_2

    .line 926
    sput-object v2, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szNewAccPath:Ljava/lang/String;

    .line 927
    const/4 v0, 0x1

    .line 937
    :cond_1
    :goto_0
    return v0

    .line 931
    :cond_2
    const/4 v3, 0x0

    sput-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szNewAccPath:Ljava/lang/String;

    .line 932
    const/4 v2, 0x0

    .line 933
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmConvertFinish()V
    .locals 1

    .prologue
    .line 2648
    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    .line 2649
    return-void
.end method

.method public static xdmDDFAllocConvertWbxmlData()Z
    .locals 2

    .prologue
    .line 2167
    const/4 v0, 0x0

    .line 2169
    .local v0, "szWbxmlBuf":Ljava/lang/String;
    const-string v0, ""

    .line 2170
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2172
    const-string v1, "# ERROR # xdmDDFAllocConvertWbxmlData Alloc Error !!! ###"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2173
    const/4 v1, 0x0

    .line 2178
    :goto_0
    return v1

    .line 2176
    :cond_0
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2177
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlDataStart(Ljava/lang/String;)V

    .line 2178
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xdmDDFCheckInbox(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "szType"    # Ljava/lang/String;

    .prologue
    .line 851
    const/4 v1, 0x0

    .line 852
    .local v1, "szId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 853
    .local v0, "i":I
    const-string v3, ""

    .line 854
    .local v3, "szPath":Ljava/lang/String;
    const-string v2, ""

    .line 856
    .local v2, "szOut":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 858
    const/4 v4, 0x0

    .line 886
    :goto_0
    return-object v4

    .line 861
    :cond_0
    const/4 v0, 0x1

    :goto_1
    const/16 v4, 0xc

    if-lt v0, v4, :cond_1

    :goto_2
    move-object v4, v2

    .line 886
    goto :goto_0

    .line 863
    :cond_1
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFGetMOType(I)Ljava/lang/String;

    move-result-object v1

    .line 864
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 861
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 869
    :cond_3
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 871
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFGetMOPath(I)Ljava/lang/String;

    move-result-object v3

    .line 872
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 874
    const-string v2, ""

    .line 875
    move-object v2, v3

    .line 876
    goto :goto_2

    .line 880
    :cond_4
    const-string v2, ""

    .line 881
    goto :goto_2
.end method

.method public static xdmDDFConvertAddTndsCodePage()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2603
    const/4 v2, 0x0

    .line 2605
    .local v2, "szWbxmlData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2608
    .local v0, "nTmp":C
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 2609
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 2611
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 2613
    .local v3, "tmpWbxml":[B
    add-int/lit8 v1, v1, -0x1

    .line 2614
    aget-byte v4, v3, v1

    int-to-char v0, v4

    .line 2616
    invoke-virtual {v2, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 2618
    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2619
    add-int/lit8 v1, v1, 0x1

    .line 2621
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2622
    add-int/lit8 v1, v1, 0x1

    .line 2624
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2625
    add-int/lit8 v1, v1, 0x1

    .line 2627
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2628
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2629
    return-void
.end method

.method public static xdmDDFConvertAddWbxmlHeader()V
    .locals 6

    .prologue
    const/16 v5, 0x1f

    .line 2183
    const/4 v2, 0x0

    .line 2185
    .local v2, "szWbxmlData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2186
    .local v0, "nIndex":I
    new-array v3, v5, [C

    .line 2188
    .local v3, "tmpWbxml":[C
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 2190
    .local v1, "nWbxmlSize":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 2196
    move v1, v0

    .line 2197
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    .line 2200
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2201
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2202
    return-void

    .line 2192
    :cond_0
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsWbxmlHeaderInfo:[C

    aget-char v4, v4, v0

    aput-char v4, v3, v0

    .line 2193
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static xdmDDFConvertCheckElement(C)V
    .locals 2
    .param p0, "cHex"    # C

    .prologue
    .line 2361
    int-to-byte v0, p0

    packed-switch v0, :pswitch_data_0

    .line 2405
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "# ERROR # What? [value : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]!!! ###"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2408
    :goto_0
    :pswitch_1
    return-void

    .line 2381
    :pswitch_2
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFProcessConvertStringData()V

    goto :goto_0

    .line 2391
    :pswitch_3
    invoke-static {p0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFProcessConvertHexData(C)V

    goto :goto_0

    .line 2395
    :pswitch_4
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertAddTndsCodePage()V

    goto :goto_0

    .line 2361
    nop

    :pswitch_data_0
    .packed-switch 0x45
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static xdmDDFConvertCheckTag()V
    .locals 3

    .prologue
    .line 2206
    const/4 v0, 0x0

    .line 2208
    .local v0, "szXmlData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v0

    .line 2209
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2211
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2f

    if-ne v1, v2, :cond_0

    .line 2214
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertXmlCloseTag()V

    .line 2221
    :goto_0
    return-void

    .line 2219
    :cond_0
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertXml2Wbxml()V

    goto :goto_0
.end method

.method public static xdmDDFConvertGetXMLTag(I)Ljava/lang/String;
    .locals 1
    .param p0, "nIndex"    # I

    .prologue
    .line 2356
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szTndsTokenStr:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static xdmDDFConvertSkip1Byte()V
    .locals 3

    .prologue
    .line 2633
    const/4 v1, 0x0

    .line 2636
    .local v1, "szXmlData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v1

    .line 2637
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v0

    .line 2639
    .local v0, "nXmlSize":I
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 2640
    add-int/lit8 v0, v0, -0x1

    .line 2642
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2643
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2644
    return-void
.end method

.method public static xdmDDFConvertString2WbxmlHex(Ljava/lang/String;)C
    .locals 6
    .param p0, "szString"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 2328
    const/4 v0, 0x0

    .line 2329
    .local v0, "nIndex":I
    const/4 v1, 0x0

    .line 2332
    .local v1, "nStringLen":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 2334
    :goto_0
    const/16 v3, 0x38

    if-ne v0, v3, :cond_0

    .line 2345
    const-string v3, "SyncML"

    const-string v4, "SyncML"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v5, v3, v5, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2347
    const/16 v3, 0x6d

    .line 2351
    :goto_1
    return v3

    .line 2336
    :cond_0
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertGetXMLTag(I)Ljava/lang/String;

    move-result-object v2

    .line 2338
    .local v2, "szXmlTag":Ljava/lang/String;
    invoke-virtual {p0, v5, v2, v5, v1}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2340
    add-int/lit8 v3, v0, 0x5

    add-int/lit8 v3, v3, 0x40

    int-to-char v3, v3

    goto :goto_1

    .line 2342
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2350
    .end local v2    # "szXmlTag":Ljava/lang/String;
    :cond_2
    const-string v3, "# ERROR # Not Found String !!! ###"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2351
    const v3, 0xffff

    goto :goto_1
.end method

.method public static xdmDDFConvertXml2Wbxml()V
    .locals 12

    .prologue
    const/16 v11, 0x3e

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2268
    const/4 v6, 0x0

    .line 2270
    .local v6, "szWbxmlData":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2274
    .local v7, "szXmlData":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2275
    .local v2, "nTagLen":I
    const-string v5, ""

    .line 2277
    .local v5, "szTag":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2280
    .local v0, "cHexValue":C
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v3

    .line 2281
    .local v3, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v6

    .line 2282
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v7

    .line 2283
    invoke-virtual {v7, v9}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2284
    .local v1, "nTag":C
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v4

    .line 2286
    .local v4, "nXmlSize":I
    const/16 v8, 0x3c

    if-ne v1, v8, :cond_0

    .line 2288
    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 2289
    invoke-virtual {v7, v9}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2291
    :goto_0
    if-ne v1, v11, :cond_2

    .line 2299
    add-int/lit8 v2, v2, 0x1

    .line 2302
    :cond_0
    if-ne v1, v11, :cond_1

    .line 2304
    add-int/lit8 v2, v2, 0x1

    .line 2305
    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 2307
    invoke-static {v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertString2WbxmlHex(Ljava/lang/String;)C

    move-result v0

    .line 2310
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2311
    add-int/lit8 v3, v3, 0x1

    .line 2313
    invoke-static {v3}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2314
    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2317
    sub-int v8, v4, v2

    invoke-static {v8}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2318
    invoke-static {v7}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2321
    :cond_1
    const/4 v5, 0x0

    .line 2323
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertCheckElement(C)V

    .line 2324
    return-void

    .line 2293
    :cond_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2294
    add-int/lit8 v2, v2, 0x1

    .line 2295
    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 2296
    invoke-virtual {v7, v9}, Ljava/lang/String;->charAt(I)C

    move-result v1

    goto :goto_0
.end method

.method public static xdmDDFConvertXmlCloseTag()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2225
    const/4 v3, 0x0

    .line 2227
    .local v3, "szWbxmlData":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2234
    .local v4, "szXmlData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 2235
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v3

    .line 2236
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v4

    .line 2237
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v2

    .line 2239
    .local v2, "nXmlSize":I
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 2240
    add-int/lit8 v2, v2, -0x2

    .line 2242
    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2244
    .local v0, "nTag":C
    :goto_0
    const/16 v5, 0x3e

    if-ne v0, v5, :cond_0

    .line 2251
    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 2252
    add-int/lit8 v2, v2, -0x1

    .line 2255
    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2256
    add-int/lit8 v1, v1, 0x1

    .line 2258
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2259
    invoke-static {v3}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2262
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2263
    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2264
    return-void

    .line 2246
    :cond_0
    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 2247
    add-int/lit8 v2, v2, -0x1

    .line 2248
    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method public static xdmDDFCreateNode(Lcom/fmm/dm/eng/parser/XDM_DM_Tree;Ljava/lang/String;)Z
    .locals 7
    .param p0, "pTree"    # Lcom/fmm/dm/eng/parser/XDM_DM_Tree;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 1181
    if-nez p0, :cond_1

    .line 1236
    :cond_0
    :goto_0
    return v3

    .line 1185
    :cond_1
    const/4 v0, 0x0

    .line 1186
    .local v0, "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->childlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 1188
    .local v1, "childlist":Lcom/fmm/dm/eng/core/XDMLinkedList;
    const/4 v4, 0x0

    .line 1191
    .local v4, "szPathTemp":Ljava/lang/String;
    if-nez v1, :cond_2

    move v3, v5

    .line 1193
    goto :goto_0

    .line 1196
    :cond_2
    invoke-static {v1, v3}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 1198
    :goto_1
    if-nez p0, :cond_3

    move v3, v5

    .line 1236
    goto :goto_0

    .line 1200
    :cond_3
    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "pTree":Lcom/fmm/dm/eng/parser/XDM_DM_Tree;
    check-cast p0, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .line 1202
    .restart local p0    # "pTree":Lcom/fmm/dm/eng/parser/XDM_DM_Tree;
    if-nez p0, :cond_4

    move v3, v5

    .line 1204
    goto :goto_0

    .line 1207
    :cond_4
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .end local v0    # "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    check-cast v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    .line 1209
    .restart local v0    # "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFXmlTagCode(Ljava/lang/String;)I

    move-result v2

    .line 1210
    .local v2, "nTagCode":I
    packed-switch v2, :pswitch_data_0

    .line 1230
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1231
    sget-object v6, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 1213
    :pswitch_0
    iput-object p1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 1214
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateNodeToOM(Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;)Z

    move-result v3

    .line 1215
    .local v3, "ret":Z
    if-eqz v3, :cond_0

    .line 1220
    const-string v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1221
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1222
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateNode(Lcom/fmm/dm/eng/parser/XDM_DM_Tree;Ljava/lang/String;)Z

    .line 1225
    const/16 v6, 0x2f

    invoke-static {p1, v6}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrrchr(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v4

    .line 1226
    move-object p1, v4

    .line 1227
    goto :goto_1

    .line 1210
    nop

    :pswitch_data_0
    .packed-switch 0x24
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmDDFCreateNodeToOM(Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;)Z
    .locals 14
    .param p0, "element"    # Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    .prologue
    .line 992
    iget v6, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->scope:I

    .line 993
    .local v6, "scope":I
    iget v7, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    .line 994
    .local v7, "format":I
    iget v5, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    .line 995
    .local v5, "aclValue":I
    iget-object v2, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    .line 996
    .local v2, "szData":Ljava/lang/String;
    const/4 v3, 0x0

    .line 997
    .local v3, "nLen":I
    const-string v1, ""

    .line 998
    .local v1, "szNodeName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 999
    .local v8, "bRet":Z
    const-string v9, ""

    .line 1001
    .local v9, "szTmpBuf":Ljava/lang/String;
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1003
    const-string v0, "Not exist nodename."

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1004
    const/4 v0, 0x0

    .line 1092
    :goto_0
    return v0

    .line 1009
    :cond_0
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    const-string v4, "."

    invoke-static {v0, v4}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1011
    const/4 v8, 0x0

    .line 1018
    :goto_1
    if-nez v8, :cond_1

    .line 1020
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1022
    const-string v0, "."

    invoke-virtual {v9, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1023
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1024
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 1026
    iput-object v9, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 1035
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    const/4 v10, 0x1

    invoke-static {v0, v4, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetXNodePath(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1037
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    const-string v4, "AAuthData"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1039
    const/4 v7, 0x1

    .line 1043
    :cond_2
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 1044
    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1045
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1047
    if-nez v5, :cond_3

    .line 1049
    const/16 v5, 0x1b

    .line 1052
    :cond_3
    if-nez v6, :cond_4

    .line 1054
    const/4 v6, 0x2

    .line 1057
    :cond_4
    const/16 v0, 0xc

    if-ne v7, v0, :cond_5

    .line 1059
    const/4 v7, 0x6

    .line 1062
    :cond_5
    const/4 v0, 0x6

    if-eq v7, v0, :cond_6

    const/4 v0, 0x7

    if-eq v7, v0, :cond_6

    const/16 v0, 0xc

    if-ne v7, v0, :cond_9

    .line 1064
    :cond_6
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFSetOMTree(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;III)V

    .line 1065
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-static {v0, v1, v5, v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->OMSETPATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1090
    :goto_3
    const/4 v1, 0x0

    .line 1092
    const/4 v0, 0x1

    goto :goto_0

    .line 1015
    :cond_7
    const/4 v8, 0x1

    goto :goto_1

    .line 1030
    :cond_8
    const-string v0, "."

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    goto :goto_2

    .line 1069
    :cond_9
    const/4 v0, 0x2

    if-ne v7, v0, :cond_b

    .line 1071
    const/4 v3, 0x0

    .line 1082
    :goto_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-ltz v0, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    int-to-long v10, v0

    const-wide/16 v12, 0x100

    cmp-long v0, v10, v12

    if-ltz v0, :cond_d

    .line 1084
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Size["

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]. Fatal ERROR."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1085
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1075
    :cond_b
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1077
    const-string v2, "null"

    .line 1078
    iput-object v2, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    .line 1080
    :cond_c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    goto :goto_4

    .line 1087
    :cond_d
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFSetOMTree(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;III)V

    goto :goto_3
.end method

.method public static xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/fmm/dm/eng/core/XDMOmTree;)I
    .locals 8
    .param p0, "szData"    # Ljava/lang/String;
    .param p1, "nLen"    # I
    .param p2, "omt"    # Lcom/fmm/dm/eng/core/XDMOmTree;

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1270
    const/4 v0, 0x0

    .line 1271
    .local v0, "ret":Z
    const/4 v2, 0x0

    .line 1273
    .local v2, "totalSize":I
    new-instance v3, Lcom/fmm/dm/eng/parser/XDM_XMLStream;

    invoke-direct {v3}, Lcom/fmm/dm/eng/parser/XDM_XMLStream;-><init>()V

    .line 1274
    .local v3, "xmlstream":Lcom/fmm/dm/eng/parser/XDM_XMLStream;
    new-instance v4, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    invoke-direct {v4}, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;-><init>()V

    .line 1276
    .local v4, "xmltree":Lcom/fmm/dm/eng/parser/XDM_DM_Tree;
    invoke-static {p0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFParseCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1278
    .local v1, "szTmpData":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1280
    invoke-static {p0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFParseCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/fmm/dm/eng/parser/XDM_XMLStream;->m_szData:Ljava/lang/String;

    .line 1281
    iput v2, v3, Lcom/fmm/dm/eng/parser/XDM_XMLStream;->size:I

    .line 1289
    :goto_0
    invoke-static {v3, v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFParsing(Lcom/fmm/dm/eng/parser/XDM_XMLStream;Lcom/fmm/dm/eng/parser/XDM_DM_Tree;)I

    move-result v6

    if-eqz v6, :cond_1

    .line 1291
    const-string v6, "Parsing Fail."

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1292
    sput-object v7, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 1319
    :goto_1
    return v5

    .line 1285
    :cond_0
    iput-object p0, v3, Lcom/fmm/dm/eng/parser/XDM_XMLStream;->m_szData:Ljava/lang/String;

    .line 1286
    iput p1, v3, Lcom/fmm/dm/eng/parser/XDM_XMLStream;->size:I

    goto :goto_0

    .line 1296
    :cond_1
    if-nez p2, :cond_2

    .line 1298
    const-string v6, "OM is NULL."

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1299
    sput-object v7, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    goto :goto_1

    .line 1303
    :cond_2
    sput-object p2, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 1305
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->XmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .line 1307
    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFXmlTagParsing(Lcom/fmm/dm/eng/parser/XDM_DM_Tree;)Z

    move-result v0

    .line 1309
    if-nez v0, :cond_3

    .line 1311
    const-string v6, "Create Node Fail. Check the xml file."

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1312
    sput-object v7, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    goto :goto_1

    .line 1316
    :cond_3
    const/4 v0, 0x1

    .line 1317
    const-string v5, "Success."

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1319
    const/4 v5, 0x1

    goto :goto_1
.end method

.method public static xdmDDFCreateTNDSNodeFromFile(ILcom/fmm/dm/eng/core/XDMOmTree;)I
    .locals 6
    .param p0, "nFileID"    # I
    .param p1, "omt"    # Lcom/fmm/dm/eng/core/XDMOmTree;

    .prologue
    const/4 v5, 0x0

    .line 1243
    const/4 v3, 0x0

    .line 1244
    .local v3, "ret":I
    const/4 v2, 0x0

    .line 1245
    .local v2, "r":Z
    const/4 v0, 0x0

    .line 1246
    .local v0, "nLen":I
    const/4 v1, 0x0

    .line 1247
    .local v1, "pData":[B
    const/4 v4, 0x0

    .line 1249
    .local v4, "szData":Ljava/lang/String;
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileSize(I)I

    move-result v0

    .line 1250
    if-gtz v0, :cond_1

    .line 1264
    :cond_0
    :goto_0
    return v5

    .line 1254
    :cond_1
    new-array v1, v0, [B

    .line 1255
    invoke-static {p0, v5, v0, v1}, Lcom/fmm/dm/db/file/XDB;->xdbReadFile(III[B)Z

    move-result v2

    .line 1257
    if-eqz v2, :cond_0

    .line 1261
    new-instance v4, Ljava/lang/String;

    .end local v4    # "szData":Ljava/lang/String;
    invoke-direct {v4, v1}, Ljava/lang/String;-><init>([B)V

    .line 1262
    .restart local v4    # "szData":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5, p1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v3

    move v5, v3

    .line 1264
    goto :goto_0
.end method

.method public static xdmDDFGetMOPath(I)Ljava/lang/String;
    .locals 2
    .param p0, "nId"    # I

    .prologue
    .line 891
    if-lez p0, :cond_0

    const/16 v0, 0xc

    if-lt p0, v0, :cond_1

    .line 893
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "wrong nId. ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 894
    const/4 v0, 0x0

    .line 896
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmManagementObjectIdPath:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static xdmDDFGetMOType(I)Ljava/lang/String;
    .locals 2
    .param p0, "nId"    # I

    .prologue
    .line 901
    if-lez p0, :cond_0

    const/16 v0, 0xc

    if-lt p0, v0, :cond_1

    .line 903
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "wrong nId. ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 904
    const/4 v0, 0x0

    .line 907
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmManagementObjectIdType:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static xdmDDFInitConvert(Ljava/lang/String;I)V
    .locals 1
    .param p0, "szInData"    # Ljava/lang/String;
    .param p1, "nInSize"    # I

    .prologue
    .line 2157
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMTndsData;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMTndsData;-><init>()V

    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    .line 2159
    invoke-static {p1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2160
    invoke-static {p0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2161
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFAllocConvertWbxmlData()Z

    .line 2162
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertAddWbxmlHeader()V

    .line 2163
    return-void
.end method

.method public static xdmDDFParseCDATA(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1324
    const-string v3, ""

    .line 1325
    .local v3, "szRet":Ljava/lang/String;
    const-string v4, ""

    .line 1326
    .local v4, "szStart":Ljava/lang/String;
    const-string v1, ""

    .line 1327
    .local v1, "szEnd":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1328
    .local v0, "cDataLen":I
    sget-object v7, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v8, 0x21

    aget-object v2, v7, v8

    .line 1329
    .local v2, "szEndStr":Ljava/lang/String;
    sget-object v7, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v8, 0x20

    aget-object v5, v7, v8

    .line 1331
    .local v5, "szStartStr":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1358
    :cond_0
    :goto_0
    return-object v6

    .line 1336
    :cond_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {p0, v5, v7}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrncmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    if-nez v7, :cond_0

    .line 1341
    invoke-static {p0, v2}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1343
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1348
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1349
    invoke-virtual {v4, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1351
    if-lez v0, :cond_0

    .line 1356
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    .line 1358
    goto :goto_0
.end method

.method public static xdmDDFParsing(Lcom/fmm/dm/eng/parser/XDM_XMLStream;Lcom/fmm/dm/eng/parser/XDM_DM_Tree;)I
    .locals 5
    .param p0, "stream"    # Lcom/fmm/dm/eng/parser/XDM_XMLStream;
    .param p1, "xmltree"    # Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .prologue
    .line 702
    const/4 v2, 0x0

    .line 704
    .local v2, "wRC":I
    if-eqz p0, :cond_0

    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDM_XMLStream;->m_szData:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    if-nez p1, :cond_1

    .line 706
    :cond_0
    const/16 v2, 0xbb9

    move v3, v2

    .line 721
    .end local v2    # "wRC":I
    .local v3, "wRC":I
    :goto_0
    return v3

    .line 710
    .end local v3    # "wRC":I
    .restart local v2    # "wRC":I
    :cond_1
    sput-object p1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .line 712
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;-><init>()V

    .line 713
    .local v0, "handler":Lcom/fmm/dm/eng/parser/XDMDDFParser;
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMXMLParse;

    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDM_XMLStream;->m_szData:Ljava/lang/String;

    invoke-direct {v1, v0, v4}, Lcom/fmm/dm/eng/parser/XDMXMLParse;-><init>(Lorg/xml/sax/helpers/DefaultHandler;Ljava/lang/String;)V

    .line 715
    .local v1, "parser":Lcom/fmm/dm/eng/parser/XDMXMLParse;
    if-nez v1, :cond_2

    .line 717
    const-string v4, "ERROR"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 718
    const/16 v2, 0xbba

    :cond_2
    move v3, v2

    .line 721
    .end local v2    # "wRC":I
    .restart local v3    # "wRC":I
    goto :goto_0
.end method

.method public static xdmDDFPrintNodePropert(Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;)V
    .locals 6
    .param p0, "element"    # Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    .prologue
    const-wide/16 v4, 0x400

    .line 942
    const-string v0, ""

    .line 944
    .local v0, "szPrintData":Ljava/lang/String;
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 946
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 949
    :cond_0
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 951
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    int-to-long v2, v1

    cmp-long v1, v4, v2

    if-gtz v1, :cond_1

    .line 953
    const-string v1, "Buffer Overflow. Increase the space. for element->name."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 954
    const/4 v0, 0x0

    .line 988
    :goto_0
    return-void

    .line 957
    :cond_1
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 958
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 961
    :cond_2
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 963
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    int-to-long v2, v1

    cmp-long v1, v4, v2

    if-gtz v1, :cond_3

    .line 965
    const-string v1, "Buffer Overflow. Increase the space. for element->data."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 966
    const/4 v0, 0x0

    .line 967
    goto :goto_0

    .line 969
    :cond_3
    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 970
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 973
    :cond_4
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 975
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    int-to-long v2, v1

    cmp-long v1, v4, v2

    if-gtz v1, :cond_5

    .line 977
    const-string v1, "Buffer Overflow. Increase the space. element->type."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 978
    const/4 v0, 0x0

    .line 979
    goto :goto_0

    .line 981
    :cond_5
    const-string v1, "] ["

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 982
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 984
    :cond_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 985
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[DDF]["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->scope:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 987
    :cond_7
    const/4 v0, 0x0

    .line 988
    goto/16 :goto_0
.end method

.method public static xdmDDFProcessConvertAccessTypeElement()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2552
    const/4 v4, 0x0

    .line 2554
    .local v4, "szWbxmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2558
    .local v5, "szXmlData":Ljava/lang/String;
    const-string v3, ""

    .line 2561
    .local v3, "szData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 2562
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v4

    .line 2563
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v5

    .line 2564
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2565
    .local v0, "nChar":C
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v2

    .line 2567
    .local v2, "nXmlSize":I
    const/16 v6, 0x3c

    if-ne v0, v6, :cond_0

    .line 2569
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2570
    add-int/lit8 v2, v2, -0x1

    .line 2571
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2572
    :goto_0
    const/16 v6, 0x2f

    if-ne v0, v6, :cond_1

    .line 2580
    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2581
    add-int/lit8 v2, v2, -0x2

    .line 2584
    :cond_0
    const/4 v0, 0x0

    .line 2585
    invoke-static {v3}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertString2WbxmlHex(Ljava/lang/String;)C

    move-result v0

    .line 2586
    add-int/lit8 v6, v0, -0x40

    int-to-char v0, v6

    .line 2589
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2590
    add-int/lit8 v1, v1, 0x1

    .line 2591
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2592
    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2595
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2596
    invoke-static {v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2598
    const/4 v3, 0x0

    .line 2599
    return-void

    .line 2574
    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2575
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2576
    add-int/lit8 v2, v2, -0x1

    .line 2578
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method public static xdmDDFProcessConvertFormatElement()V
    .locals 10

    .prologue
    const/16 v9, 0x3c

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2477
    const/4 v4, 0x0

    .line 2479
    .local v4, "szWbxmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2483
    .local v5, "szXmlData":Ljava/lang/String;
    const-string v3, ""

    .line 2486
    .local v3, "szData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 2487
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v4

    .line 2488
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v5

    .line 2489
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2490
    .local v0, "nChar":C
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v2

    .line 2492
    .local v2, "nXmlSize":I
    if-ne v0, v9, :cond_3

    .line 2494
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2495
    add-int/lit8 v2, v2, -0x1

    .line 2496
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2497
    :cond_0
    :goto_0
    const/16 v6, 0x2f

    if-ne v0, v6, :cond_2

    .line 2513
    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2514
    add-int/lit8 v2, v2, -0x2

    .line 2533
    :cond_1
    const/4 v0, 0x0

    .line 2534
    invoke-static {v3}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertString2WbxmlHex(Ljava/lang/String;)C

    move-result v0

    .line 2535
    add-int/lit8 v6, v0, -0x40

    int-to-char v0, v6

    .line 2538
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2539
    add-int/lit8 v1, v1, 0x1

    .line 2540
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2541
    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2544
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2545
    invoke-static {v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2547
    const/4 v3, 0x0

    .line 2548
    return-void

    .line 2499
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2501
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2502
    add-int/lit8 v2, v2, -0x1

    .line 2504
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2505
    const/16 v6, 0x20

    if-ne v0, v6, :cond_0

    .line 2507
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2508
    add-int/lit8 v2, v2, -0x1

    .line 2510
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0

    .line 2516
    :cond_3
    if-eq v0, v9, :cond_1

    .line 2518
    :goto_1
    if-eq v0, v9, :cond_1

    .line 2520
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2522
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2523
    add-int/lit8 v2, v2, -0x1

    .line 2525
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_1
.end method

.method public static xdmDDFProcessConvertHexData(C)V
    .locals 0
    .param p0, "cHex"    # C

    .prologue
    .line 2456
    sparse-switch p0, :sswitch_data_0

    .line 2473
    :goto_0
    return-void

    .line 2460
    :sswitch_0
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFProcessConvertFormatElement()V

    goto :goto_0

    .line 2464
    :sswitch_1
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFProcessConvertAccessTypeElement()V

    goto :goto_0

    .line 2456
    :sswitch_data_0
    .sparse-switch
        0x45 -> :sswitch_1
        0x55 -> :sswitch_0
        0x5c -> :sswitch_0
    .end sparse-switch
.end method

.method public static xdmDDFProcessConvertStringData()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2412
    const/4 v4, 0x0

    .line 2414
    .local v4, "szWbxmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2418
    .local v5, "szXmlData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 2421
    .local v1, "nStringLen":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 2422
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v4

    .line 2423
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v5

    .line 2424
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2425
    .local v0, "nChar":C
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 2428
    .local v3, "nXmlSize":I
    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2429
    add-int/lit8 v2, v2, 0x1

    .line 2432
    :goto_0
    const/16 v6, 0x3c

    if-ne v0, v6, :cond_0

    .line 2442
    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2443
    add-int/lit8 v2, v2, 0x1

    .line 2446
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2447
    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2450
    sub-int v6, v3, v1

    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2451
    invoke-static {v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2452
    return-void

    .line 2434
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2435
    add-int/lit8 v2, v2, 0x1

    .line 2436
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2437
    add-int/lit8 v1, v1, 0x1

    .line 2438
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method public static xdmDDFSetOMTree(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;III)V
    .locals 7
    .param p0, "omt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szData"    # Ljava/lang/String;
    .param p3, "nLen"    # I
    .param p4, "szMime"    # Ljava/lang/String;
    .param p5, "aclValue"    # I
    .param p6, "scope"    # I
    .param p7, "format"    # I

    .prologue
    const/4 v1, 0x3

    .line 1097
    const/4 v6, 0x0

    .line 1099
    .local v6, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v6

    .line 1101
    invoke-static {v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 1103
    if-eqz v6, :cond_0

    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p7

    .line 1105
    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFSetOMTreeProperty(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V

    .line 1106
    invoke-static {p0, p1, p5, p6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1109
    :cond_1
    sget-boolean v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->bNodeChangeMode:Z

    if-eqz v0, :cond_2

    .line 1111
    const-string v0, "bNodeChangeMode Change Node."

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p7

    .line 1112
    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFSetOMTreeProperty(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V

    .line 1113
    invoke-static {p0, p1, p5, p6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1115
    :cond_2
    return-void
.end method

.method public static xdmDDFSetOMTreeProperty(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 15
    .param p0, "omt"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szData"    # Ljava/lang/String;
    .param p3, "nLen"    # I
    .param p4, "szMime"    # Ljava/lang/String;
    .param p5, "format"    # I

    .prologue
    .line 1122
    const/16 v2, 0x100

    new-array v14, v2, [C

    .line 1123
    .local v14, "tmpbuf":[C
    const/4 v8, 0x0

    .line 1125
    .local v8, "index":I
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 1127
    const-string v13, ""

    .line 1129
    .local v13, "szTmp":Ljava/lang/String;
    :goto_0
    aget-char v2, v14, v8

    if-nez v2, :cond_5

    .line 1135
    invoke-static {p0, v13}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v12

    .line 1137
    .local v12, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v12, :cond_0

    .line 1139
    const/16 v2, 0x1b

    const/4 v3, 0x1

    invoke-static {p0, v13, v2, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmProcessCmdImplicitAdd(Ljava/lang/Object;Ljava/lang/String;II)Z

    .line 1142
    :cond_0
    const/4 v2, 0x6

    move/from16 v0, p5

    if-eq v0, v2, :cond_1

    const/4 v2, 0x7

    move/from16 v0, p5

    if-eq v0, v2, :cond_1

    const/16 v2, 0xc

    move/from16 v0, p5

    if-ne v0, v2, :cond_6

    .line 1144
    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, ""

    const/4 v7, 0x0

    move-object v2, p0

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 1155
    :cond_2
    :goto_1
    invoke-static/range {p0 .. p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v12

    .line 1156
    if-eqz v12, :cond_4

    .line 1158
    iget-object v2, v12, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v2, :cond_3

    .line 1160
    iget-object v2, v12, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 1163
    :cond_3
    new-instance v9, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v9}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 1165
    .local v9, "list":Lcom/fmm/dm/eng/core/XDMOmList;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1167
    move-object/from16 v0, p4

    iput-object v0, v9, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 1173
    :goto_2
    const/4 v2, 0x0

    iput-object v2, v9, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 1174
    iput-object v9, v12, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 1175
    move/from16 v0, p5

    iput v0, v12, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 1177
    .end local v9    # "list":Lcom/fmm/dm/eng/core/XDMOmList;
    :cond_4
    return-void

    .line 1131
    .end local v12    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_5
    aget-char v2, v14, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1132
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1148
    .restart local v12    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_6
    const/4 v5, 0x0

    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p3

    move-object/from16 v6, p2

    move/from16 v7, p3

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v2

    int-to-long v10, v2

    .line 1149
    .local v10, "nSize":J
    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-gtz v2, :cond_2

    .line 1151
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Size["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1

    .line 1171
    .end local v10    # "nSize":J
    .restart local v9    # "list":Lcom/fmm/dm/eng/core/XDMOmList;
    :cond_7
    const-string v2, "text/plain"

    iput-object v2, v9, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    goto :goto_2
.end method

.method public static xdmDDFTNDSAllocXMLData()Z
    .locals 2

    .prologue
    .line 1495
    const-string v0, ""

    .line 1497
    .local v0, "szXMLBuf":Ljava/lang/String;
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1499
    const-string v1, "# ERROR #  Alloc Error !!! ###"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1500
    const/4 v1, 0x0

    .line 1505
    :goto_0
    return v1

    .line 1503
    :cond_0
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1505
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xdmDDFTNDSAppendNameSpace()V
    .locals 4

    .prologue
    .line 1519
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v2

    .line 1520
    .local v2, "szXmlData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v1

    .line 1522
    .local v1, "nXmlSize":I
    const-string v3, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1523
    const-string v3, "<SyncML xmlns=\'syncml:dmddf1.2\'>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1525
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 1527
    .local v0, "nTmpLen":I
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1528
    add-int v3, v1, v0

    invoke-static {v3}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1529
    return-void
.end method

.method public static xdmDDFTNDSAppendSyncMLCloseTag()V
    .locals 4

    .prologue
    .line 2091
    const/4 v2, 0x0

    .line 2095
    .local v2, "szXmlData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v2

    .line 2096
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v1

    .line 2098
    .local v1, "nXmlSize":I
    const-string v3, "</SyncML>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2100
    const-string v3, "</SyncML>"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 2102
    .local v0, "nTmpLen":I
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2103
    add-int v3, v1, v0

    invoke-static {v3}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2104
    return-void
.end method

.method public static xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z
    .locals 1
    .param p0, "ptr"    # Ljava/lang/Object;

    .prologue
    .line 1510
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmDDFTNDSGetTagString(I)Ljava/lang/String;
    .locals 2
    .param p0, "eTokenIndex"    # I

    .prologue
    .line 1755
    const/4 v0, 0x0

    .line 1757
    .local v0, "nIndex":I
    const/16 v1, 0x7d

    if-ge p0, v1, :cond_0

    .line 1759
    add-int/lit8 v0, p0, -0x45

    .line 1760
    sget-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szTndsTokenStr:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 1769
    :goto_0
    return-object v1

    .line 1762
    :cond_0
    const/16 v1, 0x88

    if-ne p0, v1, :cond_1

    .line 1764
    const-string v1, "SyncML"

    goto :goto_0

    .line 1769
    :cond_1
    const-string v1, "NULL"

    goto :goto_0
.end method

.method public static xdmDDFTNDSGetWbxmlData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1470
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->m_szWbxmlData:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmDDFTNDSGetWbxmlDataStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1490
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->m_szWbxmlDataStart:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmDDFTNDSGetWbxmlSize()I
    .locals 1

    .prologue
    .line 1465
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->nWbxmlDataSize:I

    return v0
.end method

.method public static xdmDDFTNDSGetXMLData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1480
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->m_szXMLData:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmDDFTNDSGetXMLDataStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1485
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->m_szXMLDataStart:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmDDFTNDSGetXMLSize()I
    .locals 1

    .prologue
    .line 1475
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->nXMLDataSize:I

    return v0
.end method

.method public static xdmDDFTNDSInitParse(Ljava/lang/String;I)V
    .locals 1
    .param p0, "szInData"    # Ljava/lang/String;
    .param p1, "nInSize"    # I

    .prologue
    .line 1421
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMTndsData;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMTndsData;-><init>()V

    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    .line 1423
    invoke-static {p1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1424
    invoke-static {p0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1425
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSAllocXMLData()Z

    .line 1427
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSAppendNameSpace()V

    .line 1429
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;-><init>()V

    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    .line 1431
    return-void
.end method

.method public static xdmDDFTNDSMakeCloseTagString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2016
    const/4 v0, 0x0

    .line 2017
    .local v0, "eCloseTokenIndex":I
    const-string v1, ""

    .line 2019
    .local v1, "szCloseTagName":Ljava/lang/String;
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2021
    const-string v2, "# ERROR # Alloc Error !!! ###"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2022
    const/4 v2, 0x0

    .line 2031
    :goto_0
    return-object v2

    .line 2025
    :cond_0
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSManagePopTag()I

    move-result v0

    .line 2027
    const-string v1, "</"

    .line 2028
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetTagString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2029
    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 2031
    goto :goto_0
.end method

.method public static xdmDDFTNDSMakeOpenTagString(I)Ljava/lang/String;
    .locals 2
    .param p0, "eTokenIndex"    # I

    .prologue
    .line 1738
    const-string v0, ""

    .line 1740
    .local v0, "szOpenTagName":Ljava/lang/String;
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1742
    const-string v1, "# ERROR # Alloc Error !!! ###"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1743
    const/4 v1, 0x0

    .line 1750
    :goto_0
    return-object v1

    .line 1746
    :cond_0
    const-string v0, "<"

    .line 1747
    invoke-static {p0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetTagString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1748
    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1750
    goto :goto_0
.end method

.method public static xdmDDFTNDSManagePopTag()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2036
    const/4 v0, 0x0

    .line 2038
    .local v0, "eCloseTagID":I
    sget-object v2, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget v2, v2, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    if-nez v2, :cond_0

    .line 2040
    const-string v2, "# ERROR # TagSP EMPTY !!! ###"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v1

    .line 2048
    :goto_0
    return v0

    .line 2044
    :cond_0
    sget-object v2, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget v3, v2, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    .line 2045
    sget-object v2, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget-object v2, v2, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->eTagID:[I

    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget v3, v3, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    aget v0, v2, v3

    .line 2046
    sget-object v2, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget-object v2, v2, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->eTagID:[I

    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget v3, v3, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    aput v1, v2, v3

    goto :goto_0
.end method

.method public static xdmDDFTNDSManagePushTag(I)Z
    .locals 2
    .param p0, "eTokenIndex"    # I

    .prologue
    .line 1618
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_0

    .line 1620
    const-string v0, "# ERROR # TagSP FULL !!! ###"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1621
    const/4 v0, 0x0

    .line 1627
    :goto_0
    return v0

    .line 1624
    :cond_0
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->eTagID:[I

    sget-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    aput p0, v0, v1

    .line 1625
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    iget v1, v0, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDMTndsTagManage;->nTagSP:I

    .line 1627
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xdmDDFTNDSParsingACLTag()V
    .locals 0

    .prologue
    .line 1694
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1695
    return-void
.end method

.method public static xdmDDFTNDSParsingAccessTypeChildElement()V
    .locals 8

    .prologue
    .line 1843
    const/4 v6, 0x0

    .line 1845
    .local v6, "szWbxmlData":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1850
    .local v4, "pXmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1853
    .local v5, "szTypeBuf":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 1854
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v6

    .line 1855
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1856
    .local v0, "nTag":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v4

    .line 1857
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 1859
    .local v3, "nXmlSize":I
    add-int/lit8 v0, v0, -0x40

    .line 1861
    sparse-switch v0, :sswitch_data_0

    .line 1889
    :goto_0
    return-void

    .line 1869
    :sswitch_0
    const-string v5, "<"

    .line 1870
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetTagString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1871
    const-string v7, "/>"

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1879
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    .line 1881
    .local v1, "nTagLen":I
    add-int v7, v3, v1

    invoke-static {v7}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1882
    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1885
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1886
    add-int/lit8 v2, v2, -0x1

    .line 1887
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1888
    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    goto :goto_0

    .line 1861
    :sswitch_data_0
    .sparse-switch
        0x47 -> :sswitch_0
        0x4e -> :sswitch_0
        0x53 -> :sswitch_0
        0x5a -> :sswitch_0
        0x5d -> :sswitch_0
        0x6e -> :sswitch_0
    .end sparse-switch
.end method

.method public static xdmDDFTNDSParsingAccessTypeTag()V
    .locals 0

    .prologue
    .line 1683
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1684
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingAccessTypeChildElement()V

    .line 1685
    return-void
.end method

.method public static xdmDDFTNDSParsingCloseTag()V
    .locals 7

    .prologue
    .line 1982
    const/4 v4, 0x0

    .line 1984
    .local v4, "szWbxmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1987
    .local v5, "szXmlData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1988
    .local v0, "nTagLen":I
    const/4 v3, 0x0

    .line 1991
    .local v3, "szTag":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 1992
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v4

    .line 1993
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v5

    .line 1994
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v2

    .line 1997
    .local v2, "nXmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSMakeCloseTagString()Ljava/lang/String;

    move-result-object v3

    .line 1998
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2000
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 2002
    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2004
    :cond_0
    add-int v6, v2, v0

    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2005
    invoke-static {v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2008
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 2009
    add-int/lit8 v1, v1, -0x1

    .line 2010
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2011
    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2012
    return-void
.end method

.method public static xdmDDFTNDSParsingCodePage()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1533
    const/4 v1, 0x0

    .line 1534
    .local v1, "nWbxmlSize":I
    const/4 v0, 0x0

    .line 1535
    .local v0, "index":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 1538
    .local v2, "szWbxmlData":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1539
    add-int/lit8 v1, v1, 0x1

    .line 1541
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 1545
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eqz v4, :cond_0

    .line 1551
    const-string v3, "### ERROR ### TNDS Tag Right ###"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1552
    const/4 v3, 0x0

    .line 1560
    :goto_0
    return v3

    .line 1555
    :cond_0
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1556
    add-int/lit8 v1, v1, 0x1

    .line 1558
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1559
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    goto :goto_0
.end method

.method public static xdmDDFTNDSParsingDDFNameTag()V
    .locals 0

    .prologue
    .line 1668
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1669
    return-void
.end method

.method public static xdmDDFTNDSParsingFormatChildElement()V
    .locals 9

    .prologue
    .line 1775
    const/4 v5, 0x0

    .line 1777
    .local v5, "szWbxmlData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1782
    .local v6, "szXmlData":Ljava/lang/String;
    const-string v4, ""

    .line 1785
    .local v4, "szTypeBuf":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 1786
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v5

    .line 1787
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1788
    .local v0, "nTag":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v6

    .line 1789
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 1791
    .local v3, "nXmlSize":I
    add-int/lit8 v0, v0, 0x40

    .line 1793
    sparse-switch v0, :sswitch_data_0

    .line 1824
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "#ERROR!!!#  child tag vlaue is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  ###"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1829
    :goto_0
    :sswitch_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 1830
    .local v1, "nTagLen":I
    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1831
    add-int v7, v3, v1

    invoke-static {v7}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1832
    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1835
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1836
    add-int/lit8 v2, v2, -0x1

    .line 1837
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1838
    invoke-static {v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1839
    return-void

    .line 1813
    .end local v1    # "nTagLen":I
    :sswitch_1
    const-string v4, "<"

    .line 1814
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetTagString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1815
    const-string v7, "/>"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1816
    goto :goto_0

    .line 1793
    nop

    :sswitch_data_0
    .sparse-switch
        0x48 -> :sswitch_1
        0x49 -> :sswitch_1
        0x4a -> :sswitch_1
        0x4b -> :sswitch_1
        0x50 -> :sswitch_1
        0x5b -> :sswitch_1
        0x5e -> :sswitch_1
        0x65 -> :sswitch_1
        0x67 -> :sswitch_1
        0x69 -> :sswitch_1
        0x6a -> :sswitch_1
        0x6b -> :sswitch_0
        0x72 -> :sswitch_1
        0x79 -> :sswitch_1
        0x7a -> :sswitch_1
        0x7b -> :sswitch_0
        0x7c -> :sswitch_1
    .end sparse-switch
.end method

.method public static xdmDDFTNDSParsingFormatTag()V
    .locals 0

    .prologue
    .line 1657
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1658
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingFormatChildElement()V

    .line 1659
    return-void
.end method

.method public static xdmDDFTNDSParsingMIMETag()V
    .locals 0

    .prologue
    .line 1678
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1679
    return-void
.end method

.method public static xdmDDFTNDSParsingMgmtTreeTag()V
    .locals 0

    .prologue
    .line 1632
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1633
    return-void
.end method

.method public static xdmDDFTNDSParsingNodeNameTag()V
    .locals 0

    .prologue
    .line 1647
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1648
    return-void
.end method

.method public static xdmDDFTNDSParsingNodeTag()V
    .locals 0

    .prologue
    .line 1642
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1643
    return-void
.end method

.method public static xdmDDFTNDSParsingOpenTag()V
    .locals 8

    .prologue
    .line 1699
    const/4 v5, 0x0

    .line 1701
    .local v5, "szWbxmlData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1704
    .local v6, "szXmlData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1705
    .local v1, "nTagLen":I
    const/4 v4, 0x0

    .line 1709
    .local v4, "szTag":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 1710
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v5

    .line 1711
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1712
    .local v0, "nTag":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v6

    .line 1713
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 1716
    .local v3, "nXmlSize":I
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSManagePushTag(I)Z

    .line 1717
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSMakeOpenTagString(I)Ljava/lang/String;

    move-result-object v4

    .line 1718
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1720
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 1722
    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1724
    :cond_0
    add-int v7, v3, v1

    invoke-static {v7}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1725
    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1728
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1729
    add-int/lit8 v2, v2, -0x1

    .line 1730
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1731
    invoke-static {v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1733
    const/4 v4, 0x0

    .line 1734
    return-void
.end method

.method public static xdmDDFTNDSParsingPathTag()V
    .locals 0

    .prologue
    .line 1689
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1690
    return-void
.end method

.method public static xdmDDFTNDSParsingRTPropertiesTag()V
    .locals 0

    .prologue
    .line 1652
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1653
    return-void
.end method

.method public static xdmDDFTNDSParsingSyncMLTag()V
    .locals 4

    .prologue
    .line 1596
    const/4 v2, 0x0

    .line 1601
    .local v2, "szWbxmlData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 1602
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 1604
    const/16 v0, 0x88

    .line 1607
    .local v0, "nTag":I
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSManagePushTag(I)Z

    .line 1610
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1611
    add-int/lit8 v1, v1, -0x1

    .line 1612
    invoke-static {v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1613
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1614
    return-void
.end method

.method public static xdmDDFTNDSParsingTypeTag()V
    .locals 0

    .prologue
    .line 1663
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1664
    return-void
.end method

.method public static xdmDDFTNDSParsingValueTag()V
    .locals 0

    .prologue
    .line 1673
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1674
    return-void
.end method

.method public static xdmDDFTNDSParsingVerdtdTag()V
    .locals 0

    .prologue
    .line 1637
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1638
    return-void
.end method

.method public static xdmDDFTNDSParsingWbxmlHeader()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1565
    const/4 v1, 0x0

    .line 1566
    .local v1, "nWbxmlHeaderSize":I
    const/4 v0, 0x0

    .line 1567
    .local v0, "nStringTableSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 1570
    .local v2, "szWbxmlHeaderData":Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1571
    add-int/lit8 v1, v1, 0x1

    .line 1574
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1575
    add-int/lit8 v1, v1, 0x2

    .line 1578
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1579
    add-int/lit8 v1, v1, 0x1

    .line 1582
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1583
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1584
    add-int/lit8 v1, v1, 0x1

    .line 1587
    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1588
    add-int/lit8 v1, v0, 0x5

    .line 1590
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1591
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v3

    sub-int/2addr v3, v1

    invoke-static {v3}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1592
    return-void
.end method

.method public static xdmDDFTNDSProcessStringData()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2053
    const/4 v5, 0x0

    .line 2055
    .local v5, "szWbxmlData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2056
    .local v6, "szXmlData":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 2060
    .local v4, "pBufXmlData":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 2062
    .local v0, "nDataLen":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 2063
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v5

    .line 2064
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v6

    .line 2065
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2066
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 2068
    .local v3, "nXmlSize":I
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2069
    add-int/lit8 v0, v0, 0x1

    .line 2070
    invoke-virtual {v5, v9}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2072
    .local v1, "nTag":I
    :goto_0
    if-nez v1, :cond_0

    .line 2080
    add-int v7, v3, v0

    add-int/lit8 v7, v7, -0x1

    invoke-static {v7}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2081
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2083
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2084
    add-int/lit8 v7, v0, 0x1

    sub-int/2addr v2, v7

    .line 2085
    invoke-static {v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2086
    invoke-static {v5}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2087
    return-void

    .line 2074
    :cond_0
    int-to-char v7, v1

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2075
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2076
    add-int/lit8 v0, v0, 0x1

    .line 2077
    invoke-virtual {v5, v9}, Ljava/lang/String;->charAt(I)C

    move-result v1

    goto :goto_0
.end method

.method public static xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 1440
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iput-object p0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->m_szWbxmlData:Ljava/lang/String;

    .line 1441
    return-void
.end method

.method public static xdmDDFTNDSSetWbxmlDataStart(Ljava/lang/String;)V
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 1460
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iput-object p0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->m_szWbxmlDataStart:Ljava/lang/String;

    .line 1461
    return-void
.end method

.method public static xdmDDFTNDSSetWbxmlSize(I)V
    .locals 1
    .param p0, "nSize"    # I

    .prologue
    .line 1435
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iput p0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->nWbxmlDataSize:I

    .line 1436
    return-void
.end method

.method public static xdmDDFTNDSSetXMLData(Ljava/lang/String;)V
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 1450
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iput-object p0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->m_szXMLData:Ljava/lang/String;

    .line 1451
    return-void
.end method

.method public static xdmDDFTNDSSetXMLDataStart(Ljava/lang/String;)V
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 1455
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iput-object p0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->m_szXMLDataStart:Ljava/lang/String;

    .line 1456
    return-void
.end method

.method public static xdmDDFTNDSSetXMLSize(I)V
    .locals 1
    .param p0, "nSize"    # I

    .prologue
    .line 1445
    sget-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    iput p0, v0, Lcom/fmm/dm/eng/parser/XDMTndsData;->nXMLDataSize:I

    .line 1446
    return-void
.end method

.method public static xdmDDFTNDSUderMgmtTreeTagParse()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1894
    const/4 v1, 0x0

    .line 1897
    .local v1, "szData":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v0

    .line 1898
    .local v0, "nSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v1

    .line 1901
    :goto_0
    if-eqz v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1977
    :cond_0
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 1903
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 1905
    .local v2, "wbxmlData":[B
    aget-byte v4, v2, v3

    sparse-switch v4, :sswitch_data_0

    .line 1968
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1969
    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 1908
    :sswitch_0
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingCloseTag()V

    .line 1973
    :goto_2
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v0

    .line 1974
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v1

    :goto_3
    goto :goto_0

    .line 1912
    :sswitch_1
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSProcessStringData()V

    goto :goto_2

    .line 1916
    :sswitch_2
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingNodeTag()V

    goto :goto_2

    .line 1920
    :sswitch_3
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingNodeNameTag()V

    goto :goto_2

    .line 1924
    :sswitch_4
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingRTPropertiesTag()V

    goto :goto_2

    .line 1928
    :sswitch_5
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingFormatTag()V

    goto :goto_2

    .line 1932
    :sswitch_6
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingTypeTag()V

    goto :goto_2

    .line 1936
    :sswitch_7
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingDDFNameTag()V

    goto :goto_2

    .line 1940
    :sswitch_8
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingValueTag()V

    goto :goto_2

    .line 1944
    :sswitch_9
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingMIMETag()V

    goto :goto_2

    .line 1948
    :sswitch_a
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingAccessTypeTag()V

    goto :goto_2

    .line 1952
    :sswitch_b
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingPathTag()V

    goto :goto_2

    .line 1956
    :sswitch_c
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingACLTag()V

    goto :goto_2

    .line 1960
    :sswitch_d
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingVerdtdTag()V

    goto :goto_2

    .line 1964
    :sswitch_e
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingCodePage()Z

    goto :goto_2

    .line 1974
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    .line 1905
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_e
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x45 -> :sswitch_a
        0x46 -> :sswitch_c
        0x51 -> :sswitch_7
        0x5c -> :sswitch_5
        0x61 -> :sswitch_9
        0x64 -> :sswitch_2
        0x66 -> :sswitch_3
        0x6c -> :sswitch_b
        0x6f -> :sswitch_4
        0x75 -> :sswitch_6
        0x76 -> :sswitch_8
        0x77 -> :sswitch_d
    .end sparse-switch
.end method

.method public static xdmDDFXmlTagCode(Ljava/lang/String;)I
    .locals 2
    .param p0, "szName"    # Ljava/lang/String;

    .prologue
    .line 689
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 697
    const/4 v0, 0x0

    .end local v0    # "i":I
    :cond_0
    return v0

    .line 691
    .restart local v0    # "i":I
    :cond_1
    sget-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 689
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static xdmDDFXmlTagParsing(Lcom/fmm/dm/eng/parser/XDM_DM_Tree;)Z
    .locals 9
    .param p0, "pTree"    # Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .prologue
    .line 728
    if-nez p0, :cond_1

    .line 730
    const/4 v3, 0x0

    .line 846
    :cond_0
    :goto_0
    return v3

    .line 732
    :cond_1
    const/4 v0, 0x0

    .line 733
    .local v0, "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->childlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 735
    .local v1, "childlist":Lcom/fmm/dm/eng/core/XDMLinkedList;
    const/4 v3, 0x1

    .line 736
    .local v3, "ret":Z
    const-string v4, ""

    .line 737
    .local v4, "szPath":Ljava/lang/String;
    const-string v5, ""

    .line 739
    .local v5, "szPathTemp":Ljava/lang/String;
    :goto_1
    if-eqz p0, :cond_0

    .line 741
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .end local v0    # "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    check-cast v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    .line 743
    .restart local v0    # "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 745
    const-string v6, "Tag:"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 746
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 749
    :cond_2
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 751
    const-string v6, "Name:"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 752
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 755
    :cond_3
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 757
    const-string v6, "Path:"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 758
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 761
    :cond_4
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 763
    const-string v6, "Data:"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 764
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 767
    :cond_5
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFXmlTagCode(Ljava/lang/String;)I

    move-result v2

    .line 769
    .local v2, "nTagCode":I
    iget-object v6, p0, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->parent:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    if-nez v6, :cond_6

    .line 771
    const/4 v4, 0x0

    .line 773
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 775
    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 782
    :goto_2
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 784
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCheckInbox(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 785
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 787
    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 794
    :cond_6
    sget-object v6, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_om:Lcom/fmm/dm/eng/core/XDMOmTree;

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    iget-object v8, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmAgentVerifyNewAccount(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;)Z

    .line 796
    sparse-switch v2, :sswitch_data_0

    .line 838
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 839
    sget-object v6, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 842
    :cond_7
    :goto_3
    :sswitch_0
    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "pTree":Lcom/fmm/dm/eng/parser/XDM_DM_Tree;
    check-cast p0, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .restart local p0    # "pTree":Lcom/fmm/dm/eng/parser/XDM_DM_Tree;
    goto/16 :goto_1

    .line 779
    :cond_8
    const-string v6, "Path is NULL."

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_2

    .line 799
    :sswitch_1
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFPrintNodePropert(Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;)V

    .line 800
    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateNodeToOM(Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;)Z

    move-result v3

    .line 802
    if-nez v3, :cond_9

    .line 804
    const/4 v4, 0x0

    .line 805
    goto/16 :goto_0

    .line 809
    :cond_9
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    const-string v7, "./"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 811
    const-string v4, "/"

    .line 814
    :cond_a
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 816
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 818
    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 820
    :cond_b
    invoke-static {p0, v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateNode(Lcom/fmm/dm/eng/parser/XDM_DM_Tree;Ljava/lang/String;)Z

    .line 822
    const/16 v6, 0x2f

    invoke-static {v4, v6}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrrchr(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v5

    .line 824
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 826
    move-object v4, v5

    .line 829
    goto :goto_3

    .line 796
    :sswitch_data_0
    .sparse-switch
        0x1f -> :sswitch_0
        0x20 -> :sswitch_0
        0x22 -> :sswitch_0
        0x24 -> :sswitch_1
        0x37 -> :sswitch_0
    .end sparse-switch
.end method

.method public static xdmTndsParseFinish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2653
    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTndsData:Lcom/fmm/dm/eng/parser/XDMTndsData;

    .line 2654
    sput-object v0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/fmm/dm/eng/parser/XDMTndsTagManage;

    .line 2655
    return-void
.end method

.method public static xdmTndsWbxmlParse(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p0, "szInData"    # Ljava/lang/String;
    .param p1, "nInSize"    # I

    .prologue
    const/4 v5, 0x0

    .line 1364
    const-string v2, ""

    .line 1366
    .local v2, "szData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1367
    .local v0, "bSyncMLTag":Z
    const-string v3, ""

    .line 1369
    .local v3, "szOutData":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSInitParse(Ljava/lang/String;I)V

    .line 1371
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 1372
    .local v1, "nSize":I
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 1375
    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1409
    :cond_0
    if-nez v0, :cond_1

    .line 1411
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSAppendSyncMLCloseTag()V

    .line 1414
    :cond_1
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    .line 1416
    :goto_1
    return-object v5

    .line 1377
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 1379
    .local v4, "wbxmlData":[B
    const/4 v6, 0x0

    aget-byte v6, v4, v6

    sparse-switch v6, :sswitch_data_0

    .line 1400
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1401
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 1382
    :sswitch_0
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingCodePage()Z

    .line 1405
    :goto_2
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 1406
    if-eqz v1, :cond_3

    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    :goto_3
    goto :goto_0

    .line 1386
    :sswitch_1
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingWbxmlHeader()V

    goto :goto_2

    .line 1390
    :sswitch_2
    const/4 v0, 0x1

    .line 1391
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingSyncMLTag()V

    goto :goto_2

    .line 1395
    :sswitch_3
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingMgmtTreeTag()V

    .line 1396
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSUderMgmtTreeTagParse()Z

    goto :goto_2

    :cond_3
    move-object v2, v5

    .line 1406
    goto :goto_3

    .line 1379
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x60 -> :sswitch_3
        0x6d -> :sswitch_2
    .end sparse-switch
.end method

.method public static xdmTndsXml2WbxmlConvert(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p0, "szInData"    # Ljava/lang/String;
    .param p1, "nInSize"    # I

    .prologue
    const/4 v5, 0x0

    .line 2109
    const/4 v1, 0x0

    .line 2110
    .local v1, "nXmlSize":I
    const-string v3, ""

    .line 2111
    .local v3, "szXmlData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2112
    .local v0, "nChar":C
    const-string v2, ""

    .line 2114
    .local v2, "szOutData":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFInitConvert(Ljava/lang/String;I)V

    .line 2116
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v1

    .line 2117
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v3

    .line 2119
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    aget-char v0, v4, v5

    .line 2122
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 2151
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 2152
    :goto_1
    return-object v4

    .line 2124
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 2139
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2140
    const/4 v4, 0x0

    goto :goto_1

    .line 2127
    :sswitch_0
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertCheckTag()V

    .line 2143
    :goto_2
    :sswitch_1
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v1

    .line 2144
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v3

    .line 2145
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 2147
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    aget-char v0, v4, v5

    goto :goto_0

    .line 2133
    :sswitch_2
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFConvertSkip1Byte()V

    goto :goto_2

    .line 2124
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0xa -> :sswitch_2
        0x3c -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public characters([CII)V
    .locals 7
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    const/4 v6, 0x0

    .line 529
    new-instance v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;-><init>()V

    .line 531
    .local v0, "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "characters =     "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v5, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 533
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    if-nez v4, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    iget-object v0, v4, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .end local v0    # "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    check-cast v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    .line 540
    .restart local v0    # "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    if-eqz v0, :cond_0

    .line 545
    iget v4, p0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTagCode:I

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 592
    :sswitch_0
    iget-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szDDFName:Ljava/lang/String;

    .line 594
    .local v2, "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 596
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 597
    .local v1, "old_len":I
    const-string v3, ""

    .line 599
    .local v3, "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 600
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 602
    move-object v2, v3

    .line 610
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_1
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szDDFName:Ljava/lang/String;

    goto :goto_0

    .line 548
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_1
    iget-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    .line 550
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 552
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 553
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 555
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 556
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 558
    move-object v2, v3

    .line 566
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_2
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    goto :goto_0

    .line 562
    :cond_2
    const-string v2, ""

    .line 563
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 570
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_2
    iget-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 572
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 574
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 575
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 577
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 578
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 580
    move-object v2, v3

    .line 588
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_3
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    goto/16 :goto_0

    .line 584
    :cond_3
    const-string v2, ""

    .line 585
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 606
    :cond_4
    const-string v2, ""

    .line 607
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 614
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_3
    iget-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szMIME:Ljava/lang/String;

    .line 616
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 618
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 619
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 621
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 622
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 624
    move-object v2, v3

    .line 632
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_4
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szMIME:Ljava/lang/String;

    goto/16 :goto_0

    .line 628
    :cond_5
    const-string v2, ""

    .line 629
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 636
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_4
    iget-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    .line 638
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 640
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 641
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 643
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 644
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 646
    move-object v2, v3

    .line 654
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_5
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    goto/16 :goto_0

    .line 650
    :cond_6
    const-string v2, ""

    .line 651
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 659
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_5
    iget-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    .line 661
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 663
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 664
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 666
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 667
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 669
    move-object v2, v3

    .line 677
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_6
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 673
    :cond_7
    const-string v2, ""

    .line 674
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 545
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_5
        0x21 -> :sswitch_3
        0x26 -> :sswitch_1
        0x2c -> :sswitch_2
        0x35 -> :sswitch_4
        0x36 -> :sswitch_5
    .end sparse-switch
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 341
    invoke-super {p0}, Lorg/xml/sax/helpers/DefaultHandler;->endDocument()V

    .line 342
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "szNameSpaceURI"    # Ljava/lang/String;
    .param p2, "szLocalName"    # Ljava/lang/String;
    .param p3, "szQualifiedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 402
    new-instance v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;-><init>()V

    .line 403
    .local v0, "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "end =            "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 405
    sget-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    if-nez v1, :cond_1

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    sget-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    iget-object v0, v1, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .end local v0    # "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    check-cast v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    .line 412
    .restart local v0    # "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    invoke-static {p2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFXmlTagCode(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTagCode:I

    .line 414
    if-eqz v0, :cond_0

    .line 419
    iget v1, p0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTagCode:I

    packed-switch v1, :pswitch_data_0

    .line 522
    :cond_2
    :goto_1
    :pswitch_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTagCode:I

    goto :goto_0

    .line 422
    :pswitch_1
    sget-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->parent:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    sput-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    goto :goto_1

    .line 430
    :pswitch_2
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 432
    const-string v1, "Node"

    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    goto :goto_1

    .line 441
    :pswitch_3
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 443
    const-string v1, "No Data"

    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    goto :goto_1

    .line 448
    :pswitch_4
    iget v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 452
    :pswitch_5
    iget v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 456
    :pswitch_6
    iget v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 460
    :pswitch_7
    iget v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 464
    :pswitch_8
    iget v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 468
    :pswitch_9
    iput v4, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->scope:I

    goto :goto_1

    .line 472
    :pswitch_a
    iput v3, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->scope:I

    goto :goto_1

    .line 476
    :pswitch_b
    const/4 v1, 0x6

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 480
    :pswitch_c
    const/4 v1, 0x4

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 484
    :pswitch_d
    const/4 v1, 0x5

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 488
    :pswitch_e
    const/16 v1, 0x8

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 492
    :pswitch_f
    const/4 v1, 0x7

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 496
    :pswitch_10
    const/4 v1, 0x3

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 500
    :pswitch_11
    iput v4, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 504
    :pswitch_12
    iput v3, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 508
    :pswitch_13
    const/16 v1, 0x9

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 512
    :pswitch_14
    const/16 v1, 0xa

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 516
    :pswitch_15
    const/16 v1, 0xb

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 419
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_4
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_13
        :pswitch_0
        :pswitch_5
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_b
        :pswitch_2
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public startDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 348
    invoke-super {p0}, Lorg/xml/sax/helpers/DefaultHandler;->startDocument()V

    .line 349
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "szNameSpaceURI"    # Ljava/lang/String;
    .param p2, "szLocalName"    # Ljava/lang/String;
    .param p3, "szQualifiedName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 358
    invoke-static {p2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFXmlTagCode(Ljava/lang/String;)I

    move-result v2

    .line 359
    .local v2, "nTagCode":I
    iput v2, p0, Lcom/fmm/dm/eng/parser/XDMDDFParser;->gTagCode:I

    .line 360
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "start =          "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 362
    sparse-switch v2, :sswitch_data_0

    .line 397
    :goto_0
    :sswitch_0
    return-void

    .line 365
    :sswitch_1
    new-instance v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;-><init>()V

    .line 367
    .local v0, "XmlElement":Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;
    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    if-nez v3, :cond_0

    .line 369
    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    iput-object v0, v3, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .line 371
    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    sput-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->XmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .line 388
    :goto_1
    iput-object p2, v0, Lcom/fmm/dm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    goto :goto_0

    .line 375
    :cond_0
    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->childlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-nez v3, :cond_1

    .line 377
    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->childlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 380
    :cond_1
    new-instance v1, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;-><init>()V

    .line 381
    .local v1, "childtree":Lcom/fmm/dm/eng/parser/XDM_DM_Tree;
    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    iput-object v3, v1, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->parent:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    .line 382
    iput-object v0, v1, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .line 384
    sget-object v3, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDM_DM_Tree;->childlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v3, v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 386
    sput-object v1, Lcom/fmm/dm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/fmm/dm/eng/parser/XDM_DM_Tree;

    goto :goto_1

    .line 362
    nop

    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_0
        0x24 -> :sswitch_1
    .end sparse-switch
.end method
