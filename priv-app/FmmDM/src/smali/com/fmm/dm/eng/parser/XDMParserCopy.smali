.class public Lcom/fmm/dm/eng/parser/XDMParserCopy;
.super Lcom/fmm/dm/agent/XDMHandleCmd;
.source "XDMParserCopy.java"

# interfaces
.implements Lcom/fmm/dm/eng/core/XDMWbxml;


# instance fields
.field public cmdid:I

.field public cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

.field public is_noresp:I

.field public itemlist:Lcom/fmm/dm/eng/core/XDMList;

.field public meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMHandleCmd;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 15
    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 16
    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 10
    return-void
.end method


# virtual methods
.method public xdmParParseCopy(Lcom/fmm/dm/eng/parser/XDMParser;)I
    .locals 5
    .param p1, "p"    # Lcom/fmm/dm/eng/parser/XDMParser;

    .prologue
    const/4 v3, 0x0

    .line 25
    const/4 v1, -0x1

    .line 26
    .local v1, "id":I
    const/4 v2, 0x0

    .line 28
    .local v2, "res":I
    const/16 v4, 0xd

    invoke-virtual {p1, v4}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 29
    if-eqz v2, :cond_1

    move v3, v2

    .line 107
    :cond_0
    :goto_0
    return v3

    .line 34
    :cond_1
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 35
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 39
    if-eqz v2, :cond_2

    .line 41
    const-string v3, "not WBXML_ERR_OK"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 42
    goto :goto_0

    .line 49
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 56
    :goto_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_3

    .line 58
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    .line 106
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v4, p0}, Lcom/fmm/dm/eng/parser/XDMParserCopy;->xdmAgentHdlCmdCopy(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserCopy;)V

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 62
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v1, :sswitch_data_0

    .line 97
    const/4 v2, 0x2

    .line 100
    :goto_2
    if-eqz v2, :cond_2

    move v3, v2

    .line 102
    goto :goto_0

    .line 65
    :sswitch_0
    invoke-virtual {p1, v1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 66
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    goto :goto_2

    .line 70
    :sswitch_1
    invoke-virtual {p1, v1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v4

    iput v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->is_noresp:I

    goto :goto_2

    .line 74
    :sswitch_2
    new-instance v4, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v4}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 75
    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-virtual {v4, p1}, Lcom/fmm/dm/eng/parser/XDMParserCred;->xdmParParseCred(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v2

    .line 76
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->Cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    goto :goto_2

    .line 80
    :sswitch_3
    new-instance v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v4}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 81
    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-virtual {v4, p1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/fmm/dm/eng/parser/XDMParser;)I

    move-result v2

    .line 82
    iget-object v4, p1, Lcom/fmm/dm/eng/parser/XDMParser;->Meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 86
    :sswitch_4
    iget-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-virtual {p1, v4}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_2

    .line 90
    :sswitch_5
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 91
    invoke-virtual {p1}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 93
    iput v1, p1, Lcom/fmm/dm/eng/parser/XDMParser;->codePage:I

    goto :goto_2

    .line 62
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0xb -> :sswitch_0
        0xe -> :sswitch_2
        0x14 -> :sswitch_4
        0x1a -> :sswitch_3
        0x1d -> :sswitch_1
    .end sparse-switch
.end method
