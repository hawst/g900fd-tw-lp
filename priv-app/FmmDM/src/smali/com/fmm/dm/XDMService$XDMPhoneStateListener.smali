.class public Lcom/fmm/dm/XDMService$XDMPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "XDMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/XDMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XDMPhoneStateListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 823
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyListeners()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 894
    sget-object v1, Lcom/fmm/dm/XDMService;->telephonyManager:Landroid/telephony/TelephonyManager;

    sget-object v2, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    invoke-static {v1, v2}, Lcom/fmm/dm/adapter/XDMTelephonyData;->xdmGetInstance(Landroid/telephony/TelephonyManager;Landroid/telephony/ServiceState;)Lcom/fmm/dm/adapter/XDMTelephonyData;

    move-result-object v0

    .line 896
    .local v0, "telephony":Lcom/fmm/dm/adapter/XDMTelephonyData;
    if-eqz v0, :cond_1

    .line 898
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetServiceType(I)I

    .line 899
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetSimState(I)V

    .line 901
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-eqz v1, :cond_0

    .line 903
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    if-nez v1, :cond_0

    .line 905
    sput-boolean v4, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    .line 906
    invoke-static {v5, v3, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 911
    :cond_0
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-eqz v1, :cond_3

    .line 913
    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v4}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 914
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "going to Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 923
    :cond_1
    :goto_0
    sget-object v1, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    if-nez v1, :cond_2

    .line 925
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsPhoneBookInitialized:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_2

    .line 927
    invoke-static {v5, v3, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 930
    :cond_2
    return-void

    .line 916
    :cond_3
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-nez v1, :cond_1

    .line 918
    const/4 v1, 0x0

    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v1}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 919
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exit from Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "szIncomingNumber"    # Ljava/lang/String;

    .prologue
    .line 828
    const-string v0, ">>>>>>>>>>> onCallStateChanged"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 829
    sget-object v0, Lcom/fmm/dm/XDMService;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_1

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 832
    :cond_1
    if-nez p1, :cond_3

    .line 834
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_2

    .line 836
    const-string v0, "Run Resume Operation, Resume call CallStateChanged"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 837
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    .line 839
    :cond_2
    const-string v0, ">>>>>>>>>>> CALL_STATE_IDLE"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 841
    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 843
    const-string v0, ">>>>>>>>>>> CALL_STATE_RINGING"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 845
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 847
    const-string v0, ">>>>>>>>>>> CALL_STATE_OFFHOOK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDataActivity(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 890
    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "networkType"    # I

    .prologue
    .line 870
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataState()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 872
    invoke-static {p1}, Lcom/fmm/dm/XDMService;->xdmSetDataState(I)V

    .line 875
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 877
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener;->notifyListeners()V

    .line 879
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_1

    .line 881
    const-string v0, "Run Resume Operation, Resume call DataConnectionState"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 882
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    .line 885
    :cond_1
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 1
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 860
    sget-object v0, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    if-eq v0, p1, :cond_0

    .line 862
    sput-object p1, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    .line 863
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener;->notifyListeners()V

    .line 865
    :cond_0
    return-void
.end method

.method public onSignalStrengthChanged(I)V
    .locals 0
    .param p1, "asu"    # I

    .prologue
    .line 855
    return-void
.end method
