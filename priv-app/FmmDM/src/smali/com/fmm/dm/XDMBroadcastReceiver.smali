.class public Lcom/fmm/dm/XDMBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "XDMBroadcastReceiver.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XCommonInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Lcom/fmm/dm/interfaces/XNOTIInterface;


# static fields
.field public static final XCOMMON_INTENT_DATA_ROAMING:Ljava/lang/String; = "android.intent.action.DATA_ROAMING"

.field public static final XCOMMON_INTENT_FMMDM_ADMIN_SETTING:Ljava/lang/String; = "android.intent.action.FMMDM_ADMIN_SETTING"

.field public static final XCOMMON_INTENT_GET_IP_PUSH_NOTI_RECEIVED:Ljava/lang/String; = "android.intent.action.IP_PUSH_DM_NOTI_RECEIVED"

.field public static final XCOMMON_INTENT_NOTI_CANCELED:Ljava/lang/String; = "android.intent.action.NOTI_CANCELED"

.field public static final XCOMMON_INTENT_SIM_STATE_CHANGE:Ljava/lang/String; = "android.intent.action.SIM_STATE_CHANGED"

.field public static final XCOMMON_INTENT_UNLOCK_ALERT:Ljava/lang/String; = "android.intent.action.UNLOCK_FFM_ALERT"

.field public static final XCOMMON_INTENT_UPDATE_DEVICEID:Ljava/lang/String; = "android.intent.action.pcw.UPDATE_DEVICEID"

.field private static m_InitResumeIntent:Landroid/content/Intent;

.field private static m_InitResumeResult:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static xdmBroadCastNotInitHandle(I)V
    .locals 1
    .param p0, "nResumeIntent"    # I

    .prologue
    .line 355
    const-string v0, "DM Not Init"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 356
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentSetNotiResumeCase(I)V

    .line 357
    return-void
.end method

.method public static xdmBroadcastGetInitIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 340
    sget-object v0, Lcom/fmm/dm/XDMBroadcastReceiver;->m_InitResumeIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public static xdmBroadcastGetInitResumeResult()I
    .locals 1

    .prologue
    .line 325
    sget v0, Lcom/fmm/dm/XDMBroadcastReceiver;->m_InitResumeResult:I

    return v0
.end method

.method private xdmBroadcastIPPushReceived(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 283
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NotiResumeCase : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentGetNotiResumeCase()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 285
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TaskInit : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IsDMInitialized : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 289
    invoke-static {}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentGetNotiResumeCase()I

    move-result v1

    if-nez v1, :cond_0

    .line 291
    const-string v1, "pdus"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 293
    .local v0, "inbox":[B
    if-nez v0, :cond_1

    .line 295
    const-string v1, "PushData is null."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 321
    .end local v0    # "inbox":[B
    :cond_0
    :goto_0
    return-void

    .line 299
    .restart local v0    # "inbox":[B
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 300
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v0

    .line 301
    if-eqz v0, :cond_0

    .line 303
    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiAddPushDataQueue(I[B)V

    .line 305
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_2

    .line 307
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    goto :goto_0

    .line 311
    :cond_2
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiGetNotiProcessing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 313
    const-string v1, "Noti Processing..."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :cond_3
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto :goto_0
.end method

.method public static xdmBroadcastSetInitIntent(Landroid/content/Intent;)V
    .locals 0
    .param p0, "mIntent"    # Landroid/content/Intent;

    .prologue
    .line 335
    sput-object p0, Lcom/fmm/dm/XDMBroadcastReceiver;->m_InitResumeIntent:Landroid/content/Intent;

    .line 336
    return-void
.end method

.method public static xdmBroadcastSetInitResumeResult(I)V
    .locals 0
    .param p0, "nResumeResult"    # I

    .prologue
    .line 330
    sput p0, Lcom/fmm/dm/XDMBroadcastReceiver;->m_InitResumeResult:I

    .line 331
    return-void
.end method

.method public static xdmBroadcastSetUpdateURL(Landroid/content/Intent;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 345
    if-nez p0, :cond_0

    .line 347
    sget-object p0, Lcom/fmm/dm/XDMBroadcastReceiver;->m_InitResumeIntent:Landroid/content/Intent;

    .line 349
    :cond_0
    const-string v1, "DMServer"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 350
    .local v0, "szUrl":Ljava/lang/String;
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbChangeLawmoServerInfo(Ljava/lang/String;)V

    .line 351
    return-void
.end method

.method private xdmBroadcastSimChangeAlertCheck()V
    .locals 3

    .prologue
    .line 270
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v0

    .line 271
    .local v0, "nAgentType":I
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    .line 272
    .local v1, "nNetworkState":I
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 274
    :cond_0
    const-string v2, "cannot process during"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 279
    :goto_0
    return-void

    .line 278
    :cond_1
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiSimChangeAlertReport()Z

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 51
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.pcw.UPDATE_DEVICEID"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.IP_PUSH_DM_NOTI_RECEIVED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 53
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "XDMApplicationStartFlag when receive Intent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 54
    sget-boolean v4, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    if-nez v4, :cond_1

    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 56
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BroadCast Action Name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 57
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "XDMApplicationStartFlag : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 58
    sput-boolean v8, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    .line 59
    const-string v4, "FMMDM SERVICE START Set!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 60
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/fmm/dm/XDMService;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 65
    .end local v0    # "cn":Landroid/content/ComponentName;
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.IP_PUSH_DM_NOTI_RECEIVED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 67
    const-string v4, "android.intent.action.IP_PUSH_DM_NOTI_RECEIVED"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 68
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "XDMApplicationStartFlag : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 69
    invoke-direct {p0, p2}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastIPPushReceived(Landroid/content/Intent;)V

    .line 266
    :cond_2
    :goto_0
    return-void

    .line 71
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.android.internal.policy.impl.Keyguard.PCW_UNLOCKED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 73
    const-string v4, "com.android.internal.policy.impl.Keyguard.PCW_UNLOCKED"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 74
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_4

    .line 76
    invoke-static {v9}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    goto :goto_0

    .line 79
    :cond_4
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 80
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteOperation()V

    goto :goto_0

    .line 82
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.dsm.DM_WIPE_RESULT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 84
    const-string v4, "android.intent.action.dsm.DM_WIPE_RESULT"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 87
    const-string v4, "result"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 88
    .local v2, "nResultReport":I
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_6

    .line 90
    const/4 v4, 0x5

    invoke-static {v4}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    .line 91
    invoke-static {v2}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSetInitResumeResult(I)V

    goto :goto_0

    .line 94
    :cond_6
    invoke-static {v2}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentWipeResult(I)V

    goto :goto_0

    .line 98
    .end local v2    # "nResultReport":I
    :cond_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.dsm.DM_RL_LOCK_RESULT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 100
    const-string v4, "android.intent.action.dsm.DM_RL_LOCK_RESULT"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 103
    sget-object v4, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mXLAWMOWaitTimer:Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;

    if-eqz v4, :cond_8

    .line 105
    sget-object v4, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mXLAWMOWaitTimer:Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->endWaitTimer(I)V

    .line 107
    :cond_8
    const-string v4, "result"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 108
    .restart local v2    # "nResultReport":I
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_9

    .line 110
    const/16 v4, 0xc

    invoke-static {v4}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    .line 111
    invoke-static {v2}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSetInitResumeResult(I)V

    goto :goto_0

    .line 114
    :cond_9
    invoke-static {v2}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentReactivationResult(I)V

    goto :goto_0

    .line 118
    .end local v2    # "nResultReport":I
    :cond_a
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.dsm.DM_FORWARDING_RESULT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 120
    const-string v4, "android.intent.action.dsm.DM_FORWARDING_RESULT"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 123
    const-string v4, "result"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 124
    .restart local v2    # "nResultReport":I
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_b

    .line 126
    const/4 v4, 0x6

    invoke-static {v4}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    .line 127
    invoke-static {v2}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSetInitResumeResult(I)V

    goto/16 :goto_0

    .line 130
    :cond_b
    invoke-static {v2}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentForwardingResult(I)V

    goto/16 :goto_0

    .line 134
    .end local v2    # "nResultReport":I
    :cond_c
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 136
    const-string v4, "android.intent.action.SIM_STATE_CHANGED"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 138
    const-string v4, "ss"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 139
    .local v3, "szSimState":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SIMState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 141
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_d

    .line 143
    const-string v4, "DM Not Init"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 146
    :cond_d
    const-string v4, "LOADED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetPinLockState()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 148
    invoke-direct {p0}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSimChangeAlertCheck()V

    .line 149
    invoke-static {v7}, Lcom/fmm/dm/XDMService;->xdmSetPinLockState(Z)V

    goto/16 :goto_0

    .line 153
    .end local v3    # "szSimState":Ljava/lang/String;
    :cond_e
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.pcw.UPDATE_DEVICEID"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 155
    const-string v4, "android.intent.action.pcw.UPDATE_DEVICEID"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 158
    :cond_f
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.FMMDM_ADMIN_SETTING"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 160
    const-string v4, "android.intent.action.FMMDM_ADMIN_SETTING"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 161
    new-instance v1, Landroid/content/Intent;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-class v6, Lcom/fmm/dm/ui/XUIMainActivity;

    invoke-direct {v1, v4, v5, p1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    .local v1, "i":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 163
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 165
    .end local v1    # "i":Landroid/content/Intent;
    :cond_10
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.UNLOCK_FFM_ALERT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 167
    const-string v4, "android.intent.action.UNLOCK_FFM_ALERT"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 168
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_11

    .line 170
    const-string v4, "!XDMTask.g_IsDMInitialized"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 171
    const/16 v4, 0x8

    invoke-static {v4}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    goto/16 :goto_0

    .line 174
    :cond_11
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiUnlockAlertReport()V

    goto/16 :goto_0

    .line 176
    :cond_12
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.settings.reactivationlock_on"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.settings.reactivationlock_off"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 178
    :cond_13
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 179
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWSupportReactivationLock()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 183
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_14

    .line 185
    const-string v4, "!XDMTask.g_IsDMInitialized"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 186
    const/16 v4, 0xd

    invoke-static {v4}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    goto/16 :goto_0

    .line 189
    :cond_14
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiReactivationLockAlertReport()V

    goto/16 :goto_0

    .line 191
    :cond_15
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.EMERGENCY_APPLICATION_MODIFIED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 193
    const-string v4, "android.intent.action.EMERGENCY_APPLICATION_MODIFIED"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 196
    const-string v4, "date"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSetEmergencyModeDate(Ljava/lang/String;)V

    .line 197
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_16

    .line 199
    const-string v4, "!XDMTask.g_IsDMInitialized"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 200
    const/16 v4, 0xa

    invoke-static {v4}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    goto/16 :goto_0

    .line 203
    :cond_16
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiEmergencyModeChangeAlertReport()V

    goto/16 :goto_0

    .line 206
    :cond_17
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 208
    const-string v4, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 211
    const-string v4, "reason"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 212
    .restart local v2    # "nResultReport":I
    const/4 v4, 0x4

    if-eq v2, v4, :cond_18

    if-ne v2, v9, :cond_19

    .line 214
    :cond_18
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Skp emergency state, nResultReport : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 218
    :cond_19
    const-string v4, "date"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSetEmergencyModeDate(Ljava/lang/String;)V

    .line 219
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_1a

    .line 221
    const-string v4, "!XDMTask.g_IsDMInitialized"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 222
    const/16 v4, 0x9

    invoke-static {v4}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    .line 223
    invoke-static {v2}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSetInitResumeResult(I)V

    goto/16 :goto_0

    .line 226
    :cond_1a
    invoke-static {v2}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentEmergencyModeResult(I)V

    goto/16 :goto_0

    .line 230
    .end local v2    # "nResultReport":I
    :cond_1b
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.dsm.DM_LOCATION_RESULT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 232
    const-string v4, "android.intent.action.dsm.DM_LOCATION_RESULT"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 233
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetLocationProcess()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 235
    const-string v4, "GetLocation is Processing"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 238
    :cond_1c
    invoke-static {v8}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetLocationProcess(Z)V

    .line 239
    sget-object v4, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mXAMTWaitTimer:Lcom/fmm/dm/agent/amt/XAMTWaitTimer;

    if-eqz v4, :cond_1d

    .line 241
    sget-object v4, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mXAMTWaitTimer:Lcom/fmm/dm/agent/amt/XAMTWaitTimer;

    invoke-virtual {v4}, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->endWaitTimer()V

    .line 243
    :cond_1d
    sget-boolean v4, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v4, :cond_1e

    .line 245
    const/16 v4, 0xb

    invoke-static {v4}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    .line 246
    invoke-static {p2}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSetInitIntent(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 249
    :cond_1e
    invoke-static {p2}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentGetLocationResult(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 252
    :cond_1f
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 254
    const-string v4, "ACTION_LOCALE_CHANGED"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 255
    invoke-static {}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiCallBackupIndicator()V

    goto/16 :goto_0

    .line 257
    :cond_20
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.NOTI_CANCELED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 259
    const-string v4, "XCOMMON_INTENT_NOTI_CANCELED"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 260
    invoke-static {}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiInitBackupIndicator()V

    goto/16 :goto_0

    .line 264
    :cond_21
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
