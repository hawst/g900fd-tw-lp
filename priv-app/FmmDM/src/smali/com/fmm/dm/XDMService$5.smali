.class Lcom/fmm/dm/XDMService$5;
.super Landroid/content/BroadcastReceiver;
.source "XDMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fmm/dm/XDMService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/XDMService;


# direct methods
.method constructor <init>(Lcom/fmm/dm/XDMService;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, Lcom/fmm/dm/XDMService$5;->this$0:Lcom/fmm/dm/XDMService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 562
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmProtoIsWIFIConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    sget-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    if-nez v0, :cond_1

    .line 566
    const/4 v0, 0x1

    sput-boolean v0, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    .line 567
    const-string v0, "----------- XEVENT_DM_INIT WIFI ok"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 568
    const/16 v0, 0xc

    invoke-static {v0, v1, v1}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 576
    :cond_0
    :goto_0
    return-void

    .line 570
    :cond_1
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_0

    .line 572
    const-string v0, "Run Resume Operation, Resume call WIFIConnect"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 573
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    goto :goto_0
.end method
