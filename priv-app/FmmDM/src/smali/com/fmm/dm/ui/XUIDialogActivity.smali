.class public Lcom/fmm/dm/ui/XUIDialogActivity;
.super Landroid/app/Activity;
.source "XUIDialogActivity.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XEventInterface;
.implements Lcom/fmm/dm/interfaces/XNOTIInterface;
.implements Lcom/fmm/dm/interfaces/XUICInterface;
.implements Lcom/fmm/dm/interfaces/XUIInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;
    }
.end annotation


# static fields
.field public static g_UicOption:Ljava/lang/Object;

.field public static m_DialogId:I

.field private static m_UicResult:Ljava/lang/Object;


# instance fields
.field private m_UicPrgDialog:Landroid/app/ProgressDialog;

.field private m_UicTimer:Ljava/util/Timer;

.field private m_bDRMultiSelected:[Z

.field private m_nDRSingleSelected:I

.field private m_szDefaultResponseText:Ljava/lang/String;

.field private m_szResponseText:Ljava/lang/String;

.field private m_szUicConformText:Ljava/lang/String;

.field private m_szUicMenuList:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_DialogId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/fmm/dm/ui/XUIDialogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUIDialogActivity;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V

    return-void
.end method

.method static synthetic access$100(Lcom/fmm/dm/ui/XUIDialogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUIDialogActivity;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgUicDisplayResult()V

    return-void
.end method

.method static synthetic access$202(Lcom/fmm/dm/ui/XUIDialogActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUIDialogActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    return-object v0
.end method

.method private xuiDlgCPopupKillTimer()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 284
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    .line 285
    return-void
.end method

.method private xuiDlgPopup(I)V
    .locals 6
    .param p1, "type"    # I

    .prologue
    .line 243
    sget-object v2, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v2, Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 244
    .local v2, "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    const/4 v0, 0x0

    .line 246
    .local v0, "displayTime":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xuiUICPopup type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 249
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->minDT:I

    if-lez v3, :cond_0

    .line 251
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->minDT:I

    mul-int/lit16 v0, v3, 0x3e8

    .line 254
    :cond_0
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->maxDT:I

    if-lez v3, :cond_1

    .line 256
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->maxDT:I

    mul-int/lit16 v0, v3, 0x3e8

    .line 259
    :cond_1
    iget-object v3, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    if-nez v3, :cond_3

    .line 261
    if-lez v0, :cond_2

    .line 263
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "--> display time is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 264
    new-instance v1, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;

    add-int/lit8 v3, p1, 0x68

    invoke-direct {v1, p0, v3}, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;I)V

    .line 265
    .local v1, "mytime":Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;
    new-instance v3, Ljava/util/Timer;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/Timer;-><init>(Z)V

    iput-object v3, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    .line 266
    iget-object v3, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    int-to-long v4, v0

    invoke-virtual {v3, v1, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 273
    .end local v1    # "mytime":Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;
    :cond_2
    :goto_0
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v3, v3, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    iput-object v3, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    .line 276
    add-int/lit8 v3, p1, 0x68

    invoke-virtual {p0, v3}, Lcom/fmm/dm/ui/XUIDialogActivity;->showDialog(I)V

    .line 277
    return-void

    .line 271
    :cond_3
    const-string v3, "mUicTimer is running or already started!!"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xuiDlgUicDisplayResult()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 289
    sget-object v1, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v1, Lcom/fmm/dm/eng/core/XDMUicResult;

    .line 290
    .local v1, "uicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    const/4 v0, 0x0

    .line 291
    .local v0, "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    if-nez v1, :cond_0

    .line 302
    :goto_0
    return-void

    .line 294
    :cond_0
    const/4 v2, 0x1

    iput v2, v1, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    .line 296
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v0

    .line 297
    invoke-static {v0, v1}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCopyResult(Lcom/fmm/dm/eng/core/XDMUicResult;Lcom/fmm/dm/eng/core/XDMUicResult;)Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v0

    .line 299
    const/16 v2, 0x66

    invoke-static {v2, v0, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 300
    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/fmm/dm/eng/core/XDMUicResult;)Ljava/lang/Object;

    .line 301
    sput-object v3, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    goto :goto_0
.end method

.method private xuiDlgUicProgressResult(Ljava/lang/Object;)V
    .locals 1
    .param p1, "pThis"    # Ljava/lang/Object;

    .prologue
    .line 417
    move-object v0, p1

    check-cast v0, Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 419
    .local v0, "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/fmm/dm/eng/core/XDMUicOption;)Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 420
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 788
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 790
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUIDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 791
    .local v0, "sid":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sput v1, Lcom/fmm/dm/ui/XUIDialogActivity;->m_DialogId:I

    .line 792
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 12
    .param p1, "id"    # I

    .prologue
    const v7, 0x7f060025

    const/4 v11, 0x1

    const v10, 0x7f060003

    const v9, 0x108009b

    const v8, 0x1010355

    .line 425
    sparse-switch p1, :sswitch_data_0

    .line 782
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v2

    :goto_1
    return-object v2

    .line 428
    :sswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 429
    .local v2, "prgDialog":Landroid/app/ProgressDialog;
    invoke-virtual {v2, v9}, Landroid/app/ProgressDialog;->setIcon(I)V

    .line 430
    invoke-virtual {v2, v8}, Landroid/app/ProgressDialog;->setIconAttribute(I)V

    .line 431
    invoke-virtual {v2, v7}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 432
    const v6, 0x7f060007

    invoke-virtual {p0, v6}, Lcom/fmm/dm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 433
    new-instance v6, Lcom/fmm/dm/ui/XUIDialogActivity$1;

    invoke-direct {v6, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$1;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 445
    const/high16 v6, 0x7f060000

    invoke-virtual {p0, v6}, Lcom/fmm/dm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$2;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$2;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 457
    new-instance v6, Lcom/fmm/dm/ui/XUIDialogActivity$3;

    invoke-direct {v6, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$3;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto :goto_1

    .line 470
    .end local v2    # "prgDialog":Landroid/app/ProgressDialog;
    :sswitch_1
    const-string v3, ""

    .line 471
    .local v3, "szCompleteMsg":Ljava/lang/String;
    const v6, 0x7f06000e

    invoke-virtual {p0, v6}, Lcom/fmm/dm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 473
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$6;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$6;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v10, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$5;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$5;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$4;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$4;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_1

    .line 501
    .end local v3    # "szCompleteMsg":Ljava/lang/String;
    :sswitch_2
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f060006

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$9;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$9;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v10, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$8;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$8;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$7;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$7;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_1

    .line 529
    :sswitch_3
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMUicOption;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$12;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$12;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v10, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$11;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$11;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$10;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$10;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_1

    .line 559
    :sswitch_4
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMUicOption;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$16;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$16;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v10, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f060002

    new-instance v8, Lcom/fmm/dm/ui/XUIDialogActivity$15;

    invoke-direct {v8, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$15;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$14;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$14;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$13;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$13;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_1

    .line 601
    :sswitch_5
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 602
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v6, 0x7f030002

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 603
    .local v5, "textEntryView":Landroid/view/View;
    const v6, 0x7f070022

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 605
    .local v4, "text":Landroid/widget/EditText;
    if-nez v4, :cond_0

    .line 607
    const-string v6, "text is null"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 608
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V

    .line 609
    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    const/4 v7, 0x2

    invoke-virtual {p0, v6, v7}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgUicInputResult(Ljava/lang/Object;I)V

    .line 610
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUIDialogActivity;->finish()V

    goto/16 :goto_0

    .line 614
    :cond_0
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 616
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    iput-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    .line 617
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 618
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 621
    :cond_1
    new-instance v6, Lcom/fmm/dm/ui/XUIDialogActivity$17;

    invoke-direct {v6, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$17;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 636
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMUicOption;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicConformText:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$20;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$20;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v10, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$19;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$19;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$18;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$18;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_1

    .line 668
    .end local v0    # "factory":Landroid/view/LayoutInflater;
    .end local v4    # "text":Landroid/widget/EditText;
    .end local v5    # "textEntryView":Landroid/view/View;
    :sswitch_6
    iget v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 669
    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/core/XDMUicResult;

    iget v7, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v6, Lcom/fmm/dm/eng/core/XDMUicResult;->SingleSelected:I

    .line 671
    :cond_2
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMUicOption;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    iget v8, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    new-instance v9, Lcom/fmm/dm/ui/XUIDialogActivity$24;

    invoke-direct {v9, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$24;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$23;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$23;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v10, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$22;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$22;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$21;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$21;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_1

    .line 709
    :sswitch_7
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    if-eqz v6, :cond_4

    .line 711
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    array-length v6, v6

    if-ge v1, v6, :cond_4

    .line 713
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    aget-boolean v6, v6, v1

    if-eqz v6, :cond_3

    .line 714
    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/core/XDMUicResult;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMUicResult;->MultiSelected:[I

    aput v11, v6, v1

    .line 711
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 718
    .end local v1    # "i":I
    :cond_4
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMUicOption;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    iget-object v8, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    new-instance v9, Lcom/fmm/dm/ui/XUIDialogActivity$28;

    invoke-direct {v9, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$28;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$27;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$27;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v10, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$26;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$26;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$25;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$25;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_1

    .line 759
    :sswitch_8
    new-instance v6, Landroid/app/ProgressDialog;

    invoke-direct {v6, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    .line 760
    iget-object v7, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    sget-object v6, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMUicOption;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 761
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    const-string v7, "Please wait while Downloading..."

    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 762
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    const/16 v7, 0x64

    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 763
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 764
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6, v11}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 765
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6, v11}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 766
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    new-instance v7, Lcom/fmm/dm/ui/XUIDialogActivity$29;

    invoke-direct {v7, p0}, Lcom/fmm/dm/ui/XUIDialogActivity$29;-><init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 777
    iget-object v2, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_1

    .line 425
    nop

    :sswitch_data_0
    .sparse-switch
        0x67 -> :sswitch_0
        0x69 -> :sswitch_3
        0x6a -> :sswitch_4
        0x6b -> :sswitch_5
        0x6c -> :sswitch_6
        0x6d -> :sswitch_7
        0x6e -> :sswitch_8
        0x6f -> :sswitch_2
        0xab -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 798
    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 799
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 800
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 806
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 810
    :try_start_0
    sget v1, Lcom/fmm/dm/ui/XUIDialogActivity;->m_DialogId:I

    if-lez v1, :cond_0

    .line 812
    sget v1, Lcom/fmm/dm/ui/XUIDialogActivity;->m_DialogId:I

    invoke-virtual {p0, v1}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgShow(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 820
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 821
    return-void

    .line 815
    :catch_0
    move-exception v0

    .line 817
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xuiDlgShow(I)Z
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x67

    .line 825
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 826
    sparse-switch p1, :sswitch_data_0

    .line 849
    :goto_0
    return v3

    .line 829
    :sswitch_0
    invoke-virtual {p0, v2}, Lcom/fmm/dm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 832
    :sswitch_1
    invoke-virtual {p0, v2}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 833
    const/16 v0, 0xab

    invoke-virtual {p0, v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 836
    :sswitch_2
    invoke-virtual {p0, v2}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 837
    const/16 v0, 0x6f

    invoke-virtual {p0, v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 842
    :sswitch_3
    invoke-virtual {p0, v2}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 843
    sget-object v0, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-virtual {p0, v0, v3}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgUICRequest(Ljava/lang/Object;Z)V

    .line 844
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    goto :goto_0

    .line 826
    :sswitch_data_0
    .sparse-switch
        0x67 -> :sswitch_0
        0x68 -> :sswitch_3
        0x6f -> :sswitch_2
        0xab -> :sswitch_1
    .end sparse-switch
.end method

.method protected xuiDlgUICRequest(Ljava/lang/Object;Z)V
    .locals 10
    .param p1, "uicOption"    # Ljava/lang/Object;
    .param p2, "bDisplay"    # Z

    .prologue
    const/4 v7, 0x5

    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 124
    const/4 v2, 0x0

    .line 125
    .local v2, "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    const/4 v3, 0x0

    .line 127
    .local v3, "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    if-eqz p1, :cond_2

    move-object v2, p1

    .line 129
    check-cast v2, Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 137
    const-string v4, ""

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 139
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->UICType:I

    if-lt v4, v6, :cond_0

    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->UICType:I

    if-gt v4, v7, :cond_0

    .line 141
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v3

    .line 143
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->appId:I

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMUicResult;->appId:I

    .line 144
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->UICType:I

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMUicResult;->UICType:I

    .line 145
    sput-object v3, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    .line 148
    :cond_0
    if-eqz p2, :cond_1

    .line 150
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->UICType:I

    packed-switch v4, :pswitch_data_0

    .line 239
    :cond_1
    :goto_0
    return-void

    .line 133
    :cond_2
    const-string v4, "uicOption is null"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 154
    :pswitch_0
    invoke-direct {p0, v6}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto :goto_0

    .line 158
    :pswitch_1
    invoke-direct {p0, v5}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto :goto_0

    .line 162
    :pswitch_2
    sget-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget v4, v4, Lcom/fmm/dm/eng/core/XDMUicOption;->maxLen:I

    if-lez v4, :cond_3

    .line 163
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Max Len :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget v4, v4, Lcom/fmm/dm/eng/core/XDMUicOption;->maxLen:I

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicConformText:Ljava/lang/String;

    .line 164
    :cond_3
    const/4 v4, 0x3

    invoke-direct {p0, v4}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto :goto_0

    .line 168
    :pswitch_3
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuNumbers:I

    new-array v4, v4, [Ljava/lang/String;

    iput-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    .line 169
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuNumbers:I

    if-ge v1, v4, :cond_5

    .line 171
    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    aget-object v5, v5, v1

    aput-object v5, v4, v1

    .line 172
    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 173
    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v1

    .line 169
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 176
    :cond_5
    iget-object v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget v4, v4, Lcom/fmm/dm/eng/core/XDMText;->len:I

    if-eqz v4, :cond_6

    const-string v4, "0"

    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v5, v5, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_6

    .line 177
    iget-object v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    .line 181
    :goto_2
    const/4 v4, 0x4

    invoke-direct {p0, v4}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto/16 :goto_0

    .line 179
    :cond_6
    const/4 v4, -0x1

    iput v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    goto :goto_2

    .line 185
    .end local v1    # "i":I
    :pswitch_4
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuNumbers:I

    new-array v4, v4, [Ljava/lang/String;

    iput-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    .line 186
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuNumbers:I

    new-array v4, v4, [Z

    iput-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    .line 188
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuNumbers:I

    if-ge v1, v4, :cond_8

    .line 190
    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    aget-object v5, v5, v1

    aput-object v5, v4, v1

    .line 191
    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 192
    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v1

    .line 188
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 196
    :cond_8
    if-nez v3, :cond_9

    .line 198
    const-string v4, "pUicResult is null"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 202
    :cond_9
    const/4 v1, 0x0

    :goto_4
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMUicResult;->MultiSelected:[I

    array-length v4, v4

    if-ge v1, v4, :cond_c

    .line 204
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "DefaultSet":Ljava/lang/String;
    iget-object v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget v4, v4, Lcom/fmm/dm/eng/core/XDMText;->len:I

    if-eqz v4, :cond_b

    const-string v4, "0"

    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v5, v5, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_b

    .line 207
    iget-object v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 209
    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    if-eqz v4, :cond_a

    .line 211
    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    add-int/lit8 v5, v1, -0x1

    aput-boolean v6, v4, v5

    .line 202
    :cond_a
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 217
    :cond_b
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    goto :goto_5

    .line 221
    .end local v0    # "DefaultSet":Ljava/lang/String;
    :cond_c
    invoke-direct {p0, v7}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto/16 :goto_0

    .line 225
    .end local v1    # "i":I
    :pswitch_5
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->progrType:I

    if-nez v4, :cond_d

    .line 226
    const/4 v4, 0x6

    invoke-direct {p0, v4}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto/16 :goto_0

    .line 227
    :cond_d
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->progrType:I

    if-ne v4, v5, :cond_e

    .line 228
    sget-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-direct {p0, v4}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgUicProgressResult(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 231
    :cond_e
    iget-object v5, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    sget-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-wide v6, v4, Lcom/fmm/dm/eng/core/XDMUicOption;->progrCurSize:J

    const-wide/16 v8, 0x64

    mul-long/2addr v6, v8

    sget-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-wide v8, v4, Lcom/fmm/dm/eng/core/XDMUicOption;->progrMaxSize:J

    div-long/2addr v6, v8

    long-to-int v4, v6

    invoke-virtual {v5, v4}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto/16 :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public xuiDlgUicChoiceResult(Ljava/lang/Object;I)V
    .locals 7
    .param p1, "pThis"    # Ljava/lang/Object;
    .param p2, "StateCode"    # I

    .prologue
    const/16 v3, 0x10

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 369
    move-object v0, p1

    check-cast v0, Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 370
    .local v0, "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    sget-object v2, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v2, Lcom/fmm/dm/eng/core/XDMUicResult;

    .line 371
    .local v2, "uicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    const/4 v1, 0x0

    .line 373
    .local v1, "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    sparse-switch p2, :sswitch_data_0

    .line 399
    iput v3, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    .line 403
    :cond_0
    :goto_0
    iget v3, v0, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuNumbers:I

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->MenuNumbers:I

    .line 404
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v1

    .line 405
    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCopyResult(Lcom/fmm/dm/eng/core/XDMUicResult;Lcom/fmm/dm/eng/core/XDMUicResult;)Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v1

    .line 407
    const/16 v3, 0x66

    invoke-static {v3, v1, v4}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 408
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/fmm/dm/eng/core/XDMUicResult;)Ljava/lang/Object;

    .line 410
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/fmm/dm/eng/core/XDMUicOption;)Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 411
    sput-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    .line 412
    sput-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    .line 413
    return-void

    .line 377
    :sswitch_0
    iget v3, v0, Lcom/fmm/dm/eng/core/XDMUicOption;->UICType:I

    if-ne v3, v5, :cond_1

    .line 379
    iput v5, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 381
    :cond_1
    iget v3, v0, Lcom/fmm/dm/eng/core/XDMUicOption;->UICType:I

    if-ne v3, v6, :cond_0

    .line 383
    iput v6, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 389
    :sswitch_1
    const/4 v3, 0x2

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 394
    :sswitch_2
    iput v3, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 373
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method protected xuiDlgUicConfirmResult(Ljava/lang/Object;I)V
    .locals 5
    .param p1, "pThis"    # Ljava/lang/Object;
    .param p2, "StateCode"    # I

    .prologue
    const/4 v4, 0x0

    .line 306
    move-object v0, p1

    check-cast v0, Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 307
    .local v0, "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    sget-object v2, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v2, Lcom/fmm/dm/eng/core/XDMUicResult;

    .line 308
    .local v2, "uicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    const/4 v1, 0x0

    .line 310
    .local v1, "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    if-nez v2, :cond_0

    .line 324
    :goto_0
    return-void

    .line 313
    :cond_0
    iput p2, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    .line 315
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v1

    .line 316
    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCopyResult(Lcom/fmm/dm/eng/core/XDMUicResult;Lcom/fmm/dm/eng/core/XDMUicResult;)Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v1

    .line 318
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/fmm/dm/eng/core/XDMUicResult;)Ljava/lang/Object;

    .line 319
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/fmm/dm/eng/core/XDMUicOption;)Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 320
    sput-object v4, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    .line 321
    const/4 v0, 0x0

    .line 323
    const/16 v3, 0x66

    invoke-static {v3, v1, v4}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected xuiDlgUicInputResult(Ljava/lang/Object;I)V
    .locals 7
    .param p1, "pThis"    # Ljava/lang/Object;
    .param p2, "StateCode"    # I

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x0

    .line 328
    move-object v0, p1

    check-cast v0, Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 329
    .local v0, "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    sget-object v2, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v2, Lcom/fmm/dm/eng/core/XDMUicResult;

    .line 330
    .local v2, "uicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    const/4 v1, 0x0

    .line 332
    .local v1, "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "StateCode :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 334
    sparse-switch p2, :sswitch_data_0

    .line 351
    iput v6, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    .line 355
    :cond_0
    :goto_0
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v1

    .line 356
    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCopyResult(Lcom/fmm/dm/eng/core/XDMUicResult;Lcom/fmm/dm/eng/core/XDMUicResult;)Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v1

    .line 358
    const/16 v3, 0x66

    invoke-static {v3, v1, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 359
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/fmm/dm/eng/core/XDMUicResult;)Ljava/lang/Object;

    .line 360
    const/4 v2, 0x0

    .line 361
    sput-object v5, Lcom/fmm/dm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    .line 363
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/fmm/dm/eng/core/XDMUicOption;)Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 364
    sput-object v5, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    .line 365
    return-void

    .line 337
    :sswitch_0
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    .line 338
    iget-object v3, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 339
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v4, p0, Lcom/fmm/dm/ui/XUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppendStrText(Lcom/fmm/dm/eng/core/XDMText;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMText;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->text:Lcom/fmm/dm/eng/core/XDMText;

    goto :goto_0

    .line 343
    :sswitch_1
    iput v6, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 347
    :sswitch_2
    const/4 v3, 0x2

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 334
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_2
        0x10 -> :sswitch_1
    .end sparse-switch
.end method
