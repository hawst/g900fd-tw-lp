.class Lcom/fmm/dm/ui/XUINetProfileActivity$9;
.super Ljava/lang/Object;
.source "XUINetProfileActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fmm/dm/ui/XUINetProfileActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/ui/XUINetProfileActivity;


# direct methods
.method constructor <init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/fmm/dm/ui/XUINetProfileActivity$9;->this$0:Lcom/fmm/dm/ui/XUINetProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 244
    const-string v0, ""

    .line 245
    .local v0, "proxyAddr":Ljava/lang/String;
    # getter for: Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;
    invoke-static {}, Lcom/fmm/dm/ui/XUINetProfileActivity;->access$000()Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    move-result-object v1

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v0, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 246
    const-string v1, "0.0.0.0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 248
    # getter for: Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;
    invoke-static {}, Lcom/fmm/dm/ui/XUINetProfileActivity;->access$000()Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    move-result-object v1

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 255
    :goto_0
    # getter for: Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;
    invoke-static {}, Lcom/fmm/dm/ui/XUINetProfileActivity;->access$000()Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    move-result-object v1

    # getter for: Lcom/fmm/dm/ui/XUINetProfileActivity;->m_nNProfile:I
    invoke-static {}, Lcom/fmm/dm/ui/XUINetProfileActivity;->access$200()I

    move-result v2

    iput v2, v1, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ActivateID:I

    .line 256
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    # getter for: Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;
    invoke-static {}, Lcom/fmm/dm/ui/XUINetProfileActivity;->access$000()Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    move-result-object v2

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 257
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetConRef(Lcom/fmm/dm/db/file/XDBInfoConRef;)V

    .line 258
    iget-object v1, p0, Lcom/fmm/dm/ui/XUINetProfileActivity$9;->this$0:Lcom/fmm/dm/ui/XUINetProfileActivity;

    # invokes: Lcom/fmm/dm/ui/XUINetProfileActivity;->xuiCallUiDmProfile()V
    invoke-static {v1}, Lcom/fmm/dm/ui/XUINetProfileActivity;->access$100(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    .line 259
    return-void

    .line 252
    :cond_0
    # getter for: Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;
    invoke-static {}, Lcom/fmm/dm/ui/XUINetProfileActivity;->access$000()Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    move-result-object v1

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    goto :goto_0
.end method
