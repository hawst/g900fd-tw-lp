.class public Lcom/fmm/dm/ui/XUINotificationManager;
.super Ljava/lang/Object;
.source "XUINotificationManager.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XUIInterface;


# static fields
.field public static final SETTINGS_LOCATION_SECURITY:Ljava/lang/String; = "android.settings.SECURITY_SETTINGS"

.field public static final STATUS_NOTIFICATION_DM:I = 0x1

.field private static m_BackupIndicatorState:I

.field private static m_Context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    .line 24
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/ui/XUINotificationManager;->m_BackupIndicatorState:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getDeleteIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 153
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const-class v2, Lcom/fmm/dm/XDMBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 154
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.NOTI_CANCELED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    sget-object v1, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static xuiCallBackupIndicator()V
    .locals 1

    .prologue
    .line 71
    sget v0, Lcom/fmm/dm/ui/XUINotificationManager;->m_BackupIndicatorState:I

    invoke-static {v0}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 72
    return-void
.end method

.method public static xuiInitBackupIndicator()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/ui/XUINotificationManager;->m_BackupIndicatorState:I

    .line 77
    return-void
.end method

.method public static xuiNotiInitialize(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    sput-object p0, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    .line 29
    return-void
.end method

.method public static xuiSetIndicator(I)V
    .locals 8
    .param p0, "nIndicator"    # I

    .prologue
    .line 33
    sput p0, Lcom/fmm/dm/ui/XUINotificationManager;->m_BackupIndicatorState:I

    .line 34
    sget-object v1, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 36
    const-string v1, "m_Context is null, return"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    const-string v1, "notification"

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 42
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-nez v0, :cond_2

    .line 44
    const-string v1, "NotificationManager is null, return"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :cond_2
    packed-switch p0, :pswitch_data_0

    .line 64
    const-string v1, "nIndicatorState is NONE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :pswitch_0
    const v1, 0x7f020001

    const v2, 0x7f060011

    const v3, 0x7f06000f

    const v4, 0x7f060010

    const/4 v5, 0x3

    move v6, p0

    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto :goto_0

    .line 54
    :pswitch_1
    const/high16 v1, 0x7f020000

    const v2, 0x7f06000d

    const v3, 0x7f06000b

    const v4, 0x7f06000c

    const/4 v5, 0x4

    move v6, p0

    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto :goto_0

    .line 57
    :pswitch_2
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/4 v1, 0x5

    if-ge v7, v1, :cond_0

    .line 59
    invoke-virtual {v0, v7}, Landroid/app/NotificationManager;->cancel(I)V

    .line 57
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 48
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V
    .locals 14
    .param p0, "notificationManager"    # Landroid/app/NotificationManager;
    .param p1, "moodId"    # I
    .param p2, "titleId"    # I
    .param p3, "textId"    # I
    .param p4, "tickerId"    # I
    .param p5, "id"    # I
    .param p6, "nIndicatorState"    # I

    .prologue
    .line 81
    const/4 v7, 0x0

    .line 82
    .local v7, "text":Ljava/lang/CharSequence;
    const/4 v8, 0x0

    .line 83
    .local v8, "titletext":Ljava/lang/CharSequence;
    const/4 v6, 0x0

    .line 84
    .local v6, "szTickerText":Ljava/lang/String;
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    if-nez v9, :cond_0

    .line 86
    const-string v9, "m_Context is null, return"

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 149
    :goto_0
    return-void

    .line 90
    :cond_0
    if-nez p0, :cond_1

    .line 92
    const-string v9, "NotificationManager is null, return"

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_1
    if-lez p3, :cond_2

    .line 97
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    move/from16 v0, p3

    invoke-virtual {v9, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    .line 98
    :cond_2
    if-lez p2, :cond_3

    .line 99
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    .line 100
    :cond_3
    if-lez p4, :cond_4

    .line 101
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    move/from16 v0, p4

    invoke-virtual {v9, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    .end local v6    # "szTickerText":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 103
    .restart local v6    # "szTickerText":Ljava/lang/String;
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "NotificationID : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " / Notification IndicatorState : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p6

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 105
    new-instance v4, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v4, p1, v6, v10, v11}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 108
    .local v4, "notification":Landroid/app/Notification;
    packed-switch p6, :pswitch_data_0

    .line 141
    const/16 v9, 0x20

    iput v9, v4, Landroid/app/Notification;->flags:I

    .line 142
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const/4 v10, 0x0

    new-instance v11, Landroid/content/Intent;

    sget-object v12, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const-class v13, Lcom/fmm/dm/XDMApplication;

    invoke-direct {v11, v12, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 143
    .local v2, "contentIntent":Landroid/app/PendingIntent;
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    invoke-virtual {v4, v9, v8, v7, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 147
    :goto_1
    invoke-static {}, Lcom/fmm/dm/ui/XUINotificationManager;->getDeleteIntent()Landroid/app/PendingIntent;

    move-result-object v9

    iput-object v9, v4, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 148
    move/from16 v0, p5

    invoke-virtual {p0, v0, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 112
    .end local v2    # "contentIntent":Landroid/app/PendingIntent;
    :pswitch_0
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetGetLocationRequestor()Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "requestorString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 115
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const v10, 0x7f060026

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 118
    :cond_5
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 119
    .local v3, "i":Landroid/content/Intent;
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v3, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 120
    .restart local v2    # "contentIntent":Landroid/app/PendingIntent;
    const/16 v9, 0x10

    iput v9, v4, Landroid/app/Notification;->flags:I

    .line 121
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    invoke-virtual {v4, v9, v8, v7, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_1

    .line 126
    .end local v2    # "contentIntent":Landroid/app/PendingIntent;
    .end local v3    # "i":Landroid/content/Intent;
    .end local v5    # "requestorString":Ljava/lang/String;
    :pswitch_1
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetEmergencyModeRequestor()Ljava/lang/String;

    move-result-object v5

    .line 127
    .restart local v5    # "requestorString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 129
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const v10, 0x7f060026

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 132
    :cond_6
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 133
    .restart local v3    # "i":Landroid/content/Intent;
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v3, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 134
    .restart local v2    # "contentIntent":Landroid/app/PendingIntent;
    const/16 v9, 0x10

    iput v9, v4, Landroid/app/Notification;->flags:I

    .line 135
    sget-object v9, Lcom/fmm/dm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    invoke-virtual {v4, v9, v8, v7, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_1

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
