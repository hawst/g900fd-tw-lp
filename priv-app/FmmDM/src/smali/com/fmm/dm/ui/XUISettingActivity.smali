.class public Lcom/fmm/dm/ui/XUISettingActivity;
.super Landroid/preference/PreferenceActivity;
.source "XUISettingActivity.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDBInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;


# static fields
.field private static final DIALOG_SELECT_PROFILE:I = 0x1


# instance fields
.field private m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

.field private m_DlgNet:Landroid/app/Dialog;

.field private m_DlgProfile:Landroid/app/Dialog;

.field private m_PrefRoot:Landroid/preference/PreferenceScreen;

.field private m_PrefScreenNet:Landroid/preference/PreferenceScreen;

.field private m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

.field private m_ProNetworkProfileList:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

.field private m_nSelectedProfileIndex:I

.field private m_szItemProfile:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgNet:Landroid/app/Dialog;

    .line 29
    iput-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgProfile:Landroid/app/Dialog;

    .line 30
    iput-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    return-void
.end method

.method static synthetic access$000(Lcom/fmm/dm/ui/XUISettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUISettingActivity;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->xuiCallUiDmProfile()V

    return-void
.end method

.method static synthetic access$100(Lcom/fmm/dm/ui/XUISettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUISettingActivity;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->xuiUpdateProfile()V

    return-void
.end method

.method static synthetic access$200(Lcom/fmm/dm/ui/XUISettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUISettingActivity;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->xuiUpdateNetworkProfile()V

    return-void
.end method

.method static synthetic access$300(Lcom/fmm/dm/ui/XUISettingActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUISettingActivity;

    .prologue
    .line 23
    iget v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    return v0
.end method

.method static synthetic access$302(Lcom/fmm/dm/ui/XUISettingActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUISettingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    return p1
.end method

.method static synthetic access$400(Lcom/fmm/dm/ui/XUISettingActivity;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUISettingActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgNet:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/fmm/dm/ui/XUISettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUISettingActivity;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->xuiCallUiDmNetProfile()V

    return-void
.end method

.method private xuiCallUiDmNetProfile()V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/fmm/dm/ui/XUINetProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "profileIndex"

    iget v2, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0, v0}, Lcom/fmm/dm/ui/XUISettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 69
    return-void
.end method

.method private xuiCallUiDmProfile()V
    .locals 3

    .prologue
    .line 59
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/fmm/dm/ui/XUIProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "profileIndex"

    iget v2, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 61
    invoke-virtual {p0, v0}, Lcom/fmm/dm/ui/XUISettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 62
    return-void
.end method

.method private xuiUpdateNetworkProfile()V
    .locals 2

    .prologue
    .line 49
    sget-object v1, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v0, v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->szAccountName:Ljava/lang/String;

    .line 51
    .local v0, "NetworkName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    const-string v0, "Unknown"

    .line 54
    :cond_0
    iget-object v1, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 55
    return-void
.end method

.method private xuiUpdateProfile()V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_szItemProfile:[Ljava/lang/String;

    iget v2, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 44
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 183
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 189
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    if-eqz v0, :cond_0

    .line 191
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileInfo()Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 192
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbReadListInfo(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iput-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_ProNetworkProfileList:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    .line 193
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_ProNetworkProfileList:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    if-eqz v0, :cond_0

    .line 195
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v1, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_ProNetworkProfileList:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 198
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    .line 199
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 118
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 77
    :pswitch_0
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060018

    invoke-virtual {p0, v1}, Lcom/fmm/dm/ui/XUISettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_szItemProfile:[Ljava/lang/String;

    iget v2, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    new-instance v3, Lcom/fmm/dm/ui/XUISettingActivity$3;

    invoke-direct {v3, p0}, Lcom/fmm/dm/ui/XUISettingActivity$3;-><init>(Lcom/fmm/dm/ui/XUISettingActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060004

    new-instance v2, Lcom/fmm/dm/ui/XUISettingActivity$2;

    invoke-direct {v2, p0}, Lcom/fmm/dm/ui/XUISettingActivity$2;-><init>(Lcom/fmm/dm/ui/XUISettingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060001

    new-instance v2, Lcom/fmm/dm/ui/XUISettingActivity$1;

    invoke-direct {v2, p0}, Lcom/fmm/dm/ui/XUISettingActivity$1;-><init>(Lcom/fmm/dm/ui/XUISettingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgProfile:Landroid/app/Dialog;

    .line 112
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgProfile:Landroid/app/Dialog;

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onStart()V
    .locals 5

    .prologue
    .line 125
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    iput-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_szItemProfile:[Ljava/lang/String;

    .line 126
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget v3, v3, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    iput v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    .line 128
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    .line 130
    new-instance v2, Landroid/preference/PreferenceCategory;

    invoke-direct {v2, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 131
    .local v2, "launchPrefCatTitle":Landroid/preference/PreferenceCategory;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f060025

    invoke-virtual {p0, v4}, Lcom/fmm/dm/ui/XUISettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f060020

    invoke-virtual {p0, v4}, Lcom/fmm/dm/ui/XUISettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 135
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 136
    .local v0, "launchPrefCat":Landroid/preference/PreferenceCategory;
    const v3, 0x7f060018

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 137
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 139
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    .line 140
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    const-string v4, "screen_preference"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setKey(Ljava/lang/String;)V

    .line 141
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->xuiUpdateProfile()V

    .line 142
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 144
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    new-instance v4, Lcom/fmm/dm/ui/XUISettingActivity$4;

    invoke-direct {v4, p0}, Lcom/fmm/dm/ui/XUISettingActivity$4;-><init>(Lcom/fmm/dm/ui/XUISettingActivity;)V

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 158
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 159
    .local v1, "launchPrefCatNet":Landroid/preference/PreferenceCategory;
    const v3, 0x7f060015

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 160
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 162
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    .line 163
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    const-string v4, "screen_preferencenet"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setKey(Ljava/lang/String;)V

    .line 164
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUISettingActivity;->xuiUpdateNetworkProfile()V

    .line 165
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 167
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    new-instance v4, Lcom/fmm/dm/ui/XUISettingActivity$5;

    invoke-direct {v4, p0}, Lcom/fmm/dm/ui/XUISettingActivity$5;-><init>(Lcom/fmm/dm/ui/XUISettingActivity;)V

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 176
    iget-object v3, p0, Lcom/fmm/dm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v3}, Lcom/fmm/dm/ui/XUISettingActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 177
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 178
    return-void
.end method
