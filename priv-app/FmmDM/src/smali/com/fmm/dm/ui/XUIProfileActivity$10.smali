.class Lcom/fmm/dm/ui/XUIProfileActivity$10;
.super Ljava/lang/Object;
.source "XUIProfileActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fmm/dm/ui/XUIProfileActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/ui/XUIProfileActivity;


# direct methods
.method constructor <init>(Lcom/fmm/dm/ui/XUIProfileActivity;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 239
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x3

    if-ge v1, v6, :cond_0

    .line 241
    const-string v3, ""

    .line 242
    .local v3, "pAddress":Ljava/lang/String;
    const-string v0, ""

    .line 243
    .local v0, "URL":Ljava/lang/String;
    const/4 v2, 0x0

    .line 244
    .local v2, "nPort":I
    const-string v4, ""

    .line 245
    .local v4, "pProtocol":Ljava/lang/String;
    const/4 v5, 0x0

    .line 246
    .local v5, "parser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v5

    .line 247
    iget-object v0, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    .line 248
    iget-object v3, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    .line 249
    iget v2, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    .line 250
    iget-object v4, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    .line 251
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iput-object v0, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 252
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iput-object v3, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 253
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iput v2, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    .line 254
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iput-object v4, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 256
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v7}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v7, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 257
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v7}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    iput-object v7, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 258
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v7}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget v7, v7, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    iput v7, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 259
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v7}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    iput-object v7, v6, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 260
    const/4 v6, 0x1

    # setter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_bRowState:Z
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$302(Z)Z

    .line 261
    # setter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_nRow:I
    invoke-static {v1}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$402(I)I

    .line 263
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileInfo(Lcom/fmm/dm/db/file/XDBProfileInfo;)Z

    .line 264
    sget-object v6, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v6, v6, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v6, v6, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    iget-object v7, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v7}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$000(Lcom/fmm/dm/ui/XUIProfileActivity;)[Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    aput-object v7, v6, v1

    .line 266
    const/4 v0, 0x0

    .line 267
    const/4 v3, 0x0

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 272
    .end local v0    # "URL":Ljava/lang/String;
    .end local v2    # "nPort":I
    .end local v3    # "pAddress":Ljava/lang/String;
    .end local v4    # "pProtocol":Ljava/lang/String;
    .end local v5    # "parser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    :cond_0
    const/4 v6, 0x0

    # setter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_bRowState:Z
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$302(Z)Z

    .line 273
    sget-object v6, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v6, v6, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_nSelectedIndex:I
    invoke-static {}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$100()I

    move-result v7

    iput v7, v6, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 274
    sget-object v6, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v6, v6, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetProflieList(Ljava/lang/Object;)V

    .line 275
    # getter for: Lcom/fmm/dm/ui/XUIProfileActivity;->m_nSelectedIndex:I
    invoke-static {}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$100()I

    move-result v6

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileIndex(I)V

    .line 276
    iget-object v6, p0, Lcom/fmm/dm/ui/XUIProfileActivity$10;->this$0:Lcom/fmm/dm/ui/XUIProfileActivity;

    # invokes: Lcom/fmm/dm/ui/XUIProfileActivity;->xuiCallUiDmNetProfile()V
    invoke-static {v6}, Lcom/fmm/dm/ui/XUIProfileActivity;->access$200(Lcom/fmm/dm/ui/XUIProfileActivity;)V

    .line 277
    return-void
.end method
