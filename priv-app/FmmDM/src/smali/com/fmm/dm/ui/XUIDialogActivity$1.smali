.class Lcom/fmm/dm/ui/XUIDialogActivity$1;
.super Ljava/lang/Object;
.source "XUIDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fmm/dm/ui/XUIDialogActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/ui/XUIDialogActivity;


# direct methods
.method constructor <init>(Lcom/fmm/dm/ui/XUIDialogActivity;)V
    .locals 0

    .prologue
    .line 434
    iput-object p1, p0, Lcom/fmm/dm/ui/XUIDialogActivity$1;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x1

    .line 437
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 438
    invoke-static {v2}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetUserCancel(Z)V

    .line 439
    const/16 v1, 0xfb

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/fmm/dm/eng/core/XDMAbortMsgParam;

    move-result-object v0

    .line 441
    .local v0, "pFailParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    const/16 v1, 0x19

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 442
    iget-object v1, p0, Lcom/fmm/dm/ui/XUIDialogActivity$1;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-virtual {v1}, Lcom/fmm/dm/ui/XUIDialogActivity;->finish()V

    .line 443
    return-void
.end method
