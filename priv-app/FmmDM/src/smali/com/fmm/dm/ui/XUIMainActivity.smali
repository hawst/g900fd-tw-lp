.class public Lcom/fmm/dm/ui/XUIMainActivity;
.super Landroid/preference/PreferenceActivity;
.source "XUIMainActivity.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDBInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;


# static fields
.field private static final KEY_DM_SETTING:Ljava/lang/String; = "settingdm"

.field private static final KEY_SOFTWARE_UPDATE:Ljava/lang/String; = "startdm"

.field public static g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

.field private static m_Builder:Landroid/app/AlertDialog$Builder;

.field private static m_ConnectAlertDialog:Landroid/app/AlertDialog;


# instance fields
.field private m_Context:Landroid/content/Context;

.field private m_szResponseText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    sput-object v0, Lcom/fmm/dm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    .line 47
    sput-object v0, Lcom/fmm/dm/ui/XUIMainActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/ui/XUIMainActivity;->m_szResponseText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/fmm/dm/ui/XUIMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUIMainActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUIMainActivity;->xuiCallUiDmSetting()V

    return-void
.end method

.method static synthetic access$100(Lcom/fmm/dm/ui/XUIMainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUIMainActivity;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIMainActivity;->m_szResponseText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/fmm/dm/ui/XUIMainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUIMainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/fmm/dm/ui/XUIMainActivity;->m_szResponseText:Ljava/lang/String;

    return-object p1
.end method

.method private xuiCallUiDmSetting()V
    .locals 3

    .prologue
    .line 226
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 228
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_1

    .line 230
    const v1, 0x7f060008

    invoke-virtual {p0, v1}, Lcom/fmm/dm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v1

    if-gtz v1, :cond_0

    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 239
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/fmm/dm/ui/XUISettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 240
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/fmm/dm/ui/XUIMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private xuiInputCommandDialog()V
    .locals 6

    .prologue
    .line 140
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 141
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v3, 0x7f030002

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 142
    .local v2, "textEntryView":Landroid/view/View;
    const v3, 0x7f070022

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 143
    .local v1, "text":Landroid/widget/EditText;
    if-eqz v1, :cond_0

    .line 145
    new-instance v3, Lcom/fmm/dm/ui/XUIMainActivity$3;

    invoke-direct {v3, p0}, Lcom/fmm/dm/ui/XUIMainActivity$3;-><init>(Lcom/fmm/dm/ui/XUIMainActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 162
    :cond_0
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/fmm/dm/ui/XUIMainActivity;->m_Context:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    .line 163
    sget-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    const v4, 0x7f060009

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 164
    sget-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    const-string v4, "Input command key string"

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 165
    sget-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060003

    new-instance v5, Lcom/fmm/dm/ui/XUIMainActivity$4;

    invoke-direct {v5, p0}, Lcom/fmm/dm/ui/XUIMainActivity$4;-><init>(Lcom/fmm/dm/ui/XUIMainActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 172
    sget-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    sput-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    .line 173
    sget-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/fmm/dm/ui/XUIMainActivity$5;

    invoke-direct {v4, p0}, Lcom/fmm/dm/ui/XUIMainActivity$5;-><init>(Lcom/fmm/dm/ui/XUIMainActivity;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 180
    sget-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 181
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    iput-object p0, p0, Lcom/fmm/dm/ui/XUIMainActivity;->m_Context:Landroid/content/Context;

    .line 66
    new-instance v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v3, p0, Lcom/fmm/dm/ui/XUIMainActivity;->m_Context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    .line 67
    sget-object v2, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v2, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    if-nez v2, :cond_0

    .line 68
    sget-object v2, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    new-instance v3, Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-direct {v3}, Lcom/fmm/dm/agent/XDMAppConnectSetting;-><init>()V

    iput-object v3, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    .line 70
    :cond_0
    sget-object v2, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v2, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    if-nez v2, :cond_1

    .line 71
    sget-object v2, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    new-instance v3, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    invoke-direct {v3}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;-><init>()V

    iput-object v3, v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    .line 73
    :cond_1
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetProxyData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    sput-object v2, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    .line 75
    const/high16 v2, 0x7f040000

    invoke-virtual {p0, v2}, Lcom/fmm/dm/ui/XUIMainActivity;->addPreferencesFromResource(I)V

    .line 77
    const-string v2, "startdm"

    invoke-virtual {p0, v2}, Lcom/fmm/dm/ui/XUIMainActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 78
    .local v1, "SwPref":Landroid/preference/Preference;
    if-eqz v1, :cond_2

    .line 80
    new-instance v2, Lcom/fmm/dm/ui/XUIMainActivity$1;

    invoke-direct {v2, p0}, Lcom/fmm/dm/ui/XUIMainActivity$1;-><init>(Lcom/fmm/dm/ui/XUIMainActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 90
    :cond_2
    const-string v2, "settingdm"

    invoke-virtual {p0, v2}, Lcom/fmm/dm/ui/XUIMainActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 91
    .local v0, "SettingPref":Landroid/preference/Preference;
    if-eqz v0, :cond_3

    .line 93
    new-instance v2, Lcom/fmm/dm/ui/XUIMainActivity$2;

    invoke-direct {v2, p0}, Lcom/fmm/dm/ui/XUIMainActivity$2;-><init>(Lcom/fmm/dm/ui/XUIMainActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 108
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/fmm/dm/ui/XUIMainActivity;->setDefaultKeyMode(I)V

    .line 109
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x0

    .line 120
    const-string v0, "Input MasterKey"

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x1080042

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 121
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 127
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 135
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 130
    :pswitch_0
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUIMainActivity;->xuiInputCommandDialog()V

    goto :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 57
    return-void
.end method

.method protected xuiCallUiDmStartDm()V
    .locals 2

    .prologue
    .line 245
    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 247
    sget-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v0, :cond_1

    .line 249
    const v0, 0x7f060008

    invoke-virtual {p0, v0}, Lcom/fmm/dm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    const/4 v0, 0x1

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileIndex()I

    move-result v1

    invoke-static {v0, v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpUserInitiate(II)V

    goto :goto_0
.end method

.method protected xuiSelectTestMenu(Ljava/lang/String;)V
    .locals 3
    .param p1, "szInputKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key event occured on Dialog : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 186
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    const-string v0, "reset"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 191
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbFullRestAll()V

    .line 192
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileInfo()Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 193
    invoke-static {v2}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 194
    const-string v0, "Reset FFS Complete"

    invoke-static {p0, v0, v2}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 196
    :cond_2
    const-string v0, "dev"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetOEMName :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetOEMName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetFullDeviceID "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetManufacturer :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetModel :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetLanguage :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetFirmwareVersion :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFirmwareVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetSoftwareVersion :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetSoftwareVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetHardwareVersion :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetHardwareVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmDevAdpGetIMSIFromSIM :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetIMSIFromSIM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdmGetTargetSIMState :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetSIMState()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 209
    :cond_3
    const-string v0, "wbxml"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 211
    invoke-static {}, Lcom/fmm/dm/agent/XDMDebug;->xdmSetWbxmlFileLogOnOff()V

    goto/16 :goto_0

    .line 213
    :cond_4
    const-string v0, "dump"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 215
    invoke-static {}, Lcom/fmm/dm/agent/XDMDebug;->xdmSetWbxmlDumpLogOnOff()V

    goto/16 :goto_0

    .line 217
    :cond_5
    const-string v0, "privatelog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 219
    invoke-static {}, Lcom/fmm/dm/agent/XDMDebug;->xdmSetPrivateLogOnOff()V

    goto/16 :goto_0
.end method
