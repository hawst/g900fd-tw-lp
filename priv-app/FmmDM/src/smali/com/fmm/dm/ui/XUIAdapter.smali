.class public Lcom/fmm/dm/ui/XUIAdapter;
.super Ljava/lang/Object;
.source "XUIAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XEventInterface;
.implements Lcom/fmm/dm/interfaces/XNOTIInterface;
.implements Lcom/fmm/dm/interfaces/XUIInterface;


# static fields
.field private static m_bEmergencyModeChageAlert:Z

.field private static m_bReActiveAlert:Z

.field private static m_bUnlockAlert:Z

.field private static m_nDmUiMode:I

.field private static m_nInitType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    sput v0, Lcom/fmm/dm/ui/XUIAdapter;->m_nDmUiMode:I

    .line 29
    sput v0, Lcom/fmm/dm/ui/XUIAdapter;->m_nInitType:I

    .line 30
    sput-boolean v0, Lcom/fmm/dm/ui/XUIAdapter;->m_bUnlockAlert:Z

    .line 31
    sput-boolean v0, Lcom/fmm/dm/ui/XUIAdapter;->m_bEmergencyModeChageAlert:Z

    .line 32
    sput-boolean v0, Lcom/fmm/dm/ui/XUIAdapter;->m_bReActiveAlert:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInitType()I
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/ui/XUIAdapter;->m_nInitType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 37
    sget v0, Lcom/fmm/dm/ui/XUIAdapter;->m_nInitType:I

    return v0
.end method

.method public static setInitType(I)V
    .locals 2
    .param p0, "nInitType"    # I

    .prologue
    .line 42
    sput p0, Lcom/fmm/dm/ui/XUIAdapter;->m_nInitType:I

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/ui/XUIAdapter;->m_nInitType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public static xuiAdpGetEmergencyModeChangeAlert()Z
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_bEmergencyModeChageAlert : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/ui/XUIAdapter;->m_bEmergencyModeChageAlert:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 74
    sget-boolean v0, Lcom/fmm/dm/ui/XUIAdapter;->m_bEmergencyModeChageAlert:Z

    return v0
.end method

.method public static xuiAdpGetReActiveAlert()Z
    .locals 2

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_bReActiveAlert : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/ui/XUIAdapter;->m_bReActiveAlert:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 92
    sget-boolean v0, Lcom/fmm/dm/ui/XUIAdapter;->m_bReActiveAlert:Z

    return v0
.end method

.method public static xuiAdpGetUiMode()I
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nDmUiMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/ui/XUIAdapter;->m_nDmUiMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 49
    sget v0, Lcom/fmm/dm/ui/XUIAdapter;->m_nDmUiMode:I

    return v0
.end method

.method public static xuiAdpGetUnlockAlert()Z
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_bUnlockAlert : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/ui/XUIAdapter;->m_bUnlockAlert:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 61
    sget-boolean v0, Lcom/fmm/dm/ui/XUIAdapter;->m_bUnlockAlert:Z

    return v0
.end method

.method public static xuiAdpRequestNoti(I)V
    .locals 2
    .param p0, "nMsgType"    # I

    .prologue
    const/4 v1, 0x2

    .line 97
    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 99
    packed-switch p0, :pswitch_data_0

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 103
    :pswitch_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbCheckProfileListExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 106
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_0

    .line 113
    :pswitch_1
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbCheckProfileListExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    const-string v0, "XUI_DM_NOTI_INFORMATIVE. FMM backgroundmode"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 118
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_0

    .line 125
    :pswitch_2
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbCheckProfileListExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "XUI_DM_NOTI_INTERACTIVE. FMM backgroundmode"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 130
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x8d
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xuiAdpSetEmergencyModeChangeAlert(Z)V
    .locals 2
    .param p0, "bChange"    # Z

    .prologue
    .line 79
    sput-boolean p0, Lcom/fmm/dm/ui/XUIAdapter;->m_bEmergencyModeChageAlert:Z

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_bEmergencyModeChageAlert : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/ui/XUIAdapter;->m_bEmergencyModeChageAlert:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public static xuiAdpSetReActiveAlert(Z)V
    .locals 2
    .param p0, "bUnlock"    # Z

    .prologue
    .line 85
    sput-boolean p0, Lcom/fmm/dm/ui/XUIAdapter;->m_bReActiveAlert:Z

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_bReActiveAlert : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/ui/XUIAdapter;->m_bReActiveAlert:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public static xuiAdpSetUiMode(I)V
    .locals 2
    .param p0, "nUiMode"    # I

    .prologue
    .line 54
    sput p0, Lcom/fmm/dm/ui/XUIAdapter;->m_nDmUiMode:I

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nDmUiMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/ui/XUIAdapter;->m_nDmUiMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public static xuiAdpSetUnlockAlert(Z)V
    .locals 2
    .param p0, "bUnlock"    # Z

    .prologue
    .line 66
    sput-boolean p0, Lcom/fmm/dm/ui/XUIAdapter;->m_bUnlockAlert:Z

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_bUnlockAlert : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/ui/XUIAdapter;->m_bUnlockAlert:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public static xuiAdpStartSession()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 256
    const-string v2, ""

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 257
    const/4 v0, 0x0

    .line 259
    .local v0, "nStatus":I
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v0

    .line 260
    if-eqz v0, :cond_0

    .line 270
    :goto_0
    return v1

    .line 265
    :cond_0
    invoke-static {v1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetUserCancel(Z)V

    .line 266
    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpResetWBXMLLog()V

    .line 268
    const/16 v1, 0x65

    invoke-static {v3, v1}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 269
    const/16 v1, 0xb

    invoke-static {v1, v3, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 270
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xuiAdpUserInitiate(II)V
    .locals 3
    .param p0, "initType"    # I
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 275
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 276
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    .line 278
    .local v0, "context":Landroid/content/Context;
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_1

    .line 280
    const-string v1, "DM Not Init"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 281
    const v1, 0x7f060008

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v1

    if-nez v1, :cond_0

    .line 290
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileIndex(I)V

    .line 292
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v1

    if-nez v1, :cond_2

    .line 294
    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpCookieClear()V

    .line 295
    invoke-static {p0}, Lcom/fmm/dm/ui/XUIAdapter;->setInitType(I)V

    .line 297
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    const/16 v2, 0x67

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 300
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_0

    .line 305
    :cond_2
    const-string v1, "network is not ready or sessioning....."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 306
    const v1, 0x7f06000a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static xuiEmergencyModeChangeAlertReport()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 232
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    .line 234
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 236
    const-string v1, "EmergencyMode Alert start"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 237
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetEmergencyModeChangeAlert(Z)V

    .line 238
    invoke-static {v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus(I)V

    .line 240
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileIndex(I)V

    .line 242
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    const-string v1, "network is not ready or sessioning....."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 250
    const v1, 0x7f06000a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static xuiReactivationLockAlertReport()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 207
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 208
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    .line 210
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 212
    const-string v1, "ReactivationLock Alert start"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 213
    invoke-static {v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus(I)V

    .line 214
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetReActiveAlert(Z)V

    .line 216
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileIndex(I)V

    .line 217
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    const-string v1, "network is not ready or sessioning....."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 225
    const v1, 0x7f06000a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static xuiSimChangeAlertReport()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 143
    const/4 v2, 0x0

    .local v2, "bSimSwap":Z
    const/4 v0, 0x0

    .line 144
    .local v0, "bPrevState":Z
    const/4 v1, 0x0

    .line 148
    .local v1, "bRegistration":Z
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpVerifySubscId()Z

    move-result v2

    .line 149
    if-nez v2, :cond_0

    .line 151
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSimChangeAlert()Z

    move-result v0

    .line 152
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Previous state is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 158
    :cond_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/XDMService;->xdmGetAccountRegistration(Landroid/content/Context;)Z

    move-result v1

    .line 160
    if-nez v1, :cond_2

    .line 162
    const-string v3, "Device registration wasn\'t registered or MobileTracker is off"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    :cond_1
    move v3, v4

    .line 178
    :goto_0
    return v3

    .line 164
    :cond_2
    if-nez v2, :cond_3

    if-eqz v0, :cond_1

    .line 166
    :cond_3
    const-string v5, "SIM Change Alert start"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 167
    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xlawmoSetbSimChange(Z)V

    .line 168
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetSimChangeAlert(Z)V

    .line 169
    invoke-static {v4}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus(I)V

    .line 171
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileIndex(I)V

    .line 172
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 174
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_0
.end method

.method public static xuiUnlockAlertReport()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 183
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 184
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    .line 186
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 188
    const-string v1, "Unlock Alert start"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 189
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUnlockAlert(Z)V

    .line 190
    invoke-static {v2}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus(I)V

    .line 192
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileIndex(I)V

    .line 193
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const-string v1, "network is not ready or sessioning....."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 201
    const v1, 0x7f06000a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method
