.class public Lcom/fmm/dm/ui/XUINetProfileActivity;
.super Landroid/app/TabActivity;
.source "XUINetProfileActivity.java"


# static fields
.field private static final DIALOG_PROFILE_EDIT_YES_NO:I = 0x1

.field private static m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

.field private static m_bUpdateState:Z

.field private static m_nNProfile:I

.field private static m_nProfile:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    sput v0, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_nNProfile:I

    .line 38
    sput-boolean v0, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/fmm/dm/db/file/XDBNetworkProfileList;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/fmm/dm/ui/XUINetProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUINetProfileActivity;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->xuiCallUiDmProfile()V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_nNProfile:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 34
    sput p0, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_nNProfile:I

    return p0
.end method

.method static synthetic access$300(Lcom/fmm/dm/ui/XUINetProfileActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/ui/XUINetProfileActivity;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/fmm/dm/ui/XUINetProfileActivity;->xuiCurrentTabSet(I)V

    return-void
.end method

.method public static getUpdateState()Z
    .locals 1

    .prologue
    .line 45
    sget-boolean v0, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    return v0
.end method

.method public static setUpdateState(I)V
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 50
    if-nez p0, :cond_0

    .line 51
    const/4 v0, 0x0

    sput-boolean v0, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    .line 54
    :goto_0
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    goto :goto_0
.end method

.method private xuiCallUiDmProfile()V
    .locals 3

    .prologue
    .line 221
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/fmm/dm/ui/XUIProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 222
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "profileIndex"

    sget v2, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_nProfile:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 223
    invoke-virtual {p0, v0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 224
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->finish()V

    .line 225
    return-void
.end method

.method private xuiCurrentTabSet(I)V
    .locals 12
    .param p1, "selectTap"    # I

    .prologue
    const v11, 0x1090009

    const v10, 0x1090008

    .line 59
    const v8, 0x7f070003

    invoke-virtual {p0, v8}, Lcom/fmm/dm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    .line 60
    .local v7, "text":Landroid/widget/EditText;
    sget-object v8, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 61
    new-instance v8, Lcom/fmm/dm/ui/XUINetProfileActivity$1;

    invoke-direct {v8, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$1;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 77
    const v8, 0x7f070005

    invoke-virtual {p0, v8}, Lcom/fmm/dm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "text":Landroid/widget/EditText;
    check-cast v7, Landroid/widget/EditText;

    .line 78
    .restart local v7    # "text":Landroid/widget/EditText;
    sget-object v8, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 79
    new-instance v8, Lcom/fmm/dm/ui/XUINetProfileActivity$2;

    invoke-direct {v8, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$2;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 95
    const v8, 0x7f070007

    invoke-virtual {p0, v8}, Lcom/fmm/dm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "text":Landroid/widget/EditText;
    check-cast v7, Landroid/widget/EditText;

    .line 97
    .restart local v7    # "text":Landroid/widget/EditText;
    sget-object v8, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v8, v8, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 98
    .local v6, "szPortNbr":Ljava/lang/String;
    invoke-virtual {v7, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 99
    new-instance v8, Lcom/fmm/dm/ui/XUINetProfileActivity$3;

    invoke-direct {v8, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$3;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 118
    const v8, 0x7f070009

    invoke-virtual {p0, v8}, Lcom/fmm/dm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "text":Landroid/widget/EditText;
    check-cast v7, Landroid/widget/EditText;

    .line 119
    .restart local v7    # "text":Landroid/widget/EditText;
    sget-object v8, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 120
    new-instance v8, Lcom/fmm/dm/ui/XUINetProfileActivity$4;

    invoke-direct {v8, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$4;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 137
    const v8, 0x7f07000b

    invoke-virtual {p0, v8}, Lcom/fmm/dm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "text":Landroid/widget/EditText;
    check-cast v7, Landroid/widget/EditText;

    .line 138
    .restart local v7    # "text":Landroid/widget/EditText;
    sget-object v8, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 139
    new-instance v8, Lcom/fmm/dm/ui/XUINetProfileActivity$5;

    invoke-direct {v8, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$5;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 155
    const v8, 0x7f07000d

    invoke-virtual {p0, v8}, Lcom/fmm/dm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    .line 157
    .local v4, "spin":Landroid/widget/Spinner;
    const/4 v2, 0x0

    .line 158
    .local v2, "nApnType":I
    sget-object v8, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v8, v8, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->apntype:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 159
    const/4 v2, -0x1

    .line 170
    :goto_0
    const/high16 v8, 0x7f050000

    invoke-static {p0, v8, v10}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 171
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v0, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 172
    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 173
    invoke-virtual {v4, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 174
    new-instance v8, Lcom/fmm/dm/ui/XUINetProfileActivity$6;

    invoke-direct {v8, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$6;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v4, v8}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 195
    const v8, 0x7f07000f

    invoke-virtual {p0, v8}, Lcom/fmm/dm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    .line 196
    .local v5, "spin1":Landroid/widget/Spinner;
    const/4 v3, 0x0

    .line 198
    .local v3, "nAuthType":I
    sget-object v8, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v8, v8, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->auth:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 199
    sget-object v8, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v8, v8, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->auth:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 201
    :cond_0
    const v8, 0x7f050002

    invoke-static {p0, v8, v10}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    .line 202
    .local v1, "adapter1":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v1, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 203
    invoke-virtual {v5, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 204
    add-int/lit8 v8, v3, -0x1

    invoke-virtual {v5, v8}, Landroid/widget/Spinner;->setSelection(I)V

    .line 205
    new-instance v8, Lcom/fmm/dm/ui/XUINetProfileActivity$7;

    invoke-direct {v8, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$7;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v5, v8}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 216
    return-void

    .line 162
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v1    # "adapter1":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v3    # "nAuthType":I
    .end local v5    # "spin1":Landroid/widget/Spinner;
    :cond_1
    const-string v8, "*"

    sget-object v9, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v9, v9, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->apntype:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_2

    .line 163
    const/4 v2, 0x0

    goto :goto_0

    .line 164
    :cond_2
    const-string v8, "default"

    sget-object v9, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v9, v9, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->apntype:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_3

    .line 165
    const/4 v2, 0x1

    goto :goto_0

    .line 167
    :cond_3
    const/4 v2, 0x2

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 308
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 309
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "profileIndex"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_nProfile:I

    .line 310
    sput-boolean v7, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    .line 311
    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbExistsNetworkRow(J)Z

    move-result v3

    if-nez v3, :cond_0

    .line 313
    const-string v3, "Please Setting APN"

    invoke-static {p0, v3, v6}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 314
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->finish()V

    .line 358
    :goto_0
    return-void

    .line 318
    :cond_0
    new-instance v3, Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    invoke-direct {v3}, Lcom/fmm/dm/db/file/XDBNetworkProfileList;-><init>()V

    sput-object v3, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    .line 319
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->NetProfileClass:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    sput-object v3, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    .line 321
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v1

    .line 322
    .local v1, "tabHost":Landroid/widget/TabHost;
    sget-object v3, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v0, v3, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->szAccountName:Ljava/lang/String;

    .line 324
    .local v0, "item":Ljava/lang/String;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const/high16 v4, 0x7f030000

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 327
    const-string v3, "tab1"

    invoke-virtual {v1, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    .line 328
    .local v2, "ts1":Landroid/widget/TabHost$TabSpec;
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x108000c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    .line 329
    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 330
    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 332
    new-instance v3, Lcom/fmm/dm/ui/XUINetProfileActivity$11;

    invoke-direct {v3, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$11;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 355
    sget v3, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_nNProfile:I

    invoke-direct {p0, v3}, Lcom/fmm/dm/ui/XUINetProfileActivity;->xuiCurrentTabSet(I)V

    .line 356
    sget v3, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_nNProfile:I

    invoke-virtual {v1, v3}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 230
    packed-switch p1, :pswitch_data_0

    .line 272
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 233
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "PROFILE EDIT"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Do you want Save and Close Network info ?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/fmm/dm/ui/XUINetProfileActivity$10;

    invoke-direct {v1, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$10;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f060005

    new-instance v2, Lcom/fmm/dm/ui/XUINetProfileActivity$9;

    invoke-direct {v2, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$9;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f060000

    new-instance v2, Lcom/fmm/dm/ui/XUINetProfileActivity$8;

    invoke-direct {v2, p0}, Lcom/fmm/dm/ui/XUINetProfileActivity$8;-><init>(Lcom/fmm/dm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 230
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 299
    const-string v0, "SAVE"

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 300
    const/4 v0, 0x1

    const-string v1, "Revert"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 301
    const/4 v0, 0x2

    const-string v1, "Edit Profile Info"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108003e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 302
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 383
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 393
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/TabActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 386
    :pswitch_0
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->xuiNetProfileSave()V

    .line 387
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 383
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 278
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 293
    :goto_0
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 281
    :pswitch_0
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->xuiNetProfileSave()V

    .line 282
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 285
    :pswitch_1
    invoke-virtual {p0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 288
    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/fmm/dm/ui/XUINetProfileActivity;->showDialog(I)V

    goto :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected xuiNetProfileSave()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 362
    const-string v0, ""

    .line 363
    .local v0, "proxyAddr":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v0, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 364
    const-string v1, "0.0.0.0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 366
    sget-object v1, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-boolean v3, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 373
    :goto_0
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    sget-object v2, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 375
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetConRef(Lcom/fmm/dm/db/file/XDBInfoConRef;)V

    .line 377
    const-string v1, "Saved"

    invoke-static {p0, v1, v3}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 378
    return-void

    .line 370
    :cond_0
    sget-object v1, Lcom/fmm/dm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    goto :goto_0
.end method
