.class Lcom/fmm/dm/ui/XUISettingActivity$2;
.super Ljava/lang/Object;
.source "XUISettingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fmm/dm/ui/XUISettingActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/ui/XUISettingActivity;


# direct methods
.method constructor <init>(Lcom/fmm/dm/ui/XUISettingActivity;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/fmm/dm/ui/XUISettingActivity$2;->this$0:Lcom/fmm/dm/ui/XUISettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity$2;->this$0:Lcom/fmm/dm/ui/XUISettingActivity;

    # invokes: Lcom/fmm/dm/ui/XUISettingActivity;->xuiUpdateProfile()V
    invoke-static {v0}, Lcom/fmm/dm/ui/XUISettingActivity;->access$100(Lcom/fmm/dm/ui/XUISettingActivity;)V

    .line 91
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity$2;->this$0:Lcom/fmm/dm/ui/XUISettingActivity;

    # invokes: Lcom/fmm/dm/ui/XUISettingActivity;->xuiUpdateNetworkProfile()V
    invoke-static {v0}, Lcom/fmm/dm/ui/XUISettingActivity;->access$200(Lcom/fmm/dm/ui/XUISettingActivity;)V

    .line 92
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v1, p0, Lcom/fmm/dm/ui/XUISettingActivity$2;->this$0:Lcom/fmm/dm/ui/XUISettingActivity;

    # getter for: Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I
    invoke-static {v1}, Lcom/fmm/dm/ui/XUISettingActivity;->access$300(Lcom/fmm/dm/ui/XUISettingActivity;)I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 93
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetProflieList(Ljava/lang/Object;)V

    .line 94
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity$2;->this$0:Lcom/fmm/dm/ui/XUISettingActivity;

    # getter for: Lcom/fmm/dm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I
    invoke-static {v0}, Lcom/fmm/dm/ui/XUISettingActivity;->access$300(Lcom/fmm/dm/ui/XUISettingActivity;)I

    move-result v0

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileIndex(I)V

    .line 95
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileInfo()Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 96
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetConRef()Lcom/fmm/dm/db/file/XDBInfoConRef;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 98
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity$2;->this$0:Lcom/fmm/dm/ui/XUISettingActivity;

    # getter for: Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgNet:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/fmm/dm/ui/XUISettingActivity;->access$400(Lcom/fmm/dm/ui/XUISettingActivity;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity$2;->this$0:Lcom/fmm/dm/ui/XUISettingActivity;

    # getter for: Lcom/fmm/dm/ui/XUISettingActivity;->m_DlgNet:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/fmm/dm/ui/XUISettingActivity;->access$400(Lcom/fmm/dm/ui/XUISettingActivity;)Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f060015

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/fmm/dm/ui/XUISettingActivity$2;->this$0:Lcom/fmm/dm/ui/XUISettingActivity;

    invoke-virtual {v0}, Lcom/fmm/dm/ui/XUISettingActivity;->onStart()V

    .line 103
    return-void
.end method
