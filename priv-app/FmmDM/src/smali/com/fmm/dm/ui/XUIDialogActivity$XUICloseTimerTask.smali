.class public Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;
.super Ljava/util/TimerTask;
.source "XUIDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/ui/XUIDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "XUICloseTimerTask"
.end annotation


# instance fields
.field SelectDialog:I

.field final synthetic this$0:Lcom/fmm/dm/ui/XUIDialogActivity;


# direct methods
.method public constructor <init>(Lcom/fmm/dm/ui/XUIDialogActivity;I)V
    .locals 1
    .param p2, "type"    # I

    .prologue
    .line 60
    iput-object p1, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    .line 61
    iput p2, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    .line 62
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 73
    iget v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    sparse-switch v0, :sswitch_data_0

    .line 118
    :goto_0
    return-void

    .line 76
    :sswitch_0
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    const/16 v1, 0xab

    invoke-virtual {v0, v1}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 77
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 81
    :sswitch_1
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    # invokes: Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->access$000(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    .line 82
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 83
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    # invokes: Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgUicDisplayResult()V
    invoke-static {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->access$100(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    .line 84
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 88
    :sswitch_2
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    # invokes: Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->access$000(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    .line 89
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 90
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    sget-object v1, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgUicConfirmResult(Ljava/lang/Object;I)V

    .line 91
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 95
    :sswitch_3
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    # invokes: Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->access$000(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    .line 96
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 97
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    sget-object v1, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgUicInputResult(Ljava/lang/Object;I)V

    .line 98
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 103
    :sswitch_4
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    # invokes: Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->access$000(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    .line 104
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 105
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    sget-object v1, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgUicChoiceResult(Ljava/lang/Object;I)V

    .line 106
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 110
    :sswitch_5
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    # invokes: Lcom/fmm/dm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->access$000(Lcom/fmm/dm/ui/XUIDialogActivity;)V

    .line 111
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/fmm/dm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 112
    iget-object v0, p0, Lcom/fmm/dm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/fmm/dm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/fmm/dm/ui/XUIDialogActivity;->finish()V

    goto/16 :goto_0

    .line 73
    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_1
        0x6a -> :sswitch_2
        0x6b -> :sswitch_3
        0x6c -> :sswitch_4
        0x6d -> :sswitch_4
        0x6e -> :sswitch_5
        0xab -> :sswitch_0
    .end sparse-switch
.end method

.method public scheduledExecutionTime()J
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Ljava/util/TimerTask;->scheduledExecutionTime()J

    move-result-wide v0

    return-wide v0
.end method
