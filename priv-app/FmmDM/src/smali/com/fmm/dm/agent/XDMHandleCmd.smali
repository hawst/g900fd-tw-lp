.class public Lcom/fmm/dm/agent/XDMHandleCmd;
.super Ljava/lang/Object;
.source "XDMHandleCmd.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1452
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;

    .line 1453
    .local v0, "alert":Lcom/fmm/dm/eng/parser/XDMParserAlert;
    if-nez v0, :cond_0

    .line 1472
    :goto_0
    return-void

    .line 1457
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    .line 1459
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    .line 1461
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v1, :cond_1

    .line 1463
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteCred(Ljava/lang/Object;)V

    .line 1465
    :cond_1
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 1467
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_2

    .line 1469
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V

    .line 1471
    :cond_2
    const/4 v0, 0x0

    .line 1472
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteCred(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1412
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 1413
    .local v0, "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    if-nez v0, :cond_0

    .line 1424
    :goto_0
    return-void

    .line 1417
    :cond_0
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    .line 1419
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_1

    .line 1421
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1423
    :cond_1
    const/4 v0, 0x0

    .line 1424
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteElelist(Ljava/lang/Object;)V
    .locals 4
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1432
    move-object v1, p0

    check-cast v1, Lcom/fmm/dm/eng/core/XDMList;

    .line 1435
    .local v1, "h":Lcom/fmm/dm/eng/core/XDMList;
    move-object v0, v1

    .line 1436
    .local v0, "curr":Lcom/fmm/dm/eng/core/XDMList;
    :goto_0
    if-eqz v0, :cond_0

    .line 1438
    move-object v2, v0

    .line 1439
    .local v2, "tmp":Lcom/fmm/dm/eng/core/XDMList;
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 1441
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .line 1442
    const/4 v2, 0x0

    goto :goto_0

    .line 1444
    .end local v2    # "tmp":Lcom/fmm/dm/eng/core/XDMList;
    :cond_0
    return-void
.end method

.method public static xdmAgentDataStDeleteItem(Ljava/lang/Object;)V
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1343
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 1344
    .local v0, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    if-nez v0, :cond_0

    .line 1364
    :goto_0
    return-void

    .line 1349
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_1

    .line 1351
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeletePcdata(Ljava/lang/Object;)V

    .line 1354
    :cond_1
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 1355
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .line 1357
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_2

    .line 1359
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1362
    :cond_2
    const/4 v0, 0x0

    .line 1364
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V
    .locals 4
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1321
    move-object v1, p0

    check-cast v1, Lcom/fmm/dm/eng/core/XDMList;

    .line 1322
    .local v1, "header":Lcom/fmm/dm/eng/core/XDMList;
    move-object v0, v1

    .line 1325
    .local v0, "curr":Lcom/fmm/dm/eng/core/XDMList;
    :goto_0
    if-eqz v0, :cond_0

    .line 1327
    move-object v2, v0

    .line 1328
    .local v2, "tmp":Lcom/fmm/dm/eng/core/XDMList;
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 1330
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItem(Ljava/lang/Object;)V

    .line 1331
    const/4 v2, 0x0

    goto :goto_0

    .line 1334
    .end local v2    # "tmp":Lcom/fmm/dm/eng/core/XDMList;
    :cond_0
    const/4 v1, 0x0

    .line 1335
    return-void
.end method

.method public static xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V
    .locals 4
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1254
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 1255
    .local v0, "meta":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    if-nez v0, :cond_0

    .line 1278
    :goto_0
    return-void

    .line 1259
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    if-eqz v1, :cond_1

    .line 1261
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMetinfAnchor(Ljava/lang/Object;)V

    .line 1263
    :cond_1
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->mem:Lcom/fmm/dm/eng/parser/XDMParserMem;

    if-eqz v1, :cond_2

    .line 1265
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->mem:Lcom/fmm/dm/eng/parser/XDMParserMem;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMetinfMem(Ljava/lang/Object;)V

    .line 1268
    :cond_2
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    .line 1269
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 1270
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    .line 1271
    iput v3, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxmsgsize:I

    .line 1272
    iput v3, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxobjsize:I

    .line 1273
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 1274
    iput v3, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    .line 1275
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 1276
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    .line 1277
    const/4 v0, 0x0

    .line 1278
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteMetinfAnchor(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1286
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    .line 1287
    .local v0, "anchor":Lcom/fmm/dm/eng/parser/XDMParserAnchor;
    if-nez v0, :cond_0

    .line 1295
    :goto_0
    return-void

    .line 1292
    :cond_0
    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    .line 1293
    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    .line 1294
    const/4 v0, 0x0

    .line 1295
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteMetinfMem(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1303
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserMem;

    .line 1304
    .local v0, "mem":Lcom/fmm/dm/eng/parser/XDMParserMem;
    if-nez v0, :cond_0

    .line 1313
    :goto_0
    return-void

    .line 1309
    :cond_0
    iput v1, v0, Lcom/fmm/dm/eng/parser/XDMParserMem;->free:I

    .line 1310
    iput v1, v0, Lcom/fmm/dm/eng/parser/XDMParserMem;->freeid:I

    .line 1311
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    .line 1312
    const/4 v0, 0x0

    .line 1313
    goto :goto_0
.end method

.method public static xdmAgentDataStDeletePcdata(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1372
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 1373
    .local v0, "pcdata":Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    if-nez v0, :cond_0

    .line 1385
    :goto_0
    return-void

    .line 1378
    :cond_0
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 1379
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    if-eqz v1, :cond_1

    .line 1381
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMetinfAnchor(Ljava/lang/Object;)V

    .line 1384
    :cond_1
    const/4 v0, 0x0

    .line 1385
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteReplace(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1480
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;

    .line 1481
    .local v0, "rep":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    if-nez v0, :cond_0

    .line 1500
    :goto_0
    return-void

    .line 1486
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v1, :cond_1

    .line 1488
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteCred(Ljava/lang/Object;)V

    .line 1490
    :cond_1
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_2

    .line 1492
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V

    .line 1494
    :cond_2
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_3

    .line 1496
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1498
    :cond_3
    const/4 v1, 0x0

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    .line 1499
    const/4 v0, 0x0

    .line 1500
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteResults(Ljava/lang/Object;)V
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1531
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserResults;

    .line 1532
    .local v0, "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    if-nez v0, :cond_0

    .line 1553
    :goto_0
    return-void

    .line 1537
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->cmdid:I

    .line 1538
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    .line 1539
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    .line 1541
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_1

    .line 1543
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1545
    :cond_1
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    .line 1546
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    .line 1548
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_2

    .line 1550
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V

    .line 1552
    :cond_2
    const/4 v0, 0x0

    .line 1553
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteStatus(Ljava/lang/Object;)V
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1212
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .line 1213
    .local v0, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    if-nez v0, :cond_0

    .line 1246
    :goto_0
    return-void

    .line 1218
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_1

    .line 1220
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1222
    :cond_1
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_2

    .line 1224
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V

    .line 1226
    :cond_2
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v1, :cond_3

    .line 1228
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteCred(Ljava/lang/Object;)V

    .line 1230
    :cond_3
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    .line 1231
    const/4 v1, 0x0

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cmdid:I

    .line 1232
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    .line 1233
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 1234
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    .line 1236
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->sourceref:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_4

    .line 1238
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->sourceref:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteElelist(Ljava/lang/Object;)V

    .line 1240
    :cond_4
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->targetref:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_5

    .line 1242
    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->targetref:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteElelist(Ljava/lang/Object;)V

    .line 1244
    :cond_5
    const/4 v0, 0x0

    .line 1246
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteTarget(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1393
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserTarget;

    .line 1395
    .local v0, "pTarget":Lcom/fmm/dm/eng/parser/XDMParserTarget;
    if-nez v0, :cond_0

    .line 1404
    :goto_0
    return-void

    .line 1400
    :cond_0
    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserTarget;->m_szLocURI:Ljava/lang/String;

    .line 1401
    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserTarget;->m_szLocName:Ljava/lang/String;

    .line 1403
    const/4 v0, 0x0

    .line 1404
    goto :goto_0
.end method

.method public static xdmAgentDataStDuplAlert(Lcom/fmm/dm/eng/parser/XDMParserAlert;Lcom/fmm/dm/eng/parser/XDMParserAlert;)V
    .locals 2
    .param p0, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserAlert;
    .param p1, "src"    # Lcom/fmm/dm/eng/parser/XDMParserAlert;

    .prologue
    .line 686
    if-nez p1, :cond_1

    .line 712
    :cond_0
    :goto_0
    return-void

    .line 691
    :cond_1
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    if-lez v0, :cond_2

    .line 693
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    iput v0, p0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    .line 695
    :cond_2
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 697
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    .line 699
    :cond_3
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_4

    .line 701
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 702
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V

    .line 704
    :cond_4
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 706
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 708
    :cond_5
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V
    .locals 2
    .param p0, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserCred;
    .param p1, "src"    # Lcom/fmm/dm/eng/parser/XDMParserCred;

    .prologue
    .line 867
    if-nez p1, :cond_1

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 871
    :cond_1
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 873
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    .line 875
    :cond_2
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_0

    .line 877
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 878
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplItem(Lcom/fmm/dm/eng/parser/XDMParserItem;Lcom/fmm/dm/eng/parser/XDMParserItem;)V
    .locals 2
    .param p0, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserItem;
    .param p1, "src"    # Lcom/fmm/dm/eng/parser/XDMParserItem;

    .prologue
    .line 977
    if-nez p1, :cond_0

    .line 1002
    :goto_0
    return-void

    .line 982
    :cond_0
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 984
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .line 986
    :cond_1
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 988
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 990
    :cond_2
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_3

    .line 992
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 993
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 995
    :cond_3
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v0, :cond_4

    .line 997
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 998
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplPcdata(Lcom/fmm/dm/eng/parser/XDMParserPcdata;Lcom/fmm/dm/eng/parser/XDMParserPcdata;)V

    .line 1000
    :cond_4
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    iput v0, p0, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;
    .locals 7
    .param p0, "src"    # Lcom/fmm/dm/eng/core/XDMList;

    .prologue
    .line 949
    move-object v0, p0

    .line 952
    .local v0, "curr":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v1, 0x0

    .local v1, "head":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v3, 0x0

    .line 954
    .local v3, "tail":Lcom/fmm/dm/eng/core/XDMList;
    :goto_0
    if-eqz v0, :cond_1

    .line 956
    move-object v4, v0

    .line 957
    .local v4, "tmp":Lcom/fmm/dm/eng/core/XDMList;
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 959
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .local v2, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    move-object v5, v2

    .line 960
    check-cast v5, Lcom/fmm/dm/eng/parser/XDMParserItem;

    iget-object v6, v4, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    check-cast v6, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-static {v5, v6}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItem(Lcom/fmm/dm/eng/parser/XDMParserItem;Lcom/fmm/dm/eng/parser/XDMParserItem;)V

    .line 961
    if-nez v1, :cond_0

    .line 962
    invoke-static {v1, v3, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    goto :goto_0

    .line 964
    :cond_0
    invoke-static {v1, v3, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0

    .line 967
    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v4    # "tmp":Lcom/fmm/dm/eng/core/XDMList;
    :cond_1
    return-object v1
.end method

.method public static xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V
    .locals 2
    .param p0, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserMeta;
    .param p1, "src"    # Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .prologue
    .line 889
    if-nez p1, :cond_1

    .line 940
    :cond_0
    :goto_0
    return-void

    .line 894
    :cond_1
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 896
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 898
    :cond_2
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 900
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 902
    :cond_3
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 904
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    .line 906
    :cond_4
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    if-lez v0, :cond_5

    .line 908
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    iput v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    .line 910
    :cond_5
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 912
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 914
    :cond_6
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 916
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    .line 918
    :cond_7
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxmsgsize:I

    if-lez v0, :cond_8

    .line 920
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxmsgsize:I

    iput v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxmsgsize:I

    .line 922
    :cond_8
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxobjsize:I

    if-lez v0, :cond_9

    .line 924
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxobjsize:I

    iput v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxobjsize:I

    .line 926
    :cond_9
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->mem:Lcom/fmm/dm/eng/parser/XDMParserMem;

    if-eqz v0, :cond_a

    .line 928
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMem;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMem;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->mem:Lcom/fmm/dm/eng/parser/XDMParserMem;

    .line 929
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->mem:Lcom/fmm/dm/eng/parser/XDMParserMem;

    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->mem:Lcom/fmm/dm/eng/parser/XDMParserMem;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMetinfMem(Lcom/fmm/dm/eng/parser/XDMParserMem;Lcom/fmm/dm/eng/parser/XDMParserMem;)V

    .line 931
    :cond_a
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 933
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    .line 935
    :cond_b
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    if-eqz v0, :cond_0

    .line 937
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserAnchor;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    .line 938
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMetinfAnchor(Lcom/fmm/dm/eng/parser/XDMParserAnchor;Lcom/fmm/dm/eng/parser/XDMParserAnchor;)V

    goto/16 :goto_0
.end method

.method public static xdmAgentDataStDuplMetinfAnchor(Lcom/fmm/dm/eng/parser/XDMParserAnchor;Lcom/fmm/dm/eng/parser/XDMParserAnchor;)V
    .locals 1
    .param p0, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserAnchor;
    .param p1, "src"    # Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    .prologue
    .line 1071
    if-nez p1, :cond_1

    .line 1084
    :cond_0
    :goto_0
    return-void

    .line 1076
    :cond_1
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1078
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    .line 1080
    :cond_2
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1082
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplMetinfMem(Lcom/fmm/dm/eng/parser/XDMParserMem;Lcom/fmm/dm/eng/parser/XDMParserMem;)V
    .locals 1
    .param p0, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserMem;
    .param p1, "src"    # Lcom/fmm/dm/eng/parser/XDMParserMem;

    .prologue
    .line 1046
    if-nez p1, :cond_1

    .line 1062
    :cond_0
    :goto_0
    return-void

    .line 1050
    :cond_1
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMem;->free:I

    if-lez v0, :cond_2

    .line 1052
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMem;->free:I

    iput v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMem;->free:I

    .line 1054
    :cond_2
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMem;->freeid:I

    if-lez v0, :cond_3

    .line 1056
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMem;->freeid:I

    iput v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMem;->freeid:I

    .line 1058
    :cond_3
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1060
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplPcdata(Lcom/fmm/dm/eng/parser/XDMParserPcdata;Lcom/fmm/dm/eng/parser/XDMParserPcdata;)V
    .locals 3
    .param p0, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    .param p1, "src"    # Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .prologue
    .line 1011
    if-nez p1, :cond_1

    .line 1037
    :cond_0
    :goto_0
    return-void

    .line 1016
    :cond_1
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    iput v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    .line 1017
    iget v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    if-nez v1, :cond_3

    .line 1019
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    iput-object v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 1020
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    iput v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 1032
    :cond_2
    :goto_1
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    if-eqz v1, :cond_0

    .line 1034
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserAnchor;-><init>()V

    iput-object v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    .line 1035
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    invoke-static {v1, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMetinfAnchor(Lcom/fmm/dm/eng/parser/XDMParserAnchor;Lcom/fmm/dm/eng/parser/XDMParserAnchor;)V

    goto :goto_0

    .line 1024
    :cond_3
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_2

    .line 1026
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 1027
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    if-ge v0, v1, :cond_4

    .line 1028
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v2, v2, v0

    aput-char v2, v1, v0

    .line 1027
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1029
    :cond_4
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    iput v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    goto :goto_1
.end method

.method public static xdmAgentDataStDuplResults(Lcom/fmm/dm/eng/parser/XDMParserResults;Lcom/fmm/dm/eng/parser/XDMParserResults;)V
    .locals 2
    .param p0, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserResults;
    .param p1, "src"    # Lcom/fmm/dm/eng/parser/XDMParserResults;

    .prologue
    .line 1588
    if-nez p1, :cond_1

    .line 1622
    :cond_0
    :goto_0
    return-void

    .line 1593
    :cond_1
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->cmdid:I

    if-lez v0, :cond_2

    .line 1595
    iget v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->cmdid:I

    iput v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->cmdid:I

    .line 1597
    :cond_2
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1599
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    .line 1601
    :cond_3
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1603
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    .line 1605
    :cond_4
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_5

    .line 1607
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 1608
    iget-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 1610
    :cond_5
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1612
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    .line 1614
    :cond_6
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1616
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    .line 1618
    :cond_7
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 1620
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public static xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;
    .locals 3
    .param p0, "pcdata"    # Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .prologue
    const/4 v1, 0x0

    .line 1562
    const-string v0, ""

    .line 1564
    .local v0, "szData":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 1578
    :cond_0
    :goto_0
    return-object v1

    .line 1568
    :cond_1
    iget v2, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    if-nez v2, :cond_0

    .line 1573
    iget-object v2, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v2, :cond_0

    .line 1576
    iget-object v1, p0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1578
    goto :goto_0
.end method

.method public static xdmAgentDataStString2Pcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    .locals 4
    .param p0, "str"    # [C

    .prologue
    .line 1510
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    .line 1512
    .local v1, "o":Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    if-nez v1, :cond_1

    .line 1514
    const/4 v1, 0x0

    .line 1522
    .end local v1    # "o":Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    :cond_0
    return-object v1

    .line 1517
    .restart local v1    # "o":Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    :cond_1
    const/4 v2, 0x0

    iput v2, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    .line 1518
    array-length v2, p0

    iput v2, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 1519
    array-length v2, p0

    new-array v2, v2, [C

    iput-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 1520
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 1521
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v3, p0, v0

    aput-char v3, v2, v0

    .line 1520
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public xdmAgentDataStDeleteSequence(Ljava/lang/Object;)V
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1169
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;

    .line 1170
    .local v1, "sequence":Lcom/fmm/dm/eng/parser/XDMParserSequence;
    const/4 v0, 0x0

    .line 1172
    .local v0, "cmd":Lcom/fmm/dm/agent/XDMAgent;
    if-nez v1, :cond_0

    .line 1204
    :goto_0
    return-void

    .line 1175
    :cond_0
    iput v4, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->cmdid:I

    .line 1176
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v2, :cond_1

    .line 1178
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->anchor:Lcom/fmm/dm/eng/parser/XDMParserAnchor;

    .line 1179
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    .line 1180
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 1181
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    .line 1182
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput v4, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxmsgsize:I

    .line 1183
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput v4, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxobjsize:I

    .line 1184
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->mem:Lcom/fmm/dm/eng/parser/XDMParserMem;

    .line 1185
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 1186
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput v4, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    .line 1187
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 1188
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    .line 1189
    iput-object v3, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 1192
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v2, :cond_2

    .line 1194
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v4}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 1195
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 1197
    .restart local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    :goto_1
    if-eqz v0, :cond_2

    .line 1199
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 1200
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .restart local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    goto :goto_1

    .line 1203
    :cond_2
    const/4 v1, 0x0

    .line 1204
    goto :goto_0
.end method

.method public xdmAgentDataStDuplAdd(Lcom/fmm/dm/eng/parser/XDMParserAdd;Lcom/fmm/dm/eng/parser/XDMParserAdd;)V
    .locals 2
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserAdd;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserAdd;

    .prologue
    .line 721
    if-nez p2, :cond_1

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 726
    :cond_1
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    if-lez v0, :cond_2

    .line 728
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    .line 730
    :cond_2
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_3

    .line 732
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 733
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V

    .line 735
    :cond_3
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 737
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 738
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 740
    :cond_4
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserAdd;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 742
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserAdd;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplAtomic(Lcom/fmm/dm/eng/parser/XDMParserAtomic;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V
    .locals 3
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserAtomic;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    .prologue
    .line 1093
    const/4 v0, 0x0

    .line 1094
    .local v0, "cmd":Lcom/fmm/dm/agent/XDMAgent;
    if-nez p2, :cond_1

    .line 1122
    :cond_0
    :goto_0
    return-void

    .line 1099
    :cond_1
    iget v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->cmdid:I

    if-lez v1, :cond_2

    .line 1101
    iget v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->cmdid:I

    iput v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->cmdid:I

    .line 1103
    :cond_2
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_3

    .line 1105
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 1106
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v2, p2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v1, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 1108
    :cond_3
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_4

    .line 1110
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 1111
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 1112
    .restart local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    :goto_1
    if-eqz v0, :cond_0

    .line 1114
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v1, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 1115
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .restart local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    goto :goto_1

    .line 1120
    :cond_4
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-result-object v1

    iput-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplCopy(Lcom/fmm/dm/eng/parser/XDMParserCopy;Lcom/fmm/dm/eng/parser/XDMParserCopy;)V
    .locals 2
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserCopy;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserCopy;

    .prologue
    .line 785
    if-nez p2, :cond_1

    .line 808
    :cond_0
    :goto_0
    return-void

    .line 790
    :cond_1
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    if-lez v0, :cond_2

    .line 792
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    .line 794
    :cond_2
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_3

    .line 796
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 797
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V

    .line 799
    :cond_3
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserCopy;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 801
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 802
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserCopy;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 804
    :cond_4
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserCopy;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 806
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserCopy;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplDelete(Lcom/fmm/dm/eng/parser/XDMParserDelete;Lcom/fmm/dm/eng/parser/XDMParserDelete;)V
    .locals 2
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserDelete;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserDelete;

    .prologue
    .line 817
    if-nez p2, :cond_1

    .line 840
    :cond_0
    :goto_0
    return-void

    .line 822
    :cond_1
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    if-lez v0, :cond_2

    .line 824
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    .line 826
    :cond_2
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_3

    .line 828
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 829
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V

    .line 831
    :cond_3
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserDelete;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 833
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 834
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserDelete;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 836
    :cond_4
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserDelete;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 838
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserDelete;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplElelist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;
    .locals 2
    .param p1, "src"    # Lcom/fmm/dm/eng/core/XDMList;

    .prologue
    .line 849
    move-object v0, p1

    .line 850
    .local v0, "curr":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v1, 0x0

    .line 852
    .local v1, "head":Lcom/fmm/dm/eng/core/XDMList;
    :goto_0
    if-eqz v0, :cond_0

    .line 854
    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0

    .line 857
    :cond_0
    return-object v1
.end method

.method public xdmAgentDataStDuplExec(Lcom/fmm/dm/eng/parser/XDMParserExec;Lcom/fmm/dm/eng/parser/XDMParserExec;)V
    .locals 2
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserExec;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserExec;

    .prologue
    .line 656
    if-nez p2, :cond_1

    .line 677
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    if-lez v0, :cond_2

    .line 662
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    .line 664
    :cond_2
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 666
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    .line 668
    :cond_3
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserExec;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 670
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 671
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserExec;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 673
    :cond_4
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserExec;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserExec;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplGet(Lcom/fmm/dm/eng/parser/XDMParserGet;Lcom/fmm/dm/eng/parser/XDMParserGet;)V
    .locals 2
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserGet;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserGet;

    .prologue
    .line 620
    if-nez p2, :cond_1

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 625
    :cond_1
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    if-lez v0, :cond_2

    .line 627
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    .line 629
    :cond_2
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->lang:I

    if-lez v0, :cond_3

    .line 631
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->lang:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserGet;->lang:I

    .line 633
    :cond_3
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_4

    .line 635
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserGet;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 636
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserGet;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V

    .line 638
    :cond_4
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_5

    .line 640
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserGet;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 641
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserGet;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 643
    :cond_5
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserGet;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserGet;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplReplace(Lcom/fmm/dm/eng/parser/XDMParserReplace;Lcom/fmm/dm/eng/parser/XDMParserReplace;)V
    .locals 2
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserReplace;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserReplace;

    .prologue
    .line 753
    if-nez p2, :cond_1

    .line 776
    :cond_0
    :goto_0
    return-void

    .line 758
    :cond_1
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    if-lez v0, :cond_2

    .line 760
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    .line 762
    :cond_2
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_3

    .line 764
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 765
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V

    .line 767
    :cond_3
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 769
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 770
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 772
    :cond_4
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserReplace;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserReplace;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplSequence(Lcom/fmm/dm/eng/parser/XDMParserSequence;Lcom/fmm/dm/eng/parser/XDMParserSequence;)V
    .locals 3
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserSequence;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserSequence;

    .prologue
    .line 1131
    const/4 v0, 0x0

    .line 1133
    .local v0, "cmd":Lcom/fmm/dm/agent/XDMAgent;
    if-nez p2, :cond_1

    .line 1161
    :cond_0
    :goto_0
    return-void

    .line 1138
    :cond_1
    iget v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSequence;->cmdid:I

    if-lez v1, :cond_2

    .line 1140
    iget v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSequence;->cmdid:I

    iput v1, p1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->cmdid:I

    .line 1142
    :cond_2
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_3

    .line 1144
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 1145
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v2, p2, Lcom/fmm/dm/eng/parser/XDMParserSequence;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v1, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 1147
    :cond_3
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_4

    .line 1149
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 1150
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 1151
    .restart local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    :goto_1
    if-eqz v0, :cond_0

    .line 1153
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v1, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 1154
    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .restart local v0    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    goto :goto_1

    .line 1159
    :cond_4
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-result-object v1

    iput-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplStatus(Lcom/fmm/dm/eng/parser/XDMParserStatus;Lcom/fmm/dm/eng/parser/XDMParserStatus;)V
    .locals 2
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .prologue
    .line 565
    if-nez p2, :cond_1

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cmdid:I

    if-lez v0, :cond_2

    .line 571
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cmdid:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cmdid:I

    .line 573
    :cond_2
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 575
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    .line 577
    :cond_3
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 579
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    .line 581
    :cond_4
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 583
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    .line 585
    :cond_5
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->targetref:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_6

    .line 587
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->targetref:Lcom/fmm/dm/eng/core/XDMList;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->targetref:Lcom/fmm/dm/eng/core/XDMList;

    .line 589
    :cond_6
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->sourceref:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_7

    .line 591
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->sourceref:Lcom/fmm/dm/eng/core/XDMList;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->sourceref:Lcom/fmm/dm/eng/core/XDMList;

    .line 593
    :cond_7
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_8

    .line 595
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 596
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V

    .line 598
    :cond_8
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_9

    .line 600
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 601
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 603
    :cond_9
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 605
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 607
    :cond_a
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/fmm/dm/eng/core/XDMList;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplSyncHeader(Lcom/fmm/dm/eng/parser/XDMParserSyncheader;Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)V
    .locals 2
    .param p1, "dest"    # Lcom/fmm/dm/eng/parser/XDMParserSyncheader;
    .param p2, "src"    # Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    .prologue
    .line 514
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szVerdtd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szVerdtd:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szVerdtd:Ljava/lang/String;

    .line 518
    :cond_0
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szVerproto:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 520
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szVerproto:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szVerproto:Ljava/lang/String;

    .line 522
    :cond_1
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szSessionId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 524
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szSessionId:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szSessionId:Ljava/lang/String;

    .line 526
    :cond_2
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->msgid:I

    if-lez v0, :cond_3

    .line 528
    iget v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->msgid:I

    iput v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->msgid:I

    .line 530
    :cond_3
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 532
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    .line 534
    :cond_4
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szSource:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 536
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szSource:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szSource:Ljava/lang/String;

    .line 538
    :cond_5
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szLocname:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 540
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szLocname:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szLocname:Ljava/lang/String;

    .line 542
    :cond_6
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 544
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    .line 546
    :cond_7
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_8

    .line 548
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 549
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/fmm/dm/eng/parser/XDMParserCred;Lcom/fmm/dm/eng/parser/XDMParserCred;)V

    .line 551
    :cond_8
    iget-object v0, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_9

    .line 553
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 554
    iget-object v0, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/fmm/dm/eng/parser/XDMParserMeta;Lcom/fmm/dm/eng/parser/XDMParserMeta;)V

    .line 556
    :cond_9
    return-void
.end method

.method public xdmAgentHdlCmdAdd(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAdd;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "addCmd"    # Lcom/fmm/dm/eng/parser/XDMParserAdd;

    .prologue
    .line 167
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 170
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 171
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserAdd;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserAdd;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_AddCmd:Lcom/fmm/dm/eng/parser/XDMParserAdd;

    .line 172
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_AddCmd:Lcom/fmm/dm/eng/parser/XDMParserAdd;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplAdd(Lcom/fmm/dm/eng/parser/XDMParserAdd;Lcom/fmm/dm/eng/parser/XDMParserAdd;)V

    .line 174
    const-string v2, "Add"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 175
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 177
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V
    .locals 3
    .param p1, "agent"    # Lcom/fmm/dm/agent/XDMAgent;
    .param p2, "curlocate"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 354
    const/4 v0, 0x0

    .line 356
    .local v0, "cmdagent":Lcom/fmm/dm/agent/XDMAgent;
    iget v1, p2, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmdagent":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 357
    .restart local v0    # "cmdagent":Lcom/fmm/dm/agent/XDMAgent;
    if-eqz v0, :cond_a

    .line 359
    iget-boolean v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_bInProgresscmd:Z

    if-eqz v1, :cond_4

    .line 361
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    if-eqz v1, :cond_1

    .line 363
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 422
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 370
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    goto :goto_0

    .line 373
    :cond_1
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    if-eqz v1, :cond_3

    .line 375
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_2

    .line 377
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    goto :goto_0

    .line 381
    :cond_2
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 382
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    goto :goto_0

    .line 387
    :cond_3
    invoke-virtual {p0, p2, p1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 393
    :cond_4
    const-string v1, "Atomic_Start"

    iget-object v2, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Sequence_Start"

    iget-object v2, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 395
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_bInProgresscmd:Z

    .line 397
    :cond_6
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    if-eqz v1, :cond_8

    .line 399
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    iget-object p2, v1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 406
    :cond_7
    :goto_1
    if-eqz p2, :cond_9

    .line 408
    invoke-virtual {p0, p2, p1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 401
    :cond_8
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    if-eqz v1, :cond_7

    .line 403
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    iget-object p2, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    goto :goto_1

    .line 412
    :cond_9
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-result-object p2

    .line 413
    invoke-virtual {p0, p2, p1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 419
    :cond_a
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-result-object p2

    .line 420
    invoke-virtual {p0, p2, p1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdAlert(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAlert;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "alert"    # Lcom/fmm/dm/eng/parser/XDMParserAlert;

    .prologue
    .line 142
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 145
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 146
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserAlert;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Alert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    .line 147
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Alert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-static {v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplAlert(Lcom/fmm/dm/eng/parser/XDMParserAlert;Lcom/fmm/dm/eng/parser/XDMParserAlert;)V

    .line 149
    const-string v2, "Alert"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 150
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 152
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdAtomicEnd(Ljava/lang/Object;)V
    .locals 4
    .param p1, "userdata"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 295
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 296
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v0, 0x0

    .line 297
    .local v0, "locateagent":Lcom/fmm/dm/agent/XDMAgent;
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 299
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdLocateSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 300
    if-eqz v0, :cond_2

    .line 302
    iget-boolean v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_bInProgresscmd:Z

    if-eqz v2, :cond_1

    .line 304
    iput-boolean v3, v0, Lcom/fmm/dm/agent/XDMAgent;->m_bInProgresscmd:Z

    .line 312
    :cond_1
    :goto_0
    return-void

    .line 309
    :cond_2
    iput-boolean v3, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    goto :goto_0
.end method

.method public xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "atomic"    # Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    .prologue
    .line 268
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 271
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 272
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    .line 273
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplAtomic(Lcom/fmm/dm/eng/parser/XDMParserAtomic;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 274
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserAtomic;-><init>()V

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    .line 275
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplAtomic(Lcom/fmm/dm/eng/parser/XDMParserAtomic;Lcom/fmm/dm/eng/parser/XDMParserAtomic;)V

    .line 277
    const-string v2, "Atomic_Start"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 278
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 280
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 287
    :goto_0
    return-void

    .line 284
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 285
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdCopy(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserCopy;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "copyCmd"    # Lcom/fmm/dm/eng/parser/XDMParserCopy;

    .prologue
    .line 217
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 220
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 221
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserCopy;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserCopy;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_CopyCmd:Lcom/fmm/dm/eng/parser/XDMParserCopy;

    .line 223
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_CopyCmd:Lcom/fmm/dm/eng/parser/XDMParserCopy;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplCopy(Lcom/fmm/dm/eng/parser/XDMParserCopy;Lcom/fmm/dm/eng/parser/XDMParserCopy;)V

    .line 225
    const-string v2, "Copy"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 226
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 228
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 234
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdDelete(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserDelete;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "deleteCmd"    # Lcom/fmm/dm/eng/parser/XDMParserDelete;

    .prologue
    .line 243
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 246
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 247
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserDelete;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserDelete;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_DeleteCmd:Lcom/fmm/dm/eng/parser/XDMParserDelete;

    .line 248
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_DeleteCmd:Lcom/fmm/dm/eng/parser/XDMParserDelete;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplDelete(Lcom/fmm/dm/eng/parser/XDMParserDelete;Lcom/fmm/dm/eng/parser/XDMParserDelete;)V

    .line 250
    const-string v2, "Delete"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 251
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 253
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 259
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdExec(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserExec;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "exec"    # Lcom/fmm/dm/eng/parser/XDMParserExec;

    .prologue
    .line 116
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 119
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 120
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserExec;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserExec;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Exec:Lcom/fmm/dm/eng/parser/XDMParserExec;

    .line 122
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Exec:Lcom/fmm/dm/eng/parser/XDMParserExec;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplExec(Lcom/fmm/dm/eng/parser/XDMParserExec;Lcom/fmm/dm/eng/parser/XDMParserExec;)V

    .line 124
    const-string v2, "Exec"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 125
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 127
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdGet(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserGet;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "get"    # Lcom/fmm/dm/eng/parser/XDMParserGet;

    .prologue
    .line 91
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 94
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 95
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserGet;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserGet;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Get:Lcom/fmm/dm/eng/parser/XDMParserGet;

    .line 96
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Get:Lcom/fmm/dm/eng/parser/XDMParserGet;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplGet(Lcom/fmm/dm/eng/parser/XDMParserGet;Lcom/fmm/dm/eng/parser/XDMParserGet;)V

    .line 98
    const-string v2, "Get"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 99
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 101
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdLocateSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V
    .locals 2
    .param p1, "agent"    # Lcom/fmm/dm/agent/XDMAgent;
    .param p2, "curlocate"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 431
    const/4 v0, 0x0

    .line 433
    .local v0, "cmdagent":Lcom/fmm/dm/agent/XDMAgent;
    iget v1, p2, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmdagent":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 434
    .restart local v0    # "cmdagent":Lcom/fmm/dm/agent/XDMAgent;
    if-eqz v0, :cond_0

    .line 436
    iget-boolean v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_bInProgresscmd:Z

    if-eqz v1, :cond_2

    .line 438
    move-object p1, v0

    .line 439
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    if-eqz v1, :cond_1

    .line 441
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdLocateSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 443
    :cond_1
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    if-eqz v1, :cond_0

    .line 445
    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdLocateSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    goto :goto_0

    .line 450
    :cond_2
    if-eqz p1, :cond_0

    .line 452
    const/4 v1, 0x0

    iput-boolean v1, p1, Lcom/fmm/dm/agent/XDMAgent;->m_bInProgresscmd:Z

    goto :goto_0
.end method

.method public xdmAgentHdlCmdReplace(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserReplace;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "replaceCmd"    # Lcom/fmm/dm/eng/parser/XDMParserReplace;

    .prologue
    .line 192
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 195
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 196
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserReplace;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserReplace;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_ReplaceCmd:Lcom/fmm/dm/eng/parser/XDMParserReplace;

    .line 197
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_ReplaceCmd:Lcom/fmm/dm/eng/parser/XDMParserReplace;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplReplace(Lcom/fmm/dm/eng/parser/XDMParserReplace;Lcom/fmm/dm/eng/parser/XDMParserReplace;)V

    .line 199
    const-string v2, "Replace"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 200
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 202
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdSequenceEnd(Ljava/lang/Object;)V
    .locals 4
    .param p1, "userdata"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 464
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 465
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v0, 0x0

    .line 467
    .local v0, "locateagent":Lcom/fmm/dm/agent/XDMAgent;
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-eqz v2, :cond_1

    .line 469
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdLocateSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 470
    if-eqz v0, :cond_2

    .line 472
    iget-boolean v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_bInProgresscmd:Z

    if-eqz v2, :cond_1

    .line 474
    iput-boolean v3, v0, Lcom/fmm/dm/agent/XDMAgent;->m_bInProgresscmd:Z

    .line 482
    :cond_1
    :goto_0
    return-void

    .line 479
    :cond_2
    iput-boolean v3, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    goto :goto_0
.end method

.method public xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserSequence;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "sequence"    # Lcom/fmm/dm/eng/parser/XDMParserSequence;

    .prologue
    .line 321
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 324
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 325
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserSequence;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    .line 326
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplSequence(Lcom/fmm/dm/eng/parser/XDMParserSequence;Lcom/fmm/dm/eng/parser/XDMParserSequence;)V

    .line 327
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    if-eqz v2, :cond_0

    .line 329
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteSequence(Ljava/lang/Object;)V

    .line 331
    :cond_0
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserSequence;-><init>()V

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    .line 332
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplSequence(Lcom/fmm/dm/eng/parser/XDMParserSequence;Lcom/fmm/dm/eng/parser/XDMParserSequence;)V

    .line 334
    const-string v2, "Sequence_Start"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 336
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_1

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_2

    .line 338
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 345
    :goto_0
    return-void

    .line 342
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    .line 343
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdStatus(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserStatus;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "status"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .prologue
    .line 64
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 67
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 68
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserStatus;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserStatus;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .line 69
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplStatus(Lcom/fmm/dm/eng/parser/XDMParserStatus;Lcom/fmm/dm/eng/parser/XDMParserStatus;)V

    .line 71
    const-string v2, "Status"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 73
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 75
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 82
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdSyncHdr(Ljava/lang/Object;Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "header"    # Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    .prologue
    .line 39
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 42
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 43
    .local v0, "agent":Lcom/fmm/dm/agent/XDMAgent;
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;-><init>()V

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Header:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    .line 44
    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Header:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    invoke-virtual {p0, v2, p2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplSyncHeader(Lcom/fmm/dm/eng/parser/XDMParserSyncheader;Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)V

    .line 46
    const-string v2, "SyncHdr"

    iput-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 47
    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 49
    :cond_0
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/fmm/dm/agent/XDMAgent;Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V
    .locals 3
    .param p1, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 491
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMLinkedList;->top:Lcom/fmm/dm/eng/core/XDMNode;

    .line 492
    .local v1, "top":Lcom/fmm/dm/eng/core/XDMNode;
    new-instance v0, Lcom/fmm/dm/eng/core/XDMNode;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMNode;-><init>()V

    .line 494
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMNode;
    invoke-static {v0, p2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListBindObjectToNode(Lcom/fmm/dm/eng/core/XDMNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    if-eqz v0, :cond_0

    .line 498
    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 499
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 500
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    iput-object v0, v2, Lcom/fmm/dm/eng/core/XDMNode;->next:Lcom/fmm/dm/eng/core/XDMNode;

    .line 501
    iput-object v0, v1, Lcom/fmm/dm/eng/core/XDMNode;->previous:Lcom/fmm/dm/eng/core/XDMNode;

    .line 503
    iget v2, p1, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Lcom/fmm/dm/eng/core/XDMLinkedList;->count:I

    .line 505
    :cond_0
    return-void
.end method
