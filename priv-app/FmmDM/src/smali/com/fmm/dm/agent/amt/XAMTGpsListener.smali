.class public abstract Lcom/fmm/dm/agent/amt/XAMTGpsListener;
.super Ljava/lang/Object;
.source "XAMTGpsListener.java"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field public mLocation:Landroid/location/Location;

.field public mTask:Ljava/util/TimerTask;

.field public mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mLocation:Landroid/location/Location;

    return-void
.end method


# virtual methods
.method public endTimer()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTimer:Ljava/util/Timer;

    .line 42
    :cond_0
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 1
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mLocation:Landroid/location/Location;

    .line 48
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTimer:Ljava/util/Timer;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mLocation:Landroid/location/Location;

    invoke-virtual {p0, v0}, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->onReceiveData(Landroid/location/Location;)V

    .line 54
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 69
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 64
    return-void
.end method

.method public abstract onReceiveData(Landroid/location/Location;)V
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 59
    return-void
.end method

.method public abstract onTimeout()V
.end method

.method public startTimer(J)V
    .locals 3
    .param p1, "ms"    # J

    .prologue
    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mLocation:Landroid/location/Location;

    .line 23
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTimer:Ljava/util/Timer;

    .line 24
    new-instance v0, Lcom/fmm/dm/agent/amt/XAMTGpsListener$1;

    invoke-direct {v0, p0}, Lcom/fmm/dm/agent/amt/XAMTGpsListener$1;-><init>(Lcom/fmm/dm/agent/amt/XAMTGpsListener;)V

    iput-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTask:Ljava/util/TimerTask;

    .line 32
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->mTask:Ljava/util/TimerTask;

    invoke-virtual {v0, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 33
    return-void
.end method
