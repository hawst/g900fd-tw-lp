.class public Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;
.super Lcom/fmm/dm/agent/XDMAgent;
.source "XLAWMOAgent.java"


# static fields
.field public static mCategory:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 22
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->mCategory:I

    .line 23
    return-void
.end method

.method public static xlawmoAgentDecodeUtf8Message(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "szMsg"    # Ljava/lang/String;

    .prologue
    .line 1088
    :try_start_0
    new-instance v1, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1096
    .local v1, "szRet":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1090
    .end local v1    # "szRet":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1092
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "UTF-8 Decode Error."

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1093
    const-string v1, ""

    .restart local v1    # "szRet":Ljava/lang/String;
    goto :goto_0
.end method

.method public static xlawmoAgentDefaultMakeNode()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 69
    const/4 v1, 0x0

    .line 71
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/4 v0, 0x0

    .line 75
    .local v0, "nAclValue":I
    const-string v4, "xlawmoAgentDefaultMakeNode"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 77
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xdmAgentGetOM()Lcom/fmm/dm/eng/core/XDMOmTree;

    move-result-object v1

    .line 78
    if-nez v1, :cond_0

    .line 80
    const-string v4, "OM Addr is NULL"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 188
    :goto_0
    return-void

    .line 85
    :cond_0
    const-string v3, "./LAWMO"

    .line 86
    .local v3, "szLawmoRoot":Ljava/lang/String;
    const/16 v0, 0x8

    .line 87
    const/4 v4, 0x1

    invoke-static {v1, v3, v0, v4}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 90
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/State"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, "szLawmoNode":Ljava/lang/String;
    const/16 v0, 0x8

    .line 92
    sget-object v4, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v4, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 95
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AvailableWipeList"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 96
    const/16 v0, 0x8

    .line 97
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 100
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AvailableWipeList"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/InternalMem"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 101
    const/16 v0, 0x8

    .line 102
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 105
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AvailableWipeList"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/InternalMem"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ListItemName"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106
    const/16 v0, 0x8

    .line 107
    const-string v4, "InternalMem"

    invoke-static {v1, v2, v4, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 110
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AvailableWipeList"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/InternalMem"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ToBeWiped"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 111
    const/16 v0, 0x18

    .line 112
    const-string v4, "true"

    invoke-static {v1, v2, v4, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 115
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AvailableWipeList"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ExternalMem"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 116
    const/16 v0, 0x8

    .line 117
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 120
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AvailableWipeList"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ExternalMem"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ListItemName"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 121
    const/16 v0, 0x8

    .line 122
    const-string v4, "ExternalMem"

    invoke-static {v1, v2, v4, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 125
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AvailableWipeList"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ExternalMem"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ToBeWiped"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 126
    const/16 v0, 0x18

    .line 127
    const-string v4, "false"

    invoke-static {v1, v2, v4, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 130
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/LAWMOConfig"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 131
    const/16 v0, 0x8

    .line 132
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 135
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/LAWMOConfig"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/NotifyUser"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 136
    const/16 v0, 0x18

    .line 137
    const-string v4, "false"

    invoke-static {v1, v2, v4, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 140
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Operations"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 141
    const/16 v0, 0x8

    .line 142
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 145
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Operations"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/FullyLock"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 146
    const/16 v0, 0xc

    .line 147
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 150
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Operations"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/PartiallyLock"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 151
    const/16 v0, 0xc

    .line 152
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 155
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Operations"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/UnLock"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 156
    const/16 v0, 0xc

    .line 157
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 160
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Operations"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Wipe"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 161
    const/16 v0, 0xc

    .line 162
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 165
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Operations"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/FactoryReset"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 166
    const/16 v0, 0xc

    .line 167
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 170
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Ext"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 171
    const/16 v0, 0x8

    .line 172
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 175
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Ext"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Operations"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 176
    const/16 v0, 0x8

    .line 177
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 180
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Ext"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Reset"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 181
    const/16 v0, 0xc

    .line 182
    invoke-static {v1, v2, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 185
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Ext"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Pin"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 186
    const/16 v0, 0x10

    .line 187
    const-string v4, "00000000"

    invoke-static {v1, v2, v4, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0
.end method

.method public static xlawmoAgentEmergencyModeRequestorPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1052
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1058
    :cond_0
    :goto_0
    return v0

    .line 1054
    :cond_1
    const-string v1, "./Ext/OSPS/EmergencyMode/RequestorName"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1056
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentEmergencyModeStatePath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1041
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1047
    :cond_0
    :goto_0
    return v0

    .line 1043
    :cond_1
    const-string v1, "./Ext/OSPS/EmergencyMode/State"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1045
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentExecuteOperation()V
    .locals 3

    .prologue
    .line 743
    const/4 v0, 0x0

    .line 745
    .local v0, "nOperation":I
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 746
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v0

    .line 747
    packed-switch v0, :pswitch_data_0

    .line 826
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not support operation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 829
    :cond_0
    :goto_0
    return-void

    .line 750
    :pswitch_0
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 751
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 755
    :pswitch_1
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 756
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 760
    :pswitch_2
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 761
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 765
    :pswitch_3
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 766
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 770
    :pswitch_4
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 771
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 775
    :pswitch_5
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 776
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 780
    :pswitch_6
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 781
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 785
    :pswitch_7
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 786
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 790
    :pswitch_8
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 791
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 795
    :pswitch_9
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 796
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 801
    :pswitch_a
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 802
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 806
    :pswitch_b
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 807
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 811
    :pswitch_c
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 812
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 816
    :pswitch_d
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 817
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 821
    :pswitch_e
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 822
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hLawmoHandler:Landroid/os/Handler;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 747
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V
    .locals 2
    .param p0, "bAMTCheck"    # Z
    .param p1, "szResultCode"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 834
    if-eqz p0, :cond_0

    .line 836
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetResultCode(Ljava/lang/String;)V

    .line 843
    :goto_0
    const/16 v0, 0xb

    invoke-static {v0, v1, v1}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 844
    return-void

    .line 840
    :cond_0
    const-string v0, "1452"

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetResultCode(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xlawmoAgentExecuteResultReportTest()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 849
    const/16 v0, 0xb

    invoke-static {v0, v1, v1}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 850
    return-void
.end method

.method public static xlawmoAgentForwardingPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 953
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 959
    :cond_0
    :goto_0
    return v0

    .line 955
    :cond_1
    const-string v1, "./Ext/OSPS/Forwarding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 957
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentGetOperationCategory()I
    .locals 1

    .prologue
    .line 1079
    sget v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->mCategory:I

    return v0
.end method

.method public static xlawmoAgentIsCallRestrictionPhoneNumberPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 887
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 893
    :cond_0
    :goto_0
    return v0

    .line 889
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/PhoneNumber"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 891
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentIsExternalToBeWipePath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 931
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 937
    :cond_0
    :goto_0
    return v0

    .line 933
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/External/ToBeWiped"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 935
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentIsLawmoExecNode(Ljava/lang/String;)Z
    .locals 3
    .param p0, "szNodePath"    # Ljava/lang/String;

    .prologue
    .line 636
    const-string v2, ""

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 637
    const/4 v0, 0x0

    .line 638
    .local v0, "bRtn":Z
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    .line 663
    .end local v0    # "bRtn":Z
    .local v1, "bRtn":I
    :goto_0
    return v1

    .line 643
    .end local v1    # "bRtn":I
    .restart local v0    # "bRtn":Z
    :cond_0
    const-string v2, "/LAWMO"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "./Ext/OSPS/Forwarding"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "./Ext/OSPS/MasterKey"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "./Ext/OSPS/ConnectionCheck"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "./Ext/OSPS/EmergencyMode"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "./Ext/OSPS/ReactivationLock"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move v1, v0

    .line 645
    .restart local v1    # "bRtn":I
    goto :goto_0

    .line 649
    .end local v1    # "bRtn":I
    :cond_1
    const-string v2, "/PartiallyLock"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "/UnLock"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "/Wipe"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "/LockMyPhone/Operations/ChangeMsg"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "/CallRestriction/Operations/Restriction"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "/CallRestriction/Operations/Stop"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "./Ext/OSPS/Forwarding/Operations/DivertRequest"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "./Ext/OSPS/EmergencyMode"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "./Ext/OSPS/ReactivationLock/Operations/DivertRequest"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 652
    :cond_2
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetResultReportType(I)V

    .line 653
    invoke-static {p0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentVerifyOperation(Ljava/lang/String;)V

    .line 654
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 663
    .restart local v1    # "bRtn":I
    goto/16 :goto_0

    .line 659
    .end local v1    # "bRtn":I
    :cond_3
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetResultReportType(I)V

    .line 660
    invoke-static {p0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentVerifyOperation(Ljava/lang/String;)V

    .line 661
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static xlawmoAgentIsLawmoPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 854
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 860
    :cond_0
    :goto_0
    return v0

    .line 856
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 858
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentIsLockMyPhoneMessagesPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 865
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 871
    :cond_0
    :goto_0
    return v0

    .line 867
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone/Messages"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 869
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentIsLockMyPhoneReactivationLockPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 876
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 882
    :cond_0
    :goto_0
    return v0

    .line 878
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP/Ext/ReactivationLock"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 880
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentIsPasswordPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 898
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 904
    :cond_0
    :goto_0
    return v0

    .line 900
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP/Ext/Password"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 902
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentIsRingMyPhoneMessagesPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 909
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 915
    :cond_0
    :goto_0
    return v0

    .line 911
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Messages"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 913
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentIsSIMToBeWipePath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 942
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 948
    :cond_0
    :goto_0
    return v0

    .line 944
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/SIM/ToBeWiped"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 946
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentIsWipeListPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 920
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 926
    :cond_0
    :goto_0
    return v0

    .line 922
    :cond_1
    const-string v1, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 924
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentNumbersCallForwardingbPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 964
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 970
    :cond_0
    :goto_0
    return v0

    .line 966
    :cond_1
    const-string v1, "./Ext/OSPS/Forwarding/Numbers/CallForwarding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 968
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentNumbersSMSForwardingbPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 975
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 981
    :cond_0
    :goto_0
    return v0

    .line 977
    :cond_1
    const-string v1, "./Ext/OSPS/Forwarding/Numbers/SMSForwarding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 979
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static xlawmoAgentOSPSMakeNode()V
    .locals 26

    .prologue
    .line 201
    const/4 v5, 0x0

    .line 203
    .local v5, "nAclValue":I
    const-string v23, ""

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 205
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xdmAgentGetOM()Lcom/fmm/dm/eng/core/XDMOmTree;

    move-result-object v8

    .line 206
    .local v8, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    if-nez v8, :cond_0

    .line 208
    const-string v23, "OM Addr is NULL"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 551
    :goto_0
    return-void

    .line 213
    :cond_0
    const/16 v5, 0x8

    .line 214
    const-string v23, "./Ext"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 217
    const/16 v5, 0x8

    .line 218
    const-string v23, "./Ext/OSPS"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 221
    const/16 v5, 0x8

    .line 222
    const-string v23, "./Ext/OSPS/LAWMO"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 225
    const/16 v5, 0x8

    .line 226
    const-string v23, "./Ext/OSPS/LAWMO/OSP"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 229
    const/16 v5, 0x8

    .line 230
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetState()I

    move-result v7

    .line 232
    .local v7, "nState":I
    const-string v23, "./Ext/OSPS/LAWMO/OSP/State"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 235
    const/16 v5, 0x8

    .line 236
    const-string v23, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 239
    const/16 v5, 0x8

    .line 240
    const-string v23, "./Ext/OSPS/LAWMO/OSP/LAWMOConfig"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 243
    const/16 v5, 0x18

    .line 244
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetNotifyUser()Z

    move-result v3

    .line 245
    .local v3, "bNotifyUser":Z
    const-string v23, "./Ext/OSPS/LAWMO/OSP/LAWMOConfig/NotifyUser"

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 248
    const/16 v5, 0x8

    .line 249
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Operations"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 252
    const/4 v5, 0x4

    .line 253
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Operations/FullyLock"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 256
    const/4 v5, 0x4

    .line 257
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Operations/PartiallyLock"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 260
    const/4 v5, 0x4

    .line 261
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Operations/UnLock"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 264
    const/4 v5, 0x4

    .line 265
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Operations/FactoryReset"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 268
    const/16 v5, 0xc

    .line 269
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Operations/Wipe"

    const/16 v24, 0xc

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v2}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 272
    const/16 v5, 0x8

    .line 273
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 276
    const/16 v5, 0x18

    .line 277
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/Password"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 280
    const/16 v5, 0x8

    .line 281
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/AMT"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 284
    const/16 v5, 0x18

    .line 286
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v23

    if-eqz v23, :cond_1

    const/4 v6, 0x1

    .line 287
    .local v6, "nAmtStatus":I
    :goto_1
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/AMT/Status"

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 290
    const/16 v5, 0x8

    .line 291
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtPassword()Ljava/lang/String;

    move-result-object v16

    .line 292
    .local v16, "szPassword":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    move/from16 v2, v24

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 295
    const/16 v5, 0x18

    .line 296
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtLockMyPhoneMessages()Ljava/lang/String;

    move-result-object v15

    .line 297
    .local v15, "szLockMyPhoneMessages":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone/Messages"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v15, v5, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 300
    const/16 v5, 0x8

    .line 301
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone/Operations"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 304
    const/4 v5, 0x4

    .line 305
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone/Operations/ChangeMsg"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 308
    const/16 v5, 0x8

    .line 309
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 312
    const/16 v5, 0x18

    .line 313
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtCallRestrictionPhoneNumber()Ljava/lang/String;

    move-result-object v12

    .line 314
    .local v12, "szCallRestrictionPhoneNumber":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/PhoneNumber"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v12, v5, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 317
    const/16 v5, 0x8

    .line 318
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/Operations"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 321
    const/4 v5, 0x4

    .line 322
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/Operations/Restriction"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 325
    const/4 v5, 0x4

    .line 326
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/Operations/Stop"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 329
    const/16 v5, 0x8

    .line 330
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtCallRestrictionStatus()Ljava/lang/String;

    move-result-object v13

    .line 331
    .local v13, "szCallRestrictionStatus":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/Status"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v13, v5, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 334
    const/16 v5, 0x8

    .line 335
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 338
    const/16 v5, 0x8

    .line 339
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Operations"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 342
    const/4 v5, 0x4

    .line 343
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Operations/Start"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 346
    const/4 v5, 0x4

    .line 347
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Operations/Stop"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 350
    const/16 v5, 0x8

    .line 351
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtRingMyPhoneStatus()Ljava/lang/String;

    move-result-object v18

    .line 352
    .local v18, "szRingMyPhoneStatus":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Status"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    move/from16 v2, v24

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 355
    const/16 v5, 0x18

    .line 356
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtRingMyPhoneMessages()Ljava/lang/String;

    move-result-object v17

    .line 357
    .local v17, "szRingMyPhoneMessages":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Messages"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    move/from16 v2, v24

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 362
    const/16 v5, 0x8

    .line 363
    const-string v23, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/External"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 366
    const/16 v5, 0x8

    .line 367
    const-string v23, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/External/ListItemName"

    const-string v24, " "

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 370
    const/16 v5, 0x18

    .line 371
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExternalToBeWiped()Z

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v14

    .line 372
    .local v14, "szExternalWiped":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/External/ToBeWiped"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v14, v5, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 375
    const/16 v5, 0x8

    .line 376
    const-string v23, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/SIM"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 379
    const/16 v5, 0x8

    .line 380
    const-string v23, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/SIM/ListItemName"

    const-string v24, " "

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 383
    const/16 v5, 0x18

    .line 384
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSIMToBeWiped()Z

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v19

    .line 385
    .local v19, "szSIMWiped":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/SIM/ToBeWiped"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    move/from16 v2, v24

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 388
    const/16 v5, 0x8

    .line 389
    const-string v23, "./Ext/OSPS/Forwarding"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 392
    const/16 v5, 0x8

    .line 393
    const-string v23, "./Ext/OSPS/Forwarding/Numbers"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 396
    const/16 v5, 0x18

    .line 397
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetCallForwardingNumbers()Ljava/lang/String;

    move-result-object v11

    .line 398
    .local v11, "szCallForwardingNumbers":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/Forwarding/Numbers/CallForwarding"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v11, v5, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 401
    const/16 v5, 0x18

    .line 402
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSMSForwardingNumbers()Ljava/lang/String;

    move-result-object v22

    .line 403
    .local v22, "szSMSForwardingNumbers":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/Forwarding/Numbers/SMSForwarding"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move/from16 v2, v24

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 406
    const/16 v5, 0x8

    .line 407
    const-string v23, "./Ext/OSPS/Forwarding/ToBeEnabled"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 410
    const/16 v5, 0x18

    .line 411
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetCallForwardingToBeEnabled()Z

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    .line 412
    .local v10, "szCallForwardingEnabled":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/Forwarding/ToBeEnabled/CallForwarding"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v10, v5, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 415
    const/16 v5, 0x18

    .line 416
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSMSForwardingToBeEnabled()Z

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v21

    .line 417
    .local v21, "szSMSForwardingEnabled":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/Forwarding/ToBeEnabled/SMSForwarding"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    move/from16 v2, v24

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 420
    const/16 v5, 0x8

    .line 421
    const-string v23, "./Ext/OSPS/Forwarding/ToBeDisabled"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 424
    const/16 v5, 0x18

    .line 425
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetCallForwardingToBeDisabled()Z

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    .line 426
    .local v9, "szCallForwardingDisabled":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/Forwarding/ToBeDisabled/CallForwarding"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v9, v5, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 429
    const/16 v5, 0x18

    .line 430
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSMSForwardingToBeDisabled()Z

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v20

    .line 431
    .local v20, "szSMSForwardingDisabled":Ljava/lang/String;
    const-string v23, "./Ext/OSPS/Forwarding/ToBeDisabled/SMSForwarding"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    move/from16 v2, v24

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 434
    const/16 v5, 0x8

    .line 435
    const-string v23, "./Ext/OSPS/Forwarding/Operations"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 438
    const/4 v5, 0x4

    .line 439
    const-string v23, "./Ext/OSPS/Forwarding/Operations/DivertRequest"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 442
    const/16 v5, 0x8

    .line 443
    const-string v23, "./Ext/OSPS/MasterKey"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 446
    const/16 v5, 0x8

    .line 447
    const-string v23, "./Ext/OSPS/MasterKey/Operations"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 450
    const/4 v5, 0x4

    .line 451
    const-string v23, "./Ext/OSPS/MasterKey/Operations/LockRelease"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 457
    const-string v23, "./Ext/OSPS/ConnectionCheck"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 459
    const-string v23, "./Ext/OSPS/ConnectionCheck/Battery"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 461
    const-string v23, "./Ext/OSPS/ConnectionCheck/CallCount"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 463
    const-string v23, "./Ext/OSPS/ConnectionCheck/MsgCount"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 465
    const-string v23, "./Ext/OSPS/ConnectionCheck/Operations"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 467
    const-string v23, "./Ext/OSPS/ConnectionCheck/Policy"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 469
    const-string v23, "./Ext/OSPS/ConnectionCheck/ReactivationLock"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 472
    const/16 v5, 0x8

    .line 473
    const-string v23, "./Ext/OSPS/ConnectionCheck/Battery/Level"

    const-string v24, ""

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 476
    const/16 v5, 0x8

    .line 477
    const-string v23, "./Ext/OSPS/ConnectionCheck/CallCount/IncomingCall"

    const-string v24, ""

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 480
    const/16 v5, 0x8

    .line 481
    const-string v23, "./Ext/OSPS/ConnectionCheck/CallCount/OutgoingCall"

    const-string v24, ""

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 484
    const/16 v5, 0x8

    .line 485
    const-string v23, "./Ext/OSPS/ConnectionCheck/CallCount/MissedCall"

    const-string v24, ""

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 499
    const/16 v5, 0x10

    .line 500
    const-string v23, "./Ext/OSPS/ConnectionCheck/Policy/Period"

    const-string v24, ""

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 503
    const/4 v5, 0x4

    .line 504
    const-string v23, "./Ext/OSPS/ConnectionCheck/Operations/Activity"

    const/16 v24, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 507
    const/16 v5, 0x8

    .line 508
    const-string v23, "./Ext/OSPS/ConnectionCheck/ReactivationLock/State"

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetReactivationLockState()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 511
    const/16 v5, 0x8

    .line 512
    const-string v23, "./Ext/OSPS/EmergencyMode"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 515
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetEmergencyModeState()Z

    move-result v4

    .line 516
    .local v4, "bState":Z
    const/16 v5, 0x18

    .line 517
    const-string v23, "./Ext/OSPS/EmergencyMode/State"

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 520
    const/16 v5, 0x18

    .line 521
    const-string v23, "./Ext/OSPS/EmergencyMode/Date"

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetEmergencyModeDate()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 524
    const/16 v5, 0x18

    .line 525
    const-string v23, "./Ext/OSPS/EmergencyMode/RequestorName"

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetEmergencyModeDate()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 528
    const/16 v5, 0x8

    .line 529
    const-string v23, "./Ext/OSPS/EmergencyMode/Operations"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 532
    const/4 v5, 0x4

    .line 533
    const-string v23, "./Ext/OSPS/EmergencyMode/Operations/DivertRequest"

    const-string v24, ""

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 536
    const/16 v5, 0x8

    .line 537
    const-string v23, "./Ext/OSPS/ReactivationLock"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 540
    const/16 v5, 0x18

    .line 541
    const-string v23, "./Ext/OSPS/ReactivationLock/State"

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetReactivationLockState()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 544
    const/16 v5, 0x8

    .line 545
    const-string v23, "./Ext/OSPS/ReactivationLock/Operations"

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v8, v0, v5, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 548
    const/4 v5, 0x4

    .line 549
    const-string v23, "./Ext/OSPS/ReactivationLock/Operations/DivertRequest"

    const-string v24, ""

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v5, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 286
    .end local v4    # "bState":Z
    .end local v6    # "nAmtStatus":I
    .end local v9    # "szCallForwardingDisabled":Ljava/lang/String;
    .end local v10    # "szCallForwardingEnabled":Ljava/lang/String;
    .end local v11    # "szCallForwardingNumbers":Ljava/lang/String;
    .end local v12    # "szCallRestrictionPhoneNumber":Ljava/lang/String;
    .end local v13    # "szCallRestrictionStatus":Ljava/lang/String;
    .end local v14    # "szExternalWiped":Ljava/lang/String;
    .end local v15    # "szLockMyPhoneMessages":Ljava/lang/String;
    .end local v16    # "szPassword":Ljava/lang/String;
    .end local v17    # "szRingMyPhoneMessages":Ljava/lang/String;
    .end local v18    # "szRingMyPhoneStatus":Ljava/lang/String;
    .end local v19    # "szSIMWiped":Ljava/lang/String;
    .end local v20    # "szSMSForwardingDisabled":Ljava/lang/String;
    .end local v21    # "szSMSForwardingEnabled":Ljava/lang/String;
    .end local v22    # "szSMSForwardingNumbers":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    goto/16 :goto_1
.end method

.method public static xlawmoAgentPolicyPeriodPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1030
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1036
    :cond_0
    :goto_0
    return v0

    .line 1032
    :cond_1
    const-string v1, "./Ext/OSPS/ConnectionCheck/Policy/Period"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1034
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentReactivationLockStatePath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1063
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1069
    :cond_0
    :goto_0
    return v0

    .line 1065
    :cond_1
    const-string v1, "./Ext/OSPS/ReactivationLock/State"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1067
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentSetOperationCategory(I)V
    .locals 1
    .param p0, "nCategory"    # I

    .prologue
    .line 1074
    sget v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->mCategory:I

    or-int/2addr v0, p0

    sput v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->mCategory:I

    .line 1075
    return-void
.end method

.method public static xlawmoAgentToBeDisabledCallForwardingbPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1008
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1014
    :cond_0
    :goto_0
    return v0

    .line 1010
    :cond_1
    const-string v1, "./Ext/OSPS/Forwarding/ToBeDisabled/CallForwarding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1012
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentToBeDisabledSMSForwardingbPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1019
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1025
    :cond_0
    :goto_0
    return v0

    .line 1021
    :cond_1
    const-string v1, "./Ext/OSPS/Forwarding/ToBeDisabled/SMSForwarding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1023
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentToBeEnabledCallForwardingbPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 986
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 992
    :cond_0
    :goto_0
    return v0

    .line 988
    :cond_1
    const-string v1, "./Ext/OSPS/Forwarding/ToBeEnabled/CallForwarding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 990
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentToBeEnabledSMSForwardingbPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 997
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1003
    :cond_0
    :goto_0
    return v0

    .line 999
    :cond_1
    const-string v1, "./Ext/OSPS/Forwarding/ToBeEnabled/SMSForwarding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1001
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xlawmoAgentVerifyOperation(Ljava/lang/String;)V
    .locals 3
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    .line 668
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "szPath : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 669
    const/4 v0, 0x0

    .line 670
    .local v0, "nOperation":I
    const-string v1, "/FullyLock"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 672
    const/4 v0, 0x1

    .line 738
    :goto_0
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 739
    :goto_1
    return-void

    .line 674
    :cond_0
    const-string v1, "/PartiallyLock"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 676
    const/4 v0, 0x2

    goto :goto_0

    .line 678
    :cond_1
    const-string v1, "/UnLock"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 680
    const/4 v0, 0x3

    goto :goto_0

    .line 682
    :cond_2
    const-string v1, "/Wipe"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 684
    const/4 v0, 0x4

    goto :goto_0

    .line 686
    :cond_3
    const-string v1, "/FactoryReset"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 688
    const/4 v0, 0x5

    goto :goto_0

    .line 690
    :cond_4
    const-string v1, "/Reset"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 692
    const/4 v0, 0x6

    goto :goto_0

    .line 694
    :cond_5
    const-string v1, "/LockMyPhone/Operations/ChangeMsg"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 696
    const/4 v0, 0x7

    goto :goto_0

    .line 698
    :cond_6
    const-string v1, "/CallRestriction/Operations/Restriction"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 700
    const/16 v0, 0x8

    goto :goto_0

    .line 702
    :cond_7
    const-string v1, "/CallRestriction/Operations/Stop"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 704
    const/16 v0, 0x9

    goto :goto_0

    .line 706
    :cond_8
    const-string v1, "/RingMyPhone/Operations/Start"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 708
    const/16 v0, 0xa

    goto :goto_0

    .line 711
    :cond_9
    const-string v1, "./Ext/OSPS/Forwarding/Operations/DivertRequest"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 713
    const/16 v0, 0xb

    goto :goto_0

    .line 715
    :cond_a
    const-string v1, "./Ext/OSPS/MasterKey/Operations/LockRelease"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 717
    const/16 v0, 0xc

    goto :goto_0

    .line 720
    :cond_b
    const-string v1, "./Ext/OSPS/ConnectionCheck/Operations/Activity"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 722
    const/16 v0, 0xd

    goto/16 :goto_0

    .line 724
    :cond_c
    const-string v1, "./Ext/OSPS/EmergencyMode"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 726
    const/16 v0, 0xe

    goto/16 :goto_0

    .line 728
    :cond_d
    const-string v1, "./Ext/OSPS/ReactivationLock/Operations/DivertRequest"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 730
    const/16 v0, 0xf

    goto/16 :goto_0

    .line 734
    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wsLawmoAgentVerifyOperation Not support operation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private static xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V
    .locals 6
    .param p0, "Om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "nAclValue"    # I
    .param p3, "nScope"    # I

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 36
    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 37
    invoke-static {p0, p1, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 39
    :cond_0
    return-void
.end method


# virtual methods
.method public xlawmoAgentMakeNode()V
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentOSPSMakeNode()V

    .line 51
    sget-boolean v1, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    if-eqz v1, :cond_0

    .line 53
    new-instance v0, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;

    invoke-direct {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;-><init>()V

    .line 54
    .local v0, "lawmoSimChange":Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;
    invoke-virtual {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xlawmoSimChangeMakeNode()V

    .line 58
    .end local v0    # "lawmoSimChange":Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;
    :cond_0
    return-void
.end method

.method public xlawmoAgentOSPSReMakeNode()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 556
    const/4 v0, 0x0

    .line 558
    .local v0, "nAclValue":I
    const-string v4, ""

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 560
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xdmAgentGetOM()Lcom/fmm/dm/eng/core/XDMOmTree;

    move-result-object v2

    .line 561
    .local v2, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    if-nez v2, :cond_0

    .line 563
    const-string v4, "OM Addr is NULL"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 632
    :goto_0
    return-void

    .line 570
    :cond_0
    const/16 v0, 0x8

    .line 571
    const-string v3, "CHECKFAIL"

    .line 572
    .local v3, "szCount":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetBatteryLevel()I

    move-result v1

    .line 573
    .local v1, "nCount":I
    if-ltz v1, :cond_1

    .line 574
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 575
    :cond_1
    const-string v4, "./Ext/OSPS/ConnectionCheck/Battery/Level"

    invoke-static {v2, v4, v3, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 578
    const/16 v0, 0x8

    .line 579
    const-string v3, "CHECKFAIL"

    .line 580
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetIncomingCallCount()I

    move-result v1

    .line 581
    if-ltz v1, :cond_2

    .line 582
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 583
    :cond_2
    const-string v4, "./Ext/OSPS/ConnectionCheck/CallCount/IncomingCall"

    invoke-static {v2, v4, v3, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 586
    const/16 v0, 0x8

    .line 587
    const-string v3, "CHECKFAIL"

    .line 588
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetOutgoingCallCount()I

    move-result v1

    .line 589
    if-ltz v1, :cond_3

    .line 590
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 591
    :cond_3
    const-string v4, "./Ext/OSPS/ConnectionCheck/CallCount/OutgoingCall"

    invoke-static {v2, v4, v3, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 594
    const/16 v0, 0x8

    .line 595
    const-string v3, "CHECKFAIL"

    .line 596
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetMissedCallCount()I

    move-result v1

    .line 597
    if-ltz v1, :cond_4

    .line 598
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 599
    :cond_4
    const-string v4, "./Ext/OSPS/ConnectionCheck/CallCount/MissedCall"

    invoke-static {v2, v4, v3, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 621
    const/16 v0, 0x10

    .line 622
    const-string v4, "./Ext/OSPS/ConnectionCheck/Policy/Period"

    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPolicyPeriod()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 625
    const/4 v0, 0x4

    .line 626
    const-string v4, "./Ext/OSPS/ConnectionCheck/Operations/Activity"

    invoke-static {v2, v4, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 629
    const/16 v0, 0x8

    .line 630
    const-string v4, "./Ext/OSPS/ConnectionCheck/ReactivationLock/State"

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetReactivationLockState()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5, v0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0
.end method
