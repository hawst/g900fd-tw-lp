.class public Lcom/fmm/dm/agent/XDMDebug;
.super Ljava/lang/Object;
.source "XDMDebug.java"


# static fields
.field private static final DEBUG_EXCEPTION_LOG_TAG:Ljava/lang/String; = "DBG_FMMDM"

.field private static DEBUG_LOG_ON_:Z = false

.field private static final DEBUG_LOG_TAG:Ljava/lang/String; = "DBG_FMMDM"

.field private static DEBUG_PRIVATE_LOG_ON:Z

.field private static DEBUG_WBXML_DUMP:Z

.field private static DEBUG_WBXML_FILE:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 19
    sput-boolean v1, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_LOG_ON_:Z

    .line 20
    sput-boolean v1, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_PRIVATE_LOG_ON:Z

    .line 22
    sput-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_DUMP:Z

    .line 23
    sput-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_FILE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static XDM_DEBUG(Ljava/lang/String;)V
    .locals 6
    .param p0, "szContent"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 30
    sget-boolean v4, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_LOG_ON_:Z

    if-eqz v4, :cond_0

    .line 32
    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    .line 33
    .local v3, "trace":[Ljava/lang/StackTraceElement;
    const-string v2, ""

    .line 34
    .local v2, "szMsg":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 36
    .local v1, "strBuffer":Ljava/lang/StringBuffer;
    array-length v4, v3

    if-lt v4, v5, :cond_1

    .line 38
    aget-object v0, v3, v5

    .line 39
    .local v0, "elt":Ljava/lang/StackTraceElement;
    const-string v4, "["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 40
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetReleaseVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 41
    const-string v4, "]["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    const-string v4, "Line:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 43
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 44
    const-string v4, "]["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 45
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 46
    const-string v4, "] "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 47
    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 48
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 49
    const-string v4, "DBG_FMMDM"

    invoke-static {v4, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    .end local v0    # "elt":Ljava/lang/StackTraceElement;
    .end local v1    # "strBuffer":Ljava/lang/StringBuffer;
    .end local v2    # "szMsg":Ljava/lang/String;
    .end local v3    # "trace":[Ljava/lang/StackTraceElement;
    :cond_0
    :goto_0
    return-void

    .line 53
    .restart local v1    # "strBuffer":Ljava/lang/StringBuffer;
    .restart local v2    # "szMsg":Ljava/lang/String;
    .restart local v3    # "trace":[Ljava/lang/StackTraceElement;
    :cond_1
    const-string v4, "DBG_FMMDM"

    invoke-static {v4, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    .locals 6
    .param p0, "szContent"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 60
    sget-boolean v4, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_LOG_ON_:Z

    if-eqz v4, :cond_0

    .line 62
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 63
    .local v1, "strBuffer":Ljava/lang/StringBuffer;
    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    .line 64
    .local v3, "trace":[Ljava/lang/StackTraceElement;
    const-string v2, ""

    .line 66
    .local v2, "szMsg":Ljava/lang/String;
    array-length v4, v3

    if-lt v4, v5, :cond_1

    .line 68
    aget-object v0, v3, v5

    .line 70
    .local v0, "elt":Ljava/lang/StackTraceElement;
    const-string v4, "Warning!!! ["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 71
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetReleaseVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 72
    const-string v4, "]["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    const-string v4, "Line:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 74
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 75
    const-string v4, "]["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 76
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    const-string v4, "] "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 78
    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 79
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 81
    const-string v4, "DBG_FMMDM"

    invoke-static {v4, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    .end local v0    # "elt":Ljava/lang/StackTraceElement;
    .end local v1    # "strBuffer":Ljava/lang/StringBuffer;
    .end local v2    # "szMsg":Ljava/lang/String;
    .end local v3    # "trace":[Ljava/lang/StackTraceElement;
    :cond_0
    :goto_0
    return-void

    .line 85
    .restart local v1    # "strBuffer":Ljava/lang/StringBuffer;
    .restart local v2    # "szMsg":Ljava/lang/String;
    .restart local v3    # "trace":[Ljava/lang/StackTraceElement;
    :cond_1
    const-string v4, "DBG_FMMDM"

    invoke-static {v4, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    .locals 7
    .param p0, "szContent"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 92
    sget-boolean v5, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_LOG_ON_:Z

    if-eqz v5, :cond_0

    .line 94
    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v5}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    .line 95
    .local v4, "trace":[Ljava/lang/StackTraceElement;
    const-string v3, ""

    .line 96
    .local v3, "szMsg":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 98
    .local v2, "strBuffer":Ljava/lang/StringBuffer;
    aget-object v1, v4, v6

    .line 99
    .local v1, "elt":Ljava/lang/StackTraceElement;
    const-string v5, "["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetReleaseVer()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    const-string v5, "]["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    const-string v5, "Line:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 104
    const-string v5, "]["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 105
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 106
    const-string v5, "] "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 108
    sget-boolean v5, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_PRIVATE_LOG_ON:Z

    if-eqz v5, :cond_4

    .line 110
    array-length v5, v4

    if-lt v5, v6, :cond_3

    .line 112
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 114
    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 122
    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 124
    const-string v5, "DBG_FMMDM"

    invoke-static {v5, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    .end local v1    # "elt":Ljava/lang/StackTraceElement;
    .end local v2    # "strBuffer":Ljava/lang/StringBuffer;
    .end local v3    # "szMsg":Ljava/lang/String;
    .end local v4    # "trace":[Ljava/lang/StackTraceElement;
    :cond_0
    :goto_1
    return-void

    .line 118
    .restart local v1    # "elt":Ljava/lang/StackTraceElement;
    .restart local v2    # "strBuffer":Ljava/lang/StringBuffer;
    .restart local v3    # "szMsg":Ljava/lang/String;
    .restart local v4    # "trace":[Ljava/lang/StackTraceElement;
    :cond_1
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v0

    .line 119
    .local v0, "base64Data":[B
    if-eqz v0, :cond_2

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    :goto_2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 128
    .end local v0    # "base64Data":[B
    :cond_3
    const-string v5, "DBG_FMMDM"

    invoke-static {v5, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 133
    :cond_4
    array-length v5, v4

    if-lt v5, v6, :cond_5

    .line 135
    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 137
    const-string v5, "DBG_FMMDM"

    invoke-static {v5, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 141
    :cond_5
    const-string v5, "DBG_FMMDM"

    invoke-static {v5, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static XDM_DUMP([BI)V
    .locals 8
    .param p0, "szBuf"    # [B
    .param p1, "pos"    # I

    .prologue
    const/16 v7, 0x20

    const/4 v6, 0x0

    .line 149
    if-nez p0, :cond_1

    .line 151
    const-string v4, "szBuf is null"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 188
    :cond_0
    return-void

    .line 155
    :cond_1
    const/4 v0, 0x0

    .line 156
    .local v0, "i":I
    array-length v1, p0

    .line 157
    .local v1, "len":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .local v2, "szAsc":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 160
    .local v3, "szDump":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 162
    add-int v4, v0, p1

    aget-byte v4, p0, v4

    shr-int/lit8 v4, v4, 0x4

    and-int/lit8 v4, v4, 0xf

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibHexToChar(I)I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 163
    add-int v4, v0, p1

    aget-byte v4, p0, v4

    and-int/lit8 v4, v4, 0xf

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibHexToChar(I)I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166
    add-int v4, v0, p1

    aget-byte v4, p0, v4

    if-lt v4, v7, :cond_5

    add-int v4, v0, p1

    aget-byte v4, p0, v4

    const/16 v5, 0x7e

    if-gt v4, v5, :cond_5

    .line 168
    add-int v4, v0, p1

    aget-byte v4, p0, v4

    int-to-char v4, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175
    :goto_1
    rem-int/lit8 v4, v0, 0x10

    const/16 v5, 0xf

    if-eq v4, v5, :cond_2

    add-int/lit8 v4, v1, -0x1

    if-ne v0, v4, :cond_4

    .line 177
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 179
    const-string v4, "   "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 182
    :cond_3
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 185
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 160
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_5
    const/16 v4, 0x2e

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static xdmGetPrivateLogStatus()Z
    .locals 1

    .prologue
    .line 226
    sget-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_PRIVATE_LOG_ON:Z

    return v0
.end method

.method public static xdmGetWbxmlDumpStatus()Z
    .locals 1

    .prologue
    .line 213
    sget-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_DUMP:Z

    return v0
.end method

.method public static xdmGetWbxmlFileStatus()Z
    .locals 1

    .prologue
    .line 200
    sget-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_FILE:Z

    return v0
.end method

.method public static xdmSetPrivateLogOnOff()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 218
    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 219
    sget-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_PRIVATE_LOG_ON:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_PRIVATE_LOG_ON:Z

    .line 220
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XDM_PRIVATE_LOG_ON : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_PRIVATE_LOG_ON:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XDM_PRIVATE_LOG_ON : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_PRIVATE_LOG_ON:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 222
    return-void

    .line 219
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmSetWbxmlDumpLogOnOff()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 205
    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 206
    sget-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_DUMP:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_DUMP:Z

    .line 207
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEBUG_WBXML_DUMP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_DUMP:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DEBUG_WBXML_DUMP : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_DUMP:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 209
    return-void

    .line 206
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmSetWbxmlFileLogOnOff()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 192
    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 193
    sget-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_FILE:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_FILE:Z

    .line 194
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WBXML_FILE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_FILE:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WBXML_FILE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/agent/XDMDebug;->DEBUG_WBXML_FILE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 196
    return-void

    .line 193
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmWriteFile(Ljava/lang/String;[B)V
    .locals 5
    .param p0, "szFile"    # Ljava/lang/String;
    .param p1, "data"    # [B

    .prologue
    .line 231
    const/4 v1, 0x0

    .line 234
    .local v1, "fw":Ljava/io/DataOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    .end local v1    # "fw":Ljava/io/DataOutputStream;
    .local v2, "fw":Ljava/io/DataOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 243
    if-eqz v2, :cond_2

    .line 246
    :try_start_2
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 253
    .end local v2    # "fw":Ljava/io/DataOutputStream;
    .restart local v1    # "fw":Ljava/io/DataOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 248
    .end local v1    # "fw":Ljava/io/DataOutputStream;
    .restart local v2    # "fw":Ljava/io/DataOutputStream;
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v1, v2

    .line 251
    .end local v2    # "fw":Ljava/io/DataOutputStream;
    .restart local v1    # "fw":Ljava/io/DataOutputStream;
    goto :goto_0

    .line 237
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 239
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 243
    if-eqz v1, :cond_0

    .line 246
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 248
    :catch_2
    move-exception v0

    .line 250
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 243
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v1, :cond_1

    .line 246
    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 251
    :cond_1
    :goto_3
    throw v3

    .line 248
    :catch_3
    move-exception v0

    .line 250
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 243
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/DataOutputStream;
    .restart local v2    # "fw":Ljava/io/DataOutputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/DataOutputStream;
    .restart local v1    # "fw":Ljava/io/DataOutputStream;
    goto :goto_2

    .line 237
    .end local v1    # "fw":Ljava/io/DataOutputStream;
    .restart local v2    # "fw":Ljava/io/DataOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/DataOutputStream;
    .restart local v1    # "fw":Ljava/io/DataOutputStream;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/DataOutputStream;
    .restart local v2    # "fw":Ljava/io/DataOutputStream;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/DataOutputStream;
    .restart local v1    # "fw":Ljava/io/DataOutputStream;
    goto :goto_0
.end method
