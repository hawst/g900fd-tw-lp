.class public Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;
.super Ljava/lang/Object;
.source "XLAWMOAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XCommonInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;


# static fields
.field public static mContext:Landroid/content/Context;

.field private static mEmergencyModeDate:Ljava/lang/String;

.field private static mEmergencyModeRequestor:Ljava/lang/String;

.field private static mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

.field public static mXLAWMOWaitTimer:Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, ""

    sput-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeDate:Ljava/lang/String;

    .line 44
    const-string v0, ""

    sput-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeRequestor:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xlawmoAdpCallrestrictionOperationsRestriction()Z
    .locals 2

    .prologue
    .line 679
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 680
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 681
    .local v0, "bAMTStatus":Z
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 683
    if-eqz v0, :cond_0

    .line 685
    const-string v1, "20"

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtCallRestrictionStatus(Ljava/lang/String;)V

    .line 688
    :cond_0
    const-string v1, "1200"

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 689
    return v0
.end method

.method public static xlawmoAdpCallrestrictionOperationsStop()Z
    .locals 2

    .prologue
    .line 694
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 695
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 696
    .local v0, "bAMTStatus":Z
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 698
    if-eqz v0, :cond_0

    .line 700
    const-string v1, "10"

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtCallRestrictionStatus(Ljava/lang/String;)V

    .line 703
    :cond_0
    const-string v1, "1200"

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 704
    return v0
.end method

.method public static xlawmoAdpForwardingOperationsDivertRequest()Z
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 755
    const-string v10, ""

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 756
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 758
    .local v0, "bAMTStatus":Z
    if-eqz v0, :cond_e

    .line 762
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetCallForwardingToBeEnabled()Z

    move-result v2

    .line 763
    .local v2, "bCallForwardingEnabled":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSMSForwardingToBeEnabled()Z

    move-result v4

    .line 764
    .local v4, "bSMSForwardingEnabled":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetCallForwardingToBeDisabled()Z

    move-result v1

    .line 765
    .local v1, "bCallForwardingDisabled":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSMSForwardingToBeDisabled()Z

    move-result v3

    .line 766
    .local v3, "bSMSForwardingDisabled":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetCallForwardingNumbers()Ljava/lang/String;

    move-result-object v6

    .line 767
    .local v6, "szCallForwardingNumbers":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSMSForwardingNumbers()Ljava/lang/String;

    move-result-object v8

    .line 768
    .local v8, "szSMSForwardingNumbers":Ljava/lang/String;
    const-string v7, "1470"

    .line 770
    .local v7, "szForwardingResult":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    :cond_0
    if-eqz v4, :cond_2

    if-eqz v3, :cond_2

    .line 772
    :cond_1
    const-string v10, "1470"

    invoke-static {v0, v10}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    move v0, v9

    .line 829
    .end local v0    # "bAMTStatus":Z
    .end local v1    # "bCallForwardingDisabled":Z
    .end local v2    # "bCallForwardingEnabled":Z
    .end local v3    # "bSMSForwardingDisabled":Z
    .end local v4    # "bSMSForwardingEnabled":Z
    .end local v6    # "szCallForwardingNumbers":Ljava/lang/String;
    .end local v7    # "szForwardingResult":Ljava/lang/String;
    .end local v8    # "szSMSForwardingNumbers":Ljava/lang/String;
    :goto_0
    return v0

    .line 775
    .restart local v0    # "bAMTStatus":Z
    .restart local v1    # "bCallForwardingDisabled":Z
    .restart local v2    # "bCallForwardingEnabled":Z
    .restart local v3    # "bSMSForwardingDisabled":Z
    .restart local v4    # "bSMSForwardingEnabled":Z
    .restart local v6    # "szCallForwardingNumbers":Ljava/lang/String;
    .restart local v7    # "szForwardingResult":Ljava/lang/String;
    .restart local v8    # "szSMSForwardingNumbers":Ljava/lang/String;
    :cond_2
    if-eqz v2, :cond_3

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    :cond_3
    if-eqz v4, :cond_5

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 777
    :cond_4
    const-string v10, "1470"

    invoke-static {v0, v10}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    move v0, v9

    .line 778
    goto :goto_0

    .line 781
    :cond_5
    if-nez v4, :cond_6

    if-eqz v3, :cond_9

    :cond_6
    if-nez v2, :cond_7

    if-eqz v1, :cond_9

    .line 783
    :cond_7
    const-string v7, "1470"

    .line 794
    :cond_8
    :goto_1
    const-string v9, "DSMForwardingResult"

    invoke-static {v9, v7}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetDSMOperationResult(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    new-instance v5, Landroid/content/Intent;

    const-string v9, "android.intent.action.dsm.DM_FORWARDING"

    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 798
    .local v5, "intent":Landroid/content/Intent;
    const-string v9, "callEnable"

    invoke-virtual {v5, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 799
    const-string v9, "smsEnable"

    invoke-virtual {v5, v9, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 800
    const-string v9, "callDisable"

    invoke-virtual {v5, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 801
    const-string v9, "smsDisable"

    invoke-virtual {v5, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 802
    const-string v9, "callnumber"

    invoke-virtual {v5, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 803
    const-string v9, "smsnumber"

    invoke-virtual {v5, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 804
    const/16 v9, 0x20

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 805
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x11

    if-ge v9, v10, :cond_d

    .line 807
    sget-object v9, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 813
    :goto_2
    const-string v9, "Send Broadcast : XCOMMON_INTENT_FORWARDING"

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 785
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_9
    if-nez v2, :cond_a

    if-eqz v1, :cond_b

    .line 787
    :cond_a
    const-string v7, "1471"

    goto :goto_1

    .line 789
    :cond_b
    if-nez v4, :cond_c

    if-eqz v3, :cond_8

    .line 791
    :cond_c
    const-string v7, "1472"

    goto :goto_1

    .line 811
    .restart local v5    # "intent":Landroid/content/Intent;
    :cond_d
    sget-object v9, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    sget-object v10, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v9, v5, v10}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_2

    .line 825
    .end local v1    # "bCallForwardingDisabled":Z
    .end local v2    # "bCallForwardingEnabled":Z
    .end local v3    # "bSMSForwardingDisabled":Z
    .end local v4    # "bSMSForwardingEnabled":Z
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "szCallForwardingNumbers":Ljava/lang/String;
    .end local v7    # "szForwardingResult":Ljava/lang/String;
    .end local v8    # "szSMSForwardingNumbers":Ljava/lang/String;
    :cond_e
    const/16 v9, 0x10

    invoke-static {v9}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 826
    const-string v9, "1452"

    invoke-static {v0, v9}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static xlawmoAdpGetBatteryLevel()I
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 937
    const/4 v4, -0x1

    .line 938
    .local v4, "percent":I
    new-instance v2, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 939
    .local v2, "filter":Landroid/content/IntentFilter;
    sget-object v6, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 941
    .local v0, "batteryStatus":Landroid/content/Intent;
    const-string v6, "level"

    invoke-virtual {v0, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 942
    .local v3, "level":I
    const-string v6, "scale"

    invoke-virtual {v0, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 944
    .local v5, "scale":I
    if-ltz v3, :cond_0

    if-gez v5, :cond_1

    .line 948
    :cond_0
    :try_start_0
    new-instance v6, Ljava/lang/Exception;

    invoke-direct {v6}, Ljava/lang/Exception;-><init>()V

    throw v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 950
    :catch_0
    move-exception v1

    .line 952
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 955
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    mul-int/lit8 v6, v3, 0x64

    div-int v4, v6, v5

    .line 956
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cnt: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 957
    return v4
.end method

.method public static xlawmoAdpGetConnectionCheckResultCode()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 894
    const-string v6, ""

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 896
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 898
    .local v0, "bAMTStatus":Z
    if-nez v0, :cond_1

    .line 900
    const-string v5, "1452"

    .line 932
    :cond_0
    :goto_0
    return-object v5

    .line 903
    :cond_1
    const-string v5, "1200"

    .line 905
    .local v5, "szResult":Ljava/lang/String;
    const/4 v1, 0x0

    .line 906
    .local v1, "bBatteryFail":Z
    const/4 v2, 0x0

    .line 907
    .local v2, "bCallCountFail":Z
    const/4 v3, 0x0

    .line 908
    .local v3, "bMsgCountFail":Z
    const/4 v4, 0x0

    .line 910
    .local v4, "bReactiveFail":Z
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetBatteryLevel()I

    move-result v6

    if-ne v6, v7, :cond_2

    .line 912
    const/4 v1, 0x1

    .line 914
    :cond_2
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetIncomingCallCount()I

    move-result v6

    if-eq v6, v7, :cond_3

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetOutgoingCallCount()I

    move-result v6

    if-eq v6, v7, :cond_3

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetMissedCallCount()I

    move-result v6

    if-ne v6, v7, :cond_4

    .line 916
    :cond_3
    const/4 v2, 0x1

    .line 918
    :cond_4
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetIncomingSMSCount()I

    move-result v6

    if-eq v6, v7, :cond_5

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetIncomingMMSCount()I

    move-result v6

    if-ne v6, v7, :cond_6

    .line 920
    :cond_5
    const/4 v3, 0x0

    .line 922
    :cond_6
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWSupportReactivationLock()Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "CHECKFAIL"

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetReactivationLockState()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 924
    const/4 v4, 0x1

    .line 927
    :cond_7
    if-nez v1, :cond_8

    if-nez v2, :cond_8

    if-nez v3, :cond_8

    if-eqz v4, :cond_0

    .line 929
    :cond_8
    const-string v5, "1454"

    goto :goto_0
.end method

.method public static xlawmoAdpGetDate()Ljava/lang/String;
    .locals 4

    .prologue
    .line 641
    const-string v1, ""

    .line 642
    .local v1, "szDate":Ljava/lang/String;
    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ss"

    .line 643
    .local v2, "szDateFormat":Ljava/lang/String;
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 645
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v3, "GMT+0"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 646
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 647
    const-string v3, "Z"

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 648
    return-object v1
.end method

.method public static xlawmoAdpGetEmergencyModeDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 227
    sget-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeDate:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSetEmergencyModeDate(Ljava/lang/String;)V

    .line 231
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DmEmergencyModeDate : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 232
    sget-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeDate:Ljava/lang/String;

    return-object v0
.end method

.method public static xlawmoAdpGetEmergencyModeRequestor()Ljava/lang/String;
    .locals 2

    .prologue
    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mEmergencyModeRequestor : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeRequestor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 256
    sget-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeRequestor:Ljava/lang/String;

    return-object v0
.end method

.method public static xlawmoAdpGetEmergencyModeState()Z
    .locals 4

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 269
    .local v0, "bState":Z
    sget-object v2, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    move-result-object v1

    .line 270
    .local v1, "em":Lcom/sec/android/emergencymode/EmergencyManager;
    sget-object v2, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/emergencymode/EmergencyManager;->isEmergencyMode(Landroid/content/Context;)Z

    move-result v0

    .line 272
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device EmergencyMode State : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 273
    return v0
.end method

.method public static xlawmoAdpGetIncomingCallCount()I
    .locals 14

    .prologue
    .line 962
    const/4 v11, -0x1

    .line 963
    .local v11, "nIncomingCallCnt":I
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPolicyPeriod()Ljava/lang/String;

    move-result-object v12

    .line 964
    .local v12, "szPeriod":Ljava/lang/String;
    new-instance v6, Ljava/util/GregorianCalendar;

    invoke-direct {v6}, Ljava/util/GregorianCalendar;-><init>()V

    .line 966
    .local v6, "cal":Ljava/util/Calendar;
    const/4 v7, 0x0

    .line 969
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v12}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    sub-long v8, v0, v2

    .line 970
    .local v8, "date":J
    sget-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "date > ? AND type = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x1

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 973
    if-eqz v7, :cond_1

    .line 974
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    .line 984
    :goto_0
    if-eqz v7, :cond_0

    .line 986
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 987
    const/4 v7, 0x0

    .line 990
    .end local v8    # "date":J
    :cond_0
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cnt: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 991
    return v11

    .line 976
    .restart local v8    # "date":J
    :cond_1
    :try_start_1
    const-string v0, "cursor is null"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 978
    .end local v8    # "date":J
    :catch_0
    move-exception v10

    .line 980
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 984
    if-eqz v7, :cond_0

    .line 986
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 987
    const/4 v7, 0x0

    goto :goto_1

    .line 984
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 986
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 987
    const/4 v7, 0x0

    :cond_2
    throw v0
.end method

.method public static xlawmoAdpGetIncomingMMSCount()I
    .locals 3

    .prologue
    .line 1071
    const/4 v0, -0x1

    .line 1072
    .local v0, "nIncomingMMSCnt":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cnt: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1073
    return v0
.end method

.method public static xlawmoAdpGetIncomingSMSCount()I
    .locals 3

    .prologue
    .line 1064
    const/4 v0, -0x1

    .line 1065
    .local v0, "nIncomingSMSCnt":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cnt: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1066
    return v0
.end method

.method public static xlawmoAdpGetMissedCallCount()I
    .locals 14

    .prologue
    .line 1030
    const/4 v11, -0x1

    .line 1031
    .local v11, "nMissedCallCnt":I
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPolicyPeriod()Ljava/lang/String;

    move-result-object v12

    .line 1032
    .local v12, "szPeriod":Ljava/lang/String;
    new-instance v6, Ljava/util/GregorianCalendar;

    invoke-direct {v6}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1034
    .local v6, "cal":Ljava/util/Calendar;
    const/4 v7, 0x0

    .line 1037
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v12}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    sub-long v8, v0, v2

    .line 1038
    .local v8, "date":J
    sget-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "date > ? AND type = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x1

    const/4 v13, 0x3

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1041
    if-eqz v7, :cond_1

    .line 1042
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    .line 1052
    :goto_0
    if-eqz v7, :cond_0

    .line 1054
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1055
    const/4 v7, 0x0

    .line 1058
    .end local v8    # "date":J
    :cond_0
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cnt: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1059
    return v11

    .line 1044
    .restart local v8    # "date":J
    :cond_1
    :try_start_1
    const-string v0, "cursor is null"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1046
    .end local v8    # "date":J
    :catch_0
    move-exception v10

    .line 1048
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1052
    if-eqz v7, :cond_0

    .line 1054
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1055
    const/4 v7, 0x0

    goto :goto_1

    .line 1052
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 1054
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1055
    const/4 v7, 0x0

    :cond_2
    throw v0
.end method

.method public static xlawmoAdpGetNonGpsSIM()Z
    .locals 1

    .prologue
    .line 596
    sget-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    if-eqz v0, :cond_0

    .line 597
    sget-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    invoke-virtual {v0}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->xamtGpsGetNonSIMLoc()Z

    move-result v0

    .line 599
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xlawmoAdpGetNonGpsSIMMCC()I
    .locals 1

    .prologue
    .line 611
    sget v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->SIMmcc:I

    return v0
.end method

.method public static xlawmoAdpGetNonGpsSIMMNC()I
    .locals 1

    .prologue
    .line 623
    sget v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->SIMmnc:I

    return v0
.end method

.method public static xlawmoAdpGetNonGpsSIMMSISDN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 635
    sget-object v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->g_szMsisdn:Ljava/lang/String;

    return-object v0
.end method

.method public static xlawmoAdpGetOutgoingCallCount()I
    .locals 14

    .prologue
    .line 996
    const/4 v11, -0x1

    .line 997
    .local v11, "nOutcomingCallCnt":I
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPolicyPeriod()Ljava/lang/String;

    move-result-object v12

    .line 998
    .local v12, "szPeriod":Ljava/lang/String;
    new-instance v6, Ljava/util/GregorianCalendar;

    invoke-direct {v6}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1000
    .local v6, "cal":Ljava/util/Calendar;
    const/4 v7, 0x0

    .line 1003
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v12}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    sub-long v8, v0, v2

    .line 1004
    .local v8, "date":J
    sget-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "date > ? AND type = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x1

    const/4 v13, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1007
    if-eqz v7, :cond_1

    .line 1008
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    .line 1018
    :goto_0
    if-eqz v7, :cond_0

    .line 1020
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1021
    const/4 v7, 0x0

    .line 1024
    .end local v8    # "date":J
    :cond_0
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cnt: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1025
    return v11

    .line 1010
    .restart local v8    # "date":J
    :cond_1
    :try_start_1
    const-string v0, "cursor is null"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1012
    .end local v8    # "date":J
    :catch_0
    move-exception v10

    .line 1014
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1018
    if-eqz v7, :cond_0

    .line 1020
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1021
    const/4 v7, 0x0

    goto :goto_1

    .line 1018
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 1020
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1021
    const/4 v7, 0x0

    :cond_2
    throw v0
.end method

.method public static xlawmoAdpGetReactivationLockState()Ljava/lang/String;
    .locals 6

    .prologue
    .line 298
    const/4 v1, -0x2

    .line 299
    .local v1, "nState":I
    const-string v3, ""

    .line 301
    .local v3, "szStatue":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWSupportReactivationLock()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 307
    :try_start_0
    new-instance v2, Lcom/samsung/android/service/reactive/ReactiveServiceManager;

    sget-object v4, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/samsung/android/service/reactive/ReactiveServiceManager;-><init>(Landroid/content/Context;)V

    .line 308
    .local v2, "rsm":Lcom/samsung/android/service/reactive/ReactiveServiceManager;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/samsung/android/service/reactive/ReactiveServiceManager;->getFlag(I)I

    move-result v1

    .line 309
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getRequiredAuthFlag State : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 310
    if-nez v1, :cond_0

    .line 312
    const-string v4, "Reactivation Lock Flag deativated"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 313
    const-string v3, "N"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    .end local v2    # "rsm":Lcom/samsung/android/service/reactive/ReactiveServiceManager;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "szStatue : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 337
    return-object v3

    .line 315
    .restart local v2    # "rsm":Lcom/samsung/android/service/reactive/ReactiveServiceManager;
    :cond_0
    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 317
    :try_start_1
    const-string v4, "Reactivation Lock Flag ativated"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 318
    const-string v3, "Y"

    goto :goto_0

    .line 322
    :cond_1
    const-string v3, "CHECKFAIL"

    .line 323
    const-string v4, "getRequiredAuthFlag Call FAIL"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 326
    .end local v2    # "rsm":Lcom/samsung/android/service/reactive/ReactiveServiceManager;
    :catch_0
    move-exception v0

    .line 328
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 333
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v3, "CHECKFAIL"

    goto :goto_0
.end method

.method public static xlawmoAdpGetServerUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1078
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWURL()Ljava/lang/String;

    move-result-object v1

    .line 1081
    .local v1, "szUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1083
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetSalesCode()Ljava/lang/String;

    move-result-object v0

    .line 1084
    .local v0, "szSalesCode":Ljava/lang/String;
    const-string v2, "CHN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CHU"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CHM"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CTC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CHC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1086
    :cond_0
    const-string v1, "https://dm.samsungdive.cn/v1/sdm/magicsync/dm"

    .line 1093
    .end local v0    # "szSalesCode":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v1

    .line 1090
    .restart local v0    # "szSalesCode":Ljava/lang/String;
    :cond_2
    const-string v1, "https://dm.samsungdive.com/v1/sdm/magicsync/dm"

    goto :goto_0
.end method

.method public static xlawmoAdpLockMyPhoneOperationChangeMsg()Z
    .locals 4

    .prologue
    .line 653
    const-string v2, ""

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 654
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 655
    .local v0, "bAMTStatus":Z
    const/16 v2, 0x10

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 657
    if-eqz v0, :cond_0

    .line 659
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtLockMyPhoneMessages()Ljava/lang/String;

    move-result-object v1

    .line 660
    .local v1, "szMessage":Ljava/lang/String;
    sget-object v2, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 663
    .end local v1    # "szMessage":Ljava/lang/String;
    :cond_0
    const-string v2, "1200"

    invoke-static {v0, v2}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 664
    return v0
.end method

.method public static xlawmoAdpOerationConnectionCheck()Z
    .locals 1

    .prologue
    .line 888
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 889
    const/4 v0, 0x1

    return v0
.end method

.method public static xlawmoAdpOerationEmergencyMode()Z
    .locals 5

    .prologue
    .line 342
    const-string v3, ""

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 343
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 345
    .local v0, "bAMTStatus":Z
    if-eqz v0, :cond_2

    .line 349
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetEmergencyModeState()Z

    move-result v2

    .line 350
    .local v2, "bOperationState":Z
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetEmergencyModeState()Z

    move-result v1

    .line 352
    .local v1, "bDeviceState":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EmergencyMode  bOperationState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bDeviceState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 354
    if-ne v2, v1, :cond_1

    .line 356
    if-eqz v1, :cond_0

    const/16 v3, 0x64

    :goto_0
    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentEmergencyModeResult(I)V

    .line 381
    .end local v1    # "bDeviceState":Z
    .end local v2    # "bOperationState":Z
    :goto_1
    return v0

    .line 356
    .restart local v1    # "bDeviceState":Z
    .restart local v2    # "bOperationState":Z
    :cond_0
    const/16 v3, 0x65

    goto :goto_0

    .line 361
    :cond_1
    invoke-static {v2}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSetEmergencyModeState(Z)V

    goto :goto_1

    .line 375
    .end local v1    # "bDeviceState":Z
    .end local v2    # "bOperationState":Z
    :cond_2
    const/16 v3, 0x10

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 376
    const-string v3, "1452"

    invoke-static {v0, v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    goto :goto_1
.end method

.method public static xlawmoAdpOerationFactoryReset()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 511
    const-string v2, ""

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 512
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 513
    .local v0, "bAMTStatus":Z
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 514
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 516
    if-eqz v0, :cond_0

    .line 519
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.dsm.DM_FACTORY_RESET"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 520
    .local v1, "intent":Landroid/content/Intent;
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 521
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_1

    .line 523
    sget-object v2, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 529
    :goto_0
    const-string v2, "Send Broadcast : android.intent.action.dsm.DM_FACTORY_RESET"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 532
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return v0

    .line 527
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v2, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method public static xlawmoAdpOerationPartiallyLock()Z
    .locals 2

    .prologue
    .line 133
    const/4 v0, 0x1

    .line 134
    .local v0, "bRet":Z
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 139
    sget-object v1, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->XLAWMO_STATE_PARTIALLYLOCKED:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetState(I)V

    .line 142
    const-string v1, "1200"

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 143
    return v0
.end method

.method public static xlawmoAdpOerationReactivationLock()Z
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 537
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 538
    .local v0, "bAMTStatus":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWSupportReactivationLock()Z

    move-result v1

    .line 539
    .local v1, "bSupport":Z
    const-string v3, ""

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 541
    if-eqz v1, :cond_2

    .line 543
    if-eqz v0, :cond_1

    .line 548
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.dsm.DM_REACT_LOCK"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 549
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "status"

    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetReactivationState()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 550
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 551
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v3, v4, :cond_0

    .line 553
    sget-object v3, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 560
    :goto_0
    new-instance v3, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;

    const/16 v4, 0xf

    invoke-direct {v3, v4}, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;-><init>(I)V

    sput-object v3, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mXLAWMOWaitTimer:Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;

    .line 561
    const-string v3, "Send Broadcast : android.intent.action.dsm.DM_REACT_LOCK"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 585
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_1
    return v0

    .line 557
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_0
    sget-object v3, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0

    .line 573
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 574
    const-string v3, "1452"

    invoke-static {v0, v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    goto :goto_1

    .line 580
    :cond_2
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 581
    const-string v3, "1475"

    invoke-static {v0, v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    goto :goto_1
.end method

.method public static xlawmoAdpOerationUnLock()Z
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x1

    .line 150
    .local v0, "bRet":Z
    sget-object v1, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetState(I)V

    .line 151
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 153
    return v0
.end method

.method public static xlawmoAdpOerationWipe()Z
    .locals 8

    .prologue
    .line 158
    const-string v6, ""

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 159
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 161
    .local v0, "bAMTStatus":Z
    if-eqz v0, :cond_4

    .line 167
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExternalToBeWiped()Z

    move-result v1

    .line 168
    .local v1, "bExternalWiped":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetSIMToBeWiped()Z

    move-result v2

    .line 170
    .local v2, "bSIMWiped":Z
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 172
    const/16 v4, 0xa

    .line 173
    .local v4, "nWipeStatus":I
    const-string v5, "1460"

    .line 191
    .local v5, "szWipeResult":Ljava/lang/String;
    :goto_0
    const-string v6, "DSMWipeOutResult"

    invoke-static {v6, v5}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetDSMOperationResult(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.dsm.DM_WIPE"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 194
    .local v3, "intent":Landroid/content/Intent;
    const-string v6, "status"

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 195
    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 196
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x11

    if-ge v6, v7, :cond_3

    .line 198
    sget-object v6, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 204
    :goto_1
    const-string v6, "Send Broadcast : XCOMMON_INTENT_WIPE"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 222
    .end local v0    # "bAMTStatus":Z
    .end local v1    # "bExternalWiped":Z
    .end local v2    # "bSIMWiped":Z
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "nWipeStatus":I
    .end local v5    # "szWipeResult":Ljava/lang/String;
    :goto_2
    return v0

    .line 175
    .restart local v0    # "bAMTStatus":Z
    .restart local v1    # "bExternalWiped":Z
    .restart local v2    # "bSIMWiped":Z
    :cond_0
    if-eqz v1, :cond_1

    .line 177
    const/16 v4, 0x14

    .line 178
    .restart local v4    # "nWipeStatus":I
    const-string v5, "1461"

    .restart local v5    # "szWipeResult":Ljava/lang/String;
    goto :goto_0

    .line 180
    .end local v4    # "nWipeStatus":I
    .end local v5    # "szWipeResult":Ljava/lang/String;
    :cond_1
    if-eqz v2, :cond_2

    .line 182
    const/16 v4, 0x1e

    .line 183
    .restart local v4    # "nWipeStatus":I
    const-string v5, "1462"

    .restart local v5    # "szWipeResult":Ljava/lang/String;
    goto :goto_0

    .line 187
    .end local v4    # "nWipeStatus":I
    .end local v5    # "szWipeResult":Ljava/lang/String;
    :cond_2
    const-string v6, "1406"

    invoke-static {v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 188
    const/4 v0, 0x0

    goto :goto_2

    .line 202
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "nWipeStatus":I
    .restart local v5    # "szWipeResult":Ljava/lang/String;
    :cond_3
    sget-object v6, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    sget-object v7, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v6, v3, v7}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_1

    .line 216
    .end local v1    # "bExternalWiped":Z
    .end local v2    # "bSIMWiped":Z
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "nWipeStatus":I
    .end local v5    # "szWipeResult":Ljava/lang/String;
    :cond_4
    const/16 v6, 0x10

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 217
    const-string v6, "1452"

    invoke-static {v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    goto :goto_2
.end method

.method public static xlawmoAdpReceiveIntentEmergencyModeResult(I)V
    .locals 4
    .param p0, "nResultReport"    # I

    .prologue
    const/4 v3, 0x1

    .line 386
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v1

    const/16 v2, 0xe

    if-ne v1, v2, :cond_5

    .line 388
    const-string v0, "1490"

    .line 389
    .local v0, "szResultReport":Ljava/lang/String;
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 391
    const/16 v1, 0x64

    if-ne p0, v1, :cond_0

    .line 393
    const-string v0, "1492"

    .line 412
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nResultReport : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , szResultReport : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 413
    invoke-static {v3, v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 419
    .end local v0    # "szResultReport":Ljava/lang/String;
    :goto_1
    return-void

    .line 395
    .restart local v0    # "szResultReport":Ljava/lang/String;
    :cond_0
    const/16 v1, 0x65

    if-ne p0, v1, :cond_1

    .line 397
    const-string v0, "1493"

    goto :goto_0

    .line 399
    :cond_1
    if-eq p0, v3, :cond_2

    const/4 v1, 0x3

    if-eq p0, v1, :cond_2

    const/4 v1, 0x5

    if-ne p0, v1, :cond_3

    .line 401
    :cond_2
    const-string v0, "1200"

    goto :goto_0

    .line 403
    :cond_3
    const/4 v1, -0x8

    if-ne p0, v1, :cond_4

    .line 405
    const-string v0, "1491"

    goto :goto_0

    .line 409
    :cond_4
    const-string v0, "1490"

    goto :goto_0

    .line 417
    .end local v0    # "szResultReport":Ljava/lang/String;
    :cond_5
    const-string v1, "Lawmo Operation Not DONE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xlawmoAdpReceiveIntentForwardingResult(I)V
    .locals 3
    .param p0, "nResultReport"    # I

    .prologue
    .line 834
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v1

    const/16 v2, 0xb

    if-ne v1, v2, :cond_4

    .line 836
    const-string v0, "1470"

    .line 837
    .local v0, "szResultReport":Ljava/lang/String;
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 839
    const-string v1, "1200"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_1

    .line 840
    const-string v0, "1200"

    .line 848
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ForwardingResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 849
    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 855
    .end local v0    # "szResultReport":Ljava/lang/String;
    :goto_1
    return-void

    .line 841
    .restart local v0    # "szResultReport":Ljava/lang/String;
    :cond_1
    const-string v1, "1470"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_2

    .line 842
    const-string v0, "1470"

    goto :goto_0

    .line 843
    :cond_2
    const-string v1, "1471"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_3

    .line 844
    const-string v0, "1471"

    goto :goto_0

    .line 845
    :cond_3
    const-string v1, "1472"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_0

    .line 846
    const-string v0, "1472"

    goto :goto_0

    .line 853
    .end local v0    # "szResultReport":Ljava/lang/String;
    :cond_4
    const-string v1, "Lawmo Operation Not DONE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xlawmoAdpReceiveIntentGetLocationResult(Landroid/content/Intent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 491
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetDBOperation()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 493
    if-nez p0, :cond_0

    .line 495
    const-string v1, "intent is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 507
    :goto_0
    return-void

    .line 499
    :cond_0
    const-string v1, "isSucceed"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 500
    .local v0, "isSuccess":Z
    invoke-static {p0}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->xamtGpsSetLocation(Landroid/content/Intent;)V

    .line 501
    invoke-static {v0}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocation(Z)V

    goto :goto_0

    .line 505
    .end local v0    # "isSuccess":Z
    :cond_1
    const-string v1, "Lawmo Operation Not DONE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xlawmoAdpReceiveIntentReactivationResult(I)V
    .locals 3
    .param p0, "nResultReport"    # I

    .prologue
    .line 448
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v1

    const/16 v2, 0xf

    if-ne v1, v2, :cond_3

    .line 450
    const-string v0, "1475"

    .line 451
    .local v0, "szResultReport":Ljava/lang/String;
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 453
    const-string v1, "1200"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_1

    .line 454
    const-string v0, "1200"

    .line 460
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReactivationResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 461
    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 467
    .end local v0    # "szResultReport":Ljava/lang/String;
    :goto_1
    return-void

    .line 455
    .restart local v0    # "szResultReport":Ljava/lang/String;
    :cond_1
    const-string v1, "1475"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_2

    .line 456
    const-string v0, "1475"

    goto :goto_0

    .line 457
    :cond_2
    const-string v1, "1476"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_0

    .line 458
    const-string v0, "1476"

    goto :goto_0

    .line 465
    .end local v0    # "szResultReport":Ljava/lang/String;
    :cond_3
    const-string v1, "Lawmo Operation Not DONE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xlawmoAdpReceiveIntentWipeResult(I)V
    .locals 3
    .param p0, "nResultReport"    # I

    .prologue
    .line 423
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 425
    const-string v0, "1460"

    .line 426
    .local v0, "szResultReport":Ljava/lang/String;
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 428
    const-string v1, "1200"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_1

    .line 429
    const-string v0, "1200"

    .line 437
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WipeResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 438
    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 444
    .end local v0    # "szResultReport":Ljava/lang/String;
    :goto_1
    return-void

    .line 430
    .restart local v0    # "szResultReport":Ljava/lang/String;
    :cond_1
    const-string v1, "1460"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_2

    .line 431
    const-string v0, "1460"

    goto :goto_0

    .line 432
    :cond_2
    const-string v1, "1461"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_3

    .line 433
    const-string v0, "1461"

    goto :goto_0

    .line 434
    :cond_3
    const-string v1, "1462"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p0, v1, :cond_0

    .line 435
    const-string v0, "1462"

    goto :goto_0

    .line 442
    .end local v0    # "szResultReport":Ljava/lang/String;
    :cond_4
    const-string v1, "Lawmo Operation Not DONE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xlawmoAdpResumeForwardingResult()V
    .locals 3

    .prologue
    .line 859
    const-string v1, "DSMForwardingResult"

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetDSMOperationResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 861
    .local v0, "szResultReport":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 863
    const-string v1, "Read Fail DSM Content Provider"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 864
    const-string v0, "1470"

    .line 872
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forwarding Result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 873
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 874
    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 875
    return-void

    .line 866
    :cond_1
    const-string v1, "1200"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1471"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1472"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 868
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Read Fail DSM Content Provider : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 869
    const-string v0, "1470"

    goto :goto_0
.end method

.method public static xlawmoAdpResumeWipeResult()V
    .locals 3

    .prologue
    .line 471
    const-string v1, "DSMWipeOutResult"

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetDSMOperationResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 473
    .local v0, "szResultReport":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 475
    const-string v1, "Read Fail DSM Content Provider"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 476
    const-string v0, "1460"

    .line 484
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wipe Result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 485
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 486
    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 487
    return-void

    .line 478
    :cond_1
    const-string v1, "1200"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1461"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1462"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 480
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Read Fail DSM Content Provider : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 481
    const-string v0, "1460"

    goto :goto_0
.end method

.method public static xlawmoAdpRingMyPhoneOperationsStop()Z
    .locals 2

    .prologue
    .line 744
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 745
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 747
    .local v0, "bAMTStatus":Z
    const-string v1, "10"

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtRingMyPhoneStatus(Ljava/lang/String;)V

    .line 748
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 749
    return v0
.end method

.method public static xlawmoAdpSetContext(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    sput-object p0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    .line 53
    new-instance v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    invoke-direct {v0}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;-><init>()V

    sput-object v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    .line 54
    return-void
.end method

.method public static xlawmoAdpSetEmergencyModeDate(Ljava/lang/String;)V
    .locals 4
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 237
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "date : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 239
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241
    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    .line 242
    .local v1, "dateFormatPattern":Ljava/lang/String;
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 243
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v2, "GMT+0"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 244
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeDate:Ljava/lang/String;

    .line 250
    .end local v0    # "dateFormat":Ljava/text/SimpleDateFormat;
    .end local v1    # "dateFormatPattern":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DmEmergencyModeDate : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 251
    return-void

    .line 248
    :cond_0
    sput-object p0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeDate:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xlawmoAdpSetEmergencyModeRequestor(Ljava/lang/String;)V
    .locals 2
    .param p0, "requestor"    # Ljava/lang/String;

    .prologue
    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestor : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 262
    sput-object p0, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mEmergencyModeRequestor:Ljava/lang/String;

    .line 263
    return-void
.end method

.method public static xlawmoAdpSetEmergencyModeState(Z)V
    .locals 4
    .param p0, "bState"    # Z

    .prologue
    .line 279
    const/4 v1, 0x1

    .line 280
    .local v1, "nRet":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EmergencyModeState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 282
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 283
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "enabled"

    invoke-virtual {v0, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 284
    const-string v2, "flag"

    const/16 v3, 0x40

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 285
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_0

    .line 287
    sget-object v2, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 293
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nRet : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 294
    return-void

    .line 291
    :cond_0
    sget-object v2, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method public static xlawmoAdpSyncLockMyPhoneOperationChangeMsg()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 669
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtLockMyPhoneMessages()Ljava/lang/String;

    move-result-object v0

    .line 670
    .local v0, "szMessage":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v2}, Lcom/fmm/dm/XDMService;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 672
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 673
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 674
    const/4 v1, 0x1

    return v1
.end method

.method public static xlawmoAdpSyncMasterKeyOperationsLockRelease()V
    .locals 2

    .prologue
    .line 880
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.dsm.DM_LOCK_RELEASE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 881
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 882
    sget-object v1, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 883
    const-string v1, "Send Broadcast : XCOMMON_INTENT_LOCK_RELEASE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 884
    return-void
.end method

.method public static xlawmoAdpSyncOerationFullyLock()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 58
    const-string v8, ""

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 60
    .local v0, "bAMTStatus":Z
    invoke-static {v7}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 61
    invoke-static {v7}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 63
    if-eqz v0, :cond_1

    .line 65
    sget-object v8, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->XLAWMO_STATE_FULLYLOCKED:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetState(I)V

    .line 69
    const/4 v2, 0x0

    .line 71
    .local v2, "nCategory":I
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentGetOperationCategory()I

    move-result v2

    .line 73
    if-eqz v2, :cond_7

    .line 75
    const-string v3, ""

    .line 76
    .local v3, "szMessage":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtPassword()Ljava/lang/String;

    move-result-object v5

    .line 77
    .local v5, "szPassword":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetReactivationLock()Ljava/lang/String;

    move-result-object v6

    .line 78
    .local v6, "szReactivationLock":Ljava/lang/String;
    const-string v4, ""

    .line 80
    .local v4, "szNumber":Ljava/lang/String;
    and-int/lit8 v8, v2, 0x2

    if-eqz v8, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 82
    :cond_0
    sget-object v8, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetState(I)V

    .line 83
    const-string v8, "FullyLock = NOT_PASSWORD_FAIL"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move v0, v7

    .line 128
    .end local v3    # "szMessage":Ljava/lang/String;
    .end local v4    # "szNumber":Ljava/lang/String;
    .end local v5    # "szPassword":Ljava/lang/String;
    .end local v6    # "szReactivationLock":Ljava/lang/String;
    :cond_1
    :goto_0
    return v0

    .line 86
    .restart local v3    # "szMessage":Ljava/lang/String;
    .restart local v4    # "szNumber":Ljava/lang/String;
    .restart local v5    # "szPassword":Ljava/lang/String;
    .restart local v6    # "szReactivationLock":Ljava/lang/String;
    :cond_2
    and-int/lit8 v8, v2, 0x1

    if-eqz v8, :cond_4

    and-int/lit8 v8, v2, 0x4

    if-eqz v8, :cond_4

    .line 88
    const-string v8, "FullyLock = LOCKMYPHONE_MESSAGES & CALLRESTRICTION_PHONENUMBER"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 89
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtLockMyPhoneMessages()Ljava/lang/String;

    move-result-object v3

    .line 90
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtCallRestrictionPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 104
    :cond_3
    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-string v8, "android.intent.action.dsm.DM_LOCK_MY_PHONE"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 105
    .local v1, "intent":Landroid/content/Intent;
    const-string v8, "status"

    const-string v9, "20"

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const-string v8, "password"

    invoke-virtual {v1, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    const-string v8, "message"

    invoke-virtual {v1, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v8, "number"

    invoke-virtual {v1, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    const-string v8, "Y"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 111
    const-string v7, "reactivationlock"

    const/4 v8, 0x1

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 115
    :goto_2
    const/16 v7, 0x20

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 116
    new-instance v7, Landroid/content/ComponentName;

    const-string v8, "com.sec.dsm.system"

    const-string v9, "com.sec.dsm.system.DSMReceiver"

    invoke-direct {v7, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 117
    sget-object v7, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 118
    const-string v7, "Send Broadcast : XCOMMON_INTENT_LOCK_MY_PHONE"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 119
    const-string v7, "20"

    invoke-static {v7}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtCallRestrictionStatus(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_4
    and-int/lit8 v8, v2, 0x1

    if-eqz v8, :cond_5

    .line 94
    const-string v8, "FullyLock = LOCKMYPHONE_MESSAGES"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtLockMyPhoneMessages()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 97
    :cond_5
    and-int/lit8 v8, v2, 0x4

    if-eqz v8, :cond_3

    .line 99
    const-string v8, "FullyLock = CALLRESTRICTION_PHONENUMBER"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtCallRestrictionPhoneNumber()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 113
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_6
    const-string v8, "reactivationlock"

    invoke-virtual {v1, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 123
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "szMessage":Ljava/lang/String;
    .end local v4    # "szNumber":Ljava/lang/String;
    .end local v5    # "szPassword":Ljava/lang/String;
    .end local v6    # "szReactivationLock":Ljava/lang/String;
    :cond_7
    const-string v7, "Lock MyPhone None Category"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static xlawmoAdpSyncRingMyPhoneOperationsStart()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 709
    const-string v3, ""

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 710
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 711
    .local v0, "bAMTStatus":Z
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 712
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 714
    if-eqz v0, :cond_0

    .line 718
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetExtRingMyPhoneMessages()Ljava/lang/String;

    move-result-object v2

    .line 719
    .local v2, "szMessage":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 722
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.dsm.DM_RING_MY_PHONE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 723
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "message"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 724
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 726
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v3, v4, :cond_1

    .line 728
    sget-object v3, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 734
    :goto_0
    const-string v3, "Send Broadcast : XCOMMON_INTENT_RING_MY_PHONE"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 736
    const-string v3, "20"

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtRingMyPhoneStatus(Ljava/lang/String;)V

    .line 739
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "szMessage":Ljava/lang/String;
    :cond_0
    return v0

    .line 732
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "szMessage":Ljava/lang/String;
    :cond_1
    sget-object v3, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method
