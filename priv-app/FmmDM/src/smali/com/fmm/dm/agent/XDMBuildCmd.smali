.class public Lcom/fmm/dm/agent/XDMBuildCmd;
.super Lcom/fmm/dm/agent/XDMHandleCmd;
.source "XDMBuildCmd.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMHandleCmd;-><init>()V

    return-void
.end method

.method public static xdmAgentBuildCmdAlert(Lcom/fmm/dm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserAlert;
    .locals 2
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p1, "szData"    # Ljava/lang/String;

    .prologue
    .line 141
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserAlert;-><init>()V

    .line 142
    .local v0, "alertCmd":Lcom/fmm/dm/eng/parser/XDMParserAlert;
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/fmm/dm/eng/core/XDMWorkspace;)I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    .line 143
    iput-object p1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 145
    return-object v0
.end method

.method public static xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;
    .locals 12
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p1, "cmdRef"    # I
    .param p2, "szSource"    # Ljava/lang/String;
    .param p3, "szFormat"    # Ljava/lang/String;
    .param p4, "szType"    # Ljava/lang/String;
    .param p5, "size"    # I
    .param p6, "data"    # [C

    .prologue
    .line 510
    const/4 v7, 0x0

    .line 511
    .local v7, "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    const/4 v3, 0x0

    .line 512
    .local v3, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v4, 0x0

    .line 513
    .local v4, "meta":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    const/4 v1, 0x0

    .local v1, "h":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v9, 0x0

    .line 514
    .local v9, "t":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v5, 0x0

    .line 515
    .local v5, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    const/16 v10, 0x80

    new-array v6, v10, [C

    .line 518
    .local v6, "nodename":[C
    if-eqz p0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 520
    :cond_0
    const-string v10, "ws or source is null"

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 521
    const/4 v10, 0x0

    .line 598
    :goto_0
    return-object v10

    .line 524
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    const/16 v11, 0x3f

    invoke-static {v10, v11, v6}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    .line 525
    invoke-static {v6}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v8

    .line 526
    .local v8, "szNodeName1":Ljava/lang/String;
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-static {v10, v8}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 527
    if-nez v5, :cond_2

    .line 529
    const-string v10, "Result node is null"

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 530
    const/4 v10, 0x0

    goto :goto_0

    .line 534
    :cond_2
    new-instance v7, Lcom/fmm/dm/eng/parser/XDMParserResults;

    .end local v7    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    invoke-direct {v7}, Lcom/fmm/dm/eng/parser/XDMParserResults;-><init>()V

    .line 535
    .restart local v7    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    new-instance v3, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .end local v3    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-direct {v3}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 536
    .restart local v3    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/fmm/dm/eng/core/XDMWorkspace;)I

    move-result v10

    iput v10, v7, Lcom/fmm/dm/eng/parser/XDMParserResults;->cmdid:I

    .line 537
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    iput-object v10, v7, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    .line 538
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/fmm/dm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    .line 541
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 543
    iput-object p2, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 547
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    if-lez p5, :cond_8

    .line 549
    :cond_4
    new-instance v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .end local v4    # "meta":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    invoke-direct {v4}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    .line 550
    .restart local v4    # "meta":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 551
    move-object/from16 v0, p4

    iput-object v0, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 552
    :cond_5
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 553
    iput-object p3, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 554
    :cond_6
    if-lez p5, :cond_7

    .line 555
    move/from16 v0, p5

    iput v0, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    .line 556
    :cond_7
    iput-object v4, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 560
    :cond_8
    if-eqz p6, :cond_d

    .line 562
    iget v10, v5, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_a

    .line 564
    new-instance v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v10}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 565
    iget-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    const/4 v11, 0x1

    iput v11, v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    .line 566
    iget-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move/from16 v0, p5

    new-array v11, v0, [C

    iput-object v11, v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 567
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    move/from16 v0, p5

    if-ge v2, v0, :cond_9

    .line 568
    iget-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v10, v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v11, p6, v2

    aput-char v11, v10, v2

    .line 567
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 570
    :cond_9
    iget-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move/from16 v0, p5

    iput v0, v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 594
    .end local v2    # "i":I
    :goto_2
    invoke-static {v1, v9, v3}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 596
    iput-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    move-object v10, v7

    .line 598
    goto/16 :goto_0

    .line 574
    :cond_a
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_c

    const-string v10, "bin"

    invoke-virtual {v10, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_c

    .line 576
    new-instance v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v10}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 577
    iget-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move/from16 v0, p5

    new-array v11, v0, [C

    iput-object v11, v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 579
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    move/from16 v0, p5

    if-ge v2, v0, :cond_b

    .line 580
    iget-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v10, v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v11, p6, v2

    aput-char v11, v10, v2

    .line 579
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 582
    :cond_b
    iget-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move/from16 v0, p5

    iput v0, v10, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    goto :goto_2

    .line 586
    .end local v2    # "i":I
    :cond_c
    invoke-static/range {p6 .. p6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentDataStString2Pcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v10

    iput-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    goto :goto_2

    .line 592
    :cond_d
    const/4 v10, 0x0

    iput-object v10, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    goto :goto_2
.end method

.method public static xdmAgentBuildCmdGenericAlert(Lcom/fmm/dm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserAlert;
    .locals 9
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p1, "szData"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 152
    const/4 v3, 0x0

    .line 153
    .local v3, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v1, 0x0

    .local v1, "head":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v5, 0x0

    .line 156
    .local v5, "tail":Lcom/fmm/dm/eng/core/XDMList;
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserAlert;-><init>()V

    .line 157
    .local v0, "alertCmd":Lcom/fmm/dm/eng/parser/XDMParserAlert;
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/fmm/dm/eng/core/XDMWorkspace;)I

    move-result v6

    iput v6, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    .line 158
    iput-object p1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 160
    new-instance v3, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .end local v3    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-direct {v3}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 161
    .restart local v3    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    new-instance v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v6}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 162
    new-instance v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v6}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 164
    sget-boolean v6, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xlawmoGetbSimChange()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 166
    const-string v6, "Sim change alert"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 167
    const-string v6, "./Ext/OSPS/devInfo/SIM"

    iput-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 169
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v7, "urn:oma:at:lawmo:1.0:userrequest"

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 170
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v7, "warning"

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    .line 173
    const-string v4, ""

    .line 174
    .local v4, "szAlertData":Ljava/lang/String;
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 175
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iput v8, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    .line 230
    :goto_0
    invoke-static {v1, v5, v3}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 231
    iput-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 233
    return-object v0

    .line 177
    .end local v4    # "szAlertData":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUnlockAlert()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 179
    const-string v6, "Unlock alert"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 180
    const-string v6, "./Ext/OSPS/Unlock"

    iput-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 182
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v7, "urn:oma:at:lawmo:1.0:userrequest"

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 183
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v7, "warning"

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    .line 186
    const-string v4, ""

    .line 187
    .restart local v4    # "szAlertData":Ljava/lang/String;
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 188
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iput v8, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    goto :goto_0

    .line 190
    .end local v4    # "szAlertData":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetEmergencyModeChangeAlert()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 192
    const-string v6, "EmergencyMode alert"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 193
    const-string v6, "./Ext/OSPS/EmergencyMode"

    iput-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 195
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v7, "urn:oma:at:lawmo:1.0:userrequest"

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 197
    const-string v4, ""

    .line 198
    .restart local v4    # "szAlertData":Ljava/lang/String;
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 199
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iput v8, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    goto :goto_0

    .line 201
    .end local v4    # "szAlertData":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetReActiveAlert()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 203
    const-string v6, "ReActive alert"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 204
    const-string v6, "./Ext/OSPS/ReactivationLock"

    iput-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 206
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v7, "urn:oma:at:lawmo:1.0:userrequest"

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 208
    const-string v4, ""

    .line 209
    .restart local v4    # "szAlertData":Ljava/lang/String;
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 210
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iput v8, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    goto :goto_0

    .line 214
    .end local v4    # "szAlertData":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->getInitType()I

    move-result v2

    .line 215
    .local v2, "initType":I
    const/4 v6, 0x1

    if-ne v2, v6, :cond_4

    .line 217
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v7, "chr"

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 218
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v7, "org.openmobilealliance.dm.firmwareupdate.userrequest"

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 219
    const-string v6, "ALERT_TYPE_USER_INIT"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 225
    :goto_1
    const-string v4, "0"

    .line 226
    .restart local v4    # "szAlertData":Ljava/lang/String;
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 227
    iget-object v6, v3, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iput v8, v6, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    goto/16 :goto_0

    .line 223
    .end local v4    # "szAlertData":Ljava/lang/String;
    :cond_4
    const-string v6, "Init no flag"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmAgentBuildCmdGenericAlertReport(Lcom/fmm/dm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserAlert;
    .locals 9
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p1, "szData"    # Ljava/lang/String;

    .prologue
    .line 239
    const/4 v1, 0x0

    .line 240
    .local v1, "Item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v2, 0x0

    .local v2, "head":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v6, 0x0

    .line 241
    .local v6, "tail":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v5, 0x0

    .line 242
    .local v5, "szResult":Ljava/lang/String;
    const/4 v4, 0x0

    .line 243
    .local v4, "szCorrelator":Ljava/lang/String;
    const/4 v3, 0x0

    .line 245
    .local v3, "nAgentType":I
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserAlert;-><init>()V

    .line 246
    .local v0, "Alert":Lcom/fmm/dm/eng/parser/XDMParserAlert;
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/fmm/dm/eng/core/XDMWorkspace;)I

    move-result v7

    iput v7, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    .line 247
    iput-object p1, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 249
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetCorrelator()Ljava/lang/String;

    move-result-object v4

    .line 250
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 252
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    .line 254
    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    .line 258
    :cond_0
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .end local v1    # "Item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 259
    .restart local v1    # "Item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    new-instance v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v7}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 260
    iget-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    const/4 v8, 0x0

    iput v8, v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    .line 262
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v3

    .line 264
    const/4 v7, 0x1

    if-ne v3, v7, :cond_2

    .line 266
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetResultReportSourceUri()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 267
    new-instance v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v7}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 268
    iget-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v8, "urn:oma:at:lawmo:1.0:OperationComplete"

    iput-object v8, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 270
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetResultCode()Ljava/lang/String;

    move-result-object v5

    .line 271
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 273
    const-string v5, "0"

    .line 275
    :cond_1
    iget-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    iput-object v8, v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 299
    :goto_0
    invoke-static {v2, v6, v1}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v2

    .line 300
    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserAlert;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 302
    return-object v0

    .line 277
    :cond_2
    const/4 v7, 0x2

    if-ne v3, v7, :cond_4

    .line 279
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetResultReportSourceUri()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 280
    new-instance v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v7}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 281
    iget-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v8, "urn:oma:at:lawmo:1.0:OperationComplete"

    iput-object v8, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 283
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetResultCode()Ljava/lang/String;

    move-result-object v5

    .line 284
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 286
    const-string v5, "0"

    .line 287
    iget-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    iput-object v8, v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    goto :goto_0

    .line 291
    :cond_3
    iget-object v7, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    iput-object v8, v7, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    goto :goto_0

    .line 296
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "nAgentType : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmAgentBuildCmdGetCmdID(Lcom/fmm/dm/eng/core/XDMWorkspace;)I
    .locals 2
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;

    .prologue
    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "cmdid":I
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->cmdID:I

    .end local v0    # "cmdid":I
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->cmdID:I

    .line 39
    .restart local v0    # "cmdid":I
    return v0
.end method

.method public static xdmAgentBuildCmdParseTargetURI(Lcom/fmm/dm/eng/core/XDMWorkspace;)V
    .locals 14
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;

    .prologue
    const/16 v13, 0x50

    const/16 v12, 0x3f

    const/16 v11, 0x3a

    const/16 v10, 0x2f

    const/4 v9, 0x0

    .line 603
    iget-object v6, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    .line 605
    .local v6, "szTarget":Ljava/lang/String;
    const/4 v7, 0x5

    invoke-virtual {v6, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "https"

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_1

    .line 607
    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 609
    .local v5, "szSub":Ljava/lang/String;
    invoke-virtual {v5, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 610
    .local v1, "firstComma":I
    invoke-virtual {v5, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 611
    .local v2, "firstQuestion":I
    if-lez v2, :cond_0

    .line 612
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    .line 616
    :goto_0
    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 617
    .local v3, "firstSlash":I
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v5, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 621
    .local v4, "szPort":Ljava/lang/String;
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->port:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 627
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ws.port"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->port:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 655
    :goto_2
    return-void

    .line 614
    .end local v3    # "firstSlash":I
    .end local v4    # "szPort":Ljava/lang/String;
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    goto :goto_0

    .line 623
    .restart local v3    # "firstSlash":I
    .restart local v4    # "szPort":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 625
    .local v0, "e":Ljava/lang/NumberFormatException;
    iput v13, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->port:I

    goto :goto_1

    .line 631
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v1    # "firstComma":I
    .end local v2    # "firstQuestion":I
    .end local v3    # "firstSlash":I
    .end local v4    # "szPort":Ljava/lang/String;
    .end local v5    # "szSub":Ljava/lang/String;
    :cond_1
    const/4 v7, 0x7

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 633
    .restart local v5    # "szSub":Ljava/lang/String;
    invoke-virtual {v5, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 634
    .restart local v1    # "firstComma":I
    invoke-virtual {v5, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 636
    .restart local v2    # "firstQuestion":I
    if-lez v2, :cond_2

    .line 637
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "http://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    .line 641
    :goto_3
    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 642
    .restart local v3    # "firstSlash":I
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v5, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 647
    .restart local v4    # "szPort":Ljava/lang/String;
    :try_start_1
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->port:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 653
    :goto_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ws.port => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->port:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_2

    .line 639
    .end local v3    # "firstSlash":I
    .end local v4    # "szPort":Ljava/lang/String;
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "http://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    goto :goto_3

    .line 649
    .restart local v3    # "firstSlash":I
    .restart local v4    # "szPort":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 651
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    iput v13, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->port:I

    goto :goto_4
.end method

.method public static xdmAgentBuildCmdReplace(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/core/XDMLinkedList;)Lcom/fmm/dm/eng/parser/XDMParserReplace;
    .locals 6
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p1, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 347
    const/4 v3, 0x0

    .line 349
    .local v3, "replaceCmd":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    const/4 v0, 0x0

    .local v0, "head":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v4, 0x0

    .line 351
    .local v4, "tail":Lcom/fmm/dm/eng/core/XDMList;
    new-instance v3, Lcom/fmm/dm/eng/parser/XDMParserReplace;

    .end local v3    # "replaceCmd":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    invoke-direct {v3}, Lcom/fmm/dm/eng/parser/XDMParserReplace;-><init>()V

    .line 352
    .restart local v3    # "replaceCmd":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/fmm/dm/eng/core/XDMWorkspace;)I

    move-result v5

    iput v5, v3, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    .line 354
    const/4 v5, 0x0

    invoke-static {p1, v5}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 355
    invoke-static {p1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 356
    .local v1, "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :goto_0
    if-eqz v1, :cond_1

    .line 358
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 359
    .local v2, "obj2":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-static {v2, v1}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentDataStDuplItem(Lcom/fmm/dm/eng/parser/XDMParserItem;Lcom/fmm/dm/eng/parser/XDMParserItem;)V

    .line 360
    if-nez v0, :cond_0

    .line 361
    invoke-static {v0, v4, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    .line 364
    :goto_1
    invoke-static {p1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v1, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .restart local v1    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    goto :goto_0

    .line 363
    :cond_0
    invoke-static {v0, v4, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_1

    .line 366
    .end local v2    # "obj2":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :cond_1
    iput-object v0, v3, Lcom/fmm/dm/eng/parser/XDMParserReplace;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 368
    return-object v3
.end method

.method public static xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .locals 13
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p1, "cmdRef"    # I
    .param p2, "szCmd"    # Ljava/lang/String;
    .param p3, "szSource"    # Ljava/lang/String;
    .param p4, "szTarget"    # Ljava/lang/String;
    .param p5, "szData"    # Ljava/lang/String;

    .prologue
    .line 373
    const/4 v6, 0x0

    .line 379
    .local v6, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    new-instance v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .end local v6    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    invoke-direct {v6}, Lcom/fmm/dm/eng/parser/XDMParserStatus;-><init>()V

    .line 381
    .restart local v6    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/fmm/dm/eng/core/XDMWorkspace;)I

    move-result v10

    iput v10, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cmdid:I

    .line 382
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    iput-object v10, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    .line 383
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    .line 384
    iput-object p2, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    .line 385
    move-object/from16 v0, p5

    iput-object v0, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 387
    const/4 v3, 0x0

    .line 388
    .local v3, "head":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v9, 0x0

    .line 390
    .local v9, "tail":Lcom/fmm/dm/eng/core/XDMList;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 392
    move-object/from16 v7, p4

    .line 393
    .local v7, "szBuf":Ljava/lang/String;
    invoke-static {v3, v9, v7}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v3

    .line 394
    iput-object v3, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->targetref:Lcom/fmm/dm/eng/core/XDMList;

    .line 413
    .end local v7    # "szBuf":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 414
    const/4 v9, 0x0

    .line 415
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 417
    move-object/from16 v7, p3

    .line 418
    .restart local v7    # "szBuf":Ljava/lang/String;
    invoke-static {v3, v9, v7}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v3

    .line 419
    iput-object v3, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->sourceref:Lcom/fmm/dm/eng/core/XDMList;

    .line 435
    .end local v7    # "szBuf":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string v10, "407"

    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, "401"

    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_4

    :cond_2
    if-nez p1, :cond_4

    .line 437
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    .line 438
    .local v1, "chal":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    const-string v10, "b64"

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 439
    iget v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    if-nez v10, :cond_e

    .line 441
    const-string v10, "syncml:auth-basic"

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 442
    const-string v10, "XDM_CRED_TYPE_BASIC"

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 462
    :cond_3
    :goto_2
    iput-object v1, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 465
    .end local v1    # "chal":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    :cond_4
    iget-boolean v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendChal:Z

    if-nez v10, :cond_6

    .line 467
    const-string v10, "212"

    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_6

    if-nez p1, :cond_6

    .line 469
    iget v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    .line 471
    iget-object v10, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v10, :cond_5

    .line 473
    iget-object v10, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 474
    const/4 v10, 0x0

    iput-object v10, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 477
    :cond_5
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    .line 478
    .restart local v1    # "chal":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    const-string v10, "b64"

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 479
    const-string v10, "syncml:auth-md5"

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 480
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 481
    .local v2, "encoder":[B
    const/4 v10, 0x0

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 482
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 483
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v2}, Ljava/lang/String;-><init>([B)V

    .line 485
    .local v8, "szRet":Ljava/lang/String;
    invoke-static {v8}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerNonce(Ljava/lang/String;)V

    .line 486
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CRED_TYPE_MD5 WS.serverNextNonce: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/lang/String;

    iget-object v12, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-direct {v11, v12}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Encoded server nonce "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 487
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendChal:Z

    .line 488
    iput-object v1, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 493
    .end local v1    # "chal":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    .end local v2    # "encoder":[B
    .end local v8    # "szRet":Ljava/lang/String;
    :cond_6
    iget v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_7

    const-string v10, "200"

    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_7

    if-nez p1, :cond_7

    .line 495
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    .line 496
    .restart local v1    # "chal":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    const-string v10, "b64"

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 497
    const-string v10, "syncml:auth-MAC"

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 499
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 500
    .restart local v2    # "encoder":[B
    const/4 v10, 0x0

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 501
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 503
    iput-object v1, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 505
    .end local v1    # "chal":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    .end local v2    # "encoder":[B
    :cond_7
    return-object v6

    .line 396
    :cond_8
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->targetRefList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v10, :cond_0

    .line 398
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->targetRefList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 399
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->targetRefList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 400
    .local v4, "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :goto_3
    if-eqz v4, :cond_a

    .line 402
    new-instance v5, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v5}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 403
    .local v5, "obj2":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-static {v5, v4}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplItem(Lcom/fmm/dm/eng/parser/XDMParserItem;Lcom/fmm/dm/eng/parser/XDMParserItem;)V

    .line 404
    if-nez v3, :cond_9

    .line 405
    invoke-static {v3, v9, v5}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v3

    .line 408
    :goto_4
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->targetRefList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v4, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .restart local v4    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    goto :goto_3

    .line 407
    :cond_9
    invoke-static {v3, v9, v5}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_4

    .line 410
    .end local v5    # "obj2":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :cond_a
    iput-object v3, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->targetref:Lcom/fmm/dm/eng/core/XDMList;

    goto/16 :goto_0

    .line 421
    .end local v4    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :cond_b
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sourceRefList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v10, :cond_1

    .line 423
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sourceRefList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 424
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sourceRefList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 425
    .restart local v4    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :goto_5
    if-eqz v4, :cond_d

    .line 427
    if-nez v3, :cond_c

    .line 428
    invoke-static {v3, v9, v4}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v3

    .line 431
    :goto_6
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sourceRefList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v4, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .restart local v4    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    goto :goto_5

    .line 430
    :cond_c
    invoke-static {v3, v9, v4}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_6

    .line 433
    :cond_d
    iput-object v3, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->sourceref:Lcom/fmm/dm/eng/core/XDMList;

    goto/16 :goto_1

    .line 444
    .end local v4    # "obj":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .restart local v1    # "chal":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    :cond_e
    iget v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_f

    .line 446
    const-string v10, "syncml:auth-md5"

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 447
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 448
    .restart local v2    # "encoder":[B
    const/4 v10, 0x0

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 449
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 450
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v2}, Ljava/lang/String;-><init>([B)V

    .line 451
    .restart local v8    # "szRet":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CRED_TYPE_MD5 WS.serverNextNonce: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/lang/String;

    iget-object v12, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-direct {v11, v12}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Encoded server nonce "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 454
    .end local v2    # "encoder":[B
    .end local v8    # "szRet":Ljava/lang/String;
    :cond_f
    iget v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_3

    .line 456
    const-string v10, "syncml:auth-MAC"

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 457
    iget-object v10, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 458
    .restart local v2    # "encoder":[B
    const/4 v10, 0x0

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 459
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 460
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/lang/String;-><init>([B)V

    goto/16 :goto_2
.end method

.method public static xdmAgentBuildCmdSyncHeader(Lcom/fmm/dm/eng/core/XDMWorkspace;)Lcom/fmm/dm/eng/core/XDMWorkspace;
    .locals 11
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 45
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->syncHeader:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    if-nez v0, :cond_6

    .line 48
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    invoke-direct {v0}, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->syncHeader:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    .line 49
    iget-object v9, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->syncHeader:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    .line 51
    .local v9, "sh":Lcom/fmm/dm/eng/parser/XDMParserSyncheader;
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    .line 55
    const-string v0, "1.2"

    iput-object v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szVerdtd:Ljava/lang/String;

    .line 56
    const-string v0, "DM/1.2"

    iput-object v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szVerproto:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    iput-object v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szSessionId:Ljava/lang/String;

    .line 65
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->msgID:I

    iput v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->msgid:I

    .line 67
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    iput-object v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    .line 68
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSourceURI:Ljava/lang/String;

    iput-object v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szSource:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    iput-object v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szLocname:Ljava/lang/String;

    .line 71
    new-instance v8, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v8}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    .line 72
    .local v8, "meta":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->maxMsgSize:I

    iput v0, v8, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxmsgsize:I

    .line 73
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->maxObjSize:I

    iput v0, v8, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxobjsize:I

    .line 75
    iput-object v8, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 89
    .end local v8    # "meta":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    :cond_0
    :goto_0
    iget-object v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_1

    .line 91
    iput-object v5, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 94
    :cond_1
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    if-nez v0, :cond_3

    :cond_2
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    if-eq v0, v2, :cond_5

    .line 98
    :cond_3
    const/4 v10, 0x0

    .line 100
    .local v10, "szTmp":Ljava/lang/String;
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthCredType2String(I)Ljava/lang/String;

    move-result-object v10

    .line 101
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 103
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    .line 105
    const/4 v7, 0x0

    .line 127
    .local v7, "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    :cond_4
    :goto_1
    iput-object v7, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 135
    .end local v7    # "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    .end local v10    # "szTmp":Ljava/lang/String;
    :cond_5
    :goto_2
    return-object p0

    .line 79
    .end local v9    # "sh":Lcom/fmm/dm/eng/parser/XDMParserSyncheader;
    :cond_6
    iget-object v9, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->syncHeader:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    .line 80
    .restart local v9    # "sh":Lcom/fmm/dm/eng/parser/XDMParserSyncheader;
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->msgID:I

    iput v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->msgid:I

    .line 81
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    iget-object v1, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    iput-object v0, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    .line 84
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdParseTargetURI(Lcom/fmm/dm/eng/core/XDMWorkspace;)V

    goto :goto_0

    .line 107
    .restart local v10    # "szTmp":Ljava/lang/String;
    :cond_7
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    .line 109
    new-instance v8, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v8}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    .line 111
    .restart local v8    # "meta":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthCredType2String(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 112
    const-string v0, "b64"

    iput-object v0, v8, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 113
    const/4 v7, 0x0

    .line 114
    .restart local v7    # "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    new-instance v7, Lcom/fmm/dm/eng/parser/XDMParserCred;

    .end local v7    # "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    invoke-direct {v7}, Lcom/fmm/dm/eng/parser/XDMParserCred;-><init>()V

    .line 115
    .restart local v7    # "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    iput-object v8, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 117
    iget v0, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    iget-object v1, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    iget-object v2, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    iget-object v3, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    iget-object v4, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    array-length v4, v4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    .line 118
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cred data = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "credType = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 125
    .end local v7    # "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    .end local v8    # "meta":Lcom/fmm/dm/eng/parser/XDMParserMeta;
    :cond_8
    const/4 v7, 0x0

    .restart local v7    # "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    goto :goto_1

    .line 131
    .end local v7    # "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    :cond_9
    iput-object v5, v9, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    goto :goto_2
.end method

.method public static xdmAgentBuildCmdSynchrousGenericAlertReport(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .locals 7
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p1, "cmdRef"    # I
    .param p2, "szCmd"    # Ljava/lang/String;
    .param p3, "szSource"    # Ljava/lang/String;
    .param p4, "szTarget"    # Ljava/lang/String;
    .param p5, "szData"    # Ljava/lang/String;

    .prologue
    .line 307
    const/4 v3, 0x0

    .line 308
    .local v3, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    const/4 v1, 0x0

    .line 309
    .local v1, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v0, 0x0

    .local v0, "head":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v4, 0x0

    .line 310
    .local v4, "tail":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v2, 0x0

    .line 312
    .local v2, "nAgentType":I
    new-instance v3, Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .end local v3    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    invoke-direct {v3}, Lcom/fmm/dm/eng/parser/XDMParserStatus;-><init>()V

    .line 313
    .restart local v3    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .end local v1    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 315
    .restart local v1    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/fmm/dm/eng/core/XDMWorkspace;)I

    move-result v5

    iput v5, v3, Lcom/fmm/dm/eng/parser/XDMParserStatus;->cmdid:I

    .line 316
    iget-object v5, p0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    iput-object v5, v3, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    .line 317
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    .line 318
    iput-object p2, v3, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    .line 319
    iput-object p5, v3, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 321
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 323
    iput-object p4, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 326
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 328
    iput-object p3, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .line 331
    :cond_1
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v2

    .line 333
    const/4 v5, 0x1

    if-eq v2, v5, :cond_2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_3

    .line 335
    :cond_2
    new-instance v5, Lcom/fmm/dm/eng/parser/XDMParserMeta;

    invoke-direct {v5}, Lcom/fmm/dm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v5, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 336
    iget-object v5, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const-string v6, "urn:oma:at:lawmo:1.0:OperationComplete"

    iput-object v6, v5, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 339
    :cond_3
    invoke-static {v0, v4, v1}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v0

    .line 340
    iput-object v0, v3, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 342
    return-object v3
.end method
