.class public Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;
.super Lcom/fmm/dm/agent/XDMAgent;
.source "XLAWMOSimChange.java"


# static fields
.field private static bSimChangeFlag:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-boolean v0, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->bSimChangeFlag:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    .line 16
    return-void
.end method

.method public static xlawmoGetbSimChange()Z
    .locals 1

    .prologue
    .line 20
    sget-boolean v0, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->bSimChangeFlag:Z

    return v0
.end method

.method public static xlawmoSetbSimChange(Z)V
    .locals 0
    .param p0, "bSimChangeFlag"    # Z

    .prologue
    .line 25
    sput-boolean p0, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->bSimChangeFlag:Z

    .line 26
    return-void
.end method


# virtual methods
.method public xlawmoSimChangeMakeNode()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x2

    .line 30
    const/4 v1, 0x0

    .line 32
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/4 v0, 0x0

    .line 37
    .local v0, "nAclValue":I
    const-string v5, ""

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 39
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xdmAgentGetOM()Lcom/fmm/dm/eng/core/XDMOmTree;

    move-result-object v1

    .line 40
    if-nez v1, :cond_0

    .line 42
    const-string v5, "OM Addr is NULL"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 46
    :cond_0
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetNonGpsSIM()Z

    .line 49
    const/16 v0, 0x8

    .line 50
    const-string v5, "./Ext/OSPS/devInfo"

    invoke-static {v1, v5, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 53
    const-string v3, "./Ext/OSPS/devInfo/SIM"

    .line 54
    .local v3, "szSimChangeRoot":Ljava/lang/String;
    const/16 v0, 0x8

    .line 55
    invoke-static {v1, v3, v0, v6}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 58
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/MNC"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "szSimChangeNode":Ljava/lang/String;
    const/16 v0, 0x8

    .line 60
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetNonGpsSIMMNC()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5, v0, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 63
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/MCC"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 64
    const/16 v0, 0x8

    .line 65
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetNonGpsSIMMCC()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5, v0, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 68
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/MSISDN"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 69
    const/16 v0, 0x8

    .line 70
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetNonGpsSIMMSISDN()Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, "szTempData":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 72
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 73
    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 75
    const-string v5, ""

    invoke-static {v1, v2, v5, v0, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 83
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/date"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 84
    const/16 v0, 0x8

    .line 85
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5, v0, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 88
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/Ext"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 89
    const/16 v0, 0x18

    .line 90
    const-string v5, " "

    invoke-static {v1, v2, v5, v0, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 79
    :cond_2
    invoke-static {v1, v2, v4, v0, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_1
.end method
