.class final Lcom/fmm/dm/agent/amt/XAMTGpsManagerService$1;
.super Lcom/fmm/dm/agent/amt/XAMTGpsListener;
.source "XAMTGpsManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/fmm/dm/agent/amt/XAMTGpsListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveData(Landroid/location/Location;)V
    .locals 11
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 205
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    .line 206
    .local v6, "latitute":D
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    .line 207
    .local v8, "longitude":D
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    .line 208
    .local v2, "Altitute":D
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    .line 209
    .local v1, "Bearing":F
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v4

    .line 210
    .local v4, "Speed":F
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    .line 212
    .local v0, "Accuracy":F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "GPS data: lat="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", lon="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", alt="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", bearing="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", speed="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "accura"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 214
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtRemoveListener()V

    .line 215
    invoke-static {p1}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->xamtGpsSetLocation(Landroid/location/Location;)V

    .line 216
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocation(Z)V

    .line 217
    return-void
.end method

.method public onTimeout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 222
    const-string v1, "GpsLocListener Time Out"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 223
    const-string v1, "location"

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 225
    .local v0, "lm":Landroid/location/LocationManager;
    if-eqz v0, :cond_1

    .line 226
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 229
    :goto_0
    sput-boolean v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getGPSLoc:Z

    .line 231
    sget-boolean v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getNetLoc:Z

    if-nez v1, :cond_0

    .line 233
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtRemoveListener()V

    .line 234
    invoke-static {v2}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocation(Z)V

    .line 236
    :cond_0
    return-void

    .line 228
    :cond_1
    const-string v1, "LocationManager is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method
