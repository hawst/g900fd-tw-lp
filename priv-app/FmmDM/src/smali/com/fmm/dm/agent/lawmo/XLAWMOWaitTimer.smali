.class public Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;
.super Ljava/lang/Object;
.source "XLAWMOWaitTimer.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;
    }
.end annotation


# instance fields
.field private mReActiveWaitTimer:Ljava/util/Timer;


# direct methods
.method constructor <init>(I)V
    .locals 4
    .param p1, "operation"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;

    .line 18
    const/16 v1, 0xf

    if-ne p1, v1, :cond_0

    .line 20
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;

    .line 21
    new-instance v0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;

    invoke-direct {v0, p0}, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;-><init>(Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;)V

    .line 22
    .local v0, "mTask":Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;
    iget-object v1, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 24
    .end local v0    # "mTask":Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;

    return-object p1
.end method


# virtual methods
.method public endWaitTimer(I)V
    .locals 1
    .param p1, "operation"    # I

    .prologue
    .line 28
    const/16 v0, 0xf

    if-ne p1, v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 32
    const-string v0, "mReActiveWaitTimer Cancel"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;

    .line 37
    :cond_0
    return-void
.end method
