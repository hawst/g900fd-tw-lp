.class public Lcom/fmm/dm/agent/amt/XAMTAgent;
.super Lcom/fmm/dm/agent/XDMAgent;
.source "XAMTAgent.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    return-void
.end method

.method public static xamtAgentExecuteOperation()V
    .locals 3

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 211
    .local v0, "nOperation":I
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetOperation()I

    move-result v0

    .line 213
    packed-switch v0, :pswitch_data_0

    .line 231
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not support operation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 216
    :pswitch_0
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 217
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 221
    :pswitch_1
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 222
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 226
    :pswitch_2
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 227
    sget-object v1, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xamtAgentExecuteResultReport(Z)V
    .locals 2
    .param p0, "bRet"    # Z

    .prologue
    const/4 v1, 0x0

    .line 238
    if-eqz p0, :cond_0

    const-string v0, "1200"

    :goto_0
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetResultCode(Ljava/lang/String;)V

    .line 239
    const/16 v0, 0xb

    invoke-static {v0, v1, v1}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 240
    return-void

    .line 238
    :cond_0
    const-string v0, "1452"

    goto :goto_0
.end method

.method public static xamtAgentIsExecNode(Ljava/lang/String;)Z
    .locals 6
    .param p0, "szNodePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 192
    :cond_0
    :goto_0
    return v2

    .line 152
    :cond_1
    const-string v4, "./Ext/OSPS/MobileTraking/Operation/Get/Location"

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 154
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiEvent()I

    move-result v1

    .line 155
    .local v1, "nEvent":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get Noti Event : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 157
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 158
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetResultReportType(I)V

    .line 160
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->getRequestReplacedState()Z

    move-result v4

    if-nez v4, :cond_2

    .line 162
    const-string v4, ""

    invoke-static {v4}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocationRequestor(Ljava/lang/String;)V

    .line 164
    :cond_2
    invoke-static {v2}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->setGetLocationRequestorReplacedState(Z)V

    .line 166
    if-lez v1, :cond_3

    .line 168
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetLocationCount(I)V

    :goto_1
    move v2, v3

    .line 177
    goto :goto_0

    .line 172
    :cond_3
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetLocationCount()I

    move-result v0

    .line 173
    .local v0, "count":I
    add-int/lit8 v0, v0, 0x1

    .line 174
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetLocationCount(I)V

    goto :goto_1

    .line 179
    .end local v0    # "count":I
    .end local v1    # "nEvent":I
    :cond_4
    const-string v4, "./Ext/OSPS/MobileTracking/Operations/Tracking"

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 181
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 182
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetResultReportType(I)V

    move v2, v3

    .line 183
    goto :goto_0

    .line 185
    :cond_5
    const-string v4, "./Ext/OSPS/MobileTracking/Operations/Stop"

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 187
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 188
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetResultReportType(I)V

    move v2, v3

    .line 189
    goto :goto_0
.end method

.method public static xamtAgentIsMobileTrackingPath(Ljava/lang/String;)Z
    .locals 3
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 244
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 254
    :cond_0
    :goto_0
    return v0

    .line 246
    :cond_1
    const-string v2, "./Ext/OSPS/MobileTraking"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 248
    goto :goto_0

    .line 250
    :cond_2
    const-string v2, "./Ext/OSPS/MobileTracking"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 252
    goto :goto_0
.end method

.method public static xamtAgentIsPolicyEndDatePath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 270
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 276
    :cond_0
    :goto_0
    return v0

    .line 272
    :cond_1
    const-string v1, "./Ext/OSPS/MobileTracking/Policy/EndDate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 274
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xamtAgentIsPolicyIntervalPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 281
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 287
    :cond_0
    :goto_0
    return v0

    .line 283
    :cond_1
    const-string v1, "./Ext/OSPS/MobileTracking/Policy/Interval"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xamtAgentIsPolicyStartDatePath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 259
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 265
    :cond_0
    :goto_0
    return v0

    .line 261
    :cond_1
    const-string v1, "./Ext/OSPS/MobileTracking/Policy/StartDate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xamtAgentIsRequestorPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 292
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 298
    :cond_0
    :goto_0
    return v0

    .line 294
    :cond_1
    const-string v1, "./Ext/OSPS/MobileTraking/RequestorName"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xamtAgentMakeNode()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 29
    const-string v2, ""

    .line 30
    .local v2, "szNodeValue":Ljava/lang/String;
    const/16 v0, 0x18

    .line 32
    .local v0, "nAclValue":I
    const-string v3, "xamtAgentMakeNode"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xdmAgentGetOM()Lcom/fmm/dm/eng/core/XDMOmTree;

    move-result-object v1

    .line 35
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    if-nez v1, :cond_0

    .line 37
    const-string v3, "OM Addr is NULL"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 145
    :goto_0
    return-void

    .line 42
    :cond_0
    const-string v3, "./Ext/OSPS/MobileTraking"

    invoke-static {v1, v3, v0, v6}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 45
    const-string v3, "./Ext/OSPS/MobileTracking"

    invoke-static {v1, v3, v0, v6}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 48
    const-string v3, "./Ext/OSPS/MobileTraking/RequestorName"

    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetGetLocationRequestor()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 51
    const-string v3, "./Ext/OSPS/MobileTraking/GPS"

    invoke-static {v1, v3, v0, v6}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 54
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetGpsIsValid()Ljava/lang/String;

    move-result-object v2

    .line 55
    const-string v3, "./Ext/OSPS/MobileTraking/GPS/isValid"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 58
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetGpsLatitude()Ljava/lang/String;

    move-result-object v2

    .line 59
    const-string v3, "./Ext/OSPS/MobileTraking/GPS/latitude"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 62
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetGpsLongitude()Ljava/lang/String;

    move-result-object v2

    .line 63
    const-string v3, "./Ext/OSPS/MobileTraking/GPS/longitude"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 66
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetGpsHorizontalUncertainty()Ljava/lang/String;

    move-result-object v2

    .line 67
    const-string v3, "./Ext/OSPS/MobileTraking/GPS/horizontalUncertainty"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 70
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetGpsVerticalUncertainty()Ljava/lang/String;

    move-result-object v2

    .line 71
    const-string v3, "./Ext/OSPS/MobileTraking/GPS/verticalUncertainty"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 74
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetGpsDate()Ljava/lang/String;

    move-result-object v2

    .line 75
    const-string v3, "./Ext/OSPS/MobileTraking/GPS/Date"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 78
    const-string v3, "./Ext/OSPS/MobileTraking/NonGPS"

    invoke-static {v1, v3, v0, v6}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 82
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetNonGpsValid()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v2, "Y"

    .line 83
    :goto_1
    const-string v3, "./Ext/OSPS/MobileTraking/NonGPS/isValid"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 87
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetNonGpsCellId()Ljava/lang/String;

    move-result-object v2

    .line 88
    const-string v3, "./Ext/OSPS/MobileTraking/NonGPS/CellId"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 92
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetNonGpsLAC()Ljava/lang/String;

    move-result-object v2

    .line 93
    const-string v3, "./Ext/OSPS/MobileTraking/NonGPS/LAC"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 97
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetNonGpsMNC()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 98
    const-string v3, "./Ext/OSPS/MobileTraking/NonGPS/MNC"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 102
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetNonGpsMCC()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 103
    const-string v3, "./Ext/OSPS/MobileTraking/NonGPS/MCC"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 106
    const-string v3, "./Ext/OSPS/MobileTraking/Operation"

    invoke-static {v1, v3, v0, v6}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 109
    const-string v3, "./Ext/OSPS/MobileTracking/Operations"

    invoke-static {v1, v3, v0, v6}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 112
    const-string v3, "./Ext/OSPS/MobileTraking/Operation/Get"

    invoke-static {v1, v3, v0, v5}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 115
    const/4 v0, 0x4

    .line 116
    const-string v3, "./Ext/OSPS/MobileTraking/Operation/Get/Location"

    invoke-static {v1, v3, v0, v5}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 119
    const-string v3, "./Ext/OSPS/MobileTracking/Operations/Stop"

    invoke-static {v1, v3, v0, v5}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 122
    const-string v3, "./Ext/OSPS/MobileTracking/Operations/Tracking"

    invoke-static {v1, v3, v0, v5}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 125
    const/16 v0, 0x8

    .line 126
    const-string v3, "./Ext/OSPS/MobileTracking/Policy"

    invoke-static {v1, v3, v0, v5}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 128
    const/16 v0, 0x18

    .line 130
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyStartDate()Ljava/lang/String;

    move-result-object v2

    .line 131
    const-string v3, "./Ext/OSPS/MobileTracking/Policy/StartDate"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 134
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyEndDate()Ljava/lang/String;

    move-result-object v2

    .line 135
    const-string v3, "./Ext/OSPS/MobileTracking/Policy/EndDate"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 138
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyInterval()Ljava/lang/String;

    move-result-object v2

    .line 139
    const-string v3, "./Ext/OSPS/MobileTracking/Policy/Interval"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 142
    const/16 v0, 0x8

    .line 143
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetMobileTrackingStatus()Ljava/lang/String;

    move-result-object v2

    .line 144
    const-string v3, "./Ext/OSPS/MobileTracking/Status"

    invoke-static {v1, v3, v2, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 82
    :cond_1
    const-string v2, "N"

    goto/16 :goto_1
.end method

.method private static xamtAgentSetOmPath(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V
    .locals 6
    .param p0, "Om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "nAclValue"    # I
    .param p3, "nScope"    # I

    .prologue
    const/4 v2, 0x0

    .line 19
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 21
    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 22
    invoke-static {p0, p1, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 24
    :cond_0
    return-void
.end method

.method public static xamtAgentVerifyOperation(Ljava/lang/String;)V
    .locals 2
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 197
    const-string v0, "./Ext/OSPS/MobileTraking/Operation/Get/Location"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 199
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 205
    :goto_0
    return-void

    .line 203
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "wsMobileTrackingAgentVerifyOperation Not support operation : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method
