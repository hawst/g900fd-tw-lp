.class public Lcom/fmm/dm/agent/XDMAgentHandler;
.super Lcom/fmm/dm/agent/XDMAgent;
.source "XDMAgentHandler.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XUIInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    return-void
.end method

.method public static xdmAgentStringToPcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    .locals 4
    .param p0, "str"    # [C

    .prologue
    .line 705
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    .line 707
    .local v1, "o":Lcom/fmm/dm/eng/parser/XDMParserPcdata;
    const/4 v2, 0x0

    iput v2, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    .line 708
    array-length v2, p0

    iput v2, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 709
    array-length v2, p0

    new-array v2, v2, [C

    iput-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 710
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 711
    iget-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v3, p0, v0

    aput-char v3, v2, v0

    .line 710
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 712
    :cond_0
    return-object v1
.end method


# virtual methods
.method public xdmAgentGetUicOptionDRMultiChoice(Lcom/fmm/dm/eng/core/XDMUicResult;)V
    .locals 12
    .param p1, "pData"    # Lcom/fmm/dm/eng/core/XDMUicResult;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 657
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentGetWorkSpace()Lcom/fmm/dm/eng/core/XDMWorkspace;

    move-result-object v7

    .line 658
    .local v7, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    if-nez v7, :cond_1

    .line 660
    const-string v8, "ws is null"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 700
    :cond_0
    return-void

    .line 664
    :cond_1
    const/4 v5, 0x0

    .line 665
    .local v5, "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    const/4 v4, 0x0

    .line 666
    .local v4, "pStartCharacter":[C
    const/4 v3, 0x0

    .line 668
    .local v3, "pEndCharacter":[C
    const/4 v1, 0x0

    .line 669
    .local v1, "ipStartCharacter":I
    const/4 v0, 0x0

    .line 670
    .local v0, "ipEndCharacter":I
    move-object v5, p1

    .line 672
    iget-object v8, v7, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v8, v8, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v8, v8, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .line 673
    move-object v3, v4

    .line 676
    :goto_0
    array-length v8, v4

    if-ge v1, v8, :cond_0

    .line 678
    array-length v8, v3

    if-ge v0, v8, :cond_0

    .line 680
    aget-char v8, v3, v0

    const/16 v9, 0x2d

    if-eq v8, v9, :cond_2

    aget-char v8, v3, v0

    if-nez v8, :cond_3

    .line 682
    :cond_2
    new-array v6, v11, [C

    aput-char v10, v6, v10

    .line 685
    .local v6, "tmpBuf":[C
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    sub-int v9, v0, v1

    invoke-virtual {v8, v10, v9, v6, v10}, Ljava/lang/String;->getChars(II[CI)V

    .line 686
    invoke-static {v6}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 688
    .local v2, "nIndex":I
    iget-object v8, v5, Lcom/fmm/dm/eng/core/XDMUicResult;->MultiSelected:[I

    aput v11, v8, v2

    .line 690
    move-object v4, v3

    .line 692
    aget-char v8, v4, v1

    if-eqz v8, :cond_0

    .line 695
    add-int/lit8 v1, v1, 0x1

    .line 698
    .end local v2    # "nIndex":I
    .end local v6    # "tmpBuf":[C
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public xdmAgentHdlrAbortSession(I)V
    .locals 3
    .param p1, "nReason"    # I

    .prologue
    .line 647
    const/4 v0, 0x0

    .line 648
    .local v0, "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AbortReason=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 651
    const/16 v1, 0xf5

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/fmm/dm/eng/core/XDMAbortMsgParam;

    move-result-object v0

    .line 652
    const/16 v1, 0x19

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 653
    return-void
.end method

.method public xdmAgentHdlrContinueSession(ILjava/lang/Object;)V
    .locals 15
    .param p1, "nEvent"    # I
    .param p2, "pData"    # Ljava/lang/Object;

    .prologue
    .line 158
    const/4 v5, 0x0

    .line 159
    .local v5, "nRet":I
    const/4 v11, 0x0

    .line 160
    .local v11, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v3, 0x0

    .line 161
    .local v3, "nAgentType":I
    const/4 v6, 0x2

    .line 163
    .local v6, "nSyncMode":I
    const-string v12, ""

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentGetWorkSpace()Lcom/fmm/dm/eng/core/XDMWorkspace;

    move-result-object v11

    .line 165
    if-nez v11, :cond_0

    .line 167
    const-string v12, "!ws WARNING"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 170
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 625
    :cond_1
    :goto_0
    return-void

    .line 174
    :sswitch_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v3

    .line 175
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "XEVENT_DM_START nAgentType : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 176
    const/4 v12, 0x1

    if-ne v3, v12, :cond_2

    .line 178
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgnetHdlrContinueSessionLawmoStart()V

    goto :goto_0

    .line 180
    :cond_2
    const/4 v12, 0x2

    if-ne v3, v12, :cond_3

    .line 182
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgnetHdlrContinueSessionMobileTrackingStart()V

    goto :goto_0

    .line 186
    :cond_3
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgnetHdlrContinueSessionDmStart()V

    goto :goto_0

    .line 193
    :sswitch_1
    const-string v12, "XEVENT_DM_CONTINUE"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 194
    if-nez v11, :cond_4

    .line 196
    const-string v12, "ws XEVENT_DM_CONTINUE WARNING"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 199
    :cond_4
    sget-object v12, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->procState:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 203
    :try_start_0
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    if-nez v12, :cond_5

    .line 205
    new-instance v12, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v12}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    .line 207
    :cond_5
    iget-object v12, p0, Lcom/fmm/dm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    iget-object v13, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpReceiveData(Ljava/io/ByteArrayOutputStream;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 216
    :goto_1
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    if-nez v12, :cond_6

    .line 218
    const/16 v12, 0x9

    invoke-virtual {p0, v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 212
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 213
    const/4 v5, -0x4

    goto :goto_1

    .line 221
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_6
    iget-object v12, p0, Lcom/fmm/dm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    invoke-virtual {v12}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetCurHMACData()Lcom/fmm/dm/eng/core/XDMHmacData;

    move-result-object v12

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->recvHmacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    .line 223
    const/4 v12, -0x6

    if-ne v5, v12, :cond_7

    .line 226
    const/16 v12, 0xf9

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/fmm/dm/eng/core/XDMAbortMsgParam;

    move-result-object v7

    .line 227
    .local v7, "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    const/16 v12, 0x19

    const/4 v13, 0x0

    invoke-static {v12, v7, v13}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 230
    .end local v7    # "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    :cond_7
    if-eqz v5, :cond_8

    .line 232
    const/16 v12, 0x1c

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 236
    :cond_8
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStartMgmtSession()I

    move-result v5

    .line 237
    const/4 v12, -0x1

    if-ne v5, v12, :cond_9

    .line 239
    const-string v12, "XDM_RET_FAILED"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 240
    const/16 v12, 0x9

    invoke-virtual {p0, v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto/16 :goto_0

    .line 243
    :cond_9
    const/4 v12, -0x6

    if-ne v5, v12, :cond_a

    .line 245
    const-string v12, "XDM_RET_AUTH_MAX_ERROR"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 246
    const/4 v12, 0x3

    invoke-virtual {p0, v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto/16 :goto_0

    .line 249
    :cond_a
    const/4 v12, 0x3

    if-ne v5, v12, :cond_b

    .line 251
    const-string v12, "XDM_RET_ALERT_SESSION_ABORT"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 252
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrDestroySession()V

    goto/16 :goto_0

    .line 255
    :cond_b
    const/16 v12, 0x8

    if-ne v5, v12, :cond_f

    .line 257
    const-string v12, "XDM_RET_FINISH"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 258
    const-string v12, "no action command finish session"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 260
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_d

    .line 263
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetResultReportType()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_c

    .line 265
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 266
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 289
    :cond_c
    :goto_2
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrDestroySession()V

    goto/16 :goto_0

    .line 269
    :cond_d
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_c

    .line 272
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetResultReportType()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_c

    .line 275
    const-string v12, "./Ext/OSPS/MobileTraking/Operation/Get/Location"

    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetResultReportSourceUri()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 277
    const-string v12, "1200"

    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetResultCode()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    const-string v12, "Y"

    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetGpsIsValid()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 279
    const/4 v12, 0x3

    invoke-static {v12}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 283
    :cond_e
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 284
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    goto :goto_2

    .line 291
    :cond_f
    const/16 v12, 0x9

    if-ne v5, v12, :cond_10

    .line 293
    const-string v12, "XDM_RET_ABORT"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 295
    const/16 v12, 0xf4

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/fmm/dm/eng/core/XDMAbortMsgParam;

    move-result-object v7

    .line 296
    .restart local v7    # "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    const/16 v12, 0x19

    const/4 v13, 0x0

    invoke-static {v12, v7, v13}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 298
    .end local v7    # "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    :cond_10
    const/4 v12, -0x5

    if-ne v5, v12, :cond_11

    .line 300
    const-string v12, "XDM_RET_PAUSED_BECAUSE_UIC_COMMAND"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 301
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    invoke-virtual {p0, v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrlUicSendEvent(Lcom/fmm/dm/eng/core/XDMUicOption;)V

    goto/16 :goto_0

    .line 303
    :cond_11
    const/4 v12, 0x2

    if-ne v5, v12, :cond_12

    .line 305
    const-string v12, "XDM_RET_EXEC_ALTERNATIVE"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 306
    const-string v12, "Connect to the Contents Server"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 308
    const/16 v12, 0x1a

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 310
    :cond_12
    const/4 v12, 0x4

    if-ne v5, v12, :cond_13

    .line 312
    const-string v12, "XDM_RET_CHANGED_PROFILE"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 313
    const/4 v12, 0x1

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDB;->xdbSetChangedProtocol(Z)V

    .line 314
    const/16 v12, 0xb

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 316
    :cond_13
    const/4 v12, -0x7

    if-ne v5, v12, :cond_1

    .line 318
    const/16 v12, 0x15

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 326
    :sswitch_2
    const-string v12, "XEVENT_UIC_RESPONSE"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 327
    if-eqz v11, :cond_14

    if-nez p2, :cond_15

    .line 329
    :cond_14
    const-string v12, "XEVENT_UIC_RESPONSE WARNING!!!!!!!!!!!"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_15
    move-object/from16 v8, p2

    .line 332
    check-cast v8, Lcom/fmm/dm/eng/core/XDMUicResult;

    .line 334
    .local v8, "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1c

    .line 336
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_17

    .line 338
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    .line 517
    :goto_3
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    if-eqz v12, :cond_16

    .line 519
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    invoke-static {v12}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/fmm/dm/eng/core/XDMUicOption;)Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 520
    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 523
    :cond_16
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStartMgmtSession()I

    move-result v5

    .line 525
    const/4 v12, -0x1

    if-ne v5, v12, :cond_30

    .line 527
    const/16 v12, 0x9

    invoke-virtual {p0, v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto/16 :goto_0

    .line 340
    :cond_17
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    const/16 v13, 0x10

    if-ne v12, v13, :cond_1b

    .line 342
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget v12, v12, Lcom/fmm/dm/eng/core/XDMText;->len:I

    if-eqz v12, :cond_1a

    .line 344
    const-string v12, "1"

    iget-object v13, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_18

    .line 346
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto :goto_3

    .line 348
    :cond_18
    const-string v12, "0"

    iget-object v13, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_19

    .line 351
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TIMEOUT:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto :goto_3

    .line 356
    :cond_19
    const-string v12, "____UIC_TYPE_CONFIRM__&&__UIC_RESULT_TIMEOUT________"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 357
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_FALSE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto :goto_3

    .line 363
    :cond_1a
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_CANCELED:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto :goto_3

    .line 370
    :cond_1b
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_FALSE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto :goto_3

    .line 373
    :cond_1c
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x3

    if-ne v12, v13, :cond_21

    .line 375
    const/4 v2, 0x0

    .line 376
    .local v2, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v1, 0x0

    .local v1, "h":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v10, 0x0

    .line 378
    .local v10, "t":Lcom/fmm/dm/eng/core/XDMList;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "______ UIC_TYPE_INPUT _______INPUT text :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " UIC Result :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 380
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    if-nez v12, :cond_1d

    .line 382
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 384
    .restart local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 385
    invoke-static {v1, v10, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 386
    iput-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    .line 387
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 390
    :cond_1d
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1e

    .line 392
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_CANCELED:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 394
    :cond_1e
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    const/16 v13, 0x10

    if-ne v12, v13, :cond_20

    .line 396
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget v12, v12, Lcom/fmm/dm/eng/core/XDMText;->len:I

    if-eqz v12, :cond_1f

    const-string v12, "0"

    iget-object v13, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_1f

    .line 398
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 399
    .restart local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v13, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    iput-object v13, v12, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 400
    iget-object v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->text:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 401
    invoke-static {v1, v10, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 402
    iput-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    .line 403
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 408
    :cond_1f
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TIMEOUT:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 414
    :cond_20
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_FALSE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 417
    .end local v1    # "h":Lcom/fmm/dm/eng/core/XDMList;
    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v10    # "t":Lcom/fmm/dm/eng/core/XDMList;
    :cond_21
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x4

    if-eq v12, v13, :cond_22

    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x5

    if-ne v12, v13, :cond_2f

    .line 419
    :cond_22
    const/4 v1, 0x0

    .restart local v1    # "h":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v10, 0x0

    .line 421
    .restart local v10    # "t":Lcom/fmm/dm/eng/core/XDMList;
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_24

    .line 423
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 424
    .restart local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const-string v9, ""

    .line 426
    .local v9, "szDataText":Ljava/lang/String;
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->SingleSelected:I

    if-lez v12, :cond_23

    .line 427
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->SingleSelected:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 429
    :cond_23
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 430
    invoke-static {v1, v10, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 431
    iput-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    .line 432
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 434
    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v9    # "szDataText":Ljava/lang/String;
    :cond_24
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x5

    if-ne v12, v13, :cond_28

    .line 436
    const/4 v4, 0x0

    .line 438
    .local v4, "nCount":I
    const/4 v4, 0x0

    :goto_4
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->MenuNumbers:I

    if-ge v4, v12, :cond_26

    .line 440
    iget-object v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v12, v12, v4

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 441
    iget-object v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v12, v12, v4

    const/4 v13, 0x1

    if-ne v12, v13, :cond_25

    .line 443
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 444
    .restart local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const-string v9, ""

    .line 446
    .restart local v9    # "szDataText":Ljava/lang/String;
    add-int/lit8 v12, v4, 0x1

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 447
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 448
    invoke-static {v1, v10, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 438
    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v9    # "szDataText":Ljava/lang/String;
    :cond_25
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 451
    :cond_26
    if-nez v1, :cond_27

    .line 453
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 454
    .restart local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    invoke-static {v1, v10, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 457
    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :cond_27
    iput-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    .line 458
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 460
    .end local v4    # "nCount":I
    :cond_28
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_29

    .line 462
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_CANCELED:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 464
    :cond_29
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->result:I

    const/16 v13, 0x10

    if-ne v12, v13, :cond_2e

    .line 466
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget v12, v12, Lcom/fmm/dm/eng/core/XDMText;->len:I

    if-eqz v12, :cond_2d

    const-string v12, "0"

    iget-object v13, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v13, v13, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_2d

    .line 468
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_2a

    .line 470
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 472
    .restart local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMUicOption;->defaultResponse:Lcom/fmm/dm/eng/core/XDMText;

    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 473
    invoke-static {v1, v10, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 474
    iput-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    .line 475
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 480
    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :cond_2a
    const/4 v4, 0x0

    .line 482
    .restart local v4    # "nCount":I
    invoke-virtual {p0, v8}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentGetUicOptionDRMultiChoice(Lcom/fmm/dm/eng/core/XDMUicResult;)V

    .line 484
    const/4 v4, 0x0

    :goto_5
    iget v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->MenuNumbers:I

    if-ge v4, v12, :cond_2c

    .line 486
    iget-object v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v12, v12, v4

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 488
    iget-object v12, v8, Lcom/fmm/dm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v12, v12, v4

    const/4 v13, 0x1

    if-ne v12, v13, :cond_2b

    .line 490
    new-instance v2, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 491
    .restart local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const-string v9, ""

    .line 492
    .restart local v9    # "szDataText":Ljava/lang/String;
    add-int/lit8 v12, v4, 0x1

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 493
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 494
    invoke-static {v1, v10, v2}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppend(Lcom/fmm/dm/eng/core/XDMList;Lcom/fmm/dm/eng/core/XDMList;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMList;

    move-result-object v1

    .line 484
    .end local v2    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v9    # "szDataText":Ljava/lang/String;
    :cond_2b
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 497
    :cond_2c
    iput-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    .line 498
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 504
    .end local v4    # "nCount":I
    :cond_2d
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TIMEOUT:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 509
    :cond_2e
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_FALSE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 514
    .end local v1    # "h":Lcom/fmm/dm/eng/core/XDMList;
    .end local v10    # "t":Lcom/fmm/dm/eng/core/XDMList;
    :cond_2f
    sget-object v12, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_NONE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_3

    .line 530
    :cond_30
    const/4 v12, 0x3

    if-ne v5, v12, :cond_31

    .line 532
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrDestroySession()V

    goto/16 :goto_0

    .line 535
    :cond_31
    const/16 v12, 0x9

    if-ne v5, v12, :cond_32

    .line 537
    const/4 v7, 0x0

    .line 539
    .restart local v7    # "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    const/16 v12, 0xf4

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/fmm/dm/eng/core/XDMAbortMsgParam;

    move-result-object v7

    .line 540
    const/16 v12, 0x19

    const/4 v13, 0x0

    invoke-static {v12, v7, v13}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 542
    .end local v7    # "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    :cond_32
    const/16 v12, 0x8

    if-ne v5, v12, :cond_33

    .line 544
    const-string v12, "no action command finish session"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 545
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrDestroySession()V

    goto/16 :goto_0

    .line 548
    :cond_33
    const/4 v12, -0x6

    if-eq v5, v12, :cond_1

    .line 552
    const/4 v12, 0x4

    if-ne v5, v12, :cond_34

    .line 554
    const/4 v12, 0x1

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDB;->xdbSetChangedProtocol(Z)V

    .line 555
    const/16 v12, 0xb

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 557
    :cond_34
    const/4 v12, -0x7

    if-ne v5, v12, :cond_1

    .line 559
    const/16 v12, 0x15

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 565
    .end local v8    # "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    :sswitch_3
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v3

    .line 566
    const/4 v12, 0x1

    if-ne v3, v12, :cond_36

    .line 568
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteOperation()V

    .line 569
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetResultReportType()I

    move-result v6

    .line 577
    :cond_35
    :goto_6
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentGetPendingStatus()Z

    move-result v12

    if-eqz v12, :cond_37

    .line 579
    const-string v12, "XDM_TASK_RETRY"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 580
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentClose()I

    goto/16 :goto_0

    .line 571
    :cond_36
    const/4 v12, 0x2

    if-ne v3, v12, :cond_35

    .line 573
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentExecuteOperation()V

    .line 574
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetResultReportType()I

    move-result v6

    goto :goto_6

    .line 584
    :cond_37
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v12

    if-eqz v12, :cond_39

    .line 586
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbSetBackUpServerUrl()V

    .line 593
    :goto_7
    const/4 v12, 0x0

    const/16 v13, 0xab

    invoke-static {v12, v13}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 595
    const/4 v12, 0x1

    if-eq v6, v12, :cond_3a

    .line 597
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpResetSessionSaveState(I)V

    .line 598
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 601
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    .line 608
    :goto_8
    sget-boolean v12, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    if-eqz v12, :cond_38

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xlawmoGetbSimChange()Z

    move-result v12

    if-eqz v12, :cond_38

    .line 610
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xlawmoSetbSimChange(Z)V

    .line 611
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetSimChangeAlert(Z)V

    .line 614
    :cond_38
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUnlockAlert(Z)V

    .line 615
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetEmergencyModeChangeAlert(Z)V

    .line 616
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetReActiveAlert(Z)V

    .line 617
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetLocationProcess(Z)V

    .line 618
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentClose()I

    goto/16 :goto_0

    .line 590
    :cond_39
    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpCookieClear()V

    goto :goto_7

    .line 605
    :cond_3a
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    goto :goto_8

    .line 170
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x18 -> :sswitch_1
        0x1a -> :sswitch_3
        0x66 -> :sswitch_2
    .end sparse-switch
.end method

.method public xdmAgentHdlrDestroySession()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 641
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentSetSyncMode(I)Z

    .line 642
    const/16 v0, 0x1a

    invoke-static {v0, v1, v1}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 643
    return-void
.end method

.method public xdmAgentHdlrlUicSendEvent(Lcom/fmm/dm/eng/core/XDMUicOption;)V
    .locals 3
    .param p1, "pUicOption"    # Lcom/fmm/dm/eng/core/XDMUicOption;

    .prologue
    .line 629
    const/4 v0, 0x0

    .line 631
    .local v0, "pUicOptionDest":Lcom/fmm/dm/eng/core/XDMUicOption;
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 633
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateUicOption()Lcom/fmm/dm/eng/core/XDMUicOption;

    move-result-object v0

    .line 634
    invoke-static {v0, p1}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCopyUicOption(Ljava/lang/Object;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMUicOption;

    move-result-object v0

    .line 636
    const/16 v1, 0x65

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 637
    return-void
.end method

.method public xdmAgnetHdlrContinueSessionDmStart()V
    .locals 5

    .prologue
    const/16 v3, 0x9

    const/4 v4, 0x0

    .line 39
    const-string v2, ""

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentGetWorkSpace()Lcom/fmm/dm/eng/core/XDMWorkspace;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 47
    const-string v2, "xdbGetChangedProtocol, do not create new package"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 68
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/fmm/dm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 77
    .local v1, "nRet":I
    :goto_0
    if-eqz v1, :cond_4

    .line 79
    const/16 v2, 0x15

    invoke-static {v2, v4, v4}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 93
    :cond_1
    :goto_1
    return-void

    .line 51
    .end local v1    # "nRet":I
    :cond_2
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStartSession()I

    move-result v1

    .line 52
    .restart local v1    # "nRet":I
    if-eqz v1, :cond_3

    .line 54
    invoke-virtual {p0, v3}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto :goto_1

    .line 58
    :cond_3
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentCreatePackage()I

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    invoke-virtual {p0, v3}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto :goto_1

    .line 70
    .end local v1    # "nRet":I
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 74
    const/4 v1, -0x7

    .restart local v1    # "nRet":I
    goto :goto_0

    .line 83
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_4
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentSendPackage()I

    move-result v1

    .line 84
    if-nez v1, :cond_5

    .line 85
    const/16 v2, 0x66

    invoke-static {v4, v2}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 87
    :cond_5
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 89
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbSetChangedProtocol(Z)V

    .line 90
    const/16 v2, 0x1a

    invoke-static {v2, v4, v4}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 91
    const/16 v2, 0xb

    invoke-static {v2, v4, v4}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public xdmAgnetHdlrContinueSessionLawmoStart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 98
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 99
    const/4 v0, 0x0

    .line 101
    .local v0, "nRet":I
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_2

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 103
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStartGeneralAlert()I

    move-result v0

    .line 104
    const/4 v1, -0x7

    if-ne v0, v1, :cond_0

    .line 106
    const/16 v1, 0x15

    invoke-static {v1, v3, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 124
    :goto_0
    return-void

    .line 111
    :cond_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 113
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    .line 117
    :cond_1
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetResultReportType(I)V

    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgnetHdlrContinueSessionDmStart()V

    goto :goto_0
.end method

.method public xdmAgnetHdlrContinueSessionMobileTrackingStart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 129
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 130
    const/4 v0, 0x0

    .line 132
    .local v0, "nRet":I
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetOperation()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 134
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentStartGeneralAlert()I

    move-result v0

    .line 135
    const/4 v1, -0x7

    if-ne v0, v1, :cond_0

    .line 137
    const/16 v1, 0x15

    invoke-static {v1, v3, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 154
    :goto_0
    return-void

    .line 142
    :cond_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 144
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    .line 147
    :cond_1
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetResultReportType(I)V

    goto :goto_0

    .line 152
    :cond_2
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgnetHdlrContinueSessionDmStart()V

    goto :goto_0
.end method
