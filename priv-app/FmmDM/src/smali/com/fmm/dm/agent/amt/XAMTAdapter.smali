.class public Lcom/fmm/dm/agent/amt/XAMTAdapter;
.super Landroid/app/Activity;
.source "XAMTAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XCommonInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;


# static fields
.field private static mContext:Landroid/content/Context;

.field private static mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

.field private static mGetLocationRequestor:Ljava/lang/String;

.field public static mLocationProcessing:Z

.field private static mRequestorReplacedState:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    const-string v0, ""

    sput-object v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGetLocationRequestor:Ljava/lang/String;

    .line 27
    sput-boolean v1, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mRequestorReplacedState:Z

    .line 29
    sput-boolean v1, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mLocationProcessing:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static getRequestReplacedState()Z
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mRequestReplacedState : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mRequestorReplacedState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 65
    sget-boolean v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mRequestorReplacedState:Z

    return v0
.end method

.method public static setGetLocationRequestorReplacedState(Z)V
    .locals 2
    .param p0, "b"    # Z

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mRequestReplacedState : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mRequestorReplacedState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 59
    sput-boolean p0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mRequestorReplacedState:Z

    .line 60
    return-void
.end method

.method public static xamtAdpGetGetLocationRequestor()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mGetGetLocationRequestor : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGetLocationRequestor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 43
    sget-object v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGetLocationRequestor:Ljava/lang/String;

    return-object v0
.end method

.method public static xamtAdpGetGpsAccuracy()D
    .locals 2

    .prologue
    .line 109
    sget-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAccuracy:D

    return-wide v0
.end method

.method public static xamtAdpGetGpsLatitude()D
    .locals 2

    .prologue
    .line 85
    sget-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLatitude:D

    return-wide v0
.end method

.method public static xamtAdpGetGpsLongitude()D
    .locals 2

    .prologue
    .line 97
    sget-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLongitude:D

    return-wide v0
.end method

.method public static xamtAdpGetLocationProcess()Z
    .locals 2

    .prologue
    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mLocationProcessing : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mLocationProcessing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 332
    sget-boolean v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mLocationProcessing:Z

    return v0
.end method

.method public static xamtAdpGetNonGpsCellId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 175
    sget-object v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->g_szCellId:Ljava/lang/String;

    .line 176
    .local v0, "szNonGpsCellId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    const-string v0, "0"

    .line 178
    :cond_0
    return-object v0
.end method

.method public static xamtAdpGetNonGpsLAC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    sget-object v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->g_szLac:Ljava/lang/String;

    .line 161
    .local v0, "szNonGpsLAC":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    const-string v0, "0"

    .line 163
    :cond_0
    return-object v0
.end method

.method public static xamtAdpGetNonGpsMCC()I
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mcc:I

    return v0
.end method

.method public static xamtAdpGetNonGpsMNC()I
    .locals 1

    .prologue
    .line 148
    sget v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mnc:I

    return v0
.end method

.method public static xamtAdpGetNonGpsValid()Z
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    if-eqz v0, :cond_0

    .line 122
    sget-object v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    invoke-virtual {v0}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->xamtGpsGetNonLocation()Z

    move-result v0

    .line 124
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xamtAdpOerationGetLocation()Z
    .locals 2

    .prologue
    .line 232
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 233
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 235
    .local v0, "bAMTStatus":Z
    if-eqz v0, :cond_0

    .line 237
    const-string v1, "StartGPSTracking"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 238
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpStartGPSTracking()V

    .line 246
    :goto_0
    return v0

    .line 242
    :cond_0
    const/16 v1, 0xa

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 243
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentExecuteResultReport(Z)V

    goto :goto_0
.end method

.method public static xamtAdpSetContext(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    sput-object p0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mContext:Landroid/content/Context;

    .line 36
    new-instance v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    invoke-direct {v0}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;-><init>()V

    sput-object v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    .line 38
    return-void
.end method

.method public static xamtAdpSetGetLocation(Z)V
    .locals 9
    .param p0, "bGPSVaild"    # Z

    .prologue
    const/4 v8, 0x1

    .line 184
    const-string v3, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    .line 186
    .local v3, "szDateFormat":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bGPSVaild = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 188
    if-eqz p0, :cond_0

    .line 190
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetGpsLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    .line 191
    .local v4, "szLatitude":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetGpsLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v5

    .line 192
    .local v5, "szLongitude":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetGpsAccuracy()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    .line 194
    .local v1, "szAccuracy":Ljava/lang/String;
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 195
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v6, "GMT+0"

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 196
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 197
    .local v2, "szDate":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Latitud = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Longitudude = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Accuracy = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Date = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 200
    invoke-static {v8}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsIsValid(Z)V

    .line 201
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsLatitude(Ljava/lang/String;)V

    .line 202
    invoke-static {v5}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsLongitude(Ljava/lang/String;)V

    .line 203
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsHorizontalUncertainty(Ljava/lang/String;)V

    .line 204
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsVerticalUncertainty(Ljava/lang/String;)V

    .line 205
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsDate(Ljava/lang/String;)V

    .line 207
    const-string v6, "Save DB Success"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 226
    .end local v1    # "szAccuracy":Ljava/lang/String;
    .end local v4    # "szLatitude":Ljava/lang/String;
    .end local v5    # "szLongitude":Ljava/lang/String;
    :goto_0
    const/16 v6, 0xa

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 227
    invoke-static {v8}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentExecuteResultReport(Z)V

    .line 228
    return-void

    .line 211
    .end local v0    # "dateFormat":Ljava/text/SimpleDateFormat;
    .end local v2    # "szDate":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 212
    .restart local v0    # "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v6, "GMT+0"

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 213
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 216
    .restart local v2    # "szDate":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsIsValid(Z)V

    .line 217
    const-string v6, "0"

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsLatitude(Ljava/lang/String;)V

    .line 218
    const-string v6, "0"

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsLongitude(Ljava/lang/String;)V

    .line 219
    const-string v6, "0"

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsHorizontalUncertainty(Ljava/lang/String;)V

    .line 220
    const-string v6, "0"

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsVerticalUncertainty(Ljava/lang/String;)V

    .line 221
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetGpsDate(Ljava/lang/String;)V

    .line 223
    const-string v6, "bGPSVaild = false Save DB Success"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xamtAdpSetGetLocationRequestor(Ljava/lang/String;)V
    .locals 2
    .param p0, "requestor"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestor : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 49
    sput-object p0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGetLocationRequestor:Ljava/lang/String;

    .line 50
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->setGetLocationRequestorReplacedState(Z)V

    .line 54
    :cond_0
    return-void
.end method

.method public static xamtAdpSetLocationProcess(Z)V
    .locals 2
    .param p0, "bProcess"    # Z

    .prologue
    .line 325
    sput-boolean p0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mLocationProcessing:Z

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mLocationProcessing : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mLocationProcessing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method public static xamtAdpStartGPSTracking()V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    if-eqz v0, :cond_0

    .line 73
    sget-object v0, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mGPSManager:Lcom/fmm/dm/agent/amt/XAMTGpsManager;

    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->xamtGpsStartTracking(Landroid/content/Context;)V

    .line 75
    :cond_0
    return-void
.end method

.method public static xamtAdpSyncOerationStop()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 297
    const-string v2, ""

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 298
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 299
    .local v0, "bAMTStatus":Z
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 300
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 302
    if-eqz v0, :cond_0

    .line 305
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.dsm.DM_MOBILE_TRACKING_STOP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 306
    .local v1, "intent":Landroid/content/Intent;
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 307
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_1

    .line 309
    sget-object v2, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 315
    :goto_0
    const-string v2, "Send Broadcast : XCOMMON_INTENT_MOBILE_TRACKING_STOP"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 320
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return v0

    .line 313
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v2, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method public static xamtAdpSyncOerationTracking()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 251
    const-string v5, ""

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 252
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v0

    .line 253
    .local v0, "bAMTStatus":Z
    invoke-static {v6}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    .line 254
    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 256
    if-eqz v0, :cond_3

    .line 258
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyInterval()Ljava/lang/String;

    move-result-object v3

    .line 259
    .local v3, "szInterval":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyStartDate()Ljava/lang/String;

    move-result-object v4

    .line 260
    .local v4, "szStartDate":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyEndDate()Ljava/lang/String;

    move-result-object v2

    .line 263
    .local v2, "szEndDate":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 264
    const-string v3, ""

    .line 266
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 267
    const-string v4, ""

    .line 269
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 270
    const-string v2, ""

    .line 272
    :cond_2
    const-string v5, "StartGPSTracking"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 275
    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.intent.action.dsm.DM_MOBILE_TRACKING_START"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 276
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "starttime"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const-string v5, "stoptime"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const-string v5, "interval"

    invoke-virtual {v1, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    const/16 v5, 0x20

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 280
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-ge v5, v6, :cond_4

    .line 282
    sget-object v5, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 288
    :goto_0
    const-string v5, "Send Broadcast : XCOMMON_INTENT_MOBILE_TRACKING_START"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 292
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "szEndDate":Ljava/lang/String;
    .end local v3    # "szInterval":Ljava/lang/String;
    .end local v4    # "szStartDate":Ljava/lang/String;
    :cond_3
    return v0

    .line 286
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "szEndDate":Ljava/lang/String;
    .restart local v3    # "szInterval":Ljava/lang/String;
    .restart local v4    # "szStartDate":Ljava/lang/String;
    :cond_4
    sget-object v5, Lcom/fmm/dm/agent/amt/XAMTAdapter;->mContext:Landroid/content/Context;

    sget-object v6, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v5, v1, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method
