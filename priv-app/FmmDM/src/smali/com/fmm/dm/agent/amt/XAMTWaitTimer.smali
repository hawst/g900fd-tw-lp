.class public Lcom/fmm/dm/agent/amt/XAMTWaitTimer;
.super Ljava/lang/Object;
.source "XAMTWaitTimer.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XTPInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/agent/amt/XAMTWaitTimer$XAMTDelayGPSProviderTimerTask;
    }
.end annotation


# instance fields
.field private mWaitTimer:Ljava/util/Timer;


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->mWaitTimer:Ljava/util/Timer;

    .line 18
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->mWaitTimer:Ljava/util/Timer;

    .line 19
    new-instance v0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer$XAMTDelayGPSProviderTimerTask;

    invoke-direct {v0, p0}, Lcom/fmm/dm/agent/amt/XAMTWaitTimer$XAMTDelayGPSProviderTimerTask;-><init>(Lcom/fmm/dm/agent/amt/XAMTWaitTimer;)V

    .line 20
    .local v0, "mTask":Lcom/fmm/dm/agent/amt/XAMTWaitTimer$XAMTDelayGPSProviderTimerTask;
    iget-object v1, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->mWaitTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 21
    return-void
.end method

.method static synthetic access$002(Lcom/fmm/dm/agent/amt/XAMTWaitTimer;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/agent/amt/XAMTWaitTimer;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->mWaitTimer:Ljava/util/Timer;

    return-object p1
.end method


# virtual methods
.method public endWaitTimer()V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->mWaitTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->mWaitTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->mWaitTimer:Ljava/util/Timer;

    .line 30
    :cond_0
    return-void
.end method
