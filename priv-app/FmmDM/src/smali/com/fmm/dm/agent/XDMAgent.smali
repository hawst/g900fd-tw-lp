.class public Lcom/fmm/dm/agent/XDMAgent;
.super Ljava/lang/Object;
.source "XDMAgent.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XEventInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Lcom/fmm/dm/interfaces/XTPInterface;
.implements Lcom/fmm/dm/interfaces/XUICInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/agent/XDMAgent$1;
    }
.end annotation


# static fields
.field private static final DEFAULT_NONCE:Ljava/lang/String; = "SamSungNextNonce="

.field private static g_AccName:Ljava/lang/String;

.field public static g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

.field private static m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

.field private static m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

.field private static m_bPendingStatus:Z

.field private static m_nConnectRetryCount:I

.field private static m_nDMSync:I

.field private static pAccName:Ljava/lang/String;


# instance fields
.field private final PACKAGE_SIZE_GAP:I

.field public m_AddCmd:Lcom/fmm/dm/eng/parser/XDMParserAdd;

.field public m_AgentHandler:Lcom/fmm/dm/agent/XDMAgentHandler;

.field public m_Alert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

.field public m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

.field public m_CopyCmd:Lcom/fmm/dm/eng/parser/XDMParserCopy;

.field public m_DeleteCmd:Lcom/fmm/dm/eng/parser/XDMParserDelete;

.field public m_Exec:Lcom/fmm/dm/eng/parser/XDMParserExec;

.field public m_Get:Lcom/fmm/dm/eng/parser/XDMParserGet;

.field public m_Header:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

.field public m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

.field public m_ReplaceCmd:Lcom/fmm/dm/eng/parser/XDMParserReplace;

.field public m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

.field public m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

.field public m_bInProgresscmd:Z

.field public m_szCmd:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    .line 99
    sput v1, Lcom/fmm/dm/agent/XDMAgent;->m_nDMSync:I

    .line 101
    sput-boolean v1, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 103
    sput v1, Lcom/fmm/dm/agent/XDMAgent;->m_nConnectRetryCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const/16 v0, 0x80

    iput v0, p0, Lcom/fmm/dm/agent/XDMAgent;->PACKAGE_SIZE_GAP:I

    .line 110
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/fmm/dm/tp/XTPAdapter;

    invoke-direct {v0}, Lcom/fmm/dm/tp/XTPAdapter;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    .line 114
    :cond_0
    return-void
.end method

.method private xdmAgentCheckNonce(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "szNonce"    # Ljava/lang/String;

    .prologue
    .line 1638
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, p1

    .line 1650
    .end local p1    # "szNonce":Ljava/lang/String;
    .local v1, "szNonce":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 1644
    .end local v1    # "szNonce":Ljava/lang/Object;
    .restart local p1    # "szNonce":Ljava/lang/String;
    :cond_0
    const-string p1, "SamSungNextNonce="

    .line 1647
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v0

    .line 1648
    .local v0, "encoder1":[B
    new-instance p1, Ljava/lang/String;

    .end local p1    # "szNonce":Ljava/lang/String;
    invoke-direct {p1, v0}, Ljava/lang/String;-><init>([B)V

    .restart local p1    # "szNonce":Ljava/lang/String;
    move-object v1, p1

    .line 1650
    .restart local v1    # "szNonce":Ljava/lang/Object;
    goto :goto_0
.end method

.method public static xdmAgentClose()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 246
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 248
    .local v0, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inDMSync = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/fmm/dm/agent/XDMAgent;->m_nDMSync:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 249
    sget v1, Lcom/fmm/dm/agent/XDMAgent;->m_nDMSync:I

    if-lez v1, :cond_1

    .line 251
    if-eqz v0, :cond_0

    .line 253
    sget-boolean v1, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    if-eqz v1, :cond_2

    .line 255
    const-string v1, "Pending Status don\'t save"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 256
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsEnd(Lcom/fmm/dm/eng/core/XDMOmVfs;)V

    .line 263
    :goto_0
    invoke-virtual {v0}, Lcom/fmm/dm/eng/core/XDMWorkspace;->xdmFreeWorkSpace()V

    .line 264
    sput-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 267
    :cond_0
    sput-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 268
    invoke-static {v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 270
    :cond_1
    return v3

    .line 260
    :cond_2
    const-string v1, "workspace save"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 261
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmEnd(Lcom/fmm/dm/eng/core/XDMOmTree;)I

    goto :goto_0
.end method

.method private xdmAgentCmdExecLawmo(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParserExec;Lcom/fmm/dm/eng/parser/XDMParserItem;)V
    .locals 14
    .param p1, "DmWorkspace"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p2, "exec"    # Lcom/fmm/dm/eng/parser/XDMParserExec;
    .param p3, "DmParserItem"    # Lcom/fmm/dm/eng/parser/XDMParserItem;

    .prologue
    .line 6076
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetResultReportSourceUri(Ljava/lang/String;)V

    .line 6079
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetResultReportType()I

    move-result v1

    if-nez v1, :cond_3

    .line 6082
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v13

    .line 6083
    .local v13, "bAMTStatus":Z
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6085
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 6087
    const-string v6, "1200"

    .line 6100
    .local v6, "szGenericCode":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p2

    iget v2, v0, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v3, "Exec"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdSynchrousGenericAlertReport(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .line 6101
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    iget-object v2, p0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6109
    .end local v6    # "szGenericCode":Ljava/lang/String;
    .end local v13    # "bAMTStatus":Z
    :goto_1
    return-void

    .line 6089
    .restart local v13    # "bAMTStatus":Z
    :cond_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_1

    .line 6091
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpGetConnectionCheckResultCode()Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "szGenericCode":Ljava/lang/String;
    goto :goto_0

    .line 6095
    .end local v6    # "szGenericCode":Ljava/lang/String;
    :cond_1
    if-eqz v13, :cond_2

    .line 6096
    const-string v6, "1200"

    .restart local v6    # "szGenericCode":Ljava/lang/String;
    goto :goto_0

    .line 6098
    .end local v6    # "szGenericCode":Ljava/lang/String;
    :cond_2
    const-string v6, "1452"

    .restart local v6    # "szGenericCode":Ljava/lang/String;
    goto :goto_0

    .line 6106
    .end local v6    # "szGenericCode":Ljava/lang/String;
    .end local v13    # "bAMTStatus":Z
    :cond_3
    move-object/from16 v0, p2

    iget v8, v0, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v9, "Exec"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v12, "202"

    move-object v7, p1

    invoke-static/range {v7 .. v12}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .line 6107
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    iget-object v2, p0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private xdmAgentCmdExecMobileTracking(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParserExec;Lcom/fmm/dm/eng/parser/XDMParserItem;)V
    .locals 17
    .param p1, "DmWorkspace"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p2, "exec"    # Lcom/fmm/dm/eng/parser/XDMParserExec;
    .param p3, "DmParserItem"    # Lcom/fmm/dm/eng/parser/XDMParserItem;

    .prologue
    .line 6114
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetResultReportSourceUri(Ljava/lang/String;)V

    .line 6117
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetResultReportType()I

    move-result v1

    if-nez v1, :cond_3

    .line 6120
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v13

    .line 6121
    .local v13, "bAMTStatus":Z
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6123
    if-eqz v13, :cond_2

    .line 6125
    const-string v6, "1200"

    .line 6127
    .local v6, "szGenericCode":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetDBOperation()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 6129
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyInterval()Ljava/lang/String;

    move-result-object v15

    .line 6130
    .local v15, "szInterval":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyStartDate()Ljava/lang/String;

    move-result-object v16

    .line 6131
    .local v16, "szStartDate":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetPolicyEndDate()Ljava/lang/String;

    move-result-object v14

    .line 6133
    .local v14, "szEndDate":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6135
    :cond_0
    const-string v6, "1400"

    .line 6143
    .end local v14    # "szEndDate":Ljava/lang/String;
    .end local v15    # "szInterval":Ljava/lang/String;
    .end local v16    # "szStartDate":Ljava/lang/String;
    :cond_1
    :goto_0
    move-object/from16 v0, p2

    iget v2, v0, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v3, "Exec"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdSynchrousGenericAlertReport(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .line 6144
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6152
    .end local v6    # "szGenericCode":Ljava/lang/String;
    .end local v13    # "bAMTStatus":Z
    :goto_1
    return-void

    .line 6141
    .restart local v13    # "bAMTStatus":Z
    :cond_2
    const-string v6, "1452"

    .restart local v6    # "szGenericCode":Ljava/lang/String;
    goto :goto_0

    .line 6149
    .end local v6    # "szGenericCode":Ljava/lang/String;
    .end local v13    # "bAMTStatus":Z
    :cond_3
    move-object/from16 v0, p2

    iget v8, v0, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v9, "Exec"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v12, "202"

    move-object/from16 v7, p1

    invoke-static/range {v7 .. v12}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .line 6150
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    invoke-static {v1, v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static xdmAgentGetOM()Lcom/fmm/dm/eng/core/XDMOmTree;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    if-nez v0, :cond_0

    .line 136
    const-string v0, "dm_ws is NULL"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 137
    const/4 v0, 0x0

    .line 140
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    goto :goto_0
.end method

.method public static xdmAgentGetPendingStatus()Z
    .locals 1

    .prologue
    .line 8999
    sget-boolean v0, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    return v0
.end method

.method public static xdmAgentGetSyncMode()I
    .locals 3

    .prologue
    .line 145
    const/4 v0, 0x0

    .line 147
    .local v0, "nSync":I
    sget v0, Lcom/fmm/dm/agent/XDMAgent;->m_nDMSync:I

    .line 148
    if-eqz v0, :cond_0

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nSync = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 151
    :cond_0
    return v0
.end method

.method public static xdmAgentInit()I
    .locals 1

    .prologue
    .line 232
    new-instance v0, Lcom/fmm/dm/eng/core/XDMWorkspace;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMWorkspace;-><init>()V

    sput-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 233
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    if-nez v0, :cond_0

    .line 234
    const/4 v0, -0x1

    .line 241
    :goto_0
    return v0

    .line 236
    :cond_0
    const-string v0, "./SyncML/DMAcc"

    sput-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 239
    new-instance v0, Lcom/fmm/dm/eng/core/XDMAccXNode;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMAccXNode;-><init>()V

    sput-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    .line 241
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmAgentInitParser(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParser;)I
    .locals 1
    .param p0, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p1, "p"    # Lcom/fmm/dm/eng/parser/XDMParser;

    .prologue
    .line 118
    invoke-virtual {p1, p1, p0}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParseInit(Lcom/fmm/dm/eng/parser/XDMParser;Ljava/lang/Object;)V

    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public static xdmAgentMakeBootStrapNode()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2407
    sget-object v3, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 2408
    .local v3, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v1, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 2410
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/4 v2, 0x0

    .line 2412
    .local v2, "ret":I
    const-string v4, "start xdmAgentMakeBootStrapNode"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2413
    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmInit(Lcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 2415
    const/4 v2, -0x1

    .line 2438
    .end local v2    # "ret":I
    :goto_0
    return v2

    .line 2417
    .restart local v2    # "ret":I
    :cond_0
    const-string v4, "*"

    invoke-static {v1, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmSetServerId(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)I

    .line 2419
    const/16 v0, 0x9

    .line 2420
    .local v0, "aclValue":I
    const-string v4, "."

    invoke-static {v1, v4, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 2422
    const/16 v0, 0x1b

    .line 2423
    const-string v4, "./SyncML"

    invoke-static {v1, v4, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 2426
    const/16 v0, 0x1b

    .line 2427
    const-string v4, "./DMAcc"

    invoke-static {v1, v4, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 2435
    const/16 v0, 0x1b

    .line 2436
    const-string v4, "./SyncML/Con"

    invoke-static {v1, v4, v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public static xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V
    .locals 4
    .param p0, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "aclValue"    # I
    .param p3, "scope"    # I

    .prologue
    .line 1790
    const/4 v0, 0x0

    .line 1791
    .local v0, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    const/4 v1, 0x0

    .line 1792
    .local v1, "item":Lcom/fmm/dm/eng/core/XDMOmList;
    const/4 v2, 0x0

    .line 1794
    .local v2, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v2

    .line 1795
    if-eqz v2, :cond_1

    .line 1798
    if-eqz p2, :cond_0

    .line 1800
    iget-object v1, v2, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 1801
    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 1802
    .restart local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    iput p2, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    .line 1808
    :goto_0
    iput p3, v2, Lcom/fmm/dm/eng/core/XDMVnode;->scope:I

    .line 1814
    :goto_1
    return-void

    .line 1806
    :cond_0
    const-string v3, "ACL is XDM_OMACL_NONE"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 1812
    :cond_1
    const-string v3, "Not Exist"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmAgentParsingWbxml([B)I
    .locals 4
    .param p0, "buf"    # [B

    .prologue
    const/4 v3, 0x0

    .line 275
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 276
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v1, 0x0

    .line 278
    .local v1, "res":I
    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextMsg:Z

    .line 279
    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 280
    new-instance v0, Lcom/fmm/dm/eng/parser/XDMParser;

    invoke-direct {v0, p0}, Lcom/fmm/dm/eng/parser/XDMParser;-><init>([B)V

    .line 281
    .local v0, "p":Lcom/fmm/dm/eng/parser/XDMParser;
    invoke-static {v2, v0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentInitParser(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParser;)I

    .line 282
    invoke-virtual {v0}, Lcom/fmm/dm/eng/parser/XDMParser;->xdmParParse()I

    move-result v1

    .line 283
    if-eqz v1, :cond_0

    .line 285
    const/4 v3, -0x2

    .line 287
    :cond_0
    return v3
.end method

.method public static xdmAgentSaveBootstrapDateToFFS(Lcom/fmm/dm/db/file/XDBProfileInfo;)V
    .locals 2
    .param p0, "pProfileInfo"    # Lcom/fmm/dm/db/file/XDBProfileInfo;

    .prologue
    .line 3330
    const/4 v0, 0x0

    .line 3332
    .local v0, "Index":I
    iget-object v1, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetActiveProfileIndexByServerID(Ljava/lang/String;)I

    move-result v0

    .line 3333
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileInfo(Lcom/fmm/dm/db/file/XDBProfileInfo;)Z

    .line 3334
    iget-object v1, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileName(ILjava/lang/String;)V

    .line 3336
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    invoke-static {v0, v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetDMXNodeInfo(ILjava/lang/Object;)V

    .line 3337
    return-void
.end method

.method public static xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 1969
    sget-object v8, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 1972
    .local v8, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v2, 0x0

    .line 1974
    .local v2, "nLen":I
    if-nez p1, :cond_1

    .line 1999
    :cond_0
    :goto_0
    return-void

    .line 1979
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v2, v0

    .line 1980
    if-gtz v2, :cond_2

    .line 1982
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmDeleteImplicit(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    .line 1985
    :cond_2
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v4, p1

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 1987
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-static {v0, p0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v7

    .line 1988
    .local v7, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v7, :cond_0

    .line 1990
    iget-object v0, v7, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v0, :cond_3

    .line 1991
    iget-object v0, v7, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 1993
    :cond_3
    new-instance v6, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v6}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 1994
    .local v6, "list":Lcom/fmm/dm/eng/core/XDMOmList;
    const-string v0, "text/plain"

    iput-object v0, v6, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 1995
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 1996
    iput-object v6, v7, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 1997
    const/4 v0, 0x4

    iput v0, v7, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    goto :goto_0
.end method

.method public static xdmAgentSetOMAccBin(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 6
    .param p0, "omt"    # Ljava/lang/Object;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szData"    # Ljava/lang/String;
    .param p3, "size"    # I
    .param p4, "aclValue"    # I
    .param p5, "scope"    # I

    .prologue
    .line 1884
    move-object v1, p0

    check-cast v1, Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 1888
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    invoke-static {v1, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    .line 1890
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v0, :cond_1

    .line 1892
    invoke-static {p1, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMBin(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1893
    invoke-static {v1, p1, p4, p5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1910
    :cond_0
    :goto_0
    return-void

    .line 1897
    :cond_1
    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v3, v4, [C

    .line 1898
    .local v3, "temp":[C
    const/4 v4, 0x0

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v1, p1, v4, v3, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1900
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    .line 1902
    .local v2, "szTmp":Ljava/lang/String;
    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-eq p3, v4, :cond_2

    .line 1904
    invoke-static {p1, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMBin(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 1906
    :cond_2
    invoke-virtual {v2, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 1908
    invoke-static {p1, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMBin(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6
    .param p0, "omt"    # Ljava/lang/Object;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szData"    # Ljava/lang/String;
    .param p3, "aclValue"    # I
    .param p4, "scope"    # I

    .prologue
    .line 1818
    move-object v1, p0

    check-cast v1, Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 1822
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    invoke-static {v1, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    .line 1823
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1825
    const-string p2, ""

    .line 1828
    :cond_0
    if-nez v0, :cond_1

    .line 1830
    invoke-static {p1, p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1831
    invoke-static {v1, p1, p3, p4}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1850
    :goto_0
    return-void

    .line 1835
    :cond_1
    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v3, v4, [C

    .line 1836
    .local v3, "temp":[C
    const/4 v4, 0x0

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v1, p1, v4, v3, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1838
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    .line 1839
    .local v2, "szTmp":Ljava/lang/String;
    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v4, v5, :cond_3

    .line 1841
    invoke-static {p1, p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1848
    :cond_2
    :goto_1
    const/4 v3, 0x0

    .line 1850
    goto :goto_0

    .line 1843
    :cond_3
    invoke-virtual {v2, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_2

    .line 1845
    invoke-static {p1, p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static xdmAgentSetOMBin(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "szData"    # Ljava/lang/String;
    .param p2, "datasize"    # I

    .prologue
    .line 1914
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 1917
    .local v7, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v0, v7, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    const/4 v3, 0x0

    move-object v1, p0

    move v2, p2

    move-object v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 1918
    iget-object v0, v7, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-static {v0, p0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v6

    .line 1920
    .local v6, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v6, :cond_1

    .line 1922
    iget-object v0, v6, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v0, :cond_0

    .line 1924
    iget-object v0, v6, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 1926
    :cond_0
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 1927
    const/4 v0, 0x2

    iput v0, v6, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 1929
    :cond_1
    return-void
.end method

.method public static xdmAgentSetSyncMode(I)Z
    .locals 2
    .param p0, "nSync"    # I

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nSync = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 157
    sput p0, Lcom/fmm/dm/agent/XDMAgent;->m_nDMSync:I

    .line 159
    const/4 v0, 0x1

    return v0
.end method

.method public static xdmAgentSetXNodePath(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "szTarget"    # Ljava/lang/String;
    .param p2, "bTndsFlag"    # Z

    .prologue
    .line 8793
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "target["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "parent["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 8795
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    .line 8797
    new-instance v1, Lcom/fmm/dm/eng/core/XDMAccXNode;

    invoke-direct {v1}, Lcom/fmm/dm/eng/core/XDMAccXNode;-><init>()V

    sput-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    .line 8799
    const-string v1, "."

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "./DMAcc"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 8801
    :cond_0
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8802
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8803
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8806
    :cond_1
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8807
    .local v0, "szTmpBuf":Ljava/lang/String;
    const-string v1, "/ToConRef"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8809
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 8811
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 8812
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 8813
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 8816
    :cond_2
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8817
    const-string v1, "/AppAddr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8818
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 8820
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8821
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8822
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8825
    :cond_3
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8826
    const-string v1, "/Port"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8827
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 8829
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 8830
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 8831
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 8834
    :cond_4
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8835
    const-string v1, "/AppAuth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8837
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 8840
    const-string v1, "ClientSide"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 8842
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 8843
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 8844
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 8954
    :cond_5
    :goto_0
    return-void

    .line 8848
    :cond_6
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 8849
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 8850
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    goto :goto_0

    .line 8856
    .end local v0    # "szTmpBuf":Ljava/lang/String;
    :cond_7
    if-eqz p2, :cond_e

    .line 8858
    const-string v1, "."

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "./DMAcc"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_9

    .line 8860
    :cond_8
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8861
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8862
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8865
    :cond_9
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8866
    .restart local v0    # "szTmpBuf":Ljava/lang/String;
    const-string v1, "/ToConRef"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8867
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_a

    .line 8869
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 8870
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 8871
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 8874
    :cond_a
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8875
    const-string v1, "/AppAddr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8876
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_b

    .line 8878
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8879
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8880
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8883
    :cond_b
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8884
    const-string v1, "/Port"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8885
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_c

    .line 8887
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 8888
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 8889
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 8892
    :cond_c
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8893
    const-string v1, "/AppAuth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8894
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 8896
    const-string v1, "ClientSide"

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_d

    .line 8898
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 8899
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 8900
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    goto/16 :goto_0

    .line 8904
    :cond_d
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 8905
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 8906
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    goto/16 :goto_0

    .line 8912
    .end local v0    # "szTmpBuf":Ljava/lang/String;
    :cond_e
    const-string v1, "."

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "./DMAcc"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_10

    .line 8914
    :cond_f
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8917
    :cond_10
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8918
    .restart local v0    # "szTmpBuf":Ljava/lang/String;
    const-string v1, "/ToConRef"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8919
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_11

    .line 8921
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 8924
    :cond_11
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8925
    const-string v1, "/AppAddr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8926
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_12

    .line 8928
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8931
    :cond_12
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 8932
    const-string v1, "/Port"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8933
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_13

    .line 8935
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 8938
    :cond_13
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 8939
    const-string v1, "/AppAuth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8940
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 8943
    const-string v1, "ClientSide"

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_14

    .line 8945
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    goto/16 :goto_0

    .line 8949
    :cond_14
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static xdmAgentTpSetRetryCount(I)V
    .locals 0
    .param p0, "nCnt"    # I

    .prologue
    .line 8994
    sput p0, Lcom/fmm/dm/agent/XDMAgent;->m_nConnectRetryCount:I

    .line 8995
    return-void
.end method

.method public static xdmAgentV12GetAppAddrPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 3341
    const/4 v0, 0x0

    .line 3343
    .local v0, "szOut":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3345
    const-string v1, "m_szAppAddr is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3346
    const/4 v1, 0x0

    .line 3354
    :goto_0
    return-object v1

    .line 3349
    :cond_0
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 3350
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3352
    invoke-virtual {v0, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v1, v0

    .line 3354
    goto :goto_0
.end method

.method public static xdmAgentV12GetAppAddrPortPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 3359
    const/4 v0, 0x0

    .line 3361
    .local v0, "szOut":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3363
    const-string v1, "m_szAppAddrPort is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3364
    const/4 v1, 0x0

    .line 3372
    :goto_0
    return-object v1

    .line 3367
    :cond_0
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 3368
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3370
    invoke-virtual {v0, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v1, v0

    .line 3372
    goto :goto_0
.end method

.method public static xdmAgentV12GetClientAppAuthPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "pStr"    # Ljava/lang/String;

    .prologue
    .line 3377
    const/4 v0, 0x0

    .line 3379
    .local v0, "szOut":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3381
    const-string v1, "m_szClientAppAuth is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3382
    const/4 v1, 0x0

    .line 3390
    :goto_0
    return-object v1

    .line 3385
    :cond_0
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 3386
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3388
    invoke-virtual {v0, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v1, v0

    .line 3390
    goto :goto_0
.end method

.method public static xdmAgentV12GetServerAppAuthPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 3395
    const/4 v0, 0x0

    .line 3397
    .local v0, "szOut":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3399
    const-string v1, "m_szServerAppAuth is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3400
    const/4 v1, 0x0

    .line 3408
    :goto_0
    return-object v1

    .line 3403
    :cond_0
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 3404
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3406
    invoke-virtual {v0, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v1, v0

    .line 3408
    goto :goto_0
.end method

.method public static xdmAgentV12GetToConRefPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 3413
    const/4 v0, 0x0

    .line 3415
    .local v0, "szOut":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3417
    const-string v1, "m_szToConRef is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3418
    const/4 v1, 0x0

    .line 3426
    :goto_0
    return-object v1

    .line 3421
    :cond_0
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 3422
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3424
    invoke-virtual {v0, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v1, v0

    .line 3426
    goto :goto_0
.end method

.method public static xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V
    .locals 6
    .param p0, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "aclValue"    # I
    .param p3, "scope"    # I

    .prologue
    const/4 v2, 0x0

    .line 1772
    invoke-static {p0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1774
    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 1775
    invoke-static {p0, p1, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1777
    :cond_0
    return-void
.end method


# virtual methods
.method public xdmAgentAppendAclItem(Lcom/fmm/dm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/fmm/dm/eng/core/XDMOmList;
    .locals 11
    .param p1, "acllist"    # Lcom/fmm/dm/eng/core/XDMOmList;
    .param p2, "szAclValue"    # Ljava/lang/String;
    .param p3, "aclflag"    # I

    .prologue
    .line 5153
    move-object v4, p2

    .line 5156
    .local v4, "szData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 5158
    .local v1, "buf":[C
    const/4 v3, 0x0

    .line 5160
    .local v3, "found":Z
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    new-array v1, v7, [C

    .line 5162
    const-string v7, "*"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 5163
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    const/16 v8, 0x2b

    invoke-static {v7, v8, v1}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v4

    .line 5169
    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 5172
    move-object v2, p1

    .line 5173
    .local v2, "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    :goto_1
    if-eqz v2, :cond_2

    iget-object v7, v2, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v7, :cond_2

    .line 5175
    iget-object v0, v2, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 5177
    .local v0, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_0

    .line 5179
    const/4 v3, 0x1

    .line 5180
    iget v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    or-int/2addr v7, p3

    iput v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    .line 5183
    :cond_0
    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    goto :goto_1

    .line 5166
    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    .end local v2    # "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    :cond_1
    const/4 v7, 0x0

    const/16 v8, 0x2a

    aput-char v8, v1, v7

    .line 5167
    const/4 v7, 0x1

    const/4 v8, 0x0

    aput-char v8, v1, v7

    goto :goto_0

    .line 5186
    .restart local v2    # "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    :cond_2
    if-nez v3, :cond_3

    .line 5188
    const/16 v7, 0x28

    new-array v6, v7, [C

    .line 5189
    .local v6, "tmp1":[C
    new-instance v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMOmAcl;-><init>()V

    .line 5191
    .restart local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "*"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 5192
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x27

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v6, v10}, Ljava/lang/String;->getChars(II[CI)V

    .line 5198
    :goto_2
    invoke-static {v6}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    .line 5200
    iget v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    or-int/2addr v7, p3

    iput v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    .line 5201
    new-instance v5, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v5}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 5202
    .local v5, "tmp":Lcom/fmm/dm/eng/core/XDMOmList;
    iput-object v0, v5, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 5203
    const/4 v7, 0x0

    iput-object v7, v5, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 5204
    invoke-static {p1, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsAppendList(Lcom/fmm/dm/eng/core/XDMOmList;Lcom/fmm/dm/eng/core/XDMOmList;)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object p1

    .line 5206
    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    .end local v5    # "tmp":Lcom/fmm/dm/eng/core/XDMOmList;
    .end local v6    # "tmp1":[C
    :cond_3
    const/4 v1, 0x0

    .line 5207
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    new-array v1, v7, [C

    .line 5208
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    const/16 v8, 0x2b

    invoke-static {v7, v8, v1}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 5195
    .restart local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    .restart local v6    # "tmp1":[C
    :cond_4
    const/4 v7, 0x0

    const/16 v8, 0x2a

    aput-char v8, v6, v7

    .line 5196
    const/4 v7, 0x1

    const/4 v8, 0x0

    aput-char v8, v6, v7

    goto :goto_2

    .line 5210
    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    .end local v2    # "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    .end local v6    # "tmp1":[C
    :cond_5
    return-object p1
.end method

.method public xdmAgentClientInitPackage(Lcom/fmm/dm/eng/core/XDMEncoder;)I
    .locals 7
    .param p1, "e"    # Lcom/fmm/dm/eng/core/XDMEncoder;

    .prologue
    const/4 v3, -0x1

    const/4 v6, -0x4

    const/4 v4, 0x0

    .line 2003
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 2008
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const-string v5, ""

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2009
    invoke-virtual {p0, p1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageStatus(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v1

    .line 2010
    .local v1, "res":I
    if-eqz v1, :cond_1

    .line 2012
    if-ne v1, v6, :cond_0

    .line 2014
    iput-boolean v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 2076
    :goto_0
    return v3

    .line 2018
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed(%d)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2023
    :cond_1
    invoke-virtual {p0, p1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageResults(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v1

    .line 2024
    if-eqz v1, :cond_3

    .line 2026
    if-ne v1, v6, :cond_2

    .line 2028
    iput-boolean v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 2032
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed(%d)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2037
    :cond_3
    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->appId:I

    invoke-static {v5}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbGetNotiEvent(I)I

    move-result v0

    .line 2038
    .local v0, "nNotiEvent":I
    if-lez v0, :cond_4

    .line 2040
    const-string v5, "1200"

    invoke-virtual {p0, p1, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v1

    .line 2047
    :goto_1
    if-eqz v1, :cond_6

    .line 2049
    if-ne v1, v6, :cond_5

    .line 2051
    iput-boolean v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 2044
    :cond_4
    const-string v5, "1201"

    invoke-virtual {p0, p1, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 2055
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed(%d)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2060
    :cond_6
    invoke-virtual {p0, p1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageDevInfo(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v1

    .line 2061
    if-eqz v1, :cond_8

    .line 2063
    if-ne v1, v6, :cond_7

    .line 2065
    iput-boolean v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 2069
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed(%d)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2074
    :cond_8
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    move v3, v4

    .line 2076
    goto/16 :goto_0
.end method

.method public xdmAgentCmdAdd(Lcom/fmm/dm/eng/parser/XDMParserAdd;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I
    .locals 33
    .param p1, "add"    # Lcom/fmm/dm/eng/parser/XDMParserAdd;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .prologue
    .line 6472
    sget-object v3, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 6473
    .local v3, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v14, 0x0

    .line 6474
    .local v14, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/16 v23, 0x0

    .line 6475
    .local v23, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v0, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    move-object/from16 v18, v0

    .line 6476
    .local v18, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/16 v24, 0x0

    .line 6477
    .local v24, "szBuf":Ljava/lang/String;
    const/16 v30, 0x0

    .line 6478
    .local v30, "szType":Ljava/lang/String;
    const/16 v13, 0xc

    .line 6480
    .local v13, "format":I
    const/4 v12, 0x0

    .line 6481
    .local v12, "cur":Lcom/fmm/dm/eng/core/XDMList;
    const/16 v4, 0x100

    new-array v0, v4, [C

    move-object/from16 v32, v0

    .line 6482
    .local v32, "tmpbuf":[C
    const/16 v27, 0x0

    .line 6484
    .local v27, "szNodename":Ljava/lang/String;
    const/4 v9, 0x0

    .line 6485
    .local v9, "bufsize":I
    const/16 v22, 0x0

    .line 6487
    .local v22, "res":I
    const/16 v25, 0x0

    .line 6488
    .local v25, "szInbox":Ljava/lang/String;
    const/4 v15, 0x0

    .line 6490
    .local v15, "nFileId":I
    const/16 v28, 0x0

    .line 6491
    .local v28, "szOutBuf":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v21

    .line 6494
    .local v21, "process":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdTNDS()I

    move-result v15

    .line 6496
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 6497
    :cond_0
    :goto_0
    if-eqz v12, :cond_42

    .line 6499
    const/16 v30, 0x0

    .line 6500
    const/16 v13, 0xc

    .line 6501
    iget-object v14, v12, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v14    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v14, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 6503
    .restart local v14    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v4, :cond_2

    .line 6505
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v4, :cond_1

    .line 6506
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 6526
    :cond_1
    :goto_1
    iget v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    .line 6528
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 6530
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6536
    :goto_2
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6537
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6538
    goto :goto_0

    .line 6510
    :cond_2
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v4, :cond_1

    .line 6512
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 6514
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 6515
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v5, v5, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 6518
    :cond_3
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 6520
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 6521
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v5, v5, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    goto :goto_1

    .line 6534
    :cond_4
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_2

    .line 6540
    :cond_5
    if-nez v21, :cond_8

    .line 6542
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_6

    .line 6544
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6547
    :cond_6
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 6549
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6556
    :goto_3
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6557
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6558
    goto/16 :goto_0

    .line 6553
    :cond_7
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_3

    .line 6562
    :cond_8
    if-eqz p2, :cond_10

    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    if-eq v4, v5, :cond_10

    .line 6564
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    if-eqz v4, :cond_c

    .line 6567
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-virtual {v4, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 6569
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 6571
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6578
    :goto_4
    sget-object v4, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    .line 6579
    const/4 v4, 0x0

    iput-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 6618
    :goto_5
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6619
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6620
    goto/16 :goto_0

    .line 6575
    :cond_9
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_4

    .line 6583
    :cond_a
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 6585
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "216"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_5

    .line 6589
    :cond_b
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "216"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_5

    .line 6595
    :cond_c
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    if-ne v4, v5, :cond_e

    .line 6597
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 6599
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_5

    .line 6603
    :cond_d
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_5

    .line 6608
    :cond_e
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 6610
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "216"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto/16 :goto_5

    .line 6614
    :cond_f
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "216"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto/16 :goto_5

    .line 6623
    :cond_10
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 6625
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "403"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6626
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6627
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6629
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 6631
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 6636
    :cond_11
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6641
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_14

    .line 6643
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v17

    .line 6644
    .local v17, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v17, :cond_15

    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v4, :cond_15

    iget-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v4, :cond_15

    .line 6646
    const/16 v4, 0xa

    invoke-static {v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFGetMOPath(I)Ljava/lang/String;

    move-result-object v25

    .line 6647
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 6649
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6650
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6651
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6655
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 6657
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 6664
    :cond_12
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    .line 6665
    .local v29, "szTmp":Ljava/lang/String;
    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_13

    .line 6668
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6669
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6670
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6674
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 6676
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 6684
    :cond_13
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6685
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6686
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6687
    const-string v4, "node already Existed[418]"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6691
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 6693
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 6703
    .end local v17    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .end local v29    # "szTmp":Ljava/lang/String;
    :cond_14
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 6706
    invoke-static/range {v32 .. v32}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v26

    .line 6707
    .local v26, "szName":Ljava/lang/String;
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v26

    invoke-static {v0, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetXNodePath(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 6720
    .end local v26    # "szName":Ljava/lang/String;
    :cond_15
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 6721
    invoke-static/range {v32 .. v32}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v27

    .line 6723
    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v17

    .line 6724
    .restart local v17    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v17, :cond_17

    .line 6728
    const/4 v11, 0x0

    .line 6730
    .local v11, "bResultImplicitAdd":Z
    const/16 v4, 0x1b

    const/4 v5, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-static {v0, v1, v4, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmProcessCmdImplicitAdd(Ljava/lang/Object;Ljava/lang/String;II)Z

    move-result v11

    .line 6732
    if-nez v11, :cond_0

    .line 6734
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_16

    .line 6736
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6739
    :cond_16
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6740
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6741
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6743
    const-string v4, "Node depth is over 15  Command failed500"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 6745
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 6747
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 6754
    .end local v11    # "bResultImplicitAdd":Z
    :cond_17
    const/4 v4, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v4

    if-nez v4, :cond_19

    .line 6757
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_18

    .line 6758
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6760
    :cond_18
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "425"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6761
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6762
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6766
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 6768
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 6775
    :cond_19
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v4, :cond_1b

    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1b

    const-string v4, "node"

    iget-object v5, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v5, v5, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_1b

    .line 6777
    const/16 v24, 0x0

    .line 6778
    const/4 v9, 0x0

    .line 6779
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6780
    const/4 v13, 0x6

    .line 6781
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1a

    .line 6783
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v0, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 6873
    :cond_1a
    :goto_6
    iget-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-eqz v4, :cond_2d

    .line 6876
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2d

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2d

    .line 6878
    const-string v4, "application/vnd.syncml.dmtnds+xml"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 6880
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v15, v4}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 6881
    const/16 v24, 0x0

    .line 6882
    const/16 v30, 0x0

    .line 6883
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v4, :cond_28

    .line 6885
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6886
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6887
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6888
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 6889
    move-object/from16 v0, v18

    invoke-static {v15, v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNodeFromFile(ILcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v22

    .line 6891
    if-lez v22, :cond_27

    .line 6893
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6894
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6895
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6896
    goto/16 :goto_0

    .line 6786
    :cond_1b
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v4, :cond_23

    .line 6788
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v4, :cond_1c

    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v4, :cond_1c

    .line 6790
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v24

    .line 6791
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 6793
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget v9, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 6794
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v24

    .line 6801
    :cond_1c
    :goto_7
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    if-lez v4, :cond_20

    .line 6803
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6804
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6826
    :cond_1d
    :goto_8
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 6828
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v0, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 6830
    :cond_1e
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1a

    .line 6832
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatFromString(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_6

    .line 6798
    :cond_1f
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v9

    goto :goto_7

    .line 6806
    :cond_20
    iget v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    if-nez v4, :cond_1d

    .line 6810
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v4, :cond_22

    .line 6812
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget v4, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    if-lez v4, :cond_21

    .line 6814
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget v4, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto :goto_8

    .line 6818
    :cond_21
    iput v9, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto :goto_8

    .line 6823
    :cond_22
    iput v9, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto :goto_8

    .line 6838
    :cond_23
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v4, :cond_26

    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v4, :cond_26

    .line 6840
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v24

    .line 6841
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 6843
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget v9, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 6844
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v4, :cond_24

    .line 6845
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v24

    .line 6854
    :goto_9
    iget-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v4, :cond_1a

    .line 6856
    iput v9, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6857
    const/16 v30, 0x0

    .line 6858
    const/16 v13, 0xc

    goto/16 :goto_6

    .line 6847
    :cond_24
    const/16 v24, 0x0

    goto :goto_9

    .line 6851
    :cond_25
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v9

    goto :goto_9

    .line 6863
    :cond_26
    const/16 v24, 0x0

    .line 6864
    const/4 v9, 0x0

    .line 6865
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6866
    const/16 v13, 0xc

    .line 6867
    const/16 v30, 0x0

    goto/16 :goto_6

    .line 6898
    :cond_27
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6899
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6900
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6901
    goto/16 :goto_0

    .line 6904
    :cond_28
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6905
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6906
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6907
    goto/16 :goto_0

    .line 6909
    :cond_29
    const-string v4, "application/vnd.syncml.dmtnds+wbxml"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 6911
    const-string v4, "### SYNCML_MIME_TYPE_TNDS_WBXML ###"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6913
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v15, v4}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 6915
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v4, :cond_2c

    .line 6917
    const/16 v16, 0x0

    .line 6918
    .local v16, "nWbxmlDataLen":I
    const/16 v20, 0x0

    .line 6919
    .local v20, "pWbxmlData":[B
    const/16 v31, 0x0

    .line 6920
    .local v31, "szWbxml":Ljava/lang/String;
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6921
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6922
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6923
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 6926
    invoke-static {v15}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileSize(I)I

    move-result v16

    .line 6927
    move/from16 v0, v16

    new-array v0, v0, [B

    move-object/from16 v20, v0

    .line 6928
    const/4 v4, 0x0

    move/from16 v0, v16

    move-object/from16 v1, v20

    invoke-static {v15, v4, v0, v1}, Lcom/fmm/dm/db/file/XDB;->xdbReadFile(III[B)Z

    .line 6929
    new-instance v31, Ljava/lang/String;

    .end local v31    # "szWbxml":Ljava/lang/String;
    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 6930
    .restart local v31    # "szWbxml":Ljava/lang/String;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, v31

    invoke-static {v0, v4}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmTndsWbxmlParse(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v28

    .line 6931
    const/16 v19, 0x0

    .line 6932
    .local v19, "outBufSize":I
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    .line 6934
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v19

    .line 6936
    :cond_2a
    move-object/from16 v0, v28

    move/from16 v1, v19

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v22

    .line 6937
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmTndsParseFinish()V

    .line 6939
    if-lez v22, :cond_2b

    .line 6941
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6942
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6943
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6944
    goto/16 :goto_0

    .line 6946
    :cond_2b
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6947
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6948
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6949
    goto/16 :goto_0

    .line 6951
    .end local v16    # "nWbxmlDataLen":I
    .end local v19    # "outBufSize":I
    .end local v20    # "pWbxmlData":[B
    .end local v31    # "szWbxml":Ljava/lang/String;
    :cond_2c
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6952
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6953
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6954
    goto/16 :goto_0

    .line 6960
    :cond_2d
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_36

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_36

    .line 6962
    const-string v4, "application/vnd.syncml.dmtnds+xml"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_31

    .line 6965
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_2f

    .line 6967
    iget v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v4, v9

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6968
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6970
    iget-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-nez v4, :cond_2e

    .line 6972
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 6973
    invoke-static {v15}, Lcom/fmm/dm/db/file/XDB;->xdbDeleteFile(I)I

    .line 6976
    :cond_2e
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v15, v4}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 6978
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6979
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6980
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6981
    goto/16 :goto_0

    .line 6983
    :cond_2f
    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-static {v0, v9, v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v22

    .line 6984
    if-lez v22, :cond_30

    .line 6986
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6987
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6988
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6992
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetAccountFromOM(Lcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v22

    .line 6994
    goto/16 :goto_0

    .line 6997
    :cond_30
    const/16 v24, 0x0

    .line 6998
    const/16 v30, 0x0

    .line 6999
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 7000
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7001
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7002
    const-string v4, "Fail"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 7003
    const/4 v4, -0x1

    .line 7181
    .end local v17    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    :goto_a
    return v4

    .line 7005
    .restart local v17    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_31
    const-string v4, "application/vnd.syncml.dmtnds+wbxml"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_36

    .line 7007
    const-string v4, "### SYNCML_MIME_TYPE_TNDS_WBXML ###\n"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7008
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_33

    .line 7010
    iget v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v4, v9

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7011
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7013
    iget-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-nez v4, :cond_32

    .line 7015
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 7016
    invoke-static {v15}, Lcom/fmm/dm/db/file/XDB;->xdbDeleteFile(I)I

    .line 7019
    :cond_32
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v15, v4}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 7020
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 7021
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7022
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7023
    goto/16 :goto_0

    .line 7026
    :cond_33
    move-object/from16 v0, v24

    invoke-static {v0, v9}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmTndsWbxmlParse(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v28

    .line 7027
    const/16 v19, 0x0

    .line 7028
    .restart local v19    # "outBufSize":I
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_34

    .line 7030
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v19

    .line 7032
    :cond_34
    move-object/from16 v0, v28

    move/from16 v1, v19

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v22

    .line 7033
    invoke-static {}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmTndsParseFinish()V

    .line 7035
    if-lez v22, :cond_35

    .line 7037
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 7038
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7039
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7041
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_0

    .line 7043
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetAccountFromOM(Lcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v22

    goto/16 :goto_0

    .line 7049
    :cond_35
    const/16 v24, 0x0

    .line 7050
    const/16 v30, 0x0

    .line 7051
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 7052
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7053
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7054
    const-string v4, "xdmAgentBuildCmdStatus : Warning!!!. Fail"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 7055
    const/4 v4, -0x1

    goto/16 :goto_a

    .line 7061
    .end local v19    # "outBufSize":I
    :cond_36
    iget v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    if-nez v4, :cond_39

    .line 7063
    iget-object v5, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget v6, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    const/4 v7, 0x0

    move-object/from16 v4, v18

    move-object/from16 v8, v24

    invoke-static/range {v4 .. v9}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v22

    .line 7065
    const-string v4, "ADD (NO DATA)"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7066
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v17

    .line 7067
    if-eqz v17, :cond_37

    const/16 v4, 0xc

    if-eq v13, v4, :cond_37

    .line 7069
    move-object/from16 v0, v17

    iput v13, v0, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 7115
    :cond_37
    :goto_b
    if-gez v22, :cond_3d

    .line 7117
    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 7119
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_38

    .line 7121
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7125
    :cond_38
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 7126
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7128
    const/16 v24, 0x0

    .line 7129
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7130
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7132
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 7134
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 7075
    :cond_39
    iget v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    if-nez v4, :cond_3c

    .line 7079
    const-string v4, "/Update/PkgData"

    iget-object v5, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_3b

    .line 7081
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget v5, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    invoke-static {v4, v5}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetFreeVaddr(Lcom/fmm/dm/eng/core/XDMOmVfs;I)I

    move-result v10

    .line 7088
    .local v10, "addr":I
    :goto_c
    if-gez v10, :cond_3c

    .line 7091
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "420"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 7092
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7093
    const-string v4, "ADD STATUS_DEVICE_FULL"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7095
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7097
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_3a

    .line 7099
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7103
    :cond_3a
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 7105
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 7085
    .end local v10    # "addr":I
    :cond_3b
    const/4 v10, 0x0

    .restart local v10    # "addr":I
    goto :goto_c

    .line 7111
    .end local v10    # "addr":I
    :cond_3c
    iget-object v5, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget v6, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    iget v7, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    move-object/from16 v4, v18

    move-object/from16 v8, v24

    invoke-static/range {v4 .. v9}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v22

    goto/16 :goto_b

    .line 7139
    :cond_3d
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v17

    .line 7140
    if-eqz v17, :cond_40

    .line 7142
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3f

    .line 7144
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v4, :cond_3e

    .line 7146
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 7148
    :cond_3e
    new-instance v4, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v4}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 7149
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    move-object/from16 v0, v30

    iput-object v0, v4, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 7150
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 7152
    :cond_3f
    const/16 v4, 0xc

    if-eq v13, v4, :cond_40

    .line 7154
    move-object/from16 v0, v17

    iput v13, v0, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 7159
    :cond_40
    const/16 v24, 0x0

    .line 7161
    iget v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v4, :cond_41

    .line 7163
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7164
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7165
    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 7168
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 7178
    :goto_d
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7179
    iget-object v12, v12, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    goto/16 :goto_0

    .line 7172
    :cond_41
    iget v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v4, v9

    iput v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7173
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7176
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_d

    .line 7181
    .end local v17    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    :cond_42
    const/4 v4, 0x0

    goto/16 :goto_a
.end method

.method public xdmAgentCmdAlert(Lcom/fmm/dm/eng/parser/XDMParserAlert;Z)I
    .locals 12
    .param p1, "alert"    # Lcom/fmm/dm/eng/parser/XDMParserAlert;
    .param p2, "isAtomic"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6296
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 6297
    .local v0, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v10, 0x0

    .line 6298
    .local v10, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    const/4 v7, 0x0

    .line 6299
    .local v7, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v6, 0x0

    .line 6300
    .local v6, "cur":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v11, 0x0

    .line 6302
    .local v11, "szData":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_ALERT:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->procState:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 6304
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 6306
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6314
    iget v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    if-eq v1, v4, :cond_2

    .line 6316
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v10

    .line 6317
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v10}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6466
    :cond_0
    :goto_0
    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->procState:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 6467
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 6310
    :cond_1
    const-string v1, "alert->data is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6311
    const/4 v1, -0x1

    goto :goto_1

    .line 6320
    :cond_2
    if-eqz p2, :cond_3

    .line 6322
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "215"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v10

    .line 6323
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v10}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 6325
    :cond_3
    const-string v1, "1222"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 6327
    iput-boolean v4, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextMsg:Z

    .line 6328
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "200"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v10

    .line 6329
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v10}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 6332
    :cond_4
    const-string v1, "1100"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1101"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1102"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1103"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1104"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_17

    .line 6336
    :cond_5
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    if-eqz v1, :cond_6

    .line 6338
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/fmm/dm/eng/core/XDMUicOption;)Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 6339
    iput-object v3, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 6342
    :cond_6
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    if-nez v1, :cond_7

    .line 6344
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateUicOption()Lcom/fmm/dm/eng/core/XDMUicOption;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 6347
    :cond_7
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicGetUicType(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/fmm/dm/eng/core/XDMUicOption;->UICType:I

    .line 6349
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 6350
    iget-object v7, v6, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v7, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 6354
    .restart local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_11

    .line 6356
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v11

    .line 6357
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "str = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 6364
    :goto_2
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 6367
    const-string v1, "1100"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "1101"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "1102"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "1103"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "1104"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_12

    .line 6370
    :cond_8
    const-string v11, "MINDT=30"

    .line 6384
    :cond_9
    :goto_3
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    invoke-static {v11, v1}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicOptionProcess(Ljava/lang/String;Lcom/fmm/dm/eng/core/XDMUicOption;)Ljava/lang/String;

    move-result-object v11

    .line 6385
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6387
    if-eqz v6, :cond_a

    .line 6389
    iget-object v7, v6, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v7, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 6392
    .restart local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    :cond_a
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_b

    .line 6394
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v11

    .line 6397
    :cond_b
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 6399
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_c

    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_c

    .line 6401
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    .line 6405
    :cond_c
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 6407
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMUicOption;->text:Lcom/fmm/dm/eng/core/XDMText;

    invoke-static {v2, v11}, Lcom/fmm/dm/eng/core/XDMList;->xdmListAppendStrText(Lcom/fmm/dm/eng/core/XDMText;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMText;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMUicOption;->text:Lcom/fmm/dm/eng/core/XDMText;

    .line 6411
    :cond_d
    const-string v1, "1103"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "1104"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_15

    .line 6413
    :cond_e
    const/4 v8, 0x0

    .line 6414
    .local v8, "iuicMenu":I
    if-eqz v6, :cond_14

    .line 6416
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move v9, v8

    .line 6417
    .end local v8    # "iuicMenu":I
    .local v9, "iuicMenu":I
    :goto_4
    if-eqz v6, :cond_13

    .line 6419
    iget-object v7, v6, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v7, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 6421
    .restart local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_f

    .line 6423
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v11

    .line 6426
    :cond_f
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 6428
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_10

    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_10

    .line 6430
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    .line 6434
    :cond_10
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 6436
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    add-int/lit8 v8, v9, 0x1

    .end local v9    # "iuicMenu":I
    .restart local v8    # "iuicMenu":I
    aput-object v11, v1, v9

    .line 6439
    :goto_5
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move v9, v8

    .end local v8    # "iuicMenu":I
    .restart local v9    # "iuicMenu":I
    goto :goto_4

    .line 6361
    .end local v9    # "iuicMenu":I
    :cond_11
    const-string v1, "str = NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6375
    :cond_12
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_9

    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_9

    .line 6379
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_3

    .restart local v9    # "iuicMenu":I
    :cond_13
    move v8, v9

    .line 6442
    .end local v9    # "iuicMenu":I
    .restart local v8    # "iuicMenu":I
    :cond_14
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iput v8, v1, Lcom/fmm/dm/eng/core/XDMUicOption;->uicMenuNumbers:I

    .line 6445
    .end local v8    # "iuicMenu":I
    :cond_15
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicOption:Lcom/fmm/dm/eng/core/XDMUicOption;

    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->appId:I

    iput v2, v1, Lcom/fmm/dm/eng/core/XDMUicOption;->appId:I

    .line 6447
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    if-eqz v1, :cond_16

    .line 6449
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 6451
    :cond_16
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserAlert;-><init>()V

    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    .line 6452
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-static {v1, p1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplAlert(Lcom/fmm/dm/eng/parser/XDMParserAlert;Lcom/fmm/dm/eng/parser/XDMParserAlert;)V

    .line 6454
    const/4 v1, -0x5

    goto/16 :goto_1

    .line 6456
    :cond_17
    const-string v1, "1223"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_18

    .line 6458
    iput v4, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sessionAbort:I

    .line 6459
    const/4 v1, 0x3

    goto/16 :goto_1

    .line 6461
    :cond_18
    const-string v1, "1226"

    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    .restart local v9    # "iuicMenu":I
    :cond_19
    move v8, v9

    .end local v9    # "iuicMenu":I
    .restart local v8    # "iuicMenu":I
    goto :goto_5
.end method

.method public xdmAgentCmdAtomic(Lcom/fmm/dm/eng/parser/XDMParserAtomic;)I
    .locals 4
    .param p1, "atomic"    # Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    .prologue
    const/4 v2, -0x1

    .line 8734
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 8737
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    if-nez p1, :cond_1

    .line 8753
    :cond_0
    :goto_0
    return v2

    .line 8742
    :cond_1
    iget-object v3, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v3, :cond_0

    .line 8744
    iget-object v3, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdAtomicBlock(Lcom/fmm/dm/eng/parser/XDMParserAtomic;Lcom/fmm/dm/eng/core/XDMLinkedList;)I

    move-result v0

    .line 8748
    .local v0, "res":I
    if-ltz v0, :cond_0

    .line 8752
    iget v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    add-int/2addr v2, v0

    iput v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    .line 8753
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public xdmAgentCmdAtomicBlock(Lcom/fmm/dm/eng/parser/XDMParserAtomic;Lcom/fmm/dm/eng/core/XDMLinkedList;)I
    .locals 13
    .param p1, "atomic"    # Lcom/fmm/dm/eng/parser/XDMParserAtomic;
    .param p2, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    .line 3475
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 3477
    .local v0, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v8, 0x0

    .line 3478
    .local v8, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v12, 0x0

    .line 3479
    .local v12, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    const/4 v7, 0x1

    .line 3481
    .local v7, "isProcess":Z
    const/4 v10, 0x0

    .line 3482
    .local v10, "r":I
    const/4 v9, 0x1

    .line 3484
    .local v9, "num":I
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 3485
    const/4 v1, 0x0

    invoke-static {p2, v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 3486
    invoke-static {p2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/fmm/dm/agent/XDMAgent;

    .line 3488
    .local v6, "cmd":Lcom/fmm/dm/agent/XDMAgent;
    :goto_0
    if-eqz v6, :cond_1

    .line 3490
    invoke-virtual {p0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentVefifyAtomicCmd(Lcom/fmm/dm/agent/XDMAgent;)Z

    move-result v11

    .line 3491
    .local v11, "res":Z
    if-nez v11, :cond_0

    .line 3493
    const/4 v7, 0x0

    .line 3495
    :cond_0
    invoke-static {p2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v6, Lcom/fmm/dm/agent/XDMAgent;

    .restart local v6    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    goto :goto_0

    .line 3498
    .end local v11    # "res":Z
    :cond_1
    if-eqz v7, :cond_2

    .line 3500
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->cmdid:I

    const-string v2, "Atomic"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "200"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 3501
    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    .line 3509
    :goto_1
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3511
    const/4 v1, 0x0

    invoke-static {p2, v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 3512
    invoke-static {p2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v6, Lcom/fmm/dm/agent/XDMAgent;

    .line 3513
    .restart local v6    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    :goto_2
    if-eqz v6, :cond_22

    .line 3515
    iget-boolean v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    if-eqz v1, :cond_f

    .line 3517
    const-string v1, "Get"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 3519
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Get:Lcom/fmm/dm/eng/parser/XDMParserGet;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserGet;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    iget-object v8, v1, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 3520
    .restart local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3522
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Get:Lcom/fmm/dm/eng/parser/XDMParserGet;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 3529
    :goto_3
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3706
    :goto_4
    invoke-static {p2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v6, Lcom/fmm/dm/agent/XDMAgent;

    .restart local v6    # "cmd":Lcom/fmm/dm/agent/XDMAgent;
    goto :goto_2

    .line 3505
    :cond_2
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->cmdid:I

    const-string v2, "Atomic"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "507"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 3506
    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_ROLLBACK:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    goto :goto_1

    .line 3526
    :cond_3
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Get:Lcom/fmm/dm/eng/parser/XDMParserGet;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_3

    .line 3531
    :cond_4
    const-string v1, "Exec"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 3533
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Exec:Lcom/fmm/dm/eng/parser/XDMParserExec;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserExec;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    iget-object v8, v1, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 3534
    .restart local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3536
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Exec:Lcom/fmm/dm/eng/parser/XDMParserExec;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 3543
    :goto_5
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_4

    .line 3540
    :cond_5
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Exec:Lcom/fmm/dm/eng/parser/XDMParserExec;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_5

    .line 3545
    :cond_6
    const-string v1, "Add"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 3547
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_AddCmd:Lcom/fmm/dm/eng/parser/XDMParserAdd;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    iget-object v8, v1, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 3548
    .restart local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 3550
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_AddCmd:Lcom/fmm/dm/eng/parser/XDMParserAdd;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v2, "Add"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 3557
    :goto_6
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 3554
    :cond_7
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_AddCmd:Lcom/fmm/dm/eng/parser/XDMParserAdd;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_6

    .line 3559
    :cond_8
    const-string v1, "Delete"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_a

    .line 3561
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_DeleteCmd:Lcom/fmm/dm/eng/parser/XDMParserDelete;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    iget-object v8, v1, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 3563
    .restart local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 3565
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_DeleteCmd:Lcom/fmm/dm/eng/parser/XDMParserDelete;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v2, "Delete"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 3571
    :goto_7
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 3569
    :cond_9
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_DeleteCmd:Lcom/fmm/dm/eng/parser/XDMParserDelete;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_7

    .line 3573
    :cond_a
    const-string v1, "Replace"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_c

    .line 3575
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_ReplaceCmd:Lcom/fmm/dm/eng/parser/XDMParserReplace;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    iget-object v8, v1, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 3576
    .restart local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 3578
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_ReplaceCmd:Lcom/fmm/dm/eng/parser/XDMParserReplace;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v2, "Replace"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 3584
    :goto_8
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 3582
    :cond_b
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_ReplaceCmd:Lcom/fmm/dm/eng/parser/XDMParserReplace;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_8

    .line 3586
    :cond_c
    const-string v1, "Copy"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_e

    .line 3588
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_CopyCmd:Lcom/fmm/dm/eng/parser/XDMParserCopy;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    iget-object v8, v1, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 3589
    .restart local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 3591
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_CopyCmd:Lcom/fmm/dm/eng/parser/XDMParserCopy;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v2, "Copy"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 3597
    :goto_9
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 3595
    :cond_d
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_CopyCmd:Lcom/fmm/dm/eng/parser/XDMParserCopy;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_9

    .line 3601
    :cond_e
    const-string v1, "unknown command"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 3606
    :cond_f
    const-string v1, "Get"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_11

    .line 3608
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Get:Lcom/fmm/dm/eng/parser/XDMParserGet;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdGet(Lcom/fmm/dm/eng/parser/XDMParserGet;Z)I

    move-result v10

    .line 3609
    if-eqz v10, :cond_10

    .line 3611
    const-string v1, "get failed"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3612
    const/4 v9, -0x1

    .line 3710
    .end local v9    # "num":I
    :goto_a
    return v9

    .line 3614
    .restart local v9    # "num":I
    :cond_10
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 3616
    :cond_11
    const-string v1, "Exec"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_14

    .line 3618
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Exec:Lcom/fmm/dm/eng/parser/XDMParserExec;

    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdExec(Lcom/fmm/dm/eng/parser/XDMParserExec;)I

    move-result v10

    .line 3620
    const-string v1, "507"

    iget-object v2, v12, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_12

    .line 3622
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 3625
    :cond_12
    if-eqz v10, :cond_13

    .line 3627
    const-string v1, "exec failed"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3628
    const/4 v9, -0x1

    goto :goto_a

    .line 3630
    :cond_13
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 3632
    :cond_14
    const-string v1, "Add"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_17

    .line 3634
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_AddCmd:Lcom/fmm/dm/eng/parser/XDMParserAdd;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v12}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdAdd(Lcom/fmm/dm/eng/parser/XDMParserAdd;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v10

    .line 3636
    const-string v1, "507"

    iget-object v2, v12, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_15

    .line 3638
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 3641
    :cond_15
    if-eqz v10, :cond_16

    .line 3643
    const-string v1, "Add failed"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3644
    const/4 v9, -0x1

    goto :goto_a

    .line 3646
    :cond_16
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 3648
    :cond_17
    const-string v1, "Delete"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1a

    .line 3650
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_DeleteCmd:Lcom/fmm/dm/eng/parser/XDMParserDelete;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v12}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdDelete(Lcom/fmm/dm/eng/parser/XDMParserDelete;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v10

    .line 3652
    const-string v1, "507"

    iget-object v2, v12, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_18

    .line 3654
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 3657
    :cond_18
    if-eqz v10, :cond_19

    .line 3659
    const-string v1, "Delete failed"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3660
    const/4 v9, -0x1

    goto :goto_a

    .line 3662
    :cond_19
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 3664
    :cond_1a
    const-string v1, "Replace"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1d

    .line 3666
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_ReplaceCmd:Lcom/fmm/dm/eng/parser/XDMParserReplace;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v12}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdReplace(Lcom/fmm/dm/eng/parser/XDMParserReplace;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v10

    .line 3668
    const-string v1, "507"

    iget-object v2, v12, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1b

    .line 3670
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 3673
    :cond_1b
    if-eqz v10, :cond_1c

    .line 3675
    const-string v1, "Replace failed"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3676
    const/4 v9, -0x1

    goto/16 :goto_a

    .line 3678
    :cond_1c
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 3680
    :cond_1d
    const-string v1, "Copy"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_20

    .line 3682
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_CopyCmd:Lcom/fmm/dm/eng/parser/XDMParserCopy;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v12}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdCopy(Lcom/fmm/dm/eng/parser/XDMParserCopy;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v10

    .line 3684
    const-string v1, "507"

    iget-object v2, v12, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1e

    .line 3686
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 3689
    :cond_1e
    if-eqz v10, :cond_1f

    .line 3691
    const-string v1, "Copy failed"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3692
    const/4 v9, -0x1

    goto/16 :goto_a

    .line 3694
    :cond_1f
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 3696
    :cond_20
    const-string v1, "Atomic_Start"

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_21

    .line 3698
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 3699
    iget-object v1, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    iget-object v2, v6, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    iget-object v2, v2, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v1, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdAtomicBlock(Lcom/fmm/dm/eng/parser/XDMParserAtomic;Lcom/fmm/dm/eng/core/XDMLinkedList;)I

    goto/16 :goto_4

    .line 3703
    :cond_21
    const-string v1, "unknown command"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 3708
    :cond_22
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserAtomic;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    goto/16 :goto_a
.end method

.method public xdmAgentCmdCopy(Lcom/fmm/dm/eng/parser/XDMParserCopy;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I
    .locals 28
    .param p1, "copy"    # Lcom/fmm/dm/eng/parser/XDMParserCopy;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .prologue
    .line 8193
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 8194
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v0, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    move-object/from16 v16, v0

    .line 8195
    .local v16, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/16 v21, 0x0

    .line 8196
    .local v21, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    const/4 v14, 0x0

    .line 8197
    .local v14, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v11, 0x0

    .line 8199
    .local v11, "cur":Lcom/fmm/dm/eng/core/XDMList;
    const/16 v19, 0x0

    .line 8200
    .local v19, "sourcedata":[C
    const/16 v25, 0x0

    .line 8205
    .local v25, "targetdata":[C
    const/16 v3, 0x50

    new-array v0, v3, [C

    move-object/from16 v27, v0

    .line 8206
    .local v27, "tmpbuf":[C
    const/4 v8, 0x0

    .line 8207
    .local v8, "sourcesize":I
    const/16 v26, 0x0

    .line 8208
    .local v26, "targetsize":I
    const/4 v9, 0x0

    .line 8209
    .local v9, "bufsize":I
    const/4 v10, 0x0

    .line 8212
    .local v10, "bufsize1":I
    invoke-virtual/range {p0 .. p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v17

    .line 8213
    .local v17, "process":Z
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 8214
    :cond_0
    :goto_0
    if-eqz v11, :cond_21

    .line 8216
    const/16 v24, 0x0

    .line 8217
    .local v24, "szType":Ljava/lang/String;
    const/16 v12, 0xc

    .line 8218
    .local v12, "format":I
    iget-object v14, v11, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v14    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v14, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 8220
    .restart local v14    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    .line 8222
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 8224
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v7, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8231
    :goto_1
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8232
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8233
    goto :goto_0

    .line 8228
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_1

    .line 8235
    :cond_2
    if-nez v17, :cond_5

    .line 8238
    iget v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_3

    .line 8240
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8243
    :cond_3
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 8245
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8252
    :goto_2
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8253
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8254
    goto :goto_0

    .line 8249
    :cond_4
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_2

    .line 8258
    :cond_5
    if-eqz p2, :cond_d

    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v4, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    if-eq v3, v4, :cond_d

    .line 8260
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    if-eqz v3, :cond_9

    .line 8263
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-virtual {v3, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 8265
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 8267
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "418"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8274
    :goto_3
    sget-object v3, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    .line 8275
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 8315
    :goto_4
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8316
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8317
    goto/16 :goto_0

    .line 8271
    :cond_6
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "418"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_3

    .line 8280
    :cond_7
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 8282
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_4

    .line 8286
    :cond_8
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_4

    .line 8292
    :cond_9
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v4, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    if-ne v3, v4, :cond_b

    .line 8294
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 8296
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_4

    .line 8300
    :cond_a
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_4

    .line 8305
    :cond_b
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 8307
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto/16 :goto_4

    .line 8311
    :cond_c
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto/16 :goto_4

    .line 8320
    :cond_d
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 8322
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8323
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8324
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8325
    goto/16 :goto_0

    .line 8328
    :cond_e
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 8330
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8331
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8332
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8333
    goto/16 :goto_0

    .line 8336
    :cond_f
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v15

    .line 8338
    .local v15, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v15, :cond_10

    .line 8340
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8341
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8342
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8346
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 8348
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 8356
    :cond_10
    iget v9, v15, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 8357
    move v8, v9

    .line 8358
    new-array v0, v8, [C

    move-object/from16 v19, v0

    .line 8359
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    move-object/from16 v0, v19

    invoke-static {v3, v15, v0}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsGetData(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;[C)I

    .line 8361
    iget v12, v15, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 8363
    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v3, :cond_11

    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    iget-object v3, v3, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v3, :cond_11

    .line 8365
    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    iget-object v3, v3, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    .line 8373
    :goto_5
    iget v0, v15, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    move/from16 v20, v0

    .line 8376
    .local v20, "sourceformat":I
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v15

    .line 8377
    if-eqz v15, :cond_16

    iget v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_16

    iget-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v3, :cond_16

    .line 8379
    iget v10, v15, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 8380
    move/from16 v26, v10

    .line 8382
    move/from16 v0, v26

    if-ge v0, v8, :cond_12

    .line 8384
    const/16 v25, 0x0

    .line 8385
    new-array v0, v8, [C

    move-object/from16 v25, v0

    .line 8386
    move v10, v9

    .line 8387
    move/from16 v26, v8

    .line 8395
    :goto_6
    iget v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_13

    .line 8397
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8398
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8399
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8401
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 8403
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 8369
    .end local v20    # "sourceformat":I
    :cond_11
    const/16 v24, 0x0

    goto :goto_5

    .line 8391
    .restart local v20    # "sourceformat":I
    :cond_12
    const/16 v25, 0x0

    .line 8392
    move/from16 v0, v26

    new-array v0, v0, [C

    move-object/from16 v25, v0

    goto :goto_6

    .line 8409
    :cond_13
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_7
    if-ge v13, v8, :cond_14

    .line 8410
    aget-char v3, v19, v13

    aput-char v3, v25, v13

    .line 8409
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 8411
    :cond_14
    new-instance v22, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 8412
    .local v22, "szData":Ljava/lang/String;
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    move-object/from16 v0, v22

    invoke-static {v3, v15, v0, v9}, Lcom/fmm/dm/eng/core/XDMOmVfs;->xdmOmVfsSetData(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Ljava/lang/Object;I)I

    .line 8415
    iget v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_15

    .line 8417
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 8418
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8419
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 8422
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8543
    .end local v13    # "i":I
    .end local v22    # "szData":Ljava/lang/String;
    :goto_8
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8544
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    goto/16 :goto_0

    .line 8426
    .restart local v13    # "i":I
    .restart local v22    # "szData":Ljava/lang/String;
    :cond_15
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v3, v10

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 8427
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8430
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_8

    .line 8435
    .end local v13    # "i":I
    .end local v22    # "szData":Ljava/lang/String;
    :cond_16
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 8436
    invoke-static/range {v27 .. v27}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v23

    .line 8438
    .local v23, "szPathName":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v15

    .line 8439
    if-nez v15, :cond_18

    .line 8442
    iget v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_17

    .line 8444
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8447
    :cond_17
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8448
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8449
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8453
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 8455
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 8462
    :cond_18
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v15, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 8465
    iget v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_19

    .line 8467
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8469
    :cond_19
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "425"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8470
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8471
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8472
    goto/16 :goto_0

    .line 8475
    :cond_1a
    iput v9, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 8476
    move/from16 v12, v20

    .line 8477
    const/16 v24, 0x0

    .line 8478
    iget-object v4, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v3, v16

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v18

    .line 8481
    .local v18, "res":I
    if-gez v18, :cond_1c

    .line 8484
    iget v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_1b

    .line 8486
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8490
    :cond_1b
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 8491
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8493
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8494
    iget-object v11, v11, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8498
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 8500
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 8507
    :cond_1c
    iget-object v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v15

    .line 8508
    if-eqz v15, :cond_1f

    .line 8510
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    .line 8512
    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v3, :cond_1d

    .line 8514
    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 8516
    :cond_1d
    new-instance v3, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v3}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    iput-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 8517
    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    move-object/from16 v0, v24

    iput-object v0, v3, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 8518
    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 8520
    :cond_1e
    iput v12, v15, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 8524
    :cond_1f
    iget v3, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_20

    .line 8526
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 8527
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8528
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 8531
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto/16 :goto_8

    .line 8535
    :cond_20
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v3, v9

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 8536
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8539
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto/16 :goto_8

    .line 8546
    .end local v12    # "format":I
    .end local v15    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .end local v18    # "res":I
    .end local v20    # "sourceformat":I
    .end local v23    # "szPathName":Ljava/lang/String;
    .end local v24    # "szType":Ljava/lang/String;
    :cond_21
    const/4 v3, 0x0

    return v3
.end method

.method public xdmAgentCmdDelete(Lcom/fmm/dm/eng/parser/XDMParserDelete;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I
    .locals 14
    .param p1, "delcmd"    # Lcom/fmm/dm/eng/parser/XDMParserDelete;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .prologue
    .line 8551
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 8552
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v10, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 8553
    .local v10, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/4 v13, 0x0

    .line 8554
    .local v13, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    const/4 v8, 0x0

    .line 8555
    .local v8, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v7, 0x0

    .line 8556
    .local v7, "cur":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v9, 0x0

    .line 8560
    .local v9, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v11

    .line 8562
    .local v11, "process":Z
    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 8563
    :goto_0
    if-eqz v7, :cond_15

    .line 8565
    iget-object v8, v7, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 8566
    .restart local v8    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v13, 0x0

    .line 8567
    iget v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 8569
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 8571
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v6, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 8577
    :goto_1
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8578
    iget-object v7, v7, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 8579
    goto :goto_0

    .line 8575
    :cond_0
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_1

    .line 8582
    :cond_1
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 8584
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v10, v2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v9

    .line 8587
    :cond_2
    if-nez v11, :cond_6

    .line 8589
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 8591
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "215"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 8598
    :goto_2
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 8600
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 8722
    :cond_3
    :goto_3
    if-eqz v13, :cond_4

    .line 8724
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8727
    :cond_4
    iget-object v7, v7, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0

    .line 8595
    :cond_5
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "215"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_2

    .line 8604
    :cond_6
    if-eqz p2, :cond_e

    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v3, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    if-eq v2, v3, :cond_e

    .line 8606
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    if-eqz v2, :cond_a

    .line 8609
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-virtual {v2, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 8611
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 8613
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "404"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 8619
    :goto_4
    sget-object v2, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    .line 8620
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    goto :goto_3

    .line 8617
    :cond_7
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "404"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_4

    .line 8624
    :cond_8
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 8626
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "216"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_3

    .line 8630
    :cond_9
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "216"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_3

    .line 8636
    :cond_a
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v3, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    if-ne v2, v3, :cond_c

    .line 8638
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 8640
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "215"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 8644
    :cond_b
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "215"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 8649
    :cond_c
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 8651
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "216"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 8655
    :cond_d
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "216"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 8660
    :cond_e
    if-nez v9, :cond_10

    .line 8662
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 8664
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "404"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 8671
    :goto_5
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 8673
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 8668
    :cond_f
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "404"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_5

    .line 8677
    :cond_10
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {p0, v10, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentIsPermanentNode(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 8679
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 8681
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 8683
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 8686
    :cond_11
    const/4 v2, 0x2

    invoke-static {v10, v9, v2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v2

    if-nez v2, :cond_12

    .line 8688
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "425"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 8690
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 8692
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 8695
    :cond_12
    iget-object v2, v10, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    if-ne v9, v2, :cond_13

    .line 8697
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 8699
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 8701
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 8706
    :cond_13
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 8707
    iget-object v2, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v10, v2, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmDelete(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    move-result v12

    .line 8708
    .local v12, "res":I
    if-gez v12, :cond_14

    .line 8710
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 8711
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 8713
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 8718
    :cond_14
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "200"

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 8729
    .end local v12    # "res":I
    :cond_15
    const/4 v2, 0x0

    return v2
.end method

.method public xdmAgentCmdExec(Lcom/fmm/dm/eng/parser/XDMParserExec;)I
    .locals 12
    .param p1, "exec"    # Lcom/fmm/dm/eng/parser/XDMParserExec;

    .prologue
    .line 6156
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 6157
    .local v0, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v7, 0x0

    .line 6158
    .local v7, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/4 v6, 0x0

    .line 6159
    .local v6, "cur":Lcom/fmm/dm/eng/core/XDMList;
    const/4 v9, 0x0

    .line 6160
    .local v9, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    const/4 v11, 0x0

    .line 6161
    .local v11, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v10, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 6162
    .local v10, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/4 v8, 0x0

    .line 6164
    .local v8, "nAgentType":I
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 6166
    :goto_0
    if-eqz v6, :cond_e

    .line 6168
    iget-object v7, v6, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v7, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 6170
    .restart local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 6172
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6174
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 6181
    :goto_1
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6182
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6183
    goto :goto_0

    .line 6178
    :cond_0
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    goto :goto_1

    .line 6186
    :cond_1
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 6188
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 6189
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6190
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6191
    goto :goto_0

    .line 6194
    :cond_2
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v10, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v9

    .line 6195
    if-nez v9, :cond_3

    .line 6197
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 6198
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6199
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6200
    goto :goto_0

    .line 6203
    :cond_3
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentIsAccessibleNode(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 6205
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "405"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 6206
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6207
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6208
    goto/16 :goto_0

    .line 6211
    :cond_4
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {p0, v10, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentIsPermanentNode(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 6213
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "405"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 6214
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6215
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6216
    goto/16 :goto_0

    .line 6220
    :cond_5
    const/4 v1, 0x4

    invoke-static {v10, v9, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 6222
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "425"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 6223
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6224
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6225
    goto/16 :goto_0

    .line 6228
    :cond_6
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 6230
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6234
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-static {v10, v1, v2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAclCurrentNode(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_7

    .line 6236
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "425"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 6237
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6238
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6239
    goto/16 :goto_0

    .line 6243
    :cond_7
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsLawmoExecNode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 6245
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 6246
    const/4 v8, 0x1

    .line 6269
    :goto_2
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 6271
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetCorrelator(Ljava/lang/String;)V

    .line 6275
    :cond_8
    const/4 v1, 0x1

    if-ne v8, v1, :cond_c

    .line 6277
    invoke-direct {p0, v0, p1, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdExecLawmo(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParserExec;Lcom/fmm/dm/eng/parser/XDMParserItem;)V

    .line 6289
    :goto_3
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    goto/16 :goto_0

    .line 6249
    :cond_9
    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentIsExecNode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 6251
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 6252
    const/4 v8, 0x2

    goto :goto_2

    .line 6256
    :cond_a
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 6257
    const/4 v8, 0x2

    goto :goto_2

    .line 6262
    :cond_b
    const-string v1, "Error item->target->pLocURI is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6263
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "403"

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 6264
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget-object v1, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6265
    iget-object v6, v6, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 6266
    goto/16 :goto_0

    .line 6280
    :cond_c
    const/4 v1, 0x2

    if-ne v8, v1, :cond_d

    .line 6282
    invoke-direct {p0, v0, p1, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdExecMobileTracking(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParserExec;Lcom/fmm/dm/eng/parser/XDMParserItem;)V

    goto :goto_3

    .line 6286
    :cond_d
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_3

    .line 6291
    :cond_e
    const/4 v1, 0x0

    return v1
.end method

.method public xdmAgentCmdGet(Lcom/fmm/dm/eng/parser/XDMParserGet;Z)I
    .locals 30
    .param p1, "get"    # Lcom/fmm/dm/eng/parser/XDMParserGet;
    .param p2, "isAtomic"    # Z

    .prologue
    .line 3715
    sget-object v3, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 3716
    .local v3, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v0, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    move-object/from16 v20, v0

    .line 3717
    .local v20, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/16 v18, 0x0

    .line 3718
    .local v18, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    const/16 v25, 0x0

    .line 3719
    .local v25, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    const/16 v24, 0x0

    .line 3720
    .local v24, "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    const/4 v7, 0x0

    .line 3721
    .local v7, "szType":Ljava/lang/String;
    const/4 v6, 0x0

    .line 3722
    .local v6, "szFormat":Ljava/lang/String;
    const/16 v27, 0x0

    .line 3723
    .local v27, "szResultbuf":Ljava/lang/String;
    const/16 v4, 0x64

    new-array v15, v4, [Ljava/lang/String;

    .line 3724
    .local v15, "chlist":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 3726
    .local v16, "cur":Lcom/fmm/dm/eng/core/XDMList;
    const/16 v19, 0x0

    .line 3727
    .local v19, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    const/16 v22, 0x1

    .line 3728
    .local v22, "process":Z
    const/4 v14, 0x0

    .line 3730
    .local v14, "bufsize":I
    const/16 v21, 0x0

    .line 3732
    .local v21, "pData":[C
    invoke-virtual/range {p0 .. p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v22

    .line 3734
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    move-object/from16 v26, v6

    .end local v6    # "szFormat":Ljava/lang/String;
    .local v26, "szFormat":Ljava/lang/String;
    move-object/from16 v28, v7

    .line 3735
    .end local v7    # "szType":Ljava/lang/String;
    .local v28, "szType":Ljava/lang/String;
    :goto_0
    if-eqz v16, :cond_19

    .line 3737
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    move-object/from16 v18, v0

    .end local v18    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    check-cast v18, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 3739
    .restart local v18    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    .line 3741
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3743
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3749
    :goto_1
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3750
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3751
    goto :goto_0

    .line 3747
    :cond_0
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    goto :goto_1

    .line 3754
    :cond_1
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3757
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "404"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3758
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3759
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3760
    goto :goto_0

    .line 3763
    :cond_2
    if-nez v22, :cond_5

    .line 3765
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 3767
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3774
    :goto_2
    if-eqz v25, :cond_3

    .line 3776
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3778
    :cond_3
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3779
    goto/16 :goto_0

    .line 3771
    :cond_4
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    goto :goto_2

    .line 3782
    :cond_5
    if-eqz p2, :cond_8

    .line 3784
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 3786
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3793
    :goto_3
    if-eqz v25, :cond_6

    .line 3795
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3797
    :cond_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3798
    goto/16 :goto_0

    .line 3790
    :cond_7
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    goto :goto_3

    .line 3801
    :cond_8
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "?"

    invoke-static {v4, v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 3803
    const-string v4, "Get"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p1

    invoke-virtual {v0, v4, v1, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdProp(Ljava/lang/String;Lcom/fmm/dm/eng/parser/XDMParserItem;Ljava/lang/Object;)I

    move-result v23

    .line 3804
    .local v23, "res":I
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3805
    goto/16 :goto_0

    .line 3808
    .end local v23    # "res":I
    :cond_9
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-static {v0, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v19

    .line 3809
    if-nez v19, :cond_a

    .line 3811
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "404"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3812
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3813
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3814
    goto/16 :goto_0

    .line 3817
    :cond_a
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentIsAccessibleNode(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 3819
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "405"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3820
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3821
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3822
    goto/16 :goto_0

    .line 3824
    :cond_b
    const/16 v4, 0x8

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v4

    if-nez v4, :cond_c

    .line 3826
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "425"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3827
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3828
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3829
    goto/16 :goto_0

    .line 3832
    :cond_c
    move-object/from16 v0, v19

    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    if-gez v4, :cond_10

    move-object/from16 v0, v19

    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-gtz v4, :cond_10

    .line 3834
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3835
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3837
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/16 v5, 0x64

    move-object/from16 v0, v20

    invoke-static {v0, v4, v15, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetChild(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;[Ljava/lang/String;I)I

    move-result v23

    .line 3841
    .restart local v23    # "res":I
    move-object/from16 v0, v19

    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v6

    .line 3842
    .end local v26    # "szFormat":Ljava/lang/String;
    .restart local v6    # "szFormat":Ljava/lang/String;
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v4, :cond_d

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v4, :cond_d

    .line 3844
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 3851
    .end local v28    # "szType":Ljava/lang/String;
    .restart local v7    # "szType":Ljava/lang/String;
    :goto_4
    if-lez v23, :cond_e

    .line 3853
    const/4 v4, 0x0

    aget-object v27, v15, v4

    .line 3854
    const/16 v17, 0x1

    .local v17, "i":I
    :goto_5
    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_f

    .line 3856
    const-string v4, "/"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 3857
    aget-object v4, v15, v17

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 3854
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 3848
    .end local v7    # "szType":Ljava/lang/String;
    .end local v17    # "i":I
    .restart local v28    # "szType":Ljava/lang/String;
    :cond_d
    const/4 v7, 0x0

    .end local v28    # "szType":Ljava/lang/String;
    .restart local v7    # "szType":Ljava/lang/String;
    goto :goto_4

    .line 3862
    :cond_e
    const-string v27, ""

    .line 3864
    :cond_f
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    invoke-static/range {v3 .. v9}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v24

    .line 3865
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v24

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3938
    .end local v23    # "res":I
    :goto_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    move-object/from16 v26, v6

    .end local v6    # "szFormat":Ljava/lang/String;
    .restart local v26    # "szFormat":Ljava/lang/String;
    move-object/from16 v28, v7

    .end local v7    # "szType":Ljava/lang/String;
    .restart local v28    # "szType":Ljava/lang/String;
    goto/16 :goto_0

    .line 3869
    :cond_10
    move-object/from16 v0, v19

    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    iget v5, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    if-le v4, v5, :cond_11

    .line 3871
    move-object/from16 v0, p1

    iget v9, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v10, "Get"

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v13, "413"

    move-object v8, v3

    invoke-static/range {v8 .. v13}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3872
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3873
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3874
    goto/16 :goto_0

    .line 3877
    :cond_11
    move-object/from16 v0, v19

    iget v14, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 3879
    move-object/from16 v0, v19

    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v6

    .line 3881
    .end local v26    # "szFormat":Ljava/lang/String;
    .restart local v6    # "szFormat":Ljava/lang/String;
    move-object/from16 v0, v19

    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v4, [C

    move-object/from16 v21, v0

    .line 3882
    invoke-static/range {v21 .. v21}, Ljava/util/Arrays;->hashCode([C)I

    move-result v4

    if-eqz v4, :cond_12

    move-object/from16 v0, v21

    array-length v4, v0

    if-nez v4, :cond_14

    .line 3884
    :cond_12
    move-object/from16 v0, p1

    iget v9, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v10, "Get"

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v13, "215"

    move-object v8, v3

    invoke-static/range {v8 .. v13}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3885
    if-eqz v25, :cond_13

    .line 3887
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3889
    :cond_13
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    move-object/from16 v26, v6

    .line 3890
    .end local v6    # "szFormat":Ljava/lang/String;
    .restart local v26    # "szFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 3893
    .end local v26    # "szFormat":Ljava/lang/String;
    .restart local v6    # "szFormat":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v4, :cond_16

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v4, :cond_16

    .line 3895
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 3902
    .end local v28    # "szType":Ljava/lang/String;
    .restart local v7    # "szType":Ljava/lang/String;
    :goto_7
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    iget v8, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v4, v5, v1, v8}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3905
    if-eqz v21, :cond_17

    .line 3907
    new-instance v29, Ljava/lang/String;

    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 3908
    .local v29, "szValue":Ljava/lang/String;
    const-string v4, "CHECKFAIL"

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 3910
    move-object/from16 v0, p1

    iget v9, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v10, "Get"

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v13, "500"

    move-object v8, v3

    invoke-static/range {v8 .. v13}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3911
    if-eqz v25, :cond_15

    .line 3913
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3915
    :cond_15
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v16, v0

    move-object/from16 v26, v6

    .end local v6    # "szFormat":Ljava/lang/String;
    .restart local v26    # "szFormat":Ljava/lang/String;
    move-object/from16 v28, v7

    .line 3916
    .end local v7    # "szType":Ljava/lang/String;
    .restart local v28    # "szType":Ljava/lang/String;
    goto/16 :goto_0

    .line 3899
    .end local v26    # "szFormat":Ljava/lang/String;
    .end local v29    # "szValue":Ljava/lang/String;
    .restart local v6    # "szFormat":Ljava/lang/String;
    :cond_16
    const/4 v7, 0x0

    .end local v28    # "szType":Ljava/lang/String;
    .restart local v7    # "szType":Ljava/lang/String;
    goto :goto_7

    .line 3920
    :cond_17
    move-object/from16 v0, p1

    iget v9, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v10, "Get"

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v13, "200"

    move-object v8, v3

    invoke-static/range {v8 .. v13}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3921
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3923
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move v8, v14

    move-object/from16 v9, v21

    invoke-static/range {v3 .. v9}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v24

    .line 3925
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v24

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3928
    if-eqz v21, :cond_18

    .line 3930
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "item.target = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3931
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "SIM/MSISDN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_18

    .line 3932
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "item.data = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3935
    :cond_18
    const/4 v6, 0x0

    .line 3936
    const/16 v21, 0x0

    goto/16 :goto_6

    .line 3940
    .end local v6    # "szFormat":Ljava/lang/String;
    .end local v7    # "szType":Ljava/lang/String;
    .restart local v26    # "szFormat":Ljava/lang/String;
    .restart local v28    # "szType":Ljava/lang/String;
    :cond_19
    const/4 v4, 0x0

    return v4
.end method

.method public xdmAgentCmdProp(Ljava/lang/String;Lcom/fmm/dm/eng/parser/XDMParserItem;Ljava/lang/Object;)I
    .locals 15
    .param p1, "szCmd"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/fmm/dm/eng/parser/XDMParserItem;
    .param p3, "p"    # Ljava/lang/Object;

    .prologue
    .line 4001
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 4003
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v7, 0x0

    .line 4004
    .local v7, "nodename":[C
    const/4 v10, 0x0

    .line 4005
    .local v10, "prop":[C
    const-string v14, ""

    .line 4006
    .local v14, "szData":Ljava/lang/String;
    const/4 v12, 0x0

    .line 4008
    .local v12, "ret":I
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .line 4010
    const-string v2, "Get"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v9, p3

    .line 4012
    check-cast v9, Lcom/fmm/dm/eng/parser/XDMParserGet;

    .line 4014
    .local v9, "get":Lcom/fmm/dm/eng/parser/XDMParserGet;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4016
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4017
    iget v2, v9, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 4018
    .local v13, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4019
    const/4 v2, 0x0

    .line 4092
    .end local v9    # "get":Lcom/fmm/dm/eng/parser/XDMParserGet;
    .end local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :goto_0
    return v2

    .line 4022
    .restart local v9    # "get":Lcom/fmm/dm/eng/parser/XDMParserGet;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4023
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    new-array v7, v2, [C

    .line 4024
    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/16 v3, 0x3f

    invoke-static {v2, v3, v7}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v14

    .line 4025
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4027
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4028
    iget v2, v9, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 4029
    .restart local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4030
    const/4 v2, 0x0

    goto :goto_0

    .line 4033
    .end local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4034
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    new-array v10, v2, [C

    .line 4035
    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/16 v3, 0x3d

    invoke-static {v2, v3, v10}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v14

    .line 4036
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4038
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4039
    iget v2, v9, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 4040
    .restart local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4041
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 4044
    .end local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v2, p0

    move-object v3, v1

    move-object/from16 v4, p2

    move-object v5, v14

    move-object v6, v10

    move-object/from16 v8, p3

    .line 4045
    invoke-virtual/range {v2 .. v8}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdPropGet(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParserItem;Ljava/lang/String;[C[CLjava/lang/Object;)I

    move-result v12

    .end local v9    # "get":Lcom/fmm/dm/eng/parser/XDMParserGet;
    :goto_1
    move v2, v12

    .line 4092
    goto/16 :goto_0

    .line 4047
    :cond_3
    const-string v2, "Replace"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v11, p3

    .line 4049
    check-cast v11, Lcom/fmm/dm/eng/parser/XDMParserReplace;

    .line 4051
    .local v11, "replace":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4053
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4054
    iget v2, v11, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 4055
    .restart local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4056
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 4059
    .end local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4060
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    new-array v7, v2, [C

    .line 4061
    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/16 v3, 0x3f

    invoke-static {v2, v3, v7}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v14

    .line 4062
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 4064
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4065
    iget v2, v11, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 4066
    .restart local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4067
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 4069
    .end local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4070
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    new-array v10, v2, [C

    .line 4071
    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/16 v3, 0x3d

    invoke-static {v2, v3, v10}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v14

    .line 4072
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v10}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4073
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4075
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4076
    iget v2, v11, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 4077
    .restart local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4078
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 4081
    .end local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v3, p0

    move-object v4, v1

    move-object/from16 v5, p2

    move-object v6, v14

    move-object/from16 v8, p3

    .line 4082
    invoke-virtual/range {v3 .. v8}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdPropReplace(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParserItem;Ljava/lang/String;[CLjava/lang/Object;)I

    move-result v12

    .line 4083
    goto/16 :goto_1

    .end local v11    # "replace":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    :cond_7
    move-object/from16 v9, p3

    .line 4086
    check-cast v9, Lcom/fmm/dm/eng/parser/XDMParserGet;

    .line 4088
    .restart local v9    # "get":Lcom/fmm/dm/eng/parser/XDMParserGet;
    iget v2, v9, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 4089
    .restart local v13    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4090
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public xdmAgentCmdPropGet(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParserItem;Ljava/lang/String;[C[CLjava/lang/Object;)I
    .locals 33
    .param p1, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p2, "item"    # Lcom/fmm/dm/eng/parser/XDMParserItem;
    .param p3, "szData"    # Ljava/lang/String;
    .param p4, "prop"    # [C
    .param p5, "pNodeName"    # [C
    .param p6, "pkg"    # Ljava/lang/Object;

    .prologue
    .line 4097
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    move-object/from16 v27, v0

    .local v27, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    move-object/from16 v24, p6

    .line 4098
    check-cast v24, Lcom/fmm/dm/eng/parser/XDMParserGet;

    .line 4102
    .local v24, "get":Lcom/fmm/dm/eng/parser/XDMParserGet;
    const/16 v16, 0x0

    .line 4103
    .local v16, "szOutbuf":Ljava/lang/String;
    const/4 v11, 0x0

    .line 4104
    .local v11, "chreBuf":[C
    const/16 v25, 0x0

    .line 4105
    .local v25, "nFileId":I
    const/16 v29, 0x0

    .line 4108
    .local v29, "ret":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdTNDS()I

    move-result v25

    .line 4110
    invoke-static/range {p5 .. p5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v31

    .line 4111
    .local v31, "szNodename":Ljava/lang/String;
    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v26

    .line 4113
    .local v26, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v26, :cond_0

    .line 4115
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "404"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4116
    .local v30, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4117
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .line 4287
    .end local v16    # "szOutbuf":Ljava/lang/String;
    :goto_0
    return v5

    .line 4119
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_0
    invoke-static/range {p4 .. p4}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v32

    .line 4120
    .local v32, "szPropname":Ljava/lang/String;
    const-string v5, "list"

    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_6

    .line 4123
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentIsAccessibleNode(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 4125
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "405"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4126
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4127
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .local v9, "szOutbuf":Ljava/lang/Object;
    goto :goto_0

    .line 4130
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_1
    const/16 v5, 0x8

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 4132
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "425"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4133
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4134
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .restart local v9    # "szOutbuf":Ljava/lang/Object;
    goto :goto_0

    .line 4137
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_2
    const-string v5, "Struct"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 4139
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdPropGetStruct(Lcom/fmm/dm/eng/parser/XDMParserGet;Lcom/fmm/dm/eng/core/XDMVnode;Z)V

    .line 4160
    :cond_3
    :goto_1
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "200"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4161
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4162
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .restart local v9    # "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_0

    .line 4141
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_4
    const-string v5, "StructData"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_5

    .line 4143
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdPropGetStruct(Lcom/fmm/dm/eng/parser/XDMParserGet;Lcom/fmm/dm/eng/core/XDMVnode;Z)V

    goto :goto_1

    .line 4145
    :cond_5
    const-string v5, "TNDS"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 4149
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v27

    move-object/from16 v3, v26

    move-object/from16 v4, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdPropGetTnds(Lcom/fmm/dm/eng/parser/XDMParserGet;Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;Ljava/lang/String;)Z

    move-result v29

    .line 4150
    if-nez v29, :cond_3

    .line 4152
    invoke-static/range {v25 .. v25}, Lcom/fmm/dm/db/file/XDB;->xdbDeleteFile(I)I

    .line 4153
    sget-object v5, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "404"

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4154
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    sget-object v5, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget-object v5, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4155
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .restart local v9    # "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_0

    .line 4166
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_6
    const/16 v5, 0x8

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v5

    if-nez v5, :cond_7

    .line 4168
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "425"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4169
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4170
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .restart local v9    # "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_0

    .line 4173
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_7
    const-string v5, "ACL"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_9

    .line 4175
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "200"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4176
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4178
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v5, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetAclStr(Lcom/fmm/dm/eng/core/XDMOmList;Lcom/fmm/dm/eng/parser/XDMParserItem;)Ljava/lang/String;

    move-result-object v9

    .line 4179
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .local v9, "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 4180
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 4183
    :cond_8
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "chr"

    const-string v9, "text/plain"

    .end local v9    # "szOutbuf":Ljava/lang/String;
    const/4 v10, 0x0

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v11}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 4184
    .local v28, "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4186
    const/4 v9, 0x0

    .line 4187
    .restart local v9    # "szOutbuf":Ljava/lang/String;
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 4189
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_9
    const-string v5, "Format"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_b

    .line 4191
    move-object/from16 v0, v26

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v9

    .line 4192
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .restart local v9    # "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 4193
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 4195
    :cond_a
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .end local v9    # "szOutbuf":Ljava/lang/String;
    const-string v10, "200"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4196
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4199
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "chr"

    const-string v9, "text/plain"

    const/4 v10, 0x0

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v11}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 4200
    .restart local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4202
    const/4 v9, 0x0

    .line 4287
    .end local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    :goto_2
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 4204
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_b
    const-string v5, "Type"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_e

    .line 4206
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "200"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4207
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4209
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v5, :cond_d

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    iget-object v5, v5, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v5, :cond_d

    .line 4211
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    iget-object v5, v5, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 4212
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .restart local v9    # "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 4213
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 4215
    :cond_c
    move-object/from16 v0, v24

    iget v6, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "chr"

    const/4 v10, 0x0

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v11}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 4216
    .restart local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4218
    const/4 v9, 0x0

    goto :goto_2

    .line 4222
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, v24

    iget v13, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v15, ""

    const/16 v17, 0x0

    move-object/from16 v12, p1

    move-object/from16 v18, v11

    invoke-static/range {v12 .. v18}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 4223
    .restart local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    move-object/from16 v9, v16

    .local v9, "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_2

    .line 4227
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_e
    const-string v5, "Size"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_11

    .line 4229
    move-object/from16 v0, v26

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    if-ltz v5, :cond_10

    move-object/from16 v0, v26

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-lez v5, :cond_10

    .line 4231
    move-object/from16 v0, v24

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "200"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4232
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4234
    move-object/from16 v0, v26

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 4235
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .local v9, "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 4236
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 4239
    :cond_f
    move-object/from16 v0, v24

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "chr"

    const-string v21, "text/plain"

    const/16 v22, 0x0

    move-object/from16 v17, p1

    move-object/from16 v23, v11

    invoke-static/range {v17 .. v23}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 4240
    .restart local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4242
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 4246
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_10
    move-object/from16 v0, v24

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "406"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4247
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    move-object/from16 v9, v16

    .local v9, "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_2

    .line 4250
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_11
    const-string v5, "Name"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_13

    .line 4252
    move-object/from16 v0, v24

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "200"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4253
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4255
    move-object/from16 v0, v26

    iget-object v9, v0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 4256
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .local v9, "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 4257
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 4260
    :cond_12
    move-object/from16 v0, v24

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "chr"

    const-string v21, "text/plain"

    const/16 v22, 0x0

    move-object/from16 v17, p1

    move-object/from16 v23, v11

    invoke-static/range {v17 .. v23}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 4261
    .restart local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4263
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 4265
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_13
    const-string v5, "Title"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_15

    .line 4267
    move-object/from16 v0, v24

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "200"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4268
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4270
    move-object/from16 v0, v26

    iget-object v9, v0, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 4271
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .restart local v9    # "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 4272
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 4275
    :cond_14
    move-object/from16 v0, v24

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "chr"

    const-string v21, "text/plain"

    const/16 v22, 0x0

    move-object/from16 v17, p1

    move-object/from16 v23, v11

    invoke-static/range {v17 .. v23}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 4276
    .restart local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4278
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 4282
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_15
    move-object/from16 v0, v24

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "405"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 4283
    .restart local v30    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4284
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .local v9, "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_0
.end method

.method public xdmAgentCmdPropGetStruct(Lcom/fmm/dm/eng/parser/XDMParserGet;Lcom/fmm/dm/eng/core/XDMVnode;Z)V
    .locals 11
    .param p1, "get"    # Lcom/fmm/dm/eng/parser/XDMParserGet;
    .param p2, "node"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p3, "makedata"    # Z

    .prologue
    .line 4298
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 4299
    .local v0, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v9, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 4301
    .local v9, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const-string v2, ""

    .line 4302
    .local v2, "szName":Ljava/lang/String;
    const-string v3, ""

    .line 4303
    .local v3, "szFormat":Ljava/lang/String;
    const-string v4, ""

    .line 4304
    .local v4, "szType":Ljava/lang/String;
    const/4 v6, 0x0

    .line 4305
    .local v6, "data":[C
    const/4 v8, 0x0

    .line 4307
    .local v8, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    const/4 v5, 0x0

    .line 4309
    .local v5, "datasize":I
    if-nez p2, :cond_1

    .line 4363
    :cond_0
    return-void

    .line 4314
    :cond_1
    iget-object v8, p2, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 4316
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-virtual {p0, v1, p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetPathFromNode(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/lang/String;

    move-result-object v2

    .line 4318
    if-eqz p3, :cond_5

    .line 4320
    iget v1, p2, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    if-ltz v1, :cond_4

    iget v1, p2, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-lez v1, :cond_4

    .line 4322
    iget v1, p2, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    if-lez v1, :cond_3

    .line 4324
    iget v1, p2, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v3

    .line 4331
    :goto_0
    iget v7, p2, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 4332
    .local v7, "bufsize":I
    new-array v6, v7, [C

    .line 4334
    const/4 v1, 0x0

    invoke-static {v9, v2, v1, v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 4335
    iget v5, p2, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 4348
    .end local v7    # "bufsize":I
    :goto_1
    iget-object v1, p2, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentIsAccessibleNode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4350
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v10

    .line 4351
    .local v10, "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v10}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4353
    .end local v10    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    :cond_2
    const/4 v2, 0x0

    .line 4354
    const/4 v3, 0x0

    .line 4355
    const/4 v4, 0x0

    .line 4356
    const/4 v6, 0x0

    .line 4358
    :goto_2
    if-eqz v8, :cond_0

    .line 4360
    invoke-virtual {p0, p1, v8, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdPropGetStruct(Lcom/fmm/dm/eng/parser/XDMParserGet;Lcom/fmm/dm/eng/core/XDMVnode;Z)V

    .line 4361
    iget-object v8, v8, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_2

    .line 4328
    :cond_3
    const-string v3, ""

    goto :goto_0

    .line 4340
    :cond_4
    iget v1, p2, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 4345
    :cond_5
    iget v1, p2, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public xdmAgentCmdPropGetTnds(Lcom/fmm/dm/eng/parser/XDMParserGet;Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;Ljava/lang/String;)Z
    .locals 23
    .param p1, "get"    # Lcom/fmm/dm/eng/parser/XDMParserGet;
    .param p2, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p3, "node"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p4, "szPropertylist"    # Ljava/lang/String;

    .prologue
    .line 4730
    const-string v19, ""

    .line 4731
    .local v19, "szTemp":Ljava/lang/String;
    const-string v18, ""

    .line 4732
    .local v18, "szTag":Ljava/lang/String;
    const-string v5, ""

    .line 4733
    .local v5, "szName":Ljava/lang/String;
    const-string v6, ""

    .line 4734
    .local v6, "szFormat":Ljava/lang/String;
    const-string v17, ""

    .line 4735
    .local v17, "szData":Ljava/lang/String;
    const/4 v14, 0x0

    .line 4736
    .local v14, "nFlag":I
    const/4 v8, 0x0

    .line 4737
    .local v8, "nSize":I
    const-string v21, ""

    .line 4738
    .local v21, "szToken":Ljava/lang/String;
    const/4 v15, 0x0

    .line 4739
    .local v15, "ptr":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 4740
    .local v16, "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    sget-object v3, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 4741
    .local v3, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/16 v22, 0x0

    .line 4744
    .local v22, "tempPath":[C
    const/4 v13, 0x0

    .line 4746
    .local v13, "nFileId":I
    const-string v4, ""

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4747
    if-nez p3, :cond_0

    .line 4749
    const/4 v4, 0x0

    .line 4868
    :goto_0
    return v4

    .line 4752
    :cond_0
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    if-nez v4, :cond_1

    .line 4754
    const/4 v4, 0x0

    goto :goto_0

    .line 4757
    :cond_1
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdTNDS()I

    move-result v13

    .line 4759
    const-string v4, "\\+"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 4761
    if-nez v15, :cond_2

    .line 4763
    const/4 v4, 0x0

    goto :goto_0

    .line 4766
    :cond_2
    const/4 v4, 0x0

    aget-object v21, v15, v4

    .line 4767
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "token : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4768
    array-length v4, v15

    const/4 v7, 0x1

    if-le v4, v7, :cond_7

    .line 4770
    const/4 v11, 0x1

    .line 4772
    .local v11, "i":I
    :goto_1
    array-length v4, v15

    if-ge v11, v4, :cond_8

    .line 4774
    const-string v4, "ACL"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_4

    .line 4776
    or-int/lit8 v14, v14, 0x1

    .line 4791
    :cond_3
    :goto_2
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "i":I
    .local v12, "i":I
    aget-object v21, v15, v11

    move v11, v12

    .end local v12    # "i":I
    .restart local v11    # "i":I
    goto :goto_1

    .line 4778
    :cond_4
    const-string v4, "Format"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_5

    .line 4780
    or-int/lit8 v14, v14, 0x2

    goto :goto_2

    .line 4782
    :cond_5
    const-string v4, "Type"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_6

    .line 4784
    or-int/lit8 v14, v14, 0x4

    goto :goto_2

    .line 4786
    :cond_6
    const-string v4, "Value"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    .line 4788
    or-int/lit8 v14, v14, 0x8

    goto :goto_2

    .line 4796
    .end local v11    # "i":I
    :cond_7
    const/16 v14, 0xd

    .line 4797
    const-string v4, "-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 4798
    if-eqz v15, :cond_8

    .line 4800
    const/4 v4, 0x0

    aget-object v21, v15, v4

    .line 4801
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "token : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4803
    const-string v4, "ACL"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_9

    .line 4805
    and-int/lit16 v14, v14, 0xfe

    .line 4822
    :cond_8
    :goto_3
    invoke-static {v13}, Lcom/fmm/dm/db/file/XDB;->xdbDeleteFile(I)I

    .line 4824
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v7, 0x22

    aget-object v18, v4, v7

    .line 4825
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 4827
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v18, v4, v7

    .line 4828
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 4830
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/4 v7, 0x2

    aget-object v18, v4, v7

    .line 4831
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 4832
    const-string v4, "1.2"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 4834
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/4 v7, 0x3

    aget-object v18, v4, v7

    .line 4835
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 4836
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v13, v4}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 4838
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetPathFromNode(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/lang/String;

    move-result-object v20

    .line 4839
    .local v20, "szTempPath":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [C

    move-object/from16 v22, v0

    .line 4840
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 4841
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "tempPath : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v22 .. v22}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4842
    invoke-static/range {v22 .. v22}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2, v14, v4}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeTndsSubTree(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;ILjava/lang/String;)V

    .line 4844
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/4 v7, 0x1

    aget-object v18, v4, v7

    .line 4845
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v13, v4}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 4847
    sget-object v4, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v7, 0x23

    aget-object v18, v4, v7

    .line 4848
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v13, v4}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 4850
    invoke-static {v13}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileSize(I)I

    move-result v8

    .line 4851
    const/4 v4, 0x0

    invoke-static {v13, v4, v8}, Lcom/fmm/dm/db/file/XDB;->xdbReadFile(III)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    move-object v10, v4

    check-cast v10, [B

    .line 4852
    .local v10, "bTemp":[B
    new-instance v17, Ljava/lang/String;

    .end local v17    # "szData":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-direct {v0, v10}, Ljava/lang/String;-><init>([B)V

    .line 4853
    .restart local v17    # "szData":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetPathFromNode(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/lang/String;

    move-result-object v5

    .line 4854
    const/16 v4, 0x8

    invoke-static {v4}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v6

    .line 4856
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "name : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 4857
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 4859
    const-string v4, "_____ TNDSResults File Read Error!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4860
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "application/vnd.syncml.dmtnds+xml"

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v16

    .line 4866
    :goto_4
    iget-object v4, v3, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4868
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 4807
    .end local v10    # "bTemp":[B
    .end local v20    # "szTempPath":Ljava/lang/String;
    :cond_9
    const-string v4, "Format"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_a

    .line 4809
    and-int/lit16 v14, v14, 0xfd

    goto/16 :goto_3

    .line 4811
    :cond_a
    const-string v4, "Type"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_b

    .line 4813
    and-int/lit16 v14, v14, 0xfb

    goto/16 :goto_3

    .line 4815
    :cond_b
    const-string v4, "Value"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_8

    .line 4817
    and-int/lit16 v14, v14, 0xf7

    goto/16 :goto_3

    .line 4864
    .restart local v10    # "bTemp":[B
    .restart local v20    # "szTempPath":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p1

    iget v4, v0, Lcom/fmm/dm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "application/vnd.syncml.dmtnds+xml"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    invoke-static/range {v3 .. v9}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/fmm/dm/eng/parser/XDMParserResults;

    move-result-object v16

    goto :goto_4
.end method

.method public xdmAgentCmdPropReplace(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/parser/XDMParserItem;Ljava/lang/String;[CLjava/lang/Object;)I
    .locals 14
    .param p1, "ws"    # Lcom/fmm/dm/eng/core/XDMWorkspace;
    .param p2, "item"    # Lcom/fmm/dm/eng/parser/XDMParserItem;
    .param p3, "szData"    # Ljava/lang/String;
    .param p4, "pNodeName"    # [C
    .param p5, "pkg"    # Ljava/lang/Object;

    .prologue
    .line 4411
    iget-object v9, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .local v9, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    move-object/from16 v10, p5

    .line 4412
    check-cast v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;

    .line 4415
    .local v10, "replace":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    const/4 v13, 0x0

    .line 4416
    .local v13, "szOutbuf":Ljava/lang/String;
    const/4 v7, 0x0

    .line 4418
    .local v7, "acllist":Lcom/fmm/dm/eng/core/XDMOmList;
    invoke-static/range {p4 .. p4}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v12

    .line 4419
    .local v12, "szNodeName":Ljava/lang/String;
    invoke-static {v9, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 4420
    .local v8, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v8, :cond_0

    .line 4422
    const-string v1, "!node"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4424
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "404"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4425
    .local v11, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4426
    const/4 v1, 0x0

    .line 4515
    :goto_0
    return v1

    .line 4429
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_0
    invoke-virtual {p0, v9, v12}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentIsPermanentNode(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4431
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4432
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4433
    const/4 v1, 0x0

    goto :goto_0

    .line 4439
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_1
    const/16 v1, 0x10

    invoke-static {v9, v8, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 4441
    const-string v1, "!XDM_OMACL_REPLACE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4443
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "425"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4444
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4445
    const/4 v1, 0x0

    goto :goto_0

    .line 4448
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_2
    const-string v1, "ACL"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 4450
    const-string v1, "ACL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4453
    iget v1, v8, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_3

    .line 4455
    iget-object v1, v8, Lcom/fmm/dm/eng/core/XDMVnode;->ptParentNode:Lcom/fmm/dm/eng/core/XDMVnode;

    const/16 v2, 0x10

    invoke-static {v9, v1, v2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4457
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "425"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4458
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4459
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "STATUS_COMMAND_NOT_ALLOWED="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4460
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 4463
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_3
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v13

    .line 4464
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4466
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    .line 4468
    :cond_4
    invoke-virtual {p0, v7, v13}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeAcl(Lcom/fmm/dm/eng/core/XDMOmList;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object v7

    .line 4469
    const/4 v13, 0x0

    .line 4470
    iget-object v1, v8, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmDeleteAclList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 4471
    iput-object v7, v8, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 4473
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "200"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4474
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4515
    :cond_5
    :goto_1
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 4476
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_6
    const-string v1, "Format"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_7

    .line 4478
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4479
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_1

    .line 4481
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_7
    const-string v1, "Type"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 4483
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4484
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_1

    .line 4486
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_8
    const-string v1, "Size"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_9

    .line 4488
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4489
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_1

    .line 4491
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_9
    const-string v1, "Name"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_a

    .line 4493
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4494
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 4496
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_a
    const-string v1, "Title"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_b

    .line 4498
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "200"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4499
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4500
    const/4 v1, 0x0

    iput-object v1, v8, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 4501
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 4502
    iget-object v1, v8, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4504
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_5

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_5

    .line 4506
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/fmm/dm/eng/core/XDMVnode;->title:Ljava/lang/String;

    goto/16 :goto_1

    .line 4512
    .end local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_b
    iget v2, v10, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 4513
    .restart local v11    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method public xdmAgentCmdReplace(Lcom/fmm/dm/eng/parser/XDMParserReplace;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I
    .locals 23
    .param p1, "replace"    # Lcom/fmm/dm/eng/parser/XDMParserReplace;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .prologue
    .line 7569
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 7570
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v0, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    move-object/from16 v16, v0

    .line 7573
    .local v16, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/16 v22, 0x0

    .line 7574
    .local v22, "szType":Ljava/lang/String;
    const/16 v20, 0x0

    .line 7577
    .local v20, "szBuf":Ljava/lang/String;
    const/16 v10, 0xc

    .line 7580
    .local v10, "format":I
    const/4 v8, 0x0

    .line 7581
    .local v8, "bufsize":I
    const/16 v18, 0x0

    .line 7582
    .local v18, "res":I
    const/4 v14, 0x0

    .line 7584
    .local v14, "nFileId":I
    invoke-virtual/range {p0 .. p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v17

    .line 7587
    .local v17, "process":Z
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdTNDS()I

    move-result v14

    .line 7589
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 7590
    .local v9, "cur":Lcom/fmm/dm/eng/core/XDMList;
    :goto_0
    if-eqz v9, :cond_52

    .line 7592
    iget-object v11, v9, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    check-cast v11, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 7594
    .local v11, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v3, :cond_1

    .line 7596
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v3, :cond_0

    .line 7597
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iput-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 7617
    :cond_0
    :goto_1
    const/16 v19, 0x0

    .line 7619
    .local v19, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    .line 7621
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 7623
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v7, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7630
    :goto_2
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7631
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7632
    goto :goto_0

    .line 7601
    .end local v19    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v3, :cond_0

    .line 7603
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 7605
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 7606
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 7609
    :cond_2
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7611
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 7612
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    goto :goto_1

    .line 7627
    .restart local v19    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_3
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto :goto_2

    .line 7634
    :cond_4
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v3, :cond_5

    .line 7636
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget v3, v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7640
    :cond_5
    if-eqz p2, :cond_e

    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v4, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    if-eq v3, v4, :cond_e

    .line 7642
    const/16 v19, 0x0

    .line 7643
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    if-eqz v3, :cond_a

    .line 7646
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-virtual {v3, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 7648
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 7650
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7656
    :goto_3
    sget-object v3, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    .line 7657
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->tmpItem:Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 7697
    :goto_4
    if-eqz v19, :cond_6

    .line 7699
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7701
    :cond_6
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7702
    goto/16 :goto_0

    .line 7654
    :cond_7
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto :goto_3

    .line 7661
    :cond_8
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 7663
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto :goto_4

    .line 7667
    :cond_9
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto :goto_4

    .line 7673
    :cond_a
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v4, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    if-ne v3, v4, :cond_c

    .line 7675
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 7677
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto :goto_4

    .line 7681
    :cond_b
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto :goto_4

    .line 7686
    :cond_c
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 7688
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto/16 :goto_4

    .line 7692
    :cond_d
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto/16 :goto_4

    .line 7705
    :cond_e
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 7707
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7708
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7709
    goto/16 :goto_0

    .line 7713
    :cond_f
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 7715
    const-string v3, "Replace"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v3, v11, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdProp(Ljava/lang/String;Lcom/fmm/dm/eng/parser/XDMParserItem;Ljava/lang/Object;)I

    move-result v18

    .line 7716
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7717
    goto/16 :goto_0

    .line 7720
    :cond_10
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v15

    .line 7721
    .local v15, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v17, :cond_13

    .line 7723
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7727
    if-eqz p2, :cond_11

    if-eqz p3, :cond_11

    .line 7729
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 7733
    :cond_11
    if-eqz v19, :cond_12

    .line 7735
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7737
    :cond_12
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7738
    goto/16 :goto_0

    .line 7740
    :cond_13
    if-nez v15, :cond_16

    .line 7742
    const-string v3, "node == null(not exist)"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7743
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7745
    if-eqz p2, :cond_14

    if-eqz p3, :cond_14

    .line 7746
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 7749
    :cond_14
    if-eqz v19, :cond_15

    .line 7750
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7752
    :cond_15
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7753
    goto/16 :goto_0

    .line 7755
    :cond_16
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentIsPermanentNode(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 7757
    const-string v3, "Fail"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7758
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "405"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7762
    if-eqz p2, :cond_17

    if-eqz p3, :cond_17

    .line 7764
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 7768
    :cond_17
    if-eqz v19, :cond_18

    .line 7770
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7772
    :cond_18
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7773
    goto/16 :goto_0

    .line 7775
    :cond_19
    const/16 v3, 0x10

    move-object/from16 v0, v16

    invoke-static {v0, v15, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;I)Z

    move-result v3

    if-nez v3, :cond_1c

    .line 7777
    const-string v3, "xdmOmCheckAcl Fail"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7778
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "425"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7782
    if-eqz p2, :cond_1a

    if-eqz p3, :cond_1a

    .line 7784
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 7788
    :cond_1a
    if-eqz v19, :cond_1b

    .line 7790
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7792
    :cond_1b
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7793
    goto/16 :goto_0

    .line 7796
    :cond_1c
    const-string v3, "else"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7797
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 7799
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v3, :cond_27

    .line 7801
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1d

    .line 7803
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v0, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 7805
    :cond_1d
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    .line 7807
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatFromString(Ljava/lang/String;)I

    move-result v10

    .line 7810
    :cond_1e
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    if-lez v3, :cond_23

    .line 7812
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget v3, v3, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 7813
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7827
    :cond_1f
    :goto_5
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v3, :cond_26

    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v3, :cond_26

    .line 7829
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v20

    .line 7830
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 7832
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget v8, v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 7833
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v20

    .line 7867
    :cond_20
    :goto_6
    iget-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-eqz v3, :cond_2b

    .line 7869
    const-string v3, "REPLACE ws.nTNDSFlag = true"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7871
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 7873
    const-string v20, ""

    .line 7875
    :cond_21
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {v14, v3}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 7876
    iget v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_2a

    .line 7878
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7879
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7880
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 7881
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 7882
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v3, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmDeleteImplicit(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    move-result v18

    .line 7883
    if-ltz v18, :cond_22

    .line 7885
    move-object/from16 v0, v16

    invoke-static {v14, v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNodeFromFile(ILcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v18

    .line 7888
    :cond_22
    if-lez v18, :cond_29

    .line 7890
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7891
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7892
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7893
    goto/16 :goto_0

    .line 7815
    :cond_23
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    if-nez v3, :cond_1f

    .line 7817
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v3, :cond_24

    .line 7819
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget v3, v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto/16 :goto_5

    .line 7823
    :cond_24
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto/16 :goto_5

    .line 7837
    :cond_25
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v8

    goto :goto_6

    .line 7842
    :cond_26
    const/4 v8, 0x0

    .line 7843
    const/16 v20, 0x0

    .line 7844
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 7845
    const-string v3, "REPLACE ( no item->data)"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_6

    .line 7848
    :cond_27
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    if-eqz v3, :cond_20

    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v3, :cond_20

    .line 7850
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/fmm/dm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v20

    .line 7851
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 7853
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget v8, v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 7854
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    iget-object v3, v3, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v20

    .line 7860
    :goto_7
    iget-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v3, :cond_20

    .line 7862
    iput v8, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto/16 :goto_6

    .line 7858
    :cond_28
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v8

    goto :goto_7

    .line 7895
    :cond_29
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7896
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7897
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7898
    goto/16 :goto_0

    .line 7901
    :cond_2a
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7902
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7903
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7904
    goto/16 :goto_0

    .line 7907
    :cond_2b
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_31

    .line 7909
    const-string v3, "application/vnd.syncml.dmtnds+xml"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_30

    .line 7911
    iget v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_2e

    .line 7913
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v3, v8

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7914
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7916
    iget-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-nez v3, :cond_2c

    .line 7918
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 7919
    invoke-static {v14}, Lcom/fmm/dm/db/file/XDB;->xdbDeleteFile(I)I

    .line 7922
    :cond_2c
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 7924
    const-string v20, ""

    .line 7926
    :cond_2d
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {v14, v3}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 7928
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7929
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7930
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7931
    goto/16 :goto_0

    .line 7933
    :cond_2e
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v3, v4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmDeleteImplicit(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    move-result v18

    .line 7934
    if-ltz v18, :cond_2f

    .line 7936
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-static {v0, v8, v1}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v18

    .line 7938
    if-lez v18, :cond_2f

    .line 7940
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7941
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7942
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7943
    goto/16 :goto_0

    .line 7947
    :cond_2f
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7948
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7949
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7950
    const-string v3, "Delete Fail.\n"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 7951
    const/4 v3, -0x1

    .line 8188
    .end local v11    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v15    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .end local v19    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :goto_8
    return v3

    .line 7953
    .restart local v11    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .restart local v15    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .restart local v19    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_30
    const-string v3, "application/vnd.syncml.dmtnds+wbxml"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_31

    .line 7955
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "406"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 7956
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7957
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    .line 7958
    const-string v3, "Not Support TNDS with WBXML Type.\n"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 7959
    const/4 v3, -0x1

    goto :goto_8

    .line 7966
    :cond_31
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsLawmoPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 7968
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsLockMyPhoneMessagesPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 7970
    const/4 v3, 0x1

    if-ne v10, v3, :cond_39

    .line 7972
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentDecodeUtf8Message(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 7973
    .local v21, "szData":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtLockMyPhoneMessages(Ljava/lang/String;)V

    .line 7980
    .end local v21    # "szData":Ljava/lang/String;
    :goto_9
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_32

    const-string v3, "null"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_32

    const/16 v3, 0xc

    if-eq v10, v3, :cond_32

    .line 7982
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentSetOperationCategory(I)V

    .line 8022
    :cond_32
    :goto_a
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsWipeListPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_40

    .line 8024
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsExternalToBeWipePath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 8026
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExternalToBeWiped(Z)V

    .line 8064
    :cond_33
    :goto_b
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentPolicyPeriodPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_46

    .line 8066
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetPolicyPeriod(Ljava/lang/String;)V

    .line 8067
    new-instance v12, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;

    invoke-direct {v12}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;-><init>()V

    .line 8068
    .local v12, "lawmoAgent":Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;
    invoke-virtual {v12}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentOSPSReMakeNode()V

    .line 8086
    .end local v12    # "lawmoAgent":Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;
    :cond_34
    :goto_c
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentIsMobileTrackingPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 8088
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentIsPolicyStartDatePath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_49

    .line 8090
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetPolicyStartDate(Ljava/lang/String;)V

    .line 8107
    :cond_35
    :goto_d
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 8108
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 8109
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 8110
    iget-object v4, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    iget v6, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    move-object/from16 v3, v16

    move-object/from16 v7, v20

    invoke-static/range {v3 .. v8}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v18

    .line 8113
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4c

    .line 8115
    const/16 v20, 0x0

    .line 8123
    :cond_36
    :goto_e
    if-gez v18, :cond_4d

    .line 8126
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    .line 8130
    if-eqz p2, :cond_37

    if-eqz p3, :cond_37

    .line 8132
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 8181
    :cond_37
    :goto_f
    if-eqz v19, :cond_38

    .line 8183
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8185
    :cond_38
    iget-object v9, v9, Lcom/fmm/dm/eng/core/XDMList;->next:Lcom/fmm/dm/eng/core/XDMList;

    goto/16 :goto_0

    .line 7977
    :cond_39
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtLockMyPhoneMessages(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 7985
    :cond_3a
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsLockMyPhoneReactivationLockPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3b

    .line 7987
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtReactivationLock(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 7989
    :cond_3b
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsPasswordPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3c

    .line 7991
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtPassword(Ljava/lang/String;)V

    .line 7992
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_32

    const-string v3, "null"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_32

    const/16 v3, 0xc

    if-eq v10, v3, :cond_32

    .line 7994
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentSetOperationCategory(I)V

    goto/16 :goto_a

    .line 7997
    :cond_3c
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsCallRestrictionPhoneNumberPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 7999
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtCallRestrictionPhoneNumber(Ljava/lang/String;)V

    .line 8000
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_32

    const-string v3, "null"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_32

    const/16 v3, 0xc

    if-eq v10, v3, :cond_32

    .line 8002
    const/4 v3, 0x4

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentSetOperationCategory(I)V

    goto/16 :goto_a

    .line 8005
    :cond_3d
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsRingMyPhoneMessagesPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 8007
    const/4 v3, 0x1

    if-ne v10, v3, :cond_3e

    .line 8009
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentDecodeUtf8Message(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 8010
    .restart local v21    # "szData":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtRingMyPhoneMessages(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 8014
    .end local v21    # "szData":Ljava/lang/String;
    :cond_3e
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetExtRingMyPhoneMessages(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 8028
    :cond_3f
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentIsSIMToBeWipePath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 8030
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetSIMToBeWiped(Z)V

    goto/16 :goto_b

    .line 8033
    :cond_40
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentForwardingPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 8035
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentNumbersCallForwardingbPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_41

    .line 8037
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetCallForwardingNumbers(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 8039
    :cond_41
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentNumbersSMSForwardingbPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_42

    .line 8041
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetSMSForwardingNumbers(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 8043
    :cond_42
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentToBeEnabledCallForwardingbPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_43

    .line 8045
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetCallForwardingToBeEnabled(Z)V

    goto/16 :goto_b

    .line 8047
    :cond_43
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentToBeEnabledSMSForwardingbPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_44

    .line 8049
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetSMSForwardingToBeEnabled(Z)V

    goto/16 :goto_b

    .line 8051
    :cond_44
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentToBeDisabledCallForwardingbPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_45

    .line 8053
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetCallForwardingToBeDisabled(Z)V

    goto/16 :goto_b

    .line 8055
    :cond_45
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentToBeDisabledSMSForwardingbPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 8057
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetSMSForwardingToBeDisabled(Z)V

    goto/16 :goto_b

    .line 8070
    :cond_46
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentEmergencyModeStatePath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_47

    .line 8072
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetEmergencyModeState(Z)V

    goto/16 :goto_c

    .line 8074
    :cond_47
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentEmergencyModeRequestorPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_48

    .line 8076
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpSetEmergencyModeRequestor(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 8078
    :cond_48
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentReactivationLockStatePath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_34

    .line 8080
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetReactivationState(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 8092
    :cond_49
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentIsPolicyEndDatePath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4a

    .line 8094
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetPolicyEndDate(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 8096
    :cond_4a
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentIsPolicyIntervalPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4b

    .line 8098
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetPolicyInterval(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 8100
    :cond_4b
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentIsRequestorPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 8102
    invoke-static/range {v20 .. v20}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocationRequestor(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 8117
    :cond_4c
    if-eqz v15, :cond_36

    .line 8119
    const/4 v3, -0x1

    iput v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->vaddr:I

    .line 8120
    const/4 v3, 0x0

    iput v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    goto/16 :goto_e

    .line 8139
    :cond_4d
    iget-object v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v15

    .line 8140
    if-eqz v15, :cond_50

    .line 8142
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4f

    .line 8144
    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v3, :cond_4e

    .line 8146
    iget-object v3, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 8148
    :cond_4e
    new-instance v13, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v13}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 8149
    .local v13, "list":Lcom/fmm/dm/eng/core/XDMOmList;
    move-object/from16 v0, v22

    iput-object v0, v13, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 8150
    const/4 v3, 0x0

    iput-object v3, v13, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 8151
    iput-object v13, v15, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 8154
    .end local v13    # "list":Lcom/fmm/dm/eng/core/XDMOmList;
    :cond_4f
    if-lez v10, :cond_50

    .line 8156
    iput v10, v15, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 8162
    :cond_50
    iget v3, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_51

    .line 8164
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 8165
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8166
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 8169
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto/16 :goto_f

    .line 8173
    :cond_51
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v3, v8

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 8174
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 8177
    move-object/from16 v0, p1

    iget v3, v0, Lcom/fmm/dm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v19

    goto/16 :goto_f

    .line 8188
    .end local v11    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v15    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .end local v19    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_52
    const/4 v3, 0x0

    goto/16 :goto_8
.end method

.method public xdmAgentCmdSequence(Lcom/fmm/dm/eng/parser/XDMParserSequence;)I
    .locals 10
    .param p1, "sequence"    # Lcom/fmm/dm/eng/parser/XDMParserSequence;

    .prologue
    const/4 v3, 0x0

    const/4 v9, -0x1

    const/4 v8, -0x5

    .line 8758
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 8760
    .local v0, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v6, 0x0

    .line 8762
    .local v6, "res":I
    iget-boolean v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    if-nez v1, :cond_0

    .line 8764
    iget v1, p1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->cmdid:I

    const-string v2, "Sequence"

    const-string v5, "200"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v7

    .line 8765
    .local v7, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v7}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 8767
    .end local v7    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_0
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_1

    .line 8769
    iget-object v1, p1, Lcom/fmm/dm/eng/parser/XDMParserSequence;->itemlist:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdSequenceBlock(Lcom/fmm/dm/eng/core/XDMLinkedList;)I

    move-result v6

    .line 8776
    if-ne v6, v8, :cond_2

    move v1, v8

    .line 8787
    :goto_0
    return v1

    :cond_1
    move v1, v9

    .line 8773
    goto :goto_0

    .line 8781
    :cond_2
    if-gez v6, :cond_3

    move v1, v9

    .line 8783
    goto :goto_0

    :cond_3
    move v1, v6

    .line 8787
    goto :goto_0
.end method

.method public xdmAgentCmdSequenceBlock(Lcom/fmm/dm/eng/core/XDMLinkedList;)I
    .locals 9
    .param p1, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;

    .prologue
    const/4 v8, 0x0

    const/4 v5, -0x5

    .line 5682
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 5684
    .local v4, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v3, 0x0

    .line 5685
    .local v3, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    const/4 v2, 0x0

    .line 5686
    .local v2, "res":I
    const/4 v1, 0x0

    .line 5688
    .local v1, "isAtomic":Z
    invoke-static {p1, v8}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 5689
    invoke-static {p1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 5691
    .local v0, "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    if-eqz v0, :cond_0

    .line 5692
    const/4 v6, 0x1

    iput-boolean v6, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    .line 5693
    :cond_0
    if-eqz v0, :cond_9

    .line 5698
    const-string v6, "Get"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Exec"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Alert"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Add"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Replace"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Copy"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Delete"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    .line 5701
    :cond_1
    iget v6, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    .line 5704
    :cond_2
    const-string v6, "Atomic_Start"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_4

    .line 5706
    const/4 v1, 0x1

    .line 5707
    const-string v6, "Atomic_Start"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5709
    iget-boolean v6, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-eqz v6, :cond_3

    .line 5711
    iget-object v6, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    invoke-virtual {p0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdAtomic(Lcom/fmm/dm/eng/parser/XDMParserAtomic;)I

    move-result v2

    .line 5717
    :goto_0
    const/4 v1, 0x0

    .line 5736
    :goto_1
    invoke-static {p1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 5737
    .restart local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    invoke-static {p1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 5739
    if-ne v2, v5, :cond_7

    .line 5743
    iput-object p1, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->sequenceList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 5761
    :goto_2
    return v5

    .line 5715
    :cond_3
    invoke-virtual {p0, v0, v1, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentVerifyCmd(Lcom/fmm/dm/agent/XDMAgent;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v2

    goto :goto_0

    .line 5719
    :cond_4
    const-string v6, "Sequence_Start"

    iget-object v7, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_6

    .line 5721
    const-string v6, "Sequence_Start"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5722
    iget-boolean v6, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v6, :cond_5

    .line 5724
    iget-object v6, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdSequence(Lcom/fmm/dm/eng/parser/XDMParserSequence;)I

    move-result v2

    goto :goto_1

    .line 5728
    :cond_5
    invoke-virtual {p0, v0, v1, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentVerifyCmd(Lcom/fmm/dm/agent/XDMAgent;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v2

    goto :goto_1

    .line 5733
    :cond_6
    invoke-virtual {p0, v0, v1, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentVerifyCmd(Lcom/fmm/dm/agent/XDMAgent;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v2

    goto :goto_1

    .line 5746
    :cond_7
    const/4 v6, 0x3

    if-ne v2, v6, :cond_8

    move v5, v2

    .line 5748
    goto :goto_2

    .line 5750
    :cond_8
    if-eqz v2, :cond_0

    .line 5752
    const-string v5, "Processing failed"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5753
    const/4 v5, -0x1

    goto :goto_2

    .line 5759
    :cond_9
    invoke-static {p1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 5760
    iput-boolean v8, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    move v5, v2

    .line 5761
    goto :goto_2
.end method

.method public xdmAgentCmdStatus(Lcom/fmm/dm/eng/parser/XDMParserStatus;)I
    .locals 11
    .param p1, "status"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 5870
    sget-object v5, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 5873
    .local v5, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v0, 0x0

    .line 5875
    .local v0, "dValue":[B
    const-string v6, "401"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    .line 5877
    const/4 v6, -0x7

    iput v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    .line 5878
    const-string v6, "Client invalid credential(401)"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5879
    const-string v6, "SyncHdr"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_0

    .line 5881
    const-string v6, "SyncHdr Status 401. and No Chal"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5882
    const/4 v6, 0x3

    iput v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->authCount:I

    .line 5920
    :cond_0
    :goto_0
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v6, :cond_9

    .line 5922
    const-string v6, "syncml:auth-md5"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_b

    .line 5924
    iput v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    .line 5925
    iget v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetAuthType(I)V

    .line 5927
    const-string v6, "b64"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_a

    .line 5937
    sget-object v6, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 5938
    .local v2, "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5940
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {p0, v2, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5942
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v0

    .line 5943
    if-eqz v0, :cond_7

    .line 5945
    array-length v6, v0

    new-array v6, v6, [B

    iput-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    .line 5946
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v6, v0

    if-ge v1, v6, :cond_8

    .line 5947
    iget-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    aget-byte v7, v0, v1

    aput-byte v7, v6, v1

    .line 5946
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5885
    .end local v1    # "i":I
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_1
    const-string v6, "212"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    .line 5887
    iput v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    goto :goto_0

    .line 5889
    :cond_2
    const-string v6, "200"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_5

    const-string v6, "SyncHdr"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_5

    .line 5891
    iget v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    if-ne v6, v10, :cond_4

    .line 5893
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v6, :cond_3

    .line 5895
    iput v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    .line 5902
    :cond_3
    :goto_2
    const-string v6, "Client Authrization Accepted (Catch 200)"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5900
    :cond_4
    iput v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    goto :goto_2

    .line 5904
    :cond_5
    const-string v6, "213"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_0

    .line 5906
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "received Status \'buffered\' cmd "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5908
    iget-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->tempResults:Lcom/fmm/dm/eng/parser/XDMParserResults;

    if-eqz v6, :cond_6

    .line 5910
    new-instance v4, Lcom/fmm/dm/eng/parser/XDMParserResults;

    invoke-direct {v4}, Lcom/fmm/dm/eng/parser/XDMParserResults;-><init>()V

    .line 5911
    .local v4, "tmp":Lcom/fmm/dm/eng/parser/XDMParserResults;
    iget-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->tempResults:Lcom/fmm/dm/eng/parser/XDMParserResults;

    invoke-static {v4, v6}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDuplResults(Lcom/fmm/dm/eng/parser/XDMParserResults;Lcom/fmm/dm/eng/parser/XDMParserResults;)V

    .line 5912
    iget-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v6, v4}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 5916
    .end local v4    # "tmp":Lcom/fmm/dm/eng/parser/XDMParserResults;
    :cond_6
    const-string v6, "can\'t find cached results can\'t send multi-messaged"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5951
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    :cond_7
    const-string v6, "########## dValue is NULL"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5954
    :cond_8
    new-instance v3, Ljava/lang/String;

    iget-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>([B)V

    .line 5956
    .local v3, "szTemp":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "receive nextNonce:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "B64 decode String(ws.nextNonce):"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 5976
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_3
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetClientNonce(Ljava/lang/String;)V

    .line 5985
    sget-object v6, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 5986
    const-string v6, "/AAuthPref"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5988
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 6070
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_9
    :goto_4
    return v9

    .line 5962
    :cond_a
    const-string v6, "!B64"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5970
    sget-object v6, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 5971
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5973
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5974
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    iput-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    goto :goto_3

    .line 5990
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_b
    const-string v6, "syncml:auth-MAC"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_f

    .line 5992
    iput v10, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    .line 5993
    iget v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetAuthType(I)V

    .line 5995
    const-string v6, "b64"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_e

    .line 6004
    sget-object v6, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 6005
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 6007
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {p0, v2, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    .line 6008
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v0

    .line 6009
    if-eqz v0, :cond_c

    .line 6011
    array-length v6, v0

    new-array v6, v6, [B

    iput-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    .line 6012
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    array-length v6, v0

    if-ge v1, v6, :cond_d

    .line 6013
    iget-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    aget-byte v7, v0, v1

    aput-byte v7, v6, v1

    .line 6012
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 6017
    .end local v1    # "i":I
    :cond_c
    const-string v6, "########## dValue is NULL"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6019
    :cond_d
    new-instance v3, Ljava/lang/String;

    iget-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>([B)V

    .line 6020
    .restart local v3    # "szTemp":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "B64 decode nextNonce"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 6037
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_6
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetClientNonce(Ljava/lang/String;)V

    .line 6046
    sget-object v6, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 6047
    const-string v6, "/AAuthPref"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 6049
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 6031
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_e
    sget-object v6, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 6032
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 6034
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 6035
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    iput-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    goto :goto_6

    .line 6051
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_f
    const-string v6, "syncml:auth-basic"

    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_9

    .line 6054
    iput v9, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    .line 6055
    iget v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbSetAuthType(I)V

    .line 6064
    sget-object v6, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 6065
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthPref"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 6067
    iget-object v6, p1, Lcom/fmm/dm/eng/parser/XDMParserStatus;->chal:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_4
.end method

.method public xdmAgentCmdSyncHeader(Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)I
    .locals 9
    .param p1, "synchdr"    # Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/16 v5, -0x9

    const/high16 v4, 0x100000

    .line 5766
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 5769
    .local v0, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->msgid:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    .line 5771
    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5773
    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    .line 5778
    :cond_0
    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-eqz v2, :cond_6

    .line 5780
    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget v2, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxobjsize:I

    if-lez v2, :cond_4

    .line 5782
    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget v2, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxobjsize:I

    iput v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    .line 5783
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    if-gtz v2, :cond_3

    .line 5785
    iput v4, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    .line 5797
    :cond_1
    :goto_0
    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget v2, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxmsgsize:I

    if-lez v2, :cond_5

    .line 5799
    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget v2, v2, Lcom/fmm/dm/eng/parser/XDMParserMeta;->maxmsgsize:I

    iput v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxMsgSize:I

    .line 5812
    :goto_1
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    if-eq v2, v7, :cond_2

    .line 5814
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    if-eq v2, v8, :cond_a

    .line 5816
    iget-object v2, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    if-eqz v2, :cond_7

    .line 5818
    invoke-virtual {p0, p1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentVerifyServerAuth(Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)I

    move-result v2

    iput v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    .line 5840
    :cond_2
    :goto_2
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    if-ne v2, v7, :cond_c

    .line 5842
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    if-ne v2, v8, :cond_b

    .line 5844
    const-string v2, "200"

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    .line 5850
    :goto_3
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerAuthType(I)V

    .line 5862
    :goto_4
    const-string v2, "SyncHdr"

    iget-object v3, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSourceURI:Ljava/lang/String;

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 5863
    .local v6, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v2, v6}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5865
    return v1

    .line 5787
    .end local v6    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_3
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    const v3, 0x4e2000

    if-le v2, v3, :cond_1

    .line 5789
    iput v4, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    goto :goto_0

    .line 5794
    :cond_4
    iput v4, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    goto :goto_0

    .line 5803
    :cond_5
    const/16 v2, 0x1400

    iput v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxMsgSize:I

    goto :goto_1

    .line 5808
    :cond_6
    iput v4, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    .line 5809
    const/16 v2, 0x1400

    iput v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverMaxMsgSize:I

    goto :goto_1

    .line 5822
    :cond_7
    const-string v2, "Not Used Server Authentication"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 5823
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v3, -0x7

    if-eq v2, v3, :cond_8

    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    if-ne v2, v5, :cond_9

    .line 5825
    :cond_8
    const/4 v2, -0x1

    iput v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    goto :goto_2

    .line 5830
    :cond_9
    iput v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    goto :goto_2

    .line 5836
    :cond_a
    invoke-virtual {p0, p1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentVerifyServerAuth(Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)I

    move-result v2

    iput v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    goto :goto_2

    .line 5848
    :cond_b
    const-string v2, "212"

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    goto :goto_3

    .line 5853
    :cond_c
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    if-ne v2, v5, :cond_d

    .line 5855
    const-string v2, "407"

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    goto :goto_4

    .line 5859
    :cond_d
    const-string v2, "401"

    iput-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    goto :goto_4
.end method

.method public xdmAgentCmdUicAlert()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 3945
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 3946
    .local v0, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v6, 0x0

    .line 3948
    .local v6, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    if-eqz v1, :cond_2

    .line 3950
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_NONE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    if-ne v1, v2, :cond_4

    .line 3952
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "200"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 3953
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_1

    .line 3955
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    iput-object v1, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    .line 3982
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v1, v6}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3983
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 3984
    iput-object v3, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    .line 3985
    iput-object v3, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    .line 3986
    const/4 v6, 0x0

    .line 3989
    :cond_2
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    if-eq v1, v2, :cond_3

    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_NONE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    if-ne v1, v2, :cond_7

    .line 3991
    :cond_3
    const/4 v1, 0x1

    .line 3995
    :goto_1
    return v1

    .line 3958
    :cond_4
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_FALSE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    if-ne v1, v2, :cond_5

    .line 3960
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "304"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 3961
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_1

    .line 3963
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    iput-object v1, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0

    .line 3966
    :cond_5
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_CANCELED:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    if-ne v1, v2, :cond_6

    .line 3968
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "214"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 3969
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_1

    .line 3971
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    iput-object v1, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0

    .line 3976
    :cond_6
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    iget v1, v1, Lcom/fmm/dm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "215"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/fmm/dm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 3977
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    if-eqz v1, :cond_1

    .line 3979
    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicData:Lcom/fmm/dm/eng/core/XDMList;

    iput-object v1, v6, Lcom/fmm/dm/eng/parser/XDMParserStatus;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    goto :goto_0

    .line 3995
    :cond_7
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public xdmAgentCreatePackage()I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x4

    const/4 v6, -0x1

    const/4 v9, 0x0

    .line 630
    const/16 v0, 0x6a

    .line 632
    .local v0, "WBXML_CHARSET_UTF8":I
    sget-object v5, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 634
    .local v5, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v3, 0x0

    .line 635
    .local v3, "res":I
    iget-object v1, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->e:Lcom/fmm/dm/eng/core/XDMEncoder;

    .line 637
    .local v1, "e":Lcom/fmm/dm/eng/core/XDMEncoder;
    iget-object v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v8, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_INIT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    if-ne v7, v8, :cond_4

    .line 639
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentLoadWorkSpace()I

    move-result v3

    .line 640
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->getInitType()I

    move-result v7

    if-nez v7, :cond_1

    sget-boolean v7, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xlawmoGetbSimChange()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUnlockAlert()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetEmergencyModeChangeAlert()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetReActiveAlert()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 641
    :cond_1
    sget-object v7, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    .line 649
    :cond_2
    :goto_0
    if-eqz v3, :cond_6

    .line 651
    const-string v7, "xdmAgentCreatePackage failed"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 793
    :goto_1
    return v6

    .line 643
    :cond_3
    sget-object v7, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_CLIENT_INIT_MGMT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    goto :goto_0

    .line 645
    :cond_4
    iget-object v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v8, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT_REPORT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v7, v8, :cond_5

    iget-object v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v8, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_ABORT_ALERT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    if-ne v7, v8, :cond_2

    .line 647
    :cond_5
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentLoadWorkSpace()I

    move-result v3

    goto :goto_0

    .line 655
    :cond_6
    iget-object v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 656
    iget-object v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, v7}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncInit(Ljava/io/ByteArrayOutputStream;)V

    .line 658
    const-string v4, "-//SYNCML//DTD SyncML 1.2//EN"

    .line 662
    .local v4, "szWbxmlStrTbl":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1, v9, v0, v4, v7}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncStartSyncml(IILjava/lang/String;I)I

    .line 663
    invoke-static {v5}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdSyncHeader(Lcom/fmm/dm/eng/core/XDMWorkspace;)Lcom/fmm/dm/eng/core/XDMWorkspace;

    move-result-object v5

    .line 664
    iget-object v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->syncHeader:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    invoke-virtual {v1, v7}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncAddSyncHeader(Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)I

    .line 665
    invoke-virtual {v1}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncStartSyncbody()I

    .line 667
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent$1;->$SwitchMap$com$fmm$dm$interfaces$XDMInterface$XDMSyncMLState:[I

    iget-object v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    invoke-virtual {v8}, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 785
    :goto_2
    iget-boolean v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v6, :cond_7

    iget-boolean v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendRemain:Z

    if-eqz v6, :cond_8

    .line 787
    :cond_7
    iput-boolean v9, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 790
    :cond_8
    iget-boolean v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    invoke-virtual {v1, v6}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncEndSyncbody(Z)I

    .line 791
    invoke-virtual {v1}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncEndSyncml()I

    move v6, v3

    .line 793
    goto :goto_1

    .line 670
    :pswitch_0
    const-string v6, "XDM_STATE_CLIENT_INIT_MGMT"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 671
    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClientInitPackage(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    goto :goto_2

    .line 674
    :pswitch_1
    const-string v6, "XDM_STATE_PROCESSING"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 675
    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMgmtPackage(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    goto :goto_2

    .line 678
    :pswitch_2
    const-string v7, "XDM_STATE_GENERIC_ALERT"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 679
    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClientInitPackage(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v3

    .line 681
    if-eqz v3, :cond_a

    .line 683
    if-ne v3, v10, :cond_9

    .line 685
    iput-boolean v9, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_1

    .line 689
    :cond_9
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 694
    :cond_a
    const-string v7, "1226"

    invoke-virtual {p0, v1, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageGenericAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    .line 695
    if-eqz v3, :cond_c

    .line 697
    if-ne v3, v10, :cond_b

    .line 699
    iput-boolean v9, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 703
    :cond_b
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 707
    :cond_c
    iput-boolean v11, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_2

    .line 710
    :pswitch_3
    const-string v7, "XDM_STATE_GENERIC_ALERT_REPORT"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 711
    invoke-virtual {p0, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClientInitPackage(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v3

    .line 713
    if-eqz v3, :cond_e

    .line 715
    if-ne v3, v10, :cond_d

    .line 717
    iput-boolean v9, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 721
    :cond_d
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 726
    :cond_e
    const-string v7, "1226"

    invoke-virtual {p0, v1, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageReportGenericAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    .line 727
    if-eqz v3, :cond_10

    .line 729
    if-ne v3, v10, :cond_f

    .line 731
    iput-boolean v9, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 735
    :cond_f
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 739
    :cond_10
    iput-boolean v11, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_2

    .line 742
    :pswitch_4
    const-string v7, "XDM_STATE_ABORT_ALERT"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 743
    iget v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->appId:I

    invoke-static {v7}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbGetNotiEvent(I)I

    move-result v2

    .line 744
    .local v2, "nNotiEvent":I
    if-lez v2, :cond_11

    .line 746
    const-string v7, "1200"

    invoke-virtual {p0, v1, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    .line 752
    :goto_3
    if-eqz v3, :cond_13

    .line 754
    if-ne v3, v10, :cond_12

    .line 756
    iput-boolean v9, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 750
    :cond_11
    const-string v7, "1201"

    invoke-virtual {p0, v1, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    .line 760
    :cond_12
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 764
    :cond_13
    const-string v7, "1223"

    invoke-virtual {p0, v1, v7}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    .line 766
    if-eqz v3, :cond_15

    .line 768
    if-ne v3, v10, :cond_14

    .line 770
    iput-boolean v9, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 774
    :cond_14
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 779
    :cond_15
    iput-boolean v11, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_2

    .line 667
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public xdmAgentCreatePackageAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I
    .locals 3
    .param p1, "e"    # Lcom/fmm/dm/eng/core/XDMEncoder;
    .param p2, "szData"    # Ljava/lang/String;

    .prologue
    .line 2243
    const/4 v0, 0x0

    .line 2244
    .local v0, "alert":Lcom/fmm/dm/eng/parser/XDMParserAlert;
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 2246
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    invoke-static {v1, p2}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdAlert(Lcom/fmm/dm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserAlert;

    move-result-object v0

    .line 2247
    invoke-virtual {p1, v0}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncAddAlert(Lcom/fmm/dm/eng/parser/XDMParserAlert;)I

    .line 2248
    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 2250
    const/4 v2, 0x0

    return v2
.end method

.method xdmAgentCreatePackageDevInfo(Lcom/fmm/dm/eng/core/XDMEncoder;)I
    .locals 7
    .param p1, "e"    # Lcom/fmm/dm/eng/core/XDMEncoder;

    .prologue
    .line 2255
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 2256
    .local v4, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v3, 0x0

    .line 2257
    .local v3, "rep":Lcom/fmm/dm/eng/parser/XDMParserReplace;
    const/4 v0, 0x0

    .line 2258
    .local v0, "list":Lcom/fmm/dm/eng/core/XDMLinkedList;
    iget-object v2, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 2261
    .local v2, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-result-object v0

    .line 2262
    const-string v5, "./DevInfo/Lang"

    invoke-static {v2, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 2263
    .local v1, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v1, :cond_0

    .line 2264
    const-string v5, "./DevInfo/Lang"

    iget v6, v1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2266
    :cond_0
    const-string v5, "./DevInfo/DmV"

    invoke-static {v2, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 2267
    if-eqz v1, :cond_1

    .line 2268
    const-string v5, "./DevInfo/DmV"

    iget v6, v1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2270
    :cond_1
    const-string v5, "./DevInfo/Mod"

    invoke-static {v2, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 2271
    if-eqz v1, :cond_2

    .line 2272
    const-string v5, "./DevInfo/Mod"

    iget v6, v1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2274
    :cond_2
    const-string v5, "./DevInfo/Man"

    invoke-static {v2, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 2275
    if-eqz v1, :cond_3

    .line 2276
    const-string v5, "./DevInfo/Man"

    iget v6, v1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2278
    :cond_3
    const-string v5, "./DevInfo/DevId"

    invoke-static {v2, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 2279
    if-eqz v1, :cond_4

    .line 2280
    const-string v5, "./DevInfo/DevId"

    iget v6, v1, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2282
    :cond_4
    invoke-static {v4, v0}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdReplace(Lcom/fmm/dm/eng/core/XDMWorkspace;Lcom/fmm/dm/eng/core/XDMLinkedList;)Lcom/fmm/dm/eng/parser/XDMParserReplace;

    move-result-object v3

    .line 2284
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListFreeLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 2285
    invoke-virtual {p1, v3}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncAddReplace(Lcom/fmm/dm/eng/parser/XDMParserReplace;)I

    .line 2286
    invoke-static {v3}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteReplace(Ljava/lang/Object;)V

    .line 2288
    const/4 v5, 0x0

    return v5
.end method

.method public xdmAgentCreatePackageGenericAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I
    .locals 3
    .param p1, "e"    # Lcom/fmm/dm/eng/core/XDMEncoder;
    .param p2, "szData"    # Ljava/lang/String;

    .prologue
    .line 3432
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 3434
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const-string v2, ""

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3435
    invoke-static {v1, p2}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGenericAlert(Lcom/fmm/dm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserAlert;

    move-result-object v0

    .line 3436
    .local v0, "alert":Lcom/fmm/dm/eng/parser/XDMParserAlert;
    invoke-virtual {p1, v0}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncAddAlert(Lcom/fmm/dm/eng/parser/XDMParserAlert;)I

    .line 3437
    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 3439
    const/4 v2, 0x0

    return v2
.end method

.method public xdmAgentCreatePackageReportGenericAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I
    .locals 3
    .param p1, "e"    # Lcom/fmm/dm/eng/core/XDMEncoder;
    .param p2, "szData"    # Ljava/lang/String;

    .prologue
    .line 3445
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 3447
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    invoke-static {v1, p2}, Lcom/fmm/dm/agent/XDMBuildCmd;->xdmAgentBuildCmdGenericAlertReport(Lcom/fmm/dm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/fmm/dm/eng/parser/XDMParserAlert;

    move-result-object v0

    .line 3448
    .local v0, "alert":Lcom/fmm/dm/eng/parser/XDMParserAlert;
    invoke-virtual {p1, v0}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncAddAlert(Lcom/fmm/dm/eng/parser/XDMParserAlert;)I

    .line 3449
    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 3451
    const/4 v2, 0x0

    return v2
.end method

.method public xdmAgentCreatePackageResults(Lcom/fmm/dm/eng/core/XDMEncoder;)I
    .locals 23
    .param p1, "e"    # Lcom/fmm/dm/eng/core/XDMEncoder;

    .prologue
    .line 2102
    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 2103
    .local v20, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    sget-object v21, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    move-object/from16 v0, v21

    iget-object v11, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 2104
    .local v11, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/16 v16, 0x0

    .line 2110
    .local v16, "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    const/4 v13, 0x0

    .line 2111
    .local v13, "partialsize":I
    const/4 v10, 0x0

    .line 2112
    .local v10, "offset":I
    const/4 v12, 0x0

    .line 2113
    .local v12, "partialsend":Z
    const/4 v2, 0x0

    .line 2115
    .local v2, "buf":[C
    const/4 v15, 0x0

    .line 2117
    .local v15, "res":I
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 2118
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    check-cast v16, Lcom/fmm/dm/eng/parser/XDMParserResults;

    .line 2120
    .restart local v16    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    if-nez v16, :cond_1

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    move-object/from16 v21, v0

    sget-object v22, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_CLIENT_INIT_MGMT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextMsg:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    .line 2122
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 2123
    const/16 v21, 0x0

    .line 2238
    :goto_0
    return v21

    .line 2152
    .local v4, "cmdsize":I
    .local v5, "datasize":I
    .local v7, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .local v9, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .local v14, "remainsize":I
    .local v19, "usedsize":I
    :cond_0
    if-lez v5, :cond_4

    if-ge v14, v4, :cond_4

    .line 2154
    move v13, v5

    .line 2161
    :goto_1
    if-lez v13, :cond_8

    .line 2163
    new-array v2, v13, [C

    .line 2164
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v21, v0

    if-eqz v21, :cond_6

    .line 2166
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_6

    .line 2170
    const-string v21, "application/vnd.syncml.dmtnds+xml"

    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v21

    if-nez v21, :cond_6

    .line 2172
    const/4 v8, 0x0

    .line 2173
    .local v8, "nFileId":I
    new-array v3, v13, [B

    .line 2174
    .local v3, "buftmp":[B
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdTNDS()I

    move-result v8

    .line 2175
    invoke-static {v8, v10, v13, v3}, Lcom/fmm/dm/db/file/XDB;->xdbReadFile(III[B)Z

    .line 2176
    new-instance v17, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    .line 2177
    .local v17, "szData1":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toCharArray()[C

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStString2Pcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 2178
    if-eqz v12, :cond_5

    .line 2180
    const/16 v21, 0x1

    move/from16 v0, v21

    iput v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    .line 2181
    move-object/from16 v0, v20

    iget v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendPos:I

    move/from16 v21, v0

    add-int v21, v21, v13

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendPos:I

    .line 2188
    :goto_2
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncAddResults(Lcom/fmm/dm/eng/parser/XDMParserResults;)I

    .line 2189
    move-object/from16 v18, v16

    .line 2190
    .local v18, "tmp":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    check-cast v16, Lcom/fmm/dm/eng/parser/XDMParserResults;

    .line 2191
    .restart local v16    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 2192
    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteResults(Ljava/lang/Object;)V

    .line 2194
    const/4 v2, 0x0

    .line 2125
    .end local v3    # "buftmp":[B
    .end local v4    # "cmdsize":I
    .end local v5    # "datasize":I
    .end local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v8    # "nFileId":I
    .end local v9    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .end local v14    # "remainsize":I
    .end local v17    # "szData1":Ljava/lang/String;
    .end local v18    # "tmp":Lcom/fmm/dm/eng/parser/XDMParserResults;
    .end local v19    # "usedsize":I
    :cond_1
    :goto_3
    if-eqz v16, :cond_b

    .line 2127
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserResults;->itemlist:Lcom/fmm/dm/eng/core/XDMList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMList;->item:Ljava/lang/Object;

    check-cast v7, Lcom/fmm/dm/eng/parser/XDMParserItem;

    .line 2128
    .restart local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    .line 2130
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    move/from16 v21, v0

    if-lez v21, :cond_2

    .line 2132
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->size:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2143
    .restart local v5    # "datasize":I
    :goto_4
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v11, v0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v9

    .line 2144
    .restart local v9    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    invoke-static/range {p1 .. p1}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncGetBufferSize(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v19

    .line 2145
    .restart local v19    # "usedsize":I
    add-int/lit16 v4, v5, 0x80

    .line 2146
    .restart local v4    # "cmdsize":I
    move-object/from16 v0, v20

    iget v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->maxMsgSize:I

    move/from16 v21, v0

    sub-int v14, v21, v19

    .line 2148
    .restart local v14    # "remainsize":I
    const/16 v21, 0x80

    move/from16 v0, v21

    if-ge v14, v0, :cond_0

    .line 2150
    const/16 v21, -0x4

    goto/16 :goto_0

    .line 2136
    .end local v4    # "cmdsize":I
    .end local v5    # "datasize":I
    .end local v9    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .end local v14    # "remainsize":I
    .end local v19    # "usedsize":I
    :cond_2
    const/4 v5, 0x0

    .restart local v5    # "datasize":I
    goto :goto_4

    .line 2141
    .end local v5    # "datasize":I
    :cond_3
    const/4 v5, 0x0

    .restart local v5    # "datasize":I
    goto :goto_4

    .line 2158
    .restart local v4    # "cmdsize":I
    .restart local v9    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .restart local v14    # "remainsize":I
    .restart local v19    # "usedsize":I
    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 2185
    .restart local v3    # "buftmp":[B
    .restart local v8    # "nFileId":I
    .restart local v17    # "szData1":Ljava/lang/String;
    :cond_5
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendPos:I

    goto/16 :goto_2

    .line 2200
    .end local v3    # "buftmp":[B
    .end local v8    # "nFileId":I
    .end local v17    # "szData1":Ljava/lang/String;
    :cond_6
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v11, v0, v10, v2, v13}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    move-result v15

    .line 2202
    if-eqz v9, :cond_9

    iget v0, v9, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    .line 2204
    new-instance v21, Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    invoke-direct/range {v21 .. v21}, Lcom/fmm/dm/eng/parser/XDMParserPcdata;-><init>()V

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 2205
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->type:I

    .line 2206
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-object/from16 v21, v0

    new-array v0, v13, [C

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    .line 2208
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_5
    if-ge v6, v13, :cond_7

    .line 2209
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->data:[C

    move-object/from16 v21, v0

    aget-char v22, v2, v6

    aput-char v22, v21, v6

    .line 2208
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 2210
    :cond_7
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput v13, v0, Lcom/fmm/dm/eng/parser/XDMParserPcdata;->size:I

    .line 2218
    .end local v6    # "i":I
    :cond_8
    :goto_6
    if-eqz v12, :cond_a

    .line 2220
    const/16 v21, 0x1

    move/from16 v0, v21

    iput v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->moredata:I

    .line 2221
    move-object/from16 v0, v20

    iget v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendPos:I

    move/from16 v21, v0

    add-int v21, v21, v13

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendPos:I

    .line 2228
    :goto_7
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncAddResults(Lcom/fmm/dm/eng/parser/XDMParserResults;)I

    .line 2229
    move-object/from16 v18, v16

    .line 2230
    .restart local v18    # "tmp":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    check-cast v16, Lcom/fmm/dm/eng/parser/XDMParserResults;

    .line 2231
    .restart local v16    # "results":Lcom/fmm/dm/eng/parser/XDMParserResults;
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 2232
    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteResults(Ljava/lang/Object;)V

    .line 2234
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 2214
    .end local v18    # "tmp":Lcom/fmm/dm/eng/parser/XDMParserResults;
    :cond_9
    invoke-static {v2}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStString2Pcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    goto :goto_6

    .line 2225
    :cond_a
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->sendPos:I

    goto :goto_7

    .line 2236
    .end local v4    # "cmdsize":I
    .end local v5    # "datasize":I
    .end local v7    # "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    .end local v9    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .end local v14    # "remainsize":I
    .end local v19    # "usedsize":I
    :cond_b
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->resultsList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 2238
    const/16 v21, 0x0

    goto/16 :goto_0
.end method

.method public xdmAgentCreatePackageStatus(Lcom/fmm/dm/eng/core/XDMEncoder;)I
    .locals 5
    .param p1, "e"    # Lcom/fmm/dm/eng/core/XDMEncoder;

    .prologue
    const/4 v4, 0x0

    .line 2081
    const/4 v0, 0x0

    .line 2083
    .local v0, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 2084
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v3, v4}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 2086
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .line 2087
    .restart local v0    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :goto_0
    if-eqz v0, :cond_0

    .line 2089
    invoke-virtual {p1, v0}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncAddStatus(Lcom/fmm/dm/eng/parser/XDMParserStatus;)I

    .line 2090
    move-object v1, v0

    .line 2091
    .local v1, "tmp":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    check-cast v0, Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .line 2092
    .restart local v0    # "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 2093
    invoke-static {v1}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStDeleteStatus(Ljava/lang/Object;)V

    goto :goto_0

    .line 2095
    .end local v1    # "tmp":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    :cond_0
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->statusList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    .line 2097
    return v4
.end method

.method public xdmAgentGetAccountFromOM(Lcom/fmm/dm/eng/core/XDMOmTree;)I
    .locals 16
    .param p1, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;

    .prologue
    .line 7186
    const/4 v9, 0x0

    .line 7187
    .local v9, "pProfileInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    const/4 v8, 0x0

    .line 7190
    .local v8, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    const/4 v3, 0x0

    .line 7191
    .local v3, "TmpBuf":[C
    const/4 v12, 0x0

    .line 7192
    .local v12, "szAddrURI":Ljava/lang/String;
    const/4 v13, 0x0

    .line 7193
    .local v13, "szPortNum":Ljava/lang/String;
    const/4 v11, 0x0

    .line 7194
    .local v11, "szAccTmpBuf":Ljava/lang/String;
    const/16 v14, 0x100

    new-array v2, v14, [C

    .line 7196
    .local v2, "ServerUrl":[C
    const/4 v5, 0x0

    .line 7199
    .local v5, "authType":I
    new-instance v9, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .end local v9    # "pProfileInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-direct {v9}, Lcom/fmm/dm/db/file/XDBProfileInfo;-><init>()V

    .line 7201
    .restart local v9    # "pProfileInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v14, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 7203
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 7204
    const/4 v14, -0x1

    .line 7564
    :goto_0
    return v14

    .line 7206
    :cond_0
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v14, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 7208
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 7209
    const/4 v14, -0x1

    goto :goto_0

    .line 7211
    :cond_1
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v14, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 7213
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 7214
    const/4 v14, -0x1

    goto :goto_0

    .line 7216
    :cond_2
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v14, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 7218
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 7219
    const/4 v14, -0x1

    goto :goto_0

    .line 7221
    :cond_3
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v14, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 7223
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 7224
    const/4 v14, -0x1

    goto :goto_0

    .line 7226
    :cond_4
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v14, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 7228
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 7229
    const/4 v14, -0x1

    goto :goto_0

    .line 7232
    :cond_5
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7233
    const-string v14, "/AppID"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7234
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7235
    if-eqz v8, :cond_6

    .line 7237
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7243
    .local v10, "size":I
    :goto_1
    new-array v3, v10, [C

    .line 7244
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7245
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    .line 7247
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7248
    const-string v14, "/ServerID"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7249
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7250
    if-eqz v8, :cond_7

    .line 7252
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7258
    :goto_2
    new-array v3, v10, [C

    .line 7259
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7260
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 7263
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7264
    const-string v14, "/Name"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7265
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7266
    if-eqz v8, :cond_8

    .line 7268
    iget v14, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v3, v14, [C

    .line 7269
    const/4 v14, 0x0

    iget v15, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v15}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7270
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 7279
    :goto_3
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7280
    const-string v14, "/PrefConRef"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7281
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7282
    if-eqz v8, :cond_9

    .line 7284
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7290
    :goto_4
    new-array v3, v10, [C

    .line 7291
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7292
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    .line 7294
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7295
    const-string v14, "/Addr"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7296
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7297
    if-eqz v8, :cond_a

    .line 7299
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7305
    :goto_5
    new-array v3, v10, [C

    .line 7306
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7307
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    .line 7310
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 7311
    const-string v14, "/PortNbr"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7312
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7313
    if-eqz v8, :cond_b

    .line 7315
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7321
    :goto_6
    new-array v3, v10, [C

    .line 7322
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7323
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    .line 7325
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v14

    invoke-virtual {v13}, Ljava/lang/String;->toCharArray()[C

    move-result-object v15

    invoke-static {v2, v14, v15}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbDoDMBootStrapURI([C[C[C)[C

    move-result-object v2

    .line 7326
    if-nez v2, :cond_c

    .line 7327
    const/4 v14, -0x1

    goto/16 :goto_0

    .line 7241
    .end local v10    # "size":I
    :cond_6
    const/4 v10, 0x0

    .restart local v10    # "size":I
    goto/16 :goto_1

    .line 7256
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 7274
    :cond_8
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 7275
    const/16 v4, 0x1b

    .line 7276
    .local v4, "aclValue":I
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v4, v15}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_3

    .line 7288
    .end local v4    # "aclValue":I
    :cond_9
    const/4 v10, 0x0

    goto :goto_4

    .line 7303
    :cond_a
    const/4 v10, 0x0

    goto :goto_5

    .line 7319
    :cond_b
    const/4 v10, 0x0

    goto :goto_6

    .line 7328
    :cond_c
    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>([C)V

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 7329
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v14}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v7

    .line 7330
    .local v7, "getParser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    iget-object v14, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 7331
    iget-object v14, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 7332
    iget-object v14, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    .line 7333
    iget v14, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    iput v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    .line 7334
    iget-object v14, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 7336
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 7337
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 7338
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path_Org:Ljava/lang/String;

    .line 7339
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 7340
    iget v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    iput v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 7341
    const/4 v14, 0x0

    iput-boolean v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    .line 7343
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7344
    const-string v14, "/AAuthLevel"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7345
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7346
    if-eqz v8, :cond_e

    .line 7348
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7354
    :goto_7
    new-array v3, v10, [C

    .line 7355
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7356
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    .line 7358
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7359
    const-string v14, "/AAuthType"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7360
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7361
    if-eqz v8, :cond_f

    .line 7363
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7369
    :goto_8
    new-array v3, v10, [C

    .line 7370
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7372
    const-string v14, "DIGEST"

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_10

    .line 7374
    const/4 v5, 0x1

    .line 7392
    :goto_9
    iput v5, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    .line 7394
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7395
    const-string v14, "/AAuthName"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7396
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7397
    if-eqz v8, :cond_14

    .line 7399
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7405
    :goto_a
    new-array v3, v10, [C

    .line 7406
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7407
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 7409
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7410
    const-string v14, "/AAuthSecret"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7411
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7412
    if-eqz v8, :cond_15

    .line 7414
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7420
    :goto_b
    new-array v3, v10, [C

    .line 7421
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7422
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 7424
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7425
    const-string v14, "/AAuthData"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7426
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7427
    if-eqz v8, :cond_16

    .line 7429
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7430
    iget v6, v8, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 7437
    .local v6, "format":I
    :goto_c
    new-array v3, v10, [C

    .line 7438
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7439
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 7440
    iput v6, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonceFormat:I

    .line 7442
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7443
    const-string v14, "/AAuthLevel"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7444
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7445
    if-eqz v8, :cond_17

    .line 7447
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7453
    :goto_d
    new-array v3, v10, [C

    .line 7454
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7455
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    .line 7457
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7458
    const-string v14, "/AAuthType"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7459
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7460
    if-eqz v8, :cond_18

    .line 7462
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7468
    :goto_e
    new-array v3, v10, [C

    .line 7469
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7471
    const-string v14, "DIGEST"

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_19

    .line 7473
    const/4 v5, 0x1

    .line 7491
    :goto_f
    iput v5, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    .line 7493
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7494
    const-string v14, "/AAuthName"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7495
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7496
    if-eqz v8, :cond_1d

    .line 7498
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7504
    :goto_10
    new-array v3, v10, [C

    .line 7505
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7506
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 7508
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7509
    const-string v14, "/AAuthSecret"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7510
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7511
    if-eqz v8, :cond_1e

    .line 7513
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7519
    :goto_11
    new-array v3, v10, [C

    .line 7520
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7521
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 7523
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7524
    const-string v14, "/AAuthData"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7525
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7526
    if-eqz v8, :cond_1f

    .line 7528
    iget v10, v8, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    .line 7529
    iget v6, v8, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 7536
    :goto_12
    new-array v3, v10, [C

    .line 7537
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v14, v3, v10}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 7538
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 7539
    iput v6, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonceFormat:I

    .line 7542
    sget-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v11, v14, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7543
    const-string v14, "/Ext"

    invoke-virtual {v11, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 7544
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v8

    .line 7545
    if-eqz v8, :cond_d

    .line 7553
    :cond_d
    const/4 v11, 0x0

    .line 7556
    const/4 v1, 0x0

    .line 7557
    .local v1, "Index":I
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v14}, Lcom/fmm/dm/db/file/XDB;->xdbSetActiveProfileIndexByServerID(Ljava/lang/String;)I

    move-result v1

    .line 7559
    invoke-static {v9}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileInfo(Lcom/fmm/dm/db/file/XDBProfileInfo;)Z

    .line 7560
    iget-object v14, v9, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-static {v1, v14}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileName(ILjava/lang/String;)V

    .line 7562
    const/4 v9, 0x0

    .line 7563
    const/4 v14, 0x0

    sput-object v14, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    .line 7564
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 7352
    .end local v1    # "Index":I
    .end local v6    # "format":I
    :cond_e
    const/4 v10, 0x0

    goto/16 :goto_7

    .line 7367
    :cond_f
    const/4 v10, 0x0

    goto/16 :goto_8

    .line 7376
    :cond_10
    const-string v14, "BASIC"

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_11

    .line 7378
    const/4 v5, 0x0

    goto/16 :goto_9

    .line 7380
    :cond_11
    const-string v14, "HMAC"

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_12

    .line 7382
    const/4 v5, 0x2

    goto/16 :goto_9

    .line 7384
    :cond_12
    const-string v14, "DIGEST"

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_13

    .line 7386
    const/4 v5, 0x1

    goto/16 :goto_9

    .line 7390
    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_9

    .line 7403
    :cond_14
    const/4 v10, 0x0

    goto/16 :goto_a

    .line 7418
    :cond_15
    const/4 v10, 0x0

    goto/16 :goto_b

    .line 7434
    :cond_16
    const/4 v10, 0x0

    .line 7435
    const/16 v6, 0xc

    .restart local v6    # "format":I
    goto/16 :goto_c

    .line 7451
    :cond_17
    const/4 v10, 0x0

    goto/16 :goto_d

    .line 7466
    :cond_18
    const/4 v10, 0x0

    goto/16 :goto_e

    .line 7475
    :cond_19
    const-string v14, "BASIC"

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_1a

    .line 7477
    const/4 v5, 0x0

    goto/16 :goto_f

    .line 7479
    :cond_1a
    const-string v14, "HMAC"

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_1b

    .line 7481
    const/4 v5, 0x2

    goto/16 :goto_f

    .line 7483
    :cond_1b
    const-string v14, "DIGEST"

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_1c

    .line 7485
    const/4 v5, 0x1

    goto/16 :goto_f

    .line 7489
    :cond_1c
    const/4 v5, 0x0

    goto/16 :goto_f

    .line 7502
    :cond_1d
    const/4 v10, 0x0

    goto/16 :goto_10

    .line 7517
    :cond_1e
    const/4 v10, 0x0

    goto/16 :goto_11

    .line 7533
    :cond_1f
    const/4 v10, 0x0

    .line 7534
    const/16 v6, 0xc

    goto/16 :goto_12
.end method

.method public xdmAgentGetAclStr(Lcom/fmm/dm/eng/core/XDMOmList;Lcom/fmm/dm/eng/parser/XDMParserItem;)Ljava/lang/String;
    .locals 13
    .param p1, "acllist"    # Lcom/fmm/dm/eng/core/XDMOmList;
    .param p2, "item"    # Lcom/fmm/dm/eng/parser/XDMParserItem;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 4874
    const/4 v6, 0x5

    new-array v1, v6, [Ljava/lang/String;

    const-string v6, "\u0000"

    aput-object v6, v1, v8

    const-string v6, "\u0000"

    aput-object v6, v1, v9

    const-string v6, "\u0000"

    aput-object v6, v1, v10

    const-string v6, "\u0000"

    aput-object v6, v1, v11

    const-string v6, "\u0000"

    aput-object v6, v1, v12

    .line 4880
    .local v1, "buf":[Ljava/lang/String;
    move-object v2, p1

    .line 4882
    .local v2, "cur":Lcom/fmm/dm/eng/core/XDMOmList;
    if-nez p1, :cond_0

    .line 4883
    const/4 v5, 0x0

    .line 5091
    :goto_0
    return-object v5

    .line 4885
    :cond_0
    const-string v4, "\u0000"

    .line 4892
    .local v4, "szOutbuf":Ljava/lang/String;
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_6

    .line 4893
    const/4 v6, 0x0

    iput-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    .line 4898
    :goto_1
    if-eqz v2, :cond_d

    .line 4900
    iget-object v0, v2, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 4901
    .local v0, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    iget v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x1

    if-lez v6, :cond_1

    .line 4905
    aget-object v6, v1, v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_8

    .line 4906
    iget-object v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v8

    .line 4910
    :cond_1
    :goto_2
    iget v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x2

    if-lez v6, :cond_2

    .line 4913
    aget-object v6, v1, v9

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_9

    .line 4914
    iget-object v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v9

    .line 4918
    :cond_2
    :goto_3
    iget v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x4

    if-lez v6, :cond_3

    .line 4921
    aget-object v6, v1, v10

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_a

    .line 4922
    iget-object v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v10

    .line 4926
    :cond_3
    :goto_4
    iget v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x8

    if-lez v6, :cond_4

    .line 4929
    aget-object v6, v1, v11

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_b

    .line 4930
    iget-object v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v11

    .line 4934
    :cond_4
    :goto_5
    iget v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x10

    if-lez v6, :cond_5

    .line 4937
    aget-object v6, v1, v12

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_c

    .line 4938
    iget-object v6, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v12

    .line 4942
    :cond_5
    :goto_6
    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    goto :goto_1

    .line 4894
    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    :cond_6
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 4895
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    const/4 v7, 0x0

    iput-object v7, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    goto :goto_1

    .line 4897
    :cond_7
    const-string v6, "item->meta !NULL"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1

    .line 4908
    .restart local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    :cond_8
    aget-object v6, v1, v8

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v8

    goto :goto_2

    .line 4916
    :cond_9
    aget-object v6, v1, v9

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v9

    goto :goto_3

    .line 4924
    :cond_a
    aget-object v6, v1, v10

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v10

    goto :goto_4

    .line 4932
    :cond_b
    aget-object v6, v1, v11

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v11

    goto :goto_5

    .line 4940
    :cond_c
    aget-object v6, v1, v12

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v12

    goto :goto_6

    .line 4945
    .end local v0    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    :cond_d
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_7
    const/4 v6, 0x5

    if-ge v3, v6, :cond_2d

    .line 4947
    if-nez v3, :cond_f

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_f

    .line 4949
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_e

    .line 4951
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_19

    .line 4953
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4969
    :cond_e
    :goto_8
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_1c

    .line 4970
    const-string v6, "Add="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4974
    :cond_f
    :goto_9
    if-ne v3, v9, :cond_11

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_11

    .line 4976
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_10

    .line 4978
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_1d

    .line 4980
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4996
    :cond_10
    :goto_a
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_20

    .line 4997
    const-string v6, "Delete="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5001
    :cond_11
    :goto_b
    if-ne v3, v10, :cond_13

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_13

    .line 5003
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_12

    .line 5005
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_21

    .line 5007
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5023
    :cond_12
    :goto_c
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_24

    .line 5024
    const-string v6, "Exec="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5028
    :cond_13
    :goto_d
    if-ne v3, v11, :cond_15

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_15

    .line 5030
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_14

    .line 5032
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_25

    .line 5034
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5050
    :cond_14
    :goto_e
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_28

    .line 5051
    const-string v6, "Get="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5055
    :cond_15
    :goto_f
    if-ne v3, v12, :cond_17

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_17

    .line 5057
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_16

    .line 5059
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_29

    .line 5061
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5077
    :cond_16
    :goto_10
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_2c

    .line 5078
    const-string v6, "Replace="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5083
    :cond_17
    :goto_11
    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_18

    .line 5085
    aget-object v6, v1, v3

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4945
    :cond_18
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    .line 4955
    :cond_19
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 4957
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 4959
    :cond_1a
    const-string v6, "xml"

    iget-object v7, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1b

    .line 4961
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 4965
    :cond_1b
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 4972
    :cond_1c
    const-string v4, "Add="

    goto/16 :goto_9

    .line 4982
    :cond_1d
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 4984
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_a

    .line 4986
    :cond_1e
    const-string v6, "xml"

    iget-object v7, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1f

    .line 4988
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_a

    .line 4992
    :cond_1f
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_a

    .line 4999
    :cond_20
    const-string v4, "Delete="

    goto/16 :goto_b

    .line 5009
    :cond_21
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 5011
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 5013
    :cond_22
    const-string v6, "xml"

    iget-object v7, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_23

    .line 5015
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 5019
    :cond_23
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 5026
    :cond_24
    const-string v4, "Exec="

    goto/16 :goto_d

    .line 5036
    :cond_25
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_26

    .line 5038
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_e

    .line 5040
    :cond_26
    const-string v6, "xml"

    iget-object v7, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_27

    .line 5042
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_e

    .line 5046
    :cond_27
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_e

    .line 5053
    :cond_28
    const-string v4, "Get="

    goto/16 :goto_f

    .line 5063
    :cond_29
    iget-object v6, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2a

    .line 5065
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_10

    .line 5067
    :cond_2a
    const-string v6, "xml"

    iget-object v7, p2, Lcom/fmm/dm/eng/parser/XDMParserItem;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2b

    .line 5069
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_10

    .line 5073
    :cond_2b
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_10

    .line 5080
    :cond_2c
    const-string v4, "Replace="

    goto/16 :goto_11

    .line 5089
    :cond_2d
    move-object v5, v4

    .line 5091
    .local v5, "szTmp":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public xdmAgentGetDMInfoFromOmTreeVer11(Lcom/fmm/dm/db/file/XDBProfileInfo;)I
    .locals 13
    .param p1, "pProfileInfo"    # Lcom/fmm/dm/db/file/XDBProfileInfo;

    .prologue
    .line 2916
    sget-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget-object v6, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 2917
    .local v6, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/4 v2, 0x0

    .line 2918
    .local v2, "buf":[C
    const/4 v0, 0x0

    .line 2919
    .local v0, "ConRefName":[C
    const-string v8, ""

    .line 2920
    .local v8, "szAddURI":Ljava/lang/String;
    const-string v9, ""

    .line 2921
    .local v9, "szPortNum":Ljava/lang/String;
    const-string v7, ""

    .line 2923
    .local v7, "szAccBuf":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2926
    .local v4, "nRet":I
    sget-object v11, Lcom/fmm/dm/agent/XDMAgent;->pAccName:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 2928
    sget-object v11, Lcom/fmm/dm/agent/XDMAgent;->pAccName:Ljava/lang/String;

    sput-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 2929
    const/4 v11, 0x0

    sput-object v11, Lcom/fmm/dm/agent/XDMAgent;->pAccName:Ljava/lang/String;

    .line 2938
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 2939
    const-string v11, "/UserName"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2940
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 2941
    .local v5, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v5, :cond_0

    .line 2943
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 2944
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2945
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 2947
    :cond_0
    const/4 v2, 0x0

    .line 2948
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 2949
    const-string v11, "/ClientPW"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2950
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 2951
    if-eqz v5, :cond_1

    .line 2953
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 2954
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2955
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 2957
    :cond_1
    const/4 v2, 0x0

    .line 2958
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 2959
    const-string v11, "/ServerId"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2960
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 2961
    if-eqz v5, :cond_2

    .line 2963
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 2964
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2965
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 2967
    :cond_2
    const/4 v2, 0x0

    .line 2968
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 2969
    const-string v11, "/ServerPW"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2970
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 2971
    if-eqz v5, :cond_3

    .line 2973
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 2974
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2975
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 2977
    :cond_3
    const/4 v2, 0x0

    .line 2978
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 2979
    const-string v11, "/Addr"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2980
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 2981
    if-eqz v5, :cond_4

    .line 2983
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 2984
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2985
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    .line 2988
    :cond_4
    const/4 v2, 0x0

    .line 2989
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 2990
    const-string v11, "/PortNbr"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2991
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 2992
    if-eqz v5, :cond_5

    .line 2994
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 2995
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2996
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v9

    .line 2999
    :cond_5
    const/16 v11, 0x100

    new-array v10, v11, [C

    .line 3000
    .local v10, "temp1":[C
    invoke-virtual {v8}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbDoDMBootStrapURI([C[C[C)[C

    move-result-object v10

    .line 3001
    if-nez v10, :cond_7

    .line 3002
    const/4 v4, -0x1

    .line 3325
    .end local v4    # "nRet":I
    .end local v5    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .end local v10    # "temp1":[C
    :goto_0
    return v4

    .line 2933
    .restart local v4    # "nRet":I
    :cond_6
    const-string v11, "XDM_RET_FAILED"

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2934
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 2935
    const/4 v4, -0x1

    goto :goto_0

    .line 3009
    .restart local v5    # "node":Lcom/fmm/dm/eng/core/XDMVnode;
    .restart local v10    # "temp1":[C
    :cond_7
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v10}, Ljava/lang/String;-><init>([C)V

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 3011
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v11}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v3

    .line 3012
    .local v3, "getParser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    iget-object v11, v3, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 3013
    iget-object v11, v3, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 3014
    iget-object v11, v3, Lcom/fmm/dm/db/file/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    .line 3015
    iget v11, v3, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    .line 3016
    iget-object v11, v3, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 3018
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 3019
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 3020
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path_Org:Ljava/lang/String;

    .line 3021
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 3022
    iget v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 3024
    const/4 v11, 0x0

    iput-boolean v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    .line 3026
    const/4 v2, 0x0

    .line 3027
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3028
    const-string v11, "/ClientNonce"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3029
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3030
    if-eqz v5, :cond_8

    .line 3032
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3033
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3034
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 3035
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonceFormat:I

    .line 3038
    :cond_8
    const/4 v2, 0x0

    .line 3039
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3040
    const-string v11, "/AuthPref"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3042
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3043
    if-eqz v5, :cond_9

    .line 3045
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3046
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3047
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "AuthType : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3048
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthCredString2Type(Ljava/lang/String;)I

    move-result v11

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    .line 3049
    iget v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    const/4 v12, -0x1

    if-ne v11, v12, :cond_9

    .line 3051
    const/4 v11, 0x0

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    .line 3055
    :cond_9
    const/4 v2, 0x0

    .line 3056
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3057
    const-string v11, "/ServerNonce"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3058
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3059
    if-eqz v5, :cond_a

    .line 3061
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3062
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3063
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 3064
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonceFormat:I

    .line 3067
    :cond_a
    const/4 v2, 0x0

    .line 3068
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3069
    const-string v11, "/Name"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3070
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3071
    if-eqz v5, :cond_b

    .line 3073
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3074
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3075
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 3084
    :goto_1
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3085
    const-string v11, "/ConRef"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3086
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3087
    if-eqz v5, :cond_c

    .line 3089
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v11, [C

    .line 3090
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3102
    const/4 v11, 0x0

    sput-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3104
    const-string v11, "./SyncML/Con"

    sput-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3105
    sget-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    sput-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3106
    sget-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    sput-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3108
    sget-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    invoke-static {v6, v11}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3109
    if-nez v5, :cond_d

    .line 3111
    const/4 v11, 0x6

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nNetworkConnIndex:I

    .line 3112
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->Active:Z

    .line 3113
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 3114
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 3079
    :cond_b
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    iput-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 3080
    const/16 v1, 0x1b

    .line 3081
    .local v1, "aclValue":I
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    const/4 v12, 0x2

    invoke-static {v6, v7, v11, v1, v12}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_1

    .line 3095
    .end local v1    # "aclValue":I
    :cond_c
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Name : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3097
    const/4 v11, 0x6

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nNetworkConnIndex:I

    .line 3098
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->Active:Z

    .line 3099
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 3100
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 3117
    :cond_d
    const/4 v2, 0x0

    .line 3118
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3119
    const-string v11, "/NAP/Bearer"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3120
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3121
    if-eqz v5, :cond_f

    .line 3123
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3124
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3125
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "nBearer"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3126
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_e

    array-length v11, v2

    if-lez v11, :cond_e

    .line 3128
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->nBearer:I

    .line 3140
    :goto_2
    const/4 v2, 0x0

    .line 3141
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3142
    const-string v11, "/NAP/AddrType"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3143
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3144
    if-eqz v5, :cond_12

    .line 3146
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3147
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3148
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "nAddrType="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3149
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_11

    array-length v11, v2

    if-lez v11, :cond_11

    .line 3151
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    const-string v12, "NAP"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 3152
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v12, 0x1

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->nAddrType:I

    .line 3165
    :goto_3
    const/4 v2, 0x0

    .line 3166
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3167
    const-string v11, "/NAP/Addr"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3168
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3169
    if-eqz v5, :cond_14

    .line 3171
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3172
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3173
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_13

    .line 3175
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    .line 3186
    :goto_4
    const/4 v2, 0x0

    .line 3187
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3188
    const-string v11, "/NAP/Auth/PAP/Id"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3189
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3190
    if-eqz v5, :cond_16

    .line 3192
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3193
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3194
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "PAP_ID"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3195
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_15

    .line 3197
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    .line 3209
    :goto_5
    const/4 v2, 0x0

    .line 3210
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3211
    const-string v11, "/NAP/Auth/PAP/Secret"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3212
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3213
    if-eqz v5, :cond_18

    .line 3215
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3216
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3217
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_17

    .line 3219
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    .line 3231
    :goto_6
    const/4 v2, 0x0

    .line 3232
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3233
    const-string v11, "/PX/PortNbr"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3234
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3235
    if-eqz v5, :cond_1a

    .line 3237
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3238
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3239
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "nPortNbr"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3240
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_19

    array-length v11, v2

    if-lez v11, :cond_19

    .line 3242
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    .line 3253
    :goto_7
    const/4 v2, 0x0

    .line 3254
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3255
    const-string v11, "/PX/AddrType"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3256
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3257
    if-eqz v5, :cond_1c

    .line 3259
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3260
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3261
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "nAddrType"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3262
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_1b

    array-length v11, v2

    if-lez v11, :cond_1b

    .line 3264
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    .line 3276
    :goto_8
    const/4 v2, 0x0

    .line 3277
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3278
    const-string v11, "/PX/Addr"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3279
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3280
    if-eqz v5, :cond_1e

    .line 3282
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3283
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3284
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_1d

    .line 3286
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 3298
    :goto_9
    const/4 v2, 0x0

    .line 3299
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 3300
    const-string v11, "/Ext/Service"

    invoke-virtual {v7, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3301
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 3302
    if-eqz v5, :cond_20

    .line 3304
    iget v11, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v2, v11, [C

    .line 3305
    const/4 v11, 0x0

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v2, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3306
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([C)I

    move-result v11

    if-eqz v11, :cond_1f

    array-length v11, v2

    if-lez v11, :cond_1f

    .line 3308
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    .line 3320
    :goto_a
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    const/4 v12, 0x1

    iput-boolean v12, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->Active:Z

    .line 3321
    const/4 v11, 0x6

    iput v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nNetworkConnIndex:I

    .line 3323
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    goto/16 :goto_0

    .line 3132
    :cond_e
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v12, 0x0

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->nBearer:I

    goto/16 :goto_2

    .line 3137
    :cond_f
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v12, 0x0

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->nBearer:I

    goto/16 :goto_2

    .line 3154
    :cond_10
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->nAddrType:I

    goto/16 :goto_3

    .line 3158
    :cond_11
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v12, -0x1

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->nAddrType:I

    goto/16 :goto_3

    .line 3163
    :cond_12
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v12, -0x1

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->nAddrType:I

    goto/16 :goto_3

    .line 3179
    :cond_13
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    goto/16 :goto_4

    .line 3184
    :cond_14
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    goto/16 :goto_4

    .line 3201
    :cond_15
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    goto/16 :goto_5

    .line 3206
    :cond_16
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    goto/16 :goto_5

    .line 3223
    :cond_17
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    goto/16 :goto_6

    .line 3228
    :cond_18
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    goto/16 :goto_6

    .line 3246
    :cond_19
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/4 v12, 0x0

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    goto/16 :goto_7

    .line 3251
    :cond_1a
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/4 v12, 0x0

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    goto/16 :goto_7

    .line 3268
    :cond_1b
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/4 v12, -0x1

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    goto/16 :goto_8

    .line 3273
    :cond_1c
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/4 v12, -0x1

    iput v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    goto/16 :goto_8

    .line 3290
    :cond_1d
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    goto/16 :goto_9

    .line 3295
    :cond_1e
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    goto/16 :goto_9

    .line 3312
    :cond_1f
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    const/4 v12, -0x1

    iput v12, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    goto/16 :goto_a

    .line 3317
    :cond_20
    iget-object v11, p1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    const/4 v12, -0x1

    iput v12, v11, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    goto/16 :goto_a
.end method

.method public xdmAgentGetDMInfoFromOmTreeVer12(Lcom/fmm/dm/db/file/XDBProfileInfo;)I
    .locals 21
    .param p1, "pProfileInfo"    # Lcom/fmm/dm/db/file/XDBProfileInfo;

    .prologue
    .line 2494
    sget-object v17, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    move-object/from16 v0, v17

    iget-object v11, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 2496
    .local v11, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/4 v5, 0x0

    .line 2497
    .local v5, "buf":[C
    const/4 v2, 0x0

    .line 2498
    .local v2, "ConRefName":[C
    const-string v13, ""

    .line 2499
    .local v13, "szAddressURI":Ljava/lang/String;
    const-string v15, ""

    .line 2500
    .local v15, "szPortNum":Ljava/lang/String;
    const-string v14, ""

    .line 2501
    .local v14, "szAuthType":Ljava/lang/String;
    const/4 v12, 0x0

    .line 2503
    .local v12, "szAccBuf":Ljava/lang/String;
    const/4 v9, 0x0

    .line 2506
    .local v9, "nRet":I
    sget-object v17, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    sget-object v17, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    sget-object v17, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    sget-object v17, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    sget-object v17, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    sget-object v17, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 2509
    :cond_0
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 2510
    const/4 v9, -0x1

    .line 2910
    .end local v9    # "nRet":I
    :goto_0
    return v9

    .line 2513
    .restart local v9    # "nRet":I
    :cond_1
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2514
    const/16 v17, 0x40

    move/from16 v0, v17

    new-array v2, v0, [C

    .line 2517
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AppID"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2518
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2519
    .local v10, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v10, :cond_3

    .line 2521
    const/4 v5, 0x0

    .line 2522
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2523
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2525
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    .line 2526
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get AppID from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2534
    :goto_1
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/ServerID"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2535
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2537
    if-eqz v10, :cond_4

    .line 2539
    const/4 v5, 0x0

    .line 2540
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2541
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2542
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 2556
    :goto_2
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/Name"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2557
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2558
    if-eqz v10, :cond_5

    .line 2560
    const/4 v5, 0x0

    .line 2561
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2562
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2563
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 2564
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get ProfileName from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2576
    :goto_3
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/PrefConRef"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2577
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2578
    if-eqz v10, :cond_6

    .line 2580
    const/4 v5, 0x0

    .line 2581
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2582
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2583
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    .line 2584
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get PrefConRef from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2592
    :goto_4
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/Addr"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2593
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2594
    if-eqz v10, :cond_7

    .line 2596
    const/4 v5, 0x0

    .line 2597
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2598
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2599
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v13

    .line 2612
    :goto_5
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/PortNbr"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2613
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2614
    if-eqz v10, :cond_8

    .line 2616
    const/4 v5, 0x0

    .line 2617
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2618
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2619
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v15

    .line 2620
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 2622
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get PortNum from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2632
    :cond_2
    :goto_6
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v3, v0, [C

    .line 2633
    .local v3, "URLCharArray":[C
    invoke-virtual {v13}, Ljava/lang/String;->toCharArray()[C

    move-result-object v17

    invoke-virtual {v15}, Ljava/lang/String;->toCharArray()[C

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v3, v0, v1}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbDoDMBootStrapURI([C[C[C)[C

    move-result-object v3

    .line 2634
    if-nez v3, :cond_9

    .line 2635
    const/4 v9, -0x1

    goto/16 :goto_0

    .line 2530
    .end local v3    # "URLCharArray":[C
    :cond_3
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetAppID()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    .line 2531
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get AppID from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2551
    :cond_4
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerID()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    goto/16 :goto_2

    .line 2569
    :cond_5
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 2570
    const/16 v4, 0x1b

    .line 2571
    .local v4, "aclValue":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v4, v1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 2572
    const/4 v12, 0x0

    goto/16 :goto_3

    .line 2588
    .end local v4    # "aclValue":I
    :cond_6
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetPrefConRef()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    .line 2589
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get PrefConRef from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2607
    :cond_7
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_5

    .line 2627
    :cond_8
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerPort()I

    move-result v8

    .line 2628
    .local v8, "nNum":I
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    .line 2629
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get PortNum from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2637
    .end local v8    # "nNum":I
    .restart local v3    # "URLCharArray":[C
    :cond_9
    new-instance v17, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([C)V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 2638
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v7

    .line 2639
    .local v7, "getParser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    iget-object v0, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 2640
    iget-object v0, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 2641
    iget-object v0, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->pPath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    .line 2642
    iget v0, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    .line 2643
    iget-object v0, v7, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 2645
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 2646
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 2647
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path_Org:Ljava/lang/String;

    .line 2648
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 2649
    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 2650
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput-boolean v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    .line 2653
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthLevel"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2654
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2655
    if-eqz v10, :cond_d

    .line 2657
    const/4 v5, 0x0

    .line 2658
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2659
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2660
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    .line 2661
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get Client Auth Level from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2670
    :goto_7
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthType"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2671
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2672
    if-eqz v10, :cond_12

    .line 2674
    const/4 v5, 0x0

    .line 2675
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2676
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2677
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v16

    .line 2681
    .local v16, "szType":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthAAuthType2String(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .line 2687
    :goto_8
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "type > "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2689
    const-string v17, "DIGEST"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 2691
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    .line 2709
    :goto_9
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get Client Auth Type from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2718
    .end local v16    # "szType":Ljava/lang/String;
    :goto_a
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthName"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2719
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2720
    if-eqz v10, :cond_13

    .line 2722
    const/4 v5, 0x0

    .line 2723
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2724
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2725
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 2735
    :goto_b
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthSecret"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2736
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2737
    if-eqz v10, :cond_14

    .line 2739
    const/4 v5, 0x0

    .line 2740
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2741
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2742
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 2752
    :goto_c
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthData"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2753
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2754
    if-eqz v10, :cond_15

    .line 2756
    const/4 v5, 0x0

    .line 2757
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2758
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2760
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 2761
    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonceFormat:I

    .line 2762
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get ClientNonce from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2772
    :goto_d
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthLevel"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2773
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2774
    if-eqz v10, :cond_16

    .line 2776
    const/4 v5, 0x0

    .line 2777
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2778
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2779
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    .line 2780
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get Server Auth Level from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2789
    :goto_e
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthType"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2790
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2791
    if-eqz v10, :cond_1b

    .line 2793
    const/4 v5, 0x0

    .line 2794
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2795
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2796
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v16

    .line 2800
    .restart local v16    # "szType":Ljava/lang/String;
    :try_start_1
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthAAuthType2String(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v14

    .line 2806
    :goto_f
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "type > "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2808
    const-string v17, "DIGEST"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_17

    .line 2810
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    .line 2828
    :goto_10
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get Server Auth Type from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2838
    .end local v16    # "szType":Ljava/lang/String;
    :goto_11
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthName"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2839
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2840
    if-eqz v10, :cond_a

    .line 2842
    const/4 v5, 0x0

    .line 2843
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2844
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2845
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 2850
    :cond_a
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthSecret"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2851
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2852
    if-eqz v10, :cond_1c

    .line 2854
    const/4 v5, 0x0

    .line 2855
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2856
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2857
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 2867
    :goto_12
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/AAuthData"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2868
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2869
    if-eqz v10, :cond_1d

    .line 2871
    const/4 v5, 0x0

    .line 2872
    const/16 v17, 0x100

    move/from16 v0, v17

    new-array v5, v0, [C

    .line 2873
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v5, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2874
    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 2875
    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonceFormat:I

    .line 2876
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get ServerNonce from OM : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2884
    :goto_13
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/Ext"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2885
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2886
    if-eqz v10, :cond_b

    .line 2895
    :cond_b
    const-string v17, "%s%s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "/ConRef"

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2896
    invoke-static {v11, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v10

    .line 2897
    if-eqz v10, :cond_1e

    .line 2899
    const/16 v17, 0x0

    iget v0, v10, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v11, v12, v0, v2, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2909
    :cond_c
    :goto_14
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    goto/16 :goto_0

    .line 2665
    :cond_d
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetAuthLevel()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    .line 2666
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get Client Auth Level from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 2683
    .restart local v16    # "szType":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 2685
    .local v6, "ex":Ljava/lang/NumberFormatException;
    move-object/from16 v14, v16

    goto/16 :goto_8

    .line 2693
    .end local v6    # "ex":Ljava/lang/NumberFormatException;
    :cond_e
    const-string v17, "BASIC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 2695
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    goto/16 :goto_9

    .line 2697
    :cond_f
    const-string v17, "HMAC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 2699
    const/16 v17, 0x2

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    goto/16 :goto_9

    .line 2701
    :cond_10
    const-string v17, "DIGEST"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_11

    .line 2703
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    goto/16 :goto_9

    .line 2707
    :cond_11
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    goto/16 :goto_9

    .line 2713
    .end local v16    # "szType":Ljava/lang/String;
    :cond_12
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetAuthType()I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    .line 2714
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get Client Auth Type from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 2730
    :cond_13
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetUsername()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    goto/16 :goto_b

    .line 2747
    :cond_14
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetClientPassword()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    goto/16 :goto_c

    .line 2766
    :cond_15
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetClientNonce()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 2767
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonceFormat:I

    .line 2768
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get ClientNonce from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 2784
    :cond_16
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerAuthLevel()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    .line 2785
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get Server Auth Level from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 2802
    .restart local v16    # "szType":Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 2804
    .restart local v6    # "ex":Ljava/lang/NumberFormatException;
    move-object/from16 v14, v16

    goto/16 :goto_f

    .line 2812
    .end local v6    # "ex":Ljava/lang/NumberFormatException;
    :cond_17
    const-string v17, "BASIC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_18

    .line 2814
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    goto/16 :goto_10

    .line 2816
    :cond_18
    const-string v17, "HMAC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_19

    .line 2818
    const/16 v17, 0x2

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    goto/16 :goto_10

    .line 2820
    :cond_19
    const-string v17, "DIGEST"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1a

    .line 2822
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    goto/16 :goto_10

    .line 2826
    :cond_1a
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    goto/16 :goto_10

    .line 2832
    .end local v16    # "szType":Ljava/lang/String;
    :cond_1b
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerAuthType()I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p1

    iput v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    .line 2833
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get Server Auth Type from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_11

    .line 2862
    :cond_1c
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerPassword()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    goto/16 :goto_12

    .line 2880
    :cond_1d
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerNonce()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 2881
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "get ServerNonce from DB : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 2903
    :cond_1e
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_c

    .line 2905
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Name : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_14
.end method

.method public xdmAgentGetPathFromNode(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/lang/String;
    .locals 9
    .param p1, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p2, "node"    # Lcom/fmm/dm/eng/core/XDMVnode;

    .prologue
    .line 4374
    const/16 v7, 0xa

    new-array v0, v7, [Ljava/lang/String;

    .line 4375
    .local v0, "buf":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 4376
    .local v3, "level":I
    move-object v1, p2

    .line 4377
    .local v1, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    const/4 v4, 0x0

    .line 4379
    .local v4, "szName":Ljava/lang/String;
    iget-object v7, p1, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v8, p1, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v8, v8, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    invoke-static {v7, v8, p2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsGetParent(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 4380
    :goto_0
    if-eqz v1, :cond_0

    iget-object v7, p1, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v7, v7, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    if-eq v1, v7, :cond_0

    .line 4382
    iget-object v7, v1, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    aput-object v7, v0, v3

    .line 4383
    add-int/lit8 v3, v3, 0x1

    .line 4384
    iget-object v7, p1, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v8, p1, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    iget-object v8, v8, Lcom/fmm/dm/eng/core/XDMOmVfs;->root:Lcom/fmm/dm/eng/core/XDMVnode;

    invoke-static {v7, v8, v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsGetParent(Lcom/fmm/dm/eng/core/XDMOmVfs;Lcom/fmm/dm/eng/core/XDMVnode;Lcom/fmm/dm/eng/core/XDMVnode;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    goto :goto_0

    .line 4387
    :cond_0
    if-nez v1, :cond_1

    .line 4389
    const-string v6, "."

    .line 4390
    .local v6, "szOutbuf":Ljava/lang/String;
    move-object v4, v6

    move-object v5, v4

    .line 4406
    .end local v4    # "szName":Ljava/lang/String;
    .local v5, "szName":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 4394
    .end local v5    # "szName":Ljava/lang/String;
    .end local v6    # "szOutbuf":Ljava/lang/String;
    .restart local v4    # "szName":Ljava/lang/String;
    :cond_1
    const-string v6, "./"

    .line 4395
    .restart local v6    # "szOutbuf":Ljava/lang/String;
    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_2
    if-ltz v2, :cond_2

    .line 4397
    aget-object v7, v0, v2

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 4398
    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 4395
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 4400
    :cond_2
    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_3

    const-string v7, "./"

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_4

    .line 4402
    :cond_3
    iget-object v7, p2, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 4405
    :cond_4
    move-object v4, v6

    move-object v5, v4

    .line 4406
    .end local v4    # "szName":Ljava/lang/String;
    .restart local v5    # "szName":Ljava/lang/String;
    goto :goto_1
.end method

.method public xdmAgentGetWorkSpace()Lcom/fmm/dm/eng/core/XDMWorkspace;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    if-nez v0, :cond_0

    .line 126
    const-string v0, "dm_ws is NULL"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 127
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    goto :goto_0
.end method

.method public xdmAgentHandleCmd()I
    .locals 12

    .prologue
    const/4 v6, -0x5

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 5464
    sget-object v5, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 5466
    .local v5, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v2, 0x0

    .line 5467
    .local v2, "list":Lcom/fmm/dm/eng/core/XDMLinkedList;
    const/4 v4, 0x0

    .line 5468
    .local v4, "status":Lcom/fmm/dm/eng/parser/XDMParserStatus;
    const/4 v3, 0x0

    .line 5469
    .local v3, "res":I
    const/4 v1, 0x0

    .line 5471
    .local v1, "isAtomic":Z
    iget-object v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->procState:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    sget-object v9, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    if-ne v8, v9, :cond_0

    .line 5473
    iput v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->procStep:I

    .line 5474
    iput v10, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->cmdID:I

    .line 5477
    :cond_0
    :goto_0
    iget v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->procStep:I

    if-eq v8, v11, :cond_f

    .line 5480
    iget-boolean v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    if-nez v8, :cond_7

    .line 5482
    iget-object v2, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 5483
    invoke-static {v2, v7}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 5484
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 5516
    .local v0, "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    .line 5518
    iget-object v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicAlert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    if-eqz v8, :cond_f

    .line 5520
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    .line 5529
    :cond_2
    if-eqz v0, :cond_e

    .line 5531
    const-string v8, "Get"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Exec"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Alert"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Add"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Replace"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Copy"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Delete"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Atomic_Start"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Sequence_Start"

    iget-object v9, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_4

    .line 5534
    :cond_3
    iget v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    .line 5536
    :cond_4
    iget-object v8, v0, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5537
    iget-object v8, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    if-eqz v8, :cond_9

    .line 5539
    iput-boolean v10, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 5540
    const/4 v1, 0x1

    .line 5547
    :cond_5
    :goto_2
    invoke-virtual {p0, v0, v1, v4}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentVerifyCmd(Lcom/fmm/dm/agent/XDMAgent;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v3

    .line 5550
    iget-boolean v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    if-nez v8, :cond_6

    .line 5552
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 5553
    .restart local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 5558
    :cond_6
    if-ne v3, v6, :cond_a

    .line 5560
    const-string v7, "XDM_RET_PAUSED_BECAUSE_UIC_COMMAND"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5589
    .end local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    :goto_3
    return v6

    .line 5488
    :cond_7
    iget-object v2, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->sequenceList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 5490
    invoke-static {v2, v7}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 5491
    .restart local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    if-eqz v0, :cond_8

    .line 5493
    iget-object v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->sequenceList:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v8}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdSequenceBlock(Lcom/fmm/dm/eng/core/XDMLinkedList;)I

    .line 5501
    :goto_4
    iget-boolean v8, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    if-nez v8, :cond_1

    .line 5503
    iget-object v2, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    .line 5504
    invoke-static {v2, v7}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/fmm/dm/eng/core/XDMLinkedList;I)V

    .line 5505
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 5509
    .restart local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    check-cast v0, Lcom/fmm/dm/agent/XDMAgent;

    .line 5510
    .restart local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/fmm/dm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 5497
    :cond_8
    iput-boolean v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    goto :goto_4

    .line 5542
    :cond_9
    iget-object v8, v0, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    if-eqz v8, :cond_5

    .line 5544
    iput-boolean v10, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    goto :goto_2

    .line 5565
    :cond_a
    iput-boolean v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 5566
    iput-boolean v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 5567
    iput-boolean v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    .line 5570
    const/4 v8, 0x2

    if-eq v3, v8, :cond_b

    if-ne v3, v10, :cond_c

    .line 5572
    :cond_b
    iput v11, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->procStep:I

    .line 5573
    iput v7, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    move v6, v3

    .line 5574
    goto :goto_3

    .line 5576
    :cond_c
    if-ne v3, v11, :cond_d

    move v6, v3

    .line 5578
    goto :goto_3

    .line 5580
    :cond_d
    if-eqz v3, :cond_2

    .line 5582
    const-string v6, "Processing failed"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5583
    const/4 v6, -0x1

    goto :goto_3

    .line 5586
    :cond_e
    invoke-static {v2}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    goto/16 :goto_0

    .line 5588
    .end local v0    # "cmditem":Lcom/fmm/dm/agent/XDMAgent;
    :cond_f
    iget-object v6, v5, Lcom/fmm/dm/eng/core/XDMWorkspace;->list:Lcom/fmm/dm/eng/core/XDMLinkedList;

    invoke-static {v6}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/fmm/dm/eng/core/XDMLinkedList;)V

    move v6, v7

    .line 5589
    goto :goto_3
.end method

.method public xdmAgentInstallBootStrap(I[B)Z
    .locals 6
    .param p1, "nSize"    # I
    .param p2, "pData"    # [B

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2308
    const/4 v1, 0x0

    .line 2309
    .local v1, "nRet":I
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentInit()I

    .line 2310
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 2312
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_INIT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    .line 2313
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput v3, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->cmdID:I

    .line 2314
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    .line 2316
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeBootStrapNode()I

    move-result v1

    .line 2317
    if-eqz v1, :cond_0

    .line 2319
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 2320
    const-string v3, "Failed"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2352
    :goto_0
    return v2

    .line 2324
    :cond_0
    const/16 v4, 0x1c00

    if-le p1, v4, :cond_1

    .line 2326
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 2327
    const-string v3, "Size Over"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2333
    :cond_1
    :try_start_0
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4, p2}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2342
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput p1, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->bufsize:I

    .line 2343
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->procState:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 2345
    invoke-virtual {p0, p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentParsingBootStrap([B)I

    move-result v1

    .line 2346
    if-eqz v1, :cond_2

    .line 2348
    const-string v3, "Bootstrap Parse Error"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2335
    :catch_0
    move-exception v0

    .line 2337
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "Stream Write Error"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2338
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    move v2, v3

    .line 2352
    goto :goto_0
.end method

.method public xdmAgentIsAccessibleNode(Ljava/lang/String;)Z
    .locals 4
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 164
    const/4 v0, 0x0

    .line 190
    .local v0, "szInbox":Ljava/lang/String;
    const-string v3, "/AAuthSecret"

    invoke-static {p1, v3}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v1

    .line 195
    :cond_1
    const-string v3, "/AAuthData"

    invoke-static {p1, v3}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 200
    const/16 v3, 0xa

    invoke-static {v3}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFGetMOPath(I)Ljava/lang/String;

    move-result-object v0

    .line 201
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 203
    goto :goto_0

    .line 206
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {p1, v0, v3}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrncmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 211
    goto :goto_0
.end method

.method public xdmAgentIsPermanentNode(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Z
    .locals 3
    .param p1, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p2, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 218
    invoke-static {p1, p2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    .line 220
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v0, :cond_0

    .line 222
    iget v2, v0, Lcom/fmm/dm/eng/core/XDMVnode;->scope:I

    if-ne v2, v1, :cond_0

    .line 227
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public xdmAgentLibMakeSessionID()Ljava/lang/String;
    .locals 8

    .prologue
    .line 554
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 558
    .local v0, "data":Ljava/util/Calendar;
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 559
    .local v2, "szNowData":Ljava/lang/String;
    const/16 v4, 0xd

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 561
    .local v1, "second":I
    const-string v4, "%x%x"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 562
    .local v3, "szSessionid":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Make sessionid ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 564
    return-object v3
.end method

.method public xdmAgentLoadWorkSpace()I
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v10, -0x1

    const/4 v11, 0x0

    .line 801
    sget-object v9, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 802
    .local v9, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    if-nez v9, :cond_1

    .line 1051
    :cond_0
    :goto_0
    return v10

    .line 806
    :cond_1
    iget-object v6, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 807
    .local v6, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const/4 v1, 0x0

    .line 808
    .local v1, "dValue":[B
    const-string v7, ""

    .line 810
    .local v7, "szAccBuf":Ljava/lang/String;
    const/4 v5, 0x0

    .line 811
    .local v5, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    const/4 v3, 0x0

    .line 813
    .local v3, "nReSyncMode":I
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 814
    const-string v12, "/AAuthName"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 815
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 816
    if-eqz v5, :cond_0

    .line 821
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 822
    .local v0, "buf":[C
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 823
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    .line 825
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 826
    const-string v12, "/AAuthSecret"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 827
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 828
    if-eqz v5, :cond_0

    .line 832
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 833
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 834
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    .line 836
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 837
    const-string v12, "/AAuthType"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 838
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 839
    if-eqz v5, :cond_0

    .line 843
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 844
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 846
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiReSyncMode()I

    move-result v3

    .line 848
    if-ne v3, v14, :cond_4

    .line 851
    const-string v12, "DIGEST"

    invoke-static {v12}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthAAuthtring2Type(Ljava/lang/String;)I

    move-result v12

    iput v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    .line 861
    :goto_1
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 862
    const-string v12, "/AAuthType"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 863
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 864
    if-eqz v5, :cond_0

    .line 868
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 869
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 870
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiReSyncMode()I

    move-result v3

    .line 872
    if-ne v3, v14, :cond_5

    .line 875
    const-string v12, "DIGEST"

    invoke-static {v12}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthAAuthtring2Type(Ljava/lang/String;)I

    move-result v12

    iput v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    .line 888
    :cond_2
    :goto_2
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 889
    const-string v12, "/ServerID"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 890
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 891
    if-eqz v5, :cond_0

    .line 895
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 896
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 897
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    .line 899
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 900
    const-string v12, "/AAuthSecret"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 901
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 902
    if-eqz v5, :cond_0

    .line 907
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 908
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 909
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    .line 911
    if-ne v3, v14, :cond_6

    .line 913
    iget-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    aput-byte v11, v12, v11

    .line 914
    iget-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    aput-byte v11, v12, v14

    .line 915
    iget-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    const/4 v13, 0x2

    aput-byte v11, v12, v13

    .line 916
    iget-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    const/4 v13, 0x3

    aput-byte v11, v12, v13

    .line 965
    :cond_3
    :goto_3
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 966
    const-string v12, "/AAuthData"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 967
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Server szAccBuf) :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 968
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 969
    if-eqz v5, :cond_0

    .line 971
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-lez v12, :cond_b

    .line 973
    const/4 v0, 0x0

    .line 974
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 975
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 981
    :goto_4
    if-eqz v0, :cond_e

    .line 983
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    if-eq v12, v14, :cond_c

    .line 985
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_5
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-ge v2, v12, :cond_e

    .line 986
    iget-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    aget-char v13, v0, v2

    int-to-byte v13, v13

    aput-byte v13, v12, v2

    .line 985
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 857
    .end local v2    # "i":I
    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    .line 858
    .local v8, "szTmp":Ljava/lang/String;
    invoke-static {v8}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthAAuthtring2Type(Ljava/lang/String;)I

    move-result v12

    iput v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    goto/16 :goto_1

    .line 881
    .end local v8    # "szTmp":Ljava/lang/String;
    :cond_5
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    .line 882
    .restart local v8    # "szTmp":Ljava/lang/String;
    invoke-static {v8}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthAAuthtring2Type(Ljava/lang/String;)I

    move-result v12

    iput v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    .line 883
    iget v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    if-ne v12, v10, :cond_2

    .line 885
    iget v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    iput v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    goto/16 :goto_2

    .line 920
    .end local v8    # "szTmp":Ljava/lang/String;
    :cond_6
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 921
    const-string v12, "/AAuthData"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 922
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 923
    if-eqz v5, :cond_0

    .line 925
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-lez v12, :cond_7

    .line 927
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 928
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 934
    :goto_6
    if-eqz v0, :cond_3

    .line 936
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    if-eq v12, v14, :cond_9

    .line 938
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_7
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-ge v2, v12, :cond_8

    .line 939
    iget-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    aget-char v13, v0, v2

    int-to-byte v13, v13

    aput-byte v13, v12, v2

    .line 938
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 932
    .end local v2    # "i":I
    :cond_7
    const/4 v0, 0x0

    goto :goto_6

    .line 941
    .restart local v2    # "i":I
    :cond_8
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "node->size = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 945
    .end local v2    # "i":I
    :cond_9
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v1

    .line 946
    if-eqz v1, :cond_a

    .line 948
    array-length v12, v1

    new-array v12, v12, [B

    iput-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    .line 949
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_8
    array-length v12, v1

    if-ge v2, v12, :cond_3

    .line 950
    iget-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    aget-byte v13, v1, v2

    aput-byte v13, v12, v2

    .line 949
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 954
    .end local v2    # "i":I
    :cond_a
    const-string v12, "dValue is NULL"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 979
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 990
    :cond_c
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Server Next Noncenew String(buf) :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 991
    const/4 v1, 0x0

    .line 992
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v1

    .line 993
    if-eqz v1, :cond_d

    .line 995
    array-length v12, v1

    new-array v12, v12, [B

    iput-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    .line 996
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_9
    array-length v12, v1

    if-ge v2, v12, :cond_e

    .line 997
    iget-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    aget-byte v13, v1, v2

    aput-byte v13, v12, v2

    .line 996
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 1001
    .end local v2    # "i":I
    :cond_d
    const-string v12, " dValue is NULL"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1010
    :cond_e
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1011
    const-string v12, "/Addr"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1012
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 1013
    if-eqz v5, :cond_0

    .line 1017
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 1018
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1019
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    .line 1021
    sget-object v12, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 1022
    const-string v12, "/PortNbr"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1023
    invoke-static {v6, v7}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 1024
    if-eqz v5, :cond_0

    .line 1028
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 1029
    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1032
    :try_start_0
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->port:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1039
    :goto_a
    const-string v12, "./DevInfo/DevId"

    invoke-static {v6, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v5

    .line 1040
    if-eqz v5, :cond_0

    .line 1044
    iget v10, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v0, v10, [C

    .line 1045
    const-string v10, "./DevInfo/DevId"

    iget v12, v5, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v10, v11, v0, v12}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1047
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSourceURI:Ljava/lang/String;

    .line 1049
    const/4 v0, 0x0

    move v10, v11

    .line 1051
    goto/16 :goto_0

    .line 1034
    :catch_0
    move-exception v4

    .line 1036
    .local v4, "ne":Ljava/lang/NumberFormatException;
    invoke-virtual {v4}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_a
.end method

.method public xdmAgentMakeAcl(Lcom/fmm/dm/eng/core/XDMOmList;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMOmList;
    .locals 12
    .param p1, "acllist"    # Lcom/fmm/dm/eng/core/XDMOmList;
    .param p2, "szAcl"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x26

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 5096
    const/4 v0, 0x0

    .line 5097
    .local v0, "buf":[C
    const/4 v1, 0x0

    .line 5098
    .local v1, "subbuf":[C
    move-object v3, p2

    .line 5101
    .local v3, "szData":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    new-array v0, v5, [C

    .line 5102
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-static {v5, v11, v0}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v3

    .line 5103
    :goto_0
    if-eqz v0, :cond_2

    .line 5105
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 5106
    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    aput-char v7, v0, v5

    .line 5107
    :cond_0
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v4

    .line 5108
    .local v4, "szSub":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    new-array v1, v5, [C

    .line 5109
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    const/16 v6, 0x3d

    invoke-static {v5, v6, v1}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v4

    .line 5110
    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v2

    .line 5112
    .local v2, "szCmd":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 5114
    const-string v5, "Add"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    .line 5116
    invoke-virtual {p0, p1, v4, v8}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/fmm/dm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object p1

    .line 5136
    :cond_1
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 5148
    .end local v2    # "szCmd":Ljava/lang/String;
    .end local v4    # "szSub":Ljava/lang/String;
    :cond_2
    return-object p1

    .line 5118
    .restart local v2    # "szCmd":Ljava/lang/String;
    .restart local v4    # "szSub":Ljava/lang/String;
    :cond_3
    const-string v5, "Delete"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 5120
    invoke-virtual {p0, p1, v4, v9}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/fmm/dm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object p1

    goto :goto_1

    .line 5122
    :cond_4
    const-string v5, "Replace"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_5

    .line 5124
    const/16 v5, 0x10

    invoke-virtual {p0, p1, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/fmm/dm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object p1

    goto :goto_1

    .line 5126
    :cond_5
    const-string v5, "Get"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_6

    .line 5128
    const/16 v5, 0x8

    invoke-virtual {p0, p1, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/fmm/dm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object p1

    goto :goto_1

    .line 5130
    :cond_6
    const-string v5, "Exec"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    .line 5132
    invoke-virtual {p0, p1, v4, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/fmm/dm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/fmm/dm/eng/core/XDMOmList;

    move-result-object p1

    goto :goto_1

    .line 5138
    :cond_7
    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x61

    if-ne v5, v6, :cond_8

    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x6d

    if-ne v5, v6, :cond_8

    invoke-virtual {v3, v9}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x70

    if-ne v5, v6, :cond_8

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3b

    if-ne v5, v6, :cond_8

    .line 5141
    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 5144
    :cond_8
    const/4 v0, 0x0

    .line 5145
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    new-array v0, v5, [C

    .line 5146
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-static {v5, v11, v0}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v3

    .line 5147
    goto/16 :goto_0
.end method

.method public xdmAgentMakeDevDetailNode()I
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 1706
    sget-object v7, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 1707
    .local v7, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v2, v7, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 1710
    .local v2, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const-string v4, ""

    .line 1711
    .local v4, "szHwVersion":Ljava/lang/String;
    const-string v6, ""

    .line 1712
    .local v6, "szSwVersion":Ljava/lang/String;
    const-string v3, ""

    .line 1713
    .local v3, "szFwVersion":Ljava/lang/String;
    const-string v5, ""

    .line 1715
    .local v5, "szOEMName":Ljava/lang/String;
    const/16 v0, 0x8

    .line 1717
    .local v0, "aclValue":I
    const-string v8, "./DevDetail"

    invoke-static {v2, v8, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1719
    const/16 v0, 0x8

    .line 1720
    const-string v8, "./DevDetail/URI"

    invoke-static {v2, v8, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1722
    const/16 v0, 0x8

    .line 1723
    const-string v8, "./DevDetail/URI/MaxDepth"

    const-string v9, "0"

    invoke-static {v2, v8, v9, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1725
    const/16 v0, 0x8

    .line 1726
    const-string v8, "./DevDetail/URI/MaxTotLen"

    const-string v9, "0"

    invoke-static {v2, v8, v9, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1728
    const/16 v0, 0x8

    .line 1729
    const-string v8, "./DevDetail/URI/MaxSegLen"

    const-string v9, "0"

    invoke-static {v2, v8, v9, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1731
    const/16 v0, 0x8

    .line 1732
    const-string v8, "./DevDetail/DevTyp"

    const-string v9, "phone"

    invoke-static {v2, v8, v9, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1734
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetOEMName()Ljava/lang/String;

    move-result-object v5

    .line 1735
    const/16 v0, 0x8

    .line 1736
    const-string v8, "./DevDetail/OEM"

    invoke-static {v2, v8, v5, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1738
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFirmwareVersion()Ljava/lang/String;

    move-result-object v3

    .line 1739
    const/16 v0, 0x8

    .line 1740
    const-string v8, "./DevDetail/FwV"

    invoke-static {v2, v8, v3, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1742
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetSoftwareVersion()Ljava/lang/String;

    move-result-object v6

    .line 1743
    const/16 v0, 0x8

    .line 1744
    const-string v8, "./DevDetail/SwV"

    invoke-static {v2, v8, v6, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1746
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetHardwareVersion()Ljava/lang/String;

    move-result-object v4

    .line 1747
    const/16 v0, 0x8

    .line 1748
    const-string v8, "./DevDetail/HwV"

    invoke-static {v2, v8, v4, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1750
    const/16 v0, 0x8

    .line 1751
    const-string v8, "./DevDetail/LrgObj"

    const-string v9, "false"

    invoke-static {v2, v8, v9, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1752
    const-string v8, "./DevDetail/LrgObj"

    invoke-static {v2, v8}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v1

    .line 1753
    .local v1, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v1, :cond_0

    .line 1755
    const/4 v8, 0x3

    iput v8, v1, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    .line 1756
    const/4 v8, 0x0

    iput-object v8, v1, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 1759
    :cond_0
    const/16 v0, 0xa

    .line 1760
    const-string v8, "./DevDetail/Ext"

    invoke-static {v2, v8, v0, v10}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1762
    const/4 v4, 0x0

    .line 1763
    const/4 v6, 0x0

    .line 1764
    const/4 v3, 0x0

    .line 1765
    const/4 v5, 0x0

    .line 1767
    const/4 v8, 0x0

    return v8
.end method

.method public xdmAgentMakeDevInfoNode()I
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 1655
    sget-object v6, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 1656
    .local v6, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v1, v6, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 1658
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const-string v5, ""

    .line 1659
    .local v5, "szModelName":Ljava/lang/String;
    const-string v4, ""

    .line 1660
    .local v4, "szManuFact":Ljava/lang/String;
    const-string v2, ""

    .line 1661
    .local v2, "szDevID":Ljava/lang/String;
    const-string v3, ""

    .line 1663
    .local v3, "szLang":Ljava/lang/String;
    const/16 v0, 0xb

    .line 1664
    .local v0, "aclValue":I
    const-string v7, "./DevInfo"

    invoke-static {v1, v7, v0, v9}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1666
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v2

    .line 1667
    const/16 v0, 0x8

    .line 1668
    const-string v7, "./DevInfo/DevId"

    invoke-static {v1, v7, v2, v0, v9}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1670
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetManufacturer()Ljava/lang/String;

    move-result-object v4

    .line 1671
    const/16 v0, 0xb

    .line 1672
    const-string v7, "./DevInfo/Man"

    invoke-static {v1, v7, v4, v0, v9}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1674
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetModel()Ljava/lang/String;

    move-result-object v5

    .line 1675
    const/16 v0, 0xb

    .line 1676
    const-string v7, "./DevInfo/Mod"

    invoke-static {v1, v7, v5, v0, v9}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1678
    const/16 v0, 0xb

    .line 1686
    const-string v7, "./DevInfo/DmV"

    const-string v8, " 1.2"

    invoke-static {v1, v7, v8, v0, v9}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1689
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetLanguage()Ljava/lang/String;

    move-result-object v3

    .line 1690
    const/16 v0, 0xb

    .line 1691
    const-string v7, "./DevInfo/Lang"

    invoke-static {v1, v7, v3, v0, v9}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1693
    const/16 v0, 0xa

    .line 1694
    const-string v7, "./DevInfo/Ext"

    invoke-static {v1, v7, v0, v9}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1696
    const/4 v5, 0x0

    .line 1697
    const/4 v4, 0x0

    .line 1698
    const/4 v2, 0x0

    .line 1699
    const/4 v3, 0x0

    .line 1701
    const/4 v7, 0x0

    return v7
.end method

.method public xdmAgentMakeNode()I
    .locals 4

    .prologue
    .line 600
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 601
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v1, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 603
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    invoke-static {v1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmInit(Lcom/fmm/dm/eng/core/XDMOmTree;)I

    move-result v3

    if-eqz v3, :cond_0

    .line 605
    const/4 v3, -0x1

    .line 625
    :goto_0
    return v3

    .line 608
    :cond_0
    const-string v3, "*"

    invoke-static {v1, v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmSetServerId(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)I

    .line 610
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeSyncMLNode()I

    .line 611
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDevInfoNode()I

    .line 612
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDevDetailNode()I

    .line 616
    new-instance v0, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;-><init>()V

    .line 617
    .local v0, "lawmoAgent":Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;
    invoke-virtual {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentMakeNode()V

    .line 622
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAgent;->xamtAgentMakeNode()V

    .line 625
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public xdmAgentMakeSyncMLNode()I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x2

    .line 1311
    sget-object v9, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 1312
    .local v9, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-object v1, v9, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 1314
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    const-string v2, ""

    .line 1315
    .local v2, "szAccBuf":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1316
    .local v6, "authType":I
    const-string v3, ""

    .line 1318
    .local v3, "szTempBuf":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1321
    .local v7, "nRet":I
    const/16 v4, 0x9

    .line 1322
    .local v4, "aclValue":I
    const-string v0, "."

    invoke-static {v1, v0, v4, v11}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1325
    const/16 v4, 0x1b

    .line 1326
    const-string v0, "./SyncML"

    invoke-static {v1, v0, v4, v11}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1329
    const/16 v4, 0x1b

    .line 1330
    const-string v0, "."

    invoke-static {v1, v0, v4, v11}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1332
    const/16 v4, 0x1b

    .line 1333
    const-string v0, "./DMAcc"

    invoke-static {v1, v0, v4, v11}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1336
    const/16 v4, 0x1b

    .line 1337
    const-string v0, "./SyncML/Con"

    invoke-static {v1, v0, v4, v11}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1339
    const/16 v4, 0x1b

    .line 1340
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    invoke-static {v1, v0, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1342
    const-string v3, "w7"

    .line 1343
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1344
    const-string v0, "/AppID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1345
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1347
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerID()Ljava/lang/String;

    move-result-object v3

    .line 1348
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1349
    const-string v0, "/ServerID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1350
    const/16 v4, 0x1b

    .line 1351
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1353
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileName()Ljava/lang/String;

    move-result-object v3

    .line 1354
    const/16 v4, 0x1b

    .line 1355
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1356
    const-string v0, "/Name"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1357
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1359
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetPrefConRef()Ljava/lang/String;

    move-result-object v3

    .line 1360
    const/16 v4, 0x1b

    .line 1361
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1362
    const-string v0, "/PrefConRef"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1363
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1365
    const/16 v4, 0x1b

    .line 1366
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1367
    const-string v0, "/ToConRef"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1368
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1370
    const/16 v4, 0x1b

    .line 1371
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 1372
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1374
    const/16 v4, 0x1b

    .line 1375
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 1376
    const-string v0, "/ConRef"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1377
    const-string v0, "dataProxy"

    invoke-static {v1, v2, v0, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1379
    const/16 v4, 0x1b

    .line 1380
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1381
    const-string v0, "/AppAddr"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1382
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1384
    const/16 v4, 0x1b

    .line 1385
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1386
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1388
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v3

    .line 1390
    const/16 v4, 0x1b

    .line 1391
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1392
    const-string v0, "/Addr"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1393
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1395
    const/16 v4, 0x1b

    .line 1396
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1397
    const-string v0, "/AddrType"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1398
    const-string v0, "URI"

    invoke-static {v1, v2, v0, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1400
    const/16 v4, 0x1b

    .line 1401
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1402
    const-string v0, "/Port"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1403
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1405
    const/16 v4, 0x1b

    .line 1406
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 1407
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1409
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerPort()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 1410
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ServerPort = "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1411
    const/16 v4, 0x1b

    .line 1412
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 1413
    const-string v0, "/PortNbr"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1414
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1416
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetAuthType()I

    move-result v6

    .line 1417
    const/16 v4, 0x1b

    .line 1418
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1419
    const-string v0, "/AAuthPref"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1420
    invoke-static {v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthCredType2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1422
    const/16 v4, 0x1b

    .line 1423
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1424
    const-string v0, "/AppAuth"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1425
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1427
    const/16 v4, 0x1b

    .line 1428
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1429
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1431
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetAuthLevel()Ljava/lang/String;

    move-result-object v3

    .line 1432
    const/16 v4, 0x1b

    .line 1433
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1434
    const-string v0, "/AAuthLevel"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1435
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1437
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetAuthType()I

    move-result v6

    .line 1438
    const/16 v4, 0x1b

    .line 1439
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1440
    const-string v0, "/AAuthType"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1441
    invoke-static {v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthAAuthType2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1443
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetUsername()Ljava/lang/String;

    move-result-object v3

    .line 1444
    const/16 v4, 0x1b

    .line 1445
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1446
    const-string v0, "/AAuthName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1447
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1449
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetClientPassword()Ljava/lang/String;

    move-result-object v3

    .line 1450
    const/16 v4, 0x1b

    .line 1451
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1452
    const-string v0, "/AAuthSecret"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1453
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1455
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetClientNonce()Ljava/lang/String;

    move-result-object v3

    .line 1456
    invoke-direct {p0, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCheckNonce(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1458
    const/16 v4, 0x1b

    .line 1459
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1460
    const-string v0, "/AAuthData"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    .line 1461
    invoke-virtual/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccB64(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1463
    const/16 v4, 0x1b

    .line 1464
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1465
    invoke-static {v1, v2, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1467
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerAuthLevel()Ljava/lang/String;

    move-result-object v3

    .line 1468
    const/16 v4, 0x1b

    .line 1469
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1470
    const-string v0, "/AAuthLevel"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1471
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1473
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerAuthType()I

    move-result v6

    .line 1474
    const/16 v4, 0x1b

    .line 1475
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1476
    const-string v0, "/AAuthType"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1477
    invoke-static {v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthAAuthType2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1479
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerID()Ljava/lang/String;

    move-result-object v3

    .line 1480
    const/16 v4, 0x1b

    .line 1481
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1482
    const-string v0, "/AAuthName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1483
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1485
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerPassword()Ljava/lang/String;

    move-result-object v3

    .line 1486
    const/16 v4, 0x1b

    .line 1487
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1488
    const-string v0, "/AAuthSecret"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1489
    invoke-static {v1, v2, v3, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1491
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerNonce()Ljava/lang/String;

    move-result-object v3

    .line 1492
    invoke-direct {p0, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCheckNonce(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1493
    const/16 v4, 0x1b

    .line 1494
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1495
    const-string v0, "/AAuthData"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    .line 1496
    invoke-virtual/range {v0 .. v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccB64(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1498
    const/16 v4, 0x1b

    .line 1499
    sget-object v0, Lcom/fmm/dm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/fmm/dm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1500
    const-string v0, "/Ext"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1501
    const-string v0, " "

    invoke-static {v1, v2, v0, v4, v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1503
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/fmm/dm/eng/parser/XDMDDFParser;->xdmDDFGetMOPath(I)Ljava/lang/String;

    move-result-object v8

    .line 1504
    .local v8, "szInbox":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1506
    const/16 v4, 0x1b

    .line 1507
    invoke-static {v1, v8, v4, v11}, Lcom/fmm/dm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1510
    :cond_0
    const/4 v7, 0x0

    .line 1511
    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/dm/agent/XDMAgent;->pAccName:Ljava/lang/String;

    .line 1512
    const/4 v3, 0x0

    .line 1514
    return v7
.end method

.method public xdmAgentMakeTndsSubTree(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;ILjava/lang/String;)V
    .locals 20
    .param p1, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p2, "node"    # Lcom/fmm/dm/eng/core/XDMVnode;
    .param p3, "nFlag"    # I
    .param p4, "szPath"    # Ljava/lang/String;

    .prologue
    .line 4520
    const/4 v11, 0x0

    .line 4521
    .local v11, "nLen":I
    const/4 v5, 0x0

    .line 4522
    .local v5, "ac":I
    const/16 v16, 0x0

    .line 4523
    .local v16, "szTag":Ljava/lang/String;
    const/4 v12, 0x0

    .line 4524
    .local v12, "szData":Ljava/lang/String;
    const/4 v13, 0x0

    .line 4525
    .local v13, "szFormat":Ljava/lang/String;
    const/4 v14, 0x0

    .line 4526
    .local v14, "szNodeProperty":Ljava/lang/String;
    const/16 v17, 0x0

    .line 4527
    .local v17, "szType":Ljava/lang/String;
    const/4 v15, 0x0

    .line 4529
    .local v15, "szNodeUri":Ljava/lang/String;
    const/4 v8, 0x0

    .line 4530
    .local v8, "cur":Lcom/fmm/dm/eng/core/XDMVnode;
    const/4 v6, 0x0

    .line 4531
    .local v6, "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    new-instance v9, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v9}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 4532
    .local v9, "list":Lcom/fmm/dm/eng/core/XDMOmList;
    const/4 v10, 0x0

    .line 4534
    .local v10, "nFileId":I
    if-nez p2, :cond_0

    .line 4726
    :goto_0
    return-void

    .line 4538
    :cond_0
    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/fmm/dm/eng/core/XDMVnode;->childlist:Lcom/fmm/dm/eng/core/XDMVnode;

    .line 4539
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetFileIdTNDS()I

    move-result v10

    .line 4541
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x4

    aget-object v16, v18, v19

    .line 4543
    move-object/from16 v14, v16

    .line 4545
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 4547
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x8

    aget-object v16, v18, v19

    .line 4548
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "szTag : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4549
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4550
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "szPath : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4551
    move-object/from16 v0, p4

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4552
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x9

    aget-object v16, v18, v19

    .line 4553
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "szTag"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4554
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4556
    :cond_1
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    .line 4558
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x6

    aget-object v16, v18, v19

    .line 4559
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4560
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4561
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "node.name : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4562
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x7

    aget-object v16, v18, v19

    .line 4563
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4566
    :cond_2
    move-object/from16 v0, p2

    iget v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v13

    .line 4568
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    move-object/from16 v18, v0

    if-nez v18, :cond_3

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_c

    .line 4570
    :cond_3
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xc

    aget-object v16, v18, v19

    .line 4571
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4573
    and-int/lit8 v18, p3, 0x2

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 4575
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 4577
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x10

    aget-object v16, v18, v19

    .line 4578
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4579
    const-string v18, "<"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4580
    invoke-virtual {v14, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4581
    const-string v18, "/>"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4582
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x11

    aget-object v16, v18, v19

    .line 4583
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4587
    :cond_4
    and-int/lit8 v18, p3, 0x4

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 4589
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 4591
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 4592
    iget-object v0, v9, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    move-object/from16 v17, v0

    .end local v17    # "szType":Ljava/lang/String;
    check-cast v17, Ljava/lang/String;

    .line 4594
    .restart local v17    # "szType":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_5

    .line 4596
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x12

    aget-object v16, v18, v19

    .line 4597
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4598
    const-string v18, "<MIME>"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4599
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4600
    const-string v18, "</MIME>"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4601
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x13

    aget-object v16, v18, v19

    .line 4602
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4606
    :cond_5
    const/4 v9, 0x0

    .line 4608
    and-int/lit8 v18, p3, 0x1

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 4610
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    .line 4612
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/fmm/dm/eng/core/XDMVnode;->acl:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 4613
    iget-object v6, v9, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .end local v6    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    check-cast v6, Lcom/fmm/dm/eng/core/XDMOmAcl;

    .line 4615
    .restart local v6    # "acl":Lcom/fmm/dm/eng/core/XDMOmAcl;
    if-eqz v6, :cond_b

    .line 4617
    iget v5, v6, Lcom/fmm/dm/eng/core/XDMOmAcl;->ac:I

    .line 4618
    if-eqz v5, :cond_b

    .line 4620
    const/4 v4, 0x0

    .line 4621
    .local v4, "IsOtherACL":Z
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xe

    aget-object v16, v18, v19

    .line 4622
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4623
    and-int/lit8 v18, v5, 0x1

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 4625
    const-string v18, "Add=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4626
    const/4 v4, 0x1

    .line 4628
    :cond_6
    and-int/lit8 v18, v5, 0x2

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 4630
    if-eqz v4, :cond_e

    .line 4632
    const-string v18, "&amp;Delete=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4640
    :cond_7
    :goto_1
    and-int/lit8 v18, v5, 0x4

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 4642
    if-eqz v4, :cond_f

    .line 4644
    const-string v18, "&amp;Exec=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4652
    :cond_8
    :goto_2
    and-int/lit8 v18, v5, 0x8

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 4654
    if-eqz v4, :cond_10

    .line 4656
    const-string v18, "&amp;Get=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4664
    :cond_9
    :goto_3
    and-int/lit8 v18, v5, 0x10

    const/16 v19, 0x10

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 4666
    if-eqz v4, :cond_11

    .line 4668
    const-string v18, "&amp;Replace=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4676
    :cond_a
    :goto_4
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xf

    aget-object v16, v18, v19

    .line 4677
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4683
    .end local v4    # "IsOtherACL":Z
    :cond_b
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xd

    aget-object v16, v18, v19

    .line 4684
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4687
    :cond_c
    and-int/lit8 v18, p3, 0x8

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_13

    .line 4689
    move-object/from16 v0, p2

    iget v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    if-lez v18, :cond_12

    .line 4691
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xa

    aget-object v16, v18, v19

    .line 4692
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4693
    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 4695
    const-string v12, ""

    .line 4696
    invoke-virtual/range {p0 .. p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentGetPathFromNode(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;)Ljava/lang/String;

    move-result-object v15

    .line 4697
    move-object/from16 v0, p2

    iget v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v7, v0, [C

    .line 4698
    .local v7, "cTemp":[C
    const/16 v18, 0x0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v15, v1, v7, v2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    move-result v11

    .line 4699
    invoke-static {v7}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    .line 4700
    if-lez v11, :cond_d

    .line 4702
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 4704
    :cond_d
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xb

    aget-object v16, v18, v19

    .line 4705
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    .line 4717
    .end local v7    # "cTemp":[C
    :goto_5
    if-eqz v8, :cond_14

    .line 4719
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v8, v2, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeTndsSubTree(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMVnode;ILjava/lang/String;)V

    .line 4720
    iget-object v8, v8, Lcom/fmm/dm/eng/core/XDMVnode;->next:Lcom/fmm/dm/eng/core/XDMVnode;

    goto :goto_5

    .line 4636
    .restart local v4    # "IsOtherACL":Z
    :cond_e
    const-string v18, "Delete=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4637
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 4648
    :cond_f
    const-string v18, "Exec=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4649
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 4660
    :cond_10
    const-string v18, "Get=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4661
    const/4 v4, 0x1

    goto/16 :goto_3

    .line 4672
    :cond_11
    const-string v18, "Replace=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 4673
    const/4 v4, 0x1

    goto/16 :goto_4

    .line 4709
    .end local v4    # "IsOtherACL":Z
    :cond_12
    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    goto :goto_5

    .line 4714
    :cond_13
    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    goto :goto_5

    .line 4723
    :cond_14
    sget-object v18, Lcom/fmm/dm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x5

    aget-object v16, v18, v19

    .line 4725
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/fmm/dm/db/file/XDB;->xdbAppendFile(I[B)I

    goto/16 :goto_0
.end method

.method public xdmAgentMgmtPackage(Lcom/fmm/dm/eng/core/XDMEncoder;)I
    .locals 6
    .param p1, "e"    # Lcom/fmm/dm/eng/core/XDMEncoder;

    .prologue
    const/4 v2, -0x1

    const/4 v5, -0x4

    const/4 v3, 0x0

    .line 1257
    sget-object v1, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 1259
    .local v1, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget-boolean v4, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-eqz v4, :cond_1

    .line 1261
    const-string v4, "1222"

    invoke-virtual {p0, p1, v4}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/fmm/dm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v0

    .line 1262
    .local v0, "res":I
    if-eqz v0, :cond_1

    .line 1264
    if-ne v0, v5, :cond_0

    .line 1266
    iput-boolean v3, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 1304
    :goto_0
    return v2

    .line 1270
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1275
    .end local v0    # "res":I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageStatus(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v0

    .line 1276
    .restart local v0    # "res":I
    if-eqz v0, :cond_3

    .line 1278
    if-ne v0, v5, :cond_2

    .line 1280
    iput-boolean v3, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 1284
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1288
    :cond_3
    invoke-virtual {p0, p1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackageResults(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v0

    .line 1289
    if-eqz v0, :cond_5

    .line 1291
    if-ne v0, v5, :cond_4

    .line 1293
    iput-boolean v3, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 1297
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1302
    :cond_5
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/fmm/dm/eng/core/XDMWorkspace;->endOfMsg:Z

    move v2, v3

    .line 1304
    goto :goto_0
.end method

.method public xdmAgentParsingBootStrap([B)I
    .locals 5
    .param p1, "pData"    # [B

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2444
    const/4 v0, 0x0

    .line 2446
    .local v0, "nRet":I
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->procState:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    sget-object v3, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    if-ne v2, v3, :cond_0

    .line 2448
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput v1, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    .line 2449
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    .line 2450
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    .line 2451
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput-boolean v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->isFinal:Z

    .line 2453
    invoke-static {p1}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentParsingWbxml([B)I

    move-result v0

    .line 2454
    const/4 v0, 0x0

    .line 2456
    if-eqz v0, :cond_0

    .line 2458
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 2459
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Parsing package failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Abort session"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v1, v0

    .line 2487
    :goto_0
    return v1

    .line 2464
    :cond_0
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentHandleCmd()I

    move-result v0

    .line 2467
    const/4 v2, -0x5

    if-ne v0, v2, :cond_1

    .line 2469
    const-string v2, "XDM_RET_PAUSED_BECAUSE_UIC_COMMAND"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 2472
    :cond_1
    if-eqz v0, :cond_2

    .line 2474
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 2475
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling Commands failed("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). Abort session"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2476
    const/4 v1, -0x1

    goto :goto_0

    .line 2479
    :cond_2
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget-object v2, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v3, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_FINISH:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v2, v3, :cond_3

    .line 2481
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->msgID:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->msgID:I

    .line 2484
    :cond_3
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    sget-object v3, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_PROCESSING:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    .line 2485
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput v1, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authCount:I

    .line 2486
    const-string v1, "Parsing Finished"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move v1, v0

    .line 2487
    goto :goto_0
.end method

.method public xdmAgentReadBootStrap(I[B)Z
    .locals 6
    .param p1, "nSize"    # I
    .param p2, "pData"    # [B

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2358
    const/4 v1, 0x0

    .line 2359
    .local v1, "nRet":I
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentInit()I

    .line 2360
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 2362
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_INIT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    .line 2363
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput v3, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->cmdID:I

    .line 2364
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicStep:Lcom/fmm/dm/interfaces/XDMInterface$XDMAtomicStep;

    .line 2366
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeBootStrapNode()I

    move-result v1

    .line 2367
    if-eqz v1, :cond_0

    .line 2369
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 2370
    const-string v3, "Failed"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2402
    :goto_0
    return v2

    .line 2374
    :cond_0
    const/16 v4, 0x1c00

    if-le p1, v4, :cond_1

    .line 2376
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 2377
    const-string v3, "Size Over"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2383
    :cond_1
    :try_start_0
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget-object v4, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4, p2}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2392
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iput p1, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->bufsize:I

    .line 2393
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->procState:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 2395
    invoke-virtual {p0, p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentParsingBootStrap([B)I

    move-result v1

    .line 2396
    if-eqz v1, :cond_2

    .line 2398
    const-string v3, "Bootstrap Parse Error"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2385
    :catch_0
    move-exception v0

    .line 2387
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "Stream Write Error"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2388
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    move v2, v3

    .line 2402
    goto :goto_0
.end method

.method public xdmAgentSendPackage()I
    .locals 17

    .prologue
    .line 427
    const-string v3, ""

    .line 428
    .local v3, "szHmacData":Ljava/lang/String;
    const-string v4, ""

    .line 429
    .local v4, "szContentRange":Ljava/lang/String;
    sget-object v16, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 430
    .local v16, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v13, 0x0

    .line 432
    .local v13, "ret":I
    move-object/from16 v0, v16

    iget v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    if-eqz v1, :cond_0

    move-object/from16 v0, v16

    iget v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 436
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    const-string v5, "POST"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    .line 447
    const/4 v1, 0x7

    if-ne v13, v1, :cond_1

    .line 449
    const/4 v1, 0x1

    sput-boolean v1, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 450
    const/4 v13, 0x4

    :goto_0
    move v14, v13

    .line 549
    .end local v13    # "ret":I
    .local v14, "ret":I
    :goto_1
    return v14

    .line 438
    .end local v14    # "ret":I
    .restart local v13    # "ret":I
    :catch_0
    move-exception v12

    .line 440
    .local v12, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 441
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 442
    const/4 v13, -0x3

    .line 443
    const/16 v1, 0x1b

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v14, v13

    .line 444
    .end local v13    # "ret":I
    .restart local v14    # "ret":I
    goto :goto_1

    .line 454
    .end local v12    # "e":Ljava/lang/NullPointerException;
    .end local v14    # "ret":I
    .restart local v13    # "ret":I
    :cond_1
    sget-boolean v1, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    if-eqz v1, :cond_2

    .line 456
    const/4 v1, 0x0

    sput-boolean v1, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 461
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v13

    .line 476
    :goto_2
    if-nez v13, :cond_3

    .line 478
    const/16 v1, 0x18

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 463
    :catch_1
    move-exception v12

    .line 465
    .local v12, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v12}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 466
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 467
    const/4 v13, -0x3

    .line 474
    goto :goto_2

    .line 469
    .end local v12    # "e":Ljava/net/SocketTimeoutException;
    :catch_2
    move-exception v12

    .line 471
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 472
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 473
    const/4 v13, -0x3

    goto :goto_2

    .line 480
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v1, -0x2

    if-ne v13, v1, :cond_4

    .line 482
    const/16 v1, 0x15

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 487
    :cond_4
    const/16 v1, 0x1b

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 494
    :cond_5
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMWbxmlEncoder;->xdmWbxEncGetBufferSize()I

    move-result v11

    .line 495
    .local v11, "len":I
    move-object/from16 v0, v16

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->credType:I

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->nextNonce:[B

    array-length v9, v1

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    invoke-static/range {v5 .. v11}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v15

    .line 497
    .local v15, "szMac":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "algorithm=MD5, username=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mac="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 501
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    const-string v5, "POST"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    move-result v13

    .line 512
    const/4 v1, 0x7

    if-ne v13, v1, :cond_6

    .line 514
    const/4 v1, 0x1

    sput-boolean v1, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 515
    const/4 v13, 0x4

    goto/16 :goto_0

    .line 503
    :catch_3
    move-exception v12

    .line 505
    .local v12, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 506
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 507
    const/4 v13, -0x3

    .line 508
    const/16 v1, 0x1b

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v14, v13

    .line 509
    .end local v13    # "ret":I
    .restart local v14    # "ret":I
    goto/16 :goto_1

    .line 519
    .end local v12    # "e":Ljava/lang/NullPointerException;
    .end local v14    # "ret":I
    .restart local v13    # "ret":I
    :cond_6
    sget-boolean v1, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    if-eqz v1, :cond_7

    .line 521
    const/4 v1, 0x0

    sput-boolean v1, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 526
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/fmm/dm/eng/core/XDMWorkspace;->e:Lcom/fmm/dm/eng/core/XDMEncoder;

    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMEncoder;->xdmEncGetBufferSize(Lcom/fmm/dm/eng/core/XDMEncoder;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_4

    move-result v13

    .line 534
    :goto_3
    if-nez v13, :cond_8

    .line 536
    const/16 v1, 0x18

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 528
    :catch_4
    move-exception v12

    .line 530
    .local v12, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v12}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 531
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 532
    const/4 v13, -0x3

    goto :goto_3

    .line 538
    .end local v12    # "e":Ljava/net/SocketTimeoutException;
    :cond_8
    const/4 v1, -0x2

    if-ne v13, v1, :cond_9

    .line 540
    const/16 v1, 0x15

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 545
    :cond_9
    const/16 v1, 0x1b

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public xdmAgentSetOMAccB64(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6
    .param p1, "omt"    # Ljava/lang/Object;
    .param p2, "szPath"    # Ljava/lang/String;
    .param p3, "szData"    # Ljava/lang/String;
    .param p4, "aclValue"    # I
    .param p5, "scope"    # I

    .prologue
    .line 1854
    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 1859
    .local v1, "om":Lcom/fmm/dm/eng/core/XDMOmTree;
    invoke-static {v1, p2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    .line 1860
    .local v0, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-nez v0, :cond_0

    .line 1862
    invoke-virtual {p0, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1863
    invoke-static {v1, p2, p4, p5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1880
    :goto_0
    return-void

    .line 1867
    :cond_0
    iget v4, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    new-array v3, v4, [C

    .line 1868
    .local v3, "temp":[C
    const/4 v4, 0x0

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    invoke-static {v1, p2, v4, v3, v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1870
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    .line 1871
    .local v2, "szTmp":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    iget v5, v0, Lcom/fmm/dm/eng/core/XDMVnode;->size:I

    if-eq v4, v5, :cond_2

    .line 1873
    invoke-virtual {p0, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1879
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 1880
    goto :goto_0

    .line 1875
    :cond_2
    invoke-virtual {v2, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 1877
    invoke-virtual {p0, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 10
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    .line 1933
    sget-object v8, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 1936
    .local v8, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v2, 0x0

    .line 1938
    .local v2, "nLen":I
    if-nez p2, :cond_1

    .line 1940
    const-string v0, "data is NULL"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1965
    :cond_0
    :goto_0
    return-void

    .line 1944
    :cond_1
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v2, v0

    .line 1946
    if-gtz v2, :cond_2

    .line 1948
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-static {v0, p1, v9}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmDeleteImplicit(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    .line 1949
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The [%s] node is 0 length"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1952
    :cond_2
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    const/4 v3, 0x0

    move-object v1, p1

    move-object v4, p2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 1953
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-static {v0, p1}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v7

    .line 1954
    .local v7, "node":Lcom/fmm/dm/eng/core/XDMVnode;
    if-eqz v7, :cond_0

    .line 1956
    iget-object v0, v7, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    if-eqz v0, :cond_3

    .line 1957
    iget-object v0, v7, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/fmm/dm/eng/core/XDMOmList;)V

    .line 1959
    :cond_3
    new-instance v6, Lcom/fmm/dm/eng/core/XDMOmList;

    invoke-direct {v6}, Lcom/fmm/dm/eng/core/XDMOmList;-><init>()V

    .line 1960
    .local v6, "list":Lcom/fmm/dm/eng/core/XDMOmList;
    const-string v0, "text/plain"

    iput-object v0, v6, Lcom/fmm/dm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 1961
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/fmm/dm/eng/core/XDMOmList;->next:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 1962
    iput-object v6, v7, Lcom/fmm/dm/eng/core/XDMVnode;->type:Lcom/fmm/dm/eng/core/XDMOmList;

    .line 1963
    iput v9, v7, Lcom/fmm/dm/eng/core/XDMVnode;->format:I

    goto :goto_0
.end method

.method public xdmAgentStartAlert()I
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 5426
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 5428
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v1, 0x0

    .line 5430
    .local v1, "ret":I
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentInit()I

    move-result v4

    if-eqz v4, :cond_1

    .line 5459
    :cond_0
    :goto_0
    return v3

    .line 5435
    :cond_1
    iget v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->appId:I

    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbGetNotiEvent(I)I

    move-result v0

    .line 5437
    .local v0, "nNotiEvent":I
    if-ltz v0, :cond_2

    .line 5439
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiSessionID()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    .line 5446
    :goto_1
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeNode()I

    move-result v4

    if-nez v4, :cond_0

    .line 5451
    sget-object v4, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_ABORT_ALERT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    .line 5453
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackage()I

    move-result v4

    if-nez v4, :cond_0

    .line 5458
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSendPackage()I

    move-result v1

    move v3, v1

    .line 5459
    goto :goto_0

    .line 5443
    :cond_2
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentLibMakeSessionID()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    goto :goto_1
.end method

.method public xdmAgentStartGeneralAlert()I
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 5372
    const/4 v2, 0x0

    .line 5374
    .local v2, "ret":I
    const-string v4, ""

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5375
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentInit()I

    move-result v4

    if-eqz v4, :cond_1

    .line 5421
    :cond_0
    :goto_0
    return v3

    .line 5380
    :cond_1
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    iget v4, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->appId:I

    invoke-static {v4}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbGetNotiEvent(I)I

    move-result v1

    .line 5381
    .local v1, "nNotiEvent":I
    if-lez v1, :cond_2

    .line 5383
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiSessionID()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    .line 5390
    :goto_1
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeNode()I

    move-result v4

    if-nez v4, :cond_0

    .line 5395
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT_REPORT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    .line 5397
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackage()I

    move-result v4

    if-nez v4, :cond_0

    .line 5405
    :try_start_0
    iget-object v3, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 5414
    :goto_2
    if-eqz v2, :cond_3

    .line 5416
    const/4 v3, -0x7

    goto :goto_0

    .line 5387
    :cond_2
    sget-object v4, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentLibMakeSessionID()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    goto :goto_1

    .line 5407
    :catch_0
    move-exception v0

    .line 5409
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5410
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 5411
    const/4 v2, -0x7

    goto :goto_2

    .line 5419
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_3
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetIsConnected(Z)V

    .line 5420
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSendPackage()I

    move-result v2

    move v3, v2

    .line 5421
    goto :goto_0
.end method

.method public xdmAgentStartMgmtSession()I
    .locals 9

    .prologue
    const/4 v4, -0x7

    const/4 v8, 0x1

    const/4 v3, -0x1

    const/4 v7, 0x0

    .line 5215
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 5216
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v1, 0x0

    .line 5218
    .local v1, "res":I
    if-nez v2, :cond_0

    .line 5220
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Parsing package failed Abort session "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5365
    :goto_0
    return v3

    .line 5224
    :cond_0
    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->procState:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    sget-object v6, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    if-ne v5, v6, :cond_1

    .line 5226
    iput v7, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    .line 5227
    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentParsingWbxml([B)I

    move-result v1

    .line 5229
    if-eqz v1, :cond_1

    .line 5231
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Parsing package failed Abort session"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 5236
    :cond_1
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentHandleCmd()I

    move-result v1

    .line 5238
    sget-object v5, Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;->UIC_TRUE:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    iput-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->uicFlag:Lcom/fmm/dm/interfaces/XUICInterface$XUICFlag;

    .line 5240
    sparse-switch v1, :sswitch_data_0

    .line 5254
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Handling Commands failed Abort session "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 5243
    :sswitch_0
    const-string v3, "Handling Paused  Processing UIC Command"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move v3, v1

    .line 5244
    goto :goto_0

    .line 5247
    :sswitch_1
    const-string v3, "XDM_RET_ALERT_SESSION_ABORT"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move v3, v1

    .line 5248
    goto :goto_0

    .line 5258
    :sswitch_2
    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v6, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_FINISH:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v5, v6, :cond_2

    .line 5260
    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->msgID:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->msgID:I

    .line 5263
    :cond_2
    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    if-ne v5, v8, :cond_3

    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    if-eq v5, v8, :cond_9

    .line 5265
    :cond_3
    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authCount:I

    .line 5266
    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authCount:I

    const/4 v6, 0x3

    if-lt v5, v6, :cond_4

    .line 5268
    iput v7, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authCount:I

    .line 5269
    const/4 v3, -0x8

    iput v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    .line 5270
    const-string v3, "Authentification Failed Abort"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5271
    const/4 v3, -0x6

    goto/16 :goto_0

    .line 5274
    :cond_4
    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authState:I

    if-nez v5, :cond_6

    .line 5276
    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_PROCESSING:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    .line 5287
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackage()I

    move-result v5

    if-eqz v5, :cond_7

    .line 5289
    const-string v4, "failed"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5281
    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Authentification Retry...ws->dmState = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 5282
    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v6, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_CLIENT_INIT_MGMT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v5, v6, :cond_5

    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v6, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v5, v6, :cond_5

    iget-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v6, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT_REPORT:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v5, v6, :cond_5

    .line 5284
    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_PROCESSING:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    goto :goto_1

    .line 5294
    :cond_7
    sget-object v3, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v3, v3, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpConnection:I

    if-ne v3, v8, :cond_8

    .line 5299
    :try_start_0
    iget-object v3, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5308
    :goto_2
    if-eqz v1, :cond_8

    move v3, v4

    .line 5310
    goto/16 :goto_0

    .line 5301
    :catch_0
    move-exception v0

    .line 5303
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5304
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 5305
    const/4 v1, -0x2

    goto :goto_2

    .line 5314
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_8
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSendPackage()I

    move-result v1

    move v3, v1

    .line 5315
    goto/16 :goto_0

    .line 5319
    :cond_9
    sget-object v5, Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_PROCESSING:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->dmState:Lcom/fmm/dm/interfaces/XDMInterface$XDMSyncMLState;

    .line 5320
    iput v7, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->authCount:I

    .line 5321
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "total action commands = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5323
    iget v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->numAction:I

    if-nez v5, :cond_a

    iget-boolean v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->isFinal:Z

    if-eqz v5, :cond_a

    .line 5325
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbClearUicResultKeepFlag()V

    .line 5329
    invoke-static {v7}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiReSyncMode(I)Ljava/lang/Boolean;

    .line 5332
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbClearUicResultKeepFlag()V

    .line 5333
    const/16 v3, 0x8

    goto/16 :goto_0

    .line 5336
    :cond_a
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCreatePackage()I

    move-result v1

    .line 5337
    if-gez v1, :cond_b

    .line 5339
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "xdmAgentCreatePackage failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5344
    :cond_b
    sget-object v3, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v3, v3, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpConnection:I

    if-ne v3, v8, :cond_c

    .line 5348
    :try_start_1
    iget-object v3, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 5357
    :goto_3
    if-eqz v1, :cond_c

    .line 5359
    const-string v3, "XDM_RET_CONNECT_FAIL"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v4

    .line 5360
    goto/16 :goto_0

    .line 5350
    :catch_1
    move-exception v0

    .line 5352
    .restart local v0    # "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5353
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 5354
    const/4 v1, -0x2

    goto :goto_3

    .line 5364
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_c
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSendPackage()I

    move-result v1

    move v3, v1

    .line 5365
    goto/16 :goto_0

    .line 5240
    nop

    :sswitch_data_0
    .sparse-switch
        -0x5 -> :sswitch_0
        0x0 -> :sswitch_2
        0x3 -> :sswitch_1
    .end sparse-switch
.end method

.method public xdmAgentStartSession()I
    .locals 4

    .prologue
    .line 569
    const/4 v1, 0x0

    .line 570
    .local v1, "ret":I
    const/4 v0, 0x0

    .line 573
    .local v0, "nNotiEvent":I
    const-string v3, ""

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 575
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentInit()I

    move-result v3

    if-eqz v3, :cond_0

    .line 577
    const/4 v3, -0x1

    .line 595
    :goto_0
    return v3

    .line 580
    :cond_0
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 581
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    iget v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->appId:I

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbGetNotiEvent(I)I

    move-result v0

    .line 582
    if-lez v0, :cond_2

    sget-boolean v3, Lcom/fmm/dm/agent/XDMAgent;->m_bPendingStatus:Z

    if-nez v3, :cond_2

    .line 584
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiSessionID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    .line 590
    :goto_1
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeNode()I

    move-result v3

    if-eqz v3, :cond_1

    .line 592
    const/4 v1, -0x1

    :cond_1
    move v3, v1

    .line 595
    goto :goto_0

    .line 588
    :cond_2
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentLibMakeSessionID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    goto :goto_1
.end method

.method public xdmAgentTpCheckRetry()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 8978
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConntectRetryCount "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/fmm/dm/agent/XDMAgent;->m_nConnectRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 8980
    sget v1, Lcom/fmm/dm/agent/XDMAgent;->m_nConnectRetryCount:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 8982
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 8983
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 8984
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 8985
    sput v0, Lcom/fmm/dm/agent/XDMAgent;->m_nConnectRetryCount:I

    .line 8989
    :goto_0
    return v0

    .line 8988
    :cond_0
    sget v0, Lcom/fmm/dm/agent/XDMAgent;->m_nConnectRetryCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/fmm/dm/agent/XDMAgent;->m_nConnectRetryCount:I

    .line 8989
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public xdmAgentTpClose()V
    .locals 1

    .prologue
    .line 8958
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    invoke-virtual {v0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpClose()V

    .line 8959
    return-void
.end method

.method public xdmAgentTpCloseNetwork()V
    .locals 1

    .prologue
    .line 8968
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    invoke-virtual {v0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpCloseNetWork()V

    .line 8969
    return-void
.end method

.method public xdmAgentTpGetHttpEcode()I
    .locals 1

    .prologue
    .line 8973
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    invoke-virtual {v0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetHttpEcode()I

    move-result v0

    return v0
.end method

.method public xdmAgentTpInit(I)I
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 8963
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    invoke-virtual {v0, p1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpInit(I)I

    move-result v0

    return v0
.end method

.method public xdmAgentVefifyAtomicCmd(Lcom/fmm/dm/agent/XDMAgent;)Z
    .locals 4
    .param p1, "cmd"    # Lcom/fmm/dm/agent/XDMAgent;

    .prologue
    const/4 v1, 0x0

    .line 3456
    const/4 v0, 0x1

    .line 3458
    .local v0, "res":Z
    const-string v2, "Atomic_Start"

    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3469
    .end local v0    # "res":Z
    :goto_0
    return v0

    .line 3462
    .restart local v0    # "res":Z
    :cond_0
    const-string v2, "GET"

    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 3464
    goto :goto_0

    .line 3468
    :cond_1
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xdmAgentVerifyCmd(Lcom/fmm/dm/agent/XDMAgent;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I
    .locals 7
    .param p1, "cmd"    # Lcom/fmm/dm/agent/XDMAgent;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/fmm/dm/eng/parser/XDMParserStatus;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 5594
    sget-object v2, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 5595
    .local v2, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v1, 0x0

    .line 5597
    .local v1, "res":I
    const-string v3, "SyncHdr"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 5599
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_Header:Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    invoke-virtual {p0, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdSyncHeader(Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)I

    move-result v1

    :goto_0
    move v3, v1

    .line 5677
    :goto_1
    return v3

    .line 5601
    :cond_0
    const-string v3, "Status"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 5603
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_Status:Lcom/fmm/dm/eng/parser/XDMParserStatus;

    invoke-virtual {p0, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdStatus(Lcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto :goto_0

    .line 5605
    :cond_1
    const-string v3, "Get"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    .line 5607
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_Get:Lcom/fmm/dm/eng/parser/XDMParserGet;

    invoke-virtual {p0, v3, p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdGet(Lcom/fmm/dm/eng/parser/XDMParserGet;Z)I

    move-result v1

    goto :goto_0

    .line 5609
    :cond_2
    const-string v3, "Exec"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    .line 5611
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_Exec:Lcom/fmm/dm/eng/parser/XDMParserExec;

    invoke-virtual {p0, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdExec(Lcom/fmm/dm/eng/parser/XDMParserExec;)I

    move-result v1

    goto :goto_0

    .line 5613
    :cond_3
    const-string v3, "Alert"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 5615
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_Alert:Lcom/fmm/dm/eng/parser/XDMParserAlert;

    invoke-virtual {p0, v3, p2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdAlert(Lcom/fmm/dm/eng/parser/XDMParserAlert;Z)I

    move-result v1

    goto :goto_0

    .line 5617
    :cond_4
    const-string v3, "Add"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_5

    .line 5619
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_AddCmd:Lcom/fmm/dm/eng/parser/XDMParserAdd;

    invoke-virtual {p0, v3, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdAdd(Lcom/fmm/dm/eng/parser/XDMParserAdd;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto :goto_0

    .line 5621
    :cond_5
    const-string v3, "Replace"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_6

    .line 5623
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_ReplaceCmd:Lcom/fmm/dm/eng/parser/XDMParserReplace;

    invoke-virtual {p0, v3, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdReplace(Lcom/fmm/dm/eng/parser/XDMParserReplace;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto :goto_0

    .line 5625
    :cond_6
    const-string v3, "Copy"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_7

    .line 5627
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_CopyCmd:Lcom/fmm/dm/eng/parser/XDMParserCopy;

    invoke-virtual {p0, v3, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdCopy(Lcom/fmm/dm/eng/parser/XDMParserCopy;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto :goto_0

    .line 5629
    :cond_7
    const-string v3, "Delete"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_8

    .line 5631
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_DeleteCmd:Lcom/fmm/dm/eng/parser/XDMParserDelete;

    invoke-virtual {p0, v3, p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdDelete(Lcom/fmm/dm/eng/parser/XDMParserDelete;ZLcom/fmm/dm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto/16 :goto_0

    .line 5633
    :cond_8
    const-string v3, "Atomic_Start"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_a

    .line 5635
    iput-boolean v6, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 5636
    iput-boolean v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 5640
    :try_start_0
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    iget-object v3, v3, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsSaveFs(Lcom/fmm/dm/eng/core/XDMOmVfs;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5646
    :goto_2
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_Atomic:Lcom/fmm/dm/eng/parser/XDMParserAtomic;

    invoke-virtual {p0, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdAtomic(Lcom/fmm/dm/eng/parser/XDMParserAtomic;)I

    move-result v1

    .line 5647
    iget-boolean v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->atomicFlag:Z

    if-eqz v3, :cond_9

    .line 5649
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 5650
    new-instance v3, Lcom/fmm/dm/eng/core/XDMOmTree;

    invoke-direct {v3}, Lcom/fmm/dm/eng/core/XDMOmTree;-><init>()V

    iput-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    .line 5651
    iget-object v3, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->om:Lcom/fmm/dm/eng/core/XDMOmTree;

    iget-object v3, v3, Lcom/fmm/dm/eng/core/XDMOmTree;->vfs:Lcom/fmm/dm/eng/core/XDMOmVfs;

    invoke-static {v3}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmVfsInit(Lcom/fmm/dm/eng/core/XDMOmVfs;)I

    .line 5654
    :cond_9
    iput-boolean v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    goto/16 :goto_0

    .line 5642
    :catch_0
    move-exception v0

    .line 5644
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 5656
    .end local v0    # "e":Ljava/io/IOException;
    :cond_a
    const-string v3, "Sequence_Start"

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_c

    .line 5658
    iput-boolean v6, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    .line 5659
    iget-object v3, p1, Lcom/fmm/dm/agent/XDMAgent;->m_Sequence:Lcom/fmm/dm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentCmdSequence(Lcom/fmm/dm/eng/parser/XDMParserSequence;)I

    move-result v1

    .line 5661
    const/4 v3, -0x5

    if-ne v1, v3, :cond_b

    move v3, v1

    .line 5663
    goto/16 :goto_1

    .line 5667
    :cond_b
    iput-boolean v5, v2, Lcom/fmm/dm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    goto/16 :goto_0

    .line 5673
    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown Command"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/fmm/dm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5674
    const/16 v3, -0x9

    goto/16 :goto_1
.end method

.method public xdmAgentVerifyServerAuth(Lcom/fmm/dm/eng/parser/XDMParserSyncheader;)I
    .locals 13
    .param p1, "syncHeader"    # Lcom/fmm/dm/eng/parser/XDMParserSyncheader;

    .prologue
    const/4 v5, 0x0

    const/4 v12, -0x1

    const/4 v6, 0x0

    const/16 v0, -0x9

    .line 292
    sget-object v11, Lcom/fmm/dm/agent/XDMAgent;->g_DmWs:Lcom/fmm/dm/eng/core/XDMWorkspace;

    .line 293
    .local v11, "ws":Lcom/fmm/dm/eng/core/XDMWorkspace;
    const/4 v10, 0x0

    .line 294
    .local v10, "szKey":Ljava/lang/String;
    iget-object v7, p1, Lcom/fmm/dm/eng/parser/XDMParserSyncheader;->cred:Lcom/fmm/dm/eng/parser/XDMParserCred;

    .line 295
    .local v7, "cred":Lcom/fmm/dm/eng/parser/XDMParserCred;
    const/4 v9, 0x1

    .line 296
    .local v9, "ret":I
    iget-object v8, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->recvHmacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    .line 298
    .local v8, "pHMAC":Lcom/fmm/dm/eng/core/XDMHmacData;
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 300
    iget-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 302
    const-string v1, "ServerID is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 303
    iget v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v2, -0x7

    if-eq v1, v2, :cond_0

    iget v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverAuthState:I

    if-ne v1, v0, :cond_1

    .line 304
    :cond_0
    const/4 v9, -0x1

    :goto_0
    move v0, v9

    .line 421
    :goto_1
    return v0

    .line 306
    :cond_1
    const/16 v9, -0x9

    goto :goto_0

    .line 311
    :cond_2
    iget v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_a

    .line 313
    if-nez v8, :cond_3

    .line 315
    const-string v1, "HAMC is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 319
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "algorighm : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 321
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "digest : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 323
    iget-object v1, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacUserName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 325
    :cond_4
    const-string v1, "Any of MAC data is empty"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 328
    :cond_5
    const-string v1, "MD5"

    iget-object v2, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_6

    .line 330
    const-string v1, "State No Credential"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 335
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "credtype:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nextNonce:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    iget-object v2, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "httpContentLength:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->httpContentLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 338
    iget v0, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    iget-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    iget-object v2, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    iget-object v3, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    iget-object v4, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    array-length v4, v4

    iget-object v5, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget v6, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->httpContentLength:I

    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v10

    .line 339
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 341
    const-string v0, "key is null"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move v0, v12

    .line 342
    goto/16 :goto_1

    .line 345
    :cond_7
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    .line 347
    const-string v0, "key and pHMAC.hamcDigest not equal"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move v0, v12

    .line 348
    goto/16 :goto_1

    .line 350
    :cond_8
    const/4 v9, 0x1

    :cond_9
    :goto_2
    move v0, v9

    .line 421
    goto/16 :goto_1

    .line 354
    :cond_a
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    if-nez v0, :cond_b

    .line 356
    const/16 v9, -0x9

    goto :goto_2

    .line 360
    :cond_b
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v0, v0, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthCredString2Type(Ljava/lang/String;)I

    move-result v0

    iget v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    if-ne v0, v1, :cond_11

    .line 362
    const-string v0, "syncml:auth-md5"

    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_e

    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CRED_TYPE_MD5 ws.serverCredType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CRED_TYPE_MD5 ws.serverCredType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 369
    iget v0, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    iget-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    iget-object v2, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    iget-object v3, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    iget-object v4, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverNextNonce:[B

    array-length v4, v4

    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v10

    .line 370
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 372
    const-string v0, "key is null"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 373
    const/4 v9, -0x1

    goto :goto_2

    .line 377
    :cond_c
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_d

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key.compareTo(cred.data) != 0 key= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cred.data= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 380
    const-string v0, "key and cred.data not equal"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 381
    const/4 v9, -0x1

    goto/16 :goto_2

    .line 385
    :cond_d
    const/4 v9, 0x1

    goto/16 :goto_2

    .line 389
    :cond_e
    const-string v0, "syncml:auth-basic"

    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->meta:Lcom/fmm/dm/eng/parser/XDMParserMeta;

    iget-object v1, v1, Lcom/fmm/dm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 391
    iget v0, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->serverCredType:I

    iget-object v1, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    iget-object v2, v11, Lcom/fmm/dm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    move v4, v6

    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v10

    .line 393
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 395
    const-string v0, "key is null"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 396
    const/4 v9, -0x1

    goto/16 :goto_2

    .line 400
    :cond_f
    iget-object v0, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_10

    .line 402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key.compareTo(cred.data) != 0 key= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cred.data= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/fmm/dm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 403
    const-string v0, "key and cred.data not equal"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 404
    const/4 v9, -0x1

    goto/16 :goto_2

    .line 408
    :cond_10
    const/4 v9, 0x1

    goto/16 :goto_2

    .line 415
    :cond_11
    const-string v0, "server auth type is mismatch"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 416
    const/4 v9, -0x1

    goto/16 :goto_2
.end method

.method public xdmAgent_MAKE_REP_ITEM(Lcom/fmm/dm/eng/core/XDMOmTree;Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/String;I)V
    .locals 3
    .param p1, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p2, "list"    # Lcom/fmm/dm/eng/core/XDMLinkedList;
    .param p3, "szPath"    # Ljava/lang/String;
    .param p4, "node_size"    # I

    .prologue
    .line 2293
    new-array v0, p4, [C

    .line 2295
    .local v0, "buf":[C
    const/4 v2, 0x0

    invoke-static {p1, p3, v2, v0, p4}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmRead(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    move-result v2

    if-gez v2, :cond_0

    .line 2297
    const-string v2, "xdmOmRead failed"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2299
    :cond_0
    new-instance v1, Lcom/fmm/dm/eng/parser/XDMParserItem;

    invoke-direct {v1}, Lcom/fmm/dm/eng/parser/XDMParserItem;-><init>()V

    .line 2300
    .local v1, "item":Lcom/fmm/dm/eng/parser/XDMParserItem;
    iput-object p3, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 2301
    invoke-static {v0}, Lcom/fmm/dm/agent/XDMHandleCmd;->xdmAgentDataStString2Pcdata([C)Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/eng/parser/XDMParserItem;->data:Lcom/fmm/dm/eng/parser/XDMParserPcdata;

    .line 2302
    invoke-static {p2, v1}, Lcom/fmm/dm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/fmm/dm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2303
    return-void
.end method

.method public xdm_SET_OM_STR(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .param p1, "om"    # Lcom/fmm/dm/eng/core/XDMOmTree;
    .param p2, "szPath"    # Ljava/lang/String;
    .param p3, "szData"    # Ljava/lang/String;
    .param p4, "aclValue"    # I
    .param p5, "scope"    # I

    .prologue
    .line 1781
    invoke-static {p1, p2}, Lcom/fmm/dm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/fmm/dm/eng/core/XDMVnode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1783
    invoke-static {p2, p3}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1784
    invoke-static {p1, p2, p4, p5}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/fmm/dm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1786
    :cond_0
    return-void
.end method
