.class public Lcom/fmm/dm/agent/amt/XAMTDelayTimer;
.super Ljava/lang/Object;
.source "XAMTDelayTimer.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XTPInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/agent/amt/XAMTDelayTimer$XAMTDelayNetProviderTimerTask;
    }
.end annotation


# instance fields
.field private mDelayTimer:Ljava/util/Timer;


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;->mDelayTimer:Ljava/util/Timer;

    .line 17
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;->mDelayTimer:Ljava/util/Timer;

    .line 18
    new-instance v0, Lcom/fmm/dm/agent/amt/XAMTDelayTimer$XAMTDelayNetProviderTimerTask;

    invoke-direct {v0, p0}, Lcom/fmm/dm/agent/amt/XAMTDelayTimer$XAMTDelayNetProviderTimerTask;-><init>(Lcom/fmm/dm/agent/amt/XAMTDelayTimer;)V

    .line 19
    .local v0, "mTask":Lcom/fmm/dm/agent/amt/XAMTDelayTimer$XAMTDelayNetProviderTimerTask;
    iget-object v1, p0, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;->mDelayTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 20
    return-void
.end method

.method static synthetic access$002(Lcom/fmm/dm/agent/amt/XAMTDelayTimer;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/fmm/dm/agent/amt/XAMTDelayTimer;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;->mDelayTimer:Ljava/util/Timer;

    return-object p1
.end method


# virtual methods
.method public endDelayTimer()V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;->mDelayTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;->mDelayTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;->mDelayTimer:Ljava/util/Timer;

    .line 29
    :cond_0
    return-void
.end method
