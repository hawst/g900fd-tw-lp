.class public Lcom/fmm/dm/agent/XDMAppProtoNetInfo;
.super Ljava/lang/Object;
.source "XDMAppProtoNetInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMDNSAddr;,
        Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;
    }
.end annotation


# static fields
.field private static final APN_CONTENT_URI:Landroid/net/Uri;

.field private static final APN_PROJECTION:[Ljava/lang/String;

.field private static final PREFERAPN_URI:Landroid/net/Uri;

.field private static final PREFERAPN_URI2:Landroid/net/Uri;

.field public static final PROTO_ADDRESS_APN:I = 0x0

.field public static final PROTO_ADDRESS_E164:I = 0x1

.field public static final PROTO_ADDRESS_IPV4:I = 0x2

.field private static context:Landroid/content/Context;

.field public static existApn:Z

.field private static final searchProjection:[Ljava/lang/String;

.field private static selectedAPNID:I


# instance fields
.field public apntype:Ljava/lang/String;

.field public auth:Ljava/lang/String;

.field public authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

.field public dnsAddr:[Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMDNSAddr;

.field public inactiveDataTime:I

.field public m_nMmspxport:I

.field public m_szHomeURL:Ljava/lang/String;

.field public m_szMmsc:Ljava/lang/String;

.field public m_szMmsname:Ljava/lang/String;

.field public m_szMmsproxy:Ljava/lang/String;

.field public m_szProxyAddr:Ljava/lang/String;

.field public napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

.field public napAddrType:I

.field public nbaType:I

.field public opMode:I

.field public protoAppType:I

.field public protoType:I

.field public szAccountName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "apn"

    aput-object v1, v0, v5

    const-string v1, "proxy"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "port"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "user"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "server"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "password"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mmsc"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "mnc"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "numeric"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "mmsproxy"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "mmsport"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "authtype"

    aput-object v2, v0, v1

    sput-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    .line 37
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "apn"

    aput-object v1, v0, v5

    sput-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->searchProjection:[Ljava/lang/String;

    .line 40
    const-string v0, "content://telephony/carriers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_CONTENT_URI:Landroid/net/Uri;

    .line 41
    const-string v0, "content://telephony/carriers/preferapn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI:Landroid/net/Uri;

    .line 42
    const-string v0, "content://telephony/carriers/preferapn2"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI2:Landroid/net/Uri;

    .line 50
    sput v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    .line 75
    sput-boolean v3, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->existApn:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "pcontext"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    sget-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 102
    sput-object p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    .line 103
    :cond_0
    return-void
.end method

.method public static xdmAgentAppGetExistApn()Z
    .locals 1

    .prologue
    .line 107
    sget-boolean v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->existApn:Z

    return v0
.end method

.method private xdmAgentAppSaveNetInfo(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)V
    .locals 10
    .param p1, "netInfo"    # Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v9, 0x1

    .line 421
    sget v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 422
    .local v3, "szParamIndex":Ljava/lang/String;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 424
    .local v4, "values":Landroid/content/ContentValues;
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->szAccountName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 425
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v9

    iget-object v6, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->szAccountName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    :cond_0
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    if-eqz v5, :cond_7

    .line 429
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget-object v5, v5, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szApn:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 430
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v5, v5, v6

    iget-object v6, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget-object v6, v6, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szApn:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :cond_1
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget-object v5, v5, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 434
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget-object v5, v5, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    const-string v6, "0.0.0.0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 436
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v7

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :goto_0
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget v5, v5, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    if-nez v5, :cond_6

    .line 444
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v8

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_2
    :goto_1
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    if-eqz v5, :cond_8

    .line 459
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    iget-object v5, v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 460
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x5

    aget-object v5, v5, v6

    iget-object v6, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    iget-object v6, v6, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_3
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    iget-object v5, v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 462
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v5, v5, v6

    iget-object v6, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    iget-object v6, v6, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_4
    :goto_2
    iget-object v5, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->apntype:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 470
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/16 v6, 0xe

    aget-object v5, v5, v6

    iget-object v6, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->apntype:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :goto_3
    invoke-virtual {v4}, Landroid/content/ContentValues;->hashCode()I

    move-result v5

    if-nez v5, :cond_a

    .line 478
    sput v9, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    .line 515
    :goto_4
    return-void

    .line 440
    :cond_5
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v7

    iget-object v6, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget-object v6, v6, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 448
    :cond_6
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v8

    iget-object v6, p1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget v6, v6, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 454
    :cond_7
    const-string v5, "netInfo.napAddr is Null"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 466
    :cond_8
    const-string v5, "netInfo.authInfo is Null"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 473
    :cond_9
    const-string v5, "netInfo.apntype is Null"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 484
    :cond_a
    :try_start_0
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 485
    .local v1, "netInfoUri":Landroid/net/Uri;
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v1, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 486
    .local v2, "ret":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update completed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " accounts"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 493
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v5

    if-le v5, v9, :cond_c

    .line 496
    const-string v5, "persist.sys.dataprefer.simid"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_b

    .line 497
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 506
    :goto_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update completed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " accounts"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 514
    .end local v1    # "netInfoUri":Landroid/net/Uri;
    .end local v2    # "ret":I
    :goto_6
    sput v9, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    goto/16 :goto_4

    .line 499
    .restart local v1    # "netInfoUri":Landroid/net/Uri;
    .restart local v2    # "ret":I
    :cond_b
    :try_start_1
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI2:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    goto :goto_5

    .line 503
    :cond_c
    sget-object v5, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    goto :goto_5

    .line 508
    .end local v1    # "netInfoUri":Landroid/net/Uri;
    .end local v2    # "ret":I
    :catch_0
    move-exception v0

    .line 510
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "Can\'t update DB"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 511
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_6
.end method

.method public static xdmAgentAppSetExistApn(Z)V
    .locals 0
    .param p0, "bexist"    # Z

    .prologue
    .line 112
    sput-boolean p0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->existApn:Z

    .line 113
    return-void
.end method


# virtual methods
.method public xdmAgentAppGetBootStrapNetInfo()V
    .locals 11

    .prologue
    .line 121
    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 123
    const/4 v7, 0x0

    .line 124
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 125
    .local v8, "cursor1":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 129
    .local v6, "_cNetInfo":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_6

    .line 132
    const-string v0, "persist.sys.dataprefer.simid"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_5

    .line 133
    sget-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI:Landroid/net/Uri;

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->searchProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 142
    :goto_0
    if-eqz v7, :cond_c

    .line 144
    const/4 v10, 0x1

    .line 146
    .local v10, "pos":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_a

    .line 149
    const-string v0, "there is no enabled apn in PREFERAPN_URI!!!!!!!"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 150
    sget-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "current =\'1\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 151
    if-eqz v8, :cond_1

    .line 153
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 155
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 157
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 158
    sput v10, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "there is no enabled apn in PREFERAPN_URI selectedAPNID ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 166
    :cond_0
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    if-eqz v7, :cond_2

    .line 200
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 202
    :cond_2
    if-eqz v8, :cond_3

    .line 203
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 205
    :cond_3
    if-eqz v6, :cond_4

    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 209
    .end local v10    # "pos":I
    :cond_4
    :goto_2
    return-void

    .line 135
    :cond_5
    :try_start_1
    sget-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI2:Landroid/net/Uri;

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->searchProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_0

    .line 139
    :cond_6
    sget-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI:Landroid/net/Uri;

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->searchProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto/16 :goto_0

    .line 163
    .restart local v10    # "pos":I
    :cond_7
    const-string v0, "there is no enabled apn!!!!!!!"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 193
    .end local v10    # "pos":I
    :catch_0
    move-exception v9

    .line 195
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 199
    if-eqz v7, :cond_8

    .line 200
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 202
    :cond_8
    if-eqz v8, :cond_9

    .line 203
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 205
    :cond_9
    if-eqz v6, :cond_4

    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 173
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v10    # "pos":I
    :cond_a
    :try_start_3
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 176
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 178
    sget-object v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id =\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 180
    if-eqz v6, :cond_c

    .line 182
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_b

    .line 184
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 186
    sput v10, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    .line 189
    :cond_b
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 199
    .end local v10    # "pos":I
    :cond_c
    if-eqz v7, :cond_d

    .line 200
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 202
    :cond_d
    if-eqz v8, :cond_e

    .line 203
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 205
    :cond_e
    if-eqz v6, :cond_4

    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 199
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_f

    .line 200
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 202
    :cond_f
    if-eqz v8, :cond_10

    .line 203
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 205
    :cond_10
    if-eqz v6, :cond_11

    .line 206
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_11
    throw v0
.end method

.method public xdmAgentAppGetNetInfo(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)V
    .locals 16
    .param p1, "netInfo"    # Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    .prologue
    .line 213
    const/4 v7, 0x0

    .line 216
    .local v7, "_cNetInfo":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 217
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 221
    .local v9, "cursor1":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_13

    .line 224
    const-string v1, "persist.sys.dataprefer.simid"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_12

    .line 225
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI:Landroid/net/Uri;

    sget-object v3, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->searchProjection:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 234
    :goto_0
    if-eqz v8, :cond_18

    .line 236
    const/4 v11, 0x1

    .line 237
    .local v11, "pos":I
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppSetExistApn(Z)V

    .line 239
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_17

    .line 242
    const-string v1, "there is no enabled apn in PREFERAPN_URI!!!!!!!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 243
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const-string v4, "current =\'1\'"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 244
    if-eqz v9, :cond_6

    .line 246
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_14

    .line 248
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 250
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 251
    sput v11, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    .line 252
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "there is no enabled apn in PREFERAPN_URI selectedAPNID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 253
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->szAccountName:Ljava/lang/String;

    .line 256
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 257
    .local v12, "szTmpAPN":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 259
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    if-nez v1, :cond_0

    .line 261
    new-instance v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMAppConnectSetting;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    .line 263
    :cond_0
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iput-object v12, v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szApn:Ljava/lang/String;

    .line 267
    :cond_1
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 268
    .local v13, "szTmpProxyAddr":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 270
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    if-nez v1, :cond_2

    .line 272
    new-instance v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMAppConnectSetting;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    .line 275
    :cond_2
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-static {v13}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpCheckValidIPAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    .line 279
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 280
    .local v15, "tmpProxyPort":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    const/4 v2, 0x0

    iput v2, v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    .line 281
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 283
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "proxy port is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget v2, v2, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 289
    .end local v15    # "tmpProxyPort":Ljava/lang/String;
    :cond_3
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 290
    .local v14, "szTmpUsername":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 292
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    if-nez v1, :cond_4

    .line 294
    new-instance v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    .line 296
    :cond_4
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    iput-object v14, v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szId:Ljava/lang/String;

    .line 299
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    .line 302
    :cond_5
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->apntype:Ljava/lang/String;

    .line 315
    .end local v12    # "szTmpAPN":Ljava/lang/String;
    .end local v13    # "szTmpProxyAddr":Ljava/lang/String;
    .end local v14    # "szTmpUsername":Ljava/lang/String;
    :cond_6
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 317
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id =\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 319
    if-eqz v7, :cond_e

    .line 321
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_e

    .line 323
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 325
    sput v11, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->selectedAPNID:I

    .line 326
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->szAccountName:Ljava/lang/String;

    .line 329
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 330
    .restart local v12    # "szTmpAPN":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 332
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    if-nez v1, :cond_7

    .line 334
    new-instance v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMAppConnectSetting;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    .line 336
    :cond_7
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iput-object v12, v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szApn:Ljava/lang/String;

    .line 340
    :cond_8
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 341
    .restart local v13    # "szTmpProxyAddr":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 343
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    if-nez v1, :cond_9

    .line 345
    new-instance v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMAppConnectSetting;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    .line 349
    :cond_9
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-static {v13}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpCheckValidIPAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    .line 353
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 354
    .restart local v15    # "tmpProxyPort":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    const/4 v2, 0x0

    iput v2, v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    .line 355
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 357
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    .line 358
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "proxy port is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    iget v2, v2, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 363
    .end local v15    # "tmpProxyPort":Ljava/lang/String;
    :cond_a
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 364
    .restart local v14    # "szTmpUsername":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 366
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    if-nez v1, :cond_b

    .line 368
    new-instance v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    .line 370
    :cond_b
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    iput-object v14, v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szId:Ljava/lang/String;

    .line 373
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    .line 376
    :cond_c
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->APN_PROJECTION:[Ljava/lang/String;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->apntype:Ljava/lang/String;

    .line 378
    .end local v12    # "szTmpAPN":Ljava/lang/String;
    .end local v13    # "szTmpProxyAddr":Ljava/lang/String;
    .end local v14    # "szTmpUsername":Ljava/lang/String;
    :cond_d
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    .end local v11    # "pos":I
    :cond_e
    :goto_2
    if-eqz v7, :cond_f

    .line 393
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_f
    if-eqz v8, :cond_10

    .line 396
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 398
    :cond_10
    if-eqz v9, :cond_11

    .line 399
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 401
    :cond_11
    :goto_3
    return-void

    .line 227
    :cond_12
    :try_start_1
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI2:Landroid/net/Uri;

    sget-object v3, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->searchProjection:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_0

    .line 231
    :cond_13
    sget-object v1, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->PREFERAPN_URI:Landroid/net/Uri;

    sget-object v3, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->searchProjection:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_0

    .line 307
    .restart local v11    # "pos":I
    :cond_14
    const-string v1, "there is no enabled apn!!!!!!!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 385
    .end local v11    # "pos":I
    :catch_0
    move-exception v10

    .line 387
    .local v10, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    :try_start_2
    invoke-static {v1}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppSetExistApn(Z)V

    .line 388
    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 392
    if-eqz v7, :cond_15

    .line 393
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_15
    if-eqz v8, :cond_16

    .line 396
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 398
    :cond_16
    if-eqz v9, :cond_11

    .line 399
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 313
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v11    # "pos":I
    :cond_17
    :try_start_3
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    goto/16 :goto_1

    .line 383
    .end local v11    # "pos":I
    :cond_18
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppSetExistApn(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 392
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_19

    .line 393
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_19
    if-eqz v8, :cond_1a

    .line 396
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 398
    :cond_1a
    if-eqz v9, :cond_1b

    .line 399
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_1b
    throw v1
.end method

.method public xdmAgentAppProtoSetAccount(ILcom/fmm/dm/agent/XDMAppProtoNetInfo;)Z
    .locals 1
    .param p1, "protoAccount"    # I
    .param p2, "netInfo"    # Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    .prologue
    .line 411
    invoke-direct {p0, p2}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppSaveNetInfo(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)V

    .line 412
    const/4 v0, 0x1

    return v0
.end method
