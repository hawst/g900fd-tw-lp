.class public Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;
.super Ljava/lang/Object;
.source "XAMTGpsManagerService.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;


# static fields
.field static XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

.field static XAMTNetLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

.field static getGPSLoc:Z

.field static getNetLoc:Z

.field static mLocation:Landroid/location/Location;

.field static mXAMTDelayTimer:Lcom/fmm/dm/agent/amt/XAMTDelayTimer;

.field static preGPSEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 16
    sput-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->mLocation:Landroid/location/Location;

    .line 17
    sput-boolean v0, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->preGPSEnabled:Z

    .line 19
    sput-boolean v0, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getNetLoc:Z

    .line 20
    sput-boolean v0, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getGPSLoc:Z

    .line 22
    sput-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->mXAMTDelayTimer:Lcom/fmm/dm/agent/amt/XAMTDelayTimer;

    .line 200
    new-instance v0, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService$1;

    invoke-direct {v0}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService$1;-><init>()V

    sput-object v0, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    .line 239
    new-instance v0, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService$2;

    invoke-direct {v0}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService$2;-><init>()V

    sput-object v0, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTNetLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xamtGetLocationManager()Landroid/location/LocationManager;
    .locals 2

    .prologue
    .line 187
    const-string v1, "location"

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 188
    .local v0, "lm":Landroid/location/LocationManager;
    if-nez v0, :cond_0

    .line 190
    const-string v1, "location"

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "lm":Landroid/location/LocationManager;
    check-cast v0, Landroid/location/LocationManager;

    .line 191
    .restart local v0    # "lm":Landroid/location/LocationManager;
    if-nez v0, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 197
    .end local v0    # "lm":Landroid/location/LocationManager;
    :cond_0
    return-object v0
.end method

.method public static xamtGpsStartTracking(I)V
    .locals 8
    .param p0, "nStartMode"    # I

    .prologue
    const-wide/16 v6, 0x3a98

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 89
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtGetLocationManager()Landroid/location/LocationManager;

    move-result-object v0

    .line 90
    .local v0, "lm":Landroid/location/LocationManager;
    if-nez v0, :cond_0

    .line 92
    const-string v1, "LocationManager is null!!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 120
    :goto_0
    return-void

    .line 96
    :cond_0
    if-ne p0, v3, :cond_1

    .line 98
    sput-boolean v3, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getGPSLoc:Z

    .line 99
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v1, v6, v7}, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->startTimer(J)V

    .line 100
    const-string v1, "gps"

    sget-object v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v0, v1, v2, v4}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 101
    const-string v1, "GPS listener is starting"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_1
    const/4 v1, 0x2

    if-ne p0, v1, :cond_2

    .line 105
    sput-boolean v3, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getNetLoc:Z

    .line 106
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTNetLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v2, v3}, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->startTimer(J)V

    .line 107
    const-string v1, "network"

    sget-object v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTNetLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v0, v1, v2, v4}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 108
    const-string v1, "Network GPS listener is starting"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_2
    sput-boolean v3, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getGPSLoc:Z

    .line 113
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v1, v6, v7}, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->startTimer(J)V

    .line 114
    const-string v1, "gps"

    sget-object v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v0, v1, v2, v4}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 115
    const-string v1, "GPS listener is starting"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 117
    sput-boolean v3, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getNetLoc:Z

    .line 118
    new-instance v1, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;

    invoke-direct {v1}, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;-><init>()V

    sput-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->mXAMTDelayTimer:Lcom/fmm/dm/agent/amt/XAMTDelayTimer;

    goto :goto_0
.end method

.method public static xamtRemoveListener()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 124
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtGetLocationManager()Landroid/location/LocationManager;

    move-result-object v0

    .line 125
    .local v0, "lm":Landroid/location/LocationManager;
    if-nez v0, :cond_1

    .line 127
    const-string v1, "LocationManager is null!!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    sget-boolean v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getGPSLoc:Z

    if-eqz v1, :cond_2

    .line 133
    sput-boolean v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getGPSLoc:Z

    .line 134
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v1}, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->endTimer()V

    .line 135
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTGpsLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 137
    :cond_2
    sget-boolean v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getNetLoc:Z

    if-eqz v1, :cond_4

    .line 139
    sput-boolean v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getNetLoc:Z

    .line 140
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->mXAMTDelayTimer:Lcom/fmm/dm/agent/amt/XAMTDelayTimer;

    if-eqz v1, :cond_3

    .line 142
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->mXAMTDelayTimer:Lcom/fmm/dm/agent/amt/XAMTDelayTimer;

    invoke-virtual {v1}, Lcom/fmm/dm/agent/amt/XAMTDelayTimer;->endDelayTimer()V

    .line 143
    const/4 v1, 0x0

    sput-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->mXAMTDelayTimer:Lcom/fmm/dm/agent/amt/XAMTDelayTimer;

    .line 145
    :cond_3
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTNetLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v1}, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->endTimer()V

    .line 146
    sget-object v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTNetLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 149
    :cond_4
    sget-boolean v1, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->preGPSEnabled:Z

    if-nez v1, :cond_0

    .line 151
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gps"

    sget-boolean v3, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->preGPSEnabled:Z

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 152
    const-string v1, "disable the GPS_PROVIDER"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xamtSetupGpsTracking(Landroid/content/Context;)V
    .locals 7
    .param p0, "pcontext"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    const/4 v1, 0x0

    .line 27
    .local v1, "nLocCount":I
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtGetLocationManager()Landroid/location/LocationManager;

    move-result-object v0

    .line 28
    .local v0, "lm":Landroid/location/LocationManager;
    if-nez v0, :cond_0

    .line 30
    const-string v2, "LocationManager is null!!"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 85
    :goto_0
    return-void

    .line 34
    :cond_0
    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 36
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "network"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 37
    const-string v2, "enable the network provider"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 40
    :cond_1
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 42
    sput-boolean v4, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->preGPSEnabled:Z

    .line 43
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "gps"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 44
    const-string v2, "enable the GPS_PROVIDER"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 51
    :goto_1
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->xamtGpsInitLocation()V

    .line 52
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetLocationCount()I

    move-result v1

    .line 54
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v2

    if-nez v2, :cond_4

    .line 56
    const-string v2, "not support gps"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 57
    if-nez v1, :cond_3

    .line 59
    invoke-static {v6}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtGpsStartTracking(I)V

    goto :goto_0

    .line 48
    :cond_2
    sput-boolean v5, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->preGPSEnabled:Z

    goto :goto_1

    .line 63
    :cond_3
    invoke-static {v4}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocation(Z)V

    goto :goto_0

    .line 68
    :cond_4
    if-nez v1, :cond_5

    .line 70
    invoke-static {v4}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtGpsStartTracking(I)V

    goto :goto_0

    .line 72
    :cond_5
    if-gt v1, v6, :cond_6

    .line 74
    invoke-static {v5}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtGpsStartTracking(I)V

    goto :goto_0

    .line 78
    :cond_6
    invoke-static {v4}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocation(Z)V

    goto :goto_0
.end method

.method public static xamtStartNetworkGPSListener()V
    .locals 6

    .prologue
    .line 158
    sget-boolean v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getNetLoc:Z

    if-nez v2, :cond_0

    .line 160
    const-string v2, "NetLoc is false, Not start"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 183
    .local v1, "lm":Landroid/location/LocationManager;
    :goto_0
    return-void

    .line 164
    .end local v1    # "lm":Landroid/location/LocationManager;
    :cond_0
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtGetLocationManager()Landroid/location/LocationManager;

    move-result-object v1

    .line 166
    .restart local v1    # "lm":Landroid/location/LocationManager;
    if-nez v1, :cond_1

    .line 168
    const-string v2, "LocationManager is null!!!, Not start"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 169
    const/4 v2, 0x0

    sput-boolean v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->getNetLoc:Z

    goto :goto_0

    .line 175
    :cond_1
    :try_start_0
    sget-object v2, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTNetLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v4, v5}, Lcom/fmm/dm/agent/amt/XAMTGpsListener;->startTimer(J)V

    .line 176
    const-string v2, "network"

    sget-object v3, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->XAMTNetLocListener:Lcom/fmm/dm/agent/amt/XAMTGpsListener;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 177
    const-string v2, "Network GPS listener is starting"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method
