.class public Lcom/fmm/dm/agent/XDMUITask;
.super Ljava/lang/Object;
.source "XDMUITask.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XUIInterface;
.implements Ljava/lang/Runnable;


# static fields
.field public static g_hDmUiTask:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 25
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 31
    new-instance v0, Lcom/fmm/dm/agent/XDMUITask$1;

    invoke-direct {v0, p0}, Lcom/fmm/dm/agent/XDMUITask$1;-><init>(Lcom/fmm/dm/agent/XDMUITask;)V

    sput-object v0, Lcom/fmm/dm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;

    .line 40
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 41
    return-void
.end method

.method public xdmUIEvent(Landroid/os/Message;)Z
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v7, 0x6f

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 45
    const/4 v1, 0x0

    .line 47
    .local v1, "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v6, :cond_0

    .line 173
    :goto_0
    return v4

    .line 50
    :cond_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v1    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    check-cast v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    .line 52
    .restart local v1    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    iget v6, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    sparse-switch v6, :sswitch_data_0

    .line 172
    :goto_1
    const/4 p1, 0x0

    move v4, v5

    .line 173
    goto :goto_0

    .line 55
    :sswitch_0
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    if-eqz v4, :cond_1

    .line 56
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    const/16 v6, 0x69

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 58
    :cond_1
    const-string v4, "XDMService.g_hDmHandler is null !!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 62
    :sswitch_1
    const-string v6, "XUI_DM_FINISH"

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 64
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v6

    if-ne v6, v4, :cond_2

    .line 66
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    if-eqz v4, :cond_3

    .line 67
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    const/16 v6, 0xab

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 72
    :cond_2
    :goto_2
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->setInitType(I)V

    .line 73
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_1

    .line 69
    :cond_3
    const-string v4, "XDMService.g_hDmHandler is null !!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 77
    :sswitch_2
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->setInitType(I)V

    .line 78
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 80
    invoke-static {v5}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpResetSessionSaveState(I)V

    .line 82
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    goto :goto_1

    .line 88
    :sswitch_3
    iget v6, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 90
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v6

    if-ne v6, v4, :cond_4

    .line 92
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    if-eqz v4, :cond_5

    .line 93
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 98
    :cond_4
    :goto_3
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->setInitType(I)V

    .line 99
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 101
    invoke-static {v5}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus(I)V

    .line 103
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    goto :goto_1

    .line 95
    :cond_5
    const-string v4, "XDMService.g_hDmHandler is null !!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 109
    :sswitch_4
    iget v6, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 111
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v6

    if-ne v6, v4, :cond_6

    .line 113
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    if-eqz v4, :cond_7

    .line 114
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 119
    :cond_6
    :goto_4
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->setInitType(I)V

    .line 120
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto/16 :goto_1

    .line 116
    :cond_7
    const-string v4, "XDMService.g_hDmHandler is null !!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 125
    :sswitch_5
    const-string v4, "XUI_DM_CONNECT"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 129
    :sswitch_6
    const-string v4, "XUI_DM_UIC_REQUEST"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 131
    iget-object v4, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    iget-object v3, v4, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    check-cast v3, Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 132
    .local v3, "uicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    const/4 v2, 0x0

    .line 133
    .local v2, "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateUicOption()Lcom/fmm/dm/eng/core/XDMUicOption;

    move-result-object v2

    .line 134
    invoke-static {v2, v3}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCopyUicOption(Ljava/lang/Object;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMUicOption;

    move-result-object v2

    .line 136
    sput-object v2, Lcom/fmm/dm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    .line 137
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    if-eqz v4, :cond_8

    .line 138
    sget-object v4, Lcom/fmm/dm/XDMService;->g_hDmHandler:Landroid/os/Handler;

    const/16 v6, 0x68

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 140
    :cond_8
    const-string v4, "XDMService.g_hDmHandler is null !!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 147
    .end local v2    # "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    .end local v3    # "uicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    :sswitch_7
    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpCookieClear()V

    .line 148
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->setInitType(I)V

    .line 149
    invoke-static {v5}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 150
    iget v4, v1, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    invoke-static {v4}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpRequestNoti(I)V

    goto/16 :goto_1

    .line 154
    :sswitch_8
    const/4 v0, 0x0

    .line 155
    .local v0, "bStatus":Z
    invoke-static {}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentTaskGetDmInitStatus()Z

    move-result v0

    .line 156
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "XUI_DM_IDLE_STATE :"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 158
    if-eqz v0, :cond_9

    .line 160
    invoke-static {v5}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpResumeNotiAction(I)V

    goto/16 :goto_1

    .line 164
    :cond_9
    const-string v4, "DM Not Initialized"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 52
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_5
        0x66 -> :sswitch_5
        0x68 -> :sswitch_6
        0x69 -> :sswitch_0
        0x6f -> :sswitch_4
        0x83 -> :sswitch_3
        0x84 -> :sswitch_2
        0x85 -> :sswitch_4
        0x86 -> :sswitch_4
        0x87 -> :sswitch_3
        0x88 -> :sswitch_3
        0x8d -> :sswitch_7
        0x8e -> :sswitch_7
        0x8f -> :sswitch_7
        0x90 -> :sswitch_7
        0x92 -> :sswitch_8
        0xab -> :sswitch_1
    .end sparse-switch
.end method
