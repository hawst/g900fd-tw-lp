.class public Lcom/fmm/dm/agent/amt/XAMTWaitTimer$XAMTDelayGPSProviderTimerTask;
.super Ljava/util/TimerTask;
.source "XAMTWaitTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/agent/amt/XAMTWaitTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "XAMTDelayGPSProviderTimerTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/agent/amt/XAMTWaitTimer;


# direct methods
.method public constructor <init>(Lcom/fmm/dm/agent/amt/XAMTWaitTimer;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer$XAMTDelayGPSProviderTimerTask;->this$0:Lcom/fmm/dm/agent/amt/XAMTWaitTimer;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 37
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 38
    iget-object v0, p0, Lcom/fmm/dm/agent/amt/XAMTWaitTimer$XAMTDelayGPSProviderTimerTask;->this$0:Lcom/fmm/dm/agent/amt/XAMTWaitTimer;

    const/4 v1, 0x0

    # setter for: Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->mWaitTimer:Ljava/util/Timer;
    invoke-static {v0, v1}, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;->access$002(Lcom/fmm/dm/agent/amt/XAMTWaitTimer;Ljava/util/Timer;)Ljava/util/Timer;

    .line 39
    const-string v0, "Location intent not received"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpGetLocationProcess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetLocationProcess(Z)V

    .line 43
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocation(Z)V

    .line 49
    :goto_0
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 50
    return-void

    .line 47
    :cond_0
    const-string v0, "Get Location is Processing"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method
