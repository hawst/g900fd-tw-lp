.class public Lcom/fmm/dm/agent/XDMTask;
.super Ljava/lang/Object;
.source "XDMTask.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XCommonInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XEventInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Lcom/fmm/dm/interfaces/XNOTIInterface;
.implements Lcom/fmm/dm/interfaces/XTPInterface;
.implements Lcom/fmm/dm/interfaces/XUICInterface;
.implements Lcom/fmm/dm/interfaces/XUIInterface;
.implements Ljava/lang/Runnable;


# static fields
.field public static g_IsDMInitialized:Z

.field public static g_IsPhoneBookInitialized:Z

.field public static g_IsSyncTaskInit:Z

.field public static g_ResumeInitCase:I

.field public static g_hDmTask:Landroid/os/Handler;


# instance fields
.field public m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

.field private m_bDBInit:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    sput-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    .line 53
    sput-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsPhoneBookInitialized:Z

    .line 54
    sput-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    .line 55
    sput v0, Lcom/fmm/dm/agent/XDMTask;->g_ResumeInitCase:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_bDBInit:Z

    .line 63
    sget-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 67
    :cond_0
    return-void
.end method

.method private xdmAgentCheckResumeNoti(I)V
    .locals 2
    .param p1, "nresumecase"    # I

    .prologue
    .line 652
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 654
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 656
    const-string v1, "Start resumecase for DM IP-Push"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 657
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    .line 716
    :cond_0
    :goto_0
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentSetNotiResumeCase(I)V

    .line 717
    return-void

    .line 659
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 661
    const-string v1, "Start resumecase for PCW UnLock"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 662
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 663
    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteOperation()V

    goto :goto_0

    .line 666
    :cond_2
    const/4 v1, 0x5

    if-ne p1, v1, :cond_3

    .line 668
    const-string v1, "Start resumecase for PCW Wipe"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 669
    invoke-static {}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastGetInitResumeResult()I

    move-result v0

    .line 670
    .local v0, "nResultReport":I
    invoke-static {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentWipeResult(I)V

    goto :goto_0

    .line 672
    .end local v0    # "nResultReport":I
    :cond_3
    const/4 v1, 0x6

    if-ne p1, v1, :cond_4

    .line 674
    const-string v1, "Start resumecase for PCW Forwarding"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 675
    invoke-static {}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastGetInitResumeResult()I

    move-result v0

    .line 676
    .restart local v0    # "nResultReport":I
    invoke-static {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentForwardingResult(I)V

    goto :goto_0

    .line 679
    .end local v0    # "nResultReport":I
    :cond_4
    const/4 v1, 0x7

    if-ne p1, v1, :cond_5

    .line 681
    const-string v1, "Start resumecase for Update URL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 682
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastSetUpdateURL(Landroid/content/Intent;)V

    goto :goto_0

    .line 684
    :cond_5
    const/16 v1, 0x8

    if-ne p1, v1, :cond_6

    .line 686
    const-string v1, "Start resumecase for UNLOCK_ALERT"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 687
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiUnlockAlertReport()V

    goto :goto_0

    .line 689
    :cond_6
    const/16 v1, 0xa

    if-ne p1, v1, :cond_7

    .line 691
    const-string v1, "Start resumecase for EMERGENCY_APPLICATION_MODIFIED"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 692
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiEmergencyModeChangeAlertReport()V

    goto :goto_0

    .line 694
    :cond_7
    const/16 v1, 0x9

    if-ne p1, v1, :cond_8

    .line 696
    const-string v1, "Start resumecase for EMERGENCY_STATE_CHANGED "

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 697
    invoke-static {}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastGetInitResumeResult()I

    move-result v0

    .line 698
    .restart local v0    # "nResultReport":I
    invoke-static {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentEmergencyModeResult(I)V

    goto :goto_0

    .line 700
    .end local v0    # "nResultReport":I
    :cond_8
    const/16 v1, 0xb

    if-ne p1, v1, :cond_9

    .line 702
    const-string v1, "Start resumecase for GET_LOCATION "

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 703
    invoke-static {}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastGetInitIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentGetLocationResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 705
    :cond_9
    const/16 v1, 0xc

    if-ne p1, v1, :cond_a

    .line 707
    const-string v1, "Start resumecase for REACTIVATIONLOCK "

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 708
    invoke-static {}, Lcom/fmm/dm/XDMBroadcastReceiver;->xdmBroadcastGetInitResumeResult()I

    move-result v0

    .line 709
    .restart local v0    # "nResultReport":I
    invoke-static {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOAdapter;->xlawmoAdpReceiveIntentReactivationResult(I)V

    goto/16 :goto_0

    .line 711
    .end local v0    # "nResultReport":I
    :cond_a
    const/16 v1, 0xd

    if-ne p1, v1, :cond_0

    .line 713
    const-string v1, "Start resumecase for XCOMMON_INTENT_RESUME_REACTIVATIONLOCK_ALERT"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 714
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiReactivationLockAlertReport()V

    goto/16 :goto_0
.end method

.method public static xdmAgentGetNotiResumeCase()I
    .locals 1

    .prologue
    .line 642
    sget v0, Lcom/fmm/dm/agent/XDMTask;->g_ResumeInitCase:I

    return v0
.end method

.method public static xdmAgentSetNotiResumeCase(I)V
    .locals 0
    .param p0, "nCase"    # I

    .prologue
    .line 647
    sput p0, Lcom/fmm/dm/agent/XDMTask;->g_ResumeInitCase:I

    .line 648
    return-void
.end method

.method private xdmAgentTaskDBInit()Z
    .locals 3

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 102
    .local v0, "bRtn":Z
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpVerifyDevID()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbDMffs_Init()V

    .line 107
    const/4 v0, 0x1

    .line 108
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/fmm/dm/agent/XDMTask;->m_bDBInit:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return v0

    .line 110
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 117
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_0
    const-string v2, "DB Init Fail!!"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmAgentTaskGetDmInitStatus()Z
    .locals 1

    .prologue
    .line 142
    sget-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    return v0
.end method

.method public static xdmAgentTaskSetDmInitStatus(Z)V
    .locals 0
    .param p0, "bInitState"    # Z

    .prologue
    .line 147
    sput-boolean p0, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    .line 148
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 73
    new-instance v0, Lcom/fmm/dm/agent/XDMTask$1;

    invoke-direct {v0, p0}, Lcom/fmm/dm/agent/XDMTask$1;-><init>(Lcom/fmm/dm/agent/XDMTask;)V

    sput-object v0, Lcom/fmm/dm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;

    .line 90
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 91
    return-void
.end method

.method public xdmAgentDmXXXFail()V
    .locals 1

    .prologue
    .line 746
    const-string v0, "===xdmAgentDmXXXFail==="

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 747
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    invoke-virtual {v0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 748
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    invoke-virtual {v0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    .line 749
    return-void
.end method

.method public xdmAgentTaskHandler(Landroid/os/Message;)Z
    .locals 26
    .param p1, "msg"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 152
    const/4 v8, 0x0

    .line 154
    .local v8, "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    if-nez v23, :cond_0

    .line 155
    const/16 v23, 0x1

    .line 637
    :goto_0
    return v23

    .line 157
    :cond_0
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v8    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    check-cast v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;

    .line 158
    .restart local v8    # "msgItem":Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;
    iget v0, v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->type:I

    move/from16 v23, v0

    sparse-switch v23, :sswitch_data_0

    .line 636
    :cond_1
    :goto_1
    const/16 p1, 0x0

    .line 637
    const/16 v23, 0x0

    goto :goto_0

    .line 161
    :sswitch_0
    const-string v23, "XEVENT_OS_INITIALIZED"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 162
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetIsConnected(Z)V

    .line 163
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 164
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 165
    const/16 v23, 0x5

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 166
    invoke-virtual/range {p0 .. p0}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentTaskInit()V

    .line 167
    const/16 v23, 0x1

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 171
    :sswitch_1
    const-string v23, "XEVENT_PHONEBOOK_INITIALIZED"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 172
    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentTaskSetPhonebookInit(Z)V

    .line 174
    sget-boolean v23, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v23, :cond_2

    .line 175
    const/16 v23, 0xc

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 177
    :cond_2
    const-string v23, "Initialized"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1

    .line 182
    :sswitch_2
    const-string v23, "XEVENT_DM_INIT"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 184
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpGetNetStatus()I

    move-result v23

    const/16 v24, -0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_3

    .line 186
    const-string v23, "Network Status is not ready. DM Not Initialized"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 187
    const/16 v23, 0x0

    sput-boolean v23, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    goto :goto_1

    .line 191
    :cond_3
    sget-boolean v23, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v23, :cond_1

    .line 193
    const/4 v6, 0x1

    .line 195
    .local v6, "bRet":Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_bDBInit:Z

    move/from16 v23, v0

    if-nez v23, :cond_4

    .line 196
    invoke-direct/range {p0 .. p0}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentTaskDBInit()Z

    move-result v6

    .line 198
    :cond_4
    if-nez v6, :cond_5

    .line 200
    const/16 v23, 0x0

    sput-boolean v23, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    goto :goto_1

    .line 204
    :cond_5
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpEXTInit()Z

    move-result v23

    if-nez v23, :cond_6

    .line 206
    const-string v23, "XEVENT_DM_INIT : Not Initialized"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 207
    const/16 v23, 0x0

    sput-boolean v23, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    .line 208
    const/16 v23, 0x0

    sput-boolean v23, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    goto/16 :goto_1

    .line 212
    :cond_6
    const-string v23, "XEVENT_DM_INIT : Initialized"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 213
    const/16 v23, 0x1

    sput-boolean v23, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    .line 214
    const/16 v23, 0x0

    sput-boolean v23, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    .line 216
    invoke-static {}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentGetNotiResumeCase()I

    move-result v23

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentCheckResumeNoti(I)V

    goto/16 :goto_1

    .line 223
    .end local v6    # "bRet":Z
    :sswitch_3
    const-string v23, "XEVENT_DM_CONNECT"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 226
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetUserCancel(Z)V

    .line 228
    sget-boolean v23, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-eqz v23, :cond_b

    .line 230
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmProtoIsWIFIConnected()Z

    move-result v23

    if-nez v23, :cond_7

    .line 232
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileIndex()I

    move-result v23

    if-nez v23, :cond_7

    .line 233
    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/XDMService;->xdmSetDataRoamingEnabled(Z)V

    .line 236
    :cond_7
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v23

    if-eqz v23, :cond_8

    .line 238
    const-string v23, "XEVENT_DM_CONNECT : Changed Protocol"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 252
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpInit(I)I

    move-result v11

    .line 253
    .local v11, "nRet":I
    if-eqz v11, :cond_a

    .line 255
    const-string v23, "xdmAgentTpInit fail!!"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 256
    const/16 v23, 0x15

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 242
    .end local v11    # "nRet":I
    :cond_8
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileInfo()Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v15

    .line 243
    .local v15, "pNvInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    if-nez v15, :cond_9

    .line 244
    new-instance v15, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .end local v15    # "pNvInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-direct {v15}, Lcom/fmm/dm/db/file/XDBProfileInfo;-><init>()V

    .line 245
    .restart local v15    # "pNvInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    :cond_9
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "server index : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileIndex()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 246
    iget-object v0, v15, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerUrl(Ljava/lang/String;)V

    .line 247
    iget-object v0, v15, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerAddress(Ljava/lang/String;)V

    .line 248
    iget v0, v15, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerPort(I)V

    .line 249
    iget-object v0, v15, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerProtocol(Ljava/lang/String;)V

    goto :goto_2

    .line 260
    .end local v15    # "pNvInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    .restart local v11    # "nRet":I
    :cond_a
    const/16 v23, 0x20

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 265
    .end local v11    # "nRet":I
    :cond_b
    const-string v23, "XUI_DM_NOT_INIT"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 266
    const/16 v23, 0x0

    const/16 v24, 0x69

    invoke-static/range {v23 .. v24}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 273
    :sswitch_4
    const/4 v5, 0x0

    .line 274
    .local v5, "bRc":Z
    const-string v23, "XEVENT_DM_CONNECTFAIL"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCheckRetry()Z

    move-result v5

    .line 279
    if-eqz v5, :cond_c

    .line 281
    const-wide/16 v24, 0xdac

    invoke-static/range {v24 .. v25}, Ljava/lang/Thread;->sleep(J)V

    .line 282
    const/16 v23, 0xb

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 286
    :cond_c
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v23

    if-eqz v23, :cond_d

    .line 288
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbSetBackUpServerUrl()V

    .line 290
    :cond_d
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 291
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 292
    const/16 v23, 0x0

    const/16 v24, 0x6f

    invoke-static/range {v23 .. v24}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 293
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUnlockAlert(Z)V

    .line 294
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetEmergencyModeChangeAlert(Z)V

    .line 295
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetReActiveAlert(Z)V

    .line 296
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetLocationProcess(Z)V

    .line 298
    const/16 v23, 0x1

    sput v23, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    goto/16 :goto_1

    .line 304
    .end local v5    # "bRc":Z
    :sswitch_5
    const/4 v5, 0x0

    .line 305
    .restart local v5    # "bRc":Z
    const-string v23, "XEVENT_DM_SENDFAIL"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCheckRetry()Z

    move-result v5

    .line 310
    if-eqz v5, :cond_e

    .line 312
    const/16 v23, 0xb

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 315
    :cond_e
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v23

    if-eqz v23, :cond_f

    .line 317
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbSetBackUpServerUrl()V

    .line 319
    :cond_f
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 320
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 321
    const/16 v23, 0x0

    const/16 v24, 0x86

    invoke-static/range {v23 .. v24}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 322
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUnlockAlert(Z)V

    .line 323
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetEmergencyModeChangeAlert(Z)V

    .line 324
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetReActiveAlert(Z)V

    .line 325
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetLocationProcess(Z)V

    .line 327
    const/16 v23, 0x1

    sput v23, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    goto/16 :goto_1

    .line 333
    .end local v5    # "bRc":Z
    :sswitch_6
    const/4 v5, 0x0

    .line 334
    .restart local v5    # "bRc":Z
    const-string v23, "XEVENT_DM_RECEIVETFAIL"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    .line 339
    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetUserCancel()Z

    move-result v23

    if-eqz v23, :cond_10

    .line 341
    const-string v23, "Do not connect to download server due to user cancel"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 342
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentTaskUserCancelDownload(I)V

    goto/16 :goto_1

    .line 346
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCheckRetry()Z

    move-result v5

    .line 347
    if-eqz v5, :cond_11

    .line 349
    const/16 v23, 0xb

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 352
    :cond_11
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v23

    if-eqz v23, :cond_12

    .line 354
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbSetBackUpServerUrl()V

    .line 356
    :cond_12
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 357
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 358
    const/16 v23, 0x0

    const/16 v24, 0x85

    invoke-static/range {v23 .. v24}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 359
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUnlockAlert(Z)V

    .line 360
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetEmergencyModeChangeAlert(Z)V

    .line 361
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetReActiveAlert(Z)V

    .line 362
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetLocationProcess(Z)V

    .line 364
    const/16 v23, 0x1

    sput v23, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    goto/16 :goto_1

    .line 369
    .end local v5    # "bRc":Z
    :sswitch_7
    const-string v23, "XEVENT_DM_START"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 370
    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMAgent;->m_AgentHandler:Lcom/fmm/dm/agent/XDMAgentHandler;

    move-object/from16 v23, v0

    const/16 v24, 0x17

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v25}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrContinueSession(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 375
    :sswitch_8
    const-string v23, "XEVENT_DM_CONTINUE"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 377
    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetUserCancel()Z

    move-result v23

    if-eqz v23, :cond_13

    .line 379
    const-string v23, "Do not connect to download server due to user cancel"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 380
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentTaskUserCancelDownload(I)V

    goto/16 :goto_1

    .line 383
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMAgent;->m_AgentHandler:Lcom/fmm/dm/agent/XDMAgentHandler;

    move-object/from16 v23, v0

    const/16 v24, 0x18

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v25}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrContinueSession(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 387
    :sswitch_9
    const-string v23, "XEVENT_DM_ABORT"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 389
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v23, v0

    if-nez v23, :cond_14

    .line 391
    const-string v23, "param is null"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 392
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 395
    :cond_14
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetChangedProtocol()Z

    move-result v23

    if-eqz v23, :cond_15

    .line 397
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbSetBackUpServerUrl()V

    .line 400
    :cond_15
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v13, v0, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    check-cast v13, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;

    .line 402
    .local v13, "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpCookieClear()V

    .line 403
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetUnlockAlert(Z)V

    .line 404
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetEmergencyModeChangeAlert(Z)V

    .line 405
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->xuiAdpSetReActiveAlert(Z)V

    .line 406
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetLocationProcess(Z)V

    .line 408
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, " pAbortParam.abortCode:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    iget v0, v13, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 409
    iget v0, v13, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v23, v0

    const/16 v24, 0xfb

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_17

    .line 412
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 413
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentClose()I

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    .line 455
    :cond_16
    :goto_3
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 456
    iget v0, v13, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v23, v0

    packed-switch v23, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_1

    .line 463
    :pswitch_1
    const/16 v23, 0x0

    const/16 v24, 0x88

    invoke-static/range {v23 .. v24}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 418
    :cond_17
    iget v0, v13, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v23, v0

    const/16 v24, 0xf5

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_18

    .line 420
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentClose()I

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    goto :goto_3

    .line 425
    :cond_18
    iget v0, v13, Lcom/fmm/dm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v23, v0

    const/16 v24, 0xf9

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1a

    .line 427
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v9

    .line 429
    .local v9, "nAgentType":I
    const-string v23, "XEVENT_ABORT_HTTP_ERROR, not implement..."

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 430
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentClose()I

    .line 432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    .line 435
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v9, v0, :cond_19

    .line 437
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 438
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    goto :goto_3

    .line 440
    :cond_19
    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v9, v0, :cond_16

    .line 442
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetDmAgentType(I)V

    .line 443
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtSetOperation(I)V

    goto/16 :goto_3

    .line 448
    .end local v9    # "nAgentType":I
    :cond_1a
    const-string v23, " XEVENT_DM_ABORT : ELSE"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 449
    invoke-static {}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentClose()I

    .line 451
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    goto/16 :goto_3

    .line 459
    :pswitch_2
    const/16 v23, 0x0

    const/16 v24, 0x84

    invoke-static/range {v23 .. v24}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 466
    :pswitch_3
    const/16 v23, 0x0

    const/16 v24, 0x87

    invoke-static/range {v23 .. v24}, Lcom/fmm/dm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 474
    .end local v13    # "pAbortParam":Lcom/fmm/dm/eng/core/XDMAbortMsgParam;
    :sswitch_a
    const-string v23, "XEVENT_DM_FINISH"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    .line 477
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMAgent;->m_AgentHandler:Lcom/fmm/dm/agent/XDMAgentHandler;

    move-object/from16 v23, v0

    const/16 v24, 0x1a

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v25}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrContinueSession(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 483
    :sswitch_b
    const-string v23, "XEVENT_DM_TCPIP_OPEN"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 484
    const/16 v23, 0x17

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 490
    :sswitch_c
    const-string v23, "XEVENT_UIC_REQUEST"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 491
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Lcom/fmm/dm/eng/core/XDMUicOption;

    .line 493
    .local v18, "pSrcUICOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    const/16 v19, 0x0

    .line 494
    .local v19, "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    const/16 v21, 0x0

    .line 496
    .local v21, "pUicResultKeep":Lcom/fmm/dm/eng/core/XDMUicResult;
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicGetResultKeep()Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v21

    .line 497
    invoke-static {}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCreateUicOption()Lcom/fmm/dm/eng/core/XDMUicOption;

    move-result-object v19

    .line 498
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicCopyUicOption(Ljava/lang/Object;Ljava/lang/Object;)Lcom/fmm/dm/eng/core/XDMUicOption;

    move-result-object v19

    .line 499
    const/16 v23, 0x68

    const/16 v24, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v19

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendUIMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 501
    invoke-static/range {v21 .. v21}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/fmm/dm/eng/core/XDMUicResult;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 505
    .end local v18    # "pSrcUICOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    .end local v19    # "pUicOption":Lcom/fmm/dm/eng/core/XDMUicOption;
    .end local v21    # "pUicResultKeep":Lcom/fmm/dm/eng/core/XDMUicResult;
    :sswitch_d
    const-string v23, "XEVENT_UIC_RESPONSE"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 507
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Lcom/fmm/dm/eng/core/XDMUicResult;

    .line 508
    .local v20, "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    const/16 v23, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-static {v0, v1}, Lcom/fmm/dm/eng/core/XDMUic;->xdmUicSetResultKeep(Lcom/fmm/dm/eng/core/XDMUicResult;I)Lcom/fmm/dm/eng/core/XDMUicResult;

    move-result-object v20

    .line 509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMAgent;->m_AgentHandler:Lcom/fmm/dm/agent/XDMAgentHandler;

    move-object/from16 v23, v0

    const/16 v24, 0x66

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/fmm/dm/agent/XDMAgentHandler;->xdmAgentHdlrContinueSession(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 513
    .end local v20    # "pUicResult":Lcom/fmm/dm/eng/core/XDMUicResult;
    :sswitch_e
    const-string v23, "XEVENT_NOTI_RECEIVED"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 514
    const/16 v16, 0x0

    .line 515
    .local v16, "pPushMsg":Lcom/fmm/dm/noti/XNOTIMessage;
    new-instance v14, Lcom/fmm/dm/noti/XNOTIHandler;

    invoke-direct {v14}, Lcom/fmm/dm/noti/XNOTIHandler;-><init>()V

    .line 516
    .local v14, "pHandler":Lcom/fmm/dm/noti/XNOTIHandler;
    new-instance v22, Lcom/fmm/dm/noti/XNOTI;

    invoke-direct/range {v22 .. v22}, Lcom/fmm/dm/noti/XNOTI;-><init>()V

    .line 517
    .local v22, "ptMsg":Lcom/fmm/dm/noti/XNOTI;
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    check-cast v7, Lcom/fmm/dm/noti/XNOTIMessage;

    .line 519
    .local v7, "msg1":Lcom/fmm/dm/noti/XNOTIMessage;
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpVerifyDevID()Z

    move-result v23

    if-nez v23, :cond_1b

    .line 521
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 525
    :cond_1b
    invoke-virtual {v14, v7}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleMessageCopy(Ljava/lang/Object;)Lcom/fmm/dm/noti/XNOTIMessage;

    move-result-object v16

    .line 526
    if-nez v16, :cond_1c

    .line 528
    const-string v23, "pPushMsg is NULL"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 529
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 533
    :cond_1c
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/fmm/dm/noti/XNOTIHandler;->xnotiPushHdleMsgHandler(Lcom/fmm/dm/noti/XNOTIMessage;)Lcom/fmm/dm/noti/XNOTI;

    move-result-object v22

    .line 534
    if-nez v22, :cond_1d

    .line 536
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 540
    :cond_1d
    move-object/from16 v0, v22

    iget v0, v0, Lcom/fmm/dm/noti/XNOTI;->appId:I

    move/from16 v23, v0

    packed-switch v23, :pswitch_data_1

    .line 584
    const-string v23, "Not Support Application"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 543
    :pswitch_4
    const/4 v4, 0x1

    .line 545
    .local v4, "bNotiExecute":Z
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    move-object/from16 v23, v0

    if-nez v23, :cond_1e

    .line 547
    const-string v23, "triggerHeader is NULL."

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 548
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 552
    :cond_1e
    new-instance v12, Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-direct {v12}, Lcom/fmm/dm/db/file/XDBNotiInfo;-><init>()V

    .line 553
    .local v12, "notiInfo":Lcom/fmm/dm/db/file/XDBNotiInfo;
    const/16 v23, 0x0

    move/from16 v0, v23

    iput v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->appId:I

    .line 554
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/fmm/dm/noti/XNOTITriggerheader;->uiMode:I

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    .line 555
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iput-object v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    .line 556
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/fmm/dm/noti/XNOTI;->triggerHeader:Lcom/fmm/dm/noti/XNOTITriggerheader;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/noti/XNOTITriggerheader;->m_szSessionID:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iput-object v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    .line 560
    iget-object v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbCheckActiveProfileIndexByServerID(Ljava/lang/String;)Z

    move-result v23

    if-nez v23, :cond_1f

    .line 562
    const-string v23, "Not Active Profile Index By ServerID"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 563
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 568
    :cond_1f
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSessionSaveStatus()Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    move-result-object v17

    .line 569
    .local v17, "pSessionSaveInfo":Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    if-eqz v17, :cond_20

    move-object/from16 v0, v17

    iget v0, v0, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    move/from16 v23, v0

    if-nez v23, :cond_21

    :cond_20
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->getInitType()I

    move-result v23

    if-eqz v23, :cond_22

    .line 571
    :cond_21
    invoke-static {v12}, Lcom/fmm/dm/db/file/XDBNoti;->xdbNotiInsertInfo(Ljava/lang/Object;)V

    .line 572
    const-string v23, "Noti was saved"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 573
    const/4 v4, 0x0

    .line 574
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    .line 577
    :cond_22
    if-eqz v4, :cond_1

    .line 579
    const/16 v23, 0x34

    const/16 v24, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v12, v1}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 590
    .end local v4    # "bNotiExecute":Z
    .end local v7    # "msg1":Lcom/fmm/dm/noti/XNOTIMessage;
    .end local v12    # "notiInfo":Lcom/fmm/dm/db/file/XDBNotiInfo;
    .end local v14    # "pHandler":Lcom/fmm/dm/noti/XNOTIHandler;
    .end local v16    # "pPushMsg":Lcom/fmm/dm/noti/XNOTIMessage;
    .end local v17    # "pSessionSaveInfo":Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    .end local v22    # "ptMsg":Lcom/fmm/dm/noti/XNOTI;
    :sswitch_f
    const-string v23, "XEVENT_NOTI_EXECUTE"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 591
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v12, v0, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    check-cast v12, Lcom/fmm/dm/db/file/XDBNotiInfo;

    .line 593
    .restart local v12    # "notiInfo":Lcom/fmm/dm/db/file/XDBNotiInfo;
    if-nez v12, :cond_23

    .line 595
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 599
    :cond_23
    iget v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->appId:I

    move/from16 v23, v0

    packed-switch v23, :pswitch_data_2

    .line 618
    const-string v23, "Not Support Application"

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 622
    :goto_4
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 602
    :pswitch_5
    sget-boolean v23, Lcom/fmm/dm/adapter/XDMFeature;->XDM_FEATURE_SIM_CHANGE:Z

    if-eqz v23, :cond_24

    invoke-static {}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xlawmoGetbSimChange()Z

    move-result v23

    if-eqz v23, :cond_24

    .line 604
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/lawmo/XLAWMOSimChange;->xlawmoSetbSimChange(Z)V

    .line 605
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetSimChangeAlert(Z)V

    .line 608
    :cond_24
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/ui/XUIAdapter;->setInitType(I)V

    .line 609
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus(I)V

    .line 610
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 611
    iget-object v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetActiveProfileIndexByServerID(Ljava/lang/String;)I

    .line 612
    iget-object v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiSessionID(Ljava/lang/String;)V

    .line 613
    iget v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/db/file/XDB;->xdbSetNotiEvent(I)V

    .line 614
    iget v0, v12, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpProcessNotiMessage(I)V

    goto :goto_4

    .line 627
    .end local v12    # "notiInfo":Lcom/fmm/dm/db/file/XDBNotiInfo;
    :sswitch_10
    const/4 v10, 0x0

    .line 628
    .local v10, "nID":I
    iget-object v0, v8, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/fmm/dm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 629
    invoke-static {v10}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushAdpProcessWapPushMsg(I)V

    goto/16 :goto_1

    .line 158
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0xb -> :sswitch_3
        0xc -> :sswitch_2
        0x15 -> :sswitch_4
        0x17 -> :sswitch_7
        0x18 -> :sswitch_8
        0x19 -> :sswitch_9
        0x1a -> :sswitch_a
        0x1b -> :sswitch_5
        0x1c -> :sswitch_6
        0x20 -> :sswitch_b
        0x33 -> :sswitch_e
        0x34 -> :sswitch_f
        0x39 -> :sswitch_10
        0x65 -> :sswitch_c
        0x66 -> :sswitch_d
    .end sparse-switch

    .line 456
    :pswitch_data_0
    .packed-switch 0xf4
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 540
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch

    .line 599
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method public xdmAgentTaskInit()V
    .locals 2

    .prologue
    .line 124
    sget-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Lcom/fmm/dm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/fmm/dm/agent/XDMAgent;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    .line 127
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    new-instance v1, Lcom/fmm/dm/agent/XDMAgentHandler;

    invoke-direct {v1}, Lcom/fmm/dm/agent/XDMAgentHandler;-><init>()V

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAgent;->m_AgentHandler:Lcom/fmm/dm/agent/XDMAgentHandler;

    .line 130
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    iget-object v0, v0, Lcom/fmm/dm/agent/XDMAgent;->m_AgentHandler:Lcom/fmm/dm/agent/XDMAgentHandler;

    iget-object v1, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    iget-object v1, v1, Lcom/fmm/dm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    iput-object v1, v0, Lcom/fmm/dm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/fmm/dm/tp/XTPAdapter;

    .line 133
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbInit()Z

    .line 134
    invoke-direct {p0}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentTaskDBInit()Z

    .line 136
    const/4 v0, 0x1

    sput-boolean v0, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    .line 138
    :cond_0
    return-void
.end method

.method public xdmAgentTaskSetPhonebookInit(Z)V
    .locals 0
    .param p1, "initvalue"    # Z

    .prologue
    .line 95
    sput-boolean p1, Lcom/fmm/dm/agent/XDMTask;->g_IsPhoneBookInitialized:Z

    .line 96
    return-void
.end method

.method public xdmAgentTaskUserCancelDownload(I)V
    .locals 4
    .param p1, "appId"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 722
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "appId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 724
    if-nez p1, :cond_0

    .line 726
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    invoke-virtual {v0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpClose()V

    .line 727
    iget-object v0, p0, Lcom/fmm/dm/agent/XDMTask;->m_DmAgent:Lcom/fmm/dm/agent/XDMAgent;

    invoke-virtual {v0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpCloseNetwork()V

    .line 729
    invoke-static {v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 730
    invoke-static {v2}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 731
    invoke-static {v2}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetUserCancel(Z)V

    .line 741
    :goto_0
    return-void

    .line 737
    :cond_0
    const-string v0, "send generic alert for fail to download package"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 738
    const/16 v0, 0xb

    invoke-static {v0, v3, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 740
    invoke-static {v2}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetUserCancel(Z)V

    goto :goto_0
.end method
