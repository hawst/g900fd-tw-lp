.class public Lcom/fmm/dm/agent/amt/XAMTGpsManager;
.super Ljava/lang/Object;
.source "XAMTGpsManager.java"


# static fields
.field public static SIMmcc:I

.field public static SIMmnc:I

.field public static g_szCellId:Ljava/lang/String;

.field public static g_szLac:Ljava/lang/String;

.field public static g_szMsisdn:Ljava/lang/String;

.field public static mAccuracy:D

.field public static mAltitude:D

.field public static mBearing:D

.field public static mLatitude:D

.field public static mLongitude:D

.field public static mSpeed:D

.field public static mXAMTWaitTimer:Lcom/fmm/dm/agent/amt/XAMTWaitTimer;

.field public static mcc:I

.field public static mnc:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 21
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLatitude:D

    .line 22
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLongitude:D

    .line 23
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAltitude:D

    .line 24
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mBearing:D

    .line 25
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mSpeed:D

    .line 26
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAccuracy:D

    .line 27
    sput v2, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mcc:I

    .line 28
    sput v2, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mnc:I

    .line 31
    sput v2, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->SIMmcc:I

    .line 32
    sput v2, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->SIMmnc:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xamtGpsGetNonData()[I
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 110
    const/4 v9, 0x4

    new-array v6, v9, [I

    fill-array-data v6, :array_0

    .line 112
    .local v6, "nonGpsData":[I
    const/4 v4, 0x0

    .line 113
    .local v4, "mcc":I
    const/4 v5, 0x0

    .line 114
    .local v5, "mnc":I
    const/4 v3, 0x0

    .line 115
    .local v3, "lac":I
    const/4 v0, 0x0

    .line 116
    .local v0, "cellid":I
    const-string v9, "phone"

    invoke-static {v9}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 117
    .local v7, "phone":Landroid/telephony/TelephonyManager;
    if-nez v7, :cond_0

    .line 119
    const-string v9, "TelephonyManager is null!!"

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v6, v8

    .line 167
    .end local v6    # "nonGpsData":[I
    :goto_0
    return-object v6

    .line 123
    .restart local v6    # "nonGpsData":[I
    :cond_0
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v9

    if-ne v9, v10, :cond_3

    .line 125
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v2

    check-cast v2, Landroid/telephony/gsm/GsmCellLocation;

    .line 126
    .local v2, "gsm_cell":Landroid/telephony/gsm/GsmCellLocation;
    if-eqz v2, :cond_2

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v9

    if-ne v9, v10, :cond_2

    .line 130
    :try_start_0
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 132
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 133
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 135
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " mcc = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mnc = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 138
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v3

    .line 139
    invoke-virtual {v2}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v0

    .line 140
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Lac = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " cellid = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 141
    const/4 v9, 0x0

    aput v4, v6, v9

    .line 142
    const/4 v9, 0x1

    aput v5, v6, v9

    .line 143
    const/4 v9, 0x2

    aput v3, v6, v9

    .line 144
    const/4 v9, 0x3

    aput v0, v6, v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 147
    :catch_0
    move-exception v1

    .line 149
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v6, v8

    .line 150
    goto/16 :goto_0

    .line 155
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v9, "Error getting cell location info."

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v6, v8

    .line 156
    goto/16 :goto_0

    .line 159
    .end local v2    # "gsm_cell":Landroid/telephony/gsm/GsmCellLocation;
    :cond_3
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v9

    if-ne v9, v11, :cond_4

    .line 161
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CDMA not supported"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move-object v6, v8

    .line 162
    goto/16 :goto_0

    .line 166
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "PhoneType not supported"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move-object v6, v8

    .line 167
    goto/16 :goto_0

    .line 110
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public static xamtGpsGetNonSIMData()[I
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 173
    const/4 v7, 0x2

    new-array v3, v7, [I

    fill-array-data v3, :array_0

    .line 175
    .local v3, "nonGpsSIMData":[I
    const/4 v1, 0x0

    .line 176
    .local v1, "mcc":I
    const/4 v2, 0x0

    .line 178
    .local v2, "mnc":I
    const-string v7, "phone"

    invoke-static {v7}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 179
    .local v5, "tel":Landroid/telephony/TelephonyManager;
    if-nez v5, :cond_0

    .line 181
    const-string v7, "TelephonyManager is null!!"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v3, v6

    .line 207
    .end local v3    # "nonGpsSIMData":[I
    :goto_0
    return-object v3

    .line 185
    .restart local v3    # "nonGpsSIMData":[I
    :cond_0
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v4

    .line 186
    .local v4, "szSimOperator":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 190
    const/4 v7, 0x0

    const/4 v8, 0x3

    :try_start_0
    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 191
    const/4 v7, 0x3

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 192
    const/4 v7, 0x0

    aput v1, v3, v7

    .line 193
    const/4 v7, 0x1

    aput v2, v3, v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " mcc = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mnc = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v3, v6

    .line 198
    goto :goto_0

    .line 206
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v7, "Error getting cell location info from SIM."

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v3, v6

    .line 207
    goto :goto_0

    .line 173
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static xamtGpsInitLocation()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 38
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLatitude:D

    .line 39
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLongitude:D

    .line 40
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAltitude:D

    .line 41
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mBearing:D

    .line 42
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mSpeed:D

    .line 43
    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAccuracy:D

    .line 44
    return-void
.end method

.method public static xamtGpsSetLocation(Landroid/content/Intent;)V
    .locals 5
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 90
    const-string v0, "Latitude"

    invoke-virtual {p0, v0, v2, v3}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLatitude:D

    .line 91
    const-string v0, "Longitude"

    invoke-virtual {p0, v0, v2, v3}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLongitude:D

    .line 92
    const-string v0, "Altitude"

    invoke-virtual {p0, v0, v2, v3}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAltitude:D

    .line 93
    const-string v0, "Bearing"

    invoke-virtual {p0, v0, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mBearing:D

    .line 94
    const-string v0, "Speed"

    invoke-virtual {p0, v0, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mSpeed:D

    .line 95
    const-string v0, "Accuracy"

    invoke-virtual {p0, v0, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAccuracy:D

    .line 96
    return-void
.end method

.method public static xamtGpsSetLocation(Landroid/location/Location;)V
    .locals 2
    .param p0, "location"    # Landroid/location/Location;

    .prologue
    .line 100
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLatitude:D

    .line 101
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mLongitude:D

    .line 102
    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAltitude:D

    .line 103
    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mBearing:D

    .line 104
    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mSpeed:D

    .line 105
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mAccuracy:D

    .line 106
    return-void
.end method


# virtual methods
.method public xamtGpsGetNonLocation()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    const/4 v0, 0x0

    .line 50
    .local v0, "nonGPSdata":[I
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->xamtGpsGetNonData()[I

    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    .line 53
    aget v3, v0, v2

    sput v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mcc:I

    .line 54
    aget v3, v0, v1

    sput v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mnc:I

    .line 55
    const-string v3, "%04x"

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x2

    aget v5, v0, v5

    int-to-short v5, v5

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->g_szLac:Ljava/lang/String;

    .line 56
    const-string v3, "%08x"

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x3

    aget v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->g_szCellId:Ljava/lang/String;

    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mcc is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mcc:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mnc is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mnc:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " lac is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->g_szLac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cellid is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->g_szCellId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 63
    :goto_0
    return v1

    .line 62
    :cond_0
    const-string v1, "nonGPSdata is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move v1, v2

    .line 63
    goto :goto_0
.end method

.method public xamtGpsGetNonSIMLoc()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    const/4 v0, 0x0

    .line 71
    .local v0, "nonGpsSIMdata":[I
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->xamtGpsGetNonSIMData()[I

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 74
    aget v2, v0, v2

    sput v2, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->SIMmcc:I

    .line 75
    aget v2, v0, v1

    sput v2, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->SIMmnc:I

    .line 77
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmGetTargetMSISDN()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->g_szMsisdn:Ljava/lang/String;

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SIMDATA; SIMmcc is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->SIMmcc:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", SIMmnc is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->SIMmnc:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 84
    :goto_0
    return v1

    .line 83
    :cond_0
    const-string v1, "nonGPSdata is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move v1, v2

    .line 84
    goto :goto_0
.end method

.method public xamtGpsStartTracking(Landroid/content/Context;)V
    .locals 6
    .param p1, "pcontext"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 213
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v4

    if-nez v4, :cond_0

    .line 215
    const-string v4, "USER_OWNER"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 216
    move-object v3, p1

    .line 217
    .local v3, "mcontext":Landroid/content/Context;
    invoke-static {v3}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtSetupGpsTracking(Landroid/content/Context;)V

    .line 260
    .end local v3    # "mcontext":Landroid/content/Context;
    :goto_0
    return-void

    .line 221
    :cond_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtGetLocationCount()I

    move-result v0

    .line 222
    .local v0, "count":I
    const-string v4, "not USER_OWNER"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 223
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.dsm.DM_FIND_MY_PHONE"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 225
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTGpsManagerService;->xamtGetLocationManager()Landroid/location/LocationManager;

    move-result-object v2

    .line 226
    .local v2, "lm":Landroid/location/LocationManager;
    if-eqz v2, :cond_3

    .line 228
    const-string v4, "gps"

    invoke-virtual {v2, v4}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v4

    if-nez v4, :cond_2

    .line 230
    const-string v4, "not support gps"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 231
    if-nez v0, :cond_1

    .line 233
    const-string v4, "NetOnly"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 253
    :goto_1
    const-string v4, "LocCount"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 254
    const/16 v4, 0x20

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 255
    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p1, v1, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 256
    const-string v4, "Send Broadcast : XCOMMON_INTENT_GET_LOCATION"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 258
    new-instance v4, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;

    invoke-direct {v4}, Lcom/fmm/dm/agent/amt/XAMTWaitTimer;-><init>()V

    sput-object v4, Lcom/fmm/dm/agent/amt/XAMTGpsManager;->mXAMTWaitTimer:Lcom/fmm/dm/agent/amt/XAMTWaitTimer;

    goto :goto_0

    .line 237
    :cond_1
    const-string v4, "not check Location"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 238
    invoke-static {v5}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocation(Z)V

    goto :goto_0

    .line 244
    :cond_2
    const-string v4, "NetOnly"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    .line 249
    :cond_3
    const-string v4, "Location Manager is Null!!!"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 250
    invoke-static {v5}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSetGetLocation(Z)V

    goto :goto_1
.end method
