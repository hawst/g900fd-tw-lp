.class public Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;
.super Ljava/util/TimerTask;
.source "XLAWMOWaitTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "XLAWMODelayTimerTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;


# direct methods
.method public constructor <init>(Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;->this$0:Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 44
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 45
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetLawmoOperation()I

    move-result v0

    const/16 v1, 0xf

    if-ne v0, v1, :cond_1

    .line 47
    const-string v0, "ReActive Operation intent not received"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;->this$0:Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;

    # getter for: Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->access$000(Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer$XLAWMODelayTimerTask;->this$0:Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;

    const/4 v1, 0x0

    # setter for: Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->mReActiveWaitTimer:Ljava/util/Timer;
    invoke-static {v0, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;->access$002(Lcom/fmm/dm/agent/lawmo/XLAWMOWaitTimer;Ljava/util/Timer;)Ljava/util/Timer;

    .line 52
    :cond_0
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoSetLawmoOperation(I)V

    .line 53
    const/4 v0, 0x1

    const-string v1, "1475"

    invoke-static {v0, v1}, Lcom/fmm/dm/agent/lawmo/XLAWMOAgent;->xlawmoAgentExecuteResultReport(ZLjava/lang/String;)V

    .line 59
    :goto_0
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 60
    return-void

    .line 57
    :cond_1
    const-string v0, "Not Operation"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method
