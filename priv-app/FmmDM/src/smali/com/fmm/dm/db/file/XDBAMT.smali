.class public Lcom/fmm/dm/db/file/XDBAMT;
.super Ljava/lang/Object;
.source "XDBAMT.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XDBInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static LocationCount:I = 0x0

.field private static gTrackingOperation:I = 0x0

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    sput v0, Lcom/fmm/dm/db/file/XDBAMT;->gTrackingOperation:I

    .line 16
    sput v0, Lcom/fmm/dm/db/file/XDBAMT;->LocationCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbAmtGetDBOperation()I
    .locals 5

    .prologue
    .line 480
    const/4 v2, 0x0

    .line 481
    .local v2, "nOperation":I
    const-string v0, ""

    .line 485
    .local v0, "Obj":Ljava/lang/String;
    const/16 v3, 0x13b

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 492
    .end local v0    # "Obj":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 494
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 495
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xdbMobileTrackingDbGetOperation "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 497
    :cond_0
    return v2

    .line 487
    .restart local v0    # "Obj":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 489
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsAltitude()Ljava/lang/String;
    .locals 4

    .prologue
    .line 174
    const-string v2, ""

    .line 177
    .local v2, "szGpsAltitude":Ljava/lang/String;
    const/16 v3, 0x130

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :goto_0
    return-object v2

    .line 179
    :catch_0
    move-exception v1

    .line 181
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsCourse()Ljava/lang/String;
    .locals 4

    .prologue
    .line 226
    const-string v2, ""

    .line 229
    .local v2, "szGpsCourse":Ljava/lang/String;
    const/16 v3, 0x132

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :goto_0
    return-object v2

    .line 231
    :catch_0
    move-exception v1

    .line 233
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsDate()Ljava/lang/String;
    .locals 4

    .prologue
    .line 304
    const-string v2, ""

    .line 307
    .local v2, "szGpsDate":Ljava/lang/String;
    const/16 v3, 0x135

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_0
    return-object v2

    .line 309
    :catch_0
    move-exception v1

    .line 311
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsHorizontalUncertainty()Ljava/lang/String;
    .locals 4

    .prologue
    .line 252
    const-string v2, ""

    .line 255
    .local v2, "szGpsHorizontalUncertainty":Ljava/lang/String;
    const/16 v3, 0x133

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :goto_0
    return-object v2

    .line 257
    :catch_0
    move-exception v1

    .line 259
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsIsValid()Ljava/lang/String;
    .locals 4

    .prologue
    .line 96
    const-string v2, ""

    .line 99
    .local v2, "szGpsIsValid":Ljava/lang/String;
    const/16 v3, 0x12d

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    return-object v2

    .line 101
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsLatitude()Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    const-string v2, ""

    .line 125
    .local v2, "szGpsLatitude":Ljava/lang/String;
    const/16 v3, 0x12e

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    return-object v2

    .line 127
    :catch_0
    move-exception v1

    .line 129
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsLongitude()Ljava/lang/String;
    .locals 4

    .prologue
    .line 148
    const-string v2, ""

    .line 151
    .local v2, "szGpsLongitude":Ljava/lang/String;
    const/16 v3, 0x12f

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_0
    return-object v2

    .line 153
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsSpeed()Ljava/lang/String;
    .locals 4

    .prologue
    .line 200
    const-string v2, ""

    .line 203
    .local v2, "szGpsSpeed":Ljava/lang/String;
    const/16 v3, 0x131

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_0
    return-object v2

    .line 205
    :catch_0
    move-exception v1

    .line 207
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetGpsVerticalUncertainty()Ljava/lang/String;
    .locals 4

    .prologue
    .line 278
    const-string v2, ""

    .line 281
    .local v2, "szGpsVerticalUncertainty":Ljava/lang/String;
    const/16 v3, 0x134

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :goto_0
    return-object v2

    .line 283
    :catch_0
    move-exception v1

    .line 285
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetInfo(Ljava/lang/Object;)Lcom/fmm/dm/db/file/XDBAMTInfo;
    .locals 4
    .param p0, "Obj"    # Ljava/lang/Object;

    .prologue
    .line 69
    move-object v1, p0

    check-cast v1, Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 72
    .local v1, "MobileTrackingInfo":Lcom/fmm/dm/db/file/XDBAMTInfo;
    const/16 v3, 0x12c

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/fmm/dm/db/file/XDBAMTInfo;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-object v1

    .line 74
    :catch_0
    move-exception v2

    .line 76
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetLocationCount()I
    .locals 2

    .prologue
    .line 454
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Get Location Count "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/db/file/XDBAMT;->LocationCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 455
    sget v0, Lcom/fmm/dm/db/file/XDBAMT;->LocationCount:I

    return v0
.end method

.method public static xdbAmtGetMobileTrackingStatus()Ljava/lang/String;
    .locals 5

    .prologue
    .line 708
    const-string v3, ""

    .line 709
    .local v3, "szMobileTrackingStatus":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v1

    .line 711
    .local v1, "bAMTStatus":Z
    if-nez v1, :cond_0

    .line 712
    const-string v3, "30"

    .line 722
    :goto_0
    return-object v3

    .line 716
    :cond_0
    const/16 v4, 0x143

    :try_start_0
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 718
    :catch_0
    move-exception v2

    .line 720
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetNonGpsCellId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 356
    const-string v2, ""

    .line 359
    .local v2, "szNonGpsCellId":Ljava/lang/String;
    const/16 v3, 0x137

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :goto_0
    return-object v2

    .line 361
    :catch_0
    move-exception v1

    .line 363
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetNonGpsIsValid()Ljava/lang/String;
    .locals 4

    .prologue
    .line 330
    const-string v2, ""

    .line 333
    .local v2, "szNonGpsIsValid":Ljava/lang/String;
    const/16 v3, 0x136

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_0
    return-object v2

    .line 335
    :catch_0
    move-exception v1

    .line 337
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetNonGpsLAC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 382
    const-string v2, ""

    .line 385
    .local v2, "szNonGpsLAC":Ljava/lang/String;
    const/16 v3, 0x138

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    :goto_0
    return-object v2

    .line 387
    :catch_0
    move-exception v1

    .line 389
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetNonGpsMCC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 434
    const-string v2, ""

    .line 437
    .local v2, "szNonGpsMCC":Ljava/lang/String;
    const/16 v3, 0x13a

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    :goto_0
    return-object v2

    .line 439
    :catch_0
    move-exception v1

    .line 441
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetNonGpsMNC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 408
    const-string v2, ""

    .line 411
    .local v2, "szNonGpsMNC":Ljava/lang/String;
    const/16 v3, 0x139

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    :goto_0
    return-object v2

    .line 413
    :catch_0
    move-exception v1

    .line 415
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetOperation()I
    .locals 2

    .prologue
    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdbMobileTrackingGetOperation "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/db/file/XDBAMT;->gTrackingOperation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 475
    sget v0, Lcom/fmm/dm/db/file/XDBAMT;->gTrackingOperation:I

    return v0
.end method

.method public static xdbAmtGetPolicyEndDate()Ljava/lang/String;
    .locals 4

    .prologue
    .line 656
    const-string v2, ""

    .line 659
    .local v2, "szPolicyEndDate":Ljava/lang/String;
    const/16 v3, 0x141

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 665
    :goto_0
    return-object v2

    .line 661
    :catch_0
    move-exception v1

    .line 663
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetPolicyInterval()Ljava/lang/String;
    .locals 4

    .prologue
    .line 682
    const-string v2, ""

    .line 685
    .local v2, "szPolicyInterval":Ljava/lang/String;
    const/16 v3, 0x142

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    :goto_0
    return-object v2

    .line 687
    :catch_0
    move-exception v1

    .line 689
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetPolicyStartDate()Ljava/lang/String;
    .locals 4

    .prologue
    .line 630
    const-string v2, ""

    .line 633
    .local v2, "szPolicyStartDate":Ljava/lang/String;
    const/16 v3, 0x140

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    :goto_0
    return-object v2

    .line 635
    :catch_0
    move-exception v1

    .line 637
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetResultCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 604
    const-string v2, ""

    .line 607
    .local v2, "szResultCode":Ljava/lang/String;
    const/16 v3, 0x13f

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 613
    :goto_0
    return-object v2

    .line 609
    :catch_0
    move-exception v1

    .line 611
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetResultReportSourceUri()Ljava/lang/String;
    .locals 4

    .prologue
    .line 552
    const-string v2, ""

    .line 555
    .local v2, "szSourceUri":Ljava/lang/String;
    const/16 v3, 0x13d

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    :goto_0
    return-object v2

    .line 557
    :catch_0
    move-exception v1

    .line 559
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetResultReportTargetUri()Ljava/lang/String;
    .locals 4

    .prologue
    .line 578
    const-string v2, ""

    .line 581
    .local v2, "szTargetUri":Ljava/lang/String;
    const/16 v3, 0x13e

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    :goto_0
    return-object v2

    .line 583
    :catch_0
    move-exception v1

    .line 585
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtGetResultReportType()I
    .locals 5

    .prologue
    .line 514
    const/4 v2, 0x1

    .line 515
    .local v2, "nResultReportType":I
    const/4 v0, 0x0

    .line 519
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0x13c

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 526
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    .line 528
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 529
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nResultReportType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 535
    :goto_1
    return v2

    .line 521
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 523
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 533
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static xdbAmtInitNVM(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "NVMMobileTrackingInfo"    # Ljava/lang/Object;

    .prologue
    .line 20
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 22
    .local v0, "MobileTrackingInfo":Lcom/fmm/dm/db/file/XDBAMTInfo;
    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/fmm/dm/db/file/XDBAMTInfo;

    .end local v0    # "MobileTrackingInfo":Lcom/fmm/dm/db/file/XDBAMTInfo;
    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBAMTInfo;-><init>()V

    .line 51
    .restart local v0    # "MobileTrackingInfo":Lcom/fmm/dm/db/file/XDBAMTInfo;
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "Y"

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsIsValid:Ljava/lang/String;

    .line 29
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLatitude:Ljava/lang/String;

    .line 30
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLogitude:Ljava/lang/String;

    .line 31
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsHorizontalUncertainty:Ljava/lang/String;

    .line 32
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsVerticalUncertainty:Ljava/lang/String;

    .line 33
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsDate:Ljava/lang/String;

    .line 35
    const-string v1, "N"

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsIsValid:Ljava/lang/String;

    .line 36
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsCellId:Ljava/lang/String;

    .line 37
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsLAC:Ljava/lang/String;

    .line 38
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMNC:Ljava/lang/String;

    .line 39
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMCC:Ljava/lang/String;

    .line 41
    const/4 v1, 0x0

    iput v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nOperation:I

    .line 43
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    .line 44
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    .line 45
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultCode:Ljava/lang/String;

    .line 46
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyStartDate:Ljava/lang/String;

    .line 47
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyEndDate:Ljava/lang/String;

    .line 48
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyInterval:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdbAmtSetGpsAltitude(Ljava/lang/String;)V
    .locals 2
    .param p0, "szGpsAltitude"    # Ljava/lang/String;

    .prologue
    .line 164
    const/16 v1, 0x130

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    return-void

    .line 166
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetGpsCourse(Ljava/lang/String;)V
    .locals 2
    .param p0, "szGpsCourse"    # Ljava/lang/String;

    .prologue
    .line 216
    const/16 v1, 0x132

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :goto_0
    return-void

    .line 218
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetGpsDate(Ljava/lang/String;)V
    .locals 2
    .param p0, "szGpsDate"    # Ljava/lang/String;

    .prologue
    .line 294
    const/16 v1, 0x135

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    :goto_0
    return-void

    .line 296
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetGpsHorizontalUncertainty(Ljava/lang/String;)V
    .locals 2
    .param p0, "szGpsHorizontalUncertainty"    # Ljava/lang/String;

    .prologue
    .line 242
    const/16 v1, 0x133

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    :goto_0
    return-void

    .line 244
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetGpsIsValid(Z)V
    .locals 3
    .param p0, "bValid"    # Z

    .prologue
    .line 86
    const/16 v2, 0x12d

    if-eqz p0, :cond_0

    :try_start_0
    const-string v1, "Y"

    :goto_0
    invoke-static {v2, v1}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 92
    :goto_1
    return-void

    .line 86
    :cond_0
    const-string v1, "N"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbAmtSetGpsLatitude(Ljava/lang/String;)V
    .locals 2
    .param p0, "szGpsLatitude"    # Ljava/lang/String;

    .prologue
    .line 112
    const/16 v1, 0x12e

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetGpsLongitude(Ljava/lang/String;)V
    .locals 2
    .param p0, "szGpsLongitude"    # Ljava/lang/String;

    .prologue
    .line 138
    const/16 v1, 0x12f

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetGpsSpeed(Ljava/lang/String;)V
    .locals 2
    .param p0, "szGpsSpeed"    # Ljava/lang/String;

    .prologue
    .line 190
    const/16 v1, 0x131

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :goto_0
    return-void

    .line 192
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetGpsVerticalUncertainty(Ljava/lang/String;)V
    .locals 2
    .param p0, "szGpsVerticalUncertainty"    # Ljava/lang/String;

    .prologue
    .line 268
    const/16 v1, 0x134

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :goto_0
    return-void

    .line 270
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetInfo(Ljava/lang/Object;)V
    .locals 3
    .param p0, "Obj"    # Ljava/lang/Object;

    .prologue
    .line 56
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 59
    .local v0, "MobileTrackingInfo":Lcom/fmm/dm/db/file/XDBAMTInfo;
    const/16 v2, 0x12c

    :try_start_0
    invoke-static {v2, v0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetLocationCount(I)V
    .locals 2
    .param p0, "count"    # I

    .prologue
    .line 448
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set Location Count "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 449
    sput p0, Lcom/fmm/dm/db/file/XDBAMT;->LocationCount:I

    .line 450
    return-void
.end method

.method public static xdbAmtSetMobileTrackingStatus(Ljava/lang/String;)V
    .locals 2
    .param p0, "szMobileTrackingStatus"    # Ljava/lang/String;

    .prologue
    .line 698
    const/16 v1, 0x143

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 704
    :goto_0
    return-void

    .line 700
    :catch_0
    move-exception v0

    .line 702
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetNonGpsCellId(Ljava/lang/String;)V
    .locals 2
    .param p0, "szNonGpsCellId"    # Ljava/lang/String;

    .prologue
    .line 346
    const/16 v1, 0x137

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :goto_0
    return-void

    .line 348
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetNonGpsIsValid(Z)V
    .locals 3
    .param p0, "bValid"    # Z

    .prologue
    .line 320
    const/16 v2, 0x136

    if-eqz p0, :cond_0

    :try_start_0
    const-string v1, "Y"

    :goto_0
    invoke-static {v2, v1}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 326
    :goto_1
    return-void

    .line 320
    :cond_0
    const-string v1, "N"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 322
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbAmtSetNonGpsLAC(Ljava/lang/String;)V
    .locals 2
    .param p0, "szNonGpsLAC"    # Ljava/lang/String;

    .prologue
    .line 372
    const/16 v1, 0x138

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    :goto_0
    return-void

    .line 374
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetNonGpsMCC(Ljava/lang/String;)V
    .locals 2
    .param p0, "szNonGpsMCC"    # Ljava/lang/String;

    .prologue
    .line 424
    const/16 v1, 0x13a

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :goto_0
    return-void

    .line 426
    :catch_0
    move-exception v0

    .line 428
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetNonGpsMNC(Ljava/lang/String;)V
    .locals 2
    .param p0, "szNonGpsMNC"    # Ljava/lang/String;

    .prologue
    .line 398
    const/16 v1, 0x139

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :goto_0
    return-void

    .line 400
    :catch_0
    move-exception v0

    .line 402
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetOperation(I)V
    .locals 3
    .param p0, "nOperation"    # I

    .prologue
    .line 462
    const/16 v1, 0x13b

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 463
    sput p0, Lcom/fmm/dm/db/file/XDBAMT;->gTrackingOperation:I

    .line 464
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nOperation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    :goto_0
    return-void

    .line 466
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetPolicyEndDate(Ljava/lang/String;)V
    .locals 2
    .param p0, "szPolicyEndDate"    # Ljava/lang/String;

    .prologue
    .line 646
    const/16 v1, 0x141

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 652
    :goto_0
    return-void

    .line 648
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetPolicyInterval(Ljava/lang/String;)V
    .locals 2
    .param p0, "szPolicyInterval"    # Ljava/lang/String;

    .prologue
    .line 672
    const/16 v1, 0x142

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    :goto_0
    return-void

    .line 674
    :catch_0
    move-exception v0

    .line 676
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetPolicyStartDate(Ljava/lang/String;)V
    .locals 2
    .param p0, "szPolicyStartDate"    # Ljava/lang/String;

    .prologue
    .line 620
    const/16 v1, 0x140

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 626
    :goto_0
    return-void

    .line 622
    :catch_0
    move-exception v0

    .line 624
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetResultCode(Ljava/lang/String;)V
    .locals 2
    .param p0, "szResultCode"    # Ljava/lang/String;

    .prologue
    .line 594
    const/16 v1, 0x13f

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    :goto_0
    return-void

    .line 596
    :catch_0
    move-exception v0

    .line 598
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetResultReportSourceUri(Ljava/lang/String;)V
    .locals 2
    .param p0, "szSourceUri"    # Ljava/lang/String;

    .prologue
    .line 542
    const/16 v1, 0x13d

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 548
    :goto_0
    return-void

    .line 544
    :catch_0
    move-exception v0

    .line 546
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetResultReportTargetUri(Ljava/lang/String;)V
    .locals 2
    .param p0, "szTargetUri"    # Ljava/lang/String;

    .prologue
    .line 568
    const/16 v1, 0x13e

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 574
    :goto_0
    return-void

    .line 570
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAmtSetResultReportType(I)V
    .locals 3
    .param p0, "nResultReportType"    # I

    .prologue
    .line 504
    const/16 v1, 0x13c

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 510
    :goto_0
    return-void

    .line 506
    :catch_0
    move-exception v0

    .line 508
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method
