.class public Lcom/fmm/dm/db/file/XDBAdapter;
.super Ljava/lang/Object;
.source "XDBAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XTPInterface;


# static fields
.field public static final CON_REF_ACTIVED:Z = true

.field public static final CON_REF_NOT_ACTIVED:Z = false

.field private static final DEFAULT_NULL_ADDRESS:Ljava/lang/String; = "0.0.0.0"

.field private static final DEFAULT_PORT:Ljava/lang/String; = "8080"

.field private static final GPRS_APN1:Ljava/lang/String; = "epc.tmobile.com"

.field private static final GPRS_APN2:Ljava/lang/String; = "devopw.t-mobile.com"

.field private static final GPRS_APN3:Ljava/lang/String; = "internet2.voicestream.com"

.field private static final NET_PROFILE_NAME1:Ljava/lang/String; = "Production"

.field private static final NET_PROFILE_NAME2:Ljava/lang/String; = "OSPS-TestBed"

.field private static final NET_PROFILE_NAME3:Ljava/lang/String; = "mFormation"

.field private static final PROXY_ADDRESS1:Ljava/lang/String; = "0.0.0.0"

.field private static final PROXY_ADDRESS2:Ljava/lang/String; = "0.0.0.0"

.field private static final PROXY_ADDRESS3:Ljava/lang/String; = "0.0.0.0"

.field private static final PROXY_PORT1:I

.field private static final PROXY_PORT2:I

.field private static final PROXY_PORT3:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public static xdbAdpInitNetProfile(Ljava/lang/Object;I)V
    .locals 6
    .param p0, "NVMSyncMLDMInfo"    # Ljava/lang/Object;
    .param p1, "nIndex"    # I

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 52
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 54
    .local v0, "pProfileInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    const-string v1, "DM Profile"

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->NetworkConnName:Ljava/lang/String;

    .line 55
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-boolean v3, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->Active:Z

    .line 56
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/16 v2, 0x11

    iput v2, v1, Lcom/fmm/dm/db/file/XDBConRefNAP;->nBearer:I

    .line 57
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iput v5, v1, Lcom/fmm/dm/db/file/XDBConRefNAP;->nAddrType:I

    .line 58
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iput-boolean v4, v1, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticIpUse:Z

    .line 59
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iput-boolean v4, v1, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticDnsUse:Z

    .line 61
    if-nez p1, :cond_1

    .line 63
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const-string v2, "epc.tmobile.com"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    .line 64
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iput v4, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    .line 65
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iput v3, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    .line 66
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const-string v2, "0.0.0.0"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 67
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput v5, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    .line 68
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-boolean v3, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 69
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const-string v2, "Production"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    if-ne p1, v3, :cond_2

    .line 73
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const-string v2, "devopw.t-mobile.com"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    .line 74
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iput v4, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    .line 75
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iput v3, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    .line 76
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const-string v2, "0.0.0.0"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 77
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput v5, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    .line 78
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-boolean v3, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 79
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const-string v2, "OSPS-TestBed"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    goto :goto_0

    .line 82
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 84
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const-string v2, "internet2.voicestream.com"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    .line 85
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iput v4, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    .line 86
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iput v3, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    .line 87
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const-string v2, "0.0.0.0"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 88
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput v5, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    .line 89
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-boolean v3, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 90
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const-string v2, "mFormation"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdbDoDMBootStrapURI([C[C[C)[C
    .locals 11
    .param p0, "ResultURI"    # [C
    .param p1, "BootURI"    # [C
    .param p2, "BootPort"    # [C

    .prologue
    const/4 v8, 0x0

    .line 491
    const/4 v0, 0x0

    .line 492
    .local v0, "UriLen":I
    const/16 v9, 0x40

    new-array v7, v9, [C

    .line 493
    .local v7, "temp":[C
    const/4 v1, 0x0

    .line 494
    .local v1, "i":I
    const/4 v6, 0x0

    .line 495
    .local v6, "t":I
    const/4 v2, 0x0

    .line 496
    .local v2, "nCount":I
    const/4 v3, 0x0

    .line 498
    .local v3, "nPortCount":I
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object p0, v8

    .line 553
    .end local p0    # "ResultURI":[C
    :goto_0
    return-object p0

    .line 501
    .restart local p0    # "ResultURI":[C
    :cond_1
    array-length v0, p1

    .line 503
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_7

    .line 505
    aget-char v9, p1, v1

    const/16 v10, 0x2f

    if-ne v9, v10, :cond_3

    .line 507
    add-int/lit8 v2, v2, 0x1

    .line 514
    :cond_2
    :goto_2
    const/4 v9, 0x2

    if-ne v3, v9, :cond_4

    .line 516
    move-object p0, p1

    .line 517
    goto :goto_0

    .line 509
    :cond_3
    aget-char v9, p1, v1

    const/16 v10, 0x3a

    if-ne v9, v10, :cond_2

    .line 511
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 520
    :cond_4
    const/4 v9, 0x3

    if-ne v2, v9, :cond_6

    .line 522
    array-length v8, p2

    if-nez v8, :cond_5

    const/4 v8, 0x0

    aget-char v8, p2, v8

    if-nez v8, :cond_5

    .line 524
    const-string v8, "80"

    invoke-virtual {v8}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    .line 525
    invoke-static {v7}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v4

    .line 526
    .local v4, "szArg":Ljava/lang/String;
    const-string v8, ":"

    invoke-virtual {v4, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 527
    invoke-static {p2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 528
    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v5

    .line 529
    .local v5, "szPath":Ljava/lang/String;
    invoke-virtual {v5, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 530
    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 531
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object p0

    .line 533
    goto :goto_0

    .line 537
    .end local v4    # "szArg":Ljava/lang/String;
    .end local v5    # "szPath":Ljava/lang/String;
    :cond_5
    invoke-static {v7}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v4

    .line 538
    .restart local v4    # "szArg":Ljava/lang/String;
    const-string v8, ":"

    invoke-virtual {v4, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 539
    invoke-static {p2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 540
    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v5

    .line 541
    .restart local v5    # "szPath":Ljava/lang/String;
    invoke-virtual {v5, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 542
    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 543
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object p0

    .line 545
    const/4 v6, 0x0

    .line 546
    goto :goto_0

    .line 550
    .end local v4    # "szArg":Ljava/lang/String;
    .end local v5    # "szPath":Ljava/lang/String;
    :cond_6
    aget-char v9, p1, v1

    aput-char v9, v7, v6

    .line 551
    add-int/lit8 v6, v6, 0x1

    .line 503
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    move-object p0, v8

    .line 553
    goto :goto_0
.end method

.method public static xdbFileCreate(Ljava/lang/String;)Z
    .locals 4
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 147
    .local v0, "bRet":Z
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 155
    .end local v2    # "file":Ljava/io/File;
    :goto_0
    return v0

    .line 150
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbFileDelete(Ljava/lang/String;)Z
    .locals 3
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    .line 408
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 410
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 411
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    :cond_0
    const/4 v2, 0x1

    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v2

    .line 413
    :catch_0
    move-exception v0

    .line 415
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 417
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xdbFileWrite(Ljava/lang/String;ILjava/lang/Object;)Z
    .locals 6
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "nSize"    # I
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 310
    check-cast p2, [B

    .end local p2    # "data":Ljava/lang/Object;
    move-object v3, p2

    check-cast v3, [B

    .line 311
    .local v3, "tmp":[B
    const/4 v0, 0x0

    .line 315
    .local v0, "ObjOut":Ljava/io/DataOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .local v1, "ObjOut":Ljava/io/DataOutputStream;
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v1, v3, v5, p1}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 327
    if-eqz v1, :cond_0

    .line 329
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 337
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 338
    const/4 v4, 0x1

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    :cond_1
    :goto_1
    return v4

    .line 332
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_0
    move-exception v2

    .line 334
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 318
    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_1
    move-exception v2

    .line 320
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 327
    if-eqz v0, :cond_1

    .line 329
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 332
    :catch_2
    move-exception v2

    .line 334
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 325
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 327
    :goto_3
    if-eqz v0, :cond_2

    .line 329
    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 335
    :cond_2
    :goto_4
    throw v4

    .line 332
    :catch_3
    move-exception v2

    .line 334
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 325
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    goto :goto_3

    .line 318
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    goto :goto_2
.end method

.method public static xdbFileWrite(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 5
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 343
    const/4 v0, 0x0

    .line 346
    .local v0, "ObjOut":Ljava/io/ObjectOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    .end local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    .local v1, "ObjOut":Ljava/io/ObjectOutputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->reset()V

    .line 348
    invoke-virtual {v1, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 359
    if-eqz v1, :cond_0

    .line 361
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 370
    :cond_0
    :goto_0
    const/4 v3, 0x1

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    :cond_1
    :goto_1
    return v3

    .line 364
    .end local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    :catch_0
    move-exception v2

    .line 366
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 350
    .end local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    :catch_1
    move-exception v2

    .line 352
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 353
    const/4 v3, 0x0

    .line 359
    if-eqz v0, :cond_1

    .line 361
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 364
    :catch_2
    move-exception v2

    .line 366
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 357
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 359
    :goto_3
    if-eqz v0, :cond_2

    .line 361
    :try_start_5
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 367
    :cond_2
    :goto_4
    throw v3

    .line 364
    :catch_3
    move-exception v2

    .line 366
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 357
    .end local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    goto :goto_3

    .line 350
    .end local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    goto :goto_2
.end method

.method public static xdbFolderCreate(Ljava/lang/String;)Z
    .locals 5
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 162
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 163
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 165
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 166
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "make ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] folder"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return v2

    .line 170
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 173
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xdbFolderExist(Ljava/lang/String;)Z
    .locals 4
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 126
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 127
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 129
    const-string v3, "Folder is not Exist!!"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v2

    .line 133
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 138
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static xdbGetConnectType()I
    .locals 3

    .prologue
    .line 444
    const/4 v1, 0x0

    .line 446
    .local v1, "type":I
    const-string v0, ""

    .line 447
    .local v0, "szProtocol":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProtocol()Ljava/lang/String;

    move-result-object v0

    .line 448
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 450
    invoke-static {v0}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpHttpExchangeProtocolType(Ljava/lang/String;)I

    move-result v1

    .line 456
    :goto_0
    return v1

    .line 454
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public static xdbGetNotiEvent(I)I
    .locals 3
    .param p0, "nAppId"    # I

    .prologue
    .line 469
    const/4 v0, 0x0

    .line 471
    .local v0, "nEvent":I
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetNotiEvent()I

    move-result v0

    .line 473
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nEvent :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 474
    return v0
.end method


# virtual methods
.method public xdbFileCreateWrite(Ljava/lang/String;[B)Z
    .locals 5
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "data"    # [B

    .prologue
    .line 375
    const/4 v0, 0x0

    .line 378
    .local v0, "ObjOut":Ljava/io/DataOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .local v1, "ObjOut":Ljava/io/DataOutputStream;
    :try_start_1
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 390
    if-eqz v1, :cond_0

    .line 392
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 401
    :cond_0
    :goto_0
    const/4 v3, 0x1

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    :cond_1
    :goto_1
    return v3

    .line 395
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_0
    move-exception v2

    .line 397
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 381
    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_1
    move-exception v2

    .line 383
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 384
    const/4 v3, 0x0

    .line 390
    if-eqz v0, :cond_1

    .line 392
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 395
    :catch_2
    move-exception v2

    .line 397
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 388
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 390
    :goto_3
    if-eqz v0, :cond_2

    .line 392
    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 398
    :cond_2
    :goto_4
    throw v3

    .line 395
    :catch_3
    move-exception v2

    .line 397
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 388
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    goto :goto_3

    .line 381
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    goto :goto_2
.end method

.method public xdbFileExists(Ljava/lang/String;)I
    .locals 3
    .param p1, "szFileName"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 103
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    const/4 v1, 0x0

    .line 118
    :cond_0
    return v1
.end method

.method public xdbFileGetSize(Ljava/lang/String;)J
    .locals 7
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "ObjIn":Ljava/io/DataInputStream;
    const-wide/16 v4, 0x0

    .line 184
    .local v4, "len":J
    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .local v1, "ObjIn":Ljava/io/DataInputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->available()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    int-to-long v4, v3

    .line 195
    if-eqz v1, :cond_0

    .line 197
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v0, v1

    .line 205
    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    :cond_1
    :goto_0
    return-wide v4

    .line 200
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catch_0
    move-exception v2

    .line 202
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v0, v1

    .line 204
    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_0

    .line 187
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 189
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 195
    if-eqz v0, :cond_1

    .line 197
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 200
    :catch_2
    move-exception v2

    .line 202
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 193
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 195
    :goto_2
    if-eqz v0, :cond_2

    .line 197
    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 203
    :cond_2
    :goto_3
    throw v3

    .line 200
    :catch_3
    move-exception v2

    .line 202
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 193
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_2

    .line 187
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_1
.end method

.method public xdbFileRead(Ljava/lang/String;I)Ljava/lang/Object;
    .locals 6
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "nType"    # I

    .prologue
    .line 242
    const/4 v0, 0x0

    .line 243
    .local v0, "ObjIn":Ljava/io/ObjectInputStream;
    const/4 v3, 0x0

    .line 247
    .local v3, "obj":Ljava/lang/Object;
    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    .end local v0    # "ObjIn":Ljava/io/ObjectInputStream;
    .local v1, "ObjIn":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 258
    if-eqz v1, :cond_0

    .line 260
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v0, v1

    .line 269
    .end local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .end local v3    # "obj":Ljava/lang/Object;
    .restart local v0    # "ObjIn":Ljava/io/ObjectInputStream;
    :cond_1
    :goto_0
    return-object v3

    .line 263
    .end local v0    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v3    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 265
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v0, v1

    .line 267
    .end local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v0    # "ObjIn":Ljava/io/ObjectInputStream;
    goto :goto_0

    .line 250
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 252
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 258
    if-eqz v0, :cond_1

    .line 260
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 263
    :catch_2
    move-exception v2

    .line 265
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 258
    :goto_2
    if-eqz v0, :cond_2

    .line 260
    :try_start_5
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 266
    :cond_2
    :goto_3
    throw v4

    .line 263
    :catch_3
    move-exception v2

    .line 265
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 256
    .end local v0    # "ObjIn":Ljava/io/ObjectInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v0    # "ObjIn":Ljava/io/ObjectInputStream;
    goto :goto_2

    .line 250
    .end local v0    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v0    # "ObjIn":Ljava/io/ObjectInputStream;
    goto :goto_1
.end method

.method public xdbFileRead(Ljava/lang/String;[BII)Z
    .locals 6
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "buffer"    # [B
    .param p3, "offset"    # I
    .param p4, "length"    # I

    .prologue
    const/4 v4, 0x0

    .line 274
    const/4 v3, 0x0

    .line 275
    .local v3, "ret":I
    const/4 v0, 0x0

    .line 276
    .local v0, "ObjIn":Ljava/io/DataInputStream;
    if-gtz p4, :cond_1

    .line 305
    :cond_0
    :goto_0
    return v4

    .line 280
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .local v1, "ObjIn":Ljava/io/DataInputStream;
    :try_start_1
    invoke-virtual {v1, p2, p3, p4}, Ljava/io/DataInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 291
    if-eqz v1, :cond_2

    .line 293
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v0, v1

    .line 302
    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    :cond_3
    :goto_1
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 303
    const/4 v4, 0x1

    goto :goto_0

    .line 296
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catch_0
    move-exception v2

    .line 298
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v0, v1

    .line 300
    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_1

    .line 283
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 285
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 291
    if-eqz v0, :cond_3

    .line 293
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 296
    :catch_2
    move-exception v2

    .line 298
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 289
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 291
    :goto_3
    if-eqz v0, :cond_4

    .line 293
    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 299
    :cond_4
    :goto_4
    throw v4

    .line 296
    :catch_3
    move-exception v2

    .line 298
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 289
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_3

    .line 283
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_2
.end method

.method public xdbFileRead(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    .line 210
    const/4 v1, 0x0

    .line 211
    .local v1, "ObjIn":Ljava/io/ObjectInputStream;
    const/4 v4, 0x0

    .line 215
    .local v4, "objArray":[Ljava/lang/Object;
    :try_start_0
    new-instance v2, Ljava/io/ObjectInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    .end local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .local v2, "ObjIn":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    move-object v0, v5

    check-cast v0, [Ljava/lang/Object;

    move-object v4, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 226
    if-eqz v2, :cond_0

    .line 228
    :try_start_2
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 237
    .end local v2    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    :cond_1
    :goto_0
    return-object v4

    .line 231
    .end local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v2    # "ObjIn":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v3

    .line 233
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v1, v2

    .line 235
    .end local v2    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    goto :goto_0

    .line 218
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 220
    .local v3, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 226
    if-eqz v1, :cond_1

    .line 228
    :try_start_4
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 231
    :catch_2
    move-exception v3

    .line 233
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 224
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 226
    :goto_2
    if-eqz v1, :cond_2

    .line 228
    :try_start_5
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 234
    :cond_2
    :goto_3
    throw v5

    .line 231
    :catch_3
    move-exception v3

    .line 233
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 224
    .end local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v2    # "ObjIn":Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    goto :goto_2

    .line 218
    .end local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v2    # "ObjIn":Ljava/io/ObjectInputStream;
    :catch_4
    move-exception v3

    move-object v1, v2

    .end local v2    # "ObjIn":Ljava/io/ObjectInputStream;
    .restart local v1    # "ObjIn":Ljava/io/ObjectInputStream;
    goto :goto_1
.end method

.method public xdbFileRemove(Ljava/lang/String;)I
    .locals 3
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    .line 428
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 429
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 430
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    :cond_0
    const/4 v2, 0x0

    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v2

    .line 432
    :catch_0
    move-exception v0

    .line 434
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 436
    const/4 v2, -0x1

    goto :goto_0
.end method
