.class public Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;
.super Ljava/lang/Object;
.source "XDBFactoryBootstrapadapter.java"


# static fields
.field private static clientPasswordDict:[B

.field private static default_passwordLen:I

.field private static g_szPasswdDict:[[B

.field private static serverPasswordDict:[B

.field private static szDict:[B

.field private static final xdb_hexTable:[C


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/16 v3, 0xf

    .line 15
    sput v4, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->default_passwordLen:I

    .line 17
    new-array v0, v3, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->clientPasswordDict:[B

    .line 20
    new-array v0, v3, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->serverPasswordDict:[B

    .line 23
    const/4 v0, 0x3

    new-array v0, v0, [[B

    const/4 v1, 0x0

    new-array v2, v3, [B

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [B

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [B

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    sput-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->g_szPasswdDict:[[B

    .line 29
    const/16 v0, 0x28

    new-array v0, v0, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->szDict:[B

    .line 332
    new-array v0, v4, [C

    fill-array-data v0, :array_6

    sput-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdb_hexTable:[C

    return-void

    .line 17
    :array_0
    .array-data 1
        0xet
        0x6t
        0x10t
        0xct
        0xat
        0xet
        0x5t
        0xct
        0x12t
        0xat
        0xbt
        0x6t
        0xdt
        0xet
        0x5t
    .end array-data

    .line 20
    :array_1
    .array-data 1
        0xat
        0x6t
        0xet
        0xet
        0xat
        0xbt
        0x6t
        0xet
        0xbt
        0x4t
        0x4t
        0x7t
        0x11t
        0xct
        0xct
    .end array-data

    .line 23
    :array_2
    .array-data 1
        0xet
        0x6t
        0x10t
        0xct
        0xat
        0xet
        0x5t
        0xct
        0x12t
        0xat
        0xbt
        0x6t
        0xdt
        0xet
        0x5t
    .end array-data

    :array_3
    .array-data 1
        0xat
        0x6t
        0xet
        0xet
        0xat
        0xbt
        0x6t
        0xet
        0xbt
        0x4t
        0x4t
        0x7t
        0x11t
        0xct
        0xct
    .end array-data

    :array_4
    .array-data 1
        0xat
        0x6t
        0x10t
        0xet
        0xat
        0xbt
        0x5t
        0xet
        0x12t
        0x4t
        0xbt
        0x7t
        0xdt
        0xct
        0x5t
    .end array-data

    .line 29
    :array_5
    .array-data 1
        0x1t
        0xft
        0x5t
        0xbt
        0x13t
        0x1ct
        0x17t
        0x2ft
        0x23t
        0x2ct
        0x2t
        0xet
        0x6t
        0xat
        0x12t
        0xdt
        0x16t
        0x1at
        0x20t
        0x2ft
        0x3t
        0xdt
        0x7t
        0x9t
        0x11t
        0x1et
        0x15t
        0x19t
        0x21t
        0x2dt
        0x4t
        0xct
        0x8t
        0x3ft
        0x10t
        0x1ft
        0x14t
        0x18t
        0x22t
        0x2et
    .end array-data

    .line 332
    :array_6
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static xdbFBAdpCingConvertStrToUint64(Ljava/lang/String;I)J
    .locals 14
    .param p0, "szData"    # Ljava/lang/String;
    .param p1, "nBase"    # I

    .prologue
    const/16 v13, 0x30

    const/4 v12, 0x1

    const/4 v11, 0x0

    const-wide/16 v8, 0x0

    .line 216
    const/4 v1, 0x0

    .line 217
    .local v1, "negative":Z
    const-wide/16 v2, 0x0

    .line 218
    .local v2, "i":J
    const/4 v4, 0x0

    .line 219
    .local v4, "s":[C
    const/4 v0, 0x0

    .line 220
    .local v0, "c":C
    const/4 v5, 0x0

    .line 221
    .local v5, "si":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    .line 223
    .local v7, "slength":I
    if-ltz p1, :cond_0

    if-eq p1, v12, :cond_0

    const/16 v10, 0x24

    if-le p1, v10, :cond_1

    .line 300
    :cond_0
    :goto_0
    return-wide v8

    .line 226
    :cond_1
    new-array v4, v7, [C

    .line 227
    invoke-virtual {p0, v11, v7, v4, v11}, Ljava/lang/String;->getChars(II[CI)V

    .line 228
    :goto_1
    aget-char v10, v4, v5

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibIsSpace(C)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 229
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 231
    :cond_2
    if-ge v5, v7, :cond_0

    .line 236
    aget-char v10, v4, v5

    const/16 v11, 0x2d

    if-ne v10, v11, :cond_6

    .line 238
    const/4 v1, 0x1

    .line 239
    add-int/lit8 v5, v5, 0x1

    .line 251
    :goto_2
    aget-char v10, v4, v5

    if-ne v10, v13, :cond_9

    .line 253
    if-eqz p1, :cond_3

    const/16 v10, 0x10

    if-ne p1, v10, :cond_8

    :cond_3
    aget-char v10, v4, v12

    invoke-static {v10}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibToUpper(C)C

    move-result v10

    const/16 v11, 0x58

    if-ne v10, v11, :cond_8

    .line 255
    add-int/lit8 v5, v5, 0x2

    .line 256
    const/16 p1, 0x10

    .line 266
    :cond_4
    :goto_3
    const-wide/16 v2, 0x0

    .line 267
    :goto_4
    array-length v10, v4

    if-ge v5, v10, :cond_5

    .line 269
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "si":I
    .local v6, "si":I
    aget-char v0, v4, v5

    .line 270
    if-nez v4, :cond_a

    move v5, v6

    .line 295
    .end local v6    # "si":I
    .restart local v5    # "si":I
    :cond_5
    :goto_5
    if-eqz v5, :cond_0

    .line 300
    if-eqz v1, :cond_d

    neg-long v8, v2

    goto :goto_0

    .line 241
    :cond_6
    aget-char v10, v4, v5

    const/16 v11, 0x2b

    if-ne v10, v11, :cond_7

    .line 243
    const/4 v1, 0x0

    .line 244
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 248
    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    .line 258
    :cond_8
    if-nez p1, :cond_4

    .line 259
    const/16 p1, 0x8

    goto :goto_3

    .line 261
    :cond_9
    if-nez p1, :cond_4

    .line 263
    const/16 p1, 0xa

    goto :goto_3

    .line 275
    .end local v5    # "si":I
    .restart local v6    # "si":I
    :cond_a
    if-lt v0, v13, :cond_b

    const/16 v10, 0x39

    if-gt v0, v10, :cond_b

    .line 277
    add-int/lit8 v10, v0, -0x30

    int-to-char v0, v10

    .line 288
    :goto_6
    if-lt v0, p1, :cond_c

    move v5, v6

    .line 289
    .end local v6    # "si":I
    .restart local v5    # "si":I
    goto :goto_5

    .line 279
    .end local v5    # "si":I
    .restart local v6    # "si":I
    :cond_b
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibIsAlpha(C)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 281
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibToUpper(C)C

    move-result v10

    add-int/lit8 v10, v10, -0x41

    add-int/lit8 v10, v10, 0xa

    int-to-char v0, v10

    goto :goto_6

    .line 291
    :cond_c
    int-to-long v10, p1

    mul-long/2addr v2, v10

    .line 292
    int-to-long v10, v0

    add-long/2addr v2, v10

    move v5, v6

    .end local v6    # "si":I
    .restart local v5    # "si":I
    goto :goto_4

    :cond_d
    move-wide v8, v2

    .line 300
    goto :goto_0

    .end local v5    # "si":I
    .restart local v6    # "si":I
    :cond_e
    move v5, v6

    .end local v6    # "si":I
    .restart local v5    # "si":I
    goto :goto_5
.end method

.method private static xdbFBAdpCingConvertUint64ToA(J[CI)[C
    .locals 10
    .param p0, "nVal"    # J
    .param p2, "pBuf"    # [C
    .param p3, "nRadix"    # I

    .prologue
    .line 177
    const/4 v3, 0x0

    .line 178
    .local v3, "p":[C
    const/4 v4, 0x0

    .line 179
    .local v4, "pFirstDig":[C
    const/4 v2, 0x0

    .line 180
    .local v2, "nDigVal":I
    const/4 v5, 0x0

    .line 182
    .local v5, "pi":I
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p2}, Ljava/lang/String;-><init>([C)V

    .line 183
    .local v7, "szBuf":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 188
    :cond_0
    int-to-long v8, p3

    rem-long v8, p0, v8

    long-to-int v2, v8

    .line 190
    int-to-long v8, p3

    div-long/2addr p0, v8

    .line 193
    const/16 v8, 0x9

    if-le v2, v8, :cond_1

    .line 196
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "pi":I
    .local v6, "pi":I
    add-int/lit8 v8, v2, -0xa

    add-int/lit8 v8, v8, 0x61

    int-to-char v8, v8

    aput-char v8, v3, v5

    move v5, v6

    .line 203
    .end local v6    # "pi":I
    .restart local v5    # "pi":I
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v8, p0, v8

    if-gtz v8, :cond_0

    .line 205
    array-length v8, v3

    new-array v4, v8, [C

    .line 206
    const/4 v0, 0x0

    .line 207
    .local v0, "i":I
    const/4 v1, 0x0

    .line 208
    .local v1, "j":I
    array-length v8, v3

    add-int/lit8 v0, v8, -0x1

    const/4 v1, 0x0

    :goto_1
    if-ltz v0, :cond_2

    .line 209
    aget-char v8, v3, v0

    aput-char v8, v4, v1

    .line 208
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 201
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_1
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "pi":I
    .restart local v6    # "pi":I
    add-int/lit8 v8, v2, 0x30

    int-to-char v8, v8

    aput-char v8, v3, v5

    move v5, v6

    .end local v6    # "pi":I
    .restart local v5    # "pi":I
    goto :goto_0

    .line 211
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    :cond_2
    return-object v4
.end method

.method public static xdbFBAdpEncodeHex([B)[C
    .locals 8
    .param p0, "data"    # [B

    .prologue
    .line 345
    array-length v4, p0

    .line 346
    .local v4, "len":I
    mul-int/lit8 v6, v4, 0x2

    new-array v5, v6, [C

    .line 347
    .local v5, "output":[C
    const/4 v2, 0x0

    .line 349
    .local v2, "j":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v3, v2

    .end local v2    # "j":I
    .local v3, "j":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 351
    aget-byte v0, p0, v1

    .line 353
    .local v0, "d":B
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    sget-object v6, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdb_hexTable:[C

    and-int/lit8 v7, v0, 0xf

    aget-char v6, v6, v7

    aput-char v6, v5, v3

    .line 354
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .restart local v3    # "j":I
    sget-object v6, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdb_hexTable:[C

    ushr-int/lit8 v7, v0, 0x4

    and-int/lit8 v7, v7, 0xf

    aget-char v6, v6, v7

    aput-char v6, v5, v2

    .line 349
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 357
    .end local v0    # "d":B
    :cond_0
    return-object v5
.end method

.method public static xdbFBAdpGenerateClientPassword(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "passwordLen"    # I

    .prologue
    .line 62
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "szDevid":Ljava/lang/String;
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGenerateClientPasswordKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, "szKey":Ljava/lang/String;
    invoke-static {p0, v1, p1}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePassword(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static xdbFBAdpGenerateClientPasswordKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "szDeviceId"    # Ljava/lang/String;

    .prologue
    .line 34
    sget-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->clientPasswordDict:[B

    invoke-static {p0, v0}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGenerateKeyFromDict(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static xdbFBAdpGenerateKeyFromDict(Ljava/lang/String;[B)Ljava/lang/String;
    .locals 10
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "dict"    # [B

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 45
    .local v0, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 46
    .local v1, "length":I
    const-wide/16 v2, 0x0

    .local v2, "serial1":J
    const-wide/16 v4, 0x0

    .line 48
    .local v4, "serial2":J
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v7, v1, -0x3

    if-ge v0, v7, :cond_0

    .line 50
    add-int/lit8 v7, v0, 0x3

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    int-to-byte v7, v7

    aget-byte v8, p1, v0

    mul-int/2addr v7, v8

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 51
    add-int/lit8 v7, v0, 0x3

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    int-to-byte v7, v7

    add-int/lit8 v8, v0, 0x2

    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    int-to-byte v8, v8

    mul-int/2addr v7, v8

    aget-byte v8, p1, v0

    mul-int/2addr v7, v8

    int-to-long v8, v7

    add-long/2addr v4, v8

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 55
    .local v6, "szTemp":Ljava/lang/String;
    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 56
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 57
    return-object v6
.end method

.method public static xdbFBAdpGeneratePasswd(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 21
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "szServerId"    # Ljava/lang/String;
    .param p2, "nKeyType"    # I

    .prologue
    .line 363
    const-string v15, ""

    .line 364
    .local v15, "szKey":Ljava/lang/String;
    const/4 v11, 0x0

    .line 365
    .local v11, "szBuf":[B
    const-string v17, ""

    .line 367
    .local v17, "szTmpBuf":Ljava/lang/String;
    const/4 v6, 0x0

    .line 368
    .local v6, "devidSize":I
    const/4 v4, 0x0

    .line 369
    .local v4, "cnt":I
    const-string v16, ""

    .line 370
    .local v16, "szTempPasswd":Ljava/lang/String;
    const-string v13, ""

    .line 372
    .local v13, "szDest":Ljava/lang/String;
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePasswdKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v15

    .line 374
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 375
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    new-array v11, v0, [B

    .line 378
    :try_start_0
    const-string v18, "UTF-8"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 387
    :goto_0
    :try_start_1
    const-string v18, "MD5"

    invoke-static/range {v18 .. v18}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v10

    .line 388
    .local v10, "md5Digest":Ljava/security/MessageDigest;
    invoke-virtual {v10}, Ljava/security/MessageDigest;->reset()V

    .line 390
    invoke-virtual {v10, v11}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    .line 391
    .local v7, "digest":[B
    invoke-static {v7}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpEncodeHex([B)[C

    move-result-object v9

    .line 393
    .local v9, "encodedStr1":[C
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 396
    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v19

    add-int/lit8 v20, v6, -0x2

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 397
    const/16 v18, 0x1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v19

    add-int/lit8 v20, v6, -0x1

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 399
    new-instance v5, Lcom/fmm/dm/db/file/XDBCrypt;

    invoke-direct {v5}, Lcom/fmm/dm/db/file/XDBCrypt;-><init>()V

    .line 400
    .local v5, "cryptData":Lcom/fmm/dm/db/file/XDBCrypt;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v11}, Lcom/fmm/dm/db/file/XDBCrypt;->xdbCryptGenerate(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v12

    .line 401
    .local v12, "szCrypteDeviceID":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v12

    .line 404
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v3, v0, [C

    .line 405
    .local v3, "cBuf":[C
    const/16 v18, 0x0

    const/16 v19, 0x2

    aget-char v19, v9, v19

    aput-char v19, v3, v18

    .line 406
    const/16 v18, 0x1

    const/16 v19, 0x4

    aget-char v19, v9, v19

    aput-char v19, v3, v18

    .line 407
    const/16 v18, 0x2

    const/16 v19, 0x8

    aget-char v19, v9, v19

    aput-char v19, v3, v18

    .line 409
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v16

    .line 410
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 412
    const/4 v2, 0x0

    .line 413
    .local v2, "SBuffer":Ljava/lang/StringBuffer;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 416
    if-nez p2, :cond_0

    .line 418
    const/4 v4, 0x0

    :goto_1
    const/16 v18, 0x3

    move/from16 v0, v18

    if-ge v4, v0, :cond_2

    .line 420
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 418
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 380
    .end local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .end local v3    # "cBuf":[C
    .end local v5    # "cryptData":Lcom/fmm/dm/db/file/XDBCrypt;
    .end local v7    # "digest":[B
    .end local v9    # "encodedStr1":[C
    .end local v10    # "md5Digest":Ljava/security/MessageDigest;
    .end local v12    # "szCrypteDeviceID":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 382
    .local v8, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v8}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 424
    .end local v8    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .restart local v3    # "cBuf":[C
    .restart local v5    # "cryptData":Lcom/fmm/dm/db/file/XDBCrypt;
    .restart local v7    # "digest":[B
    .restart local v9    # "encodedStr1":[C
    .restart local v10    # "md5Digest":Ljava/security/MessageDigest;
    .restart local v12    # "szCrypteDeviceID":Ljava/lang/String;
    :cond_0
    const/16 v18, 0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 426
    const/4 v4, 0x0

    :goto_2
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ge v4, v0, :cond_2

    .line 428
    :try_start_2
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 426
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 434
    :cond_1
    const/4 v4, 0x0

    :goto_3
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ge v4, v0, :cond_2

    .line 436
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 434
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 440
    :cond_2
    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .end local v13    # "szDest":Ljava/lang/String;
    .local v14, "szDest":Ljava/lang/String;
    move-object v13, v14

    .line 448
    .end local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .end local v3    # "cBuf":[C
    .end local v5    # "cryptData":Lcom/fmm/dm/db/file/XDBCrypt;
    .end local v7    # "digest":[B
    .end local v9    # "encodedStr1":[C
    .end local v10    # "md5Digest":Ljava/security/MessageDigest;
    .end local v12    # "szCrypteDeviceID":Ljava/lang/String;
    .end local v14    # "szDest":Ljava/lang/String;
    .restart local v13    # "szDest":Ljava/lang/String;
    :goto_4
    return-object v13

    .line 443
    :catch_1
    move-exception v8

    .line 445
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4
.end method

.method private static xdbFBAdpGeneratePasswdKey(Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "type"    # I

    .prologue
    .line 453
    const/4 v1, 0x0

    .line 454
    .local v1, "i":I
    const/4 v2, 0x0

    .line 455
    .local v2, "nLen":I
    const/4 v4, 0x0

    .local v4, "serial1":I
    const/4 v5, 0x0

    .line 456
    .local v5, "serial2":I
    const-string v6, ""

    .line 458
    .local v6, "szKeyStr":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 459
    new-array v3, v2, [B

    .line 462
    .local v3, "pDeviceId":[B
    :try_start_0
    const-string v7, "UTF-8"

    invoke-virtual {p0, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 469
    :goto_0
    const/4 v1, 0x0

    :goto_1
    add-int/lit8 v7, v2, -0x3

    if-ge v1, v7, :cond_0

    .line 471
    add-int/lit8 v7, v1, 0x3

    aget-byte v7, v3, v7

    sget-object v8, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->g_szPasswdDict:[[B

    aget-object v8, v8, p1

    aget-byte v8, v8, v1

    mul-int/2addr v7, v8

    add-int/2addr v4, v7

    .line 472
    add-int/lit8 v7, v1, 0x3

    aget-byte v7, v3, v7

    add-int/lit8 v8, v1, 0x2

    aget-byte v8, v3, v8

    mul-int/2addr v7, v8

    sget-object v8, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->g_szPasswdDict:[[B

    aget-object v8, v8, p1

    aget-byte v8, v8, v1

    mul-int/2addr v7, v8

    add-int/2addr v5, v7

    .line 469
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 464
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 475
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 476
    return-object v6
.end method

.method private static xdbFBAdpGeneratePassword(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 15
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "szKey"    # Ljava/lang/String;
    .param p2, "passwordLen"    # I

    .prologue
    .line 92
    const/4 v8, 0x0

    .line 93
    .local v8, "szDevid":Ljava/lang/String;
    const/4 v10, 0x0

    .line 94
    .local v10, "szRet":Ljava/lang/String;
    const/16 v13, 0x19

    move/from16 v0, p2

    if-ne v13, v0, :cond_0

    .line 96
    const/4 v13, 0x0

    .line 137
    :goto_0
    return-object v13

    .line 99
    :cond_0
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v8

    .line 101
    invoke-static {v8}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGet36BasedDevID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 102
    .local v9, "szNewDevId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 103
    .local v1, "SBuffer":Ljava/lang/StringBuffer;
    move-object v7, v8

    .line 104
    .local v7, "szBuffer":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 105
    invoke-virtual {v7, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 109
    :try_start_0
    const-string v13, "MD5"

    invoke-static {v13}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    .line 110
    .local v6, "md5Digest":Ljava/security/MessageDigest;
    invoke-virtual {v6}, Ljava/security/MessageDigest;->reset()V

    .line 111
    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v6, v13}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    .line 112
    .local v2, "digest":[B
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpEncodeHex([B)[C

    move-result-object v4

    .line 114
    .local v4, "encodedStr1":[C
    const/4 v13, 0x6

    new-array v5, v13, [C

    .line 116
    .local v5, "encodedStr2":[C
    const/4 v13, 0x0

    const/4 v14, 0x2

    aget-char v14, v4, v14

    aput-char v14, v5, v13

    .line 117
    const/4 v13, 0x1

    const/4 v14, 0x7

    aget-char v14, v4, v14

    aput-char v14, v5, v13

    .line 118
    const/4 v13, 0x2

    const/16 v14, 0x8

    aget-char v14, v4, v14

    aput-char v14, v5, v13

    .line 119
    const/4 v13, 0x3

    const/16 v14, 0xc

    aget-char v14, v4, v14

    aput-char v14, v5, v13

    .line 120
    const/4 v13, 0x4

    const/16 v14, 0x19

    aget-char v14, v4, v14

    aput-char v14, v5, v13

    .line 121
    const/4 v13, 0x5

    const/16 v14, 0x1e

    aget-char v14, v4, v14

    aput-char v14, v5, v13

    .line 123
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    .line 124
    .local v12, "szTmpPassword":Ljava/lang/String;
    invoke-virtual {v12, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 126
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 127
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 128
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 129
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 131
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v1}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v10    # "szRet":Ljava/lang/String;
    .local v11, "szRet":Ljava/lang/String;
    move-object v10, v11

    .end local v2    # "digest":[B
    .end local v4    # "encodedStr1":[C
    .end local v5    # "encodedStr2":[C
    .end local v6    # "md5Digest":Ljava/security/MessageDigest;
    .end local v11    # "szRet":Ljava/lang/String;
    .end local v12    # "szTmpPassword":Ljava/lang/String;
    .restart local v10    # "szRet":Ljava/lang/String;
    :goto_1
    move-object v13, v10

    .line 137
    goto :goto_0

    .line 133
    :catch_0
    move-exception v3

    .line 135
    .local v3, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v3}, Ljava/security/NoSuchAlgorithmException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbFBAdpGeneratePswdByID(ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "nTypePwd"    # I
    .param p1, "szId"    # Ljava/lang/String;

    .prologue
    .line 76
    const-string v1, ""

    .line 77
    .local v1, "szPasswd":Ljava/lang/String;
    sget v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->default_passwordLen:I

    .line 79
    .local v0, "passwordLen":I
    const/4 v2, 0x2

    if-ne p0, v2, :cond_1

    .line 81
    invoke-static {p1, v0}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGenerateClientPassword(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 87
    :cond_0
    :goto_0
    return-object v1

    .line 83
    :cond_1
    if-nez p0, :cond_0

    .line 85
    invoke-static {p1, v0}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGenerateServerPassword(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static xdbFBAdpGenerateServerPassword(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "passwordLen"    # I

    .prologue
    .line 69
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "szDevid":Ljava/lang/String;
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGenerateServerPasswordKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "szKey":Ljava/lang/String;
    invoke-static {p0, v1, p1}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePassword(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static xdbFBAdpGenerateServerPasswordKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "szDeviceId"    # Ljava/lang/String;

    .prologue
    .line 39
    sget-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->serverPasswordDict:[B

    invoke-static {p0, v0}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGenerateKeyFromDict(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static xdbFBAdpGet36BasedDevID(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "szDeviceID"    # Ljava/lang/String;

    .prologue
    .line 142
    const-wide/16 v2, 0x0

    .line 143
    .local v2, "nIndex":J
    const/4 v1, 0x0

    .line 144
    .local v1, "i":I
    const/4 v7, 0x0

    .line 145
    .local v7, "tmpdevid":[C
    const/4 v4, 0x0

    .line 146
    .local v4, "nLength":I
    const/16 v0, 0xa

    .line 149
    .local v0, "TEMP_DEVID_SIZE":I
    new-array v5, v0, [C

    .line 150
    .local v5, "pBase36String":[C
    new-array v7, v0, [C

    .line 152
    const/16 v8, 0xa

    invoke-static {p0, v8}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpCingConvertStrToUint64(Ljava/lang/String;I)J

    move-result-wide v2

    .line 153
    const/16 v8, 0x24

    invoke-static {v2, v3, v7, v8}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpCingConvertUint64ToA(J[CI)[C

    move-result-object v7

    .line 155
    array-length v4, v7

    .line 157
    if-ge v4, v0, :cond_1

    .line 159
    const/4 v1, 0x0

    :goto_0
    sub-int v8, v0, v4

    if-ge v1, v8, :cond_0

    .line 161
    const/16 v8, 0x30

    aput-char v8, v5, v1

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v7}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 172
    .local v6, "szTmpDevid":Ljava/lang/String;
    :goto_1
    return-object v6

    .line 168
    .end local v6    # "szTmpDevid":Ljava/lang/String;
    :cond_1
    move-object v5, v7

    .line 169
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v5}, Ljava/lang/String;-><init>([C)V

    .restart local v6    # "szTmpDevid":Ljava/lang/String;
    goto :goto_1
.end method

.method private static xdbFBAdpOspGenerateDevPwdKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "szDeviceId"    # Ljava/lang/String;

    .prologue
    .line 481
    const-string v5, ""

    .line 482
    .local v5, "szKey":Ljava/lang/String;
    const/4 v11, 0x0

    .line 485
    .local v11, "szToken":Ljava/lang/String;
    const/4 v4, 0x0

    .line 486
    .local v4, "searchSlash":I
    const-wide/16 v6, 0x0

    .local v6, "serialNum1":J
    const-wide/16 v8, 0x0

    .line 487
    .local v8, "serialNum2":J
    const/4 v10, 0x0

    .line 488
    .local v10, "szTmp":Ljava/lang/String;
    const/16 v12, 0x40

    new-array v0, v12, [C

    .line 490
    .local v0, "filterNum":[C
    const/16 v12, 0x3a

    invoke-virtual {p0, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 491
    add-int/lit8 v12, v4, 0x1

    invoke-virtual {p0, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 492
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-nez v12, :cond_0

    .line 493
    const/4 v12, 0x0

    .line 520
    :goto_0
    return-object v12

    .line 495
    :cond_0
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    .line 496
    .local v3, "len":I
    const/4 v2, 0x0

    .line 498
    .local v2, "j":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 500
    invoke-virtual {v11, v1}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibIsAlphaNum(C)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 502
    invoke-virtual {v11, v1}, Ljava/lang/String;->charAt(I)C

    move-result v12

    aput-char v12, v0, v2

    .line 503
    add-int/lit8 v2, v2, 0x1

    .line 498
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 506
    :cond_2
    move v3, v2

    .line 508
    const/4 v1, 0x0

    :goto_2
    add-int/lit8 v12, v3, -0x1

    if-ge v1, v12, :cond_3

    .line 510
    aget-char v12, v0, v1

    sget-object v13, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->szDict:[B

    aget-byte v13, v13, v1

    mul-int/2addr v12, v13

    int-to-long v12, v12

    add-long/2addr v6, v12

    .line 511
    aget-char v12, v0, v1

    sub-int v13, v3, v1

    add-int/lit8 v13, v13, -0x1

    aget-char v13, v0, v13

    mul-int/2addr v12, v13

    sget-object v13, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->szDict:[B

    aget-byte v13, v13, v1

    mul-int/2addr v12, v13

    int-to-long v12, v12

    add-long/2addr v8, v12

    .line 508
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 513
    :cond_3
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 514
    invoke-virtual {v5, v10}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 516
    const/4 v10, 0x0

    .line 517
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 518
    invoke-virtual {v5, v10}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v12, v5

    .line 520
    goto :goto_0
.end method

.method public static xdbFBAdpOspGenerateDevicePwd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "szServerId"    # Ljava/lang/String;

    .prologue
    .line 525
    const-string v17, ""

    .line 526
    .local v17, "szTmpBuf":Ljava/lang/String;
    const-string v15, ""

    .line 527
    .local v15, "szKey":Ljava/lang/String;
    const-string v13, ""

    .line 528
    .local v13, "szDevicePw":Ljava/lang/String;
    const-string v12, ""

    .line 529
    .local v12, "szCryptResult":Ljava/lang/String;
    const-string v16, ""

    .line 531
    .local v16, "szTempPasswd":Ljava/lang/String;
    const/4 v11, 0x0

    .line 533
    .local v11, "szBuf":[B
    const/4 v6, 0x0

    .line 534
    .local v6, "devidSize":I
    const/4 v4, 0x0

    .line 536
    .local v4, "cnt":I
    invoke-static/range {p0 .. p0}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpOspGenerateDevPwdKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 537
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 538
    const/16 v18, 0x0

    .line 592
    :goto_0
    return-object v18

    .line 540
    :cond_0
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 541
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    new-array v11, v0, [B

    .line 544
    :try_start_0
    const-string v18, "UTF-8"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 553
    :goto_1
    :try_start_1
    const-string v18, "MD5"

    invoke-static/range {v18 .. v18}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v10

    .line 554
    .local v10, "md5Digest":Ljava/security/MessageDigest;
    invoke-virtual {v10}, Ljava/security/MessageDigest;->reset()V

    .line 556
    invoke-virtual {v10, v11}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    .line 557
    .local v7, "digest":[B
    invoke-static {v7}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpEncodeHex([B)[C

    move-result-object v8

    .line 559
    .local v8, "digestHex":[C
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 562
    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v19

    add-int/lit8 v20, v6, -0x2

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 563
    const/16 v18, 0x1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v19

    add-int/lit8 v20, v6, -0x1

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 565
    new-instance v5, Lcom/fmm/dm/db/file/XDBCrypt;

    invoke-direct {v5}, Lcom/fmm/dm/db/file/XDBCrypt;-><init>()V

    .line 566
    .local v5, "cryptData":Lcom/fmm/dm/db/file/XDBCrypt;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v11}, Lcom/fmm/dm/db/file/XDBCrypt;->xdbCryptGenerate(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v12

    .line 567
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v12

    .line 569
    const/16 v18, 0x4

    move/from16 v0, v18

    new-array v3, v0, [C

    .line 570
    .local v3, "cBuf":[C
    const/16 v18, 0x0

    const/16 v19, 0x1

    aget-char v19, v8, v19

    aput-char v19, v3, v18

    .line 571
    const/16 v18, 0x1

    const/16 v19, 0x4

    aget-char v19, v8, v19

    aput-char v19, v3, v18

    .line 572
    const/16 v18, 0x2

    const/16 v19, 0x5

    aget-char v19, v8, v19

    aput-char v19, v3, v18

    .line 573
    const/16 v18, 0x3

    const/16 v19, 0x7

    aget-char v19, v8, v19

    aput-char v19, v3, v18

    .line 575
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v16

    .line 576
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 578
    const/4 v2, 0x0

    .line 579
    .local v2, "SBuffer":Ljava/lang/StringBuffer;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 581
    const/4 v4, 0x0

    :goto_2
    const/16 v18, 0x3

    move/from16 v0, v18

    if-ge v4, v0, :cond_1

    .line 583
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 581
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 546
    .end local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .end local v3    # "cBuf":[C
    .end local v5    # "cryptData":Lcom/fmm/dm/db/file/XDBCrypt;
    .end local v7    # "digest":[B
    .end local v8    # "digestHex":[C
    .end local v10    # "md5Digest":Ljava/security/MessageDigest;
    :catch_0
    move-exception v9

    .line 548
    .local v9, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v9}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 585
    .end local v9    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .restart local v3    # "cBuf":[C
    .restart local v5    # "cryptData":Lcom/fmm/dm/db/file/XDBCrypt;
    .restart local v7    # "digest":[B
    .restart local v8    # "digestHex":[C
    .restart local v10    # "md5Digest":Ljava/security/MessageDigest;
    :cond_1
    :try_start_2
    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .end local v13    # "szDevicePw":Ljava/lang/String;
    .local v14, "szDevicePw":Ljava/lang/String;
    move-object v13, v14

    .end local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .end local v3    # "cBuf":[C
    .end local v5    # "cryptData":Lcom/fmm/dm/db/file/XDBCrypt;
    .end local v7    # "digest":[B
    .end local v8    # "digestHex":[C
    .end local v10    # "md5Digest":Ljava/security/MessageDigest;
    .end local v14    # "szDevicePw":Ljava/lang/String;
    .restart local v13    # "szDevicePw":Ljava/lang/String;
    :goto_3
    move-object/from16 v18, v13

    .line 592
    goto/16 :goto_0

    .line 587
    :catch_1
    move-exception v9

    .line 589
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 7
    .param p0, "buffer"    # Ljava/lang/StringBuffer;

    .prologue
    .line 305
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    .line 306
    .local v4, "nLen":I
    rem-int/lit8 v2, v4, 0x2

    .line 307
    .local v2, "mod":I
    if-nez v2, :cond_0

    div-int/lit8 v5, v4, 0x2

    .line 309
    .local v5, "secondHalfPos":I
    :goto_0
    move v1, v5

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_2

    .line 311
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 312
    .local v0, "ch":C
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 313
    if-nez v2, :cond_1

    sub-int v6, v4, v1

    add-int/lit8 v3, v6, -0x1

    .line 314
    .local v3, "nInsertPos":I
    :goto_2
    invoke-virtual {p0, v3, v0}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 309
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 307
    .end local v0    # "ch":C
    .end local v1    # "i":I
    .end local v3    # "nInsertPos":I
    .end local v5    # "secondHalfPos":I
    :cond_0
    div-int/lit8 v6, v4, 0x2

    add-int/lit8 v5, v6, 0x1

    goto :goto_0

    .line 313
    .restart local v0    # "ch":C
    .restart local v1    # "i":I
    .restart local v5    # "secondHalfPos":I
    :cond_1
    sub-int v3, v4, v1

    goto :goto_2

    .line 324
    .end local v0    # "ch":C
    :cond_2
    return-object p0
.end method
