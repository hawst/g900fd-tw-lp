.class public interface abstract Lcom/fmm/dm/db/sql/XLAWMODbSql;
.super Ljava/lang/Object;
.source "XLAWMODbSql.java"


# static fields
.field public static final XLAWMO_DB_SQL_AMT_STATUS:Ljava/lang/String; = "AmtStatus"

.field public static final XLAWMO_DB_SQL_CALLRESTRICTION_PHONENUMBER:Ljava/lang/String; = "CallRestrictionPhoneNumber"

.field public static final XLAWMO_DB_SQL_CALLRESTRICTION_STATUS:Ljava/lang/String; = "CallRestrictionStatus"

.field public static final XLAWMO_DB_SQL_CORRELATOR:Ljava/lang/String; = "Correlator"

.field public static final XLAWMO_DB_SQL_EXTERNAL_MEM_TABLE:Ljava/lang/String; = "ExternalMem"

.field public static final XLAWMO_DB_SQL_EXTERNAL_MEM_TABLE_CREATE:Ljava/lang/String; = "create table if not exists ExternalMem (rowid integer primary key autoincrement, ListItemName text, ToBeWiped boolean);"

.field public static final XLAWMO_DB_SQL_EXTOPERATION:Ljava/lang/String; = "ExtOperation"

.field public static final XLAWMO_DB_SQL_INFO_TABLE:Ljava/lang/String; = "LAWMO"

.field public static final XLAWMO_DB_SQL_INFO_TABLE_CREATE:Ljava/lang/String; = "create table if not exists LAWMO (rowid integer primary key autoincrement, State integer, NotifyUser boolean, Operation integer, ExtOperation integer, Pin text, ResultReportType integer, ResultReportSourceUri text, ResultReportTargetUri text, ResultCode text, AmtStatus integer, Registration boolean, SimChangeState boolean, Password text, LockMyPhoneMessages text, ReactivationLock text, CallRestrictionPhoneNumber text, CallRestrictionStatus text, RingMyPhoneStatus text, RingMyPhoneMessages text, Correlator text);"

.field public static final XLAWMO_DB_SQL_INTERNAL_MEM_TABLE:Ljava/lang/String; = "InternalMem"

.field public static final XLAWMO_DB_SQL_INTERNAL_MEM_TABLE_CREATE:Ljava/lang/String; = "create table if not exists InternalMem (rowid integer primary key autoincrement, ListItemName text, ToBeWiped boolean);"

.field public static final XLAWMO_DB_SQL_LIST_ITEM_NAME:Ljava/lang/String; = "ListItemName"

.field public static final XLAWMO_DB_SQL_LOCKMYPHONE_MESSAGES:Ljava/lang/String; = "LockMyPhoneMessages"

.field public static final XLAWMO_DB_SQL_NOTIFY_USER:Ljava/lang/String; = "NotifyUser"

.field public static final XLAWMO_DB_SQL_OPERATION:Ljava/lang/String; = "Operation"

.field public static final XLAWMO_DB_SQL_PASSWORD:Ljava/lang/String; = "Password"

.field public static final XLAWMO_DB_SQL_PIN:Ljava/lang/String; = "Pin"

.field public static final XLAWMO_DB_SQL_REACTIVATIONLOCK:Ljava/lang/String; = "ReactivationLock"

.field public static final XLAWMO_DB_SQL_REGISTRATION:Ljava/lang/String; = "Registration"

.field public static final XLAWMO_DB_SQL_RESULT_CODE:Ljava/lang/String; = "ResultCode"

.field public static final XLAWMO_DB_SQL_RESULT_REPORT_SOURCE_URI:Ljava/lang/String; = "ResultReportSourceUri"

.field public static final XLAWMO_DB_SQL_RESULT_REPORT_TARGET_URI:Ljava/lang/String; = "ResultReportTargetUri"

.field public static final XLAWMO_DB_SQL_RESULT_REPORT_TYPE:Ljava/lang/String; = "ResultReportType"

.field public static final XLAWMO_DB_SQL_RINGMYPHONE_MESSAGES:Ljava/lang/String; = "RingMyPhoneMessages"

.field public static final XLAWMO_DB_SQL_RINGMYPHONE_STATUS:Ljava/lang/String; = "RingMyPhoneStatus"

.field public static final XLAWMO_DB_SQL_SIMCHANGESTATE:Ljava/lang/String; = "SimChangeState"

.field public static final XLAWMO_DB_SQL_STATE:Ljava/lang/String; = "State"

.field public static final XLAWMO_DB_SQL_TO_BE_WIPED:Ljava/lang/String; = "ToBeWiped"
