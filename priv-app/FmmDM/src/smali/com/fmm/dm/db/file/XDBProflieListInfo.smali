.class public Lcom/fmm/dm/db/file/XDBProflieListInfo;
.super Ljava/lang/Object;
.source "XDBProflieListInfo.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public MagicNumber:I

.field public NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

.field public ProfileName:[Ljava/lang/String;

.field public Profileindex:I

.field public m_szNetworkConnName:Ljava/lang/String;

.field public m_szSessionID:Ljava/lang/String;

.field public nDestoryNotiTime:J

.field public nNotiEvent:I

.field public nNotiReSyncMode:I

.field public nProxyIndex:I

.field public tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szNetworkConnName:Ljava/lang/String;

    .line 27
    iput v1, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nProxyIndex:I

    .line 28
    iput v1, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    .line 30
    new-instance v0, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    .line 31
    new-instance v0, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    .line 32
    return-void
.end method
