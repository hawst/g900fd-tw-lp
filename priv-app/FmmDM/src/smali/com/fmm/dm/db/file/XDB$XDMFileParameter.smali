.class final enum Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
.super Ljava/lang/Enum;
.source "XDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/db/file/XDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "XDMFileParameter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/fmm/dm/db/file/XDB$XDMFileParameter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

.field public static final enum FileMax:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

.field public static final enum FileObjectData:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

.field public static final enum FileObjectTreeInfo:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

.field public static final enum FileTndsXmlData:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;


# instance fields
.field private final nFileId:I

.field private final nIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    new-instance v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    const-string v1, "FileObjectTreeInfo"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileObjectTreeInfo:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    .line 68
    new-instance v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    const-string v1, "FileObjectData"

    const/16 v2, 0x101

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileObjectData:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    .line 69
    new-instance v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    const-string v1, "FileTndsXmlData"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileTndsXmlData:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    .line 70
    new-instance v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    const-string v1, "FileMax"

    const/16 v2, 0x103

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileMax:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    .line 65
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    sget-object v1, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileObjectTreeInfo:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileObjectData:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileTndsXmlData:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileMax:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    aput-object v1, v0, v6

    sput-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->$VALUES:[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "nIndex"    # I
    .param p4, "nFileId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput p3, p0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->nIndex:I

    .line 78
    iput p4, p0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->nFileId:I

    .line 79
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    const-class v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    return-object v0
.end method

.method public static values()[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->$VALUES:[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-virtual {v0}, [Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    return-object v0
.end method


# virtual methods
.method FileId()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->nFileId:I

    return v0
.end method

.method Index()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->nIndex:I

    return v0
.end method
