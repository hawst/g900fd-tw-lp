.class public Lcom/fmm/dm/db/file/XDBNvm;
.super Ljava/lang/Object;
.source "XDBNvm.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

.field public NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

.field public NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

.field public NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

.field public NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

.field public NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

.field public NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

.field public NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

.field public tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBProflieListInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 28
    new-instance v0, Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBProfileInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 29
    new-instance v0, Lcom/fmm/dm/db/file/XDBSimInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBSimInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    .line 33
    new-instance v0, Lcom/fmm/dm/db/file/XDBAccXListNode;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBAccXListNode;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    .line 34
    new-instance v0, Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBResyncModeInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .line 39
    new-instance v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBLawmoInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 44
    new-instance v0, Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBAMTInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 47
    new-instance v0, Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBNotiInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    .line 49
    new-instance v0, Lcom/fmm/dm/db/file/XDBAgentInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBAgentInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    .line 50
    return-void
.end method
