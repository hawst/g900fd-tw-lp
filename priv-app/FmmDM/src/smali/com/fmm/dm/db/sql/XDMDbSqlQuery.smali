.class public Lcom/fmm/dm/db/sql/XDMDbSqlQuery;
.super Ljava/lang/Object;
.source "XDMDbSqlQuery.java"

# interfaces
.implements Lcom/fmm/dm/db/sql/XDMDbSql;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmDbDeleteAccXListNodeRow(J)V
    .locals 6
    .param p0, "rowId"    # J

    .prologue
    .line 1089
    const/4 v0, 0x0

    .line 1093
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1094
    const-string v2, "accxlistnode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1102
    if-eqz v0, :cond_0

    .line 1103
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1105
    :cond_0
    :goto_0
    return-void

    .line 1096
    :catch_0
    move-exception v1

    .line 1098
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1102
    if-eqz v0, :cond_0

    .line 1103
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1102
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 1103
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbDeleteNetworkRow(J)V
    .locals 6
    .param p0, "rowId"    # J

    .prologue
    .line 453
    const/4 v0, 0x0

    .line 457
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 458
    const-string v2, "network"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    if-eqz v0, :cond_0

    .line 467
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 460
    :catch_0
    move-exception v1

    .line 462
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 466
    if-eqz v0, :cond_0

    .line 467
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 466
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 467
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbDeleteProfileListRow(J)V
    .locals 6
    .param p0, "rowId"    # J

    .prologue
    .line 710
    const/4 v0, 0x0

    .line 714
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 715
    const-string v2, "profilelist"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 723
    if-eqz v0, :cond_0

    .line 724
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 726
    :cond_0
    :goto_0
    return-void

    .line 717
    :catch_0
    move-exception v1

    .line 719
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 723
    if-eqz v0, :cond_0

    .line 724
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 723
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 724
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbDeleteProfileRow(J)V
    .locals 6
    .param p0, "rowId"    # J

    .prologue
    .line 171
    const/4 v0, 0x0

    .line 175
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 176
    const-string v2, "profile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    if-eqz v0, :cond_0

    .line 185
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 178
    :catch_0
    move-exception v1

    .line 180
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    if-eqz v0, :cond_0

    .line 185
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 184
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 185
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbDeleteResyncModeRow(J)V
    .locals 6
    .param p0, "rowId"    # J

    .prologue
    .line 1242
    const/4 v0, 0x0

    .line 1246
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1247
    const-string v2, "resyncmode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1255
    if-eqz v0, :cond_0

    .line 1256
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1258
    :cond_0
    :goto_0
    return-void

    .line 1249
    :catch_0
    move-exception v1

    .line 1251
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1255
    if-eqz v0, :cond_0

    .line 1256
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1255
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 1256
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbDeleteSimInfoRow(J)V
    .locals 6
    .param p0, "rowId"    # J

    .prologue
    .line 939
    const/4 v0, 0x0

    .line 943
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 944
    const-string v2, "siminfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 952
    if-eqz v0, :cond_0

    .line 953
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 955
    :cond_0
    :goto_0
    return-void

    .line 946
    :catch_0
    move-exception v1

    .line 948
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 952
    if-eqz v0, :cond_0

    .line 953
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 952
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 953
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbExistsAccXListNodeRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 1180
    const/4 v10, 0x1

    .line 1181
    .local v10, "bRet":Z
    const/4 v11, 0x0

    .line 1182
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1183
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x7

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "account"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "appaddr"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "appaddrport"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "clientappauth"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "serverappauth"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "toconref"

    aput-object v2, v3, v1

    .line 1188
    .local v3, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1189
    const/4 v1, 0x1

    const-string v2, "accxlistnode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1191
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 1193
    const/4 v10, 0x1

    .line 1207
    :goto_0
    if-eqz v11, :cond_0

    .line 1208
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1210
    :cond_0
    if-eqz v0, :cond_1

    .line 1211
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1214
    :cond_1
    :goto_1
    return v10

    .line 1197
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 1200
    :catch_0
    move-exception v12

    .line 1202
    .local v12, "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    .line 1203
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1207
    if-eqz v11, :cond_3

    .line 1208
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1210
    :cond_3
    if-eqz v0, :cond_1

    .line 1211
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 1207
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_4

    .line 1208
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1210
    :cond_4
    if-eqz v0, :cond_5

    .line 1211
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdmDbExistsNetworkRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 607
    const/4 v10, 0x1

    .line 608
    .local v10, "bRet":Z
    const/4 v11, 0x0

    .line 609
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 610
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v1, 0x16

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "homeurl"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "service"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "active"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "proxyuse"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "napnetworkprofilename"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "napbearer"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string v2, "napaddrtype"

    aput-object v2, v3, v1

    const/16 v1, 0x8

    const-string v2, "napaddr"

    aput-object v2, v3, v1

    const/16 v1, 0x9

    const-string v2, "nappapid"

    aput-object v2, v3, v1

    const/16 v1, 0xa

    const-string v2, "nappapsecret"

    aput-object v2, v3, v1

    const/16 v1, 0xb

    const-string v2, "pxportnbr"

    aput-object v2, v3, v1

    const/16 v1, 0xc

    const-string v2, "pxaddrtype"

    aput-object v2, v3, v1

    const/16 v1, 0xd

    const-string v2, "pxaddr"

    aput-object v2, v3, v1

    const/16 v1, 0xe

    const-string v2, "pxpapid"

    aput-object v2, v3, v1

    const/16 v1, 0xf

    const-string v2, "pxpapsecret"

    aput-object v2, v3, v1

    const/16 v1, 0x10

    const-string v2, "staticipuse"

    aput-object v2, v3, v1

    const/16 v1, 0x11

    const-string v2, "staticip"

    aput-object v2, v3, v1

    const/16 v1, 0x12

    const-string v2, "staticdnsuse"

    aput-object v2, v3, v1

    const/16 v1, 0x13

    const-string v2, "staticdns1"

    aput-object v2, v3, v1

    const/16 v1, 0x14

    const-string v2, "staticdns2"

    aput-object v2, v3, v1

    const/16 v1, 0x15

    const-string v2, "trafficclass"

    aput-object v2, v3, v1

    .line 636
    .local v3, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 637
    const/4 v1, 0x1

    const-string v2, "network"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 639
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 641
    const/4 v10, 0x1

    .line 655
    :goto_0
    if-eqz v11, :cond_0

    .line 656
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 658
    :cond_0
    if-eqz v0, :cond_1

    .line 659
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 661
    :cond_1
    :goto_1
    return v10

    .line 645
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 648
    :catch_0
    move-exception v12

    .line 650
    .local v12, "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    .line 651
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 655
    if-eqz v11, :cond_3

    .line 656
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 658
    :cond_3
    if-eqz v0, :cond_1

    .line 659
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 655
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_4

    .line 656
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 658
    :cond_4
    if-eqz v0, :cond_5

    .line 659
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdmDbExistsProfileListRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 855
    const/4 v10, 0x1

    .line 856
    .local v10, "bRet":Z
    const/4 v11, 0x0

    .line 857
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 858
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v1, 0x17

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "networkconnname"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "proxyindex"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "profileindex"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "profilename1"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "profilename2"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "profilename3"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string v2, "sessionid"

    aput-object v2, v3, v1

    const/16 v1, 0x8

    const-string v2, "notievent"

    aput-object v2, v3, v1

    const/16 v1, 0x9

    const-string v2, "destorynotitime"

    aput-object v2, v3, v1

    const/16 v1, 0xa

    const-string v2, "notiresyncmode"

    aput-object v2, v3, v1

    const/16 v1, 0xb

    const-string v2, "magicnumber"

    aput-object v2, v3, v1

    const/16 v1, 0xc

    const-string v2, "sessionsavestate"

    aput-object v2, v3, v1

    const/16 v1, 0xd

    const-string v2, "notiuievent"

    aput-object v2, v3, v1

    const/16 v1, 0xe

    const-string v2, "notiretrycount"

    aput-object v2, v3, v1

    const/16 v1, 0xf

    const-string v2, "status"

    aput-object v2, v3, v1

    const/16 v1, 0x10

    const-string v2, "appid"

    aput-object v2, v3, v1

    const/16 v1, 0x11

    const-string v2, "uictype"

    aput-object v2, v3, v1

    const/16 v1, 0x12

    const-string v2, "result"

    aput-object v2, v3, v1

    const/16 v1, 0x13

    const-string v2, "number"

    aput-object v2, v3, v1

    const/16 v1, 0x14

    const-string v2, "text"

    aput-object v2, v3, v1

    const/16 v1, 0x15

    const-string v2, "len"

    aput-object v2, v3, v1

    const/16 v1, 0x16

    const-string v2, "size"

    aput-object v2, v3, v1

    .line 885
    .local v3, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 886
    const/4 v1, 0x1

    const-string v2, "profilelist"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 888
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 890
    const/4 v10, 0x1

    .line 904
    :goto_0
    if-eqz v11, :cond_0

    .line 905
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 907
    :cond_0
    if-eqz v0, :cond_1

    .line 908
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 910
    :cond_1
    :goto_1
    return v10

    .line 894
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 897
    :catch_0
    move-exception v12

    .line 899
    .local v12, "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    .line 900
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 904
    if-eqz v11, :cond_3

    .line 905
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 907
    :cond_3
    if-eqz v0, :cond_1

    .line 908
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 904
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_4

    .line 905
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 907
    :cond_4
    if-eqz v0, :cond_5

    .line 908
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdmDbExistsProfileRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 342
    const/4 v12, 0x1

    .line 343
    .local v12, "ret":Z
    const/4 v10, 0x0

    .line 344
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 345
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v1, 0x1f

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "protocol"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "serverport"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "serverurl"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "serverip"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "path"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "protocol_org"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string v2, "serverport_org"

    aput-object v2, v3, v1

    const/16 v1, 0x8

    const-string v2, "serverurl_org"

    aput-object v2, v3, v1

    const/16 v1, 0x9

    const-string v2, "serverip_org"

    aput-object v2, v3, v1

    const/16 v1, 0xa

    const-string v2, "path_org"

    aput-object v2, v3, v1

    const/16 v1, 0xb

    const-string v2, "changedprotocol"

    aput-object v2, v3, v1

    const/16 v1, 0xc

    const-string v2, "obextype"

    aput-object v2, v3, v1

    const/16 v1, 0xd

    const-string v2, "authtype"

    aput-object v2, v3, v1

    const/16 v1, 0xe

    const-string v2, "serverauthtype"

    aput-object v2, v3, v1

    const/16 v1, 0xf

    const-string v2, "appid"

    aput-object v2, v3, v1

    const/16 v1, 0x10

    const-string v2, "authlevel"

    aput-object v2, v3, v1

    const/16 v1, 0x11

    const-string v2, "serverauthlevel"

    aput-object v2, v3, v1

    const/16 v1, 0x12

    const-string v2, "prefconref"

    aput-object v2, v3, v1

    const/16 v1, 0x13

    const-string v2, "username"

    aput-object v2, v3, v1

    const/16 v1, 0x14

    const-string v2, "password"

    aput-object v2, v3, v1

    const/16 v1, 0x15

    const-string v2, "serverid"

    aput-object v2, v3, v1

    const/16 v1, 0x16

    const-string v2, "serverpwd"

    aput-object v2, v3, v1

    const/16 v1, 0x17

    const-string v2, "clientnonce"

    aput-object v2, v3, v1

    const/16 v1, 0x18

    const-string v2, "servernonce"

    aput-object v2, v3, v1

    const/16 v1, 0x19

    const-string v2, "servernonceformat"

    aput-object v2, v3, v1

    const/16 v1, 0x1a

    const-string v2, "clientnonceformat"

    aput-object v2, v3, v1

    const/16 v1, 0x1b

    const-string v2, "profilename"

    aput-object v2, v3, v1

    const/16 v1, 0x1c

    const-string v2, "networkconnname"

    aput-object v2, v3, v1

    const/16 v1, 0x1d

    const-string v2, "networkconnindex"

    aput-object v2, v3, v1

    const/16 v1, 0x1e

    const-string v2, "magicnumber"

    aput-object v2, v3, v1

    .line 380
    .local v3, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 381
    const/4 v1, 0x1

    const-string v2, "profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 383
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 385
    const/4 v12, 0x1

    .line 399
    :goto_0
    if-eqz v10, :cond_0

    .line 400
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 402
    :cond_0
    if-eqz v0, :cond_1

    .line 403
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 405
    :cond_1
    :goto_1
    return v12

    .line 389
    :cond_2
    const/4 v12, 0x0

    goto :goto_0

    .line 392
    :catch_0
    move-exception v11

    .line 394
    .local v11, "e":Ljava/lang/Exception;
    const/4 v12, 0x0

    .line 395
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 399
    if-eqz v10, :cond_3

    .line 400
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 402
    :cond_3
    if-eqz v0, :cond_1

    .line 403
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 399
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 400
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 402
    :cond_4
    if-eqz v0, :cond_5

    .line 403
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdmDbExistsResyncModeRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 1325
    const/4 v10, 0x1

    .line 1326
    .local v10, "bRet":Z
    const/4 v11, 0x0

    .line 1327
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1328
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "nonceresyncmode"

    aput-object v1, v3, v4

    .line 1333
    .local v3, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1334
    const/4 v1, 0x1

    const-string v2, "resyncmode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1336
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 1338
    const/4 v10, 0x1

    .line 1352
    :goto_0
    if-eqz v11, :cond_0

    .line 1353
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1355
    :cond_0
    if-eqz v0, :cond_1

    .line 1356
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1359
    :cond_1
    :goto_1
    return v10

    .line 1342
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 1345
    :catch_0
    move-exception v12

    .line 1347
    .local v12, "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    .line 1348
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1352
    if-eqz v11, :cond_3

    .line 1353
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1355
    :cond_3
    if-eqz v0, :cond_1

    .line 1356
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 1352
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_4

    .line 1353
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1355
    :cond_4
    if-eqz v0, :cond_5

    .line 1356
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdmDbExistsSimInfoRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 1021
    const/4 v10, 0x1

    .line 1022
    .local v10, "bRet":Z
    const/4 v11, 0x0

    .line 1023
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1024
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "imsi"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "imsi2"

    aput-object v2, v3, v1

    .line 1029
    .local v3, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1030
    const/4 v1, 0x1

    const-string v2, "siminfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1032
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 1034
    const/4 v10, 0x1

    .line 1048
    :goto_0
    if-eqz v11, :cond_0

    .line 1049
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1051
    :cond_0
    if-eqz v0, :cond_1

    .line 1052
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1055
    :cond_1
    :goto_1
    return v10

    .line 1038
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 1041
    :catch_0
    move-exception v12

    .line 1043
    .local v12, "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    .line 1044
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048
    if-eqz v11, :cond_3

    .line 1049
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1051
    :cond_3
    if-eqz v0, :cond_1

    .line 1052
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 1048
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_4

    .line 1049
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1051
    :cond_4
    if-eqz v0, :cond_5

    .line 1052
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdmDbFetchAccXListNodeRow(JLcom/fmm/dm/db/file/XDBAccXNodeInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "rowId"    # J
    .param p2, "accxnode"    # Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    .prologue
    .line 1109
    const/4 v12, 0x0

    .line 1110
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 1111
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x7

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "account"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "appaddr"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "appaddrport"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "clientappauth"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string v4, "serverappauth"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string v4, "toconref"

    aput-object v4, v5, v3

    .line 1116
    .local v5, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1117
    const/4 v3, 0x1

    const-string v4, "accxlistnode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1119
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 1121
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1124
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1125
    .local v14, "id":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAccount:Ljava/lang/String;

    .line 1126
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAppAddr:Ljava/lang/String;

    .line 1127
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAppAddrPort:Ljava/lang/String;

    .line 1128
    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szClientAppAuth:Ljava/lang/String;

    .line 1129
    const/4 v3, 0x5

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szServerAppAuth:Ljava/lang/String;

    .line 1130
    const/4 v3, 0x6

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szToConRef:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1134
    .end local v14    # "id":J
    :catch_0
    move-exception v13

    .line 1136
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1140
    if-eqz v12, :cond_0

    .line 1141
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1143
    :cond_0
    if-eqz v2, :cond_1

    .line 1144
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1147
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 1140
    :cond_2
    if-eqz v12, :cond_3

    .line 1141
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1143
    :cond_3
    if-eqz v2, :cond_1

    .line 1144
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 1140
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_4

    .line 1141
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1143
    :cond_4
    if-eqz v2, :cond_5

    .line 1144
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v3
.end method

.method public static xdmDbFetchNetworkRow(JLcom/fmm/dm/db/file/XDBInfoConRef;)Ljava/lang/Object;
    .locals 16
    .param p0, "rowId"    # J
    .param p2, "conref"    # Lcom/fmm/dm/db/file/XDBInfoConRef;

    .prologue
    .line 516
    const/4 v12, 0x0

    .line 517
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 518
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v3, 0x16

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "homeurl"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "service"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "active"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "proxyuse"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string v4, "napnetworkprofilename"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string v4, "napbearer"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string v4, "napaddrtype"

    aput-object v4, v5, v3

    const/16 v3, 0x8

    const-string v4, "napaddr"

    aput-object v4, v5, v3

    const/16 v3, 0x9

    const-string v4, "nappapid"

    aput-object v4, v5, v3

    const/16 v3, 0xa

    const-string v4, "nappapsecret"

    aput-object v4, v5, v3

    const/16 v3, 0xb

    const-string v4, "pxportnbr"

    aput-object v4, v5, v3

    const/16 v3, 0xc

    const-string v4, "pxaddrtype"

    aput-object v4, v5, v3

    const/16 v3, 0xd

    const-string v4, "pxaddr"

    aput-object v4, v5, v3

    const/16 v3, 0xe

    const-string v4, "pxpapid"

    aput-object v4, v5, v3

    const/16 v3, 0xf

    const-string v4, "pxpapsecret"

    aput-object v4, v5, v3

    const/16 v3, 0x10

    const-string v4, "staticipuse"

    aput-object v4, v5, v3

    const/16 v3, 0x11

    const-string v4, "staticip"

    aput-object v4, v5, v3

    const/16 v3, 0x12

    const-string v4, "staticdnsuse"

    aput-object v4, v5, v3

    const/16 v3, 0x13

    const-string v4, "staticdns1"

    aput-object v4, v5, v3

    const/16 v3, 0x14

    const-string v4, "staticdns2"

    aput-object v4, v5, v3

    const/16 v3, 0x15

    const-string v4, "trafficclass"

    aput-object v4, v5, v3

    .line 544
    .local v5, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 545
    const/4 v3, 0x1

    const-string v4, "network"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 547
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_8

    .line 549
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 552
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 553
    .local v14, "id":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->m_szHomeUrl:Ljava/lang/String;

    .line 554
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    .line 555
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_2

    .line 556
    const/4 v3, 0x1

    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->Active:Z

    .line 559
    :goto_1
    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_5

    .line 560
    const/4 v3, 0x1

    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 563
    :goto_2
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v4, 0x5

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    .line 564
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v4, 0x6

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->nBearer:I

    .line 565
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/4 v4, 0x7

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->nAddrType:I

    .line 566
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    const/16 v4, 0x8

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    .line 567
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    const/16 v4, 0x9

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    .line 568
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    const/16 v4, 0xa

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    .line 569
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/16 v4, 0xb

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    .line 570
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/16 v4, 0xc

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    .line 571
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    const/16 v4, 0xd

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 572
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBConRefPX;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    const/16 v4, 0xe

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    .line 573
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBConRefPX;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    const/16 v4, 0xf

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    .line 574
    const/16 v3, 0x10

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_6

    .line 575
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticIpUse:Z

    .line 578
    :goto_3
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    const/16 v4, 0x11

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->m_szStaticIp:Ljava/lang/String;

    .line 579
    const/16 v3, 0x12

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_7

    .line 580
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticDnsUse:Z

    .line 583
    :goto_4
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    const/16 v4, 0x13

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->szStaticDns1:I

    .line 584
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    const/16 v4, 0x14

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->szStaticDns2:I

    .line 585
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    const/16 v4, 0x15

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->nTrafficClass:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 589
    .end local v14    # "id":J
    :catch_0
    move-exception v13

    .line 591
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595
    if-eqz v12, :cond_0

    .line 596
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 598
    :cond_0
    if-eqz v2, :cond_1

    .line 599
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 602
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_5
    return-object p2

    .line 558
    .restart local v14    # "id":J
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->Active:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 595
    .end local v14    # "id":J
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_3

    .line 596
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 598
    :cond_3
    if-eqz v2, :cond_4

    .line 599
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_4
    throw v3

    .line 562
    .restart local v14    # "id":J
    :cond_5
    const/4 v3, 0x0

    :try_start_3
    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    goto/16 :goto_2

    .line 577
    :cond_6
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticIpUse:Z

    goto :goto_3

    .line 582
    :cond_7
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticDnsUse:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 595
    .end local v14    # "id":J
    :cond_8
    if-eqz v12, :cond_9

    .line 596
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 598
    :cond_9
    if-eqz v2, :cond_1

    .line 599
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_5
.end method

.method public static xdmDbFetchProfileListRow(JLcom/fmm/dm/db/file/XDBProflieListInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "rowId"    # J
    .param p2, "profilelist"    # Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .prologue
    .line 774
    const/4 v12, 0x0

    .line 775
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 776
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v3, 0x17

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "networkconnname"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "proxyindex"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "profileindex"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "profilename1"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string v4, "profilename2"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string v4, "profilename3"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string v4, "sessionid"

    aput-object v4, v5, v3

    const/16 v3, 0x8

    const-string v4, "notievent"

    aput-object v4, v5, v3

    const/16 v3, 0x9

    const-string v4, "destorynotitime"

    aput-object v4, v5, v3

    const/16 v3, 0xa

    const-string v4, "notiresyncmode"

    aput-object v4, v5, v3

    const/16 v3, 0xb

    const-string v4, "magicnumber"

    aput-object v4, v5, v3

    const/16 v3, 0xc

    const-string v4, "sessionsavestate"

    aput-object v4, v5, v3

    const/16 v3, 0xd

    const-string v4, "notiuievent"

    aput-object v4, v5, v3

    const/16 v3, 0xe

    const-string v4, "notiretrycount"

    aput-object v4, v5, v3

    const/16 v3, 0xf

    const-string v4, "status"

    aput-object v4, v5, v3

    const/16 v3, 0x10

    const-string v4, "appid"

    aput-object v4, v5, v3

    const/16 v3, 0x11

    const-string v4, "uictype"

    aput-object v4, v5, v3

    const/16 v3, 0x12

    const-string v4, "result"

    aput-object v4, v5, v3

    const/16 v3, 0x13

    const-string v4, "number"

    aput-object v4, v5, v3

    const/16 v3, 0x14

    const-string v4, "text"

    aput-object v4, v5, v3

    const/16 v3, 0x15

    const-string v4, "len"

    aput-object v4, v5, v3

    const/16 v3, 0x16

    const-string v4, "size"

    aput-object v4, v5, v3

    .line 803
    .local v5, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 804
    const/4 v3, 0x1

    const-string v4, "profilelist"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 806
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 808
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 811
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 812
    .local v14, "id":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szNetworkConnName:Ljava/lang/String;

    .line 813
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nProxyIndex:I

    .line 814
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 815
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v6, 0x4

    invoke-interface {v12, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    .line 816
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v6, 0x5

    invoke-interface {v12, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    .line 817
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v4, 0x2

    const/4 v6, 0x6

    invoke-interface {v12, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    .line 818
    const/4 v3, 0x7

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szSessionID:Ljava/lang/String;

    .line 819
    const/16 v3, 0x8

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiEvent:I

    .line 820
    const/16 v3, 0x9

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v6, v3

    move-object/from16 v0, p2

    iput-wide v6, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nDestoryNotiTime:J

    .line 821
    const/16 v3, 0xa

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiReSyncMode:I

    .line 822
    const/16 v3, 0xb

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->MagicNumber:I

    .line 823
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    const/16 v4, 0xc

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    .line 824
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    const/16 v4, 0xd

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    .line 825
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    const/16 v4, 0xe

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    .line 826
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    const/16 v4, 0xf

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->eStatus:I

    .line 827
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    const/16 v4, 0x10

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->appId:I

    .line 828
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    const/16 v4, 0x11

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->UICType:I

    .line 829
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    const/16 v4, 0x12

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->result:I

    .line 830
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    const/16 v4, 0x13

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->number:I

    .line 831
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    const/16 v4, 0x14

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->m_szText:Ljava/lang/String;

    .line 832
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    const/16 v4, 0x15

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->nLen:I

    .line 833
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    const/16 v4, 0x16

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->nSize:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 837
    .end local v14    # "id":J
    :catch_0
    move-exception v13

    .line 839
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 843
    if-eqz v12, :cond_0

    .line 844
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_0
    if-eqz v2, :cond_1

    .line 847
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 850
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 843
    :cond_2
    if-eqz v12, :cond_3

    .line 844
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_3
    if-eqz v2, :cond_1

    .line 847
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 843
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_4

    .line 844
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_4
    if-eqz v2, :cond_5

    .line 847
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v3
.end method

.method public static xdmDbFetchProfileRow(JLcom/fmm/dm/db/file/XDBProfileInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "rowId"    # J
    .param p2, "dminfo"    # Lcom/fmm/dm/db/file/XDBProfileInfo;

    .prologue
    .line 243
    const/4 v12, 0x0

    .line 244
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 245
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v3, 0x1f

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "protocol"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "serverport"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "serverurl"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "serverip"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string v4, "path"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string v4, "protocol_org"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string v4, "serverport_org"

    aput-object v4, v5, v3

    const/16 v3, 0x8

    const-string v4, "serverurl_org"

    aput-object v4, v5, v3

    const/16 v3, 0x9

    const-string v4, "serverip_org"

    aput-object v4, v5, v3

    const/16 v3, 0xa

    const-string v4, "path_org"

    aput-object v4, v5, v3

    const/16 v3, 0xb

    const-string v4, "changedprotocol"

    aput-object v4, v5, v3

    const/16 v3, 0xc

    const-string v4, "obextype"

    aput-object v4, v5, v3

    const/16 v3, 0xd

    const-string v4, "authtype"

    aput-object v4, v5, v3

    const/16 v3, 0xe

    const-string v4, "serverauthtype"

    aput-object v4, v5, v3

    const/16 v3, 0xf

    const-string v4, "appid"

    aput-object v4, v5, v3

    const/16 v3, 0x10

    const-string v4, "authlevel"

    aput-object v4, v5, v3

    const/16 v3, 0x11

    const-string v4, "serverauthlevel"

    aput-object v4, v5, v3

    const/16 v3, 0x12

    const-string v4, "prefconref"

    aput-object v4, v5, v3

    const/16 v3, 0x13

    const-string v4, "username"

    aput-object v4, v5, v3

    const/16 v3, 0x14

    const-string v4, "password"

    aput-object v4, v5, v3

    const/16 v3, 0x15

    const-string v4, "serverid"

    aput-object v4, v5, v3

    const/16 v3, 0x16

    const-string v4, "serverpwd"

    aput-object v4, v5, v3

    const/16 v3, 0x17

    const-string v4, "clientnonce"

    aput-object v4, v5, v3

    const/16 v3, 0x18

    const-string v4, "servernonce"

    aput-object v4, v5, v3

    const/16 v3, 0x19

    const-string v4, "servernonceformat"

    aput-object v4, v5, v3

    const/16 v3, 0x1a

    const-string v4, "clientnonceformat"

    aput-object v4, v5, v3

    const/16 v3, 0x1b

    const-string v4, "profilename"

    aput-object v4, v5, v3

    const/16 v3, 0x1c

    const-string v4, "networkconnname"

    aput-object v4, v5, v3

    const/16 v3, 0x1d

    const-string v4, "networkconnindex"

    aput-object v4, v5, v3

    const/16 v3, 0x1e

    const-string v4, "magicnumber"

    aput-object v4, v5, v3

    .line 280
    .local v5, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 281
    const/4 v3, 0x1

    const-string v4, "profile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 283
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_5

    .line 285
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 288
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 289
    .local v14, "id":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 290
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    .line 291
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "smldm"

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbDecryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 292
    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 293
    const/4 v3, 0x5

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    .line 294
    const/4 v3, 0x6

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 295
    const/4 v3, 0x7

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 296
    const/16 v3, 0x8

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "smldm"

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbDecryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 297
    const/16 v3, 0x9

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 298
    const/16 v3, 0xa

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path_Org:Ljava/lang/String;

    .line 299
    const/16 v3, 0xb

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_2

    .line 300
    const/4 v3, 0x1

    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    .line 303
    :goto_1
    const/16 v3, 0xc

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ObexType:I

    .line 304
    const/16 v3, 0xd

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    .line 305
    const/16 v3, 0xe

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    .line 306
    const/16 v3, 0xf

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    .line 307
    const/16 v3, 0x10

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    .line 308
    const/16 v3, 0x11

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    .line 309
    const/16 v3, 0x12

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    .line 310
    const/16 v3, 0x13

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const-string v4, "smldm"

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbDecryptor([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 311
    const/16 v3, 0x14

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const-string v4, "smldm"

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbDecryptor([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 312
    const/16 v3, 0x15

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const-string v4, "smldm"

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbDecryptor([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 313
    const/16 v3, 0x16

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const-string v4, "smldm"

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbDecryptor([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 314
    const/16 v3, 0x17

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 315
    const/16 v3, 0x18

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 316
    const/16 v3, 0x19

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonceFormat:I

    .line 317
    const/16 v3, 0x1a

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonceFormat:I

    .line 318
    const/16 v3, 0x1b

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 319
    const/16 v3, 0x1c

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->NetworkConnName:Ljava/lang/String;

    .line 320
    const/16 v3, 0x1d

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->nNetworkConnIndex:I

    .line 321
    const/16 v3, 0x1e

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->MagicNumber:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 325
    .end local v14    # "id":J
    :catch_0
    move-exception v13

    .line 327
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331
    if-eqz v12, :cond_0

    .line 332
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 334
    :cond_0
    if-eqz v2, :cond_1

    .line 335
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 337
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-object p2

    .line 302
    .restart local v14    # "id":J
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 331
    .end local v14    # "id":J
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_3

    .line 332
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 334
    :cond_3
    if-eqz v2, :cond_4

    .line 335
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_4
    throw v3

    .line 331
    :cond_5
    if-eqz v12, :cond_6

    .line 332
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 334
    :cond_6
    if-eqz v2, :cond_1

    .line 335
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_2
.end method

.method public static xdmDbFetchResyncModeRow(JLcom/fmm/dm/db/file/XDBResyncModeInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "rowId"    # J
    .param p2, "resyncmode"    # Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .prologue
    .line 1262
    const/4 v12, 0x0

    .line 1263
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 1264
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "nonceresyncmode"

    aput-object v4, v5, v3

    .line 1269
    .local v5, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1270
    const/4 v3, 0x1

    const-string v4, "resyncmode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1272
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_5

    .line 1274
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1277
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1278
    .local v14, "id":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_2

    .line 1279
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBResyncModeInfo;->nNoceResyncMode:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1285
    .end local v14    # "id":J
    :catch_0
    move-exception v13

    .line 1287
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1291
    if-eqz v12, :cond_0

    .line 1292
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1294
    :cond_0
    if-eqz v2, :cond_1

    .line 1295
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1297
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 1281
    .restart local v14    # "id":J
    :cond_2
    const/4 v3, 0x1

    :try_start_2
    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBResyncModeInfo;->nNoceResyncMode:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1291
    .end local v14    # "id":J
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_3

    .line 1292
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1294
    :cond_3
    if-eqz v2, :cond_4

    .line 1295
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_4
    throw v3

    .line 1291
    :cond_5
    if-eqz v12, :cond_6

    .line 1292
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1294
    :cond_6
    if-eqz v2, :cond_1

    .line 1295
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1
.end method

.method public static xdmDbFetchSimInfoRow(JLcom/fmm/dm/db/file/XDBSimInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "rowId"    # J
    .param p2, "siminfo"    # Lcom/fmm/dm/db/file/XDBSimInfo;

    .prologue
    .line 959
    const/4 v12, 0x0

    .line 960
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 961
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x3

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "imsi"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "imsi2"

    aput-object v4, v5, v3

    .line 966
    .local v5, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 967
    const/4 v3, 0x1

    const-string v4, "siminfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 969
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 971
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 974
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 975
    .local v14, "id":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "smldm"

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbDecryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    .line 976
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "smldm"

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbDecryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 980
    .end local v14    # "id":J
    :catch_0
    move-exception v13

    .line 982
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 986
    if-eqz v12, :cond_0

    .line 987
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 989
    :cond_0
    if-eqz v2, :cond_1

    .line 990
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 992
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 986
    :cond_2
    if-eqz v12, :cond_3

    .line 987
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 989
    :cond_3
    if-eqz v2, :cond_1

    .line 990
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 986
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_4

    .line 987
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 989
    :cond_4
    if-eqz v2, :cond_5

    .line 990
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v3
.end method

.method public static xdmDbFullReset()V
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 55
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 56
    const-string v2, "DROP TABLE IF EXISTS profile"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    const-string v2, "DROP TABLE IF EXISTS network"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 58
    const-string v2, "DROP TABLE IF EXISTS profilelist"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 59
    const-string v2, "DROP TABLE IF EXISTS siminfo"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 60
    const-string v2, "DROP TABLE IF EXISTS accxlistnode"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 61
    const-string v2, "DROP TABLE IF EXISTS resyncmode"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 62
    const-string v2, "DROP TABLE IF EXISTS DmAgnetInfo"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 71
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 73
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    :try_start_1
    const-string v2, "create table if not exists profile (rowid integer primary key autoincrement, protocol text, serverport integer, serverurl text, serverip text, path text, protocol_org text, serverport_org integer, serverurl_org text, serverip_org text, path_org text, changedprotocol integer, obextype integer, authtype integer, serverauthtype integer, appid text, authlevel text, serverauthlevel text, prefconref text, username byte, password byte, serverid byte, serverpwd byte, clientnonce text, servernonce text, servernonceformat integer, clientnonceformat integer, profilename text, networkconnname text, networkconnindex integer, magicnumber integer);"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 84
    const-string v2, "create table if not exists network (rowid integer primary key autoincrement, homeurl text, service integer, active integer, proxyuse integer, napnetworkprofilename text, napbearer integer, napaddrtype integer, napaddr text, nappapid text, nappapsecret text, pxportnbr integer, pxaddrtype integer, pxaddr text, pxpapid text, pxpapsecret text, staticipuse integer, staticip text, staticdnsuse integer, staticdns1 integer, staticdns2 integer, trafficclass integer);"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 85
    const-string v2, "create table if not exists profilelist (rowid integer primary key autoincrement, networkconnname text, proxyindex integer, profileindex integer, profilename1 text, profilename2 text, profilename3 text, sessionid text, notievent integer, destorynotitime integer, notiresyncmode integer, magicnumber integer, sessionsavestate integer, notiuievent integer, notiretrycount integer, status integer, appid integer, uictype integer, result integer, number integer, text text, len integer, size integer);"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 86
    const-string v2, "create table if not exists siminfo (rowid integer primary key autoincrement, imsi text, imsi2 text);"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 88
    const-string v2, "create table if not exists accxlistnode (rowid integer primary key autoincrement, account text, appaddr text, appaddrport text, clientappauth text, serverappauth text, toconref text);"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 89
    const-string v2, "create table if not exists resyncmode (rowid integer primary key autoincrement, nonceresyncmode integer);"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 91
    const-string v2, "create table if not exists DmAgnetInfo (rowid integer primary key autoincrement, AgentType integer);"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 95
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 96
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 100
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 101
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 103
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 104
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 115
    :cond_0
    :goto_1
    return-void

    .line 75
    :catch_0
    move-exception v1

    .line 77
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 106
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 108
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 112
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 113
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbInsertAccXListNodeRow(Lcom/fmm/dm/db/file/XDBAccXNodeInfo;)V
    .locals 5
    .param p0, "accxnode"    # Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    .prologue
    .line 1061
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1062
    .local v2, "initialValues":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 1066
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "account"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAccount:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const-string v3, "appaddr"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAppAddr:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    const-string v3, "appaddrport"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAppAddrPort:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    const-string v3, "clientappauth"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szClientAppAuth:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    const-string v3, "serverappauth"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szServerAppAuth:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    const-string v3, "toconref"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szToConRef:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1074
    const-string v3, "accxlistnode"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1082
    if-eqz v0, :cond_0

    .line 1083
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1085
    :cond_0
    :goto_0
    return-void

    .line 1076
    :catch_0
    move-exception v1

    .line 1078
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1082
    if-eqz v0, :cond_0

    .line 1083
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1082
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 1083
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbInsertNetworkRow(Lcom/fmm/dm/db/file/XDBInfoConRef;)V
    .locals 5
    .param p0, "ConRef"    # Lcom/fmm/dm/db/file/XDBInfoConRef;

    .prologue
    .line 410
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 411
    .local v2, "initialValues":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 415
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "homeurl"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->m_szHomeUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v3, "service"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 417
    const-string v3, "active"

    iget-boolean v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->Active:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 418
    const-string v3, "proxyuse"

    iget-boolean v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 419
    const-string v3, "napnetworkprofilename"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string v3, "napbearer"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->nBearer:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 421
    const-string v3, "napaddrtype"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->nAddrType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 422
    const-string v3, "napaddr"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v3, "nappapid"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const-string v3, "nappapsecret"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const-string v3, "pxportnbr"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 426
    const-string v3, "pxaddrtype"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 427
    const-string v3, "pxaddr"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    const-string v3, "pxpapid"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string v3, "pxpapsecret"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v3, "staticipuse"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget-boolean v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticIpUse:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 431
    const-string v3, "staticip"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->m_szStaticIp:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const-string v3, "staticdnsuse"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget-boolean v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticDnsUse:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 433
    const-string v3, "staticdns1"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->szStaticDns1:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 434
    const-string v3, "staticdns2"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->szStaticDns2:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 435
    const-string v3, "trafficclass"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->nTrafficClass:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 437
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 438
    const-string v3, "network"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    if-eqz v0, :cond_0

    .line 447
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 440
    :catch_0
    move-exception v1

    .line 442
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446
    if-eqz v0, :cond_0

    .line 447
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 446
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 447
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbInsertProfileListRow(Lcom/fmm/dm/db/file/XDBProflieListInfo;)V
    .locals 6
    .param p0, "profilelist"    # Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .prologue
    .line 666
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 667
    .local v2, "initialValues":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 671
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "networkconnname"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szNetworkConnName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    const-string v3, "proxyindex"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nProxyIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 673
    const-string v3, "profileindex"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 674
    const-string v3, "profilename1"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    const-string v3, "profilename2"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    const-string v3, "profilename3"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const-string v3, "sessionid"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szSessionID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    const-string v3, "notievent"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiEvent:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679
    const-string v3, "destorynotitime"

    iget-wide v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nDestoryNotiTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 680
    const-string v3, "notiresyncmode"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiReSyncMode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 681
    const-string v3, "magicnumber"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->MagicNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 682
    const-string v3, "sessionsavestate"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 683
    const-string v3, "notiuievent"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 684
    const-string v3, "notiretrycount"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 685
    const-string v3, "status"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->eStatus:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 686
    const-string v3, "appid"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->appId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 687
    const-string v3, "uictype"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->UICType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 688
    const-string v3, "result"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->result:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 689
    const-string v3, "number"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->number:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 690
    const-string v3, "text"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->m_szText:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    const-string v3, "len"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->nLen:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 692
    const-string v3, "size"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->nSize:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 694
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 695
    const-string v3, "profilelist"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 703
    if-eqz v0, :cond_0

    .line 704
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 706
    :cond_0
    :goto_0
    return-void

    .line 697
    :catch_0
    move-exception v1

    .line 699
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 703
    if-eqz v0, :cond_0

    .line 704
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 703
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 704
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbInsertProfileRow(Lcom/fmm/dm/db/file/XDBProfileInfo;)V
    .locals 6
    .param p0, "dminfo"    # Lcom/fmm/dm/db/file/XDBProfileInfo;

    .prologue
    .line 119
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 120
    .local v2, "initialValues":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 124
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "protocol"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v3, "serverport"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 126
    const-string v3, "serverurl"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v3, "serverip"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v3, "path"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v3, "protocol_org"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v3, "serverport_org"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 131
    const-string v3, "serverurl_org"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v3, "serverip_org"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v3, "path_org"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path_Org:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v3, "changedprotocol"

    iget-boolean v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 135
    const-string v3, "obextype"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ObexType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 136
    const-string v3, "authtype"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 137
    const-string v3, "serverauthtype"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 138
    const-string v3, "appid"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v3, "authlevel"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v3, "serverauthlevel"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v3, "prefconref"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v3, "username"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 143
    const-string v3, "password"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 144
    const-string v3, "serverid"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 145
    const-string v3, "serverpwd"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 146
    const-string v3, "clientnonce"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v3, "servernonce"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v3, "servernonceformat"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonceFormat:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 149
    const-string v3, "clientnonceformat"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonceFormat:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 150
    const-string v3, "profilename"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v3, "networkconnname"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->NetworkConnName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v3, "networkconnindex"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->nNetworkConnIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 153
    const-string v3, "magicnumber"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBProfileInfo;->MagicNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 155
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 156
    const-string v3, "profile"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    if-eqz v0, :cond_0

    .line 165
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    if-eqz v0, :cond_0

    .line 165
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 164
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 165
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbInsertResyncModeRow(Lcom/fmm/dm/db/file/XDBResyncModeInfo;)V
    .locals 5
    .param p0, "resyncmode"    # Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .prologue
    .line 1219
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1220
    .local v2, "initialValues":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 1224
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "nonceresyncmode"

    iget-boolean v4, p0, Lcom/fmm/dm/db/file/XDBResyncModeInfo;->nNoceResyncMode:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1226
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1227
    const-string v3, "resyncmode"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1235
    if-eqz v0, :cond_0

    .line 1236
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1238
    :cond_0
    :goto_0
    return-void

    .line 1229
    :catch_0
    move-exception v1

    .line 1231
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1235
    if-eqz v0, :cond_0

    .line 1236
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1235
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 1236
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbInsertSimInfoRow(Lcom/fmm/dm/db/file/XDBSimInfo;)V
    .locals 6
    .param p0, "siminfo"    # Lcom/fmm/dm/db/file/XDBSimInfo;

    .prologue
    .line 915
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 916
    .local v2, "initialValues":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 920
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "imsi"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    const-string v3, "imsi2"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 924
    const-string v3, "siminfo"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 932
    if-eqz v0, :cond_0

    .line 933
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 935
    :cond_0
    :goto_0
    return-void

    .line 926
    :catch_0
    move-exception v1

    .line 928
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 932
    if-eqz v0, :cond_0

    .line 933
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 932
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 933
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbResetNetworkTable()V
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x0

    .line 33
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 34
    const-string v2, "DROP TABLE IF EXISTS network"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 35
    const-string v2, "create table if not exists network (rowid integer primary key autoincrement, homeurl text, service integer, active integer, proxyuse integer, napnetworkprofilename text, napbearer integer, napaddrtype integer, napaddr text, nappapid text, nappapsecret text, pxportnbr integer, pxaddrtype integer, pxaddr text, pxpapid text, pxpapsecret text, staticipuse integer, staticip text, staticdnsuse integer, staticdns1 integer, staticdns2 integer, trafficclass integer);"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    if-eqz v0, :cond_0

    .line 44
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v1

    .line 39
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 43
    if-eqz v0, :cond_0

    .line 44
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 43
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 44
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbSqlAgentInfoDeleteRow(J)V
    .locals 6
    .param p0, "RowId"    # J

    .prologue
    .line 1389
    const/4 v0, 0x0

    .line 1393
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1394
    const-string v2, "DmAgnetInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1402
    if-eqz v0, :cond_0

    .line 1403
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1405
    :cond_0
    :goto_0
    return-void

    .line 1396
    :catch_0
    move-exception v1

    .line 1398
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1402
    if-eqz v0, :cond_0

    .line 1403
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1402
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 1403
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xdmDbSqlAgentInfoExistsRow(J)Z
    .locals 14
    .param p0, "RowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 1471
    const/4 v11, 0x1

    .line 1472
    .local v11, "bRet":Z
    const/4 v10, 0x0

    .line 1473
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1474
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "AgentType"

    aput-object v1, v3, v4

    .line 1479
    .local v3, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1480
    const/4 v1, 0x1

    const-string v2, "DmAgnetInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1482
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 1484
    const/4 v11, 0x1

    .line 1498
    :goto_0
    if-eqz v10, :cond_0

    .line 1499
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1501
    :cond_0
    if-eqz v0, :cond_1

    .line 1502
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1505
    :cond_1
    :goto_1
    return v11

    .line 1488
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 1491
    :catch_0
    move-exception v12

    .line 1493
    .local v12, "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    .line 1494
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1498
    if-eqz v10, :cond_3

    .line 1499
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1501
    :cond_3
    if-eqz v0, :cond_1

    .line 1502
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 1498
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 1499
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1501
    :cond_4
    if-eqz v0, :cond_5

    .line 1502
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdmDbSqlAgentInfoFetchRow(JLcom/fmm/dm/db/file/XDBAgentInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "RowId"    # J
    .param p2, "DmAgentInfo"    # Lcom/fmm/dm/db/file/XDBAgentInfo;

    .prologue
    .line 1409
    const/4 v14, 0x0

    .line 1410
    .local v14, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 1411
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "AgentType"

    aput-object v4, v5, v3

    .line 1416
    .local v5, "FROM":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1417
    const/4 v3, 0x1

    const-string v4, "DmAgnetInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1419
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 1421
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1424
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1426
    .local v12, "Id":J
    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBAgentInfo;->m_nAgentType:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1430
    .end local v12    # "Id":J
    :catch_0
    move-exception v15

    .line 1432
    .local v15, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v15}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1436
    if-eqz v14, :cond_0

    .line 1437
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1439
    :cond_0
    if-eqz v2, :cond_1

    .line 1440
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1443
    .end local v15    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 1436
    :cond_2
    if-eqz v14, :cond_3

    .line 1437
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1439
    :cond_3
    if-eqz v2, :cond_1

    .line 1440
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 1436
    :catchall_0
    move-exception v3

    if-eqz v14, :cond_4

    .line 1437
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1439
    :cond_4
    if-eqz v2, :cond_5

    .line 1440
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v3
.end method

.method public static xdmDbSqlAgentInfoInsertRow(Lcom/fmm/dm/db/file/XDBAgentInfo;)V
    .locals 5
    .param p0, "DmAgentInfo"    # Lcom/fmm/dm/db/file/XDBAgentInfo;

    .prologue
    .line 1366
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1367
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 1371
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "AgentType"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBAgentInfo;->m_nAgentType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1373
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1374
    const-string v3, "DmAgnetInfo"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1382
    if-eqz v1, :cond_0

    .line 1383
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1385
    :cond_0
    :goto_0
    return-void

    .line 1376
    :catch_0
    move-exception v2

    .line 1378
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1382
    if-eqz v1, :cond_0

    .line 1383
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1382
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 1383
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbSqlAgentInfoUpdateRow(JLcom/fmm/dm/db/file/XDBAgentInfo;)V
    .locals 6
    .param p0, "RowId"    # J
    .param p2, "DmAgentInfo"    # Lcom/fmm/dm/db/file/XDBAgentInfo;

    .prologue
    .line 1448
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1449
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 1453
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "AgentType"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBAgentInfo;->m_nAgentType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1455
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1456
    const-string v3, "DmAgnetInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1464
    if-eqz v1, :cond_0

    .line 1465
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1467
    :cond_0
    :goto_0
    return-void

    .line 1458
    :catch_0
    move-exception v2

    .line 1460
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1464
    if-eqz v1, :cond_0

    .line 1465
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1464
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 1465
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbSqlCreate(ILjava/lang/Object;)I
    .locals 1
    .param p0, "SqlID"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    .line 1537
    sparse-switch p0, :sswitch_data_0

    .line 1586
    .end local p1    # "Input":Ljava/lang/Object;
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1540
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_0
    check-cast p1, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbInsertProfileListRow(Lcom/fmm/dm/db/file/XDBProflieListInfo;)V

    goto :goto_0

    .line 1546
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_1
    check-cast p1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbInsertProfileRow(Lcom/fmm/dm/db/file/XDBProfileInfo;)V

    goto :goto_0

    .line 1550
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_2
    check-cast p1, Lcom/fmm/dm/db/file/XDBSimInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbInsertSimInfoRow(Lcom/fmm/dm/db/file/XDBSimInfo;)V

    goto :goto_0

    .line 1555
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_3
    check-cast p1, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbInsertAccXListNodeRow(Lcom/fmm/dm/db/file/XDBAccXNodeInfo;)V

    goto :goto_0

    .line 1559
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_4
    check-cast p1, Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbInsertResyncModeRow(Lcom/fmm/dm/db/file/XDBResyncModeInfo;)V

    goto :goto_0

    .line 1563
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_5
    check-cast p1, Lcom/fmm/dm/db/file/XDBAgentInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlAgentInfoInsertRow(Lcom/fmm/dm/db/file/XDBAgentInfo;)V

    goto :goto_0

    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_6
    move-object v0, p1

    .line 1568
    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInsertRow(Lcom/fmm/dm/db/file/XDBLawmoInfo;)V

    move-object v0, p1

    .line 1569
    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInternalMemInsertRow(Lcom/fmm/dm/db/file/XDBLawmoInfo;)V

    .line 1570
    check-cast p1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExternalMemInsertRow(Lcom/fmm/dm/db/file/XDBLawmoInfo;)V

    goto :goto_0

    .line 1575
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_7
    check-cast p1, Lcom/fmm/dm/db/file/XDBAMTInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlInsertRow(Lcom/fmm/dm/db/file/XDBAMTInfo;)V

    goto :goto_0

    .line 1579
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_8
    check-cast p1, Lcom/fmm/dm/db/file/XDBNotiInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoInsertRow(Lcom/fmm/dm/db/file/XDBNotiInfo;)V

    goto :goto_0

    .line 1537
    nop

    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_0
        0x52 -> :sswitch_1
        0x53 -> :sswitch_1
        0x54 -> :sswitch_1
        0x55 -> :sswitch_2
        0x56 -> :sswitch_4
        0x57 -> :sswitch_5
        0x58 -> :sswitch_6
        0x59 -> :sswitch_7
        0x60 -> :sswitch_8
        0x83 -> :sswitch_3
    .end sparse-switch
.end method

.method public static xdmDbSqlDelete(I)I
    .locals 2
    .param p0, "SqlID"    # I

    .prologue
    const-wide/16 v0, 0x1

    .line 1694
    sparse-switch p0, :sswitch_data_0

    .line 1752
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1697
    :sswitch_0
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbDeleteProfileListRow(J)V

    goto :goto_0

    .line 1701
    :sswitch_1
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbDeleteProfileRow(J)V

    .line 1702
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbDeleteNetworkRow(J)V

    goto :goto_0

    .line 1706
    :sswitch_2
    const-wide/16 v0, 0x2

    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbDeleteProfileRow(J)V

    goto :goto_0

    .line 1711
    :sswitch_3
    const-wide/16 v0, 0x3

    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbDeleteProfileRow(J)V

    goto :goto_0

    .line 1716
    :sswitch_4
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbDeleteSimInfoRow(J)V

    goto :goto_0

    .line 1721
    :sswitch_5
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbDeleteAccXListNodeRow(J)V

    goto :goto_0

    .line 1725
    :sswitch_6
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbDeleteResyncModeRow(J)V

    goto :goto_0

    .line 1729
    :sswitch_7
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlAgentInfoDeleteRow(J)V

    goto :goto_0

    .line 1734
    :sswitch_8
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlDeleteRow(J)V

    .line 1735
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInternalMemDeleteRow(J)V

    .line 1736
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExternalMemDeleteRow(J)V

    goto :goto_0

    .line 1741
    :sswitch_9
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlDeleteRow(J)V

    goto :goto_0

    .line 1745
    :sswitch_a
    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoDeleteRow(J)V

    goto :goto_0

    .line 1694
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_0
        0x52 -> :sswitch_1
        0x53 -> :sswitch_2
        0x54 -> :sswitch_3
        0x55 -> :sswitch_4
        0x56 -> :sswitch_6
        0x57 -> :sswitch_7
        0x58 -> :sswitch_8
        0x59 -> :sswitch_9
        0x60 -> :sswitch_a
        0x83 -> :sswitch_5
    .end sparse-switch
.end method

.method public static xdmDbSqlDelete(II)I
    .locals 2
    .param p0, "SqlID"    # I
    .param p1, "rowId"    # I

    .prologue
    .line 1757
    sparse-switch p0, :sswitch_data_0

    .line 1779
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1761
    :sswitch_0
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlDeleteRow(J)V

    .line 1762
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInternalMemDeleteRow(J)V

    .line 1763
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExternalMemDeleteRow(J)V

    goto :goto_0

    .line 1768
    :sswitch_1
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlDeleteRow(J)V

    goto :goto_0

    .line 1772
    :sswitch_2
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoDeleteRow(J)V

    goto :goto_0

    .line 1757
    :sswitch_data_0
    .sparse-switch
        0x58 -> :sswitch_0
        0x59 -> :sswitch_1
        0x60 -> :sswitch_2
    .end sparse-switch
.end method

.method public static xdmDbSqlDelete(ILjava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "SqlID"    # I
    .param p1, "szColName"    # Ljava/lang/String;
    .param p2, "szColData"    # Ljava/lang/String;

    .prologue
    .line 1784
    packed-switch p0, :pswitch_data_0

    .line 1794
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1787
    :pswitch_0
    invoke-static {p1, p2}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoDeleteRow(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1784
    nop

    :pswitch_data_0
    .packed-switch 0x60
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmDbSqlExistsRow(I)Z
    .locals 4
    .param p0, "SqlID"    # I

    .prologue
    const-wide/16 v2, 0x1

    .line 1510
    const/4 v0, 0x0

    .line 1512
    .local v0, "bExists":Z
    sparse-switch p0, :sswitch_data_0

    .line 1532
    :goto_0
    return v0

    .line 1516
    :sswitch_0
    invoke-static {v2, v3}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExistsRow(J)Z

    move-result v0

    .line 1517
    goto :goto_0

    .line 1521
    :sswitch_1
    invoke-static {v2, v3}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlExistsRow(J)Z

    move-result v0

    .line 1522
    goto :goto_0

    .line 1525
    :sswitch_2
    invoke-static {}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoExistsRow()Z

    move-result v0

    .line 1526
    goto :goto_0

    .line 1512
    :sswitch_data_0
    .sparse-switch
        0x58 -> :sswitch_0
        0x59 -> :sswitch_1
        0x60 -> :sswitch_2
    .end sparse-switch
.end method

.method public static xdmDbSqlRead(I)Ljava/lang/Object;
    .locals 8
    .param p0, "FileID"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    const-wide/16 v4, 0x1

    .line 1799
    sparse-switch p0, :sswitch_data_0

    .line 1865
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 1802
    :sswitch_0
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFetchProfileListRow(JLcom/fmm/dm/db/file/XDBProflieListInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 1803
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    goto :goto_0

    .line 1806
    :sswitch_1
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    new-instance v2, Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBProfileInfo;-><init>()V

    aput-object v2, v1, v3

    .line 1807
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    aget-object v1, v1, v3

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFetchProfileRow(JLcom/fmm/dm/db/file/XDBProfileInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    aput-object v1, v2, v3

    .line 1808
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    aget-object v1, v1, v3

    goto :goto_0

    .line 1811
    :sswitch_2
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    new-instance v2, Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBProfileInfo;-><init>()V

    aput-object v2, v1, v6

    .line 1812
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    const-wide/16 v4, 0x2

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    aget-object v1, v1, v6

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFetchProfileRow(JLcom/fmm/dm/db/file/XDBProfileInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    aput-object v1, v2, v6

    .line 1813
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    aget-object v1, v1, v6

    goto :goto_0

    .line 1816
    :sswitch_3
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    new-instance v2, Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBProfileInfo;-><init>()V

    aput-object v2, v1, v7

    .line 1817
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    const-wide/16 v4, 0x3

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    aget-object v1, v1, v7

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFetchProfileRow(JLcom/fmm/dm/db/file/XDBProfileInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    aput-object v1, v2, v7

    .line 1818
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    aget-object v1, v1, v7

    goto :goto_0

    .line 1821
    :sswitch_4
    new-instance v1, Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-direct {v1}, Lcom/fmm/dm/db/file/XDBInfoConRef;-><init>()V

    sput-object v1, Lcom/fmm/dm/db/file/XDB;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 1822
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFetchNetworkRow(JLcom/fmm/dm/db/file/XDBInfoConRef;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBInfoConRef;

    sput-object v1, Lcom/fmm/dm/db/file/XDB;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 1823
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    goto :goto_0

    .line 1826
    :sswitch_5
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFetchSimInfoRow(JLcom/fmm/dm/db/file/XDBSimInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBSimInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    .line 1827
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    goto/16 :goto_0

    .line 1830
    :sswitch_6
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 1832
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    new-instance v2, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;-><init>()V

    aput-object v2, v1, v0

    .line 1833
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v2, v1, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    int-to-long v4, v0

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aget-object v1, v1, v0

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFetchAccXListNodeRow(JLcom/fmm/dm/db/file/XDBAccXNodeInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aput-object v1, v2, v0

    .line 1830
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1835
    :cond_0
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    goto/16 :goto_0

    .line 1838
    .end local v0    # "i":I
    :sswitch_7
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFetchResyncModeRow(JLcom/fmm/dm/db/file/XDBResyncModeInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .line 1839
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    goto/16 :goto_0

    .line 1842
    :sswitch_8
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlAgentInfoFetchRow(JLcom/fmm/dm/db/file/XDBAgentInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBAgentInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    .line 1843
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    goto/16 :goto_0

    .line 1847
    :sswitch_9
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 1848
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInternalMemFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 1849
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExternalMemFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 1850
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    goto/16 :goto_0

    .line 1854
    :sswitch_a
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlFetchRow(JLcom/fmm/dm/db/file/XDBAMTInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBAMTInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 1856
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    goto/16 :goto_0

    .line 1859
    :sswitch_b
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-static {v1}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoFetchRow(Lcom/fmm/dm/db/file/XDBNotiInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBNotiInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    .line 1860
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    goto/16 :goto_0

    .line 1799
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_0
        0x52 -> :sswitch_1
        0x53 -> :sswitch_2
        0x54 -> :sswitch_3
        0x55 -> :sswitch_5
        0x56 -> :sswitch_7
        0x57 -> :sswitch_8
        0x58 -> :sswitch_9
        0x59 -> :sswitch_a
        0x60 -> :sswitch_b
        0x80 -> :sswitch_4
        0x83 -> :sswitch_6
    .end sparse-switch
.end method

.method public static xdmDbSqlRead(II)Ljava/lang/Object;
    .locals 4
    .param p0, "FileID"    # I
    .param p1, "rowId"    # I

    .prologue
    .line 1870
    sparse-switch p0, :sswitch_data_0

    .line 1892
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1874
    :sswitch_0
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    int-to-long v2, p1

    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v2, v3, v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 1875
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    int-to-long v2, p1

    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v2, v3, v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInternalMemFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 1876
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    int-to-long v2, p1

    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v2, v3, v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExternalMemFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 1877
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    goto :goto_0

    .line 1881
    :sswitch_1
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    int-to-long v2, p1

    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {v2, v3, v0}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlFetchRow(JLcom/fmm/dm/db/file/XDBAMTInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDBAMTInfo;

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 1883
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    goto :goto_0

    .line 1886
    :sswitch_2
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    int-to-long v2, p1

    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-static {v2, v3, v0}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoFetchRow(JLcom/fmm/dm/db/file/XDBNotiInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDBNotiInfo;

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    .line 1887
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    goto :goto_0

    .line 1870
    nop

    :sswitch_data_0
    .sparse-switch
        0x58 -> :sswitch_0
        0x59 -> :sswitch_1
        0x60 -> :sswitch_2
    .end sparse-switch
.end method

.method public static xdmDbSqlUpdate(ILjava/lang/Object;)V
    .locals 6
    .param p0, "SqlID"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v4, 0x1

    .line 1591
    sparse-switch p0, :sswitch_data_0

    .line 1661
    .end local p1    # "Input":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 1594
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_0
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 1595
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbUpdateProfileListRow(JLcom/fmm/dm/db/file/XDBProflieListInfo;)V

    goto :goto_0

    .line 1599
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_1
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 1600
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbUpdateProfileRow(JLcom/fmm/dm/db/file/XDBProfileInfo;)V

    goto :goto_0

    .line 1604
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_2
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 1605
    const-wide/16 v2, 0x2

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-static {v2, v3, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbUpdateProfileRow(JLcom/fmm/dm/db/file/XDBProfileInfo;)V

    goto :goto_0

    .line 1609
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_3
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 1610
    const-wide/16 v2, 0x3

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-static {v2, v3, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbUpdateProfileRow(JLcom/fmm/dm/db/file/XDBProfileInfo;)V

    goto :goto_0

    .line 1614
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_4
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    check-cast p1, Lcom/fmm/dm/db/file/XDBInfoConRef;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 1615
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbUpdateNetworkRow(JLcom/fmm/dm/db/file/XDBInfoConRef;)V

    goto :goto_0

    .line 1619
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_5
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBSimInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    .line 1620
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbUpdateSimInfoRow(JLcom/fmm/dm/db/file/XDBSimInfo;)V

    goto :goto_0

    .line 1624
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_6
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBAccXListNode;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    .line 1625
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 1626
    int-to-long v2, v0

    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aget-object v1, v1, v0

    invoke-static {v2, v3, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbUpdateAccXListNodeRow(JLcom/fmm/dm/db/file/XDBAccXNodeInfo;)V

    .line 1625
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1630
    .end local v0    # "i":I
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_7
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .line 1631
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbUpdateResyncModeRow(JLcom/fmm/dm/db/file/XDBResyncModeInfo;)V

    goto/16 :goto_0

    .line 1635
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_8
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/db/file/XDBAgentInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    .line 1636
    check-cast p1, Lcom/fmm/dm/db/file/XDBAgentInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {v4, v5, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlAgentInfoUpdateRow(JLcom/fmm/dm/db/file/XDBAgentInfo;)V

    goto/16 :goto_0

    .line 1641
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_9
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    move-object v1, p1

    .line 1642
    check-cast v1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V

    move-object v1, p1

    .line 1643
    check-cast v1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v4, v5, v1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInternalMemUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V

    .line 1644
    check-cast p1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {v4, v5, p1}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExternalMemUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V

    goto/16 :goto_0

    .line 1649
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_a
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/db/file/XDBAMTInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 1650
    check-cast p1, Lcom/fmm/dm/db/file/XDBAMTInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {v4, v5, p1}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlUpdateRow(JLcom/fmm/dm/db/file/XDBAMTInfo;)V

    goto/16 :goto_0

    .line 1654
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_b
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v1, p1

    check-cast v1, Lcom/fmm/dm/db/file/XDBNotiInfo;

    iput-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    .line 1655
    check-cast p1, Lcom/fmm/dm/db/file/XDBNotiInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {v4, v5, p1}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoUpdateRow(JLcom/fmm/dm/db/file/XDBNotiInfo;)V

    goto/16 :goto_0

    .line 1591
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_0
        0x52 -> :sswitch_1
        0x53 -> :sswitch_2
        0x54 -> :sswitch_3
        0x55 -> :sswitch_5
        0x56 -> :sswitch_7
        0x57 -> :sswitch_8
        0x58 -> :sswitch_9
        0x59 -> :sswitch_a
        0x60 -> :sswitch_b
        0x80 -> :sswitch_4
        0x83 -> :sswitch_6
    .end sparse-switch
.end method

.method public static xdmDbSqlUpdate(IILjava/lang/Object;)Z
    .locals 4
    .param p0, "SqlID"    # I
    .param p1, "rowId"    # I
    .param p2, "Input"    # Ljava/lang/Object;

    .prologue
    .line 1665
    sparse-switch p0, :sswitch_data_0

    .line 1689
    .end local p2    # "Input":Ljava/lang/Object;
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1669
    .restart local p2    # "Input":Ljava/lang/Object;
    :sswitch_0
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v0, p2

    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 1670
    int-to-long v2, p1

    move-object v0, p2

    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v2, v3, v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V

    .line 1671
    int-to-long v2, p1

    move-object v0, p2

    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v2, v3, v0}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInternalMemUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V

    .line 1672
    int-to-long v0, p1

    check-cast p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .end local p2    # "Input":Ljava/lang/Object;
    invoke-static {v0, v1, p2}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExternalMemUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V

    goto :goto_0

    .line 1677
    .restart local p2    # "Input":Ljava/lang/Object;
    :sswitch_1
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v0, p2

    check-cast v0, Lcom/fmm/dm/db/file/XDBAMTInfo;

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 1678
    int-to-long v0, p1

    check-cast p2, Lcom/fmm/dm/db/file/XDBAMTInfo;

    .end local p2    # "Input":Ljava/lang/Object;
    invoke-static {v0, v1, p2}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlUpdateRow(JLcom/fmm/dm/db/file/XDBAMTInfo;)V

    goto :goto_0

    .line 1682
    .restart local p2    # "Input":Ljava/lang/Object;
    :sswitch_2
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v0, p2

    check-cast v0, Lcom/fmm/dm/db/file/XDBNotiInfo;

    iput-object v0, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    .line 1683
    int-to-long v0, p1

    check-cast p2, Lcom/fmm/dm/db/file/XDBNotiInfo;

    .end local p2    # "Input":Ljava/lang/Object;
    invoke-static {v0, v1, p2}, Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;->xnotiDbSqlInfoUpdateRow(JLcom/fmm/dm/db/file/XDBNotiInfo;)V

    goto :goto_0

    .line 1665
    nop

    :sswitch_data_0
    .sparse-switch
        0x58 -> :sswitch_0
        0x59 -> :sswitch_1
        0x60 -> :sswitch_2
    .end sparse-switch
.end method

.method public static xdmDbUpdateAccXListNodeRow(JLcom/fmm/dm/db/file/XDBAccXNodeInfo;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "accxnode"    # Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    .prologue
    .line 1152
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1153
    .local v0, "args":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 1157
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "account"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAccount:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    const-string v3, "appaddr"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAppAddr:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    const-string v3, "appaddrport"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAppAddrPort:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const-string v3, "clientappauth"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szClientAppAuth:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161
    const-string v3, "serverappauth"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szServerAppAuth:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    const-string v3, "toconref"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szToConRef:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1165
    const-string v3, "accxlistnode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1173
    if-eqz v1, :cond_0

    .line 1174
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1176
    :cond_0
    :goto_0
    return-void

    .line 1167
    :catch_0
    move-exception v2

    .line 1169
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1173
    if-eqz v1, :cond_0

    .line 1174
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1173
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 1174
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbUpdateNetworkRow(JLcom/fmm/dm/db/file/XDBInfoConRef;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "conref"    # Lcom/fmm/dm/db/file/XDBInfoConRef;

    .prologue
    .line 473
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 474
    .local v0, "args":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 478
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "homeurl"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->m_szHomeUrl:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v3, "service"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->nService:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 480
    const-string v3, "active"

    iget-boolean v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->Active:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 481
    const-string v3, "proxyuse"

    iget-boolean v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 482
    const-string v3, "napnetworkprofilename"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const-string v3, "napbearer"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->nBearer:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 484
    const-string v3, "napaddrtype"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->nAddrType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 485
    const-string v3, "napaddr"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    const-string v3, "nappapid"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    const-string v3, "nappapsecret"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    const-string v3, "pxportnbr"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 489
    const-string v3, "pxaddrtype"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->nAddrType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 490
    const-string v3, "pxaddr"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const-string v3, "pxpapid"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const-string v3, "pxpapsecret"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefPX;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const-string v3, "staticipuse"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget-boolean v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticIpUse:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 494
    const-string v3, "staticip"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->m_szStaticIp:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v3, "staticdnsuse"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget-boolean v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->bStaticDnsUse:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 496
    const-string v3, "staticdns1"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->szStaticDns1:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 497
    const-string v3, "staticdns2"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->szStaticDns2:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 498
    const-string v3, "trafficclass"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;->nTrafficClass:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 500
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 501
    const-string v3, "network"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 509
    if-eqz v1, :cond_0

    .line 510
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 503
    :catch_0
    move-exception v2

    .line 505
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 509
    if-eqz v1, :cond_0

    .line 510
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 509
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 510
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbUpdateProfileListRow(JLcom/fmm/dm/db/file/XDBProflieListInfo;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "profilelist"    # Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .prologue
    .line 730
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 731
    .local v0, "args":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 735
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "networkconnname"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szNetworkConnName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    const-string v3, "proxyindex"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nProxyIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 737
    const-string v3, "profileindex"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 738
    const-string v3, "profilename1"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    const-string v3, "profilename2"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-string v3, "profilename3"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    const-string v3, "sessionid"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szSessionID:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    const-string v3, "notievent"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiEvent:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 743
    const-string v3, "destorynotitime"

    iget-wide v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nDestoryNotiTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 744
    const-string v3, "notiresyncmode"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiReSyncMode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 745
    const-string v3, "magicnumber"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->MagicNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 746
    const-string v3, "sessionsavestate"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 747
    const-string v3, "notiuievent"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 748
    const-string v3, "notiretrycount"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 749
    const-string v3, "status"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->eStatus:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 750
    const-string v3, "appid"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->appId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 751
    const-string v3, "uictype"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->UICType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 752
    const-string v3, "result"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->result:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 753
    const-string v3, "number"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->number:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 754
    const-string v3, "text"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->m_szText:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    const-string v3, "len"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->nLen:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 756
    const-string v3, "size"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v4, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->nSize:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 758
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 759
    const-string v3, "profilelist"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 767
    if-eqz v1, :cond_0

    .line 768
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 761
    :catch_0
    move-exception v2

    .line 763
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 767
    if-eqz v1, :cond_0

    .line 768
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 767
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 768
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbUpdateProfileRow(JLcom/fmm/dm/db/file/XDBProfileInfo;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "dminfo"    # Lcom/fmm/dm/db/file/XDBProfileInfo;

    .prologue
    .line 191
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 192
    .local v0, "args":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 196
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "protocol"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string v3, "serverport"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 198
    const-string v3, "serverurl"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v3, "serverip"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v3, "path"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v3, "protocol_org"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v3, "serverport_org"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 203
    const-string v3, "serverurl_org"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v3, "serverip_org"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v3, "path_org"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path_Org:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v3, "changedprotocol"

    iget-boolean v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 207
    const-string v3, "obextype"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ObexType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 208
    const-string v3, "authtype"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209
    const-string v3, "serverauthtype"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 210
    const-string v3, "appid"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v3, "authlevel"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v3, "serverauthlevel"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v3, "prefconref"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v3, "username"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 215
    const-string v3, "password"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 216
    const-string v3, "serverid"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 217
    const-string v3, "serverpwd"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 218
    const-string v3, "clientnonce"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v3, "servernonce"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v3, "servernonceformat"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonceFormat:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 221
    const-string v3, "clientnonceformat"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonceFormat:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 222
    const-string v3, "profilename"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v3, "networkconnname"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->NetworkConnName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v3, "networkconnindex"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->nNetworkConnIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 225
    const-string v3, "magicnumber"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBProfileInfo;->MagicNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 227
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 228
    const-string v3, "profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    if-eqz v1, :cond_0

    .line 237
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 230
    :catch_0
    move-exception v2

    .line 232
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    if-eqz v1, :cond_0

    .line 237
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 236
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 237
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbUpdateResyncModeRow(JLcom/fmm/dm/db/file/XDBResyncModeInfo;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "resyncmode"    # Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .prologue
    .line 1302
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1303
    .local v0, "args":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 1307
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "nonceresyncmode"

    iget-boolean v4, p2, Lcom/fmm/dm/db/file/XDBResyncModeInfo;->nNoceResyncMode:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1309
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1310
    const-string v3, "resyncmode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1318
    if-eqz v1, :cond_0

    .line 1319
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1321
    :cond_0
    :goto_0
    return-void

    .line 1312
    :catch_0
    move-exception v2

    .line 1314
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1318
    if-eqz v1, :cond_0

    .line 1319
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1318
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 1319
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdmDbUpdateSimInfoRow(JLcom/fmm/dm/db/file/XDBSimInfo;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "siminfo"    # Lcom/fmm/dm/db/file/XDBSimInfo;

    .prologue
    .line 997
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 998
    .local v0, "args":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 1002
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "imsi"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    const-string v3, "imsi2"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBSimInfo;->m_szIMSI2:Ljava/lang/String;

    const-string v5, "smldm"

    invoke-static {v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1006
    const-string v3, "siminfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1014
    if-eqz v1, :cond_0

    .line 1015
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1017
    :cond_0
    :goto_0
    return-void

    .line 1008
    :catch_0
    move-exception v2

    .line 1010
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1014
    if-eqz v1, :cond_0

    .line 1015
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1014
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 1015
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method
