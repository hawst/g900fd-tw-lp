.class public Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;
.super Ljava/lang/Object;
.source "XLAWMODbSqlQuery.java"

# interfaces
.implements Lcom/fmm/dm/db/sql/XLAWMODbSql;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xlawmoDbSqCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 19
    :try_start_0
    const-string v1, "create table if not exists LAWMO (rowid integer primary key autoincrement, State integer, NotifyUser boolean, Operation integer, ExtOperation integer, Pin text, ResultReportType integer, ResultReportSourceUri text, ResultReportTargetUri text, ResultCode text, AmtStatus integer, Registration boolean, SimChangeState boolean, Password text, LockMyPhoneMessages text, ReactivationLock text, CallRestrictionPhoneNumber text, CallRestrictionStatus text, RingMyPhoneStatus text, RingMyPhoneMessages text, Correlator text);"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 20
    const-string v1, "create table if not exists InternalMem (rowid integer primary key autoincrement, ListItemName text, ToBeWiped boolean);"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 21
    const-string v1, "create table if not exists ExternalMem (rowid integer primary key autoincrement, ListItemName text, ToBeWiped boolean);"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    :goto_0
    return-void

    .line 23
    :catch_0
    move-exception v0

    .line 25
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xlawmoDbSqlDeleteRow(J)V
    .locals 6
    .param p0, "RowId"    # J

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 91
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 92
    const-string v2, "LAWMO"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    if-eqz v0, :cond_0

    .line 101
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v1

    .line 96
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    if-eqz v0, :cond_0

    .line 101
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 100
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 101
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xlawmoDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 33
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS LAWMO"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 34
    const-string v1, "DROP TABLE IF EXISTS InternalMem"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 35
    const-string v1, "DROP TABLE IF EXISTS ExternalMem"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xlawmoDbSqlExistsRow(J)Z
    .locals 14
    .param p0, "RowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 235
    const/4 v11, 0x1

    .line 236
    .local v11, "bRet":Z
    const/4 v10, 0x0

    .line 237
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 238
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v1, 0x15

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "State"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "NotifyUser"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "Operation"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "ExtOperation"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "Pin"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "ResultReportType"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string v2, "ResultReportSourceUri"

    aput-object v2, v3, v1

    const/16 v1, 0x8

    const-string v2, "ResultReportTargetUri"

    aput-object v2, v3, v1

    const/16 v1, 0x9

    const-string v2, "ResultCode"

    aput-object v2, v3, v1

    const/16 v1, 0xa

    const-string v2, "AmtStatus"

    aput-object v2, v3, v1

    const/16 v1, 0xb

    const-string v2, "Registration"

    aput-object v2, v3, v1

    const/16 v1, 0xc

    const-string v2, "SimChangeState"

    aput-object v2, v3, v1

    const/16 v1, 0xd

    const-string v2, "Password"

    aput-object v2, v3, v1

    const/16 v1, 0xe

    const-string v2, "LockMyPhoneMessages"

    aput-object v2, v3, v1

    const/16 v1, 0xf

    const-string v2, "ReactivationLock"

    aput-object v2, v3, v1

    const/16 v1, 0x10

    const-string v2, "CallRestrictionPhoneNumber"

    aput-object v2, v3, v1

    const/16 v1, 0x11

    const-string v2, "CallRestrictionStatus"

    aput-object v2, v3, v1

    const/16 v1, 0x12

    const-string v2, "RingMyPhoneStatus"

    aput-object v2, v3, v1

    const/16 v1, 0x13

    const-string v2, "RingMyPhoneMessages"

    aput-object v2, v3, v1

    const/16 v1, 0x14

    const-string v2, "Correlator"

    aput-object v2, v3, v1

    .line 263
    .local v3, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 264
    const/4 v1, 0x1

    const-string v2, "LAWMO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 266
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 268
    const/4 v11, 0x1

    .line 282
    :goto_0
    if-eqz v10, :cond_0

    .line 283
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 285
    :cond_0
    if-eqz v0, :cond_1

    .line 286
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 289
    :cond_1
    :goto_1
    return v11

    .line 272
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 275
    :catch_0
    move-exception v12

    .line 277
    .local v12, "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    .line 278
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282
    if-eqz v10, :cond_3

    .line 283
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 285
    :cond_3
    if-eqz v0, :cond_1

    .line 286
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 282
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 283
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 285
    :cond_4
    if-eqz v0, :cond_5

    .line 286
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xlawmoDbSqlExternalMemDeleteRow(J)V
    .locals 6
    .param p0, "RowId"    # J

    .prologue
    .line 467
    const/4 v0, 0x0

    .line 471
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 472
    const-string v2, "ExternalMem"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 480
    if-eqz v0, :cond_0

    .line 481
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 483
    :cond_0
    :goto_0
    return-void

    .line 474
    :catch_0
    move-exception v1

    .line 476
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480
    if-eqz v0, :cond_0

    .line 481
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 480
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 481
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xlawmoDbSqlExternalMemExistsRow(J)Z
    .locals 14
    .param p0, "RowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 553
    const/4 v11, 0x1

    .line 554
    .local v11, "bRet":Z
    const/4 v10, 0x0

    .line 555
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 556
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "ListItemName"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "ToBeWiped"

    aput-object v2, v3, v1

    .line 561
    .local v3, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 562
    const/4 v1, 0x1

    const-string v2, "ExternalMem"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 564
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 566
    const/4 v11, 0x1

    .line 580
    :goto_0
    if-eqz v10, :cond_0

    .line 581
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 583
    :cond_0
    if-eqz v0, :cond_1

    .line 584
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 587
    :cond_1
    :goto_1
    return v11

    .line 570
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 573
    :catch_0
    move-exception v12

    .line 575
    .local v12, "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    .line 576
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580
    if-eqz v10, :cond_3

    .line 581
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 583
    :cond_3
    if-eqz v0, :cond_1

    .line 584
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 580
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 581
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 583
    :cond_4
    if-eqz v0, :cond_5

    .line 584
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xlawmoDbSqlExternalMemFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "RowId"    # J
    .param p2, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 511
    const/4 v12, 0x0

    .line 512
    .local v12, "Cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 513
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x3

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "ListItemName"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "ToBeWiped"

    aput-object v4, v5, v3

    .line 518
    .local v5, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 519
    const/4 v3, 0x1

    const-string v4, "ExternalMem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 521
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_5

    .line 523
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 526
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 527
    .local v14, "Id":J
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    .line 528
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_2

    .line 529
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 535
    .end local v14    # "Id":J
    :catch_0
    move-exception v13

    .line 537
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 541
    if-eqz v12, :cond_0

    .line 542
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 544
    :cond_0
    if-eqz v2, :cond_1

    .line 545
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 548
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 531
    .restart local v14    # "Id":J
    :cond_2
    :try_start_2
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 541
    .end local v14    # "Id":J
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_3

    .line 542
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 544
    :cond_3
    if-eqz v2, :cond_4

    .line 545
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_4
    throw v3

    .line 541
    :cond_5
    if-eqz v12, :cond_6

    .line 542
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 544
    :cond_6
    if-eqz v2, :cond_1

    .line 545
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1
.end method

.method public static xlawmoDbSqlExternalMemInsertRow(Lcom/fmm/dm/db/file/XDBLawmoInfo;)V
    .locals 5
    .param p0, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 443
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 444
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 448
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "ListItemName"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v3, "ToBeWiped"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-boolean v4, v4, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 451
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 452
    const-string v3, "ExternalMem"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    if-eqz v1, :cond_0

    .line 461
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 454
    :catch_0
    move-exception v2

    .line 456
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460
    if-eqz v1, :cond_0

    .line 461
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 460
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 461
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xlawmoDbSqlExternalMemUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V
    .locals 6
    .param p0, "RowId"    # J
    .param p2, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 487
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 488
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 492
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "ListItemName"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const-string v3, "ToBeWiped"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-boolean v4, v4, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 495
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 496
    const-string v3, "ExternalMem"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    if-eqz v1, :cond_0

    .line 505
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 498
    :catch_0
    move-exception v2

    .line 500
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    if-eqz v1, :cond_0

    .line 505
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 504
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 505
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xlawmoDbSqlFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "RowId"    # J
    .param p2, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 149
    const/4 v12, 0x0

    .line 150
    .local v12, "Cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 151
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v3, 0x15

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "State"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "NotifyUser"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "Operation"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "ExtOperation"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string v4, "Pin"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string v4, "ResultReportType"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string v4, "ResultReportSourceUri"

    aput-object v4, v5, v3

    const/16 v3, 0x8

    const-string v4, "ResultReportTargetUri"

    aput-object v4, v5, v3

    const/16 v3, 0x9

    const-string v4, "ResultCode"

    aput-object v4, v5, v3

    const/16 v3, 0xa

    const-string v4, "AmtStatus"

    aput-object v4, v5, v3

    const/16 v3, 0xb

    const-string v4, "Registration"

    aput-object v4, v5, v3

    const/16 v3, 0xc

    const-string v4, "SimChangeState"

    aput-object v4, v5, v3

    const/16 v3, 0xd

    const-string v4, "Password"

    aput-object v4, v5, v3

    const/16 v3, 0xe

    const-string v4, "LockMyPhoneMessages"

    aput-object v4, v5, v3

    const/16 v3, 0xf

    const-string v4, "ReactivationLock"

    aput-object v4, v5, v3

    const/16 v3, 0x10

    const-string v4, "CallRestrictionPhoneNumber"

    aput-object v4, v5, v3

    const/16 v3, 0x11

    const-string v4, "CallRestrictionStatus"

    aput-object v4, v5, v3

    const/16 v3, 0x12

    const-string v4, "RingMyPhoneStatus"

    aput-object v4, v5, v3

    const/16 v3, 0x13

    const-string v4, "RingMyPhoneMessages"

    aput-object v4, v5, v3

    const/16 v3, 0x14

    const-string v4, "Correlator"

    aput-object v4, v5, v3

    .line 176
    .local v5, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 177
    const/4 v3, 0x1

    const-string v4, "LAWMO"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 179
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_7

    .line 181
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 184
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 185
    .local v14, "Id":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nState:I

    .line 186
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_2

    .line 187
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bNotifyUser:Z

    .line 190
    :goto_1
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nOperation:I

    .line 191
    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nExtOperation:I

    .line 192
    const/4 v3, 0x5

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPin:Ljava/lang/String;

    .line 193
    const/4 v3, 0x6

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nResultReportType:I

    .line 194
    const/4 v3, 0x7

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    .line 195
    const/16 v3, 0x8

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    .line 196
    const/16 v3, 0x9

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultCode:Ljava/lang/String;

    .line 197
    const/16 v3, 0xa

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nAmtStaus:I

    .line 198
    const/16 v3, 0xb

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_5

    .line 199
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    .line 202
    :goto_2
    const/16 v3, 0xc

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_6

    .line 203
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bSimChangeState:Z

    .line 206
    :goto_3
    const/16 v3, 0xd

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPassword:Ljava/lang/String;

    .line 207
    const/16 v3, 0xe

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szLockMyPhoneMessages:Ljava/lang/String;

    .line 208
    const/16 v3, 0xf

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szReactivationLock:Ljava/lang/String;

    .line 209
    const/16 v3, 0x10

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionPhoneNumber:Ljava/lang/String;

    .line 210
    const/16 v3, 0x11

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionStatus:Ljava/lang/String;

    .line 211
    const/16 v3, 0x12

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneStatus:Ljava/lang/String;

    .line 212
    const/16 v3, 0x13

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneMessages:Ljava/lang/String;

    .line 213
    const/16 v3, 0x14

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCorrelator:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 217
    .end local v14    # "Id":J
    :catch_0
    move-exception v13

    .line 219
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 223
    if-eqz v12, :cond_0

    .line 224
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 226
    :cond_0
    if-eqz v2, :cond_1

    .line 227
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 230
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_4
    return-object p2

    .line 189
    .restart local v14    # "Id":J
    :cond_2
    const/4 v3, 0x1

    :try_start_2
    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bNotifyUser:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 223
    .end local v14    # "Id":J
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_3

    .line 224
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 226
    :cond_3
    if-eqz v2, :cond_4

    .line 227
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_4
    throw v3

    .line 201
    .restart local v14    # "Id":J
    :cond_5
    const/4 v3, 0x1

    :try_start_3
    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    goto/16 :goto_2

    .line 205
    :cond_6
    const/4 v3, 0x1

    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bSimChangeState:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 223
    .end local v14    # "Id":J
    :cond_7
    if-eqz v12, :cond_8

    .line 224
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 226
    :cond_8
    if-eqz v2, :cond_1

    .line 227
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_4
.end method

.method public static xlawmoDbSqlInsertRow(Lcom/fmm/dm/db/file/XDBLawmoInfo;)V
    .locals 5
    .param p0, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 45
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 46
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 50
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "State"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 51
    const-string v3, "NotifyUser"

    iget-boolean v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bNotifyUser:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 52
    const-string v3, "Operation"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nOperation:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 53
    const-string v3, "ExtOperation"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nExtOperation:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 54
    const-string v3, "Pin"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPin:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v3, "ResultReportType"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nResultReportType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 56
    const-string v3, "ResultReportSourceUri"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v3, "ResultReportTargetUri"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v3, "ResultCode"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultCode:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v3, "AmtStatus"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nAmtStaus:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 60
    const-string v3, "Registration"

    iget-boolean v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 61
    const-string v3, "SimChangeState"

    iget-boolean v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bSimChangeState:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 62
    const-string v3, "Password"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v3, "LockMyPhoneMessages"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szLockMyPhoneMessages:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v3, "ReactivationLock"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szReactivationLock:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v3, "CallRestrictionPhoneNumber"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v3, "CallRestrictionStatus"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionStatus:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v3, "RingMyPhoneStatus"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneStatus:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "RingMyPhoneMessages"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneMessages:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "Correlator"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCorrelator:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 72
    const-string v3, "LAWMO"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    if-eqz v1, :cond_0

    .line 81
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v2

    .line 76
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    if-eqz v1, :cond_0

    .line 81
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 80
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 81
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xlawmoDbSqlInternalMemDeleteRow(J)V
    .locals 6
    .param p0, "RowId"    # J

    .prologue
    .line 318
    const/4 v0, 0x0

    .line 322
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 323
    const-string v2, "InternalMem"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    if-eqz v0, :cond_0

    .line 332
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 325
    :catch_0
    move-exception v1

    .line 327
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331
    if-eqz v0, :cond_0

    .line 332
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 331
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 332
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xlawmoDbSqlInternalMemExistsRow(J)Z
    .locals 14
    .param p0, "RowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 404
    const/4 v11, 0x1

    .line 405
    .local v11, "bRet":Z
    const/4 v10, 0x0

    .line 406
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 407
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "ListItemName"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "ToBeWiped"

    aput-object v2, v3, v1

    .line 412
    .local v3, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 413
    const/4 v1, 0x1

    const-string v2, "InternalMem"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 415
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 417
    const/4 v11, 0x1

    .line 431
    :goto_0
    if-eqz v10, :cond_0

    .line 432
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 434
    :cond_0
    if-eqz v0, :cond_1

    .line 435
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 438
    :cond_1
    :goto_1
    return v11

    .line 421
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 424
    :catch_0
    move-exception v12

    .line 426
    .local v12, "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    .line 427
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 431
    if-eqz v10, :cond_3

    .line 432
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 434
    :cond_3
    if-eqz v0, :cond_1

    .line 435
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 431
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 432
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 434
    :cond_4
    if-eqz v0, :cond_5

    .line 435
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xlawmoDbSqlInternalMemFetchRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "RowId"    # J
    .param p2, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 362
    const/4 v12, 0x0

    .line 363
    .local v12, "Cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 364
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x3

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "ListItemName"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "ToBeWiped"

    aput-object v4, v5, v3

    .line 369
    .local v5, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 370
    const/4 v3, 0x1

    const-string v4, "InternalMem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 372
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_5

    .line 374
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 377
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 378
    .local v14, "Id":J
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    .line 379
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_2

    .line 380
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 386
    .end local v14    # "Id":J
    :catch_0
    move-exception v13

    .line 388
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392
    if-eqz v12, :cond_0

    .line 393
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_0
    if-eqz v2, :cond_1

    .line 396
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 399
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 382
    .restart local v14    # "Id":J
    :cond_2
    :try_start_2
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 392
    .end local v14    # "Id":J
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_3

    .line 393
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_3
    if-eqz v2, :cond_4

    .line 396
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_4
    throw v3

    .line 392
    :cond_5
    if-eqz v12, :cond_6

    .line 393
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_6
    if-eqz v2, :cond_1

    .line 396
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1
.end method

.method public static xlawmoDbSqlInternalMemInsertRow(Lcom/fmm/dm/db/file/XDBLawmoInfo;)V
    .locals 5
    .param p0, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 294
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 295
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 299
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "ListItemName"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const-string v3, "ToBeWiped"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-boolean v4, v4, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 302
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 303
    const-string v3, "InternalMem"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    if-eqz v1, :cond_0

    .line 312
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 305
    :catch_0
    move-exception v2

    .line 307
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    if-eqz v1, :cond_0

    .line 312
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 311
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 312
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xlawmoDbSqlInternalMemUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V
    .locals 6
    .param p0, "RowId"    # J
    .param p2, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 338
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 339
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 343
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "ListItemName"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v3, "ToBeWiped"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-boolean v4, v4, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 346
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 347
    const-string v3, "InternalMem"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    if-eqz v1, :cond_0

    .line 356
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 349
    :catch_0
    move-exception v2

    .line 351
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355
    if-eqz v1, :cond_0

    .line 356
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 355
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 356
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xlawmoDbSqlUpdateRow(JLcom/fmm/dm/db/file/XDBLawmoInfo;)V
    .locals 6
    .param p0, "RowId"    # J
    .param p2, "LawmoInfo"    # Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .prologue
    .line 107
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 108
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 112
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "State"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 113
    const-string v3, "NotifyUser"

    iget-boolean v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bNotifyUser:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 114
    const-string v3, "Operation"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nOperation:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 115
    const-string v3, "ExtOperation"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nExtOperation:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 116
    const-string v3, "Pin"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPin:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v3, "ResultReportType"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nResultReportType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 118
    const-string v3, "ResultReportSourceUri"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v3, "ResultReportTargetUri"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v3, "ResultCode"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultCode:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v3, "AmtStatus"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nAmtStaus:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    const-string v3, "Registration"

    iget-boolean v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 123
    const-string v3, "SimChangeState"

    iget-boolean v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bSimChangeState:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 124
    const-string v3, "Password"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPassword:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v3, "LockMyPhoneMessages"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szLockMyPhoneMessages:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v3, "ReactivationLock"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szReactivationLock:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v3, "CallRestrictionPhoneNumber"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v3, "CallRestrictionStatus"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionStatus:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v3, "RingMyPhoneStatus"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneStatus:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v3, "RingMyPhoneMessages"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneMessages:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v3, "Correlator"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCorrelator:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 134
    const-string v3, "LAWMO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    if-eqz v1, :cond_0

    .line 143
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v2

    .line 138
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    if-eqz v1, :cond_0

    .line 143
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 142
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 143
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method
