.class public interface abstract Lcom/fmm/dm/db/sql/XDMDbSql;
.super Ljava/lang/Object;
.source "XDMDbSql.java"


# static fields
.field public static final DATABASE_ACCXLISTNODE_CREATE:Ljava/lang/String; = "create table if not exists accxlistnode (rowid integer primary key autoincrement, account text, appaddr text, appaddrport text, clientappauth text, serverappauth text, toconref text);"

.field public static final DATABASE_DM_AGENT_INFO_CREATE:Ljava/lang/String; = "create table if not exists DmAgnetInfo (rowid integer primary key autoincrement, AgentType integer);"

.field public static final DATABASE_NAME:Ljava/lang/String; = "fmmdm.db"

.field public static final DATABASE_NETWORK_CREATE:Ljava/lang/String; = "create table if not exists network (rowid integer primary key autoincrement, homeurl text, service integer, active integer, proxyuse integer, napnetworkprofilename text, napbearer integer, napaddrtype integer, napaddr text, nappapid text, nappapsecret text, pxportnbr integer, pxaddrtype integer, pxaddr text, pxpapid text, pxpapsecret text, staticipuse integer, staticip text, staticdnsuse integer, staticdns1 integer, staticdns2 integer, trafficclass integer);"

.field public static final DATABASE_PROFILELIST_CREATE:Ljava/lang/String; = "create table if not exists profilelist (rowid integer primary key autoincrement, networkconnname text, proxyindex integer, profileindex integer, profilename1 text, profilename2 text, profilename3 text, sessionid text, notievent integer, destorynotitime integer, notiresyncmode integer, magicnumber integer, sessionsavestate integer, notiuievent integer, notiretrycount integer, status integer, appid integer, uictype integer, result integer, number integer, text text, len integer, size integer);"

.field public static final DATABASE_PROFILE_CREATE:Ljava/lang/String; = "create table if not exists profile (rowid integer primary key autoincrement, protocol text, serverport integer, serverurl text, serverip text, path text, protocol_org text, serverport_org integer, serverurl_org text, serverip_org text, path_org text, changedprotocol integer, obextype integer, authtype integer, serverauthtype integer, appid text, authlevel text, serverauthlevel text, prefconref text, username byte, password byte, serverid byte, serverpwd byte, clientnonce text, servernonce text, servernonceformat integer, clientnonceformat integer, profilename text, networkconnname text, networkconnindex integer, magicnumber integer);"

.field public static final DATABASE_RESYNCMODE_CREATE:Ljava/lang/String; = "create table if not exists resyncmode (rowid integer primary key autoincrement, nonceresyncmode integer);"

.field public static final DATABASE_SIMINFO_CREATE:Ljava/lang/String; = "create table if not exists siminfo (rowid integer primary key autoincrement, imsi text, imsi2 text);"

.field public static final DATABASE_VERSION:I = 0x3

.field public static final XDB_ACCXLISTNODE_TABLE:Ljava/lang/String; = "accxlistnode"

.field public static final XDB_DM_AGENT_INFO_TABLE:Ljava/lang/String; = "DmAgnetInfo"

.field public static final XDB_NETWORK_TABLE:Ljava/lang/String; = "network"

.field public static final XDB_PROFILELIST_TABLE:Ljava/lang/String; = "profilelist"

.field public static final XDB_PROFILE_TABLE:Ljava/lang/String; = "profile"

.field public static final XDB_RESYNCMODE_TABLE:Ljava/lang/String; = "resyncmode"

.field public static final XDB_SIMINFO_TABLE:Ljava/lang/String; = "siminfo"

.field public static final XDB_SQL_ERR_COLUMN_NOT_FOUND:I = 0x3

.field public static final XDB_SQL_ERR_EXIST:I = 0x1

.field public static final XDB_SQL_ERR_TABLE_NOT_FOUND:I = 0x2

.field public static final XDB_SQL_FAIL:I = 0x4

.field public static final XDB_SQL_OK:I = 0x0

.field public static final XDM_SQL_DB_ACCXLISTNODE_ACCOUNT:Ljava/lang/String; = "account"

.field public static final XDM_SQL_DB_ACCXLISTNODE_APPADDR:Ljava/lang/String; = "appaddr"

.field public static final XDM_SQL_DB_ACCXLISTNODE_APPADDRPORT:Ljava/lang/String; = "appaddrport"

.field public static final XDM_SQL_DB_ACCXLISTNODE_CLIENTAPPAUTH:Ljava/lang/String; = "clientappauth"

.field public static final XDM_SQL_DB_ACCXLISTNODE_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_ACCXLISTNODE_SERVERAPPAUTH:Ljava/lang/String; = "serverappauth"

.field public static final XDM_SQL_DB_ACCXLISTNODE_TOCONREF:Ljava/lang/String; = "toconref"

.field public static final XDM_SQL_DB_AGENT_INFO_AGENT_TYPE:Ljava/lang/String; = "AgentType"

.field public static final XDM_SQL_DB_NETWORK1_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_NETWORK_ACTIVE:Ljava/lang/String; = "active"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICDNS1:Ljava/lang/String; = "staticdns1"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICDNS2:Ljava/lang/String; = "staticdns2"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICDNSUSE:Ljava/lang/String; = "staticdnsuse"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICIP:Ljava/lang/String; = "staticip"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICIPUSE:Ljava/lang/String; = "staticipuse"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_TRAFFICCLASS:Ljava/lang/String; = "trafficclass"

.field public static final XDM_SQL_DB_NETWORK_HOMEURL:Ljava/lang/String; = "homeurl"

.field public static final XDM_SQL_DB_NETWORK_NAP_ADDR:Ljava/lang/String; = "napaddr"

.field public static final XDM_SQL_DB_NETWORK_NAP_ADDRTYPE:Ljava/lang/String; = "napaddrtype"

.field public static final XDM_SQL_DB_NETWORK_NAP_AUTH_PAPID:Ljava/lang/String; = "nappapid"

.field public static final XDM_SQL_DB_NETWORK_NAP_AUTH_PAPSECRET:Ljava/lang/String; = "nappapsecret"

.field public static final XDM_SQL_DB_NETWORK_NAP_BEARER:Ljava/lang/String; = "napbearer"

.field public static final XDM_SQL_DB_NETWORK_NAP_NETWORKPROFILENAME:Ljava/lang/String; = "napnetworkprofilename"

.field public static final XDM_SQL_DB_NETWORK_PROXYUSE:Ljava/lang/String; = "proxyuse"

.field public static final XDM_SQL_DB_NETWORK_PX_ADDR:Ljava/lang/String; = "pxaddr"

.field public static final XDM_SQL_DB_NETWORK_PX_ADDRTYPE:Ljava/lang/String; = "pxaddrtype"

.field public static final XDM_SQL_DB_NETWORK_PX_AUTH_PAPID:Ljava/lang/String; = "pxpapid"

.field public static final XDM_SQL_DB_NETWORK_PX_AUTH_PAPSECRET:Ljava/lang/String; = "pxpapsecret"

.field public static final XDM_SQL_DB_NETWORK_PX_PORTNBR:Ljava/lang/String; = "pxportnbr"

.field public static final XDM_SQL_DB_NETWORK_SERVICE:Ljava/lang/String; = "service"

.field public static final XDM_SQL_DB_PROFILE1_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_PROFILE2_ROWID:J = 0x2L

.field public static final XDM_SQL_DB_PROFILE3_ROWID:J = 0x3L

.field public static final XDM_SQL_DB_PROFILELIST_DESTORYNOTITIME:Ljava/lang/String; = "destorynotitime"

.field public static final XDM_SQL_DB_PROFILELIST_MAGICNUMBER:Ljava/lang/String; = "magicnumber"

.field public static final XDM_SQL_DB_PROFILELIST_NETWORKCONNNAME:Ljava/lang/String; = "networkconnname"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIEVENT:Ljava/lang/String; = "notievent"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIRESUMESTATE_NOTIRETRYCOUNT:Ljava/lang/String; = "notiretrycount"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIRESUMESTATE_NOTIUIEVENT:Ljava/lang/String; = "notiuievent"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIRESUMESTATE_SESSIONSAVESTATE:Ljava/lang/String; = "sessionsavestate"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIRESYNCMODE:Ljava/lang/String; = "notiresyncmode"

.field public static final XDM_SQL_DB_PROFILELIST_PROFILEINDEX:Ljava/lang/String; = "profileindex"

.field public static final XDM_SQL_DB_PROFILELIST_PROFILENAME1:Ljava/lang/String; = "profilename1"

.field public static final XDM_SQL_DB_PROFILELIST_PROFILENAME2:Ljava/lang/String; = "profilename2"

.field public static final XDM_SQL_DB_PROFILELIST_PROFILENAME3:Ljava/lang/String; = "profilename3"

.field public static final XDM_SQL_DB_PROFILELIST_PROXYINDEX:Ljava/lang/String; = "proxyindex"

.field public static final XDM_SQL_DB_PROFILELIST_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_PROFILELIST_SESSIONID:Ljava/lang/String; = "sessionid"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_APPID:Ljava/lang/String; = "appid"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_LEN:Ljava/lang/String; = "len"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_NUMBER:Ljava/lang/String; = "number"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_RESULT:Ljava/lang/String; = "result"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_SIZE:Ljava/lang/String; = "size"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_STATUS:Ljava/lang/String; = "status"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_TEXT:Ljava/lang/String; = "text"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_UICTYPE:Ljava/lang/String; = "uictype"

.field public static final XDM_SQL_DB_PROFILE_APPID:Ljava/lang/String; = "appid"

.field public static final XDM_SQL_DB_PROFILE_AUTHLEVEL:Ljava/lang/String; = "authlevel"

.field public static final XDM_SQL_DB_PROFILE_AUTHTYPE:Ljava/lang/String; = "authtype"

.field public static final XDM_SQL_DB_PROFILE_CHANGEDPROTOCOL:Ljava/lang/String; = "changedprotocol"

.field public static final XDM_SQL_DB_PROFILE_CLIENTNONCE:Ljava/lang/String; = "clientnonce"

.field public static final XDM_SQL_DB_PROFILE_CLIENTNONCEFORMAT:Ljava/lang/String; = "clientnonceformat"

.field public static final XDM_SQL_DB_PROFILE_MAGICNUMBER:Ljava/lang/String; = "magicnumber"

.field public static final XDM_SQL_DB_PROFILE_NETWORKCONNINDEX:Ljava/lang/String; = "networkconnindex"

.field public static final XDM_SQL_DB_PROFILE_NETWORKCONNNAME:Ljava/lang/String; = "networkconnname"

.field public static final XDM_SQL_DB_PROFILE_OBEXTYPE:Ljava/lang/String; = "obextype"

.field public static final XDM_SQL_DB_PROFILE_PASSWORD:Ljava/lang/String; = "password"

.field public static final XDM_SQL_DB_PROFILE_PATH:Ljava/lang/String; = "path"

.field public static final XDM_SQL_DB_PROFILE_PATH_ORG:Ljava/lang/String; = "path_org"

.field public static final XDM_SQL_DB_PROFILE_PREFCONREF:Ljava/lang/String; = "prefconref"

.field public static final XDM_SQL_DB_PROFILE_PROFILENAME:Ljava/lang/String; = "profilename"

.field public static final XDM_SQL_DB_PROFILE_PROTOCOL:Ljava/lang/String; = "protocol"

.field public static final XDM_SQL_DB_PROFILE_PROTOCOL_ORG:Ljava/lang/String; = "protocol_org"

.field public static final XDM_SQL_DB_PROFILE_SERVERAUTHLEVEL:Ljava/lang/String; = "serverauthlevel"

.field public static final XDM_SQL_DB_PROFILE_SERVERAUTHTYPE:Ljava/lang/String; = "serverauthtype"

.field public static final XDM_SQL_DB_PROFILE_SERVERID:Ljava/lang/String; = "serverid"

.field public static final XDM_SQL_DB_PROFILE_SERVERIP:Ljava/lang/String; = "serverip"

.field public static final XDM_SQL_DB_PROFILE_SERVERIP_ORG:Ljava/lang/String; = "serverip_org"

.field public static final XDM_SQL_DB_PROFILE_SERVERNONCE:Ljava/lang/String; = "servernonce"

.field public static final XDM_SQL_DB_PROFILE_SERVERNONCEFORMAT:Ljava/lang/String; = "servernonceformat"

.field public static final XDM_SQL_DB_PROFILE_SERVERPORT:Ljava/lang/String; = "serverport"

.field public static final XDM_SQL_DB_PROFILE_SERVERPORT_ORG:Ljava/lang/String; = "serverport_org"

.field public static final XDM_SQL_DB_PROFILE_SERVERPWD:Ljava/lang/String; = "serverpwd"

.field public static final XDM_SQL_DB_PROFILE_SERVERURL:Ljava/lang/String; = "serverurl"

.field public static final XDM_SQL_DB_PROFILE_SERVERURL_ORG:Ljava/lang/String; = "serverurl_org"

.field public static final XDM_SQL_DB_PROFILE_USERNAME:Ljava/lang/String; = "username"

.field public static final XDM_SQL_DB_RESYNCMODE_NONCERESYNCMODE:Ljava/lang/String; = "nonceresyncmode"

.field public static final XDM_SQL_DB_RESYNCMODE_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_ROWID:Ljava/lang/String; = "rowid"

.field public static final XDM_SQL_DB_SIMINFO_IMSI:Ljava/lang/String; = "imsi"

.field public static final XDM_SQL_DB_SIMINFO_IMSI2:Ljava/lang/String; = "imsi2"

.field public static final XDM_SQL_DB_SIMINFO_ROWID:J = 0x1L

.field public static final xdmSqlDbIdAMTInfo:I = 0x59

.field public static final xdmSqlDbIdAccXNode:I = 0x83

.field public static final xdmSqlDbIdDmAgentInfo:I = 0x57

.field public static final xdmSqlDbIdIMSIInfo:I = 0x55

.field public static final xdmSqlDbIdLawmoInfo:I = 0x58

.field public static final xdmSqlDbIdNetworkInfo:I = 0x80

.field public static final xdmSqlDbIdNotiInfo:I = 0x60

.field public static final xdmSqlDbIdProfileInfo1:I = 0x52

.field public static final xdmSqlDbIdProfileInfo2:I = 0x53

.field public static final xdmSqlDbIdProfileInfo3:I = 0x54

.field public static final xdmSqlDbIdProfileList:I = 0x51

.field public static final xdmSqlDbIdResyncMode:I = 0x56
