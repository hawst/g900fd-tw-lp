.class public Lcom/fmm/dm/db/sql/XNOTIDbSqlQuery;
.super Ljava/lang/Object;
.source "XNOTIDbSqlQuery.java"

# interfaces
.implements Lcom/fmm/dm/db/sql/XNOTIDbSql;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xnotiDbSqlCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 17
    :try_start_0
    const-string v1, "create table if not exists NOTIFICATION (rowid integer primary key autoincrement, appId integer, uiMode integer, sessinoId text, serverId text);"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :goto_0
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 21
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xnotiDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 29
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS NOTIFICATION"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :goto_0
    return-void

    .line 31
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xnotiDbSqlInfoDeleteRow(J)V
    .locals 6
    .param p0, "rowId"    # J

    .prologue
    .line 65
    const/4 v0, 0x0

    .line 69
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 70
    const-string v2, "NOTIFICATION"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v1

    .line 74
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 78
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 79
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xnotiDbSqlInfoDeleteRow(Ljava/lang/String;I)V
    .locals 5
    .param p0, "szColName"    # Ljava/lang/String;
    .param p1, "colData"    # I

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 89
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 90
    const-string v2, "NOTIFICATION"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 98
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 99
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xnotiDbSqlInfoDeleteRow(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "szColName"    # Ljava/lang/String;
    .param p1, "szColData"    # Ljava/lang/String;

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 109
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 110
    const-string v2, "NOTIFICATION"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 118
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 119
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xnotiDbSqlInfoExistsRow(Ljava/lang/String;Ljava/lang/String;)I
    .locals 13
    .param p0, "szColName"    # Ljava/lang/String;
    .param p1, "szColData"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 306
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const-string v1, "rowId"

    aput-object v1, v3, v2

    const-string v1, "appId"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "uiMode"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "sessinoId"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "serverId"

    aput-object v2, v3, v1

    .line 308
    .local v3, "From":[Ljava/lang/String;
    const/4 v12, -0x1

    .line 309
    .local v12, "nRowId":I
    const/4 v10, 0x0

    .line 310
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 313
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 314
    const/4 v1, 0x1

    const-string v2, "NOTIFICATION"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 316
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 318
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    .line 319
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    .line 328
    :cond_0
    if-eqz v10, :cond_1

    .line 329
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 331
    :cond_1
    if-eqz v0, :cond_2

    .line 332
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 335
    :cond_2
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rowID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 336
    return v12

    .line 322
    :catch_0
    move-exception v11

    .line 324
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 328
    if-eqz v10, :cond_3

    .line 329
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 331
    :cond_3
    if-eqz v0, :cond_2

    .line 332
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 328
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 329
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 331
    :cond_4
    if-eqz v0, :cond_5

    .line 332
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xnotiDbSqlInfoExistsRow()Z
    .locals 13

    .prologue
    const/4 v4, 0x1

    .line 228
    const/4 v11, 0x1

    .line 229
    .local v11, "bRet":Z
    const/4 v10, 0x0

    .line 230
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 231
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowId"

    aput-object v2, v3, v1

    const-string v1, "appId"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "uiMode"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "sessinoId"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "serverId"

    aput-object v2, v3, v1

    .line 236
    .local v3, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 237
    const/4 v1, 0x1

    const-string v2, "NOTIFICATION"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 239
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 241
    const/4 v11, 0x1

    .line 255
    :goto_0
    if-eqz v10, :cond_0

    .line 256
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 258
    :cond_0
    if-eqz v0, :cond_1

    .line 259
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 262
    :cond_1
    :goto_1
    return v11

    .line 245
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 248
    :catch_0
    move-exception v12

    .line 250
    .local v12, "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    .line 251
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255
    if-eqz v10, :cond_3

    .line 256
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 258
    :cond_3
    if-eqz v0, :cond_1

    .line 259
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 255
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 256
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 258
    :cond_4
    if-eqz v0, :cond_5

    .line 259
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xnotiDbSqlInfoExistsRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 267
    const/4 v11, 0x1

    .line 268
    .local v11, "bRet":Z
    const/4 v10, 0x0

    .line 269
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 270
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowId"

    aput-object v2, v3, v1

    const-string v1, "appId"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "uiMode"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "sessinoId"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "serverId"

    aput-object v2, v3, v1

    .line 275
    .local v3, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 276
    const/4 v1, 0x1

    const-string v2, "NOTIFICATION"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 278
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 280
    const/4 v11, 0x1

    .line 294
    :goto_0
    if-eqz v10, :cond_0

    .line 295
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 297
    :cond_0
    if-eqz v0, :cond_1

    .line 298
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 301
    :cond_1
    :goto_1
    return v11

    .line 284
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 287
    :catch_0
    move-exception v12

    .line 289
    .local v12, "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    .line 290
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294
    if-eqz v10, :cond_3

    .line 295
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 297
    :cond_3
    if-eqz v0, :cond_1

    .line 298
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 294
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 295
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 297
    :cond_4
    if-eqz v0, :cond_5

    .line 298
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xnotiDbSqlInfoFetchRow(JLcom/fmm/dm/db/file/XDBNotiInfo;)Ljava/lang/Object;
    .locals 12
    .param p0, "rowId"    # J
    .param p2, "notiInfo"    # Lcom/fmm/dm/db/file/XDBNotiInfo;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 189
    const/4 v10, 0x0

    .line 190
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 191
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const-string v1, "rowId"

    aput-object v1, v3, v4

    const-string v1, "appId"

    aput-object v1, v3, v2

    const-string v1, "uiMode"

    aput-object v1, v3, v5

    const-string v1, "sessinoId"

    aput-object v1, v3, v6

    const/4 v1, 0x4

    const-string v2, "serverId"

    aput-object v2, v3, v1

    .line 195
    .local v3, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 196
    const/4 v1, 0x1

    const-string v2, "NOTIFICATION"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 198
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 200
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 202
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->rowId:I

    .line 203
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->appId:I

    .line 204
    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    .line 205
    const/4 v1, 0x3

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    .line 206
    const/4 v1, 0x4

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 210
    :catch_0
    move-exception v11

    .line 212
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    if-eqz v10, :cond_0

    .line 217
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 219
    :cond_0
    if-eqz v0, :cond_1

    .line 220
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 223
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 216
    :cond_2
    if-eqz v10, :cond_3

    .line 217
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 219
    :cond_3
    if-eqz v0, :cond_1

    .line 220
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 216
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 217
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 219
    :cond_4
    if-eqz v0, :cond_5

    .line 220
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xnotiDbSqlInfoFetchRow(Lcom/fmm/dm/db/file/XDBNotiInfo;)Ljava/lang/Object;
    .locals 12
    .param p0, "notiInfo"    # Lcom/fmm/dm/db/file/XDBNotiInfo;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 151
    const/4 v10, 0x0

    .line 152
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 153
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const-string v1, "rowId"

    aput-object v1, v3, v4

    const-string v1, "appId"

    aput-object v1, v3, v2

    const-string v1, "uiMode"

    aput-object v1, v3, v5

    const-string v1, "sessinoId"

    aput-object v1, v3, v6

    const/4 v1, 0x4

    const-string v2, "serverId"

    aput-object v2, v3, v1

    .line 158
    .local v3, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 159
    const/4 v1, 0x1

    const-string v2, "NOTIFICATION"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 161
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 163
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    .line 164
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->rowId:I

    .line 165
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->appId:I

    .line 166
    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    .line 167
    const/4 v1, 0x3

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    .line 168
    const/4 v1, 0x4

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    :cond_0
    if-eqz v10, :cond_1

    .line 178
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 180
    :cond_1
    if-eqz v0, :cond_2

    .line 181
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 184
    :cond_2
    :goto_0
    return-object p0

    .line 171
    :catch_0
    move-exception v11

    .line 173
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    if-eqz v10, :cond_3

    .line 178
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 180
    :cond_3
    if-eqz v0, :cond_2

    .line 181
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 177
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 178
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 180
    :cond_4
    if-eqz v0, :cond_5

    .line 181
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xnotiDbSqlInfoInsertRow(Lcom/fmm/dm/db/file/XDBNotiInfo;)V
    .locals 5
    .param p0, "notiInfo"    # Lcom/fmm/dm/db/file/XDBNotiInfo;

    .prologue
    .line 39
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 40
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 44
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "appId"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->appId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 45
    const-string v3, "uiMode"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 46
    const-string v3, "sessinoId"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v3, "serverId"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 50
    const-string v3, "NOTIFICATION"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    if-eqz v1, :cond_0

    .line 59
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 52
    :catch_0
    move-exception v2

    .line 54
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    if-eqz v1, :cond_0

    .line 59
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 58
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 59
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xnotiDbSqlInfoUpdateRow(JLcom/fmm/dm/db/file/XDBNotiInfo;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "notiInfo"    # Lcom/fmm/dm/db/file/XDBNotiInfo;

    .prologue
    .line 125
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 126
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 130
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "appId"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->appId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 131
    const-string v3, "uiMode"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 132
    const-string v3, "sessinoId"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v3, "serverId"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 136
    const-string v3, "NOTIFICATION"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    if-eqz v1, :cond_0

    .line 145
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 138
    :catch_0
    move-exception v2

    .line 140
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    if-eqz v1, :cond_0

    .line 145
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 144
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 145
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method
