.class public Lcom/fmm/dm/db/file/XDBAESCrypt;
.super Ljava/lang/Object;
.source "XDBAESCrypt.java"


# static fields
.field private static final CRYPTO_KEY_ALGORITHM:Ljava/lang/String; = "AES"

.field private static final CRYPTO_KEY_SIZE:I = 0x80

.field private static final CRYPTO_RANDOM_ALGORITHM:Ljava/lang/String; = "SHA1PRNG"

.field private static final CRYPTO_RANDOM_ALGORITHM2:Ljava/lang/String; = "Crypto"

.field public static final CRYPTO_SEED_PASSWORD:Ljava/lang/String; = "smldm"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbDecryptor([B)Ljava/lang/String;
    .locals 6
    .param p0, "decryptData"    # [B

    .prologue
    .line 108
    const-string v2, ""

    .line 109
    .local v2, "szDeCryptionResult":Ljava/lang/String;
    const/4 v1, 0x0

    .line 112
    .local v1, "result":[B
    const/4 v4, 0x2

    const/4 v5, 0x0

    :try_start_0
    invoke-static {p0, v4, v5}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 113
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szDeCryptionResult":Ljava/lang/String;
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    move-object v2, v3

    .line 119
    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v2    # "szDeCryptionResult":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 115
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbDecryptor([BLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "decryptData"    # [B
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 73
    const-string v2, ""

    .line 74
    .local v2, "szDeCryptionResult":Ljava/lang/String;
    const/4 v1, 0x0

    .line 77
    .local v1, "result":[B
    const/4 v4, 0x2

    :try_start_0
    invoke-static {p0, v4, p1}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 78
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szDeCryptionResult":Ljava/lang/String;
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    move-object v2, v3

    .line 84
    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v2    # "szDeCryptionResult":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 80
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbDecryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "szBase64"    # Ljava/lang/String;
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v3, ""

    .line 90
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    const/4 v0, 0x0

    .line 91
    .local v0, "decryptData":[B
    const/4 v2, 0x0

    .line 95
    .local v2, "result":[B
    const/4 v5, 0x0

    :try_start_0
    invoke-static {p0, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 96
    const/4 v5, 0x2

    invoke-static {v0, v5, p1}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B

    move-result-object v2

    .line 97
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .local v4, "szDeCryptionResult":Ljava/lang/String;
    move-object v3, v4

    .line 103
    .end local v4    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v3    # "szDeCryptionResult":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 99
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbEncryptor(Ljava/lang/String;)[B
    .locals 5
    .param p0, "szEncryptText"    # Ljava/lang/String;

    .prologue
    .line 58
    const/4 v1, 0x0

    .line 61
    .local v1, "encryptionResult":[B
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 68
    :goto_0
    return-object v1

    .line 63
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 4
    .param p0, "szEncryptText"    # Ljava/lang/String;
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 26
    const/4 v1, 0x0

    .line 29
    .local v1, "encryptionResult":[B
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 35
    :goto_0
    return-object v1

    .line 31
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "szEncryptText"    # Ljava/lang/String;
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 40
    const/4 v1, 0x0

    .line 41
    .local v1, "encryptionResult":[B
    const-string v2, ""

    .line 44
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v4, p1}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 45
    if-eqz v1, :cond_0

    .line 46
    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 53
    :cond_0
    :goto_0
    return-object v2

    .line 48
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xdbGetCryptionResult([BILjava/lang/String;)[B
    .locals 12
    .param p0, "cryptionData"    # [B
    .param p1, "nCryptionMode"    # I
    .param p2, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    const/16 v10, 0x10

    .line 124
    const/4 v3, 0x0

    .line 125
    .local v3, "mCipher":Ljavax/crypto/Cipher;
    const/4 v5, 0x0

    .line 126
    .local v5, "mLogCipher":Ljavax/crypto/Cipher;
    const/4 v8, 0x0

    .line 127
    .local v8, "nCurCryptionMode":I
    const/4 v0, 0x0

    .line 128
    .local v0, "cryptResult":[B
    const/4 v1, 0x0

    .line 129
    .local v1, "cryptionKey":[B
    const/4 v7, 0x0

    .line 130
    .local v7, "mSecureRandom":Ljava/security/SecureRandom;
    const/4 v4, 0x0

    .line 131
    .local v4, "mKeyGenerator":Ljavax/crypto/KeyGenerator;
    const/4 v6, 0x0

    .line 135
    .local v6, "mSecretKey":Ljavax/crypto/SecretKey;
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 137
    if-eqz v5, :cond_0

    if-eq p1, v8, :cond_1

    .line 139
    :cond_0
    const-string v9, "AES"

    invoke-static {v9}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v5

    .line 140
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v10, 0x172c

    const/16 v11, 0x10

    invoke-static {v10, v11}, Lcom/fmm/dm/db/file/XDBAESCrypt;->xdbMealyMachine(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    const-string v11, "AES"

    invoke-direct {v9, v10, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v5, p1, v9}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 141
    move v8, p1

    .line 143
    :cond_1
    invoke-virtual {v5, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 167
    :goto_0
    return-object v0

    .line 147
    :cond_2
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v9, v10, :cond_3

    .line 148
    const-string v9, "SHA1PRNG"

    const-string v10, "Crypto"

    invoke-static {v9, v10}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v7

    .line 151
    :goto_1
    const-string v9, "AES"

    invoke-static {v9}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v4

    .line 152
    const-string v9, "smldm"

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/security/SecureRandom;->setSeed([B)V

    .line 153
    const/16 v9, 0x80

    invoke-virtual {v4, v9, v7}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 154
    invoke-virtual {v4}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v6

    .line 155
    invoke-interface {v6}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    .line 157
    const-string v9, "AES"

    invoke-static {v9}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 158
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    const-string v10, "AES"

    invoke-direct {v9, v1, v10}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v3, p1, v9}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 159
    invoke-virtual {v3, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    goto :goto_0

    .line 150
    :cond_3
    const-string v9, "SHA1PRNG"

    invoke-static {v9}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_1

    .line 162
    :catch_0
    move-exception v2

    .line 164
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xdbMealyMachine(II)Ljava/lang/String;
    .locals 14
    .param p0, "v"    # I
    .param p1, "sz"    # I

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x2

    .line 172
    new-array v6, p1, [B

    .line 173
    .local v6, "str":[B
    const/16 v7, 0x10

    new-array v3, v7, [[I

    new-array v7, v9, [I

    fill-array-data v7, :array_0

    aput-object v7, v3, v10

    new-array v7, v9, [I

    fill-array-data v7, :array_1

    aput-object v7, v3, v11

    new-array v7, v9, [I

    fill-array-data v7, :array_2

    aput-object v7, v3, v9

    new-array v7, v9, [I

    fill-array-data v7, :array_3

    aput-object v7, v3, v12

    new-array v7, v9, [I

    fill-array-data v7, :array_4

    aput-object v7, v3, v13

    const/4 v7, 0x5

    new-array v8, v9, [I

    fill-array-data v8, :array_5

    aput-object v8, v3, v7

    const/4 v7, 0x6

    new-array v8, v9, [I

    fill-array-data v8, :array_6

    aput-object v8, v3, v7

    const/4 v7, 0x7

    new-array v8, v9, [I

    fill-array-data v8, :array_7

    aput-object v8, v3, v7

    const/16 v7, 0x8

    new-array v8, v9, [I

    fill-array-data v8, :array_8

    aput-object v8, v3, v7

    const/16 v7, 0x9

    new-array v8, v9, [I

    fill-array-data v8, :array_9

    aput-object v8, v3, v7

    const/16 v7, 0xa

    new-array v8, v9, [I

    fill-array-data v8, :array_a

    aput-object v8, v3, v7

    const/16 v7, 0xb

    new-array v8, v9, [I

    fill-array-data v8, :array_b

    aput-object v8, v3, v7

    const/16 v7, 0xc

    new-array v8, v9, [I

    fill-array-data v8, :array_c

    aput-object v8, v3, v7

    const/16 v7, 0xd

    new-array v8, v9, [I

    fill-array-data v8, :array_d

    aput-object v8, v3, v7

    const/16 v7, 0xe

    new-array v8, v9, [I

    fill-array-data v8, :array_e

    aput-object v8, v3, v7

    const/16 v7, 0xf

    new-array v8, v9, [I

    fill-array-data v8, :array_f

    aput-object v8, v3, v7

    .line 191
    .local v3, "next":[[I
    const/16 v7, 0x10

    new-array v4, v7, [[C

    new-array v7, v9, [C

    fill-array-data v7, :array_10

    aput-object v7, v4, v10

    new-array v7, v9, [C

    fill-array-data v7, :array_11

    aput-object v7, v4, v11

    new-array v7, v9, [C

    fill-array-data v7, :array_12

    aput-object v7, v4, v9

    new-array v7, v9, [C

    fill-array-data v7, :array_13

    aput-object v7, v4, v12

    new-array v7, v9, [C

    fill-array-data v7, :array_14

    aput-object v7, v4, v13

    const/4 v7, 0x5

    new-array v8, v9, [C

    fill-array-data v8, :array_15

    aput-object v8, v4, v7

    const/4 v7, 0x6

    new-array v8, v9, [C

    fill-array-data v8, :array_16

    aput-object v8, v4, v7

    const/4 v7, 0x7

    new-array v8, v9, [C

    fill-array-data v8, :array_17

    aput-object v8, v4, v7

    const/16 v7, 0x8

    new-array v8, v9, [C

    fill-array-data v8, :array_18

    aput-object v8, v4, v7

    const/16 v7, 0x9

    new-array v8, v9, [C

    fill-array-data v8, :array_19

    aput-object v8, v4, v7

    const/16 v7, 0xa

    new-array v8, v9, [C

    fill-array-data v8, :array_1a

    aput-object v8, v4, v7

    const/16 v7, 0xb

    new-array v8, v9, [C

    fill-array-data v8, :array_1b

    aput-object v8, v4, v7

    const/16 v7, 0xc

    new-array v8, v9, [C

    fill-array-data v8, :array_1c

    aput-object v8, v4, v7

    const/16 v7, 0xd

    new-array v8, v9, [C

    fill-array-data v8, :array_1d

    aput-object v8, v4, v7

    const/16 v7, 0xe

    new-array v8, v9, [C

    fill-array-data v8, :array_1e

    aput-object v8, v4, v7

    const/16 v7, 0xf

    new-array v8, v9, [C

    fill-array-data v8, :array_1f

    aput-object v8, v4, v7

    .line 210
    .local v4, "out_char":[[C
    const/4 v5, 0x0

    .local v5, "state":I
    const/4 v1, 0x0

    .local v1, "len":I
    move v2, v1

    .line 212
    .end local v1    # "len":I
    .local v2, "len":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 214
    and-int/lit8 v0, p0, 0x1

    .line 215
    .local v0, "input":I
    shr-int/lit8 p0, p0, 0x1

    .line 216
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "len":I
    .restart local v1    # "len":I
    aget-object v7, v4, v5

    aget-char v7, v7, v0

    int-to-byte v7, v7

    aput-byte v7, v6, v2

    .line 217
    aget-object v7, v3, v5

    aget v5, v7, v0

    move v2, v1

    .line 218
    .end local v1    # "len":I
    .restart local v2    # "len":I
    goto :goto_0

    .line 220
    .end local v0    # "input":I
    :cond_0
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v6}, Ljava/lang/String;-><init>([B)V

    return-object v7

    .line 173
    nop

    :array_0
    .array-data 4
        0xb
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x4
    .end array-data

    :array_2
    .array-data 4
        0x8
        0xf
    .end array-data

    :array_3
    .array-data 4
        0xb
        0x2
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3
    .end array-data

    :array_5
    .array-data 4
        0x9
        0x0
    .end array-data

    :array_6
    .array-data 4
        0xf
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_8
    .array-data 4
        0x5
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_b
    .array-data 4
        0x1
        0x6
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_d
    .array-data 4
        0x3
        0xd
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_f
    .array-data 4
        0x2
        0xd
    .end array-data

    .line 191
    :array_10
    .array-data 2
        0x73s
        0x33s
    .end array-data

    :array_11
    .array-data 2
        0x76s
        0x6es
    .end array-data

    :array_12
    .array-data 2
        0x31s
        0x39s
    .end array-data

    :array_13
    .array-data 2
        0x6ds
        0x30s
    .end array-data

    :array_14
    .array-data 2
        0x65s
        0x63s
    .end array-data

    :array_15
    .array-data 2
        0x33s
        0x42s
    .end array-data

    :array_16
    .array-data 2
        0x37s
        0x4es
    .end array-data

    :array_17
    .array-data 2
        0x6bs
        0x32s
    .end array-data

    :array_18
    .array-data 2
        0x32s
        0x43s
    .end array-data

    :array_19
    .array-data 2
        0x61s
        0x43s
    .end array-data

    :array_1a
    .array-data 2
        0x4as
        0x32s
    .end array-data

    :array_1b
    .array-data 2
        0x79s
        0x6cs
    .end array-data

    :array_1c
    .array-data 2
        0x38s
        0x64s
    .end array-data

    :array_1d
    .array-data 2
        0x31s
        0x30s
    .end array-data

    :array_1e
    .array-data 2
        0x41s
        0x5es
    .end array-data

    :array_1f
    .array-data 2
        0x37s
        0x30s
    .end array-data
.end method
