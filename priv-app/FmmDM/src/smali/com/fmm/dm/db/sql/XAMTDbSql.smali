.class public interface abstract Lcom/fmm/dm/db/sql/XAMTDbSql;
.super Ljava/lang/Object;
.source "XAMTDbSql.java"


# static fields
.field public static final XAMT_DB_SQL_GPS_ALTITUDE:Ljava/lang/String; = "GpsAltitude"

.field public static final XAMT_DB_SQL_GPS_COURSE:Ljava/lang/String; = "GpsCourse"

.field public static final XAMT_DB_SQL_GPS_DATE:Ljava/lang/String; = "GpsDate"

.field public static final XAMT_DB_SQL_GPS_HORIZONTAL_UNCERTANITY:Ljava/lang/String; = "GpsHorizontalUncertainty"

.field public static final XAMT_DB_SQL_GPS_ISVALID:Ljava/lang/String; = "GpsIsValid"

.field public static final XAMT_DB_SQL_GPS_LATITUDE:Ljava/lang/String; = "GpsLatitude"

.field public static final XAMT_DB_SQL_GPS_LONGITUDE:Ljava/lang/String; = "GpsLongitude"

.field public static final XAMT_DB_SQL_GPS_SPEED:Ljava/lang/String; = "GpsSpeed"

.field public static final XAMT_DB_SQL_GPS_VERTICAL_UNCERTANITY:Ljava/lang/String; = "GpsVerticalUncertainty"

.field public static final XAMT_DB_SQL_INFO_TABLE:Ljava/lang/String; = "MobileTracking"

.field public static final XAMT_DB_SQL_INFO_TABLE_CREATE:Ljava/lang/String; = "create table if not exists MobileTracking (rowid integer primary key autoincrement, GpsIsValid text, GpsLatitude text, GpsLongitude text, GpsAltitude text, GpsSpeed text, GpsCourse text, GpsHorizontalUncertainty text, GpsVerticalUncertainty text, GpsDate text, NonGpsValid text, NonGpsCellId text, NonGpsLAC text, NonGpsMNC text, NonGpsMCC text, Operation integer, ResultReportType integer, ResultReportSourceUri text, ResultReportTargetUri text, ResultCode text, PolicyStartDate text, PolicyEndDate text, PolicyInterval text, MobileTrackingStatus text);"

.field public static final XAMT_DB_SQL_INFO_TABLE_DROP:Ljava/lang/String; = "drop table if exists MobileTracking"

.field public static final XAMT_DB_SQL_NONGPS_CELLID:Ljava/lang/String; = "NonGpsCellId"

.field public static final XAMT_DB_SQL_NONGPS_ISVALID:Ljava/lang/String; = "NonGpsValid"

.field public static final XAMT_DB_SQL_NONGPS_LAC:Ljava/lang/String; = "NonGpsLAC"

.field public static final XAMT_DB_SQL_NONGPS_MCC:Ljava/lang/String; = "NonGpsMCC"

.field public static final XAMT_DB_SQL_NONGPS_MNC:Ljava/lang/String; = "NonGpsMNC"

.field public static final XAMT_DB_SQL_OPERATION:Ljava/lang/String; = "Operation"

.field public static final XAMT_DB_SQL_POLICY_ENDDATE:Ljava/lang/String; = "PolicyEndDate"

.field public static final XAMT_DB_SQL_POLICY_INTERVAL:Ljava/lang/String; = "PolicyInterval"

.field public static final XAMT_DB_SQL_POLICY_STARTDATE:Ljava/lang/String; = "PolicyStartDate"

.field public static final XAMT_DB_SQL_RESULT_CODE:Ljava/lang/String; = "ResultCode"

.field public static final XAMT_DB_SQL_RESULT_REPORT_SOURCE_URI:Ljava/lang/String; = "ResultReportSourceUri"

.field public static final XAMT_DB_SQL_RESULT_REPORT_TARGET_URI:Ljava/lang/String; = "ResultReportTargetUri"

.field public static final XAMT_DB_SQL_RESULT_REPORT_TYPE:Ljava/lang/String; = "ResultReportType"

.field public static final XAMT_DB_SQL_STATUS:Ljava/lang/String; = "MobileTrackingStatus"
