.class public Lcom/fmm/dm/db/file/XDBCrypt;
.super Ljava/lang/Object;
.source "XDBCrypt.java"


# static fields
.field private static C:[B

.field private static D:[B

.field private static E:[B

.field private static FP:[B

.field private static IP:[B

.field private static KS:[[B

.field private static MAX_CRYPT_BITS_SIZE:I

.field private static MAX_ENCRYPT_SIZE:I

.field private static P:[B

.field private static PC1_C:[B

.field private static PC1_D:[B

.field private static PC2_C:[B

.field private static PC2_D:[B

.field private static S:[[B

.field private static e2:[B

.field private static m_cryptCryptByte:[B

.field private static m_szCryptCryptResult:Ljava/lang/String;

.field private static preS:[B

.field private static shifts:[B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v2, 0x1c

    const/16 v1, 0x18

    const/16 v4, 0x10

    const/16 v3, 0x40

    .line 7
    new-array v0, v3, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->IP:[B

    .line 10
    new-array v0, v3, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->FP:[B

    .line 13
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->PC1_C:[B

    .line 16
    new-array v0, v2, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->PC1_D:[B

    .line 19
    new-array v0, v4, [B

    fill-array-data v0, :array_4

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->shifts:[B

    .line 22
    new-array v0, v1, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->PC2_C:[B

    .line 25
    new-array v0, v1, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->PC2_D:[B

    .line 28
    const/16 v0, 0x30

    new-array v0, v0, [B

    fill-array-data v0, :array_7

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->e2:[B

    .line 31
    const/16 v0, 0x8

    new-array v0, v0, [[B

    const/4 v1, 0x0

    new-array v2, v3, [B

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [B

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [B

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v3, [B

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [B

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [B

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [B

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [B

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->S:[[B

    .line 42
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_10

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->P:[B

    .line 45
    sput v3, Lcom/fmm/dm/db/file/XDBCrypt;->MAX_CRYPT_BITS_SIZE:I

    .line 46
    sput v4, Lcom/fmm/dm/db/file/XDBCrypt;->MAX_ENCRYPT_SIZE:I

    return-void

    .line 7
    :array_0
    .array-data 1
        0x3at
        0x32t
        0x2at
        0x22t
        0x1at
        0x12t
        0xat
        0x2t
        0x3ct
        0x34t
        0x2ct
        0x24t
        0x1ct
        0x14t
        0xct
        0x4t
        0x3et
        0x36t
        0x2et
        0x26t
        0x1et
        0x16t
        0xet
        0x6t
        0x40t
        0x38t
        0x30t
        0x28t
        0x20t
        0x18t
        0x10t
        0x8t
        0x39t
        0x31t
        0x29t
        0x21t
        0x19t
        0x11t
        0x9t
        0x1t
        0x3bt
        0x33t
        0x2bt
        0x23t
        0x1bt
        0x13t
        0xbt
        0x3t
        0x3dt
        0x35t
        0x2dt
        0x25t
        0x1dt
        0x15t
        0xdt
        0x5t
        0x3ft
        0x37t
        0x2ft
        0x27t
        0x1ft
        0x17t
        0xft
        0x7t
    .end array-data

    .line 10
    :array_1
    .array-data 1
        0x28t
        0x8t
        0x30t
        0x10t
        0x38t
        0x18t
        0x40t
        0x20t
        0x27t
        0x7t
        0x2ft
        0xft
        0x37t
        0x17t
        0x3ft
        0x1ft
        0x26t
        0x6t
        0x2et
        0xet
        0x36t
        0x16t
        0x3et
        0x1et
        0x25t
        0x5t
        0x2dt
        0xdt
        0x35t
        0x15t
        0x3dt
        0x1dt
        0x24t
        0x4t
        0x2ct
        0xct
        0x34t
        0x14t
        0x3ct
        0x1ct
        0x23t
        0x3t
        0x2bt
        0xbt
        0x33t
        0x13t
        0x3bt
        0x1bt
        0x22t
        0x2t
        0x2at
        0xat
        0x32t
        0x12t
        0x3at
        0x1at
        0x21t
        0x1t
        0x29t
        0x9t
        0x31t
        0x11t
        0x39t
        0x19t
    .end array-data

    .line 13
    :array_2
    .array-data 1
        0x39t
        0x31t
        0x29t
        0x21t
        0x19t
        0x11t
        0x9t
        0x1t
        0x3at
        0x32t
        0x2at
        0x22t
        0x1at
        0x12t
        0xat
        0x2t
        0x3bt
        0x33t
        0x2bt
        0x23t
        0x1bt
        0x13t
        0xbt
        0x3t
        0x3ct
        0x34t
        0x2ct
        0x24t
    .end array-data

    .line 16
    :array_3
    .array-data 1
        0x3ft
        0x37t
        0x2ft
        0x27t
        0x1ft
        0x17t
        0xft
        0x7t
        0x3et
        0x36t
        0x2et
        0x26t
        0x1et
        0x16t
        0xet
        0x6t
        0x3dt
        0x35t
        0x2dt
        0x25t
        0x1dt
        0x15t
        0xdt
        0x5t
        0x1ct
        0x14t
        0xct
        0x4t
    .end array-data

    .line 19
    :array_4
    .array-data 1
        0x1t
        0x1t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x1t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x1t
    .end array-data

    .line 22
    :array_5
    .array-data 1
        0xet
        0x11t
        0xbt
        0x18t
        0x1t
        0x5t
        0x3t
        0x1ct
        0xft
        0x6t
        0x15t
        0xat
        0x17t
        0x13t
        0xct
        0x4t
        0x1at
        0x8t
        0x10t
        0x7t
        0x1bt
        0x14t
        0xdt
        0x2t
    .end array-data

    .line 25
    :array_6
    .array-data 1
        0x29t
        0x34t
        0x1ft
        0x25t
        0x2ft
        0x37t
        0x1et
        0x28t
        0x33t
        0x2dt
        0x21t
        0x30t
        0x2ct
        0x31t
        0x27t
        0x38t
        0x22t
        0x35t
        0x2et
        0x2at
        0x32t
        0x24t
        0x1dt
        0x20t
    .end array-data

    .line 28
    :array_7
    .array-data 1
        0x20t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        0x8t
        0x9t
        0xat
        0xbt
        0xct
        0xdt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
        0x18t
        0x19t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1ct
        0x1dt
        0x1et
        0x1ft
        0x20t
        0x1t
    .end array-data

    .line 31
    :array_8
    .array-data 1
        0xet
        0x4t
        0xdt
        0x1t
        0x2t
        0xft
        0xbt
        0x8t
        0x3t
        0xat
        0x6t
        0xct
        0x5t
        0x9t
        0x0t
        0x7t
        0x0t
        0xft
        0x7t
        0x4t
        0xet
        0x2t
        0xdt
        0x1t
        0xat
        0x6t
        0xct
        0xbt
        0x9t
        0x5t
        0x3t
        0x8t
        0x4t
        0x1t
        0xet
        0x8t
        0xdt
        0x6t
        0x2t
        0xbt
        0xft
        0xct
        0x9t
        0x7t
        0x3t
        0xat
        0x5t
        0x0t
        0xft
        0xct
        0x8t
        0x2t
        0x4t
        0x9t
        0x1t
        0x7t
        0x5t
        0xbt
        0x3t
        0xet
        0xat
        0x0t
        0x6t
        0xdt
    .end array-data

    :array_9
    .array-data 1
        0xft
        0x1t
        0x8t
        0xet
        0x6t
        0xbt
        0x3t
        0x4t
        0x9t
        0x7t
        0x2t
        0xdt
        0xct
        0x0t
        0x5t
        0xat
        0x3t
        0xdt
        0x4t
        0x7t
        0xft
        0x2t
        0x8t
        0xet
        0xct
        0x0t
        0x1t
        0xat
        0x6t
        0x9t
        0xbt
        0x5t
        0x0t
        0xet
        0x7t
        0xbt
        0xat
        0x4t
        0xdt
        0x1t
        0x5t
        0x8t
        0xct
        0x6t
        0x9t
        0x3t
        0x2t
        0xft
        0xdt
        0x8t
        0xat
        0x1t
        0x3t
        0xft
        0x4t
        0x2t
        0xbt
        0x6t
        0x7t
        0xct
        0x0t
        0x5t
        0xet
        0x9t
    .end array-data

    :array_a
    .array-data 1
        0xat
        0x0t
        0x9t
        0xet
        0x6t
        0x3t
        0xft
        0x5t
        0x1t
        0xdt
        0xct
        0x7t
        0xbt
        0x4t
        0x2t
        0x8t
        0xdt
        0x7t
        0x0t
        0x9t
        0x3t
        0x4t
        0x6t
        0xat
        0x2t
        0x8t
        0x5t
        0xet
        0xct
        0xbt
        0xft
        0x1t
        0xdt
        0x6t
        0x4t
        0x9t
        0x8t
        0xft
        0x3t
        0x0t
        0xbt
        0x1t
        0x2t
        0xct
        0x5t
        0xat
        0xet
        0x7t
        0x1t
        0xat
        0xdt
        0x0t
        0x6t
        0x9t
        0x8t
        0x7t
        0x4t
        0xft
        0xet
        0x3t
        0xbt
        0x5t
        0x2t
        0xct
    .end array-data

    :array_b
    .array-data 1
        0x7t
        0xdt
        0xet
        0x3t
        0x0t
        0x6t
        0x9t
        0xat
        0x1t
        0x2t
        0x8t
        0x5t
        0xbt
        0xct
        0x4t
        0xft
        0xdt
        0x8t
        0xbt
        0x5t
        0x6t
        0xft
        0x0t
        0x3t
        0x4t
        0x7t
        0x2t
        0xct
        0x1t
        0xat
        0xet
        0x9t
        0xat
        0x6t
        0x9t
        0x0t
        0xct
        0xbt
        0x7t
        0xdt
        0xft
        0x1t
        0x3t
        0xet
        0x5t
        0x2t
        0x8t
        0x4t
        0x3t
        0xft
        0x0t
        0x6t
        0xat
        0x1t
        0xdt
        0x8t
        0x9t
        0x4t
        0x5t
        0xbt
        0xct
        0x7t
        0x2t
        0xet
    .end array-data

    :array_c
    .array-data 1
        0x2t
        0xct
        0x4t
        0x1t
        0x7t
        0xat
        0xbt
        0x6t
        0x8t
        0x5t
        0x3t
        0xft
        0xdt
        0x0t
        0xet
        0x9t
        0xet
        0xbt
        0x2t
        0xct
        0x4t
        0x7t
        0xdt
        0x1t
        0x5t
        0x0t
        0xft
        0xat
        0x3t
        0x9t
        0x8t
        0x6t
        0x4t
        0x2t
        0x1t
        0xbt
        0xat
        0xdt
        0x7t
        0x8t
        0xft
        0x9t
        0xct
        0x5t
        0x6t
        0x3t
        0x0t
        0xet
        0xbt
        0x8t
        0xct
        0x7t
        0x1t
        0xet
        0x2t
        0xdt
        0x6t
        0xft
        0x0t
        0x9t
        0xat
        0x4t
        0x5t
        0x3t
    .end array-data

    :array_d
    .array-data 1
        0xct
        0x1t
        0xat
        0xft
        0x9t
        0x2t
        0x6t
        0x8t
        0x0t
        0xdt
        0x3t
        0x4t
        0xet
        0x7t
        0x5t
        0xbt
        0xat
        0xft
        0x4t
        0x2t
        0x7t
        0xct
        0x9t
        0x5t
        0x6t
        0x1t
        0xdt
        0xet
        0x0t
        0xbt
        0x3t
        0x8t
        0x9t
        0xet
        0xft
        0x5t
        0x2t
        0x8t
        0xct
        0x3t
        0x7t
        0x0t
        0x4t
        0xat
        0x1t
        0xdt
        0xbt
        0x6t
        0x4t
        0x3t
        0x2t
        0xct
        0x9t
        0x5t
        0xft
        0xat
        0xbt
        0xet
        0x1t
        0x7t
        0x6t
        0x0t
        0x8t
        0xdt
    .end array-data

    :array_e
    .array-data 1
        0x4t
        0xbt
        0x2t
        0xet
        0xft
        0x0t
        0x8t
        0xdt
        0x3t
        0xct
        0x9t
        0x7t
        0x5t
        0xat
        0x6t
        0x1t
        0xdt
        0x0t
        0xbt
        0x7t
        0x4t
        0x9t
        0x1t
        0xat
        0xet
        0x3t
        0x5t
        0xct
        0x2t
        0xft
        0x8t
        0x6t
        0x1t
        0x4t
        0xbt
        0xdt
        0xct
        0x3t
        0x7t
        0xet
        0xat
        0xft
        0x6t
        0x8t
        0x0t
        0x5t
        0x9t
        0x2t
        0x6t
        0xbt
        0xdt
        0x8t
        0x1t
        0x4t
        0xat
        0x7t
        0x9t
        0x5t
        0x0t
        0xft
        0xet
        0x2t
        0x3t
        0xct
    .end array-data

    :array_f
    .array-data 1
        0xdt
        0x2t
        0x8t
        0x4t
        0x6t
        0xft
        0xbt
        0x1t
        0xat
        0x9t
        0x3t
        0xet
        0x5t
        0x0t
        0xct
        0x7t
        0x1t
        0xft
        0xdt
        0x8t
        0xat
        0x3t
        0x7t
        0x4t
        0xct
        0x5t
        0x6t
        0xbt
        0x0t
        0xet
        0x9t
        0x2t
        0x7t
        0xbt
        0x4t
        0x1t
        0x9t
        0xct
        0xet
        0x2t
        0x0t
        0x6t
        0xat
        0xdt
        0xft
        0x3t
        0x5t
        0x8t
        0x2t
        0x1t
        0xet
        0x7t
        0x4t
        0xat
        0x8t
        0xdt
        0xft
        0xct
        0x9t
        0x0t
        0x3t
        0x5t
        0x6t
        0xbt
    .end array-data

    .line 42
    :array_10
    .array-data 1
        0x10t
        0x7t
        0x14t
        0x15t
        0x1dt
        0xct
        0x1ct
        0x11t
        0x1t
        0xft
        0x17t
        0x1at
        0x5t
        0x12t
        0x1ft
        0xat
        0x2t
        0x8t
        0x18t
        0xet
        0x20t
        0x1bt
        0x3t
        0x9t
        0x13t
        0xdt
        0x1et
        0x6t
        0x16t
        0xbt
        0x4t
        0x19t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v1, 0x1c

    const/16 v2, 0x30

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-array v0, v1, [B

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->C:[B

    .line 60
    new-array v0, v1, [B

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->D:[B

    .line 61
    const/16 v0, 0x10

    filled-new-array {v0, v2}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[B

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->KS:[[B

    .line 62
    new-array v0, v2, [B

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->E:[B

    .line 63
    new-array v0, v2, [B

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->preS:[B

    .line 65
    sget v0, Lcom/fmm/dm/db/file/XDBCrypt;->MAX_ENCRYPT_SIZE:I

    new-array v0, v0, [B

    sput-object v0, Lcom/fmm/dm/db/file/XDBCrypt;->m_cryptCryptByte:[B

    .line 66
    return-void
.end method

.method private xdbCryptDESEncrypt([B)[B
    .locals 20
    .param p1, "block"    # [B

    .prologue
    .line 197
    const/16 v17, 0x20

    move/from16 v0, v17

    new-array v6, v0, [B

    .line 198
    .local v6, "left":[B
    const/16 v17, 0x20

    move/from16 v0, v17

    new-array v8, v0, [B

    .line 199
    .local v8, "right":[B
    const/16 v17, 0x20

    move/from16 v0, v17

    new-array v7, v0, [B

    .line 200
    .local v7, "old":[B
    const/16 v17, 0x20

    move/from16 v0, v17

    new-array v1, v0, [B

    .line 203
    .local v1, "f":[B
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    const/16 v17, 0x20

    move/from16 v0, v17

    if-ge v4, v0, :cond_0

    .line 205
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->IP:[B

    aget-byte v17, v17, v4

    add-int/lit8 v17, v17, -0x1

    aget-byte v17, p1, v17

    aput-byte v17, v6, v4

    .line 203
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 208
    :cond_0
    :goto_1
    const/16 v17, 0x40

    move/from16 v0, v17

    if-ge v4, v0, :cond_1

    .line 210
    add-int/lit8 v17, v4, -0x20

    sget-object v18, Lcom/fmm/dm/db/file/XDBCrypt;->IP:[B

    aget-byte v18, v18, v4

    add-int/lit8 v18, v18, -0x1

    aget-byte v18, p1, v18

    aput-byte v18, v8, v17

    .line 208
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 213
    :cond_1
    const/4 v3, 0x0

    .local v3, "ii":I
    :goto_2
    const/16 v17, 0x10

    move/from16 v0, v17

    if-ge v3, v0, :cond_7

    .line 215
    move v2, v3

    .line 217
    .local v2, "i":I
    const/4 v4, 0x0

    :goto_3
    const/16 v17, 0x20

    move/from16 v0, v17

    if-ge v4, v0, :cond_2

    .line 219
    aget-byte v17, v8, v4

    aput-byte v17, v7, v4

    .line 217
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 222
    :cond_2
    const/4 v4, 0x0

    :goto_4
    const/16 v17, 0x30

    move/from16 v0, v17

    if-ge v4, v0, :cond_3

    .line 224
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->preS:[B

    sget-object v18, Lcom/fmm/dm/db/file/XDBCrypt;->E:[B

    aget-byte v18, v18, v4

    add-int/lit8 v18, v18, -0x1

    aget-byte v18, v8, v18

    sget-object v19, Lcom/fmm/dm/db/file/XDBCrypt;->KS:[[B

    aget-object v19, v19, v2

    aget-byte v19, v19, v4

    xor-int v18, v18, v19

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v17, v4

    .line 222
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 227
    :cond_3
    const/4 v4, 0x0

    :goto_5
    const/16 v17, 0x8

    move/from16 v0, v17

    if-ge v4, v0, :cond_4

    .line 229
    mul-int/lit8 v17, v4, 0x6

    move/from16 v0, v17

    int-to-byte v0, v0

    move/from16 v16, v0

    .line 231
    .local v16, "temp":B
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->preS:[B

    add-int/lit8 v18, v16, 0x0

    aget-byte v17, v17, v18

    shl-int/lit8 v9, v17, 0x5

    .line 232
    .local v9, "s1":I
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->preS:[B

    add-int/lit8 v18, v16, 0x1

    aget-byte v17, v17, v18

    shl-int/lit8 v10, v17, 0x3

    .line 233
    .local v10, "s2":I
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->preS:[B

    add-int/lit8 v18, v16, 0x2

    aget-byte v17, v17, v18

    shl-int/lit8 v11, v17, 0x2

    .line 234
    .local v11, "s3":I
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->preS:[B

    add-int/lit8 v18, v16, 0x3

    aget-byte v17, v17, v18

    shl-int/lit8 v12, v17, 0x1

    .line 235
    .local v12, "s4":I
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->preS:[B

    add-int/lit8 v18, v16, 0x4

    aget-byte v17, v17, v18

    shl-int/lit8 v13, v17, 0x0

    .line 236
    .local v13, "s5":I
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->preS:[B

    add-int/lit8 v18, v16, 0x5

    aget-byte v17, v17, v18

    shl-int/lit8 v14, v17, 0x4

    .line 238
    .local v14, "s6":I
    add-int v17, v9, v10

    add-int v17, v17, v11

    add-int v17, v17, v12

    add-int v17, v17, v13

    add-int v15, v17, v14

    .line 239
    .local v15, "sSum":I
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->S:[[B

    aget-object v17, v17, v4

    aget-byte v5, v17, v15

    .line 241
    .local v5, "k":I
    mul-int/lit8 v17, v4, 0x4

    move/from16 v0, v17

    int-to-byte v0, v0

    move/from16 v16, v0

    .line 242
    add-int/lit8 v17, v16, 0x0

    shr-int/lit8 v18, v5, 0x3

    and-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v1, v17

    .line 243
    add-int/lit8 v17, v16, 0x1

    shr-int/lit8 v18, v5, 0x2

    and-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v1, v17

    .line 244
    add-int/lit8 v17, v16, 0x2

    shr-int/lit8 v18, v5, 0x1

    and-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v1, v17

    .line 245
    add-int/lit8 v17, v16, 0x3

    shr-int/lit8 v18, v5, 0x0

    and-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v1, v17

    .line 227
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5

    .line 248
    .end local v5    # "k":I
    .end local v9    # "s1":I
    .end local v10    # "s2":I
    .end local v11    # "s3":I
    .end local v12    # "s4":I
    .end local v13    # "s5":I
    .end local v14    # "s6":I
    .end local v15    # "sSum":I
    .end local v16    # "temp":B
    :cond_4
    const/4 v4, 0x0

    :goto_6
    const/16 v17, 0x20

    move/from16 v0, v17

    if-ge v4, v0, :cond_5

    .line 250
    aget-byte v17, v6, v4

    sget-object v18, Lcom/fmm/dm/db/file/XDBCrypt;->P:[B

    aget-byte v18, v18, v4

    add-int/lit8 v18, v18, -0x1

    aget-byte v18, v1, v18

    xor-int v17, v17, v18

    move/from16 v0, v17

    int-to-byte v0, v0

    move/from16 v17, v0

    aput-byte v17, v8, v4

    .line 248
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 253
    :cond_5
    const/4 v4, 0x0

    :goto_7
    const/16 v17, 0x20

    move/from16 v0, v17

    if-ge v4, v0, :cond_6

    .line 255
    aget-byte v17, v7, v4

    aput-byte v17, v6, v4

    .line 253
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 213
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 259
    .end local v2    # "i":I
    :cond_7
    const/4 v4, 0x0

    :goto_8
    const/16 v17, 0x20

    move/from16 v0, v17

    if-ge v4, v0, :cond_8

    .line 261
    aget-byte v16, v6, v4

    .line 262
    .restart local v16    # "temp":B
    aget-byte v17, v8, v4

    aput-byte v17, v6, v4

    .line 263
    aput-byte v16, v8, v4

    .line 259
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 266
    .end local v16    # "temp":B
    :cond_8
    const/4 v4, 0x0

    .line 267
    const/4 v4, 0x0

    :goto_9
    const/16 v17, 0x40

    move/from16 v0, v17

    if-ge v4, v0, :cond_a

    .line 269
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->FP:[B

    aget-byte v2, v17, v4

    .line 270
    .restart local v2    # "i":I
    const/16 v17, 0x21

    move/from16 v0, v17

    if-ge v2, v0, :cond_9

    .line 272
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->FP:[B

    aget-byte v17, v17, v4

    add-int/lit8 v17, v17, -0x1

    aget-byte v17, v6, v17

    aput-byte v17, p1, v4

    .line 267
    :goto_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 276
    :cond_9
    sget-object v17, Lcom/fmm/dm/db/file/XDBCrypt;->FP:[B

    aget-byte v17, v17, v4

    add-int/lit8 v17, v17, -0x21

    aget-byte v17, v8, v17

    aput-byte v17, p1, v4

    goto :goto_a

    .line 279
    .end local v2    # "i":I
    :cond_a
    return-object p1
.end method

.method private xdbCryptEExpandsion([B)V
    .locals 10
    .param p1, "salt"    # [B

    .prologue
    .line 157
    if-nez p1, :cond_1

    .line 192
    :cond_0
    return-void

    .line 160
    :cond_1
    const/4 v3, 0x0

    .line 162
    .local v3, "k":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v4, v3

    .end local v3    # "k":I
    .local v4, "k":I
    :goto_0
    const/4 v6, 0x2

    if-ge v1, v6, :cond_0

    .line 166
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "k":I
    .restart local v3    # "k":I
    aget-byte v0, p1, v4

    .line 167
    .local v0, "c":B
    sget-object v6, Lcom/fmm/dm/db/file/XDBCrypt;->m_cryptCryptByte:[B

    aput-byte v0, v6, v1

    .line 169
    const/16 v6, 0x5a

    if-le v0, v6, :cond_3

    .line 171
    add-int/lit8 v6, v0, -0x3b

    int-to-byte v0, v6

    .line 182
    :goto_1
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    const/4 v6, 0x6

    if-ge v2, v6, :cond_5

    .line 184
    shr-int v6, v0, v2

    and-int/lit8 v6, v6, 0x1

    int-to-byte v6, v6

    if-eqz v6, :cond_2

    .line 186
    sget-object v6, Lcom/fmm/dm/db/file/XDBCrypt;->E:[B

    mul-int/lit8 v7, v1, 0x6

    add-int/2addr v7, v2

    aget-byte v5, v6, v7

    .line 187
    .local v5, "temp":B
    sget-object v6, Lcom/fmm/dm/db/file/XDBCrypt;->E:[B

    mul-int/lit8 v7, v1, 0x6

    add-int/2addr v7, v2

    sget-object v8, Lcom/fmm/dm/db/file/XDBCrypt;->E:[B

    mul-int/lit8 v9, v1, 0x6

    add-int/2addr v9, v2

    add-int/lit8 v9, v9, 0x18

    aget-byte v8, v8, v9

    aput-byte v8, v6, v7

    .line 188
    sget-object v6, Lcom/fmm/dm/db/file/XDBCrypt;->E:[B

    mul-int/lit8 v7, v1, 0x6

    add-int/2addr v7, v2

    add-int/lit8 v7, v7, 0x18

    aput-byte v5, v6, v7

    .line 182
    .end local v5    # "temp":B
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 173
    .end local v2    # "j":I
    :cond_3
    const/16 v6, 0x39

    if-le v0, v6, :cond_4

    .line 175
    add-int/lit8 v6, v0, -0x35

    int-to-byte v0, v6

    goto :goto_1

    .line 179
    :cond_4
    add-int/lit8 v6, v0, -0x2e

    int-to-byte v0, v6

    goto :goto_1

    .line 162
    .restart local v2    # "j":I
    :cond_5
    add-int/lit8 v1, v1, 0x1

    move v4, v3

    .end local v3    # "k":I
    .restart local v4    # "k":I
    goto :goto_0
.end method

.method private xdbCryptEncrypt([B)V
    .locals 7
    .param p1, "password"    # [B

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 287
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0x19

    if-ge v1, v3, :cond_0

    .line 289
    invoke-direct {p0, p1}, Lcom/fmm/dm/db/file/XDBCrypt;->xdbCryptDESEncrypt([B)[B

    move-result-object p1

    .line 287
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 292
    :cond_0
    const/4 v1, 0x0

    :goto_1
    const/16 v3, 0xb

    if-ge v1, v3, :cond_4

    .line 294
    const/4 v0, 0x0

    .line 295
    .local v0, "c":B
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    const/4 v3, 0x6

    if-ge v2, v3, :cond_1

    .line 297
    shl-int/lit8 v3, v0, 0x1

    int-to-byte v0, v3

    .line 298
    mul-int/lit8 v3, v1, 0x6

    add-int/2addr v3, v2

    aget-byte v3, p1, v3

    or-int/2addr v3, v0

    int-to-byte v0, v3

    .line 295
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 300
    :cond_1
    add-int/lit8 v3, v0, 0x2e

    int-to-byte v0, v3

    .line 301
    const/16 v3, 0x39

    if-le v0, v3, :cond_2

    .line 303
    add-int/lit8 v3, v0, 0x7

    int-to-byte v0, v3

    .line 305
    :cond_2
    const/16 v3, 0x5a

    if-le v0, v3, :cond_3

    .line 307
    add-int/lit8 v3, v0, 0x6

    int-to-byte v0, v3

    .line 309
    :cond_3
    sget-object v3, Lcom/fmm/dm/db/file/XDBCrypt;->m_cryptCryptByte:[B

    add-int/lit8 v4, v1, 0x2

    aput-byte v0, v3, v4

    .line 292
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 311
    .end local v0    # "c":B
    .end local v2    # "j":I
    :cond_4
    sget-object v3, Lcom/fmm/dm/db/file/XDBCrypt;->m_cryptCryptByte:[B

    add-int/lit8 v4, v1, 0x2

    aput-byte v5, v3, v4

    .line 312
    sget-object v3, Lcom/fmm/dm/db/file/XDBCrypt;->m_cryptCryptByte:[B

    aget-byte v3, v3, v6

    if-nez v3, :cond_5

    .line 314
    sget-object v3, Lcom/fmm/dm/db/file/XDBCrypt;->m_cryptCryptByte:[B

    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->m_cryptCryptByte:[B

    aget-byte v4, v4, v5

    aput-byte v4, v3, v6

    .line 316
    :cond_5
    return-void
.end method

.method private xdbCryptInitPassword([B[B)[B
    .locals 6
    .param p1, "key"    # [B
    .param p2, "password"    # [B

    .prologue
    const/4 v5, 0x0

    .line 70
    const/4 v0, 0x0

    .line 71
    .local v0, "i":I
    const/4 v2, 0x0

    .line 73
    .local v2, "j":I
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 74
    :cond_0
    const/4 p2, 0x0

    .line 94
    .end local p2    # "password":[B
    :cond_1
    return-object p2

    .line 76
    .restart local p2    # "password":[B
    :cond_2
    const/4 v3, 0x0

    .line 78
    .local v3, "k":I
    :goto_0
    aget-byte v4, p1, v3

    if-eqz v4, :cond_4

    sget v4, Lcom/fmm/dm/db/file/XDBCrypt;->MAX_CRYPT_BITS_SIZE:I

    if-ge v0, v4, :cond_4

    .line 80
    const/4 v2, 0x6

    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_1
    if-ltz v2, :cond_3

    .line 82
    aget-byte v4, p1, v3

    shr-int/2addr v4, v2

    and-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    aput-byte v4, p2, v1

    .line 83
    add-int/lit8 v0, v1, 0x1

    .line 80
    .end local v1    # "i":I
    .restart local v0    # "i":I
    add-int/lit8 v2, v2, -0x1

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 85
    :cond_3
    add-int/lit8 v3, v3, 0x1

    .line 86
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aput-byte v5, p2, v1

    goto :goto_0

    .line 89
    :cond_4
    :goto_2
    sget v4, Lcom/fmm/dm/db/file/XDBCrypt;->MAX_CRYPT_BITS_SIZE:I

    add-int/lit8 v4, v4, 0x2

    if-ge v0, v4, :cond_1

    .line 91
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    aput-byte v5, p2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_2
.end method

.method private xdbCryptSetKey([B)V
    .locals 10
    .param p1, "key"    # [B

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x1b

    .line 113
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v4, 0x1c

    if-ge v0, v4, :cond_0

    .line 115
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->C:[B

    sget-object v5, Lcom/fmm/dm/db/file/XDBCrypt;->PC1_C:[B

    aget-byte v5, v5, v0

    add-int/lit8 v5, v5, -0x1

    aget-byte v5, p1, v5

    aput-byte v5, v4, v0

    .line 116
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->D:[B

    sget-object v5, Lcom/fmm/dm/db/file/XDBCrypt;->PC1_D:[B

    aget-byte v5, v5, v0

    add-int/lit8 v5, v5, -0x1

    aget-byte v5, p1, v5

    aput-byte v5, v4, v0

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_0
    const/4 v0, 0x0

    :goto_1
    const/16 v4, 0x10

    if-ge v0, v4, :cond_5

    .line 121
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_2
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->shifts:[B

    aget-byte v4, v4, v0

    if-ge v2, v4, :cond_3

    .line 123
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->C:[B

    aget-byte v3, v4, v9

    .line 124
    .local v3, "temp":B
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_3
    if-ge v1, v8, :cond_1

    .line 126
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->C:[B

    sget-object v5, Lcom/fmm/dm/db/file/XDBCrypt;->C:[B

    add-int/lit8 v6, v1, 0x1

    aget-byte v5, v5, v6

    aput-byte v5, v4, v1

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 128
    :cond_1
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->C:[B

    aput-byte v3, v4, v8

    .line 130
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->D:[B

    aget-byte v3, v4, v9

    .line 131
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v8, :cond_2

    .line 133
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->D:[B

    sget-object v5, Lcom/fmm/dm/db/file/XDBCrypt;->D:[B

    add-int/lit8 v6, v1, 0x1

    aget-byte v5, v5, v6

    aput-byte v5, v4, v1

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 135
    :cond_2
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->D:[B

    aput-byte v3, v4, v8

    .line 121
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 138
    .end local v1    # "j":I
    .end local v3    # "temp":B
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_5
    const/16 v4, 0x18

    if-ge v1, v4, :cond_4

    .line 140
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->KS:[[B

    aget-object v4, v4, v0

    sget-object v5, Lcom/fmm/dm/db/file/XDBCrypt;->C:[B

    sget-object v6, Lcom/fmm/dm/db/file/XDBCrypt;->PC2_C:[B

    aget-byte v6, v6, v1

    add-int/lit8 v6, v6, -0x1

    aget-byte v5, v5, v6

    aput-byte v5, v4, v1

    .line 142
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->KS:[[B

    aget-object v4, v4, v0

    add-int/lit8 v5, v1, 0x18

    sget-object v6, Lcom/fmm/dm/db/file/XDBCrypt;->D:[B

    sget-object v7, Lcom/fmm/dm/db/file/XDBCrypt;->PC2_D:[B

    aget-byte v7, v7, v1

    add-int/lit8 v7, v7, -0x1c

    add-int/lit8 v7, v7, -0x1

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 119
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 146
    .end local v1    # "j":I
    .end local v2    # "k":I
    :cond_5
    const/4 v0, 0x0

    :goto_6
    const/16 v4, 0x30

    if-ge v0, v4, :cond_6

    .line 148
    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->E:[B

    sget-object v5, Lcom/fmm/dm/db/file/XDBCrypt;->e2:[B

    aget-byte v5, v5, v0

    aput-byte v5, v4, v0

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 150
    :cond_6
    return-void
.end method

.method private xdbCrypttZeroPassword([B)[B
    .locals 2
    .param p1, "password"    # [B

    .prologue
    .line 101
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v1, Lcom/fmm/dm/db/file/XDBCrypt;->MAX_CRYPT_BITS_SIZE:I

    add-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_0

    .line 103
    const/4 v1, 0x0

    aput-byte v1, p1, v0

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_0
    return-object p1
.end method


# virtual methods
.method public xdbCryptGenerate(Ljava/lang/String;[B)Ljava/lang/String;
    .locals 5
    .param p1, "szKey"    # Ljava/lang/String;
    .param p2, "salt"    # [B

    .prologue
    .line 330
    sget v3, Lcom/fmm/dm/db/file/XDBCrypt;->MAX_CRYPT_BITS_SIZE:I

    add-int/lit8 v3, v3, 0x2

    new-array v2, v3, [B

    .line 331
    .local v2, "password":[B
    const/4 v1, 0x0

    .line 334
    .local v1, "key":[B
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 335
    invoke-direct {p0, v1, v2}, Lcom/fmm/dm/db/file/XDBCrypt;->xdbCryptInitPassword([B[B)[B

    move-result-object v2

    .line 337
    if-eqz v2, :cond_0

    .line 339
    invoke-direct {p0, v2}, Lcom/fmm/dm/db/file/XDBCrypt;->xdbCryptSetKey([B)V

    .line 340
    invoke-direct {p0, v2}, Lcom/fmm/dm/db/file/XDBCrypt;->xdbCrypttZeroPassword([B)[B

    move-result-object v2

    .line 341
    invoke-direct {p0, p2}, Lcom/fmm/dm/db/file/XDBCrypt;->xdbCryptEExpandsion([B)V

    .line 342
    invoke-direct {p0, v2}, Lcom/fmm/dm/db/file/XDBCrypt;->xdbCryptEncrypt([B)V

    .line 345
    :cond_0
    new-instance v3, Ljava/lang/String;

    sget-object v4, Lcom/fmm/dm/db/file/XDBCrypt;->m_cryptCryptByte:[B

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    sput-object v3, Lcom/fmm/dm/db/file/XDBCrypt;->m_szCryptCryptResult:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :goto_0
    sget-object v3, Lcom/fmm/dm/db/file/XDBCrypt;->m_szCryptCryptResult:Ljava/lang/String;

    return-object v3

    .line 347
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "xdbCryptGenerate Fail"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method
