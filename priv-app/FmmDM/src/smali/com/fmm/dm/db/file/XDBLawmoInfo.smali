.class public Lcom/fmm/dm/db/file/XDBLawmoInfo;
.super Ljava/lang/Object;
.source "XDBLawmoInfo.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

.field public InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

.field public bNotifyUser:Z

.field public bRegistration:Z

.field public bSimChangeState:Z

.field public m_szCallRestrictionPhoneNumber:Ljava/lang/String;

.field public m_szCallRestrictionStatus:Ljava/lang/String;

.field public m_szCorrelator:Ljava/lang/String;

.field public m_szLockMyPhoneMessages:Ljava/lang/String;

.field public m_szPassword:Ljava/lang/String;

.field public m_szPin:Ljava/lang/String;

.field public m_szReactivationLock:Ljava/lang/String;

.field public m_szResultCode:Ljava/lang/String;

.field public m_szResultReportSourceUri:Ljava/lang/String;

.field public m_szResultReportTargetUri:Ljava/lang/String;

.field public m_szRingMyPhoneMessages:Ljava/lang/String;

.field public m_szRingMyPhoneStatus:Ljava/lang/String;

.field public nAmtStaus:I

.field public nExtOperation:I

.field public nOperation:I

.field public nResultReportType:I

.field public nState:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    sget-object v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nState:I

    .line 44
    new-instance v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    .line 45
    iget-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const-string v1, "InternalMem"

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    .line 48
    new-instance v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    .line 49
    iget-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const-string v1, "ExternalMem"

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    .line 50
    iget-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iput-boolean v2, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    .line 52
    iput-boolean v2, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bNotifyUser:Z

    .line 54
    iput v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nOperation:I

    .line 55
    iput v4, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nExtOperation:I

    .line 56
    const-string v0, "00000000"

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPin:Ljava/lang/String;

    .line 58
    iput v3, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nResultReportType:I

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultCode:Ljava/lang/String;

    .line 63
    sget-object v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->XLAWMO_STATUS_AMT_OFF:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nAmtStaus:I

    .line 64
    iput-boolean v2, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    .line 65
    iput-boolean v2, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bSimChangeState:Z

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPassword:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szLockMyPhoneMessages:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionPhoneNumber:Ljava/lang/String;

    .line 69
    const-string v0, "10"

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionStatus:Ljava/lang/String;

    .line 70
    const-string v0, "10"

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneStatus:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneMessages:Ljava/lang/String;

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCorrelator:Ljava/lang/String;

    .line 73
    return-void
.end method
