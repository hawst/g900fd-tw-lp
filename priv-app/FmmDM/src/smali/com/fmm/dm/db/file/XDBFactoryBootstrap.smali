.class public Lcom/fmm/dm/db/file/XDBFactoryBootstrap;
.super Ljava/lang/Object;
.source "XDBFactoryBootstrap.java"

# interfaces
.implements Lcom/fmm/dm/db/sql/XDMDbSql;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field public static final DEFAULT_NONCE:Ljava/lang/String; = "MTIzNDU="

.field public static final FACTORYBOOTSTRAP_CLIENTID:I = 0x1

.field public static final FACTORYBOOTSTRAP_CLIENTPWD:I = 0x2

.field public static final FACTORYBOOTSTRAP_SERVERPWD:I = 0x0

.field public static final OSPS_SERVER_PWD:Ljava/lang/String; = "T1NQIERNIFNlcnZlcg=="

.field public static OSP_ProfileName:[Ljava/lang/String; = null

.field public static OSP_ServerID:[Ljava/lang/String; = null

.field public static OSP_ServerUrl:[Ljava/lang/String; = null

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "5e8o9279r2"

    aput-object v1, v0, v2

    const-string v1, "samsung"

    aput-object v1, v0, v3

    const-string v1, "T-Mobile"

    aput-object v1, v0, v4

    sput-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;->OSP_ServerID:[Ljava/lang/String;

    .line 28
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "PCW-Server"

    aput-object v1, v0, v2

    const-string v1, "LAWMO-Server"

    aput-object v1, v0, v3

    const-string v1, "Mformation"

    aput-object v1, v0, v4

    sput-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;->OSP_ProfileName:[Ljava/lang/String;

    .line 30
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "https://dm.samsungdive.com/v1/sdm/magicsync/dm"

    aput-object v1, v0, v2

    const-string v1, "https://dms.ospserver.net:443/v1/device/magicsync/dm"

    aput-object v1, v0, v3

    const-string v1, "http://mfiop12.mformation.com:80/oma/iop"

    aput-object v1, v0, v4

    sput-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;->OSP_ServerUrl:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbFBGenerateFactoryNonce()Ljava/lang/String;
    .locals 10

    .prologue
    .line 127
    const-string v6, ""

    .line 129
    .local v6, "szNonce":Ljava/lang/String;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 130
    .local v1, "date":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 131
    .local v4, "seed":J
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3, v4, v5}, Ljava/util/Random;-><init>(J)V

    .line 132
    .local v3, "rnd":Ljava/util/Random;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "SSNextNonce"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 133
    .local v7, "szTemp":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 135
    .local v0, "buf":[B
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 136
    .local v2, "encoder":[B
    new-instance v6, Ljava/lang/String;

    .end local v6    # "szNonce":Ljava/lang/String;
    invoke-direct {v6, v2}, Ljava/lang/String;-><init>([B)V

    .line 138
    .restart local v6    # "szNonce":Ljava/lang/String;
    return-object v6
.end method

.method public static xdbFBGetFactoryBootstrapData(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 11
    .param p0, "pNVMSyncMLDMInfo"    # Ljava/lang/Object;
    .param p1, "nIdex"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 37
    const/4 v1, 0x0

    .line 38
    .local v1, "pProfileInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    const/4 v4, 0x0

    .line 41
    .local v4, "szNonce":Ljava/lang/String;
    const/4 v5, 0x0

    .line 42
    .local v5, "xdbURLParser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    const/4 v6, 0x0

    .line 43
    .local v6, "xdbURLParser2":Lcom/fmm/dm/db/file/XDBUrlInfo;
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v3

    .local v3, "szDevid":Ljava/lang/String;
    move-object v1, p0

    .line 45
    check-cast v1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 46
    sget-object v7, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;->OSP_ProfileName:[Ljava/lang/String;

    aget-object v7, v7, p1

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 47
    sget-object v7, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;->OSP_ServerID:[Ljava/lang/String;

    aget-object v7, v7, p1

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 48
    sget-object v7, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;->OSP_ServerUrl:[Ljava/lang/String;

    aget-object v7, v7, p1

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 49
    iput v8, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    .line 53
    const-string v7, "w7"

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    .line 54
    const-string v7, "CLCRED"

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    .line 55
    const-string v7, "SRVCRED"

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    .line 58
    iget-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v7}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v5

    .line 59
    iget-object v7, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 60
    iget-object v7, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 61
    iget-object v7, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    .line 62
    iget v7, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    iput v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    .line 63
    iget-object v7, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 64
    iget-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 66
    iget-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    invoke-static {v7}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v6

    .line 67
    iget-object v7, v6, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 68
    iget-object v7, v6, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 69
    iget-object v7, v6, Lcom/fmm/dm/db/file/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path_Org:Ljava/lang/String;

    .line 70
    iget v7, v6, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    iput v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 71
    iget-object v7, v6, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 72
    iput-boolean v8, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    .line 74
    const/4 v7, 0x3

    if-ge p1, v7, :cond_2

    .line 76
    iput v9, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    .line 77
    iput v9, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    .line 79
    if-ne p1, v10, :cond_1

    .line 81
    iget-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v3, v7, v8}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePasswd(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 82
    iget-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v3, v7, v9}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePasswd(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 83
    iget-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v3, v7, v10}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePasswd(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 91
    :goto_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;->xdbFBGenerateFactoryNonce()Ljava/lang/String;

    move-result-object v4

    .line 95
    new-instance v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;-><init>()V

    .line 96
    .local v0, "dm_AccXNodeInfo":Lcom/fmm/dm/db/file/XDBAccXNodeInfo;
    const-string v7, "./DMAcc/OSP"

    iput-object v7, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAccount:Ljava/lang/String;

    .line 97
    const-string v7, "./DMAcc/OSP/AppAddr/AppAddrX"

    iput-object v7, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAppAddr:Ljava/lang/String;

    .line 98
    const-string v7, "./DMAcc/OSP/AppAddr/AppAddrX/Port/PortX"

    iput-object v7, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szAppAddrPort:Ljava/lang/String;

    .line 99
    const-string v7, "./DMAcc/OSP/AppAuth/ClientSide"

    iput-object v7, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szClientAppAuth:Ljava/lang/String;

    .line 100
    const-string v7, "./DMAcc/OSP/AppAuth/ServerSide"

    iput-object v7, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szServerAppAuth:Ljava/lang/String;

    .line 101
    const-string v7, "./DMAcc/OSP/ToConRef/Connectivity Reference Name"

    iput-object v7, v0, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;->m_szToConRef:Ljava/lang/String;

    .line 102
    const/16 v7, 0x83

    invoke-static {v7, v0}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlCreate(ILjava/lang/Object;)I

    move-result v2

    .line 103
    .local v2, "ret":I
    if-eqz v2, :cond_0

    .line 104
    const-string v7, "SqlCreate fail"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 107
    :cond_0
    iput-object v4, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 108
    iput-object v4, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 109
    iput v9, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonceFormat:I

    .line 110
    iput v9, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonceFormat:I

    .line 116
    .end local v0    # "dm_AccXNodeInfo":Lcom/fmm/dm/db/file/XDBAccXNodeInfo;
    .end local v2    # "ret":I
    :goto_1
    return-object v1

    .line 87
    :cond_1
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 88
    iget-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    iget-object v8, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/fmm/dm/db/file/XDBFactoryBootstrapadapter;->xdbFBAdpOspGenerateDevicePwd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 89
    const-string v7, "T1NQIERNIFNlcnZlcg=="

    iput-object v7, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    goto :goto_0

    .line 114
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Wrong Index : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbFBGetFactoryBootstrapServerID(I)Ljava/lang/String;
    .locals 1
    .param p0, "nIdex"    # I

    .prologue
    .line 121
    sget-object v0, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;->OSP_ServerID:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method
