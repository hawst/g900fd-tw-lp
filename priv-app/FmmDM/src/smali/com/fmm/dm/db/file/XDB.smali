.class public Lcom/fmm/dm/db/file/XDB;
.super Lcom/fmm/dm/db/file/XDBFactoryBootstrap;
.source "XDB.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDBInterface;
.implements Lcom/fmm/dm/interfaces/XUICInterface;
.implements Lcom/fmm/dm/interfaces/XUIInterface;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/db/file/XDB$XDMFileParameter;,
        Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;
    }
.end annotation


# static fields
.field public static ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef; = null

.field public static final DMINFOMAGIC:I = 0x933

.field public static final DMPROFILEMAGIC:I = 0xec7

.field private static final FFS_OWNER_SYNCML:I = 0xf0

.field public static final NVMAMTInfo:I = 0x59

.field public static final NVMDMAccXNode:I = 0x83

.field public static final NVMDMAgentInfo:I = 0x57

.field public static final NVMDMInfo1:I = 0x52

.field public static final NVMDMInfo2:I = 0x53

.field public static final NVMDMInfo3:I = 0x54

.field public static final NVMDMNetworkInfo1:I = 0x80

.field public static final NVMDMProfile:I = 0x51

.field public static final NVMIMSIInfo:I = 0x55

.field public static final NVMLawmoInfo:I = 0x58

.field public static final NVMNotiInfo:I = 0x60

.field public static final NVMResyncMode:I = 0x56

.field public static NetProfileClass:Lcom/fmm/dm/db/file/XDBNetworkProfileList; = null

.field public static ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo; = null

.field private static XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam; = null

.field private static XDMNVMParamCount:I = 0x0

.field public static XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm; = null

.field public static final XDM_FFS_FILE_EXTENTION:Ljava/lang/String; = ".cfg"

.field public static final XDM_NET_PROFILE_LIST:I = 0x1

.field public static final XDM_PROFILE_LIST:I = 0x0

.field public static final XDM_PROFILE_LIST_VIEW:I = 0x2

.field public static dbadapter:Lcom/fmm/dm/db/file/XDBAdapter; = null

.field private static final serialVersionUID:J = 0x1L

.field private static xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    .line 123
    sget-object v0, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_MAX:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v0

    sput v0, Lcom/fmm/dm/db/file/XDB;->XDMNVMParamCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/fmm/dm/db/file/XDBFactoryBootstrap;-><init>()V

    .line 65
    return-void
.end method

.method public static xdbAdpAppFileCreate(I[B)I
    .locals 2
    .param p0, "FileID"    # I
    .param p1, "pBuffer"    # [B

    .prologue
    .line 2002
    const/4 v0, 0x0

    .line 2003
    .local v0, "szFileName":Ljava/lang/String;
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v0

    .line 2005
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    invoke-virtual {v1, v0, p1}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileCreateWrite(Ljava/lang/String;[B)Z

    .line 2006
    const/4 v1, 0x0

    return v1
.end method

.method public static xdbAdpAppendFileWrite(I[B)I
    .locals 9
    .param p0, "FileID"    # I
    .param p1, "buffer"    # [B

    .prologue
    .line 1797
    const-string v6, ""

    .line 1798
    .local v6, "szFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1799
    .local v0, "Data":Ljava/io/DataOutputStream;
    const/4 v2, 0x0

    .line 1800
    .local v2, "File":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 1802
    .local v5, "nRtnStatus":I
    if-lez p0, :cond_3

    .line 1803
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v6

    .line 1809
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    const/4 v7, 0x1

    invoke-direct {v3, v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1810
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .local v3, "File":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1812
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .local v1, "Data":Ljava/io/DataOutputStream;
    :try_start_2
    monitor-enter v1
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 1814
    :try_start_3
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->write([B)V

    .line 1815
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1816
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    .line 1817
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/FileDescriptor;->sync()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 1833
    if-eqz v1, :cond_0

    .line 1835
    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 1846
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 1848
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    :cond_2
    :goto_1
    move v7, v5

    .line 1858
    :goto_2
    return v7

    .line 1805
    :cond_3
    const/4 v7, 0x2

    goto :goto_2

    .line 1815
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v7

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v7
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_b
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 1819
    :catch_0
    move-exception v4

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 1821
    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    .local v4, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1822
    const/4 v5, 0x3

    .line 1833
    if-eqz v0, :cond_4

    .line 1835
    :try_start_a
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 1846
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :cond_4
    :goto_4
    if-eqz v2, :cond_2

    .line 1848
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    goto :goto_1

    .line 1851
    :catch_1
    move-exception v4

    .line 1853
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1854
    const/4 v5, 0x5

    .line 1856
    goto :goto_1

    .line 1838
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v4

    .line 1840
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1841
    const/4 v5, 0x5

    goto :goto_0

    .line 1851
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 1853
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1854
    const/4 v5, 0x5

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 1856
    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    goto :goto_1

    .line 1838
    .local v4, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v4

    .line 1840
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1841
    const/4 v5, 0x5

    goto :goto_4

    .line 1824
    .end local v4    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 1826
    .restart local v4    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_c
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 1827
    const/4 v5, 0x4

    .line 1833
    if-eqz v0, :cond_5

    .line 1835
    :try_start_d
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    .line 1846
    :cond_5
    :goto_6
    if-eqz v2, :cond_2

    .line 1848
    :try_start_e
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6

    goto :goto_1

    .line 1851
    :catch_6
    move-exception v4

    .line 1853
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1854
    const/4 v5, 0x5

    .line 1856
    goto :goto_1

    .line 1838
    :catch_7
    move-exception v4

    .line 1840
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1841
    const/4 v5, 0x5

    goto :goto_6

    .line 1831
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v7

    .line 1833
    :goto_7
    if-eqz v0, :cond_6

    .line 1835
    :try_start_f
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    .line 1846
    :cond_6
    :goto_8
    if-eqz v2, :cond_7

    .line 1848
    :try_start_10
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9

    .line 1855
    :cond_7
    :goto_9
    throw v7

    .line 1838
    :catch_8
    move-exception v4

    .line 1840
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1841
    const/4 v5, 0x5

    goto :goto_8

    .line 1851
    .end local v4    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v4

    .line 1853
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1854
    const/4 v5, 0x5

    goto :goto_9

    .line 1831
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v7

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    goto :goto_7

    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catchall_3
    move-exception v7

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    goto :goto_7

    .line 1824
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v4

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    goto :goto_5

    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v4

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    goto :goto_5

    .line 1819
    :catch_c
    move-exception v4

    goto/16 :goto_3

    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catch_d
    move-exception v4

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    goto/16 :goto_3
.end method

.method public static xdbAdpFileDelete(Ljava/lang/String;I)I
    .locals 4
    .param p0, "szName"    # Ljava/lang/String;
    .param p1, "FileID"    # I

    .prologue
    .line 2036
    const/4 v1, 0x0

    .line 2037
    .local v1, "eRet":I
    const/4 v2, 0x0

    .line 2039
    .local v2, "szFileName":Ljava/lang/String;
    if-lez p1, :cond_0

    .line 2041
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v2

    .line 2057
    :goto_0
    :try_start_0
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    invoke-virtual {v3, v2}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileRemove(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v3, v1

    .line 2063
    :goto_2
    return v3

    .line 2045
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2047
    const-string v3, "pszFileName is NULL"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2048
    const/4 v3, 0x2

    goto :goto_2

    .line 2052
    :cond_1
    move-object v2, p0

    goto :goto_0

    .line 2059
    :catch_0
    move-exception v0

    .line 2061
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbAdpFileExists(Ljava/lang/String;I)I
    .locals 3
    .param p0, "szName"    # Ljava/lang/String;
    .param p1, "FileID"    # I

    .prologue
    .line 2011
    const/4 v1, 0x0

    .line 2012
    .local v1, "szFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2014
    .local v0, "eRet":I
    if-lez p1, :cond_0

    .line 2016
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v1

    .line 2030
    :goto_0
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    invoke-virtual {v2, v1}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileExists(Ljava/lang/String;)I

    move-result v0

    move v2, v0

    .line 2031
    :goto_1
    return v2

    .line 2020
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2022
    const/4 v2, 0x2

    goto :goto_1

    .line 2026
    :cond_1
    move-object v1, p0

    goto :goto_0
.end method

.method public static xdbAgentInfoDbRead(I)Ljava/lang/Object;
    .locals 1
    .param p0, "nType"    # I

    .prologue
    .line 274
    packed-switch p0, :pswitch_data_0

    .line 283
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 286
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 277
    :pswitch_0
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    goto :goto_0

    .line 280
    :pswitch_1
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBAgentInfo;->m_nAgentType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 274
    :pswitch_data_0
    .packed-switch 0x6e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xdbAgentInfoDbWrite(ILjava/lang/Object;)V
    .locals 2
    .param p0, "nType"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    .line 291
    packed-switch p0, :pswitch_data_0

    .line 302
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 305
    .end local p1    # "Input":Ljava/lang/Object;
    :goto_0
    return-void

    .line 294
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_0
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBAgentInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    goto :goto_0

    .line 298
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_1
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBAgentInfo;->m_nAgentType:I

    goto :goto_0

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x6e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xdbAmtDbRead(I)Ljava/lang/Object;
    .locals 1
    .param p0, "nType"    # I

    .prologue
    .line 505
    packed-switch p0, :pswitch_data_0

    .line 580
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 583
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 508
    :pswitch_0
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    goto :goto_0

    .line 511
    :pswitch_1
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsIsValid:Ljava/lang/String;

    goto :goto_0

    .line 514
    :pswitch_2
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLatitude:Ljava/lang/String;

    goto :goto_0

    .line 517
    :pswitch_3
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLogitude:Ljava/lang/String;

    goto :goto_0

    .line 520
    :pswitch_4
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsAltitude:Ljava/lang/String;

    goto :goto_0

    .line 523
    :pswitch_5
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsSpeed:Ljava/lang/String;

    goto :goto_0

    .line 526
    :pswitch_6
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsCourse:Ljava/lang/String;

    goto :goto_0

    .line 529
    :pswitch_7
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsHorizontalUncertainty:Ljava/lang/String;

    goto :goto_0

    .line 532
    :pswitch_8
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsVerticalUncertainty:Ljava/lang/String;

    goto :goto_0

    .line 535
    :pswitch_9
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsDate:Ljava/lang/String;

    goto :goto_0

    .line 538
    :pswitch_a
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsIsValid:Ljava/lang/String;

    goto :goto_0

    .line 541
    :pswitch_b
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsCellId:Ljava/lang/String;

    goto :goto_0

    .line 544
    :pswitch_c
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsLAC:Ljava/lang/String;

    goto :goto_0

    .line 547
    :pswitch_d
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMNC:Ljava/lang/String;

    goto :goto_0

    .line 550
    :pswitch_e
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMCC:Ljava/lang/String;

    goto :goto_0

    .line 553
    :pswitch_f
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nOperation:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 556
    :pswitch_10
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nResultReportType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 559
    :pswitch_11
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 562
    :pswitch_12
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 565
    :pswitch_13
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 568
    :pswitch_14
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyStartDate:Ljava/lang/String;

    goto/16 :goto_0

    .line 571
    :pswitch_15
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyEndDate:Ljava/lang/String;

    goto/16 :goto_0

    .line 574
    :pswitch_16
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyInterval:Ljava/lang/String;

    goto/16 :goto_0

    .line 577
    :pswitch_17
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szMobileTrackingStatus:Ljava/lang/String;

    goto/16 :goto_0

    .line 505
    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public static xdbAmtDbWrite(ILjava/lang/Object;)V
    .locals 2
    .param p0, "nType"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    .line 588
    packed-switch p0, :pswitch_data_0

    .line 687
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 690
    .end local p1    # "Input":Ljava/lang/Object;
    :goto_0
    return-void

    .line 591
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_0
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBAMTInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Lcom/fmm/dm/db/file/XDBAMTInfo;

    iput-object p1, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    goto :goto_0

    .line 595
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_1
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsIsValid:Ljava/lang/String;

    goto :goto_0

    .line 599
    :pswitch_2
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLatitude:Ljava/lang/String;

    goto :goto_0

    .line 603
    :pswitch_3
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLogitude:Ljava/lang/String;

    goto :goto_0

    .line 607
    :pswitch_4
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsAltitude:Ljava/lang/String;

    goto :goto_0

    .line 611
    :pswitch_5
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsSpeed:Ljava/lang/String;

    goto :goto_0

    .line 615
    :pswitch_6
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsCourse:Ljava/lang/String;

    goto :goto_0

    .line 619
    :pswitch_7
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsHorizontalUncertainty:Ljava/lang/String;

    goto :goto_0

    .line 623
    :pswitch_8
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsVerticalUncertainty:Ljava/lang/String;

    goto :goto_0

    .line 627
    :pswitch_9
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsDate:Ljava/lang/String;

    goto :goto_0

    .line 631
    :pswitch_a
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsIsValid:Ljava/lang/String;

    goto :goto_0

    .line 635
    :pswitch_b
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsCellId:Ljava/lang/String;

    goto/16 :goto_0

    .line 639
    :pswitch_c
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsLAC:Ljava/lang/String;

    goto/16 :goto_0

    .line 643
    :pswitch_d
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMNC:Ljava/lang/String;

    goto/16 :goto_0

    .line 647
    :pswitch_e
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMCC:Ljava/lang/String;

    goto/16 :goto_0

    .line 651
    :pswitch_f
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nOperation:I

    goto/16 :goto_0

    .line 655
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_10
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nResultReportType:I

    goto/16 :goto_0

    .line 659
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_11
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 663
    :pswitch_12
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 667
    :pswitch_13
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 671
    :pswitch_14
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyStartDate:Ljava/lang/String;

    goto/16 :goto_0

    .line 675
    :pswitch_15
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyEndDate:Ljava/lang/String;

    goto/16 :goto_0

    .line 679
    :pswitch_16
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyInterval:Ljava/lang/String;

    goto/16 :goto_0

    .line 683
    :pswitch_17
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szMobileTrackingStatus:Ljava/lang/String;

    goto/16 :goto_0

    .line 588
    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public static xdbAppendFile(I[B)I
    .locals 3
    .param p0, "FileID"    # I
    .param p1, "pBuffer"    # [B

    .prologue
    .line 1726
    const/4 v1, 0x0

    .line 1730
    .local v1, "eRet":I
    :try_start_0
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbFileOpen(I)I

    move-result v1

    .line 1731
    if-eqz v1, :cond_1

    .line 1733
    invoke-static {p0, p1}, Lcom/fmm/dm/db/file/XDB;->xdbAdpAppFileCreate(I[B)I

    move-result v1

    .line 1734
    if-eqz v1, :cond_0

    .line 1736
    const-string v2, "Create FAILED"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1752
    :cond_0
    :goto_0
    return v1

    .line 1741
    :cond_1
    invoke-static {p0, p1}, Lcom/fmm/dm/db/file/XDB;->xdbAdpAppendFileWrite(I[B)I

    move-result v1

    .line 1742
    if-eqz v1, :cond_0

    .line 1744
    const-string v2, "Append FAILED"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1748
    :catch_0
    move-exception v0

    .line 1750
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbCacheSpecificFileSize(Ljava/io/File;)J
    .locals 13
    .param p0, "TargetFolder"    # Ljava/io/File;

    .prologue
    .line 2115
    const-string v9, ""

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2117
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 2118
    .local v0, "ChildFile":[Ljava/io/File;
    const/4 v3, 0x0

    .line 2119
    .local v3, "nSize":I
    const-wide/16 v4, 0x0

    .line 2121
    .local v4, "nTotalFileSize":J
    if-nez v0, :cond_0

    .line 2123
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "can not get child list of "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-wide v6, v4

    .line 2160
    .end local v4    # "nTotalFileSize":J
    .local v6, "nTotalFileSize":J
    :goto_0
    return-wide v6

    .line 2127
    .end local v6    # "nTotalFileSize":J
    .restart local v4    # "nTotalFileSize":J
    :cond_0
    array-length v3, v0

    .line 2128
    const-string v9, "nfilenum of directory [%s] : %d "

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2132
    if-lez v3, :cond_4

    .line 2134
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_4

    .line 2136
    :try_start_0
    aget-object v9, v0, v2

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 2137
    .local v8, "szName":Ljava/lang/String;
    const-string v9, "lost+found"

    invoke-virtual {v9, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "recovery"

    invoke-virtual {v9, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2139
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File name : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2140
    aget-object v9, v0, v2

    invoke-virtual {v9}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2142
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is file."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2143
    sget-object v9, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    aget-object v10, v0, v2

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileGetSize(Ljava/lang/String;)J

    move-result-wide v10

    add-long/2addr v4, v10

    .line 2134
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2147
    :cond_3
    aget-object v9, v0, v2

    invoke-static {v9}, Lcom/fmm/dm/db/file/XDB;->xdbGetInternalFileSize(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    add-long/2addr v4, v10

    goto :goto_2

    .line 2153
    .end local v8    # "szName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 2155
    .local v1, "ex":Ljava/lang/Exception;
    const-string v9, "fail to delete"

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-wide v6, v4

    .line 2156
    .end local v4    # "nTotalFileSize":J
    .restart local v6    # "nTotalFileSize":J
    goto/16 :goto_0

    .line 2159
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "i":I
    .end local v6    # "nTotalFileSize":J
    .restart local v4    # "nTotalFileSize":J
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "nTotalFileSize = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-wide v6, v4

    .line 2160
    .end local v4    # "nTotalFileSize":J
    .restart local v6    # "nTotalFileSize":J
    goto/16 :goto_0
.end method

.method public static xdbChangeLawmoServerInfo(Ljava/lang/String;)V
    .locals 5
    .param p0, "szServerURL"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 3311
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3313
    const-string v3, "serverURL is null. return"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3350
    :goto_0
    return-void

    .line 3317
    :cond_0
    const/16 v3, 0x52

    invoke-static {v3}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 3320
    .local v0, "pProfileInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-virtual {p0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    .line 3325
    invoke-static {p0}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v1

    .line 3327
    .local v1, "xdbURLParser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    iget-object v3, v1, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 3328
    iget-object v3, v1, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 3329
    iget-object v3, v1, Lcom/fmm/dm/db/file/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    .line 3330
    iget v3, v1, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    .line 3331
    iget-object v3, v1, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 3332
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 3334
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v2

    .line 3335
    .local v2, "xdbURLParser2":Lcom/fmm/dm/db/file/XDBUrlInfo;
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 3336
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 3337
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path_Org:Ljava/lang/String;

    .line 3338
    iget v3, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    iput v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 3339
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 3340
    iput-boolean v4, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    .line 3342
    invoke-static {v0, v4}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileInfo(Lcom/fmm/dm/db/file/XDBProfileInfo;I)Z

    .line 3344
    const-string v3, "Changed Lawmo ServerInfo"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 3348
    .end local v1    # "xdbURLParser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    .end local v2    # "xdbURLParser2":Lcom/fmm/dm/db/file/XDBUrlInfo;
    :cond_1
    const-string v3, "Not Changed Lawmo ServerInfo"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbCheckActiveProfileIndexByServerID(Ljava/lang/String;)Z
    .locals 5
    .param p0, "szInputServerId"    # Ljava/lang/String;

    .prologue
    .line 3046
    const/4 v3, 0x0

    .line 3047
    .local v3, "szServerId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 3049
    .local v0, "bActiveProfile":Z
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3051
    const-string v4, "ServerID is NULL"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3052
    const/4 v4, 0x0

    .line 3073
    :goto_0
    return v4

    .line 3055
    :cond_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerID()Ljava/lang/String;

    move-result-object v3

    .line 3056
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3057
    const/4 v4, 0x1

    goto :goto_0

    .line 3059
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v4, 0x3

    if-ge v2, v4, :cond_2

    .line 3061
    add-int/lit8 v4, v2, 0x52

    invoke-static {v4}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 3063
    .local v1, "dmInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    if-eqz v1, :cond_3

    .line 3065
    iget-object v4, v1, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3067
    const/4 v0, 0x1

    .end local v1    # "dmInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    :cond_2
    move v4, v0

    .line 3073
    goto :goto_0

    .line 3059
    .restart local v1    # "dmInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static xdbCheckProfileListExist()Z
    .locals 4

    .prologue
    .line 2983
    const/4 v2, 0x0

    .line 2984
    .local v2, "pDMProfile":Lcom/fmm/dm/db/file/XDBProflieListInfo;
    const/4 v0, 0x0

    .line 2985
    .local v0, "bReturn":Z
    const/4 v1, 0x0

    .line 2987
    .local v1, "i":I
    new-instance v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .end local v2    # "pDMProfile":Lcom/fmm/dm/db/file/XDBProflieListInfo;
    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBProflieListInfo;-><init>()V

    .line 2989
    .restart local v2    # "pDMProfile":Lcom/fmm/dm/db/file/XDBProflieListInfo;
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbGetProflieList(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "pDMProfile":Lcom/fmm/dm/db/file/XDBProflieListInfo;
    check-cast v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 2991
    .restart local v2    # "pDMProfile":Lcom/fmm/dm/db/file/XDBProflieListInfo;
    if-eqz v2, :cond_0

    .line 2993
    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v1, v3, :cond_0

    .line 2995
    iget-object v3, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2997
    const/4 v0, 0x1

    .line 3002
    :cond_0
    return v0

    .line 2993
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xdbClearUicResultKeepFlag()V
    .locals 4

    .prologue
    .line 3268
    const/4 v1, 0x0

    .line 3271
    .local v1, "eUIcKeepFlag":I
    const/16 v2, 0xe

    :try_start_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3277
    :goto_0
    return-void

    .line 3273
    :catch_0
    move-exception v0

    .line 3275
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbDMffs_Init()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x1

    .line 1934
    const/4 v1, 0x0

    .line 1936
    .local v1, "nCount":I
    const-string v2, "xdbDMffs_Init"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1941
    const/16 v0, 0x83

    .line 1942
    .local v0, "AreaCodeTemp":I
    const-wide/16 v2, 0x3

    invoke-static {v2, v3}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbExistsAccXListNodeRow(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1943
    invoke-static {v0, v6}, Lcom/fmm/dm/db/file/XDB;->xdbInitFfsFile(II)Z

    .line 1946
    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    .line 1948
    add-int/lit8 v0, v1, 0x52

    .line 1949
    add-int/lit8 v2, v1, 0x1

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbExistsProfileRow(J)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1950
    add-int/lit16 v2, v1, 0x933

    invoke-static {v0, v2}, Lcom/fmm/dm/db/file/XDB;->xdbInitFfsFile(II)Z

    .line 1946
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1953
    :cond_2
    const/16 v0, 0x51

    .line 1954
    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbExistsProfileListRow(J)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1955
    const/16 v2, 0xec7

    invoke-static {v0, v2}, Lcom/fmm/dm/db/file/XDB;->xdbInitFfsFile(II)Z

    .line 1959
    :goto_1
    const/16 v0, 0x55

    .line 1960
    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbExistsSimInfoRow(J)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1961
    invoke-static {v0, v6}, Lcom/fmm/dm/db/file/XDB;->xdbInitFfsFile(II)Z

    .line 1965
    :cond_3
    const/16 v0, 0x56

    .line 1966
    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbExistsResyncModeRow(J)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1967
    invoke-static {v0, v6}, Lcom/fmm/dm/db/file/XDB;->xdbInitFfsFile(II)Z

    .line 1970
    :cond_4
    const/16 v0, 0x57

    .line 1971
    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlAgentInfoExistsRow(J)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1972
    invoke-static {v0, v6}, Lcom/fmm/dm/db/file/XDB;->xdbInitFfsFile(II)Z

    .line 1976
    :cond_5
    const/16 v0, 0x58

    .line 1977
    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExistsRow(J)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlInternalMemExistsRow(J)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XLAWMODbSqlQuery;->xlawmoDbSqlExternalMemExistsRow(J)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1979
    :cond_6
    invoke-static {v0, v6}, Lcom/fmm/dm/db/file/XDB;->xdbInitFfsFile(II)Z

    .line 1985
    :cond_7
    const/16 v0, 0x59

    .line 1986
    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;->xamtDbSqlExistsRow(J)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1988
    invoke-static {v0, v6}, Lcom/fmm/dm/db/file/XDB;->xdbInitFfsFile(II)Z

    .line 1991
    :cond_8
    return-void

    .line 1957
    :cond_9
    invoke-static {v6}, Lcom/fmm/dm/db/file/XDB;->xdbReadListInfo(I)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static xdbDelete(II)Z
    .locals 2
    .param p0, "FileID"    # I
    .param p1, "rowId"    # I

    .prologue
    .line 806
    const/4 v0, 0x0

    .line 808
    .local v0, "wRC":I
    sparse-switch p0, :sswitch_data_0

    .line 823
    const-string v1, "Not Support file id----"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 826
    :goto_0
    if-nez v0, :cond_0

    .line 828
    const/4 v1, 0x1

    .line 832
    :goto_1
    return v1

    .line 811
    :sswitch_0
    const/16 v1, 0x58

    invoke-static {v1, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlDelete(II)I

    move-result v0

    .line 812
    goto :goto_0

    .line 815
    :sswitch_1
    const/16 v1, 0x59

    invoke-static {v1, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlDelete(II)I

    move-result v0

    .line 816
    goto :goto_0

    .line 819
    :sswitch_2
    const/16 v1, 0x60

    invoke-static {v1, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlDelete(II)I

    move-result v0

    .line 820
    goto :goto_0

    .line 832
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 808
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xfa -> :sswitch_2
        0x12c -> :sswitch_1
    .end sparse-switch
.end method

.method public static xdbDelete(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "FileID"    # I
    .param p1, "szColName"    # Ljava/lang/String;
    .param p2, "szColData"    # Ljava/lang/String;

    .prologue
    .line 838
    const/4 v0, 0x0

    .line 840
    .local v0, "wRC":I
    sparse-switch p0, :sswitch_data_0

    .line 855
    const-string v1, "Not Support file id----"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 858
    :goto_0
    if-nez v0, :cond_0

    .line 860
    const/4 v1, 0x1

    .line 864
    :goto_1
    return v1

    .line 843
    :sswitch_0
    const/16 v1, 0x58

    invoke-static {v1, p1, p2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlDelete(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 844
    goto :goto_0

    .line 847
    :sswitch_1
    const/16 v1, 0x59

    invoke-static {v1, p1, p2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlDelete(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 848
    goto :goto_0

    .line 851
    :sswitch_2
    const/16 v1, 0x60

    invoke-static {v1, p1, p2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlDelete(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 852
    goto :goto_0

    .line 864
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 840
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xfa -> :sswitch_2
        0x12c -> :sswitch_1
    .end sparse-switch
.end method

.method public static xdbDeleteFile(I)I
    .locals 2
    .param p0, "FileID"    # I

    .prologue
    .line 1884
    const/4 v0, 0x0

    .line 1885
    .local v0, "rc":I
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbAdpFileDelete(Ljava/lang/String;I)I

    move-result v0

    .line 1886
    return v0
.end method

.method public static xdbDeleteFile(Ljava/lang/String;)Z
    .locals 1
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    .line 1892
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileDelete(Ljava/lang/String;)Z

    move-result v0

    .line 1894
    .local v0, "bRet":Z
    return v0
.end method

.method public static xdbFileGetNameFromCallerID(I)Ljava/lang/String;
    .locals 7
    .param p0, "FileID"    # I

    .prologue
    .line 1995
    const v3, 0x249f00

    add-int/2addr v3, p0

    int-to-long v0, v3

    .line 1996
    .local v0, "handle":J
    const-string v3, "%s/%d%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "data/data/com.fmm.dm/"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, ".cfg"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1997
    .local v2, "szFileName":Ljava/lang/String;
    return-object v2
.end method

.method private static xdbFileOpen(I)I
    .locals 6
    .param p0, "FileID"    # I

    .prologue
    .line 1757
    const-string v3, ""

    .line 1758
    .local v3, "szFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1760
    .local v0, "Input":Ljava/io/DataInputStream;
    if-lez p0, :cond_3

    .line 1762
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v3

    .line 1765
    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1776
    .end local v0    # "Input":Ljava/io/DataInputStream;
    .local v1, "Input":Ljava/io/DataInputStream;
    if-eqz v1, :cond_0

    .line 1778
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1792
    :cond_0
    :goto_0
    const/4 v4, 0x0

    move-object v0, v1

    .end local v1    # "Input":Ljava/io/DataInputStream;
    .restart local v0    # "Input":Ljava/io/DataInputStream;
    :cond_1
    :goto_1
    return v4

    .line 1781
    .end local v0    # "Input":Ljava/io/DataInputStream;
    .restart local v1    # "Input":Ljava/io/DataInputStream;
    :catch_0
    move-exception v2

    .line 1783
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1767
    .end local v1    # "Input":Ljava/io/DataInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "Input":Ljava/io/DataInputStream;
    :catch_1
    move-exception v2

    .line 1769
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1770
    const/4 v4, 0x3

    .line 1776
    if-eqz v0, :cond_1

    .line 1778
    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 1781
    :catch_2
    move-exception v2

    .line 1783
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 1774
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 1776
    if-eqz v0, :cond_2

    .line 1778
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1784
    :cond_2
    :goto_2
    throw v4

    .line 1781
    :catch_3
    move-exception v2

    .line 1783
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 1789
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    const/4 v4, 0x5

    goto :goto_1
.end method

.method public static xdbFullResetFFS()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1911
    const/4 v1, 0x0

    .line 1915
    .local v1, "i":I
    sget-object v3, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileMax:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-virtual {v3}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v0

    .line 1916
    .local v0, "IndexLen":I
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 1918
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam;

    aget-object v2, v3, v1

    .line 1919
    .local v2, "pFileParam":Lcom/fmm/dm/db/file/XDBFileParam;
    iget v3, v2, Lcom/fmm/dm/db/file/XDBFileParam;->FileID:I

    invoke-static {v4, v3}, Lcom/fmm/dm/db/file/XDB;->xdbAdpFileExists(Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_0

    .line 1921
    iget v3, v2, Lcom/fmm/dm/db/file/XDBFileParam;->FileID:I

    invoke-static {v4, v3}, Lcom/fmm/dm/db/file/XDB;->xdbAdpFileDelete(Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_0

    .line 1923
    const/4 v3, 0x0

    iput v3, v2, Lcom/fmm/dm/db/file/XDBFileParam;->nSize:I

    .line 1916
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1928
    .end local v2    # "pFileParam":Lcom/fmm/dm/db/file/XDBFileParam;
    :cond_1
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    return-object v3
.end method

.method public static xdbFullRestAll()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1899
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbFullResetFFS()Ljava/lang/Object;

    .line 1900
    invoke-static {}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbFullReset()V

    .line 1901
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbInit()Z

    .line 1902
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbDMffs_Init()V

    .line 1903
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpEXTInit()Z

    .line 1904
    invoke-static {v0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetIsConnected(Z)V

    .line 1905
    invoke-static {v0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 1906
    invoke-static {v0}, Lcom/fmm/dm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 1907
    return-void
.end method

.method public static xdbGetAppID()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2677
    const-string v2, ""

    .line 2680
    .local v2, "szAppID":Ljava/lang/String;
    const/16 v3, 0x3b

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2686
    :goto_0
    return-object v2

    .line 2682
    :catch_0
    move-exception v1

    .line 2684
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetAuthLevel()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2691
    const-string v2, ""

    .line 2694
    .local v2, "szAuthLevel":Ljava/lang/String;
    const/16 v3, 0x3c

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2700
    :goto_0
    return-object v2

    .line 2696
    :catch_0
    move-exception v1

    .line 2698
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetAuthType()I
    .locals 3

    .prologue
    .line 2273
    const/4 v0, 0x0

    .line 2276
    .local v0, "authType":I
    const/16 v2, 0x44

    :try_start_0
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2282
    :goto_0
    return v0

    .line 2278
    :catch_0
    move-exception v1

    .line 2280
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetChangedProtocol()Z
    .locals 3

    .prologue
    .line 2339
    const/4 v0, 0x0

    .line 2342
    .local v0, "bChanged":Z
    const/16 v2, 0x4a

    :try_start_0
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2348
    :goto_0
    return v0

    .line 2344
    :catch_0
    move-exception v1

    .line 2346
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetClientNonce()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2803
    const-string v2, ""

    .line 2806
    .local v2, "szNonce":Ljava/lang/String;
    const/16 v3, 0x41

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2812
    :goto_0
    return-object v2

    .line 2808
    :catch_0
    move-exception v1

    .line 2810
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetClientPassword()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2761
    const-string v2, ""

    .line 2764
    .local v2, "szClientPassword":Ljava/lang/String;
    const/16 v3, 0x37

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2770
    :goto_0
    return-object v2

    .line 2766
    :catch_0
    move-exception v1

    .line 2768
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetConBack()Lcom/fmm/dm/db/file/XDBNetConProfileBackup;
    .locals 4

    .prologue
    .line 2905
    const/4 v2, 0x0

    .line 2908
    .local v2, "ptConBack":Lcom/fmm/dm/db/file/XDBNetConProfileBackup;
    const/16 v3, 0x49

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/fmm/dm/db/file/XDBNetConProfileBackup;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2914
    :goto_0
    return-object v2

    .line 2910
    :catch_0
    move-exception v1

    .line 2912
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetConRef()Lcom/fmm/dm/db/file/XDBInfoConRef;
    .locals 4

    .prologue
    .line 2879
    const/4 v2, 0x0

    .line 2882
    .local v2, "ptConRef":Lcom/fmm/dm/db/file/XDBInfoConRef;
    const/16 v3, 0x48

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/fmm/dm/db/file/XDBInfoConRef;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2888
    :goto_0
    return-object v2

    .line 2884
    :catch_0
    move-exception v1

    .line 2886
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetDMXNodeInfo(I)Ljava/lang/Object;
    .locals 3
    .param p0, "nIndex"    # I

    .prologue
    .line 2421
    const/4 v1, 0x0

    .line 2424
    .local v1, "ptAccXNodeInfo":Ljava/lang/Object;
    :try_start_0
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdb_E2P_XDM_ACCXNODE_INFO(I)I

    move-result v2

    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2430
    .end local v1    # "ptAccXNodeInfo":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 2426
    .restart local v1    # "ptAccXNodeInfo":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 2428
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetDmAgentType()I
    .locals 4

    .prologue
    .line 3294
    const/4 v1, 0x0

    .line 3298
    .local v1, "nRet":I
    const/16 v3, 0x6f

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 3299
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 3300
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3306
    :cond_0
    :goto_0
    return v1

    .line 3302
    :catch_0
    move-exception v0

    .line 3304
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetExists(I)Z
    .locals 2
    .param p0, "FileID"    # I

    .prologue
    .line 750
    const/4 v0, 0x0

    .line 752
    .local v0, "bExists":Z
    sparse-switch p0, :sswitch_data_0

    .line 766
    const-string v1, "Not Support file id----"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 769
    :goto_0
    return v0

    .line 755
    :sswitch_0
    const/16 v1, 0x58

    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlExistsRow(I)Z

    move-result v0

    .line 756
    goto :goto_0

    .line 759
    :sswitch_1
    const/16 v1, 0x59

    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlExistsRow(I)Z

    move-result v0

    .line 760
    goto :goto_0

    .line 763
    :sswitch_2
    const/16 v1, 0x60

    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlExistsRow(I)Z

    move-result v0

    .line 764
    goto :goto_0

    .line 752
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xfa -> :sswitch_2
        0x12c -> :sswitch_1
    .end sparse-switch
.end method

.method public static xdbGetFileIdObjectData()I
    .locals 1

    .prologue
    .line 2227
    sget-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileObjectData:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileId()I

    move-result v0

    return v0
.end method

.method public static xdbGetFileIdObjectTreeInfo()I
    .locals 1

    .prologue
    .line 2232
    sget-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileObjectTreeInfo:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileId()I

    move-result v0

    return v0
.end method

.method public static xdbGetFileIdTNDS()I
    .locals 1

    .prologue
    .line 2237
    sget-object v0, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileTndsXmlData:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileId()I

    move-result v0

    return v0
.end method

.method public static xdbGetFileSize(I)I
    .locals 6
    .param p0, "FileID"    # I

    .prologue
    .line 1863
    const/4 v2, 0x0

    .line 1864
    .local v2, "szFileName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1866
    .local v1, "nSize":I
    if-lez p0, :cond_0

    .line 1867
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v2

    .line 1873
    :try_start_0
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    invoke-virtual {v3, v2}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileGetSize(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    long-to-int v1, v4

    :goto_0
    move v3, v1

    .line 1879
    :goto_1
    return v3

    .line 1869
    :cond_0
    const/4 v3, -0x1

    goto :goto_1

    .line 1875
    :catch_0
    move-exception v0

    .line 1877
    .local v0, "ex":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xdbGetInternalFileSize(Ljava/io/File;)J
    .locals 13
    .param p0, "TargetFolder"    # Ljava/io/File;

    .prologue
    .line 2068
    const-string v9, ""

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2070
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 2071
    .local v0, "ChildFile":[Ljava/io/File;
    const/4 v3, 0x0

    .line 2072
    .local v3, "nSize":I
    const-wide/16 v4, 0x0

    .line 2074
    .local v4, "nTotalFileSize":J
    if-nez v0, :cond_0

    .line 2076
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "can not get child list of "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-wide v6, v4

    .line 2110
    .end local v4    # "nTotalFileSize":J
    .local v6, "nTotalFileSize":J
    :goto_0
    return-wide v6

    .line 2080
    .end local v6    # "nTotalFileSize":J
    .restart local v4    # "nTotalFileSize":J
    :cond_0
    array-length v3, v0

    .line 2081
    const-string v9, "nfilenum of directory [%s] : %d "

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2085
    if-lez v3, :cond_2

    .line 2087
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 2089
    :try_start_0
    aget-object v9, v0, v2

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 2090
    .local v8, "szName":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File name : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2091
    aget-object v9, v0, v2

    invoke-virtual {v9}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2093
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is file."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2094
    sget-object v9, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    aget-object v10, v0, v2

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileGetSize(Ljava/lang/String;)J

    move-result-wide v10

    add-long/2addr v4, v10

    .line 2087
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2098
    :cond_1
    aget-object v9, v0, v2

    invoke-static {v9}, Lcom/fmm/dm/db/file/XDB;->xdbGetInternalFileSize(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    add-long/2addr v4, v10

    goto :goto_2

    .line 2103
    .end local v8    # "szName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 2105
    .local v1, "ex":Ljava/lang/Exception;
    const-string v9, "fail to delete"

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-wide v6, v4

    .line 2106
    .end local v4    # "nTotalFileSize":J
    .restart local v6    # "nTotalFileSize":J
    goto :goto_0

    .line 2109
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "i":I
    .end local v6    # "nTotalFileSize":J
    .restart local v4    # "nTotalFileSize":J
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "nTotalFileSize = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-wide v6, v4

    .line 2110
    .end local v4    # "nTotalFileSize":J
    .restart local v6    # "nTotalFileSize":J
    goto/16 :goto_0
.end method

.method public static xdbGetNetworkConnName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2862
    const-string v2, ""

    .line 2865
    .local v2, "szNetConnName":Ljava/lang/String;
    const/4 v3, 0x7

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 2866
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NetConnName is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2874
    :goto_0
    return-object v2

    .line 2869
    :catch_0
    move-exception v1

    .line 2871
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNetworkIdx()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2955
    const-string v2, ""

    .line 2958
    .local v2, "szIdx":Ljava/lang/String;
    const/16 v3, 0x46

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2964
    :goto_0
    return-object v2

    .line 2960
    :catch_0
    move-exception v1

    .line 2962
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNofiInfo()Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    .locals 4

    .prologue
    .line 2969
    const/4 v2, 0x0

    .line 2972
    .local v2, "ptNofiInfo":Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    const/16 v3, 0xb

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2978
    :goto_0
    return-object v2

    .line 2974
    :catch_0
    move-exception v1

    .line 2976
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNonceResync()Z
    .locals 5

    .prologue
    .line 2447
    const/4 v0, 0x0

    .line 2451
    .local v0, "bResync":Z
    const/16 v3, 0x96

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 2452
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 2453
    check-cast v2, Ljava/lang/Boolean;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2454
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ResyncMode is"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2460
    :goto_0
    return v0

    .line 2456
    :catch_0
    move-exception v1

    .line 2458
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNotiDigest(Ljava/lang/String;I[BI)Ljava/lang/String;
    .locals 11
    .param p0, "szServerID"    # Ljava/lang/String;
    .param p1, "nAuthType"    # I
    .param p2, "pPacketBody"    # [B
    .param p3, "nBodyLen"    # I

    .prologue
    const/4 v0, 0x0

    .line 3218
    const-string v10, ""

    .line 3219
    .local v10, "szServerNonce":Ljava/lang/String;
    const-string v1, ""

    .line 3220
    .local v1, "szServerId":Ljava/lang/String;
    const-string v2, ""

    .line 3221
    .local v2, "szServerPwd":Ljava/lang/String;
    const-string v9, ""

    .line 3223
    .local v9, "szDigest":Ljava/lang/String;
    const/4 v4, 0x0

    .line 3225
    .local v4, "nNonceLen":I
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 3227
    const-string v5, "pServerID is NULL"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3263
    :cond_0
    :goto_0
    return-object v0

    .line 3231
    :cond_1
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbSetActiveProfileIndexByServerID(Ljava/lang/String;)I

    move-result v8

    .line 3234
    .local v8, "nActive":I
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerID()Ljava/lang/String;

    move-result-object v1

    .line 3235
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerPassword()Ljava/lang/String;

    move-result-object v2

    .line 3236
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerNonce()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 3243
    :goto_1
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3247
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nActive = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3250
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "szServerNonce = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3252
    const/4 v3, 0x0

    .line 3253
    .local v3, "pNonce":[B
    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v3

    .line 3256
    if-eqz v3, :cond_0

    .line 3261
    array-length v4, v3

    move v0, p1

    move-object v5, p2

    move v6, p3

    .line 3262
    invoke-static/range {v0 .. v6}, Lcom/fmm/dm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v9

    move-object v0, v9

    .line 3263
    goto :goto_0

    .line 3238
    .end local v3    # "pNonce":[B
    :catch_0
    move-exception v7

    .line 3240
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbGetNotiEvent()I
    .locals 4

    .prologue
    .line 2832
    const/4 v1, 0x0

    .line 2836
    .local v1, "nEvent":I
    const/16 v3, 0xa

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 2837
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 2838
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2844
    :cond_0
    :goto_0
    return v1

    .line 2840
    :catch_0
    move-exception v0

    .line 2842
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNotiReSyncMode()I
    .locals 4

    .prologue
    .line 3150
    const/4 v1, 0x0

    .line 3154
    .local v1, "nMode":I
    const/16 v3, 0xc

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 3155
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 3156
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3162
    :cond_0
    :goto_0
    return v1

    .line 3158
    :catch_0
    move-exception v0

    .line 3160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNotiSessionID()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2605
    const-string v2, ""

    .line 2608
    .local v2, "szSessionId":Ljava/lang/String;
    const/16 v3, 0x8

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2615
    :goto_0
    return-object v2

    .line 2610
    :catch_0
    move-exception v1

    .line 2612
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetPrefConRef()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2719
    const-string v2, ""

    .line 2722
    .local v2, "szPrefConRef":Ljava/lang/String;
    const/16 v3, 0x3e

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2728
    :goto_0
    return-object v2

    .line 2724
    :catch_0
    move-exception v1

    .line 2726
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProfileIndex()I
    .locals 3

    .prologue
    .line 2245
    const/4 v1, 0x0

    .line 2248
    .local v1, "nIdx":I
    const/4 v2, 0x2

    :try_start_0
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2254
    :goto_0
    return v1

    .line 2250
    :catch_0
    move-exception v0

    .line 2252
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProfileInfo()Lcom/fmm/dm/db/file/XDBProfileInfo;
    .locals 4

    .prologue
    .line 2353
    new-instance v2, Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBProfileInfo;-><init>()V

    .line 2356
    .local v2, "ptProfileInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    const/16 v3, 0x32

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2362
    :goto_0
    return-object v2

    .line 2358
    :catch_0
    move-exception v1

    .line 2360
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProfileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2395
    const-string v2, "server1"

    .line 2398
    .local v2, "szProfileName":Ljava/lang/String;
    const/16 v3, 0x47

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2404
    :goto_0
    return-object v2

    .line 2400
    :catch_0
    move-exception v1

    .line 2402
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProflieIdx()I
    .locals 4

    .prologue
    .line 2514
    const/4 v1, 0x0

    .line 2518
    .local v1, "nIdx":I
    const/4 v3, 0x2

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 2519
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 2520
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2526
    :cond_0
    :goto_0
    return v1

    .line 2522
    :catch_0
    move-exception v0

    .line 2524
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProflieList(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "ptProflieList"    # Ljava/lang/Object;

    .prologue
    .line 2491
    const/4 v1, 0x6

    :try_start_0
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 2497
    :goto_0
    return-object p0

    .line 2493
    :catch_0
    move-exception v0

    .line 2495
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProtocol()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2579
    const-string v2, ""

    .line 2582
    .local v2, "szProtocol":Ljava/lang/String;
    const/16 v3, 0x34

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2588
    :goto_0
    return-object v2

    .line 2584
    :catch_0
    move-exception v1

    .line 2586
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerAddress()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2663
    const-string v2, ""

    .line 2666
    .local v2, "szServerIP":Ljava/lang/String;
    const/16 v3, 0x39

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2672
    :goto_0
    return-object v2

    .line 2668
    :catch_0
    move-exception v1

    .line 2670
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerAuthLevel()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2705
    const-string v2, ""

    .line 2708
    .local v2, "szAuthLevel":Ljava/lang/String;
    const/16 v3, 0x3d

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2714
    :goto_0
    return-object v2

    .line 2710
    :catch_0
    move-exception v1

    .line 2712
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerAuthType()I
    .locals 3

    .prologue
    .line 2287
    const/4 v0, 0x0

    .line 2290
    .local v0, "authType":I
    const/16 v2, 0x45

    :try_start_0
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2296
    :goto_0
    return v0

    .line 2292
    :catch_0
    move-exception v1

    .line 2294
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerID()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2733
    const-string v2, ""

    .line 2736
    .local v2, "szSeverid":Ljava/lang/String;
    const/16 v3, 0x3f

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2742
    :goto_0
    return-object v2

    .line 2738
    :catch_0
    move-exception v1

    .line 2740
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerNonce()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2789
    const-string v2, ""

    .line 2792
    .local v2, "szNonce":Ljava/lang/String;
    const/16 v3, 0x42

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2798
    :goto_0
    return-object v2

    .line 2794
    :catch_0
    move-exception v1

    .line 2796
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerPassword()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2775
    const-string v2, ""

    .line 2778
    .local v2, "szServerPassword":Ljava/lang/String;
    const/16 v3, 0x40

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2784
    :goto_0
    return-object v2

    .line 2780
    :catch_0
    move-exception v1

    .line 2782
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerPort()I
    .locals 3

    .prologue
    .line 2817
    const/4 v1, 0x0

    .line 2820
    .local v1, "port":I
    const/16 v2, 0x38

    :try_start_0
    invoke-static {v2}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2827
    :goto_0
    return v1

    .line 2822
    :catch_0
    move-exception v0

    .line 2824
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2649
    const-string v2, ""

    .line 2652
    .local v2, "szServerUrl":Ljava/lang/String;
    const/16 v3, 0x43

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2658
    :goto_0
    return-object v2

    .line 2654
    :catch_0
    move-exception v1

    .line 2656
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetSessionSaveStatus()Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    .locals 5

    .prologue
    .line 3102
    new-instance v2, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;-><init>()V

    .line 3105
    .local v2, "pSessionSaveInfo":Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    const/16 v3, 0xb

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3112
    :goto_0
    if-nez v2, :cond_0

    .line 3114
    const-string v3, "Read Error"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3115
    const/4 v2, 0x0

    .line 3120
    :goto_1
    return-object v2

    .line 3107
    :catch_0
    move-exception v1

    .line 3109
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 3119
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nSessionSaveState ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], nNotiUiEvent ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbGetSettingContentProviderDB(Ljava/lang/String;)Z
    .locals 6
    .param p0, "szUrl"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3354
    const/4 v1, 0x0

    .line 3357
    .local v1, "nCheck":I
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, p0, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3363
    :goto_0
    if-ne v1, v2, :cond_0

    :goto_1
    return v2

    .line 3359
    :catch_0
    move-exception v0

    .line 3361
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    move v2, v3

    .line 3363
    goto :goto_1
.end method

.method public static xdbGetSettingRemoteControlEnabled()Z
    .locals 6

    .prologue
    .line 3381
    const/4 v0, 0x0

    .line 3382
    .local v0, "bMTEnable":Z
    const/4 v2, 0x0

    .line 3392
    .local v2, "state":I
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "remote_control"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 3399
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 3408
    const/4 v0, 0x0

    .line 3413
    :goto_1
    invoke-static {}, Lcom/fmm/dm/adapter/XDMTargetAdapter;->xdmCheckActivationLock()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3415
    const-string v3, "Activation Lock : true, run operation"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3416
    const/4 v0, 0x1

    .line 3419
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remote Control Status = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3420
    return v0

    .line 3394
    :catch_0
    move-exception v1

    .line 3396
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Read Fail Remote Control"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 3402
    .end local v1    # "e":Ljava/lang/Exception;
    :pswitch_0
    const/4 v0, 0x0

    .line 3403
    goto :goto_1

    .line 3405
    :pswitch_1
    const/4 v0, 0x1

    .line 3406
    goto :goto_1

    .line 3399
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xdbGetSimIMSI()Lcom/fmm/dm/db/file/XDBSimInfo;
    .locals 4

    .prologue
    .line 3184
    const/4 v2, 0x0

    .line 3187
    .local v2, "pIMSI":Lcom/fmm/dm/db/file/XDBSimInfo;
    const/16 v3, 0x78

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/fmm/dm/db/file/XDBSimInfo;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3193
    :goto_0
    return-object v2

    .line 3189
    :catch_0
    move-exception v1

    .line 3191
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xdbGetSyncMLFileParamAreaCode(I)Ljava/lang/Object;
    .locals 3
    .param p0, "areacode"    # I

    .prologue
    .line 1679
    const/16 v1, 0x51

    if-ne p0, v1, :cond_0

    .line 1681
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_DM_PROFILE:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .line 1721
    :goto_0
    return-object v0

    .line 1683
    :cond_0
    const/16 v1, 0x52

    if-lt p0, v1, :cond_1

    const/16 v1, 0x54

    if-gt p0, v1, :cond_1

    .line 1685
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_DM_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .local v0, "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    goto :goto_0

    .line 1687
    .end local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    :cond_1
    const/16 v1, 0x55

    if-ne p0, v1, :cond_2

    .line 1689
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_IMSI_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    goto :goto_0

    .line 1691
    .end local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    :cond_2
    const/16 v1, 0x83

    if-ne p0, v1, :cond_3

    .line 1693
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_DM_ACC_X_NODE:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    goto :goto_0

    .line 1695
    .end local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    :cond_3
    const/16 v1, 0x56

    if-ne p0, v1, :cond_4

    .line 1697
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_NOTI_RESYNC_MODE:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    goto :goto_0

    .line 1699
    .end local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    :cond_4
    const/16 v1, 0x57

    if-ne p0, v1, :cond_5

    .line 1701
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_DM_AGENT_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    goto :goto_0

    .line 1703
    .end local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    :cond_5
    const/16 v1, 0x58

    if-ne p0, v1, :cond_6

    .line 1705
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_LAWMO_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    goto :goto_0

    .line 1707
    .end local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    :cond_6
    const/16 v1, 0x59

    if-ne p0, v1, :cond_7

    .line 1709
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_AMT_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    goto :goto_0

    .line 1711
    .end local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    :cond_7
    const/16 v1, 0x60

    if-ne p0, v1, :cond_8

    .line 1713
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_NOTI_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    goto :goto_0

    .line 1717
    .end local v0    # "SyncMLFileParamtemp":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FFS not find area code by num :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1718
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static xdbGetUsername()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2747
    const-string v2, ""

    .line 2750
    .local v2, "szUserName":Ljava/lang/String;
    const/16 v3, 0x36

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2756
    :goto_0
    return-object v2

    .line 2752
    :catch_0
    move-exception v1

    .line 2754
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbInit()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 175
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->values()[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    move-result-object v1

    .line 177
    .local v1, "Files":[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    sget-object v7, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileMax:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-virtual {v7}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v7

    new-array v7, v7, [Lcom/fmm/dm/db/file/XDBFileParam;

    sput-object v7, Lcom/fmm/dm/db/file/XDB;->XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam;

    .line 179
    move-object v2, v1

    .local v2, "arr$":[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v2, v4

    .line 181
    .local v0, "File":Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v7

    sget-object v8, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileMax:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-virtual {v8}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v8

    if-ne v7, v8, :cond_1

    .line 190
    .end local v0    # "File":Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    :cond_0
    sget v7, Lcom/fmm/dm/db/file/XDB;->XDMNVMParamCount:I

    new-array v7, v7, [Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sput-object v7, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    .line 191
    sget-object v7, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    sget v8, Lcom/fmm/dm/db/file/XDB;->XDMNVMParamCount:I

    invoke-static {v7, v8}, Lcom/fmm/dm/db/file/XDB;->xdbSyncMLFileParmInit([Lcom/fmm/dm/db/file/XDBFileNVMParam;I)[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    move-result-object v7

    sput-object v7, Lcom/fmm/dm/db/file/XDB;->xdmSyncMLFileParam:[Lcom/fmm/dm/db/file/XDBFileNVMParam;

    .line 193
    const/4 v7, 0x3

    new-array v7, v7, [Lcom/fmm/dm/db/file/XDBProfileInfo;

    sput-object v7, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 194
    new-instance v7, Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    invoke-direct {v7}, Lcom/fmm/dm/db/file/XDBNetworkProfileList;-><init>()V

    sput-object v7, Lcom/fmm/dm/db/file/XDB;->NetProfileClass:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    .line 195
    new-instance v7, Lcom/fmm/dm/db/file/XDBNvm;

    invoke-direct {v7}, Lcom/fmm/dm/db/file/XDBNvm;-><init>()V

    sput-object v7, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    .line 196
    new-instance v7, Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-direct {v7}, Lcom/fmm/dm/db/file/XDBInfoConRef;-><init>()V

    sput-object v7, Lcom/fmm/dm/db/file/XDB;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 197
    new-instance v7, Lcom/fmm/dm/db/file/XDBAdapter;

    invoke-direct {v7}, Lcom/fmm/dm/db/file/XDBAdapter;-><init>()V

    sput-object v7, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    .line 205
    const/4 v6, 0x1

    .end local v2    # "arr$":[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :goto_1
    return v6

    .line 184
    .restart local v0    # "File":Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    .restart local v2    # "arr$":[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_1
    sget-object v7, Lcom/fmm/dm/db/file/XDB;->XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam;

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v8

    new-instance v9, Lcom/fmm/dm/db/file/XDBFileParam;

    invoke-direct {v9}, Lcom/fmm/dm/db/file/XDBFileParam;-><init>()V

    aput-object v9, v7, v8

    .line 185
    sget-object v7, Lcom/fmm/dm/db/file/XDB;->XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam;

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileId()I

    move-result v8

    iput v8, v7, Lcom/fmm/dm/db/file/XDBFileParam;->FileID:I

    .line 186
    sget-object v7, Lcom/fmm/dm/db/file/XDB;->XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam;

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v8

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/fmm/dm/db/file/XDBFileParam;->hFile:I

    .line 187
    sget-object v7, Lcom/fmm/dm/db/file/XDB;->XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam;

    invoke-virtual {v0}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v8

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/fmm/dm/db/file/XDBFileParam;->nSize:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 199
    .end local v0    # "File":Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    .end local v2    # "arr$":[Lcom/fmm/dm/db/file/XDB$XDMFileParameter;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_0
    move-exception v3

    .line 201
    .local v3, "ex":Ljava/lang/Exception;
    const-string v7, "xdbInit Exception"

    invoke-static {v7}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbInitFfsFile(II)Z
    .locals 5
    .param p0, "FileID"    # I
    .param p1, "nMagicNumber"    # I

    .prologue
    const/4 v2, 0x0

    .line 1640
    const/4 v1, 0x0

    .line 1641
    .local v1, "wRC":I
    const/4 v0, 0x0

    .line 1643
    .local v0, "pfileparam":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbGetSyncMLFileParamAreaCode(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "pfileparam":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    check-cast v0, Lcom/fmm/dm/db/file/XDBFileNVMParam;

    .line 1644
    .restart local v0    # "pfileparam":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    if-nez v0, :cond_1

    .line 1670
    :cond_0
    :goto_0
    return v2

    .line 1647
    :cond_1
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbSetSyncMLNVMUser(Lcom/fmm/dm/db/file/XDBFileNVMParam;)Lcom/fmm/dm/db/file/XDBFileNVMParam;

    move-result-object v0

    .line 1648
    if-eqz v0, :cond_0

    .line 1651
    iget v3, v0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    iget-object v4, v0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    invoke-static {v3, v4, p1}, Lcom/fmm/dm/db/file/XDB;->xdbLoadCallback(ILjava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    .line 1652
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    if-eqz v3, :cond_0

    .line 1655
    const/16 v3, 0x51

    if-lt p0, v3, :cond_0

    const/16 v3, 0x60

    if-gt p0, v3, :cond_0

    .line 1657
    iget-object v3, v0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    invoke-static {p0, v3}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlCreate(ILjava/lang/Object;)I

    move-result v1

    .line 1664
    if-nez v1, :cond_0

    .line 1666
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static xdbInitNVMSyncDMInfo(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 2
    .param p0, "SyncdmInfo"    # Ljava/lang/Object;
    .param p1, "nMagicNumber"    # I

    .prologue
    .line 1544
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 1545
    .local v0, "NVMSyncMLDMInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    const/4 v1, 0x0

    .line 1547
    .local v1, "nProfileIndex":I
    add-int/lit16 v1, p1, -0x933

    .line 1548
    invoke-static {v0, v1}, Lcom/fmm/dm/db/file/XDB;->xdbFBGetFactoryBootstrapData(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "NVMSyncMLDMInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    check-cast v0, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 1549
    .restart local v0    # "NVMSyncMLDMInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    invoke-static {v0, v1}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbAdpInitNetProfile(Ljava/lang/Object;I)V

    .line 1550
    iput p1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->MagicNumber:I

    .line 1551
    return-object v0
.end method

.method static xdbInitNVMSyncDMProfile(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 5
    .param p0, "pProfileList"    # Ljava/lang/Object;
    .param p1, "nMagicNumber"    # I

    .prologue
    .line 1512
    move-object v1, p0

    check-cast v1, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 1513
    .local v1, "ptProflieList":Lcom/fmm/dm/db/file/XDBProflieListInfo;
    const/4 v0, 0x0

    .line 1514
    .local v0, "nCount":I
    const-string v2, ""

    .line 1515
    .local v2, "szTemp":Ljava/lang/String;
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    if-nez v3, :cond_1

    .line 1517
    :cond_0
    const-string v3, "XDMNvmClass or XDMNvmClass.tProfileList is null. return"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1518
    const/4 p0, 0x0

    .line 1539
    .end local p0    # "pProfileList":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 1521
    .restart local p0    # "pProfileList":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v3, 0x3

    if-ge v0, v3, :cond_3

    .line 1523
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput v0, v3, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 1524
    const/16 v3, 0x47

    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "szTemp":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 1526
    .restart local v2    # "szTemp":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1528
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerID()Ljava/lang/String;

    move-result-object v2

    .line 1529
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1530
    const-string v2, ""

    .line 1532
    :cond_2
    iget-object v3, v1, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    aput-object v2, v3, v0

    .line 1521
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1534
    :cond_3
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v3, v3, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    const/4 v4, 0x0

    iput v4, v3, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 1536
    iput p1, v1, Lcom/fmm/dm/db/file/XDBProflieListInfo;->MagicNumber:I

    .line 1537
    const-string v3, "DM Profile"

    iput-object v3, v1, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szNetworkConnName:Ljava/lang/String;

    goto :goto_0
.end method

.method private static xdbInitNVMSyncDMSimIMSI(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "SyncDMSimInfo"    # Ljava/lang/Object;

    .prologue
    .line 1556
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBSimInfo;

    .line 1557
    .local v0, "pSimInfo":Lcom/fmm/dm/db/file/XDBSimInfo;
    return-object v0
.end method

.method public static xdbInsert(ILjava/lang/Object;)Z
    .locals 2
    .param p0, "FileID"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    .line 774
    const/4 v0, 0x0

    .line 776
    .local v0, "wRC":I
    sparse-switch p0, :sswitch_data_0

    .line 791
    const-string v1, "Not Support file id----"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 794
    :goto_0
    if-nez v0, :cond_0

    .line 796
    const/4 v1, 0x1

    .line 800
    :goto_1
    return v1

    .line 779
    :sswitch_0
    const/16 v1, 0x58

    invoke-static {v1, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlCreate(ILjava/lang/Object;)I

    move-result v0

    .line 780
    goto :goto_0

    .line 783
    :sswitch_1
    const/16 v1, 0x59

    invoke-static {v1, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlCreate(ILjava/lang/Object;)I

    move-result v0

    .line 784
    goto :goto_0

    .line 787
    :sswitch_2
    const/16 v1, 0x60

    invoke-static {v1, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlCreate(ILjava/lang/Object;)I

    move-result v0

    .line 788
    goto :goto_0

    .line 800
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 776
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xfa -> :sswitch_2
        0x12c -> :sswitch_1
    .end sparse-switch
.end method

.method public static xdbLawmoDbRead(I)Ljava/lang/Object;
    .locals 1
    .param p0, "nType"    # I

    .prologue
    .line 309
    packed-switch p0, :pswitch_data_0

    .line 387
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 390
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 312
    :pswitch_0
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    goto :goto_0

    .line 315
    :pswitch_1
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nState:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 318
    :pswitch_2
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    goto :goto_0

    .line 321
    :pswitch_3
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-boolean v0, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 324
    :pswitch_4
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    goto :goto_0

    .line 327
    :pswitch_5
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iget-boolean v0, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 330
    :pswitch_6
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-boolean v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bNotifyUser:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 333
    :pswitch_7
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nOperation:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 336
    :pswitch_8
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nExtOperation:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 339
    :pswitch_9
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPin:Ljava/lang/String;

    goto :goto_0

    .line 342
    :pswitch_a
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nResultReportType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 345
    :pswitch_b
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    goto :goto_0

    .line 348
    :pswitch_c
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    goto :goto_0

    .line 351
    :pswitch_d
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 354
    :pswitch_e
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nAmtStaus:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 357
    :pswitch_f
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-boolean v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 360
    :pswitch_10
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-boolean v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bSimChangeState:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 363
    :pswitch_11
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szLockMyPhoneMessages:Ljava/lang/String;

    goto/16 :goto_0

    .line 366
    :pswitch_12
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPassword:Ljava/lang/String;

    goto/16 :goto_0

    .line 369
    :pswitch_13
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szReactivationLock:Ljava/lang/String;

    goto/16 :goto_0

    .line 372
    :pswitch_14
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionPhoneNumber:Ljava/lang/String;

    goto/16 :goto_0

    .line 375
    :pswitch_15
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionStatus:Ljava/lang/String;

    goto/16 :goto_0

    .line 378
    :pswitch_16
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneStatus:Ljava/lang/String;

    goto/16 :goto_0

    .line 381
    :pswitch_17
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneMessages:Ljava/lang/String;

    goto/16 :goto_0

    .line 384
    :pswitch_18
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCorrelator:Ljava/lang/String;

    goto/16 :goto_0

    .line 309
    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_11
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public static xdbLawmoDbWrite(ILjava/lang/Object;)V
    .locals 2
    .param p0, "nType"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    .line 395
    packed-switch p0, :pswitch_data_0

    .line 498
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 501
    .end local p1    # "Input":Ljava/lang/Object;
    :goto_0
    return-void

    .line 398
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_0
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    goto :goto_0

    .line 402
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_1
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nState:I

    goto :goto_0

    .line 406
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_2
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    goto :goto_0

    .line 410
    :pswitch_3
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    goto :goto_0

    .line 414
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_4
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    goto :goto_0

    .line 418
    :pswitch_5
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    goto :goto_0

    .line 422
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_6
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bNotifyUser:Z

    goto :goto_0

    .line 426
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_7
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nOperation:I

    goto :goto_0

    .line 430
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_8
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nExtOperation:I

    goto :goto_0

    .line 434
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_9
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPin:Ljava/lang/String;

    goto/16 :goto_0

    .line 438
    :pswitch_a
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nResultReportType:I

    goto/16 :goto_0

    .line 442
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_b
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 446
    :pswitch_c
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 450
    :pswitch_d
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 454
    :pswitch_e
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nAmtStaus:I

    goto/16 :goto_0

    .line 458
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_f
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    goto/16 :goto_0

    .line 462
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_10
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bSimChangeState:Z

    goto/16 :goto_0

    .line 466
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_11
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPassword:Ljava/lang/String;

    goto/16 :goto_0

    .line 470
    :pswitch_12
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szLockMyPhoneMessages:Ljava/lang/String;

    goto/16 :goto_0

    .line 474
    :pswitch_13
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szReactivationLock:Ljava/lang/String;

    goto/16 :goto_0

    .line 478
    :pswitch_14
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionPhoneNumber:Ljava/lang/String;

    goto/16 :goto_0

    .line 482
    :pswitch_15
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionStatus:Ljava/lang/String;

    goto/16 :goto_0

    .line 486
    :pswitch_16
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneStatus:Ljava/lang/String;

    goto/16 :goto_0

    .line 490
    :pswitch_17
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneMessages:Ljava/lang/String;

    goto/16 :goto_0

    .line 494
    :pswitch_18
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCorrelator:Ljava/lang/String;

    goto/16 :goto_0

    .line 395
    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method private static xdbLoadCallback(ILjava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0, "areaCode"    # I
    .param p1, "pNVMUser"    # Ljava/lang/Object;
    .param p2, "magicNumber"    # I

    .prologue
    .line 1596
    const/16 v0, 0x51

    if-ne p0, v0, :cond_1

    .line 1598
    invoke-static {p1, p2}, Lcom/fmm/dm/db/file/XDB;->xdbInitNVMSyncDMProfile(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    .line 1635
    :cond_0
    :goto_0
    return-object p1

    .line 1600
    :cond_1
    const/16 v0, 0x52

    if-lt p0, v0, :cond_2

    const/16 v0, 0x54

    if-gt p0, v0, :cond_2

    .line 1602
    invoke-static {p1, p2}, Lcom/fmm/dm/db/file/XDB;->xdbInitNVMSyncDMInfo(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1604
    :cond_2
    const/16 v0, 0x55

    if-ne p0, v0, :cond_3

    .line 1606
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDB;->xdbInitNVMSyncDMSimIMSI(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1608
    :cond_3
    const/16 v0, 0x83

    if-ne p0, v0, :cond_4

    .line 1611
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDB;->xdbNVMSyncDMAccXNodeInit(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1613
    :cond_4
    const/16 v0, 0x56

    if-ne p0, v0, :cond_5

    .line 1616
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDB;->xdbNVMSyncDMResyncModeInit(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1618
    :cond_5
    const/16 v0, 0x57

    if-ne p0, v0, :cond_6

    .line 1620
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDB;->xdbNVMSyncDmAgentInfoInit(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1622
    :cond_6
    const/16 v0, 0x58

    if-ne p0, v0, :cond_7

    .line 1624
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoInitNVMLawmoInfo(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1626
    :cond_7
    const/16 v0, 0x59

    if-ne p0, v0, :cond_8

    .line 1628
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDBAMT;->xdbAmtInitNVM(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1630
    :cond_8
    const/16 v0, 0x60

    if-ne p0, v0, :cond_0

    .line 1632
    invoke-static {p1}, Lcom/fmm/dm/db/file/XDBNoti;->xdbNotiInitNVM(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0
.end method

.method private static xdbNVMSyncDMAccXNodeInit(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "SyncDMAccXNode"    # Ljava/lang/Object;

    .prologue
    .line 1562
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBAccXListNode;

    .line 1563
    .local v0, "stSyncDMAccXNode":Lcom/fmm/dm/db/file/XDBAccXListNode;
    return-object v0
.end method

.method private static xdbNVMSyncDMResyncModeInit(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "SyncDMResyncMode"    # Ljava/lang/Object;

    .prologue
    .line 1568
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .line 1574
    .local v0, "bResyncMode":Lcom/fmm/dm/db/file/XDBResyncModeInfo;
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/fmm/dm/db/file/XDBResyncModeInfo;->nNoceResyncMode:Z

    .line 1575
    return-object v0
.end method

.method private static xdbNVMSyncDmAgentInfoInit(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "NVMDmAgentInfo"    # Ljava/lang/Object;

    .prologue
    .line 1580
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBAgentInfo;

    .line 1582
    .local v0, "DmAgentInfo":Lcom/fmm/dm/db/file/XDBAgentInfo;
    if-nez v0, :cond_0

    .line 1584
    new-instance v0, Lcom/fmm/dm/db/file/XDBAgentInfo;

    .end local v0    # "DmAgentInfo":Lcom/fmm/dm/db/file/XDBAgentInfo;
    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBAgentInfo;-><init>()V

    .line 1591
    .restart local v0    # "DmAgentInfo":Lcom/fmm/dm/db/file/XDBAgentInfo;
    :goto_0
    return-object v0

    .line 1588
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lcom/fmm/dm/db/file/XDBAgentInfo;->m_nAgentType:I

    goto :goto_0
.end method

.method public static xdbNotiDbRead(I)Ljava/lang/Object;
    .locals 1
    .param p0, "nType"    # I

    .prologue
    .line 694
    packed-switch p0, :pswitch_data_0

    .line 712
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 715
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 697
    :pswitch_0
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    goto :goto_0

    .line 700
    :pswitch_1
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBNotiInfo;->appId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 703
    :pswitch_2
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    iget v0, v0, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 706
    :pswitch_3
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    goto :goto_0

    .line 709
    :pswitch_4
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    goto :goto_0

    .line 694
    nop

    :pswitch_data_0
    .packed-switch 0xfa
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static xdbNotiDbWrite(ILjava/lang/Object;)V
    .locals 2
    .param p0, "nType"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    .line 720
    packed-switch p0, :pswitch_data_0

    .line 743
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 746
    .end local p1    # "Input":Ljava/lang/Object;
    :goto_0
    return-void

    .line 723
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_0
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p1, Lcom/fmm/dm/db/file/XDBNotiInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    check-cast p1, Lcom/fmm/dm/db/file/XDBNotiInfo;

    iput-object p1, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    goto :goto_0

    .line 727
    .restart local p1    # "Input":Ljava/lang/Object;
    :pswitch_1
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBNotiInfo;->appId:I

    goto :goto_0

    .line 731
    :pswitch_2
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBNotiInfo;->uiMode:I

    goto :goto_0

    .line 735
    :pswitch_3
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    goto :goto_0

    .line 739
    :pswitch_4
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    goto :goto_0

    .line 720
    :pswitch_data_0
    .packed-switch 0xfa
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static xdbRead(I)Ljava/lang/Object;
    .locals 9
    .param p0, "nType"    # I

    .prologue
    const/16 v8, 0x51

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 870
    if-ltz p0, :cond_0

    const/16 v2, 0xf

    if-ge p0, v2, :cond_0

    .line 872
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    invoke-static {v8}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 873
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    if-nez v2, :cond_f

    move-object v2, v3

    .line 1075
    :goto_0
    return-object v2

    .line 877
    :cond_0
    const/16 v2, 0x48

    if-ne p0, v2, :cond_1

    .line 879
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    const/16 v2, 0x80

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 880
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    if-nez v2, :cond_f

    move-object v2, v3

    .line 881
    goto :goto_0

    .line 883
    :cond_1
    const/16 v2, 0x32

    if-lt p0, v2, :cond_3

    const/16 v2, 0x4b

    if-ge p0, v2, :cond_3

    .line 885
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    invoke-static {v8}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 886
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    if-nez v2, :cond_2

    move-object v2, v3

    .line 887
    goto :goto_0

    .line 888
    :cond_2
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget v1, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 889
    .local v1, "rowid":I
    const/4 v0, 0x0

    .line 890
    .local v0, "FileID":I
    packed-switch v1, :pswitch_data_0

    .line 904
    :goto_1
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBProfileInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 906
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    if-nez v2, :cond_f

    move-object v2, v3

    .line 907
    goto :goto_0

    .line 893
    :pswitch_0
    const/16 v0, 0x52

    .line 894
    goto :goto_1

    .line 896
    :pswitch_1
    const/16 v0, 0x53

    .line 897
    goto :goto_1

    .line 899
    :pswitch_2
    const/16 v0, 0x54

    .line 900
    goto :goto_1

    .line 910
    .end local v0    # "FileID":I
    .end local v1    # "rowid":I
    :cond_3
    const/16 v2, 0x64

    if-lt p0, v2, :cond_4

    const/16 v2, 0x67

    if-ge p0, v2, :cond_4

    .line 912
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x83

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBAccXListNode;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    .line 913
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    if-nez v2, :cond_f

    move-object v2, v3

    .line 914
    goto :goto_0

    .line 917
    :cond_4
    const/16 v2, 0x96

    if-lt p0, v2, :cond_5

    const/16 v2, 0x97

    if-ge p0, v2, :cond_5

    .line 919
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x56

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    .line 920
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    if-nez v2, :cond_f

    move-object v2, v3

    .line 921
    goto/16 :goto_0

    .line 924
    :cond_5
    const/16 v2, 0x78

    if-lt p0, v2, :cond_6

    const/16 v2, 0x79

    if-ge p0, v2, :cond_6

    .line 926
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x55

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBSimInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    .line 927
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    if-nez v2, :cond_f

    move-object v2, v3

    .line 928
    goto/16 :goto_0

    .line 930
    :cond_6
    const/16 v2, 0x6e

    if-lt p0, v2, :cond_8

    const/16 v2, 0x70

    if-ge p0, v2, :cond_8

    .line 932
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x57

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBAgentInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    .line 933
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    if-nez v2, :cond_7

    move-object v2, v3

    .line 934
    goto/16 :goto_0

    .line 936
    :cond_7
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbAgentInfoDbRead(I)Ljava/lang/Object;

    move-result-object v2

    goto/16 :goto_0

    .line 940
    :cond_8
    const/16 v2, 0xc8

    if-lt p0, v2, :cond_a

    const/16 v2, 0xe1

    if-ge p0, v2, :cond_a

    .line 942
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x58

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 943
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    if-nez v2, :cond_9

    move-object v2, v3

    .line 944
    goto/16 :goto_0

    .line 946
    :cond_9
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbLawmoDbRead(I)Ljava/lang/Object;

    move-result-object v2

    goto/16 :goto_0

    .line 949
    :cond_a
    const/16 v2, 0x12c

    if-lt p0, v2, :cond_c

    const/16 v2, 0x144

    if-ge p0, v2, :cond_c

    .line 951
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x59

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBAMTInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 952
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    if-nez v2, :cond_b

    move-object v2, v3

    .line 953
    goto/16 :goto_0

    .line 955
    :cond_b
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbAmtDbRead(I)Ljava/lang/Object;

    move-result-object v2

    goto/16 :goto_0

    .line 957
    :cond_c
    const/16 v2, 0xfa

    if-lt p0, v2, :cond_e

    const/16 v2, 0xff

    if-ge p0, v2, :cond_e

    .line 959
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x60

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBNotiInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    .line 960
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    if-nez v2, :cond_d

    move-object v2, v3

    .line 961
    goto/16 :goto_0

    .line 963
    :cond_d
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbNotiDbRead(I)Ljava/lang/Object;

    move-result-object v2

    goto/16 :goto_0

    .line 967
    :cond_e
    const-string v2, "----wrong file id----"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v2, v3

    .line 968
    goto/16 :goto_0

    .line 971
    :cond_f
    sparse-switch p0, :sswitch_data_0

    move-object v2, v3

    .line 1075
    goto/16 :goto_0

    .line 974
    :sswitch_0
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->MagicNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 976
    :sswitch_1
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nProxyIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 978
    :sswitch_2
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 980
    :sswitch_3
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    aget-object v2, v2, v5

    goto/16 :goto_0

    .line 982
    :sswitch_4
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    aget-object v2, v2, v6

    goto/16 :goto_0

    .line 984
    :sswitch_5
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    aget-object v2, v2, v7

    goto/16 :goto_0

    .line 986
    :sswitch_6
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    goto/16 :goto_0

    .line 988
    :sswitch_7
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szNetworkConnName:Ljava/lang/String;

    goto/16 :goto_0

    .line 990
    :sswitch_8
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szSessionID:Ljava/lang/String;

    goto/16 :goto_0

    .line 992
    :sswitch_9
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-wide v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nDestoryNotiTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_0

    .line 994
    :sswitch_a
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiEvent:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 996
    :sswitch_b
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    goto/16 :goto_0

    .line 999
    :sswitch_c
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiReSyncMode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 1001
    :sswitch_d
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    goto/16 :goto_0

    .line 1003
    :sswitch_e
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->eStatus:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 1005
    :sswitch_f
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    goto/16 :goto_0

    .line 1007
    :sswitch_10
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->MagicNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 1009
    :sswitch_11
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    goto/16 :goto_0

    .line 1011
    :sswitch_12
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ObexType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 1013
    :sswitch_13
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    goto/16 :goto_0

    .line 1015
    :sswitch_14
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    goto/16 :goto_0

    .line 1017
    :sswitch_15
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 1019
    :sswitch_16
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    goto/16 :goto_0

    .line 1021
    :sswitch_17
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    goto/16 :goto_0

    .line 1024
    :sswitch_18
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    goto/16 :goto_0

    .line 1027
    :sswitch_19
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    goto/16 :goto_0

    .line 1030
    :sswitch_1a
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    goto/16 :goto_0

    .line 1033
    :sswitch_1b
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    goto/16 :goto_0

    .line 1035
    :sswitch_1c
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    goto/16 :goto_0

    .line 1037
    :sswitch_1d
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    goto/16 :goto_0

    .line 1039
    :sswitch_1e
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    goto/16 :goto_0

    .line 1041
    :sswitch_1f
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    goto/16 :goto_0

    .line 1043
    :sswitch_20
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 1045
    :sswitch_21
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 1047
    :sswitch_22
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 1049
    :sswitch_23
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->nNetworkConnIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 1051
    :sswitch_24
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    goto/16 :goto_0

    .line 1053
    :sswitch_25
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    goto/16 :goto_0

    .line 1055
    :sswitch_26
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConBackup:Lcom/fmm/dm/db/file/XDBNetConProfileBackup;

    goto/16 :goto_0

    .line 1057
    :sswitch_27
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-boolean v2, v2, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 1060
    :sswitch_28
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aget-object v2, v2, v5

    goto/16 :goto_0

    .line 1063
    :sswitch_29
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aget-object v2, v2, v6

    goto/16 :goto_0

    .line 1066
    :sswitch_2a
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aget-object v2, v2, v7

    goto/16 :goto_0

    .line 1069
    :sswitch_2b
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    iget-boolean v2, v2, Lcom/fmm/dm/db/file/XDBResyncModeInfo;->nNoceResyncMode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 1071
    :sswitch_2c
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    goto/16 :goto_0

    .line 890
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 971
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x32 -> :sswitch_f
        0x33 -> :sswitch_10
        0x34 -> :sswitch_11
        0x35 -> :sswitch_12
        0x36 -> :sswitch_13
        0x37 -> :sswitch_14
        0x38 -> :sswitch_15
        0x39 -> :sswitch_16
        0x3a -> :sswitch_17
        0x3b -> :sswitch_18
        0x3c -> :sswitch_19
        0x3d -> :sswitch_1a
        0x3e -> :sswitch_1b
        0x3f -> :sswitch_1c
        0x40 -> :sswitch_1d
        0x41 -> :sswitch_1e
        0x42 -> :sswitch_1f
        0x43 -> :sswitch_20
        0x44 -> :sswitch_21
        0x45 -> :sswitch_22
        0x46 -> :sswitch_23
        0x47 -> :sswitch_24
        0x48 -> :sswitch_25
        0x49 -> :sswitch_26
        0x4a -> :sswitch_27
        0x64 -> :sswitch_28
        0x65 -> :sswitch_29
        0x66 -> :sswitch_2a
        0x78 -> :sswitch_2c
        0x96 -> :sswitch_2b
    .end sparse-switch
.end method

.method public static xdbRead(II)Ljava/lang/Object;
    .locals 5
    .param p0, "nType"    # I
    .param p1, "rowId"    # I

    .prologue
    const/4 v3, 0x0

    .line 1080
    const/4 v1, 0x0

    .line 1082
    .local v1, "rtnObj":Ljava/lang/Object;
    sparse-switch p0, :sswitch_data_0

    .line 1138
    const-string v2, "Not Support file id----"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1141
    .end local v1    # "rtnObj":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 1085
    .restart local v1    # "rtnObj":Ljava/lang/Object;
    :sswitch_0
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x51

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 1086
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    if-nez v2, :cond_0

    move-object v1, v3

    .line 1087
    goto :goto_0

    .line 1089
    :cond_0
    const/4 v0, 0x0

    .line 1090
    .local v0, "FileID":I
    packed-switch p1, :pswitch_data_0

    .line 1104
    :goto_1
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBProfileInfo;

    iput-object v2, v3, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    goto :goto_0

    .line 1093
    :pswitch_0
    const/16 v0, 0x52

    .line 1094
    goto :goto_1

    .line 1096
    :pswitch_1
    const/16 v0, 0x53

    .line 1097
    goto :goto_1

    .line 1099
    :pswitch_2
    const/16 v0, 0x54

    .line 1100
    goto :goto_1

    .line 1110
    .end local v0    # "FileID":I
    :sswitch_1
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x58

    invoke-static {v2, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 1111
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    if-nez v2, :cond_1

    move-object v1, v3

    .line 1112
    goto :goto_0

    .line 1114
    :cond_1
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbLawmoDbRead(I)Ljava/lang/Object;

    move-result-object v1

    .line 1116
    goto :goto_0

    .line 1121
    :sswitch_2
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x59

    invoke-static {v2, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBAMTInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    .line 1122
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    if-nez v2, :cond_2

    move-object v1, v3

    .line 1123
    goto :goto_0

    .line 1125
    :cond_2
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbAmtDbRead(I)Ljava/lang/Object;

    move-result-object v1

    .line 1127
    goto :goto_0

    .line 1130
    :sswitch_3
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x60

    invoke-static {v2, p1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBNotiInfo;

    iput-object v2, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    .line 1131
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    if-nez v2, :cond_3

    move-object v1, v3

    .line 1132
    goto :goto_0

    .line 1134
    :cond_3
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbNotiDbRead(I)Ljava/lang/Object;

    move-result-object v1

    .line 1135
    goto :goto_0

    .line 1082
    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xfa -> :sswitch_3
        0x12c -> :sswitch_2
    .end sparse-switch

    .line 1090
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdbReadFile(III)Ljava/lang/Object;
    .locals 4
    .param p0, "FileID"    # I
    .param p1, "nOffset"    # I
    .param p2, "nSize"    # I

    .prologue
    .line 2182
    const/4 v2, 0x0

    .line 2183
    .local v2, "szFileName":Ljava/lang/String;
    new-array v0, p2, [B

    .line 2186
    .local v0, "Input":[B
    :try_start_0
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v2

    .line 2187
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    invoke-virtual {v3, v2, v0, p1, p2}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileRead(Ljava/lang/String;[BII)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2193
    :goto_0
    return-object v0

    .line 2189
    :catch_0
    move-exception v1

    .line 2191
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbReadFile(III[B)Z
    .locals 4
    .param p0, "FileID"    # I
    .param p1, "nOffset"    # I
    .param p2, "nSize"    # I
    .param p3, "pData"    # [B

    .prologue
    .line 2165
    const/4 v2, 0x0

    .line 2166
    .local v2, "szFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2170
    .local v0, "bRet":Z
    :try_start_0
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v2

    .line 2171
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->dbadapter:Lcom/fmm/dm/db/file/XDBAdapter;

    invoke-virtual {v3, v2, p3, p1, p2}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileRead(Ljava/lang/String;[BII)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2177
    :goto_0
    return v0

    .line 2173
    :catch_0
    move-exception v1

    .line 2175
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbReadListInfo(I)Ljava/lang/Object;
    .locals 4
    .param p0, "nType"    # I

    .prologue
    .line 245
    const/4 v1, 0x0

    .line 247
    .local v1, "outPut":Ljava/lang/Object;
    packed-switch p0, :pswitch_data_0

    .line 268
    .end local v1    # "outPut":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 250
    .restart local v1    # "outPut":Ljava/lang/Object;
    :pswitch_0
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    const/16 v2, 0x51

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput-object v2, v3, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 251
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v2, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    goto :goto_0

    .line 254
    :pswitch_1
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->NetProfileClass:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    const-string v3, "NetWork Info"

    iput-object v3, v2, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->m_szConRefName:Ljava/lang/String;

    .line 255
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->NetProfileClass:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    const/16 v2, 0x80

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-object v2, v3, Lcom/fmm/dm/db/file/XDBNetworkProfileList;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    .line 256
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->NetProfileClass:Lcom/fmm/dm/db/file/XDBNetworkProfileList;

    goto :goto_0

    .line 259
    :pswitch_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    .line 261
    sget-object v3, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    add-int/lit8 v2, v0, 0x52

    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/dm/db/file/XDBProfileInfo;

    aput-object v2, v3, v0

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 263
    :cond_0
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->ProfileInfoClass:[Lcom/fmm/dm/db/file/XDBProfileInfo;

    goto :goto_0

    .line 247
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdbSetActiveProfileIndexByServerID(Ljava/lang/String;)I
    .locals 6
    .param p0, "szInputServerId"    # Ljava/lang/String;

    .prologue
    .line 3007
    const/4 v4, 0x0

    .line 3008
    .local v4, "szServerId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 3012
    .local v2, "nProfileIdx":I
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProflieIdx()I

    move-result v2

    .line 3014
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3016
    const-string v5, "ServerID is NULL"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 3041
    .end local v2    # "nProfileIdx":I
    .local v3, "nProfileIdx":I
    :goto_0
    return v3

    .line 3020
    .end local v3    # "nProfileIdx":I
    .restart local v2    # "nProfileIdx":I
    :cond_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerID()Ljava/lang/String;

    move-result-object v4

    .line 3021
    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v3, v2

    .line 3023
    .end local v2    # "nProfileIdx":I
    .restart local v3    # "nProfileIdx":I
    goto :goto_0

    .line 3026
    .end local v3    # "nProfileIdx":I
    .restart local v2    # "nProfileIdx":I
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v5, 0x3

    if-ge v1, v5, :cond_2

    .line 3028
    add-int/lit8 v5, v1, 0x52

    invoke-static {v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 3030
    .local v0, "dmInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    if-eqz v0, :cond_3

    .line 3032
    iget-object v5, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3034
    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetProfileIndex(I)V

    .line 3035
    move v2, v1

    .end local v0    # "dmInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    :cond_2
    move v3, v2

    .line 3041
    .end local v2    # "nProfileIdx":I
    .restart local v3    # "nProfileIdx":I
    goto :goto_0

    .line 3026
    .end local v3    # "nProfileIdx":I
    .restart local v0    # "dmInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    .restart local v2    # "nProfileIdx":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static xdbSetAuthType(I)V
    .locals 3
    .param p0, "authType"    # I

    .prologue
    .line 2303
    const/16 v1, 0x44

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2309
    :goto_0
    return-void

    .line 2305
    :catch_0
    move-exception v0

    .line 2307
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetBackUpServerUrl()V
    .locals 2

    .prologue
    .line 3167
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3169
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetProfileInfo()Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-result-object v0

    .line 3170
    .local v0, "pNvInfo":Lcom/fmm/dm/db/file/XDBProfileInfo;
    if-nez v0, :cond_0

    .line 3172
    const-string v1, "pNvInfo is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3180
    :goto_0
    return-void

    .line 3175
    :cond_0
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerUrl(Ljava/lang/String;)V

    .line 3176
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerAddress(Ljava/lang/String;)V

    .line 3177
    iget v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort_Org:I

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerPort(I)V

    .line 3178
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerProtocol(Ljava/lang/String;)V

    .line 3179
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/db/file/XDB;->xdbSetChangedProtocol(Z)V

    goto :goto_0
.end method

.method public static xdbSetChangedProtocol(Z)V
    .locals 3
    .param p0, "bChanged"    # Z

    .prologue
    .line 2325
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "xdbSetChangedProtocol : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2329
    const/16 v1, 0x4a

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2335
    :goto_0
    return-void

    .line 2331
    :catch_0
    move-exception v0

    .line 2333
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetClientNonce(Ljava/lang/String;)V
    .locals 2
    .param p0, "szNonce"    # Ljava/lang/String;

    .prologue
    .line 2632
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2645
    :goto_0
    return-void

    .line 2639
    :cond_0
    const/16 v1, 0x41

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2641
    :catch_0
    move-exception v0

    .line 2643
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetConBack(Lcom/fmm/dm/db/file/XDBNetConProfileBackup;)V
    .locals 2
    .param p0, "ptConBack"    # Lcom/fmm/dm/db/file/XDBNetConProfileBackup;

    .prologue
    .line 2921
    const/16 v1, 0x49

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2927
    :goto_0
    return-void

    .line 2923
    :catch_0
    move-exception v0

    .line 2925
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetConRef(Lcom/fmm/dm/db/file/XDBInfoConRef;)V
    .locals 2
    .param p0, "ptConRef"    # Lcom/fmm/dm/db/file/XDBInfoConRef;

    .prologue
    .line 2895
    const/16 v1, 0x48

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2901
    :goto_0
    return-void

    .line 2897
    :catch_0
    move-exception v0

    .line 2899
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetDMXNodeInfo(ILjava/lang/Object;)V
    .locals 2
    .param p0, "nIndex"    # I
    .param p1, "ptAccXNodeInfo"    # Ljava/lang/Object;

    .prologue
    .line 2437
    :try_start_0
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdb_E2P_XDM_ACCXNODE_INFO(I)I

    move-result v1

    invoke-static {v1, p1}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2443
    :goto_0
    return-void

    .line 2439
    :catch_0
    move-exception v0

    .line 2441
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetDmAgentType(I)V
    .locals 3
    .param p0, "nAgentType"    # I

    .prologue
    .line 3281
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AgentType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3284
    const/16 v1, 0x6f

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3290
    :goto_0
    return-void

    .line 3286
    :catch_0
    move-exception v0

    .line 3288
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNetworkIdx(I)V
    .locals 3
    .param p0, "nIdx"    # I

    .prologue
    .line 2945
    const/16 v1, 0x46

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2951
    :goto_0
    return-void

    .line 2947
    :catch_0
    move-exception v0

    .line 2949
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNetworkName(Ljava/lang/String;)V
    .locals 2
    .param p0, "szName"    # Ljava/lang/String;

    .prologue
    .line 2933
    const/4 v1, 0x7

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2939
    :goto_0
    return-void

    .line 2935
    :catch_0
    move-exception v0

    .line 2937
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNetworkProflieName(Ljava/lang/String;)V
    .locals 2
    .param p0, "szProflieName"    # Ljava/lang/String;

    .prologue
    .line 2479
    const/4 v1, 0x7

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2485
    :goto_0
    return-void

    .line 2481
    :catch_0
    move-exception v0

    .line 2483
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNonceResync(I)V
    .locals 3
    .param p0, "bResyncMode"    # I

    .prologue
    .line 2467
    const/16 v1, 0x96

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2473
    :goto_0
    return-void

    .line 2469
    :catch_0
    move-exception v0

    .line 2471
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNotiEvent(I)V
    .locals 3
    .param p0, "nEvent"    # I

    .prologue
    .line 2851
    const/16 v1, 0xa

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2857
    :goto_0
    return-void

    .line 2853
    :catch_0
    move-exception v0

    .line 2855
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNotiReSyncMode(I)Ljava/lang/Boolean;
    .locals 3
    .param p0, "nMode"    # I

    .prologue
    .line 3127
    const/16 v1, 0xc

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3133
    :goto_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    .line 3129
    :catch_0
    move-exception v0

    .line 3131
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNotiReSyncMode(Z)V
    .locals 3
    .param p0, "nMode"    # Z

    .prologue
    .line 3140
    const/16 v1, 0xc

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3146
    :goto_0
    return-void

    .line 3142
    :catch_0
    move-exception v0

    .line 3144
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNotiSessionID(Ljava/lang/String;)V
    .locals 2
    .param p0, "szSessionID"    # Ljava/lang/String;

    .prologue
    .line 2595
    const/16 v1, 0x8

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2601
    :goto_0
    return-void

    .line 2597
    :catch_0
    move-exception v0

    .line 2599
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProfileIndex(I)V
    .locals 5
    .param p0, "idx"    # I

    .prologue
    .line 2261
    const/4 v1, 0x2

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2268
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set Profile Index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2269
    return-void

    .line 2263
    :catch_0
    move-exception v0

    .line 2265
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "unable to write index\n%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProfileInfo(Lcom/fmm/dm/db/file/XDBProfileInfo;)Z
    .locals 3
    .param p0, "ptProfileInfo"    # Lcom/fmm/dm/db/file/XDBProfileInfo;

    .prologue
    .line 2367
    const/4 v1, 0x0

    .line 2370
    .local v1, "nRet":Z
    const/16 v2, 0x32

    :try_start_0
    invoke-static {v2, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2376
    :goto_0
    return v1

    .line 2372
    :catch_0
    move-exception v0

    .line 2374
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProfileInfo(Lcom/fmm/dm/db/file/XDBProfileInfo;I)Z
    .locals 3
    .param p0, "ptProfileInfo"    # Lcom/fmm/dm/db/file/XDBProfileInfo;
    .param p1, "RowId"    # I

    .prologue
    .line 2381
    const/4 v1, 0x0

    .line 2384
    .local v1, "nRet":Z
    const/16 v2, 0x32

    :try_start_0
    invoke-static {v2, p1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(IILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2390
    :goto_0
    return v1

    .line 2386
    :catch_0
    move-exception v0

    .line 2388
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProfileName(ILjava/lang/String;)V
    .locals 2
    .param p0, "nIdx"    # I
    .param p1, "szProflieName"    # Ljava/lang/String;

    .prologue
    .line 2411
    :try_start_0
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdb_E2P_XDM_PROFILENAME(I)I

    move-result v1

    invoke-static {v1, p1}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2417
    :goto_0
    return-void

    .line 2413
    :catch_0
    move-exception v0

    .line 2415
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProflieList(Ljava/lang/Object;)V
    .locals 2
    .param p0, "ptProflieList"    # Ljava/lang/Object;

    .prologue
    .line 2504
    const/4 v1, 0x6

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2510
    :goto_0
    return-void

    .line 2506
    :catch_0
    move-exception v0

    .line 2508
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerAddress(Ljava/lang/String;)V
    .locals 2
    .param p0, "szAddress"    # Ljava/lang/String;

    .prologue
    .line 2545
    const/16 v1, 0x39

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2551
    :goto_0
    return-void

    .line 2547
    :catch_0
    move-exception v0

    .line 2549
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerAuthType(I)V
    .locals 3
    .param p0, "authType"    # I

    .prologue
    .line 2315
    const/16 v1, 0x45

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2321
    :goto_0
    return-void

    .line 2317
    :catch_0
    move-exception v0

    .line 2319
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerNonce(Ljava/lang/String;)V
    .locals 2
    .param p0, "szNonce"    # Ljava/lang/String;

    .prologue
    .line 2622
    const/16 v1, 0x42

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2628
    :goto_0
    return-void

    .line 2624
    :catch_0
    move-exception v0

    .line 2626
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerPort(I)V
    .locals 3
    .param p0, "nPort"    # I

    .prologue
    .line 2557
    const/16 v1, 0x38

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2563
    :goto_0
    return-void

    .line 2559
    :catch_0
    move-exception v0

    .line 2561
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerProtocol(Ljava/lang/String;)V
    .locals 2
    .param p0, "szProtocol"    # Ljava/lang/String;

    .prologue
    .line 2569
    const/16 v1, 0x34

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2575
    :goto_0
    return-void

    .line 2571
    :catch_0
    move-exception v0

    .line 2573
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerUrl(Ljava/lang/String;)V
    .locals 2
    .param p0, "szServerUrl"    # Ljava/lang/String;

    .prologue
    .line 2533
    const/16 v1, 0x43

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2539
    :goto_0
    return-void

    .line 2535
    :catch_0
    move-exception v0

    .line 2537
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetSessionSaveStatus(III)Z
    .locals 5
    .param p0, "nState"    # I
    .param p1, "nNotiUiEvent"    # I
    .param p2, "nNotiRetryCount"    # I

    .prologue
    .line 3078
    new-instance v2, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;-><init>()V

    .line 3079
    .local v2, "pSessionSave":Lcom/fmm/dm/db/file/XDBSessionSaveInfo;
    const/4 v0, 0x1

    .line 3080
    .local v0, "bResult":Z
    const-string v3, ""

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3081
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nSessionSaveState ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], nNotiUiEvent ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], nNotiRetrycount ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3083
    iput p0, v2, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nSessionSaveState:I

    .line 3084
    iput p1, v2, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiUiEvent:I

    .line 3085
    iput p2, v2, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;->nNotiRetryCount:I

    .line 3089
    const/16 v3, 0xb

    :try_start_0
    invoke-static {v3, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3096
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "return value :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3097
    return v0

    .line 3091
    :catch_0
    move-exception v1

    .line 3093
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3094
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdbSetSettingContentProviderDB(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "szUrl"    # Ljava/lang/String;
    .param p1, "bCheck"    # Z

    .prologue
    .line 3368
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    .line 3371
    .local v1, "nCheck":I
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, p0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3377
    :goto_1
    return-void

    .line 3368
    .end local v1    # "nCheck":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 3373
    .restart local v1    # "nCheck":I
    :catch_0
    move-exception v0

    .line 3375
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbSetSimIMSI(Lcom/fmm/dm/db/file/XDBSimInfo;)V
    .locals 2
    .param p0, "pIMSI"    # Lcom/fmm/dm/db/file/XDBSimInfo;

    .prologue
    .line 3198
    if-nez p0, :cond_0

    .line 3200
    const-string v1, "pIMSI is NULL"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3213
    :goto_0
    return-void

    .line 3206
    :cond_0
    const/16 v1, 0x78

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3208
    :catch_0
    move-exception v0

    .line 3210
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetSyncMLNVMUser(Lcom/fmm/dm/db/file/XDBFileNVMParam;)Lcom/fmm/dm/db/file/XDBFileNVMParam;
    .locals 2
    .param p0, "pfileparam"    # Lcom/fmm/dm/db/file/XDBFileNVMParam;

    .prologue
    .line 1464
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x51

    if-ne v0, v1, :cond_0

    .line 1466
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    .line 1507
    :goto_0
    return-object p0

    .line 1468
    :cond_0
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x52

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x54

    if-gt v0, v1, :cond_1

    .line 1470
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1472
    :cond_1
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x55

    if-ne v0, v1, :cond_2

    .line 1474
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1476
    :cond_2
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x83

    if-ne v0, v1, :cond_3

    .line 1479
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1481
    :cond_3
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x56

    if-ne v0, v1, :cond_4

    .line 1484
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1486
    :cond_4
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x57

    if-ne v0, v1, :cond_5

    .line 1488
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1490
    :cond_5
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x58

    if-ne v0, v1, :cond_6

    .line 1492
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1494
    :cond_6
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x59

    if-ne v0, v1, :cond_7

    .line 1496
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1498
    :cond_7
    iget v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x60

    if-ne v0, v1, :cond_8

    .line 1500
    sget-object v0, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1504
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not Support Area Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static xdbSyncMLFileParmInit([Lcom/fmm/dm/db/file/XDBFileNVMParam;I)[Lcom/fmm/dm/db/file/XDBFileNVMParam;
    .locals 5
    .param p0, "xdmSyncMLFileParam2"    # [Lcom/fmm/dm/db/file/XDBFileNVMParam;
    .param p1, "MaxIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 131
    const/4 v0, 0x0

    .line 132
    .local v0, "Areacodeindex":[I
    new-array v0, p1, [I

    .line 135
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_DM_PROFILE:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x51

    aput v3, v0, v2

    .line 136
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_DM_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x52

    aput v3, v0, v2

    .line 137
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_IMSI_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x55

    aput v3, v0, v2

    .line 141
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_DM_ACC_X_NODE:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x83

    aput v3, v0, v2

    .line 142
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_NOTI_RESYNC_MODE:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x56

    aput v3, v0, v2

    .line 145
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_DM_AGENT_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x57

    aput v3, v0, v2

    .line 149
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_LAWMO_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x58

    aput v3, v0, v2

    .line 154
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_AMT_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x59

    aput v3, v0, v2

    .line 157
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->NVM_NOTI_INFO:Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x60

    aput v3, v0, v2

    .line 159
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 161
    new-instance v2, Lcom/fmm/dm/db/file/XDBFileNVMParam;

    invoke-direct {v2}, Lcom/fmm/dm/db/file/XDBFileNVMParam;-><init>()V

    aput-object v2, p0, v1

    .line 162
    aget-object v2, p0, v1

    aget v3, v0, v1

    iput v3, v2, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    .line 163
    aget-object v2, p0, v1

    iput v4, v2, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pExtFileID:I

    .line 164
    aget-object v2, p0, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/dm/db/file/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 167
    :cond_0
    return-object p0
.end method

.method public static xdbWrite(IILjava/lang/Object;)V
    .locals 3
    .param p0, "nType"    # I
    .param p1, "rowId"    # I
    .param p2, "oInput"    # Ljava/lang/Object;

    .prologue
    .line 1406
    if-nez p2, :cond_0

    .line 1408
    const-string v1, "oInput is null"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1460
    .end local p2    # "oInput":Ljava/lang/Object;
    :goto_0
    return-void

    .line 1412
    .restart local p2    # "oInput":Ljava/lang/Object;
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 1457
    const-string v1, "Not Support file id----"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1415
    :sswitch_0
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    check-cast p2, Lcom/fmm/dm/db/file/XDBProfileInfo;

    .end local p2    # "oInput":Ljava/lang/Object;
    iput-object p2, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    .line 1416
    const/4 v0, 0x0

    .line 1418
    .local v0, "SqlID":I
    packed-switch p1, :pswitch_data_0

    .line 1432
    :goto_1
    sget-object v1, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v1, v1, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-static {v0, v1}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto :goto_0

    .line 1421
    :pswitch_0
    const/16 v0, 0x52

    .line 1422
    goto :goto_1

    .line 1424
    :pswitch_1
    const/16 v0, 0x53

    .line 1425
    goto :goto_1

    .line 1427
    :pswitch_2
    const/16 v0, 0x54

    .line 1428
    goto :goto_1

    .line 1438
    .end local v0    # "SqlID":I
    .restart local p2    # "oInput":Ljava/lang/Object;
    :sswitch_1
    invoke-static {p0, p2}, Lcom/fmm/dm/db/file/XDB;->xdbLawmoDbWrite(ILjava/lang/Object;)V

    .line 1439
    const/16 v1, 0x58

    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v1, p1, v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(IILjava/lang/Object;)Z

    goto :goto_0

    .line 1446
    :sswitch_2
    invoke-static {p0, p2}, Lcom/fmm/dm/db/file/XDB;->xdbAmtDbWrite(ILjava/lang/Object;)V

    .line 1447
    const/16 v1, 0x59

    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {v1, p1, v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(IILjava/lang/Object;)Z

    goto :goto_0

    .line 1452
    :sswitch_3
    invoke-static {p0, p2}, Lcom/fmm/dm/db/file/XDB;->xdbNotiDbWrite(ILjava/lang/Object;)V

    .line 1453
    const/16 v1, 0x60

    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-static {v1, p1, v2}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(IILjava/lang/Object;)Z

    goto :goto_0

    .line 1412
    nop

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xfa -> :sswitch_3
        0x12c -> :sswitch_2
    .end sparse-switch

    .line 1418
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdbWrite(ILjava/lang/Object;)V
    .locals 10
    .param p0, "nType"    # I
    .param p1, "oInput"    # Ljava/lang/Object;

    .prologue
    const/16 v9, 0x51

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 1146
    if-nez p1, :cond_1

    .line 1148
    const-string v4, "oInput is null"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1402
    :cond_0
    :goto_0
    return-void

    .line 1152
    :cond_1
    sparse-switch p0, :sswitch_data_0

    .line 1300
    :goto_1
    new-instance v1, Lcom/fmm/dm/db/file/XDBFileNVMParam;

    invoke-direct {v1}, Lcom/fmm/dm/db/file/XDBFileNVMParam;-><init>()V

    .line 1301
    .local v1, "pfileparam":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    const-string v3, ""

    .line 1303
    .local v3, "szFileName":Ljava/lang/String;
    if-ltz p0, :cond_2

    const/16 v4, 0xf

    if-ge p0, v4, :cond_2

    .line 1305
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-static {v9, v4}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto :goto_0

    .line 1155
    .end local v1    # "pfileparam":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    .end local v3    # "szFileName":Ljava/lang/String;
    :sswitch_0
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->MagicNumber:I

    goto :goto_1

    .line 1158
    :sswitch_1
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nProxyIndex:I

    goto :goto_1

    .line 1161
    :sswitch_2
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    goto :goto_1

    .line 1164
    :sswitch_3
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    goto :goto_1

    .line 1167
    :sswitch_4
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    goto :goto_1

    .line 1170
    :sswitch_5
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    goto :goto_1

    .line 1173
    :sswitch_6
    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput-object v4, v5, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    goto :goto_1

    .line 1176
    :sswitch_7
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szNetworkConnName:Ljava/lang/String;

    goto/16 :goto_1

    .line 1179
    :sswitch_8
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->m_szSessionID:Ljava/lang/String;

    goto/16 :goto_1

    .line 1182
    :sswitch_9
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nDestoryNotiTime:J

    goto/16 :goto_1

    .line 1185
    :sswitch_a
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiEvent:I

    goto/16 :goto_1

    .line 1188
    :sswitch_b
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    iput-object v4, v5, Lcom/fmm/dm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/fmm/dm/db/file/XDBSessionSaveInfo;

    goto/16 :goto_1

    .line 1192
    :sswitch_c
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->nNotiReSyncMode:I

    goto/16 :goto_1

    .line 1195
    :sswitch_d
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    iput-object v4, v5, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    goto/16 :goto_1

    .line 1198
    :sswitch_e
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBUICResultKeepInfo;->eStatus:I

    goto/16 :goto_1

    .line 1201
    :sswitch_f
    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBProfileInfo;

    iput-object v4, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    goto/16 :goto_1

    .line 1204
    :sswitch_10
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->MagicNumber:I

    goto/16 :goto_1

    .line 1207
    :sswitch_11
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    goto/16 :goto_1

    .line 1210
    :sswitch_12
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ObexType:I

    goto/16 :goto_1

    .line 1213
    :sswitch_13
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    goto/16 :goto_1

    .line 1216
    :sswitch_14
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    goto/16 :goto_1

    .line 1219
    :sswitch_15
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPort:I

    goto/16 :goto_1

    .line 1222
    :sswitch_16
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    goto/16 :goto_1

    .line 1225
    :sswitch_17
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->Path:Ljava/lang/String;

    goto/16 :goto_1

    .line 1229
    :sswitch_18
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->AppID:Ljava/lang/String;

    goto/16 :goto_1

    .line 1232
    :sswitch_19
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    goto/16 :goto_1

    .line 1235
    :sswitch_1a
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    goto/16 :goto_1

    .line 1238
    :sswitch_1b
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    goto/16 :goto_1

    .line 1241
    :sswitch_1c
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    goto/16 :goto_1

    .line 1244
    :sswitch_1d
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    goto/16 :goto_1

    .line 1247
    :sswitch_1e
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    goto/16 :goto_1

    .line 1250
    :sswitch_1f
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    goto/16 :goto_1

    .line 1253
    :sswitch_20
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    goto/16 :goto_1

    .line 1256
    :sswitch_21
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->AuthType:I

    goto/16 :goto_1

    .line 1259
    :sswitch_22
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->nServerAuthType:I

    goto/16 :goto_1

    .line 1262
    :sswitch_23
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->nNetworkConnIndex:I

    goto/16 :goto_1

    .line 1265
    :sswitch_24
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    goto/16 :goto_1

    .line 1268
    :sswitch_25
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBInfoConRef;

    iput-object v4, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    goto/16 :goto_1

    .line 1271
    :sswitch_26
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBNetConProfileBackup;

    iput-object v4, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConBackup:Lcom/fmm/dm/db/file/XDBNetConProfileBackup;

    goto/16 :goto_1

    .line 1274
    :sswitch_27
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iput-boolean v5, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->bChangedProtocol:Z

    goto/16 :goto_1

    .line 1279
    :sswitch_28
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v5, v4, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aput-object v4, v5, v8

    goto/16 :goto_1

    .line 1283
    :sswitch_29
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v5, v4, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aput-object v4, v5, v6

    goto/16 :goto_1

    .line 1287
    :sswitch_2a
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    iget-object v5, v4, Lcom/fmm/dm/db/file/XDBAccXListNode;->stAccXNodeList:[Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBAccXNodeInfo;

    aput-object v4, v5, v7

    goto/16 :goto_1

    .line 1291
    :sswitch_2b
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iput-boolean v5, v4, Lcom/fmm/dm/db/file/XDBResyncModeInfo;->nNoceResyncMode:Z

    goto/16 :goto_1

    .line 1294
    :sswitch_2c
    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    move-object v4, p1

    check-cast v4, Lcom/fmm/dm/db/file/XDBSimInfo;

    iput-object v4, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    goto/16 :goto_1

    .line 1307
    .restart local v1    # "pfileparam":Lcom/fmm/dm/db/file/XDBFileNVMParam;
    .restart local v3    # "szFileName":Ljava/lang/String;
    :cond_2
    const/16 v4, 0x48

    if-ne p0, v4, :cond_4

    .line 1309
    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbExistsNetworkRow(J)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1311
    const/16 v4, 0x80

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    .line 1313
    invoke-static {}, Lcom/fmm/dm/ui/XUINetProfileActivity;->getUpdateState()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1315
    sget-object v4, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBConRefNAP;->Addr:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->szAccountName:Ljava/lang/String;

    .line 1316
    sget-object v4, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szApn:Ljava/lang/String;

    .line 1317
    sget-object v4, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/dm/agent/XDMAppConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    .line 1318
    sget-object v4, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->napAddr:Lcom/fmm/dm/agent/XDMAppConnectSetting;

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v5, v5, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    iput v5, v4, Lcom/fmm/dm/agent/XDMAppConnectSetting;->nPrimary_proxy_port:I

    .line 1319
    sget-object v4, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szId:Ljava/lang/String;

    .line 1320
    sget-object v4, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    iget-object v4, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->authInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBConRefNAP;->Auth:Lcom/fmm/dm/db/file/XDBConRefAuth;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/dm/agent/XDMAppProtoNetInfo$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    .line 1321
    sget-object v4, Lcom/fmm/dm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    invoke-static {v4}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpNetSaveProfile(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)I

    .line 1322
    invoke-static {v8}, Lcom/fmm/dm/ui/XUINetProfileActivity;->setUpdateState(I)V

    goto/16 :goto_0

    .line 1327
    :cond_3
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBProfileInfo;->ConRef:Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-static {v4}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbInsertNetworkRow(Lcom/fmm/dm/db/file/XDBInfoConRef;)V

    goto/16 :goto_0

    .line 1331
    :cond_4
    const/16 v4, 0x32

    if-lt p0, v4, :cond_7

    const/16 v4, 0x4b

    if-ge p0, v4, :cond_7

    .line 1333
    const/4 v0, 0x0

    .line 1334
    .local v0, "SqlID":I
    const/4 v2, 0x0

    .line 1336
    .local v2, "row":I
    invoke-static {}, Lcom/fmm/dm/ui/XUIProfileActivity;->xuiGetRowState()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1338
    invoke-static {}, Lcom/fmm/dm/ui/XUIProfileActivity;->xuiGetRow()I

    move-result v2

    .line 1352
    :goto_2
    packed-switch v2, :pswitch_data_0

    .line 1366
    :goto_3
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/fmm/dm/db/file/XDBProfileInfo;

    invoke-static {v0, v4}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1342
    :cond_5
    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    invoke-static {v9}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iput-object v4, v5, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    .line 1343
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    if-eqz v4, :cond_6

    .line 1345
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->tProfileList:Lcom/fmm/dm/db/file/XDBProflieListInfo;

    iget v2, v4, Lcom/fmm/dm/db/file/XDBProflieListInfo;->Profileindex:I

    goto :goto_2

    .line 1349
    :cond_6
    const-string v4, "XDMNvmClass.tProfileList is null"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_2

    .line 1355
    :pswitch_0
    const/16 v0, 0x52

    .line 1356
    goto :goto_3

    .line 1358
    :pswitch_1
    const/16 v0, 0x53

    .line 1359
    goto :goto_3

    .line 1361
    :pswitch_2
    const/16 v0, 0x54

    .line 1362
    goto :goto_3

    .line 1368
    .end local v0    # "SqlID":I
    .end local v2    # "row":I
    :cond_7
    const/16 v4, 0x64

    if-lt p0, v4, :cond_8

    const/16 v4, 0x67

    if-ge p0, v4, :cond_8

    .line 1370
    const/16 v4, 0x83

    iput v4, v1, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    .line 1371
    iget v4, v1, Lcom/fmm/dm/db/file/XDBFileNVMParam;->AreaCode:I

    invoke-static {v4}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v3

    .line 1372
    sget-object v4, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v4, v4, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAccXNode:Lcom/fmm/dm/db/file/XDBAccXListNode;

    invoke-static {v3, v4}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileWrite(Ljava/lang/String;Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1374
    :cond_8
    const/16 v4, 0x96

    if-lt p0, v4, :cond_9

    const/16 v4, 0x97

    if-ge p0, v4, :cond_9

    .line 1376
    const/16 v4, 0x56

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLResyncMode:Lcom/fmm/dm/db/file/XDBResyncModeInfo;

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1378
    :cond_9
    const/16 v4, 0x78

    if-lt p0, v4, :cond_a

    const/16 v4, 0x79

    if-ge p0, v4, :cond_a

    .line 1380
    const/16 v4, 0x55

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSYNCMLSimInfo:Lcom/fmm/dm/db/file/XDBSimInfo;

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1382
    :cond_a
    const/16 v4, 0x6e

    if-lt p0, v4, :cond_b

    const/16 v4, 0x70

    if-ge p0, v4, :cond_b

    .line 1384
    invoke-static {p0, p1}, Lcom/fmm/dm/db/file/XDB;->xdbAgentInfoDbWrite(ILjava/lang/Object;)V

    .line 1385
    const/16 v4, 0x57

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLDmAgentInfo:Lcom/fmm/dm/db/file/XDBAgentInfo;

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1387
    :cond_b
    const/16 v4, 0xc8

    if-lt p0, v4, :cond_c

    const/16 v4, 0xe1

    if-ge p0, v4, :cond_c

    .line 1389
    invoke-static {p0, p1}, Lcom/fmm/dm/db/file/XDB;->xdbLawmoDbWrite(ILjava/lang/Object;)V

    .line 1390
    const/16 v4, 0x58

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLLawmoInfo:Lcom/fmm/dm/db/file/XDBLawmoInfo;

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1392
    :cond_c
    const/16 v4, 0x12c

    if-lt p0, v4, :cond_d

    const/16 v4, 0x144

    if-ge p0, v4, :cond_d

    .line 1394
    invoke-static {p0, p1}, Lcom/fmm/dm/db/file/XDB;->xdbAmtDbWrite(ILjava/lang/Object;)V

    .line 1395
    const/16 v4, 0x59

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLAMTInfo:Lcom/fmm/dm/db/file/XDBAMTInfo;

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1397
    :cond_d
    const/16 v4, 0xfa

    if-lt p0, v4, :cond_0

    const/16 v4, 0xff

    if-ge p0, v4, :cond_0

    .line 1399
    invoke-static {p0, p1}, Lcom/fmm/dm/db/file/XDB;->xdbNotiDbWrite(ILjava/lang/Object;)V

    .line 1400
    const/16 v4, 0x60

    sget-object v5, Lcom/fmm/dm/db/file/XDB;->XDMNvmClass:Lcom/fmm/dm/db/file/XDBNvm;

    iget-object v5, v5, Lcom/fmm/dm/db/file/XDBNvm;->NVMSyncMLNotiInfo:Lcom/fmm/dm/db/file/XDBNotiInfo;

    invoke-static {v4, v5}, Lcom/fmm/dm/db/sql/XDMDbSqlQuery;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1152
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x32 -> :sswitch_f
        0x33 -> :sswitch_10
        0x34 -> :sswitch_11
        0x35 -> :sswitch_12
        0x36 -> :sswitch_13
        0x37 -> :sswitch_14
        0x38 -> :sswitch_15
        0x39 -> :sswitch_16
        0x3a -> :sswitch_17
        0x3b -> :sswitch_18
        0x3c -> :sswitch_19
        0x3d -> :sswitch_1a
        0x3e -> :sswitch_1b
        0x3f -> :sswitch_1c
        0x40 -> :sswitch_1d
        0x41 -> :sswitch_1e
        0x42 -> :sswitch_1f
        0x43 -> :sswitch_20
        0x44 -> :sswitch_21
        0x45 -> :sswitch_22
        0x46 -> :sswitch_23
        0x47 -> :sswitch_24
        0x48 -> :sswitch_25
        0x49 -> :sswitch_26
        0x4a -> :sswitch_27
        0x64 -> :sswitch_28
        0x65 -> :sswitch_29
        0x66 -> :sswitch_2a
        0x78 -> :sswitch_2c
        0x96 -> :sswitch_2b
    .end sparse-switch

    .line 1352
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdbWriteFile(IILjava/lang/Object;)Z
    .locals 2
    .param p0, "FileID"    # I
    .param p1, "nSize"    # I
    .param p2, "pData"    # Ljava/lang/Object;

    .prologue
    .line 2198
    const/4 v1, 0x0

    .line 2199
    .local v1, "szFileName":Ljava/lang/String;
    const/4 v0, 0x1

    .line 2201
    .local v0, "bRet":Z
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDB;->xdbFileGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v1

    .line 2203
    invoke-static {v1, p1, p2}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbFileWrite(Ljava/lang/String;ILjava/lang/Object;)Z

    move-result v0

    .line 2204
    return v0
.end method

.method public static xdb_E2P_XDM_ACCXNODE_INFO(I)I
    .locals 1
    .param p0, "i"    # I

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "ret":I
    packed-switch p0, :pswitch_data_0

    .line 240
    .end local v0    # "ret":I
    :goto_0
    return v0

    .line 232
    .restart local v0    # "ret":I
    :pswitch_0
    const/16 v0, 0x64

    goto :goto_0

    .line 234
    :pswitch_1
    const/16 v0, 0x65

    goto :goto_0

    .line 236
    :pswitch_2
    const/16 v0, 0x66

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdb_E2P_XDM_PROFILENAME(I)I
    .locals 1
    .param p0, "i"    # I

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 212
    .local v0, "ret":I
    packed-switch p0, :pswitch_data_0

    .line 223
    .end local v0    # "ret":I
    :goto_0
    return v0

    .line 215
    .restart local v0    # "ret":I
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 217
    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    .line 219
    :pswitch_2
    const/4 v0, 0x5

    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public xdbGetXDMFileParamFileID(I)Lcom/fmm/dm/db/file/XDBFileParam;
    .locals 3
    .param p1, "FileID"    # I

    .prologue
    .line 2210
    sget-object v2, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->FileMax:Lcom/fmm/dm/db/file/XDB$XDMFileParameter;

    invoke-virtual {v2}, Lcom/fmm/dm/db/file/XDB$XDMFileParameter;->Index()I

    move-result v0

    .line 2211
    .local v0, "IndexLen":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 2213
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/fmm/dm/db/file/XDBFileParam;->FileID:I

    if-ne v2, p1, :cond_0

    .line 2215
    sget-object v2, Lcom/fmm/dm/db/file/XDB;->XDMFileParam:[Lcom/fmm/dm/db/file/XDBFileParam;

    aget-object v2, v2, v1

    .line 2219
    :goto_1
    return-object v2

    .line 2211
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2219
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
