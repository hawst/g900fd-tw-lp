.class public Lcom/fmm/dm/db/file/XDBInfoConRef;
.super Ljava/lang/Object;
.source "XDBInfoConRef.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public Active:Z

.field public NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

.field public PX:Lcom/fmm/dm/db/file/XDBConRefPX;

.field public bProxyUse:Z

.field public m_szHomeUrl:Ljava/lang/String;

.field public nService:I

.field public tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->m_szHomeUrl:Ljava/lang/String;

    .line 20
    new-instance v0, Lcom/fmm/dm/db/file/XDBConRefNAP;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBConRefNAP;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->NAP:Lcom/fmm/dm/db/file/XDBConRefNAP;

    .line 21
    new-instance v0, Lcom/fmm/dm/db/file/XDBConRefPX;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBConRefPX;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    .line 22
    new-instance v0, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;-><init>()V

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBInfoConRef;->tAdvSetting:Lcom/fmm/dm/db/file/XDBConRefAdvanceSetting;

    .line 23
    return-void
.end method
