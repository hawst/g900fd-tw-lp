.class public Lcom/fmm/dm/db/sql/XAMTDbSqlQuery;
.super Ljava/lang/Object;
.source "XAMTDbSqlQuery.java"

# interfaces
.implements Lcom/fmm/dm/db/sql/XAMTDbSql;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xamtDbSqlCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 19
    :try_start_0
    const-string v1, "create table if not exists MobileTracking (rowid integer primary key autoincrement, GpsIsValid text, GpsLatitude text, GpsLongitude text, GpsAltitude text, GpsSpeed text, GpsCourse text, GpsHorizontalUncertainty text, GpsVerticalUncertainty text, GpsDate text, NonGpsValid text, NonGpsCellId text, NonGpsLAC text, NonGpsMNC text, NonGpsMCC text, Operation integer, ResultReportType integer, ResultReportSourceUri text, ResultReportTargetUri text, ResultCode text, PolicyStartDate text, PolicyEndDate text, PolicyInterval text, MobileTrackingStatus text);"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    :goto_0
    return-void

    .line 21
    :catch_0
    move-exception v0

    .line 23
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xamtDbSqlDeleteRow(J)V
    .locals 6
    .param p0, "RowId"    # J

    .prologue
    .line 89
    const/4 v0, 0x0

    .line 93
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 94
    const-string v2, "MobileTracking"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    if-eqz v0, :cond_0

    .line 103
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v1

    .line 98
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    if-eqz v0, :cond_0

    .line 103
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 102
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 103
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xamtDbSqlDrop(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 31
    :try_start_0
    const-string v1, "drop table if exists MobileTracking"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xamtDbSqlExistsRow(J)Z
    .locals 14
    .param p0, "RowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 240
    const/4 v10, 0x0

    .line 241
    .local v10, "Cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 242
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v12, 0x1

    .line 243
    .local v12, "nRet":Z
    const/16 v1, 0x18

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "GpsIsValid"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "GpsLatitude"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "GpsLongitude"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "GpsAltitude"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "GpsSpeed"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "GpsCourse"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string v2, "GpsHorizontalUncertainty"

    aput-object v2, v3, v1

    const/16 v1, 0x8

    const-string v2, "GpsVerticalUncertainty"

    aput-object v2, v3, v1

    const/16 v1, 0x9

    const-string v2, "GpsDate"

    aput-object v2, v3, v1

    const/16 v1, 0xa

    const-string v2, "NonGpsValid"

    aput-object v2, v3, v1

    const/16 v1, 0xb

    const-string v2, "NonGpsCellId"

    aput-object v2, v3, v1

    const/16 v1, 0xc

    const-string v2, "NonGpsLAC"

    aput-object v2, v3, v1

    const/16 v1, 0xd

    const-string v2, "NonGpsMNC"

    aput-object v2, v3, v1

    const/16 v1, 0xe

    const-string v2, "NonGpsMCC"

    aput-object v2, v3, v1

    const/16 v1, 0xf

    const-string v2, "Operation"

    aput-object v2, v3, v1

    const/16 v1, 0x10

    const-string v2, "ResultReportType"

    aput-object v2, v3, v1

    const/16 v1, 0x11

    const-string v2, "ResultReportSourceUri"

    aput-object v2, v3, v1

    const/16 v1, 0x12

    const-string v2, "ResultReportTargetUri"

    aput-object v2, v3, v1

    const/16 v1, 0x13

    const-string v2, "ResultCode"

    aput-object v2, v3, v1

    const/16 v1, 0x14

    const-string v2, "PolicyStartDate"

    aput-object v2, v3, v1

    const/16 v1, 0x15

    const-string v2, "PolicyEndDate"

    aput-object v2, v3, v1

    const/16 v1, 0x16

    const-string v2, "PolicyInterval"

    aput-object v2, v3, v1

    const/16 v1, 0x17

    const-string v2, "MobileTrackingStatus"

    aput-object v2, v3, v1

    .line 271
    .local v3, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 272
    const/4 v1, 0x1

    const-string v2, "MobileTracking"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 274
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 276
    const/4 v12, 0x1

    .line 290
    :goto_0
    if-eqz v10, :cond_0

    .line 291
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 293
    :cond_0
    if-eqz v0, :cond_1

    .line 294
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 296
    :cond_1
    :goto_1
    return v12

    .line 280
    :cond_2
    const/4 v12, 0x0

    goto :goto_0

    .line 283
    :catch_0
    move-exception v11

    .line 285
    .local v11, "e":Ljava/lang/Exception;
    const/4 v12, 0x0

    .line 286
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 290
    if-eqz v10, :cond_3

    .line 291
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 293
    :cond_3
    if-eqz v0, :cond_1

    .line 294
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 290
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 291
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 293
    :cond_4
    if-eqz v0, :cond_5

    .line 294
    invoke-static {v0}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xamtDbSqlFetchRow(JLcom/fmm/dm/db/file/XDBAMTInfo;)Ljava/lang/Object;
    .locals 16
    .param p0, "RowId"    # J
    .param p2, "MobileTrackingInfo"    # Lcom/fmm/dm/db/file/XDBAMTInfo;

    .prologue
    .line 157
    const/4 v12, 0x0

    .line 158
    .local v12, "Cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 159
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v3, 0x18

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rowid"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "GpsIsValid"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "GpsLatitude"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "GpsLongitude"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "GpsAltitude"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string v4, "GpsSpeed"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string v4, "GpsCourse"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string v4, "GpsHorizontalUncertainty"

    aput-object v4, v5, v3

    const/16 v3, 0x8

    const-string v4, "GpsVerticalUncertainty"

    aput-object v4, v5, v3

    const/16 v3, 0x9

    const-string v4, "GpsDate"

    aput-object v4, v5, v3

    const/16 v3, 0xa

    const-string v4, "NonGpsValid"

    aput-object v4, v5, v3

    const/16 v3, 0xb

    const-string v4, "NonGpsCellId"

    aput-object v4, v5, v3

    const/16 v3, 0xc

    const-string v4, "NonGpsLAC"

    aput-object v4, v5, v3

    const/16 v3, 0xd

    const-string v4, "NonGpsMNC"

    aput-object v4, v5, v3

    const/16 v3, 0xe

    const-string v4, "NonGpsMCC"

    aput-object v4, v5, v3

    const/16 v3, 0xf

    const-string v4, "Operation"

    aput-object v4, v5, v3

    const/16 v3, 0x10

    const-string v4, "ResultReportType"

    aput-object v4, v5, v3

    const/16 v3, 0x11

    const-string v4, "ResultReportSourceUri"

    aput-object v4, v5, v3

    const/16 v3, 0x12

    const-string v4, "ResultReportTargetUri"

    aput-object v4, v5, v3

    const/16 v3, 0x13

    const-string v4, "ResultCode"

    aput-object v4, v5, v3

    const/16 v3, 0x14

    const-string v4, "PolicyStartDate"

    aput-object v4, v5, v3

    const/16 v3, 0x15

    const-string v4, "PolicyEndDate"

    aput-object v4, v5, v3

    const/16 v3, 0x16

    const-string v4, "PolicyInterval"

    aput-object v4, v5, v3

    const/16 v3, 0x17

    const-string v4, "MobileTrackingStatus"

    aput-object v4, v5, v3

    .line 187
    .local v5, "From":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 188
    const/4 v3, 0x1

    const-string v4, "MobileTracking"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rowid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p0

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 190
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 192
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 195
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 196
    .local v14, "Id":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsIsValid:Ljava/lang/String;

    .line 197
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLatitude:Ljava/lang/String;

    .line 198
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLogitude:Ljava/lang/String;

    .line 199
    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsAltitude:Ljava/lang/String;

    .line 200
    const/4 v3, 0x5

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsSpeed:Ljava/lang/String;

    .line 201
    const/4 v3, 0x6

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsCourse:Ljava/lang/String;

    .line 202
    const/4 v3, 0x7

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsHorizontalUncertainty:Ljava/lang/String;

    .line 203
    const/16 v3, 0x8

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsVerticalUncertainty:Ljava/lang/String;

    .line 204
    const/16 v3, 0x9

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsDate:Ljava/lang/String;

    .line 205
    const/16 v3, 0xa

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsIsValid:Ljava/lang/String;

    .line 206
    const/16 v3, 0xb

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsCellId:Ljava/lang/String;

    .line 207
    const/16 v3, 0xc

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsLAC:Ljava/lang/String;

    .line 208
    const/16 v3, 0xd

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMNC:Ljava/lang/String;

    .line 209
    const/16 v3, 0xe

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMCC:Ljava/lang/String;

    .line 210
    const/16 v3, 0xf

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nOperation:I

    .line 211
    const/16 v3, 0x10

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nResultReportType:I

    .line 212
    const/16 v3, 0x11

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    .line 213
    const/16 v3, 0x12

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    .line 214
    const/16 v3, 0x13

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultCode:Ljava/lang/String;

    .line 215
    const/16 v3, 0x14

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyStartDate:Ljava/lang/String;

    .line 216
    const/16 v3, 0x15

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyEndDate:Ljava/lang/String;

    .line 217
    const/16 v3, 0x16

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyInterval:Ljava/lang/String;

    .line 218
    const/16 v3, 0x17

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szMobileTrackingStatus:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 222
    .end local v14    # "Id":J
    :catch_0
    move-exception v13

    .line 224
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    if-eqz v12, :cond_0

    .line 229
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 231
    :cond_0
    if-eqz v2, :cond_1

    .line 232
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 235
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 228
    :cond_2
    if-eqz v12, :cond_3

    .line 229
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 231
    :cond_3
    if-eqz v2, :cond_1

    .line 232
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 228
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_4

    .line 229
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 231
    :cond_4
    if-eqz v2, :cond_5

    .line 232
    invoke-static {v2}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v3
.end method

.method public static xamtDbSqlInsertRow(Lcom/fmm/dm/db/file/XDBAMTInfo;)V
    .locals 5
    .param p0, "MobileTrackingInfo"    # Lcom/fmm/dm/db/file/XDBAMTInfo;

    .prologue
    .line 41
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 42
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 46
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "GpsIsValid"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsIsValid:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v3, "GpsLatitude"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLatitude:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v3, "GpsLongitude"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLogitude:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v3, "GpsAltitude"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsAltitude:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v3, "GpsSpeed"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsSpeed:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v3, "GpsCourse"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsCourse:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v3, "GpsHorizontalUncertainty"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsHorizontalUncertainty:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v3, "GpsVerticalUncertainty"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsVerticalUncertainty:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v3, "GpsDate"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsDate:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v3, "NonGpsValid"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsIsValid:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v3, "NonGpsCellId"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsCellId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v3, "NonGpsLAC"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsLAC:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v3, "NonGpsMNC"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMNC:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v3, "NonGpsMCC"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMCC:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v3, "Operation"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nOperation:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 63
    const-string v3, "ResultReportType"

    iget v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nResultReportType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 64
    const-string v3, "ResultReportSourceUri"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v3, "ResultReportTargetUri"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v3, "ResultCode"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultCode:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "PolicyStartDate"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyStartDate:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "PolicyEndDate"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyEndDate:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "PolicyInterval"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyInterval:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "MobileTrackingStatus"

    iget-object v4, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szMobileTrackingStatus:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 74
    const-string v3, "MobileTracking"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    if-eqz v1, :cond_0

    .line 83
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 76
    :catch_0
    move-exception v2

    .line 78
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    if-eqz v1, :cond_0

    .line 83
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 82
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 83
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xamtDbSqlUpdateRow(JLcom/fmm/dm/db/file/XDBAMTInfo;)V
    .locals 6
    .param p0, "RowId"    # J
    .param p2, "MobileTrackingInfo"    # Lcom/fmm/dm/db/file/XDBAMTInfo;

    .prologue
    .line 109
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 110
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 114
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "GpsIsValid"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsIsValid:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v3, "GpsLatitude"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLatitude:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v3, "GpsLongitude"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLogitude:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v3, "GpsAltitude"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsAltitude:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v3, "GpsSpeed"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsSpeed:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v3, "GpsCourse"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsCourse:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v3, "GpsHorizontalUncertainty"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsHorizontalUncertainty:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v3, "GpsVerticalUncertainty"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsVerticalUncertainty:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v3, "GpsDate"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsDate:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v3, "NonGpsValid"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsIsValid:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v3, "NonGpsCellId"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsCellId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v3, "NonGpsLAC"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsLAC:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v3, "NonGpsMNC"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMNC:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v3, "NonGpsMCC"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMCC:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v3, "Operation"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->nOperation:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 131
    const-string v3, "ResultReportType"

    iget v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->nResultReportType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 132
    const-string v3, "ResultReportSourceUri"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v3, "ResultReportTargetUri"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v3, "ResultCode"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultCode:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v3, "PolicyStartDate"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyStartDate:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v3, "PolicyEndDate"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyEndDate:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v3, "PolicyInterval"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyInterval:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v3, "MobileTrackingStatus"

    iget-object v4, p2, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szMobileTrackingStatus:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 142
    const-string v3, "MobileTracking"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    if-eqz v1, :cond_0

    .line 151
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v2

    .line 146
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    if-eqz v1, :cond_0

    .line 151
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 150
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 151
    invoke-static {v1}, Lcom/fmm/dm/db/sql/XDMDbHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method
