.class public interface abstract Lcom/fmm/dm/db/sql/XNOTIDbSql;
.super Ljava/lang/Object;
.source "XNOTIDbSql.java"


# static fields
.field public static final XNOTI_DB_SQL_APPID:Ljava/lang/String; = "appId"

.field public static final XNOTI_DB_SQL_INFO_TABLE:Ljava/lang/String; = "NOTIFICATION"

.field public static final XNOTI_DB_SQL_INFO_TABLE_CREATE:Ljava/lang/String; = "create table if not exists NOTIFICATION (rowid integer primary key autoincrement, appId integer, uiMode integer, sessinoId text, serverId text);"

.field public static final XNOTI_DB_SQL_SERVERID:Ljava/lang/String; = "serverId"

.field public static final XNOTI_DB_SQL_SESSIONID:Ljava/lang/String; = "sessinoId"

.field public static final XNOTI_DB_SQL_UIMODE:Ljava/lang/String; = "uiMode"
