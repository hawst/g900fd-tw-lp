.class public Lcom/fmm/dm/db/file/XDBLawmo;
.super Ljava/lang/Object;
.source "XDBLawmo.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDBInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Ljava/io/Serializable;


# static fields
.field static final DSMURI:Landroid/net/Uri;

.field static final PCWDEVICEID:Landroid/net/Uri;

.field static final PCWURI:Landroid/net/Uri;

.field private static bCallForwardingDisabled:Z = false

.field private static bCallForwardingEnabled:Z = false

.field private static bEmergencyModeState:Z = false

.field private static bExternalWiped:Z = false

.field private static bSIMWiped:Z = false

.field private static bSMSForwardingDisabled:Z = false

.field private static bSMSForwardingEnabled:Z = false

.field private static gLawmoOperation:I = 0x0

.field private static m_nReactivationState:I = 0x0

.field private static m_szCallForwardingNumbers:Ljava/lang/String; = null

.field private static m_szPolicyPeriod:Ljava/lang/String; = null

.field private static m_szSMSForwardingNumbers:Ljava/lang/String; = null

.field static final sDSMKEY:Ljava/lang/String; = "Key"

.field static final sDSMVALUE:Ljava/lang/String; = "Value"

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    sput v1, Lcom/fmm/dm/db/file/XDBLawmo;->gLawmoOperation:I

    .line 26
    sput-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bExternalWiped:Z

    .line 27
    sput-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bSIMWiped:Z

    .line 28
    sput-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bCallForwardingEnabled:Z

    .line 29
    sput-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bSMSForwardingEnabled:Z

    .line 30
    sput-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bCallForwardingDisabled:Z

    .line 31
    sput-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bSMSForwardingDisabled:Z

    .line 32
    sput-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bEmergencyModeState:Z

    .line 33
    const-string v0, ""

    sput-object v0, Lcom/fmm/dm/db/file/XDBLawmo;->m_szCallForwardingNumbers:Ljava/lang/String;

    .line 34
    const-string v0, ""

    sput-object v0, Lcom/fmm/dm/db/file/XDBLawmo;->m_szSMSForwardingNumbers:Ljava/lang/String;

    .line 35
    const-string v0, "3600"

    sput-object v0, Lcom/fmm/dm/db/file/XDBLawmo;->m_szPolicyPeriod:Ljava/lang/String;

    .line 36
    sput v1, Lcom/fmm/dm/db/file/XDBLawmo;->m_nReactivationState:I

    .line 40
    const-string v0, "content://com.sec.dsm.system.dsmcontentprovider/dsm"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/db/file/XDBLawmo;->DSMURI:Landroid/net/Uri;

    .line 41
    const-string v0, "content://com.sec.pcw.device/value"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/db/file/XDBLawmo;->PCWURI:Landroid/net/Uri;

    .line 42
    const-string v0, "content://com.sec.pcw.device/deviceid"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/db/file/XDBLawmo;->PCWDEVICEID:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static xdbLawmoCheckRegFile()Z
    .locals 5

    .prologue
    .line 668
    const/4 v0, 0x0

    .line 669
    .local v0, "bExist":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/system/samsungaccount.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 670
    .local v2, "szFileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 672
    .local v1, "mtfile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 674
    const/4 v0, 0x1

    .line 675
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 678
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File exist? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 679
    return v0
.end method

.method protected static xdbLawmoContainsKeyDSMOperation(Ljava/lang/String;)Z
    .locals 12
    .param p0, "szKey"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1100
    const/4 v9, 0x0

    .line 1101
    .local v9, "ret":Z
    const/4 v7, 0x0

    .line 1104
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "Value"

    aput-object v1, v2, v0

    .line 1106
    .local v2, "szProjection":[Ljava/lang/String;
    const-string v3, "Key = ?"

    .line 1107
    .local v3, "szSelection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p0, v4, v0

    .line 1110
    .local v4, "szSelectionArgs":[Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->DSMURI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1111
    const/4 v6, 0x0

    .line 1112
    .local v6, "count":I
    if-eqz v7, :cond_0

    .line 1114
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 1115
    if-lez v6, :cond_2

    move v9, v10

    .line 1124
    :cond_0
    :goto_0
    if-eqz v7, :cond_1

    .line 1126
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1129
    .end local v2    # "szProjection":[Ljava/lang/String;
    .end local v3    # "szSelection":Ljava/lang/String;
    .end local v4    # "szSelectionArgs":[Ljava/lang/String;
    .end local v6    # "count":I
    :cond_1
    :goto_1
    return v9

    .restart local v2    # "szProjection":[Ljava/lang/String;
    .restart local v3    # "szSelection":Ljava/lang/String;
    .restart local v4    # "szSelectionArgs":[Ljava/lang/String;
    .restart local v6    # "count":I
    :cond_2
    move v9, v11

    .line 1115
    goto :goto_0

    .line 1118
    .end local v2    # "szProjection":[Ljava/lang/String;
    .end local v3    # "szSelection":Ljava/lang/String;
    .end local v4    # "szSelectionArgs":[Ljava/lang/String;
    .end local v6    # "count":I
    :catch_0
    move-exception v8

    .line 1120
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1124
    if-eqz v7, :cond_1

    .line 1126
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1124
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 1126
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static xdbLawmoGetAmtStatus()I
    .locals 5

    .prologue
    .line 564
    sget-object v3, Lcom/fmm/dm/db/file/XDBLawmo;->XLAWMO_STATUS_AMT_OFF:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 565
    .local v2, "nAmtStatus":I
    const/4 v0, 0x0

    .line 569
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xd6

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 576
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    .line 578
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 585
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AmtStatus: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 586
    return v2

    .line 571
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 573
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 582
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    sget-object v3, Lcom/fmm/dm/db/file/XDBLawmo;->XLAWMO_STATUS_AMT_OFF:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1
.end method

.method public static xdbLawmoGetCallForwardingNumbers()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1028
    sget-object v0, Lcom/fmm/dm/db/file/XDBLawmo;->m_szCallForwardingNumbers:Ljava/lang/String;

    return-object v0
.end method

.method public static xdbLawmoGetCallForwardingToBeDisabled()Z
    .locals 1

    .prologue
    .line 972
    sget-boolean v0, Lcom/fmm/dm/db/file/XDBLawmo;->bCallForwardingDisabled:Z

    return v0
.end method

.method public static xdbLawmoGetCallForwardingToBeEnabled()Z
    .locals 1

    .prologue
    .line 950
    sget-boolean v0, Lcom/fmm/dm/db/file/XDBLawmo;->bCallForwardingEnabled:Z

    return v0
.end method

.method public static xdbLawmoGetCorrelator()Ljava/lang/String;
    .locals 5

    .prologue
    .line 907
    const-string v2, ""

    .line 910
    .local v2, "szCorrelator":Ljava/lang/String;
    const/16 v3, 0xe0

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 916
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pCorrelator = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 917
    return-object v2

    .line 912
    :catch_0
    move-exception v1

    .line 914
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetDSMOperationResult(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "szKey"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1134
    const-string v8, "Key = ?"

    .line 1135
    .local v8, "szSelection":Ljava/lang/String;
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "Value"

    aput-object v0, v2, v1

    .line 1137
    .local v2, "szProjection":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 1138
    .local v9, "szValueData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1142
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p0, v4, v0

    .line 1144
    .local v4, "szSelectionArgs":[Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->DSMURI:Landroid/net/Uri;

    const-string v3, "Key = ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1146
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1148
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1157
    :cond_0
    if-eqz v6, :cond_1

    .line 1159
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1162
    .end local v4    # "szSelectionArgs":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v9

    .line 1151
    :catch_0
    move-exception v7

    .line 1153
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1157
    if-eqz v6, :cond_1

    .line 1159
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1157
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1159
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static xdbLawmoGetDbLawmoOperation()I
    .locals 5

    .prologue
    .line 345
    const/4 v2, 0x3

    .line 346
    .local v2, "nOperation":I
    const/4 v0, 0x0

    .line 350
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xcf

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 357
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    .line 359
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 360
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xdbLawmoDbGetDbLawmoOperation: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 366
    :goto_1
    return v2

    .line 352
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 354
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 364
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x3

    goto :goto_1
.end method

.method public static xdbLawmoGetEmergencyModeState()Z
    .locals 2

    .prologue
    .line 994
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Get bEmergencyModeState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bEmergencyModeState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 995
    sget-boolean v0, Lcom/fmm/dm/db/file/XDBLawmo;->bEmergencyModeState:Z

    return v0
.end method

.method public static xdbLawmoGetExtCallRestrictionPhoneNumber()Ljava/lang/String;
    .locals 4

    .prologue
    .line 788
    const-string v2, ""

    .line 791
    .local v2, "szCallRestrictionPhoneNumber":Ljava/lang/String;
    const/16 v3, 0xdc

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 797
    :goto_0
    return-object v2

    .line 793
    :catch_0
    move-exception v1

    .line 795
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetExtCallRestrictionStatus()Ljava/lang/String;
    .locals 5

    .prologue
    .line 814
    const-string v3, ""

    .line 815
    .local v3, "szCallRestrictionStatus":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v1

    .line 817
    .local v1, "bAMTStatus":Z
    if-nez v1, :cond_0

    .line 818
    const-string v3, "30"

    .line 828
    :goto_0
    return-object v3

    .line 822
    :cond_0
    const/16 v4, 0xdd

    :try_start_0
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 824
    :catch_0
    move-exception v2

    .line 826
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetExtLockMyPhoneMessages()Ljava/lang/String;
    .locals 4

    .prologue
    .line 728
    const-string v2, ""

    .line 731
    .local v2, "szLockMyPhoneMessages":Ljava/lang/String;
    const/16 v3, 0xda

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 737
    :goto_0
    return-object v2

    .line 733
    :catch_0
    move-exception v1

    .line 735
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetExtOperation()I
    .locals 5

    .prologue
    .line 383
    const/4 v2, 0x3

    .line 384
    .local v2, "nOperation":I
    const/4 v0, 0x0

    .line 388
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xd0

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 395
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    .line 397
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 398
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ExtOperation: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 404
    :goto_1
    return v2

    .line 390
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 392
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 402
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x3

    goto :goto_1
.end method

.method public static xdbLawmoGetExtPassword()Ljava/lang/String;
    .locals 4

    .prologue
    .line 742
    const-string v2, ""

    .line 745
    .local v2, "szPassword":Ljava/lang/String;
    const/16 v3, 0xd9

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 751
    :goto_0
    return-object v2

    .line 747
    :catch_0
    move-exception v1

    .line 749
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetExtPin()Ljava/lang/String;
    .locals 4

    .prologue
    .line 421
    const-string v2, ""

    .line 424
    .local v2, "szPin":Ljava/lang/String;
    const/16 v3, 0xd1

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :goto_0
    return-object v2

    .line 426
    :catch_0
    move-exception v1

    .line 428
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetExtRingMyPhoneMessages()Ljava/lang/String;
    .locals 4

    .prologue
    .line 881
    const-string v2, ""

    .line 884
    .local v2, "szRingMyPhoneMessages":Ljava/lang/String;
    const/16 v3, 0xdf

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 890
    :goto_0
    return-object v2

    .line 886
    :catch_0
    move-exception v1

    .line 888
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetExtRingMyPhoneStatus()Ljava/lang/String;
    .locals 5

    .prologue
    .line 845
    const-string v3, ""

    .line 846
    .local v3, "szRingMyPhoneStatus":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetSettingRemoteControlEnabled()Z

    move-result v1

    .line 848
    .local v1, "bAMTStatus":Z
    if-nez v1, :cond_0

    .line 849
    const-string v3, "30"

    .line 859
    :goto_0
    return-object v3

    .line 853
    :cond_0
    const/16 v4, 0xde

    :try_start_0
    invoke-static {v4}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 855
    :catch_0
    move-exception v2

    .line 857
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetExternalMemListItemName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 235
    const-string v2, ""

    .line 238
    .local v2, "szListItemName":Ljava/lang/String;
    const/16 v3, 0xcc

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :goto_0
    return-object v2

    .line 240
    :catch_0
    move-exception v1

    .line 242
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetExternalMemToBeWiped()Z
    .locals 5

    .prologue
    .line 261
    const/4 v1, 0x0

    .line 262
    .local v1, "bToBeWiped":Z
    const/4 v0, 0x0

    .line 266
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xcd

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 273
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eqz v3, :cond_0

    .line 275
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 276
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InternalMemToBeWiped: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 282
    :goto_1
    return v1

    .line 268
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 270
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 280
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static xdbLawmoGetExternalToBeWiped()Z
    .locals 1

    .prologue
    .line 928
    sget-boolean v0, Lcom/fmm/dm/db/file/XDBLawmo;->bExternalWiped:Z

    return v0
.end method

.method public static xdbLawmoGetInfo()Lcom/fmm/dm/db/file/XDBLawmoInfo;
    .locals 4

    .prologue
    .line 107
    const/4 v1, 0x0

    .line 110
    .local v1, "LawmoInfo":Lcom/fmm/dm/db/file/XDBLawmoInfo;
    const/16 v3, 0xc8

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    return-object v1

    .line 112
    :catch_0
    move-exception v2

    .line 114
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetInternalMemListItemName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 171
    const-string v2, ""

    .line 174
    .local v2, "szListItemName":Ljava/lang/String;
    const/16 v3, 0xca

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_0
    return-object v2

    .line 176
    :catch_0
    move-exception v1

    .line 178
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetInternalMemToBeWiped()Z
    .locals 5

    .prologue
    .line 197
    const/4 v1, 0x0

    .line 198
    .local v1, "bToBeWiped":Z
    const/4 v0, 0x0

    .line 202
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xcb

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 209
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 212
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InternalMemToBeWiped: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 218
    :goto_1
    return v1

    .line 204
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 206
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static xdbLawmoGetLawmoOperation()I
    .locals 2

    .prologue
    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdbLawmoDbGetLawmoOperation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/db/file/XDBLawmo;->gLawmoOperation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 340
    sget v0, Lcom/fmm/dm/db/file/XDBLawmo;->gLawmoOperation:I

    return v0
.end method

.method public static xdbLawmoGetNotifyUser()Z
    .locals 5

    .prologue
    .line 299
    const/4 v1, 0x0

    .line 300
    .local v1, "bNotifyUser":Z
    const/4 v0, 0x0

    .line 304
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xce

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 311
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eqz v3, :cond_0

    .line 313
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 314
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NotifyUser: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 320
    :goto_1
    return v1

    .line 306
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 308
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 318
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static xdbLawmoGetPCWDeviceID()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1198
    const-string v7, ""

    .line 1199
    .local v7, "deviceid":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1203
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->PCWDEVICEID:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1205
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1209
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 1218
    :cond_0
    if-eqz v6, :cond_1

    .line 1220
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1223
    :cond_1
    :goto_0
    return-object v7

    .line 1212
    :catch_0
    move-exception v8

    .line 1214
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1218
    if-eqz v6, :cond_1

    .line 1220
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1218
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1220
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static xdbLawmoGetPCWSupportReactivationLock()Z
    .locals 10

    .prologue
    .line 1228
    const/4 v6, 0x0

    .line 1229
    .local v6, "bSupport":Z
    const-string v9, ""

    .line 1230
    .local v9, "sztmp":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1234
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->PCWDEVICEID:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1236
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1238
    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1247
    :cond_0
    if-eqz v7, :cond_1

    .line 1249
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1253
    :cond_1
    :goto_0
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "Y"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1255
    const-string v0, "Support Reactivation Lock"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1256
    const/4 v6, 0x1

    .line 1265
    :goto_1
    return v6

    .line 1241
    :catch_0
    move-exception v8

    .line 1243
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1247
    if-eqz v7, :cond_1

    .line 1249
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1247
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 1249
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1260
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sztmp : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1261
    const-string v0, "Not Support Reactivation Lock"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1262
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static xdbLawmoGetPCWURL()Ljava/lang/String;
    .locals 13

    .prologue
    .line 1167
    const/4 v10, 0x0

    .line 1168
    .local v10, "szValueData":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1169
    .local v7, "cursor":Landroid/database/Cursor;
    const/16 v6, 0x8

    .line 1173
    .local v6, "URL_SAFE":I
    :try_start_0
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->PCWURI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1175
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177
    const-string v0, "mvalue"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 1178
    .local v12, "urlColumnIndex":I
    invoke-interface {v7, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1179
    .local v9, "szDBUrl":Ljava/lang/String;
    new-instance v11, Ljava/lang/String;

    const/16 v0, 0x8

    invoke-static {v9, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v10    # "szValueData":Ljava/lang/String;
    .local v11, "szValueData":Ljava/lang/String;
    move-object v10, v11

    .line 1188
    .end local v9    # "szDBUrl":Ljava/lang/String;
    .end local v11    # "szValueData":Ljava/lang/String;
    .end local v12    # "urlColumnIndex":I
    .restart local v10    # "szValueData":Ljava/lang/String;
    :cond_0
    if-eqz v7, :cond_1

    .line 1190
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1193
    :cond_1
    :goto_0
    return-object v10

    .line 1182
    :catch_0
    move-exception v8

    .line 1184
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1188
    if-eqz v7, :cond_1

    .line 1190
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1188
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 1190
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static xdbLawmoGetPolicyPeriod()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1050
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Get Policy Period = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/fmm/dm/db/file/XDBLawmo;->m_szPolicyPeriod:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1054
    :try_start_0
    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->m_szPolicyPeriod:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1063
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->m_szPolicyPeriod:Ljava/lang/String;

    return-object v1

    .line 1056
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 1058
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1059
    const-string v1, "Not changed Long Type, Set default period : 3600"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1060
    const-string v1, "3600"

    sput-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->m_szPolicyPeriod:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdbLawmoGetReactivationLock()Ljava/lang/String;
    .locals 5

    .prologue
    .line 756
    const-string v2, ""

    .line 759
    .local v2, "szLock":Ljava/lang/String;
    const/16 v3, 0xdb

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 765
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ReactivationLock : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 766
    return-object v2

    .line 761
    :catch_0
    move-exception v1

    .line 763
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetReactivationState()I
    .locals 2

    .prologue
    .line 1016
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Get m_szReactivationState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/db/file/XDBLawmo;->m_nReactivationState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1017
    sget v0, Lcom/fmm/dm/db/file/XDBLawmo;->m_nReactivationState:I

    return v0
.end method

.method public static xdbLawmoGetRegistration()Z
    .locals 5

    .prologue
    .line 603
    const/4 v1, 0x0

    .line 604
    .local v1, "bRegistration":Z
    const/4 v0, 0x0

    .line 608
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xd7

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 615
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eqz v3, :cond_0

    .line 617
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 624
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Registration: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 625
    return v1

    .line 610
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 612
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 621
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static xdbLawmoGetResultCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 537
    const-string v2, ""

    .line 540
    .local v2, "szResultCode":Ljava/lang/String;
    const/16 v3, 0xd5

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 546
    :goto_0
    return-object v2

    .line 542
    :catch_0
    move-exception v1

    .line 544
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetResultReportSourceUri()Ljava/lang/String;
    .locals 4

    .prologue
    .line 485
    const-string v2, ""

    .line 488
    .local v2, "szSourceUri":Ljava/lang/String;
    const/16 v3, 0xd3

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 494
    :goto_0
    return-object v2

    .line 490
    :catch_0
    move-exception v1

    .line 492
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetResultReportTargetUri()Ljava/lang/String;
    .locals 4

    .prologue
    .line 511
    const-string v2, ""

    .line 514
    .local v2, "szTargetUri":Ljava/lang/String;
    const/16 v3, 0xd4

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 520
    :goto_0
    return-object v2

    .line 516
    :catch_0
    move-exception v1

    .line 518
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoGetResultReportType()I
    .locals 5

    .prologue
    .line 447
    const/4 v2, 0x1

    .line 448
    .local v2, "nResultReportType":I
    const/4 v0, 0x0

    .line 452
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xd2

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 459
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    .line 461
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 462
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ResultReportType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 468
    :goto_1
    return v2

    .line 454
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 456
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 466
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static xdbLawmoGetSIMToBeWiped()Z
    .locals 1

    .prologue
    .line 939
    sget-boolean v0, Lcom/fmm/dm/db/file/XDBLawmo;->bSIMWiped:Z

    return v0
.end method

.method public static xdbLawmoGetSMSForwardingNumbers()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1039
    sget-object v0, Lcom/fmm/dm/db/file/XDBLawmo;->m_szSMSForwardingNumbers:Ljava/lang/String;

    return-object v0
.end method

.method public static xdbLawmoGetSMSForwardingToBeDisabled()Z
    .locals 1

    .prologue
    .line 983
    sget-boolean v0, Lcom/fmm/dm/db/file/XDBLawmo;->bSMSForwardingDisabled:Z

    return v0
.end method

.method public static xdbLawmoGetSMSForwardingToBeEnabled()Z
    .locals 1

    .prologue
    .line 961
    sget-boolean v0, Lcom/fmm/dm/db/file/XDBLawmo;->bSMSForwardingEnabled:Z

    return v0
.end method

.method public static xdbLawmoGetSimChangeAlert()Z
    .locals 5

    .prologue
    .line 642
    const/4 v1, 0x0

    .line 643
    .local v1, "bState":Z
    const/4 v0, 0x0

    .line 647
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xd8

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 654
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eqz v3, :cond_0

    .line 656
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 657
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM Change Alert = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 663
    :goto_1
    return v1

    .line 649
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 651
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 661
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static xdbLawmoGetState()I
    .locals 5

    .prologue
    .line 133
    sget-object v3, Lcom/fmm/dm/db/file/XDBLawmo;->XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 134
    .local v2, "nState":I
    const/4 v0, 0x0

    .line 138
    .local v0, "Obj":Ljava/lang/Object;
    const/16 v3, 0xc9

    :try_start_0
    invoke-static {v3}, Lcom/fmm/dm/db/file/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 145
    .end local v0    # "Obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    .line 147
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 148
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LawmoState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 154
    :goto_1
    return v2

    .line 140
    .restart local v0    # "Obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 152
    .end local v0    # "Obj":Ljava/lang/Object;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    sget-object v3, Lcom/fmm/dm/db/file/XDBLawmo;->XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1
.end method

.method public static xdbLawmoInitNVMLawmoInfo(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p0, "NVMLawmoInfo"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 46
    move-object v0, p0

    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 48
    .local v0, "LawmoInfo":Lcom/fmm/dm/db/file/XDBLawmoInfo;
    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .end local v0    # "LawmoInfo":Lcom/fmm/dm/db/file/XDBLawmoInfo;
    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBLawmoInfo;-><init>()V

    .line 85
    .restart local v0    # "LawmoInfo":Lcom/fmm/dm/db/file/XDBLawmoInfo;
    :goto_0
    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoCheckRegFile()Z

    move-result v1

    iput-boolean v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    .line 87
    return-object v0

    .line 54
    :cond_0
    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nState:I

    .line 56
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const-string v2, "InternalMem"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    .line 57
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->InternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iput-boolean v4, v1, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    .line 59
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    const-string v2, "ExternalMem"

    iput-object v2, v1, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->m_szListItemName:Ljava/lang/String;

    .line 60
    iget-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->ExternalMem:Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;

    iput-boolean v3, v1, Lcom/fmm/dm/db/file/XDBLawmoAvailableWipeListItemInfo;->bToBeWiped:Z

    .line 62
    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bNotifyUser:Z

    .line 64
    iput v5, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nOperation:I

    .line 65
    iput v5, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nExtOperation:I

    .line 66
    const-string v1, "00000000"

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPin:Ljava/lang/String;

    .line 68
    iput v4, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nResultReportType:I

    .line 69
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    .line 70
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    .line 71
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szResultCode:Ljava/lang/String;

    .line 72
    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->XLAWMO_STATUS_AMT_OFF:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->nAmtStaus:I

    .line 73
    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bRegistration:Z

    .line 74
    iput-boolean v3, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->bSimChangeState:Z

    .line 75
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szPassword:Ljava/lang/String;

    .line 76
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szLockMyPhoneMessages:Ljava/lang/String;

    .line 77
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szReactivationLock:Ljava/lang/String;

    .line 78
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionPhoneNumber:Ljava/lang/String;

    .line 79
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCallRestrictionStatus:Ljava/lang/String;

    .line 80
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneStatus:Ljava/lang/String;

    .line 81
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szRingMyPhoneMessages:Ljava/lang/String;

    .line 82
    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;->m_szCorrelator:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdbLawmoSetAmtStatus(I)V
    .locals 3
    .param p0, "nAmtStatus"    # I

    .prologue
    .line 554
    const/16 v1, 0xd6

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 560
    :goto_0
    return-void

    .line 556
    :catch_0
    move-exception v0

    .line 558
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetCallForwardingNumbers(Ljava/lang/String;)V
    .locals 2
    .param p0, "szNumbers"    # Ljava/lang/String;

    .prologue
    .line 1022
    sput-object p0, Lcom/fmm/dm/db/file/XDBLawmo;->m_szCallForwardingNumbers:Ljava/lang/String;

    .line 1023
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set Call Forwarding Numbers = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->m_szCallForwardingNumbers:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1024
    return-void
.end method

.method public static xdbLawmoSetCallForwardingToBeDisabled(Z)V
    .locals 2
    .param p0, "bDisabled"    # Z

    .prologue
    .line 966
    sput-boolean p0, Lcom/fmm/dm/db/file/XDBLawmo;->bCallForwardingDisabled:Z

    .line 967
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set Call Forwarding ToBeDisabled = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bCallForwardingDisabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 968
    return-void
.end method

.method public static xdbLawmoSetCallForwardingToBeEnabled(Z)V
    .locals 2
    .param p0, "bEnabled"    # Z

    .prologue
    .line 944
    sput-boolean p0, Lcom/fmm/dm/db/file/XDBLawmo;->bCallForwardingEnabled:Z

    .line 945
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set CallForwarding ToBeEnabled = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bCallForwardingEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 946
    return-void
.end method

.method public static xdbLawmoSetCorrelator(Ljava/lang/String;)V
    .locals 2
    .param p0, "szCorrelator"    # Ljava/lang/String;

    .prologue
    .line 897
    const/16 v1, 0xe0

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 903
    :goto_0
    return-void

    .line 899
    :catch_0
    move-exception v0

    .line 901
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetDSMOperationResult(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "szKey"    # Ljava/lang/String;
    .param p1, "szValue"    # Ljava/lang/String;

    .prologue
    .line 1068
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1070
    :cond_0
    const-string v4, "key or Value Null"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1096
    :goto_0
    return-void

    .line 1076
    :cond_1
    :try_start_0
    const-string v1, "Key = ?"

    .line 1077
    .local v1, "szSelection":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p0, v2, v4

    .line 1080
    .local v2, "szSelectionArgs":[Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1081
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "Key"

    invoke-virtual {v3, v4, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    const-string v4, "Value"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083
    invoke-static {p0}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoContainsKeyDSMOperation(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1085
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/fmm/dm/db/file/XDBLawmo;->DSMURI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1092
    .end local v1    # "szSelection":Ljava/lang/String;
    .end local v2    # "szSelectionArgs":[Ljava/lang/String;
    .end local v3    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 1094
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1089
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "szSelection":Ljava/lang/String;
    .restart local v2    # "szSelectionArgs":[Ljava/lang/String;
    .restart local v3    # "values":Landroid/content/ContentValues;
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/fmm/dm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/fmm/dm/db/file/XDBLawmo;->DSMURI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static xdbLawmoSetEmergencyModeState(Z)V
    .locals 2
    .param p0, "bState"    # Z

    .prologue
    .line 988
    sput-boolean p0, Lcom/fmm/dm/db/file/XDBLawmo;->bEmergencyModeState:Z

    .line 989
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set bEmergencyModeState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bEmergencyModeState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 990
    return-void
.end method

.method public static xdbLawmoSetExtCallRestrictionPhoneNumber(Ljava/lang/String;)V
    .locals 2
    .param p0, "szCallRestrictionPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 771
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 773
    const-string p0, ""

    .line 778
    :cond_0
    const/16 v1, 0xdc

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 784
    :goto_0
    return-void

    .line 780
    :catch_0
    move-exception v0

    .line 782
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExtCallRestrictionStatus(Ljava/lang/String;)V
    .locals 2
    .param p0, "szCallRestrictionStatus"    # Ljava/lang/String;

    .prologue
    .line 804
    const/16 v1, 0xdd

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 810
    :goto_0
    return-void

    .line 806
    :catch_0
    move-exception v0

    .line 808
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExtLockMyPhoneMessages(Ljava/lang/String;)V
    .locals 3
    .param p0, "szLockMyPhoneMessages"    # Ljava/lang/String;

    .prologue
    .line 684
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 685
    const-string p0, ""

    .line 687
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "szLockMyPhoneMessages "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 691
    const/16 v1, 0xda

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 697
    :goto_0
    return-void

    .line 693
    :catch_0
    move-exception v0

    .line 695
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExtOperation(I)V
    .locals 3
    .param p0, "nOperation"    # I

    .prologue
    .line 373
    const/16 v1, 0xd0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :goto_0
    return-void

    .line 375
    :catch_0
    move-exception v0

    .line 377
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExtPassword(Ljava/lang/String;)V
    .locals 3
    .param p0, "szPassword"    # Ljava/lang/String;

    .prologue
    .line 701
    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 702
    const-string v1, "\r"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 705
    const/16 v1, 0xd9

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 711
    :goto_0
    return-void

    .line 707
    :catch_0
    move-exception v0

    .line 709
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExtPin(Ljava/lang/String;)V
    .locals 2
    .param p0, "szPin"    # Ljava/lang/String;

    .prologue
    .line 411
    const/16 v1, 0xd1

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    :goto_0
    return-void

    .line 413
    :catch_0
    move-exception v0

    .line 415
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExtReactivationLock(Ljava/lang/String;)V
    .locals 3
    .param p0, "szLock"    # Ljava/lang/String;

    .prologue
    .line 717
    const/16 v1, 0xdb

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 723
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReactivationLock : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 724
    return-void

    .line 719
    :catch_0
    move-exception v0

    .line 721
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExtRingMyPhoneMessages(Ljava/lang/String;)V
    .locals 3
    .param p0, "szRingMyPhoneMessages"    # Ljava/lang/String;

    .prologue
    .line 864
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 865
    const-string p0, ""

    .line 867
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "szRingMyPhoneMessages "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 871
    const/16 v1, 0xdf

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    :goto_0
    return-void

    .line 873
    :catch_0
    move-exception v0

    .line 875
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExtRingMyPhoneStatus(Ljava/lang/String;)V
    .locals 2
    .param p0, "szRingMyPhoneStatus"    # Ljava/lang/String;

    .prologue
    .line 835
    const/16 v1, 0xde

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 841
    :goto_0
    return-void

    .line 837
    :catch_0
    move-exception v0

    .line 839
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExternalMemListItemName(Ljava/lang/String;)V
    .locals 2
    .param p0, "szListItemName"    # Ljava/lang/String;

    .prologue
    .line 225
    const/16 v1, 0xcc

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExternalMemToBeWiped(Z)V
    .locals 3
    .param p0, "bToBeWiped"    # Z

    .prologue
    .line 251
    const/16 v1, 0xcd

    :try_start_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :goto_0
    return-void

    .line 253
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetExternalToBeWiped(Z)V
    .locals 2
    .param p0, "bToBeWiped"    # Z

    .prologue
    .line 922
    sput-boolean p0, Lcom/fmm/dm/db/file/XDBLawmo;->bExternalWiped:Z

    .line 923
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set External ToBeWiped = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bExternalWiped:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 924
    return-void
.end method

.method public static xdbLawmoSetInfo(Ljava/lang/Object;)V
    .locals 3
    .param p0, "Obj"    # Ljava/lang/Object;

    .prologue
    .line 92
    const/4 v0, 0x0

    .local v0, "LawmoInfo":Lcom/fmm/dm/db/file/XDBLawmoInfo;
    move-object v0, p0

    .line 94
    check-cast v0, Lcom/fmm/dm/db/file/XDBLawmoInfo;

    .line 97
    const/16 v2, 0xc8

    :try_start_0
    invoke-static {v2, v0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetInternalMemListItemName(Ljava/lang/String;)V
    .locals 2
    .param p0, "szListItemName"    # Ljava/lang/String;

    .prologue
    .line 161
    const/16 v1, 0xca

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetInternalMemToBeWiped(Z)V
    .locals 3
    .param p0, "bToBeWiped"    # Z

    .prologue
    .line 187
    const/16 v1, 0xcb

    :try_start_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetLawmoOperation(I)V
    .locals 3
    .param p0, "nOperation"    # I

    .prologue
    .line 327
    const/16 v1, 0xcf

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 328
    sput p0, Lcom/fmm/dm/db/file/XDBLawmo;->gLawmoOperation:I

    .line 329
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nOperation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :goto_0
    return-void

    .line 331
    :catch_0
    move-exception v0

    .line 333
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetNotifyUser(Z)V
    .locals 3
    .param p0, "bNotifyUser"    # Z

    .prologue
    .line 289
    const/16 v1, 0xce

    :try_start_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_0
    return-void

    .line 291
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetPolicyPeriod(Ljava/lang/String;)V
    .locals 2
    .param p0, "szPeriod"    # Ljava/lang/String;

    .prologue
    .line 1044
    sput-object p0, Lcom/fmm/dm/db/file/XDBLawmo;->m_szPolicyPeriod:Ljava/lang/String;

    .line 1045
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set Policy Period = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1046
    return-void
.end method

.method public static xdbLawmoSetReactivationState(Ljava/lang/String;)V
    .locals 2
    .param p0, "szState"    # Ljava/lang/String;

    .prologue
    .line 1000
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1002
    const-string v0, "Y"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1004
    const/4 v0, 0x1

    sput v0, Lcom/fmm/dm/db/file/XDBLawmo;->m_nReactivationState:I

    .line 1011
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set m_nReactivationState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/fmm/dm/db/file/XDBLawmo;->m_nReactivationState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1012
    return-void

    .line 1006
    :cond_1
    const-string v0, "N"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1008
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/db/file/XDBLawmo;->m_nReactivationState:I

    goto :goto_0
.end method

.method public static xdbLawmoSetRegistration(Z)V
    .locals 3
    .param p0, "bRegistration"    # Z

    .prologue
    .line 593
    const/16 v1, 0xd7

    :try_start_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 599
    :goto_0
    return-void

    .line 595
    :catch_0
    move-exception v0

    .line 597
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetResultCode(Ljava/lang/String;)V
    .locals 2
    .param p0, "szResultCode"    # Ljava/lang/String;

    .prologue
    .line 527
    const/16 v1, 0xd5

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_0
    return-void

    .line 529
    :catch_0
    move-exception v0

    .line 531
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetResultReportSourceUri(Ljava/lang/String;)V
    .locals 2
    .param p0, "szSourceUri"    # Ljava/lang/String;

    .prologue
    .line 475
    const/16 v1, 0xd3

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :goto_0
    return-void

    .line 477
    :catch_0
    move-exception v0

    .line 479
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetResultReportTargetUri(Ljava/lang/String;)V
    .locals 2
    .param p0, "szTargetUri"    # Ljava/lang/String;

    .prologue
    .line 501
    const/16 v1, 0xd4

    :try_start_0
    invoke-static {v1, p0}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 507
    :goto_0
    return-void

    .line 503
    :catch_0
    move-exception v0

    .line 505
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetResultReportType(I)V
    .locals 3
    .param p0, "nResultReportType"    # I

    .prologue
    .line 437
    const/16 v1, 0xd2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    :goto_0
    return-void

    .line 439
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetSIMToBeWiped(Z)V
    .locals 2
    .param p0, "bToBeWiped"    # Z

    .prologue
    .line 933
    sput-boolean p0, Lcom/fmm/dm/db/file/XDBLawmo;->bSIMWiped:Z

    .line 934
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set SIM ToBeWiped = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bSIMWiped:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 935
    return-void
.end method

.method public static xdbLawmoSetSMSForwardingNumbers(Ljava/lang/String;)V
    .locals 2
    .param p0, "szNumbers"    # Ljava/lang/String;

    .prologue
    .line 1033
    sput-object p0, Lcom/fmm/dm/db/file/XDBLawmo;->m_szSMSForwardingNumbers:Ljava/lang/String;

    .line 1034
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set Call Forwarding Numbers = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/dm/db/file/XDBLawmo;->m_szSMSForwardingNumbers:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1035
    return-void
.end method

.method public static xdbLawmoSetSMSForwardingToBeDisabled(Z)V
    .locals 2
    .param p0, "bEnabled"    # Z

    .prologue
    .line 977
    sput-boolean p0, Lcom/fmm/dm/db/file/XDBLawmo;->bSMSForwardingDisabled:Z

    .line 978
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set SMS Forwarding ToBeDisabled = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bSMSForwardingDisabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 979
    return-void
.end method

.method public static xdbLawmoSetSMSForwardingToBeEnabled(Z)V
    .locals 2
    .param p0, "bEnabled"    # Z

    .prologue
    .line 955
    sput-boolean p0, Lcom/fmm/dm/db/file/XDBLawmo;->bSMSForwardingEnabled:Z

    .line 956
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set SMS Forwarding ToBeEnabled = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/db/file/XDBLawmo;->bSMSForwardingEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 957
    return-void
.end method

.method public static xdbLawmoSetSimChangeAlert(Z)V
    .locals 3
    .param p0, "bState"    # Z

    .prologue
    .line 632
    const/16 v1, 0xd8

    :try_start_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 638
    :goto_0
    return-void

    .line 634
    :catch_0
    move-exception v0

    .line 636
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbLawmoSetState(I)V
    .locals 3
    .param p0, "nState"    # I

    .prologue
    .line 123
    const/16 v1, 0xc9

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fmm/dm/db/file/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method
