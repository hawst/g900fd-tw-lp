.class public Lcom/fmm/dm/db/file/XDBAMTInfo;
.super Ljava/lang/Object;
.source "XDBAMTInfo.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XAMTInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public m_szGpsAltitude:Ljava/lang/String;

.field public m_szGpsCourse:Ljava/lang/String;

.field public m_szGpsDate:Ljava/lang/String;

.field public m_szGpsHorizontalUncertainty:Ljava/lang/String;

.field public m_szGpsIsValid:Ljava/lang/String;

.field public m_szGpsLatitude:Ljava/lang/String;

.field public m_szGpsLogitude:Ljava/lang/String;

.field public m_szGpsSpeed:Ljava/lang/String;

.field public m_szGpsVerticalUncertainty:Ljava/lang/String;

.field public m_szMobileTrackingStatus:Ljava/lang/String;

.field public m_szNonGpsCellId:Ljava/lang/String;

.field public m_szNonGpsIsValid:Ljava/lang/String;

.field public m_szNonGpsLAC:Ljava/lang/String;

.field public m_szNonGpsMCC:Ljava/lang/String;

.field public m_szNonGpsMNC:Ljava/lang/String;

.field public m_szPolicyEndDate:Ljava/lang/String;

.field public m_szPolicyInterval:Ljava/lang/String;

.field public m_szPolicyStartDate:Ljava/lang/String;

.field public m_szResultCode:Ljava/lang/String;

.field public m_szResultReportSourceUri:Ljava/lang/String;

.field public m_szResultReportTargetUri:Ljava/lang/String;

.field public nOperation:I

.field public nResultReportType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, "Y"

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsIsValid:Ljava/lang/String;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLatitude:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsLogitude:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsAltitude:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsSpeed:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsCourse:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsHorizontalUncertainty:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsVerticalUncertainty:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szGpsDate:Ljava/lang/String;

    .line 53
    const-string v0, "N"

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsIsValid:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsCellId:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsLAC:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMNC:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szNonGpsMCC:Ljava/lang/String;

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nOperation:I

    .line 61
    const/4 v0, 0x1

    iput v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->nResultReportType:I

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportSourceUri:Ljava/lang/String;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultReportTargetUri:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szResultCode:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyStartDate:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyEndDate:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szPolicyInterval:Ljava/lang/String;

    .line 69
    const-string v0, "10"

    iput-object v0, p0, Lcom/fmm/dm/db/file/XDBAMTInfo;->m_szMobileTrackingStatus:Ljava/lang/String;

    .line 70
    return-void
.end method
