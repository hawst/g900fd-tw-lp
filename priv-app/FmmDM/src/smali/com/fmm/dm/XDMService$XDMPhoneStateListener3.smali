.class public Lcom/fmm/dm/XDMService$XDMPhoneStateListener3;
.super Landroid/telephony/PhoneStateListener;
.source "XDMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/XDMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XDMPhoneStateListener3"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1049
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyListeners()V
    .locals 7

    .prologue
    const/16 v6, 0xc

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1126
    const/4 v0, 0x0

    .line 1127
    .local v0, "telephony1":Lcom/fmm/dm/adapter/XDMTelephonyData;
    const/4 v1, 0x0

    .line 1129
    .local v1, "telephony2":Lcom/fmm/dm/adapter/XDMTelephonyData;
    sget-object v2, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    invoke-static {v2}, Lcom/fmm/dm/adapter/XDMDsdsAdapter;->xdmDsdsGetTelephonyData1(Landroid/telephony/ServiceState;)Lcom/fmm/dm/adapter/XDMTelephonyData;

    move-result-object v0

    .line 1130
    sget-object v2, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    invoke-static {v2}, Lcom/fmm/dm/adapter/XDMDsdsAdapter;->xdmDsdsGetTelephonyData2(Landroid/telephony/ServiceState;)Lcom/fmm/dm/adapter/XDMTelephonyData;

    move-result-object v1

    .line 1132
    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    .line 1134
    :cond_0
    const-string v2, "XDMTelephonyData is null. "

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1178
    :cond_1
    :goto_0
    return-void

    .line 1138
    :cond_2
    iget v2, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-eqz v2, :cond_7

    .line 1139
    iget v2, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmSetServiceType(I)I

    .line 1145
    :goto_1
    iget v2, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmSetSimState(I)V

    .line 1146
    iget v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmSetSimState2(I)V

    .line 1148
    iget v2, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-nez v2, :cond_3

    iget v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-eqz v2, :cond_4

    .line 1150
    :cond_3
    sget-boolean v2, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v2, :cond_4

    sget-boolean v2, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v2, :cond_4

    sget-boolean v2, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    if-nez v2, :cond_4

    .line 1152
    sput-boolean v5, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    .line 1153
    invoke-static {v6, v4, v4}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 1158
    :cond_4
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    if-nez v2, :cond_9

    iget-boolean v2, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-nez v2, :cond_5

    iget-boolean v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-eqz v2, :cond_9

    .line 1160
    :cond_5
    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v5}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 1162
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "going to Roaming Area - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1171
    :cond_6
    :goto_2
    sget-object v2, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getState()I

    move-result v2

    if-nez v2, :cond_1

    .line 1173
    sget-boolean v2, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/fmm/dm/agent/XDMTask;->g_IsPhoneBookInitialized:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v2, :cond_1

    .line 1175
    invoke-static {v6, v4, v4}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1140
    :cond_7
    iget v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-eqz v2, :cond_8

    .line 1141
    iget v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    invoke-static {v2}, Lcom/fmm/dm/XDMService;->xdmSetServiceType(I)I

    goto :goto_1

    .line 1143
    :cond_8
    invoke-static {v3}, Lcom/fmm/dm/XDMService;->xdmSetServiceType(I)I

    goto :goto_1

    .line 1164
    :cond_9
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-boolean v2, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-nez v2, :cond_6

    iget-boolean v2, v1, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-nez v2, :cond_6

    .line 1166
    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v3}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 1168
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exit from Roaming Area - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "szIncomingNumber"    # Ljava/lang/String;

    .prologue
    .line 1054
    const-string v0, ">>>>>>>>>>> onCallStateChanged"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1056
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDsdsAdapter;->xdmDsdsCheckService()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1081
    :cond_0
    :goto_0
    return-void

    .line 1061
    :cond_1
    sget-object v0, Lcom/fmm/dm/XDMService;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    .line 1064
    if-nez p1, :cond_3

    .line 1066
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_2

    .line 1068
    const-string v0, "Run Resume Operation, Resume call CallStateChanged"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1069
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    .line 1071
    :cond_2
    const-string v0, ">>>>>>>>>>> CALL_STATE_IDLE"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 1073
    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 1075
    const-string v0, ">>>>>>>>>>> CALL_STATE_RINGING"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 1077
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1079
    const-string v0, ">>>>>>>>>>> CALL_STATE_OFFHOOK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDataActivity(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 1122
    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "networkType"    # I

    .prologue
    .line 1102
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataState()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1104
    invoke-static {p1}, Lcom/fmm/dm/XDMService;->xdmSetDataState(I)V

    .line 1107
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 1109
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener3;->notifyListeners()V

    .line 1111
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_1

    .line 1113
    const-string v0, "Run Resume Operation, Resume call DataConnectionState"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1114
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    .line 1117
    :cond_1
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 1
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 1092
    sget-object v0, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    if-eq v0, p1, :cond_0

    .line 1094
    sput-object p1, Lcom/fmm/dm/XDMService;->serviceState:Landroid/telephony/ServiceState;

    .line 1095
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener3;->notifyListeners()V

    .line 1097
    :cond_0
    return-void
.end method

.method public onSignalStrengthChanged(I)V
    .locals 0
    .param p1, "asu"    # I

    .prologue
    .line 1087
    return-void
.end method
