.class public interface abstract Lcom/fmm/dm/interfaces/XCommonInterface;
.super Ljava/lang/Object;
.source "XCommonInterface.java"


# static fields
.field public static final PACKAGE_NAME_ANDROID:Ljava/lang/String; = "android"

.field public static final PACKAGE_NAME_FOTACLIENT:Ljava/lang/String; = "fotaclient"

.field public static final PACKAGE_NAME_SETTING:Ljava/lang/String; = "settings"

.field public static final XCOMMON_INTENT_EMERGENCY_APPLICATION_MODIFIED:Ljava/lang/String; = "android.intent.action.EMERGENCY_APPLICATION_MODIFIED"

.field public static final XCOMMON_INTENT_EMERGENCY_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.EMERGENCY_STATE_CHANGED"

.field public static final XCOMMON_INTENT_FACTORY_RESET:Ljava/lang/String; = "android.intent.action.dsm.DM_FACTORY_RESET"

.field public static final XCOMMON_INTENT_FORWARDING:Ljava/lang/String; = "android.intent.action.dsm.DM_FORWARDING"

.field public static final XCOMMON_INTENT_FORWARDING_RESULT:Ljava/lang/String; = "android.intent.action.dsm.DM_FORWARDING_RESULT"

.field public static final XCOMMON_INTENT_GET_LOCATION:Ljava/lang/String; = "android.intent.action.dsm.DM_FIND_MY_PHONE"

.field public static final XCOMMON_INTENT_GET_LOCATION_RESULT:Ljava/lang/String; = "android.intent.action.dsm.DM_LOCATION_RESULT"

.field public static final XCOMMON_INTENT_LOCK_MY_PHONE:Ljava/lang/String; = "android.intent.action.dsm.DM_LOCK_MY_PHONE"

.field public static final XCOMMON_INTENT_LOCK_RELEASE:Ljava/lang/String; = "android.intent.action.dsm.DM_LOCK_RELEASE"

.field public static final XCOMMON_INTENT_MOBILE_TRACKING_START:Ljava/lang/String; = "android.intent.action.dsm.DM_MOBILE_TRACKING_START"

.field public static final XCOMMON_INTENT_MOBILE_TRACKING_STOP:Ljava/lang/String; = "android.intent.action.dsm.DM_MOBILE_TRACKING_STOP"

.field public static final XCOMMON_INTENT_PCW_UNLOCKED_ACTION:Ljava/lang/String; = "com.android.internal.policy.impl.Keyguard.PCW_UNLOCKED"

.field public static final XCOMMON_INTENT_REACTIVATIONLOCK:Ljava/lang/String; = "android.intent.action.dsm.DM_REACT_LOCK"

.field public static final XCOMMON_INTENT_REACTIVATIONLOCK_OFF:Ljava/lang/String; = "android.settings.reactivationlock_off"

.field public static final XCOMMON_INTENT_REACTIVATIONLOCK_ON:Ljava/lang/String; = "android.settings.reactivationlock_on"

.field public static final XCOMMON_INTENT_REACTIVATIONLOCK_RESULT:Ljava/lang/String; = "android.intent.action.dsm.DM_RL_LOCK_RESULT"

.field public static final XCOMMON_INTENT_RESUME_EMERGENCY_APPLICATION_MODIFIED:I = 0xa

.field public static final XCOMMON_INTENT_RESUME_EMERGENCY_STATE_CHANGED:I = 0x9

.field public static final XCOMMON_INTENT_RESUME_GET_LOCATION:I = 0xb

.field public static final XCOMMON_INTENT_RESUME_IP_PUSH:I = 0x1

.field public static final XCOMMON_INTENT_RESUME_NONE:I = 0x0

.field public static final XCOMMON_INTENT_RESUME_PCW_FORWARDING:I = 0x6

.field public static final XCOMMON_INTENT_RESUME_PCW_UNLOCKED:I = 0x2

.field public static final XCOMMON_INTENT_RESUME_PCW_WIPE:I = 0x5

.field public static final XCOMMON_INTENT_RESUME_REACTIVATIONLOCK:I = 0xc

.field public static final XCOMMON_INTENT_RESUME_REACTIVATIONLOCK_ALERT:I = 0xd

.field public static final XCOMMON_INTENT_RESUME_UNLOCK_ALERT:I = 0x8

.field public static final XCOMMON_INTENT_RESUME_UPDATE_URL:I = 0x7

.field public static final XCOMMON_INTENT_RING_MY_PHONE:Ljava/lang/String; = "android.intent.action.dsm.DM_RING_MY_PHONE"

.field public static final XCOMMON_INTENT_WIPE:Ljava/lang/String; = "android.intent.action.dsm.DM_WIPE"

.field public static final XCOMMON_INTENT_WIPE_RESULT:Ljava/lang/String; = "android.intent.action.dsm.DM_WIPE_RESULT"
