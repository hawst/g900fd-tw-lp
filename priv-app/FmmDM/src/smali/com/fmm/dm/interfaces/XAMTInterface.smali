.class public interface abstract Lcom/fmm/dm/interfaces/XAMTInterface;
.super Ljava/lang/Object;
.source "XAMTInterface.java"


# static fields
.field public static final OSPS_AMT_GPS_DATE_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/GPS/Date"

.field public static final OSPS_AMT_GPS_HORIZONTAL_UNCERTANITY_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/GPS/horizontalUncertainty"

.field public static final OSPS_AMT_GPS_ISVALID_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/GPS/isValid"

.field public static final OSPS_AMT_GPS_LATITUDE_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/GPS/latitude"

.field public static final OSPS_AMT_GPS_LONGITUDE_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/GPS/longitude"

.field public static final OSPS_AMT_GPS_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/GPS"

.field public static final OSPS_AMT_GPS_VERTICAL_UNCERTANITY_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/GPS/verticalUncertainty"

.field public static final OSPS_AMT_NONGPS_CELLID_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/NonGPS/CellId"

.field public static final OSPS_AMT_NONGPS_ISVALID_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/NonGPS/isValid"

.field public static final OSPS_AMT_NONGPS_LAC_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/NonGPS/LAC"

.field public static final OSPS_AMT_NONGPS_MCC_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/NonGPS/MCC"

.field public static final OSPS_AMT_NONGPS_MNC_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/NonGPS/MNC"

.field public static final OSPS_AMT_NONGPS_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/NonGPS"

.field public static final OSPS_AMT_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTracking/Operations"

.field public static final OSPS_AMT_OPERATION_GET_LOCATION_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/Operation/Get/Location"

.field public static final OSPS_AMT_OPERATION_GET_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/Operation/Get"

.field public static final OSPS_AMT_OPERATION_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/Operation"

.field public static final OSPS_AMT_OPERATION_STOP_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTracking/Operations/Stop"

.field public static final OSPS_AMT_OPERATION_TRACKING_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTracking/Operations/Tracking"

.field public static final OSPS_AMT_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking"

.field public static final OSPS_AMT_PATH2:Ljava/lang/String; = "./Ext/OSPS/MobileTracking"

.field public static final OSPS_AMT_POLICY_ENDDATE_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTracking/Policy/EndDate"

.field public static final OSPS_AMT_POLICY_INTERVAL_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTracking/Policy/Interval"

.field public static final OSPS_AMT_POLICY_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTracking/Policy"

.field public static final OSPS_AMT_POLICY_STARTDATE_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTracking/Policy/StartDate"

.field public static final OSPS_AMT_REQUESTOR_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTraking/RequestorName"

.field public static final OSPS_AMT_STATUS_PATH:Ljava/lang/String; = "./Ext/OSPS/MobileTracking/Status"

.field public static final XAMT_ALERT_TYPE_RESULT_REPORT:Ljava/lang/String; = "urn:oma:at:lawmo:1.0:OperationComplete"

.field public static final XAMT_EXT_STATUS_AMT_OFF:Ljava/lang/String; = "30"

.field public static final XAMT_EXT_STATUS_STOP:Ljava/lang/String; = "10"

.field public static final XAMT_EXT_STATUS_TRACKING:Ljava/lang/String; = "20"

.field public static final XAMT_GENERIC_AMT_OFF:Ljava/lang/String; = "1452"

.field public static final XAMT_GENERIC_CLIENT_ERROR:Ljava/lang/String; = "1400"

.field public static final XAMT_GENERIC_SUCCEEDED:Ljava/lang/String; = "1200"

.field public static final XAMT_GPS_LOCATION_TIME_OUT:I = 0x3a98

.field public static final XAMT_GPS_NETWORK_LOCATION_DELAY_TIME:I = 0x7d0

.field public static final XAMT_LAST_LOCATION_MODE_GPS:I = 0x1

.field public static final XAMT_LAST_LOCATION_MODE_NETWORK:I = 0x2

.field public static final XAMT_LAST_LOCATION_MODE_NONE:I = 0x0

.field public static final XAMT_LOCATION_INTENT_WAIT_TIME:I = 0x3a98

.field public static final XAMT_NETWORK_LOCATION_TIME_OUT:I = 0x2710

.field public static final XAMT_OPERATION_DONE:I = 0xa

.field public static final XAMT_OPERATION_GET_LOCATION:I = 0x1

.field public static final XAMT_OPERATION_NONE:I = 0x0

.field public static final XAMT_OPERATION_STOP:I = 0x3

.field public static final XAMT_OPERATION_TRACKING:I = 0x2

.field public static final XAMT_OPERATION_TRACKING_CONTINUE:I = 0x2

.field public static final XAMT_OPERATION_TRACKING_DONE:I = 0x0

.field public static final XAMT_OPERATION_TRACKING_OK:I = 0x1

.field public static final XAMT_START_LOCATION_ALL:I = 0x0

.field public static final XAMT_START_LOCATION_GPS:I = 0x1

.field public static final XAMT_START_LOCATION_NETWORK:I = 0x2
