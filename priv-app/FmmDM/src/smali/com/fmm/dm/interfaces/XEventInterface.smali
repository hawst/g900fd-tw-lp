.class public interface abstract Lcom/fmm/dm/interfaces/XEventInterface;
.super Ljava/lang/Object;
.source "XEventInterface.java"


# static fields
.field public static final XEVENT_ABORT_ALERT_DISPLAY:I = 0xf2

.field public static final XEVENT_ABORT_AUTH_FAIL:I = 0xf7

.field public static final XEVENT_ABORT_CONNECTION_FAIL:I = 0xfe

.field public static final XEVENT_ABORT_ERROR:I = 0xc0

.field public static final XEVENT_ABORT_HTTP_ERROR:I = 0xf9

.field public static final XEVENT_ABORT_INTERNAL_SERVER_ERROR:I = 0xf6

.field public static final XEVENT_ABORT_OBEX_ERROR:I = 0xf8

.field public static final XEVENT_ABORT_PARSING_FAIL:I = 0xff

.field public static final XEVENT_ABORT_RECEIVE_FAIL:I = 0xfc

.field public static final XEVENT_ABORT_SEND_FAIL:I = 0xfd

.field public static final XEVENT_ABORT_SYNCDM_ERROR:I = 0xf5

.field public static final XEVENT_ABORT_SYNC_AIRPLAIN_MODE:I = 0xf3

.field public static final XEVENT_ABORT_SYNC_RETRY:I = 0xf4

.field public static final XEVENT_ABORT_USB_DEACTIVATE:I = 0xfa

.field public static final XEVENT_ABORT_USER_REQ:I = 0xfb

.field public static final XEVENT_DM_ABORT:I = 0x19

.field public static final XEVENT_DM_BOOTSTRAP_RECEIVED:I = 0x27

.field public static final XEVENT_DM_CLEAR_SESSION:I = 0x63

.field public static final XEVENT_DM_CONNECT:I = 0xb

.field public static final XEVENT_DM_CONNECTFAIL:I = 0x15

.field public static final XEVENT_DM_CONNECTRETRY:I = 0x16

.field public static final XEVENT_DM_CONTINUE:I = 0x18

.field public static final XEVENT_DM_DDF_PARSER_ACTIVE:I = 0x3d

.field public static final XEVENT_DM_DDF_PARSER_PROCESS:I = 0x3e

.field public static final XEVENT_DM_FINISH:I = 0x1a

.field public static final XEVENT_DM_IDLE_STATE:I = 0xd

.field public static final XEVENT_DM_INIT:I = 0xc

.field public static final XEVENT_DM_MAX:I = 0x64

.field public static final XEVENT_DM_RECEIVEFAIL:I = 0x1c

.field public static final XEVENT_DM_SENDFAIL:I = 0x1b

.field public static final XEVENT_DM_SOCKET_CONNECTED:I = 0x23

.field public static final XEVENT_DM_SOCKET_DATA_RECEIVED:I = 0x25

.field public static final XEVENT_DM_SOCKET_DISCONNECTED:I = 0x24

.field public static final XEVENT_DM_SOCKET_SSL_TUNNEL_CONNECT:I = 0x26

.field public static final XEVENT_DM_START:I = 0x17

.field public static final XEVENT_DM_TCPIP_CLOSE:I = 0x22

.field public static final XEVENT_DM_TCPIP_OPEN:I = 0x20

.field public static final XEVENT_DM_TCPIP_SEND:I = 0x21

.field public static final XEVENT_NETWORK_STATUS_UPDATED:I = 0x2

.field public static final XEVENT_NOTI_BACKGROUND:I = 0x36

.field public static final XEVENT_NOTI_EXECUTE:I = 0x34

.field public static final XEVENT_NOTI_INFORMATIVE:I = 0x37

.field public static final XEVENT_NOTI_INTERACTIVE:I = 0x38

.field public static final XEVENT_NOTI_NOT_SPECIFIED:I = 0x35

.field public static final XEVENT_NOTI_RECEIVED:I = 0x33

.field public static final XEVENT_OS_INITIALIZED:I = 0x0

.field public static final XEVENT_PHONEBOOK_INITIALIZED:I = 0x1

.field public static final XEVENT_UIC_REQUEST:I = 0x65

.field public static final XEVENT_UIC_RESPONSE:I = 0x66

.field public static final XEVENT_WAP_PUSH_RECEIVE:I = 0x39
