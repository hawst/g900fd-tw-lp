.class public final enum Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;
.super Ljava/lang/Enum;
.source "XDMInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/interfaces/XDMInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "XDMProcessingState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_ADD:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_ALERT:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_COPY:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_DELETE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_GET:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_REPLACE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_RESULTS:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_STATUS:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum XDM_PROC_SYNCHDR:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 117
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_NONE"

    invoke-direct {v0, v1, v3}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 118
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_SYNCHDR"

    invoke-direct {v0, v1, v4}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_SYNCHDR:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 119
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_ALERT"

    invoke-direct {v0, v1, v5}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_ALERT:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 120
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_STATUS"

    invoke-direct {v0, v1, v6}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_STATUS:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 121
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_RESULTS"

    invoke-direct {v0, v1, v7}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_RESULTS:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 122
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_GET"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_GET:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 123
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_EXEC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 124
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_ADD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_ADD:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 125
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_REPLACE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_REPLACE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 126
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_DELETE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_DELETE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 127
    new-instance v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_COPY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_COPY:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    .line 115
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_SYNCHDR:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_ALERT:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_STATUS:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_RESULTS:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_GET:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_EXEC:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_ADD:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_REPLACE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_DELETE:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_COPY:Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->$VALUES:[Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 115
    const-class v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    return-object v0
.end method

.method public static values()[Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->$VALUES:[Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    invoke-virtual {v0}, [Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/fmm/dm/interfaces/XDMInterface$XDMProcessingState;

    return-object v0
.end method
