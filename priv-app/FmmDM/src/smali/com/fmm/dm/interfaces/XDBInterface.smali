.class public interface abstract Lcom/fmm/dm/interfaces/XDBInterface;
.super Ljava/lang/Object;
.source "XDBInterface.java"


# static fields
.field public static final E2P_SYNCML_SIM_IMSI:I = 0x78

.field public static final E2P_XAMT_GPS_ALTITUDE:I = 0x130

.field public static final E2P_XAMT_GPS_COURSE:I = 0x132

.field public static final E2P_XAMT_GPS_DATE:I = 0x135

.field public static final E2P_XAMT_GPS_HORIZONTAL_UNCERTANITY:I = 0x133

.field public static final E2P_XAMT_GPS_ISVALID:I = 0x12d

.field public static final E2P_XAMT_GPS_LATITUDE:I = 0x12e

.field public static final E2P_XAMT_GPS_LONGITUDE:I = 0x12f

.field public static final E2P_XAMT_GPS_SPEED:I = 0x131

.field public static final E2P_XAMT_GPS_VERTICAL_UNCERTANITY:I = 0x134

.field public static final E2P_XAMT_IDX:I = 0x12c

.field public static final E2P_XAMT_INFO:I = 0x12c

.field public static final E2P_XAMT_MAX:I = 0x144

.field public static final E2P_XAMT_NONGPS_CELLID:I = 0x137

.field public static final E2P_XAMT_NONGPS_ISVALID:I = 0x136

.field public static final E2P_XAMT_NONGPS_LAC:I = 0x138

.field public static final E2P_XAMT_NONGPS_MCC:I = 0x13a

.field public static final E2P_XAMT_NONGPS_MNC:I = 0x139

.field public static final E2P_XAMT_OPERATION:I = 0x13b

.field public static final E2P_XAMT_POLICY_ENDDATE:I = 0x141

.field public static final E2P_XAMT_POLICY_INTERVAL:I = 0x142

.field public static final E2P_XAMT_POLICY_STARTDATE:I = 0x140

.field public static final E2P_XAMT_RESULT_CODE:I = 0x13f

.field public static final E2P_XAMT_RESULT_REPORT_SOURCE_URI:I = 0x13d

.field public static final E2P_XAMT_RESULT_REPORT_TARGET_URI:I = 0x13e

.field public static final E2P_XAMT_RESULT_REPORT_TYPE:I = 0x13c

.field public static final E2P_XAMT_STATUS:I = 0x143

.field public static final E2P_XDM_ACCNODE_IDX:I = 0x64

.field public static final E2P_XDM_ACCNODE_MAX:I = 0x67

.field public static final E2P_XDM_ACCXNODE_INFO1:I = 0x64

.field public static final E2P_XDM_ACCXNODE_INFO2:I = 0x65

.field public static final E2P_XDM_ACCXNODE_INFO3:I = 0x66

.field public static final E2P_XDM_AGENT_IDX:I = 0x6e

.field public static final E2P_XDM_AGENT_INFO:I = 0x6e

.field public static final E2P_XDM_AGENT_INFO_AGENT_TYPE:I = 0x6f

.field public static final E2P_XDM_AGENT_MAX:I = 0x70

.field public static final E2P_XDM_APPID:I = 0x3b

.field public static final E2P_XDM_AUTHLEVEL:I = 0x3c

.field public static final E2P_XDM_AUTHTYPE:I = 0x44

.field public static final E2P_XDM_CHANGED_PROTOCOL:I = 0x4a

.field public static final E2P_XDM_CLIENT_NONCE:I = 0x41

.field public static final E2P_XDM_DESTORY_NOTIFICATION:I = 0x9

.field public static final E2P_XDM_INFO:I = 0x32

.field public static final E2P_XDM_INFO_CONREF:I = 0x48

.field public static final E2P_XDM_INFO_CON_BACKUP:I = 0x49

.field public static final E2P_XDM_INFO_IDX:I = 0x32

.field public static final E2P_XDM_INFO_MAX:I = 0x4b

.field public static final E2P_XDM_INFO_PROFILENAME:I = 0x47

.field public static final E2P_XDM_MAGIC:I = 0x33

.field public static final E2P_XDM_NETWORKCONNINDEX:I = 0x46

.field public static final E2P_XDM_NETWORKCONNNAME:I = 0x7

.field public static final E2P_XDM_NOTI_NOTI_RESYNC_MODE:I = 0xc

.field public static final E2P_XDM_NOTI_SAVED_INFO:I = 0xb

.field public static final E2P_XDM_OBEX:I = 0x35

.field public static final E2P_XDM_PASSWORD:I = 0x37

.field public static final E2P_XDM_PATH:I = 0x3a

.field public static final E2P_XDM_PREFCONREF:I = 0x3e

.field public static final E2P_XDM_PROFILE:I = 0x6

.field public static final E2P_XDM_PROFILENAME1:I = 0x3

.field public static final E2P_XDM_PROFILENAME2:I = 0x4

.field public static final E2P_XDM_PROFILENAME3:I = 0x5

.field public static final E2P_XDM_PROFILE_IDX:I = 0x0

.field public static final E2P_XDM_PROFILE_INDEX:I = 0x2

.field public static final E2P_XDM_PROFILE_INDEX_NOTI_EVT:I = 0xa

.field public static final E2P_XDM_PROFILE_MAGIC:I = 0x0

.field public static final E2P_XDM_PROFILE_MAX:I = 0xf

.field public static final E2P_XDM_PROTOCOL:I = 0x34

.field public static final E2P_XDM_PROXY_PROFILE_INDEX:I = 0x1

.field public static final E2P_XDM_RESYNC_IDX:I = 0x96

.field public static final E2P_XDM_RESYNC_MAX:I = 0x97

.field public static final E2P_XDM_RESYNC_MODE:I = 0x96

.field public static final E2P_XDM_SERVERAUTHLEVEL:I = 0x3d

.field public static final E2P_XDM_SERVERID:I = 0x3f

.field public static final E2P_XDM_SERVERIP:I = 0x39

.field public static final E2P_XDM_SERVERPASSWORD:I = 0x40

.field public static final E2P_XDM_SERVERPORT:I = 0x38

.field public static final E2P_XDM_SERVERURL:I = 0x43

.field public static final E2P_XDM_SERVER_AUTHTYPE:I = 0x45

.field public static final E2P_XDM_SERVER_NONCE:I = 0x42

.field public static final E2P_XDM_SESSIONID:I = 0x8

.field public static final E2P_XDM_SIM_IDX:I = 0x78

.field public static final E2P_XDM_SIM_MAX:I = 0x79

.field public static final E2P_XDM_UIC_RESULT_KEEP:I = 0xd

.field public static final E2P_XDM_UIC_RESULT_KEEP_FLAG:I = 0xe

.field public static final E2P_XDM_USERNAME:I = 0x36

.field public static final E2P_XLAWMO_AMT_STATUS:I = 0xd6

.field public static final E2P_XLAWMO_CALLRESTRICTION_PHONENUMBER:I = 0xdc

.field public static final E2P_XLAWMO_CALLRESTRICTION_STATUS:I = 0xdd

.field public static final E2P_XLAWMO_CORRELATOR:I = 0xe0

.field public static final E2P_XLAWMO_EXTERNALMEM_LISTITEMNAME:I = 0xcc

.field public static final E2P_XLAWMO_EXTERNALMEM_TOBEWIPED:I = 0xcd

.field public static final E2P_XLAWMO_EXTOPERATION:I = 0xd0

.field public static final E2P_XLAWMO_IDX:I = 0xc8

.field public static final E2P_XLAWMO_INFO:I = 0xc8

.field public static final E2P_XLAWMO_INTERNALMEM_LISTITEMNAME:I = 0xca

.field public static final E2P_XLAWMO_INTERNALMEM_TOBEWIPED:I = 0xcb

.field public static final E2P_XLAWMO_LOCKMYPHONE_MESSAGES:I = 0xda

.field public static final E2P_XLAWMO_MAX:I = 0xe1

.field public static final E2P_XLAWMO_NOTIFYUSER:I = 0xce

.field public static final E2P_XLAWMO_OPERATION:I = 0xcf

.field public static final E2P_XLAWMO_PASSWORD:I = 0xd9

.field public static final E2P_XLAWMO_PIN:I = 0xd1

.field public static final E2P_XLAWMO_REACTIVATION_LOCK:I = 0xdb

.field public static final E2P_XLAWMO_REGISTRATION:I = 0xd7

.field public static final E2P_XLAWMO_RESULT_CODE:I = 0xd5

.field public static final E2P_XLAWMO_RESULT_REPORT_SOURCE_URI:I = 0xd3

.field public static final E2P_XLAWMO_RESULT_REPORT_TARGET_URI:I = 0xd4

.field public static final E2P_XLAWMO_RESULT_REPORT_TYPE:I = 0xd2

.field public static final E2P_XLAWMO_RINGMYPHONE_MESSAGES:I = 0xdf

.field public static final E2P_XLAWMO_RINGMYPHONE_STATUS:I = 0xde

.field public static final E2P_XLAWMO_SIMCHANGESTATE:I = 0xd8

.field public static final E2P_XLAWMO_STATE:I = 0xc9

.field public static final E2P_XNOTI_APPID:I = 0xfb

.field public static final E2P_XNOTI_IDX:I = 0xfa

.field public static final E2P_XNOTI_INFO:I = 0xfa

.field public static final E2P_XNOTI_MAX:I = 0xff

.field public static final E2P_XNOTI_SERVERID:I = 0xfe

.field public static final E2P_XNOTI_SESSIONID:I = 0xfd

.field public static final E2P_XNOTI_UIMODE:I = 0xfc

.field public static final XDB_FILE_APPEND:I = 0x10

.field public static final XDB_FILE_CREATE:I = 0x1

.field public static final XDB_FILE_READ:I = 0x2

.field public static final XDB_FILE_SEEK_CUR:I = 0x1

.field public static final XDB_FILE_SEEK_END:I = 0x2

.field public static final XDB_FILE_SEEK_SET:I = 0x0

.field public static final XDB_FILE_TRUNCATE:I = 0x8

.field public static final XDB_FILE_WRITE:I = 0x4

.field public static final XDB_FS_ERR_BAD_PARAM:I = 0x2

.field public static final XDB_FS_ERR_FILE_NOT_FOUND:I = 0x3

.field public static final XDB_FS_ERR_NO_MEM_READY:I = 0x4

.field public static final XDB_FS_FAIL:I = 0x5

.field public static final XDB_FS_OK:I
