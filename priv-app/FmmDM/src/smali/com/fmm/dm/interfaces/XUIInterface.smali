.class public interface abstract Lcom/fmm/dm/interfaces/XUIInterface;
.super Ljava/lang/Object;
.source "XUIInterface.java"


# static fields
.field public static final XUI_DM_ABORT_BYUSER:I = 0x84

.field public static final XUI_DM_CONNECT:I = 0x66

.field public static final XUI_DM_CONNECTING:I = 0x67

.field public static final XUI_DM_CONNECT_FAIL:I = 0x6f

.field public static final XUI_DM_FINISH:I = 0xab

.field public static final XUI_DM_HTTP_INTERNAL_ERROR:I = 0x87

.field public static final XUI_DM_IDLE_STATE:I = 0x92

.field public static final XUI_DM_INVALID_CREDENTIALS:I = 0x7b

.field public static final XUI_DM_IN_PROGRESS:I = 0x79

.field public static final XUI_DM_MISSING_CREDENTIALS:I = 0x7c

.field public static final XUI_DM_NETWORK_ERR:I = 0x83

.field public static final XUI_DM_NONE:I = 0x0

.field public static final XUI_DM_NOTI_BACKGROUND:I = 0x8e

.field public static final XUI_DM_NOTI_INFORMATIVE:I = 0x8f

.field public static final XUI_DM_NOTI_INTERACTIVE:I = 0x90

.field public static final XUI_DM_NOTI_NOT_SPECIFIED:I = 0x8d

.field public static final XUI_DM_NOTI_RESUME_MAX:I = 0x91

.field public static final XUI_DM_NOT_INIT:I = 0x69

.field public static final XUI_DM_PAYMENT_REQIRED:I = 0x7d

.field public static final XUI_DM_RECV_FAIL:I = 0x85

.field public static final XUI_DM_SEND_FAIL:I = 0x86

.field public static final XUI_DM_SERVER_BUSY:I = 0x7a

.field public static final XUI_DM_SYNC_ERROR:I = 0x88

.field public static final XUI_DM_SYNC_START:I = 0x65

.field public static final XUI_DM_UIC_REQUEST:I = 0x68

.field public static final XUI_INDICATOR_ALL_OFF:I = 0x5

.field public static final XUI_INDICATOR_EMERGENCY_MODE:I = 0x4

.field public static final XUI_INDICATOR_NONE:I = 0x0

.field public static final XUI_INDICATOR_OFF:I = 0x1

.field public static final XUI_INDICATOR_PCW_FIND_MY_MOBILE:I = 0x3

.field public static final XUI_INDICATOR_SYNC_DM:I = 0x2
