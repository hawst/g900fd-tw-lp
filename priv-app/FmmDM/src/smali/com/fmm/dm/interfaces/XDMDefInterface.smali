.class public interface abstract Lcom/fmm/dm/interfaces/XDMDefInterface;
.super Ljava/lang/Object;
.source "XDMDefInterface.java"


# static fields
.field public static final XDM_AMT:Z = true

.field public static final XDM_DUAL_DM:Z = true

.field public static final XDM_FULL_FIRMWARE_INFORMATION:Z = true

.field public static final XDM_LAWMO:Z = true

.field public static final XDM_LAWMO_EXTEND_SERVICE:Z = true

.field public static final XDM_LAWMO_PCW_SECOND:Z = true

.field public static final XDM_NONE_PROXY:Z = false

.field public static final XDM_OPERATOR_CTC:Ljava/lang/String; = "CTC"

.field public static final XDM_OPERATOR_KOR:Ljava/lang/String; = "KOR"

.field public static final XDM_OPERATOR_KOR_KT:Ljava/lang/String; = "KT"

.field public static final XDM_OPERATOR_KOR_LG:Ljava/lang/String; = "LG"

.field public static final XDM_OPERATOR_KOR_LTELG:Ljava/lang/String; = "LTELG"

.field public static final XDM_OPERATOR_KOR_SKT:Ljava/lang/String; = "SKT"

.field public static final XDM_SIMSLOT_1:I = 0x0

.field public static final XDM_SIMSLOT_2:I = 0x1

.field public static final XDM_SIMULATOR_TEST:Z = false

.field public static final XDM_UNLOCK_ALERT:Z = true

.field public static final XDM_VERSION_V11:Z = false

.field public static final XDM_VERSION_V12:Z = true

.field public static final XDM_VERSION_V12_NONCE_RESYNC:Z
