.class public interface abstract Lcom/fmm/dm/interfaces/XLAWMOInterface;
.super Ljava/lang/Object;
.source "XLAWMOInterface.java"


# static fields
.field public static final OSPS_CONNCHECK_BATTERY_LEVEL_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/Battery/Level"

.field public static final OSPS_CONNCHECK_BATTERY_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/Battery"

.field public static final OSPS_CONNCHECK_CALLCOUNT_INCOMINGCALL_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/CallCount/IncomingCall"

.field public static final OSPS_CONNCHECK_CALLCOUNT_MISSEDCALL_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/CallCount/MissedCall"

.field public static final OSPS_CONNCHECK_CALLCOUNT_OUTGOINGCALL_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/CallCount/OutgoingCall"

.field public static final OSPS_CONNCHECK_CALLCOUNT_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/CallCount"

.field public static final OSPS_CONNCHECK_MSGCOUNT_INCOMINGMMS_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/MsgCount/IncomingMMS"

.field public static final OSPS_CONNCHECK_MSGCOUNT_INCOMINGSMS_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/MsgCount/IncomingSMS"

.field public static final OSPS_CONNCHECK_MSGCOUNT_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/MsgCount"

.field public static final OSPS_CONNCHECK_OPERATIONS_ACTIVITY_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/Operations/Activity"

.field public static final OSPS_CONNCHECK_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/Operations"

.field public static final OSPS_CONNCHECK_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck"

.field public static final OSPS_CONNCHECK_POLICY_DEFAULT_PERIOD:Ljava/lang/String; = "3600"

.field public static final OSPS_CONNCHECK_POLICY_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/Policy"

.field public static final OSPS_CONNCHECK_POLICY_PERIOD_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/Policy/Period"

.field public static final OSPS_CONNCHECK_REACTIVATIONLOCK_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/ReactivationLock"

.field public static final OSPS_CONNCHECK_REACTIVATIONLOCK_STATE_PATH:Ljava/lang/String; = "./Ext/OSPS/ConnectionCheck/ReactivationLock/State"

.field public static final OSPS_EMERGENCYMODE_DATE_PATH:Ljava/lang/String; = "./Ext/OSPS/EmergencyMode/Date"

.field public static final OSPS_EMERGENCYMODE_OPERATIONS_DIVERTREQUEST_PATH:Ljava/lang/String; = "./Ext/OSPS/EmergencyMode/Operations/DivertRequest"

.field public static final OSPS_EMERGENCYMODE_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/EmergencyMode/Operations"

.field public static final OSPS_EMERGENCYMODE_PATH:Ljava/lang/String; = "./Ext/OSPS/EmergencyMode"

.field public static final OSPS_EMERGENCYMODE_REQUESTOR_NAME_PATH:Ljava/lang/String; = "./Ext/OSPS/EmergencyMode/RequestorName"

.field public static final OSPS_EMERGENCYMODE_STATE_PATH:Ljava/lang/String; = "./Ext/OSPS/EmergencyMode/State"

.field public static final OSPS_EXTENTION_PATH:Ljava/lang/String; = "./Ext"

.field public static final OSPS_FORWARDING_NUMBERS_CALLFORWARDING_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/Numbers/CallForwarding"

.field public static final OSPS_FORWARDING_NUMBERS_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/Numbers"

.field public static final OSPS_FORWARDING_NUMBERS_SMSFORWARDING_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/Numbers/SMSForwarding"

.field public static final OSPS_FORWARDING_OPERATIONS_DIVERTREQUEST_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/Operations/DivertRequest"

.field public static final OSPS_FORWARDING_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/Operations"

.field public static final OSPS_FORWARDING_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding"

.field public static final OSPS_FORWARDING_TOBEDISABLED_CALLFORWARDING_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/ToBeDisabled/CallForwarding"

.field public static final OSPS_FORWARDING_TOBEDISABLED_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/ToBeDisabled"

.field public static final OSPS_FORWARDING_TOBEDISABLED_SMSFORWARDING_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/ToBeDisabled/SMSForwarding"

.field public static final OSPS_FORWARDING_TOBEENABLED_CALLFORWARDING_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/ToBeEnabled/CallForwarding"

.field public static final OSPS_FORWARDING_TOBEENABLED_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/ToBeEnabled"

.field public static final OSPS_FORWARDING_TOBEENABLED_SMSFORWARDING_PATH:Ljava/lang/String; = "./Ext/OSPS/Forwarding/ToBeEnabled/SMSForwarding"

.field public static final OSPS_LAWMO_CONFIG_NOTIUSER_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/LAWMOConfig/NotifyUser"

.field public static final OSPS_LAWMO_CONFIG_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/LAWMOConfig"

.field public static final OSPS_LAWMO_EXT_AMT_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/AMT"

.field public static final OSPS_LAWMO_EXT_AMT_STATUS_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/AMT/Status"

.field public static final OSPS_LAWMO_EXT_CALLRESTRICTION_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/Operations"

.field public static final OSPS_LAWMO_EXT_CALLRESTRICTION_OPERATIONS_RESTRICTION_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/Operations/Restriction"

.field public static final OSPS_LAWMO_EXT_CALLRESTRICTION_OPERATIONS_STOP_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/Operations/Stop"

.field public static final OSPS_LAWMO_EXT_CALLRESTRICTION_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction"

.field public static final OSPS_LAWMO_EXT_CALLRESTRICTION_PHONENUMBER_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/PhoneNumber"

.field public static final OSPS_LAWMO_EXT_CALLRESTRICTION_STATUS_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/CallRestriction/Status"

.field public static final OSPS_LAWMO_EXT_LOCKMYPHONE_MESSAGES_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone/Messages"

.field public static final OSPS_LAWMO_EXT_LOCKMYPHONE_OPERATIONS_CHANGEMSG_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone/Operations/ChangeMsg"

.field public static final OSPS_LAWMO_EXT_LOCKMYPHONE_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone/Operations"

.field public static final OSPS_LAWMO_EXT_LOCKMYPHONE_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/LockMyPhone"

.field public static final OSPS_LAWMO_EXT_LOCKMYPHONE_REACTIVATIONLOCK_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/ReactivationLock"

.field public static final OSPS_LAWMO_EXT_PASSWORD_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/Password"

.field public static final OSPS_LAWMO_EXT_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext"

.field public static final OSPS_LAWMO_EXT_RINGMYPHONE_MESSAGES_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Messages"

.field public static final OSPS_LAWMO_EXT_RINGMYPHONE_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Operations"

.field public static final OSPS_LAWMO_EXT_RINGMYPHONE_OPERATIONS_START_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Operations/Start"

.field public static final OSPS_LAWMO_EXT_RINGMYPHONE_OPERATIONS_STOP_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Operations/Stop"

.field public static final OSPS_LAWMO_EXT_RINGMYPHONE_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone"

.field public static final OSPS_LAWMO_EXT_RINGMYPHONE_STATUS_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Ext/RingMyPhone/Status"

.field public static final OSPS_LAWMO_OPERATIONS_FACTORYRESET_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Operations/FactoryReset"

.field public static final OSPS_LAWMO_OPERATIONS_FULLYLOCK_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Operations/FullyLock"

.field public static final OSPS_LAWMO_OPERATIONS_PARTIALLYLOCK_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Operations/PartiallyLock"

.field public static final OSPS_LAWMO_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Operations"

.field public static final OSPS_LAWMO_OPERATIONS_UNLOCK_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Operations/UnLock"

.field public static final OSPS_LAWMO_OPERATIONS_WIPE_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/Operations/Wipe"

.field public static final OSPS_LAWMO_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP"

.field public static final OSPS_LAWMO_ROOT_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO"

.field public static final OSPS_LAWMO_STATE_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/State"

.field public static final OSPS_LAWMO_WIPELIST_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/AvailableWipeList"

.field public static final OSPS_LAWMO_WIPE_WIPELIST_EXTERNAL_LISTITEMNAME_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/External/ListItemName"

.field public static final OSPS_LAWMO_WIPE_WIPELIST_EXTERNAL_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/External"

.field public static final OSPS_LAWMO_WIPE_WIPELIST_EXTERNAL_TOBEWIPED_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/External/ToBeWiped"

.field public static final OSPS_LAWMO_WIPE_WIPELIST_SIM_LISTITEMNAME_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/SIM/ListItemName"

.field public static final OSPS_LAWMO_WIPE_WIPELIST_SIM_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/SIM"

.field public static final OSPS_LAWMO_WIPE_WIPELIST_SIM_TOBEWIPED_PATH:Ljava/lang/String; = "./Ext/OSPS/LAWMO/OSP/AvailableWipeList/SIM/ToBeWiped"

.field public static final OSPS_MASTERKEY_OPERATIONS_LOCKRELEASE_PATH:Ljava/lang/String; = "./Ext/OSPS/MasterKey/Operations/LockRelease"

.field public static final OSPS_MASTERKEY_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/MasterKey/Operations"

.field public static final OSPS_MASTERKEY_PATH:Ljava/lang/String; = "./Ext/OSPS/MasterKey"

.field public static final OSPS_REACTIVATIONLOCK_OPERATIONS_DIVERTREQUEST_PATH:Ljava/lang/String; = "./Ext/OSPS/ReactivationLock/Operations/DivertRequest"

.field public static final OSPS_REACTIVATIONLOCK_OPERATIONS_PATH:Ljava/lang/String; = "./Ext/OSPS/ReactivationLock/Operations"

.field public static final OSPS_REACTIVATIONLOCK_PATH:Ljava/lang/String; = "./Ext/OSPS/ReactivationLock"

.field public static final OSPS_REACTIVATIONLOCK_STATE_PATH:Ljava/lang/String; = "./Ext/OSPS/ReactivationLock/State"

.field public static final OSPS_ROOT_PATH:Ljava/lang/String; = "./Ext/OSPS"

.field public static final XLAWMO_ALERT_TYPE_RESULT_REPORT:Ljava/lang/String; = "urn:oma:at:lawmo:1.0:OperationComplete"

.field public static final XLAWMO_ALERT_TYPE_USER_ALERT:Ljava/lang/String; = "urn:oma:at:lawmo:1.0:userrequest"

.field public static final XLAWMO_AVAILABLEWIPELIST_PATH:Ljava/lang/String; = "/AvailableWipeList"

.field public static final XLAWMO_CALLRESTRICTION_OPERATIONS_RESTRICTION:I = 0x8

.field public static final XLAWMO_CALLRESTRICTION_OPERATIONS_STOP:I = 0x9

.field public static final XLAWMO_CALLRESTRICTION_PHONENUMBER:I = 0x4

.field public static final XLAWMO_CALLRESTRICTION_RESTRICTION_PATH:Ljava/lang/String; = "/CallRestriction/Operations/Restriction"

.field public static final XLAWMO_CALLRESTRICTION_STOP_PATH:Ljava/lang/String; = "/CallRestriction/Operations/Stop"

.field public static final XLAWMO_CONNCHECK_FAIL_RESULT:Ljava/lang/String; = "CHECKFAIL"

.field public static final XLAWMO_EMERGENCYMODE_DISABLED:I = 0x5

.field public static final XLAWMO_EMERGENCYMODE_DISABLING:I = 0x4

.field public static final XLAWMO_EMERGENCYMODE_ENABLED:I = 0x3

.field public static final XLAWMO_EMERGENCYMODE_ENABLING:I = 0x2

.field public static final XLAWMO_EMERGENCYMODE_ERR_INVALID_STATE:I = -0x4

.field public static final XLAWMO_EMERGENCYMODE_ERR_INVALID_TYPE:I = -0x3

.field public static final XLAWMO_EMERGENCYMODE_ERR_MODIFYING:I = -0x2

.field public static final XLAWMO_EMERGENCYMODE_ERR_NOT_FOUND_SERVICE:I = -0x5

.field public static final XLAWMO_EMERGENCYMODE_ERR_NULL_CONTEXT:I = -0x7

.field public static final XLAWMO_EMERGENCYMODE_ERR_PERMISSION_DENIED:I = -0x6

.field public static final XLAWMO_EMERGENCYMODE_ERR_UNKNOWN_FAIL:I = -0x9

.field public static final XLAWMO_EMERGENCYMODE_ERR_USER_CANCELED:I = -0x8

.field public static final XLAWMO_EMERGENCYMODE_FAIL:I = -0x1

.field public static final XLAWMO_EMERGENCYMODE_SAME_OFF:I = 0x65

.field public static final XLAWMO_EMERGENCYMODE_SAME_ON:I = 0x64

.field public static final XLAWMO_EMERGENCYMODE_SUCCESS:I = 0x1

.field public static final XLAWMO_ETC:I = 0x8

.field public static final XLAWMO_EXTERNALMEM_PATH:Ljava/lang/String; = "/ExternalMem"

.field public static final XLAWMO_EXT_PATH:Ljava/lang/String; = "/Ext"

.field public static final XLAWMO_EXT_STATUS_AMT_OFF:Ljava/lang/String; = "30"

.field public static final XLAWMO_EXT_STATUS_START_OR_RESTRICTION:Ljava/lang/String; = "20"

.field public static final XLAWMO_EXT_STATUS_STOP:Ljava/lang/String; = "10"

.field public static final XLAWMO_FACTORYRESET_PATH:Ljava/lang/String; = "/FactoryReset"

.field public static final XLAWMO_FORWARDING_OPERATIONS_DIVERT:I = 0xb

.field public static final XLAWMO_FULLYLOCK_PATH:Ljava/lang/String; = "/FullyLock"

.field public static final XLAWMO_GENERIC_ALREADY_CHECK_REACTIVATIONLOCK:Ljava/lang/String; = "1476"

.field public static final XLAWMO_GENERIC_CLIENT_ERROR:Ljava/lang/String; = "1400"

.field public static final XLAWMO_GENERIC_CONNCHECK_FAIL:Ljava/lang/String; = "1454"

.field public static final XLAWMO_GENERIC_EMERGENCYMODE_FAIL:Ljava/lang/String; = "1490"

.field public static final XLAWMO_GENERIC_EMERGENCYMODE_SAME_OFF:Ljava/lang/String; = "1493"

.field public static final XLAWMO_GENERIC_EMERGENCYMODE_SAME_ON:Ljava/lang/String; = "1492"

.field public static final XLAWMO_GENERIC_EMERGENCYMODE_USERCANCEL:Ljava/lang/String; = "1491"

.field public static final XLAWMO_GENERIC_FORWARDING_ALL_FAIL:Ljava/lang/String; = "1470"

.field public static final XLAWMO_GENERIC_FORWARDING_CALL_FAIL:Ljava/lang/String; = "1471"

.field public static final XLAWMO_GENERIC_FORWARDING_SMS_FAIL:Ljava/lang/String; = "1472"

.field public static final XLAWMO_GENERIC_FULLY_LOCK_DEVICE_FAILED:Ljava/lang/String; = "1402"

.field public static final XLAWMO_GENERIC_MASTERKEY_SCREEN_OFF:Ljava/lang/String; = "1453"

.field public static final XLAWMO_GENERIC_PARTIALLY_LOCK_DEVICE_FAILED:Ljava/lang/String; = "1403"

.field public static final XLAWMO_GENERIC_REACTIVATIONLOCK_FAIL:Ljava/lang/String; = "1475"

.field public static final XLAWMO_GENERIC_SUCCEEDED:Ljava/lang/String; = "1200"

.field public static final XLAWMO_GENERIC_UNLOCK_DEVICE_FAILED:Ljava/lang/String; = "1404"

.field public static final XLAWMO_GENERIC_USER_CANCELLED:Ljava/lang/String; = "1401"

.field public static final XLAWMO_GENERIC_VENDOR_SUCCESSFUL:Ljava/lang/String; = "1250"

.field public static final XLAWMO_GENERIC_WIPE_ALL_FAIL:Ljava/lang/String; = "1460"

.field public static final XLAWMO_GENERIC_WIPE_DEVICE_DATA_FAILED:Ljava/lang/String; = "1405"

.field public static final XLAWMO_GENERIC_WIPE_EXTERNAL_FAIL:Ljava/lang/String; = "1461"

.field public static final XLAWMO_GENERIC_WIPE_NOT_PERFORMED:Ljava/lang/String; = "1406"

.field public static final XLAWMO_GENERIC_WIPE_SIM_FAIL:Ljava/lang/String; = "1462"

.field public static final XLAWMO_GENERIC_WIPE_SUCCEEDED_BOTH_DATA_AND_LISTF:Ljava/lang/String; = "1202"

.field public static final XLAWMO_GENERIC_WIPE_SUCCEEDED_WITH_DATA:Ljava/lang/String; = "1201"

.field public static final XLAWMO_INTERNALMEM_PATH:Ljava/lang/String; = "/InternalMem"

.field public static final XLAWMO_LAWMOCONFIG_PATH:Ljava/lang/String; = "/LAWMOConfig"

.field public static final XLAWMO_LISTITEMNAME_PATH:Ljava/lang/String; = "/ListItemName"

.field public static final XLAWMO_LOCKMYPHONE_CHANGEMSG_PATH:Ljava/lang/String; = "/LockMyPhone/Operations/ChangeMsg"

.field public static final XLAWMO_LOCKMYPHONE_MESSAGES:I = 0x1

.field public static final XLAWMO_LOCKMYPHONE_OPERATION_CHANGEMSG:I = 0x7

.field public static final XLAWMO_MASTERKEY_OPERATIONS_LOCKRELEASE:I = 0xc

.field public static final XLAWMO_NOTIFYUSER_PATH:Ljava/lang/String; = "/NotifyUser"

.field public static final XLAWMO_OPERATIONS_PATH:Ljava/lang/String; = "/Operations"

.field public static final XLAWMO_OPERATION_CONNECTION_CHECK:I = 0xd

.field public static final XLAWMO_OPERATION_DONE:I = 0x10

.field public static final XLAWMO_OPERATION_EMERGENCY:I = 0xe

.field public static final XLAWMO_OPERATION_FACTORYRESET:I = 0x5

.field public static final XLAWMO_OPERATION_FULLYLOCK:I = 0x1

.field public static final XLAWMO_OPERATION_NONE:I = 0x0

.field public static final XLAWMO_OPERATION_PARTIALLYLOCK:I = 0x2

.field public static final XLAWMO_OPERATION_REACTIVATIONLOCK:I = 0xf

.field public static final XLAWMO_OPERATION_RESET:I = 0x6

.field public static final XLAWMO_OPERATION_UNLOCK:I = 0x3

.field public static final XLAWMO_OPERATION_WIPE:I = 0x4

.field public static final XLAWMO_PARTIALLYLOCK_PATH:Ljava/lang/String; = "/PartiallyLock"

.field public static final XLAWMO_PASSWORD:I = 0x2

.field public static final XLAWMO_PATH:Ljava/lang/String; = "/LAWMO"

.field public static final XLAWMO_PIN_PATH:Ljava/lang/String; = "/Pin"

.field public static final XLAWMO_REACTIVATIONLOCK_STATE_DEFAULT:I = -0x2

.field public static final XLAWMO_REACTIVATIONLOCK_STATE_OFF:I = 0x0

.field public static final XLAWMO_REACTIVATIONLOCK_STATE_ON:I = 0x1

.field public static final XLAWMO_REACTIVELOCK_INTENT_WAIT_TIME:I = 0x7530

.field public static final XLAWMO_RESET_PATH:Ljava/lang/String; = "/Reset"

.field public static final XLAWMO_RINGMYPHONE_OPERATIONS_START:I = 0xa

.field public static final XLAWMO_RINGMYPHONE_START_PATH:Ljava/lang/String; = "/RingMyPhone/Operations/Start"

.field public static final XLAWMO_SIM_CHANGE_DATE_PATH:Ljava/lang/String; = "/date"

.field public static final XLAWMO_SIM_CHANGE_DEVINFO_PATH:Ljava/lang/String; = "./Ext/OSPS/devInfo"

.field public static final XLAWMO_SIM_CHANGE_EXT_PATH:Ljava/lang/String; = "/Ext"

.field public static final XLAWMO_SIM_CHANGE_MCC_PATH:Ljava/lang/String; = "/MCC"

.field public static final XLAWMO_SIM_CHANGE_MNC_PATH:Ljava/lang/String; = "/MNC"

.field public static final XLAWMO_SIM_CHANGE_MSISDN_PATH:Ljava/lang/String; = "/MSISDN"

.field public static final XLAWMO_SIM_CHANGE_SIM_PATH:Ljava/lang/String; = "./Ext/OSPS/devInfo/SIM"

.field public static final XLAWMO_STATE_FULLYLOCKED:Ljava/lang/Integer;

.field public static final XLAWMO_STATE_PARTIALLYLOCKED:Ljava/lang/Integer;

.field public static final XLAWMO_STATE_PATH:Ljava/lang/String; = "/State"

.field public static final XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

.field public static final XLAWMO_STATUS_AMT_OFF:Ljava/lang/Integer;

.field public static final XLAWMO_STATUS_AMT_ON:Ljava/lang/Integer;

.field public static final XLAWMO_TOBEWIPED_PATH:Ljava/lang/String; = "/ToBeWiped"

.field public static final XLAWMO_UNLOCK_ALERT_PATH:Ljava/lang/String; = "./Ext/OSPS/Unlock"

.field public static final XLAWMO_UNLOCK_PATH:Ljava/lang/String; = "/UnLock"

.field public static final XLAWMO_WIPE_PATH:Ljava/lang/String; = "/Wipe"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/interfaces/XLAWMOInterface;->XLAWMO_STATE_FULLYLOCKED:Ljava/lang/Integer;

    .line 117
    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/interfaces/XLAWMOInterface;->XLAWMO_STATE_PARTIALLYLOCKED:Ljava/lang/Integer;

    .line 118
    const/16 v0, 0x1e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/interfaces/XLAWMOInterface;->XLAWMO_STATE_UNLOCKED:Ljava/lang/Integer;

    .line 121
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/interfaces/XLAWMOInterface;->XLAWMO_STATUS_AMT_OFF:Ljava/lang/Integer;

    .line 122
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/fmm/dm/interfaces/XLAWMOInterface;->XLAWMO_STATUS_AMT_ON:Ljava/lang/Integer;

    return-void
.end method
