.class public Lcom/fmm/dm/XDMUnlockBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "XDMUnlockBroadcastReceiver.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XCommonInterface;
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XLAWMOInterface;
.implements Lcom/fmm/dm/interfaces/XNOTIInterface;


# static fields
.field public static final XCOMMON_INTENT_DATA_ROAMING:Ljava/lang/String; = "android.intent.action.DATA_ROAMING"

.field public static final XCOMMON_INTENT_FMMDM_ADMIN_SETTING:Ljava/lang/String; = "android.intent.action.FMMDM_ADMIN_SETTING"

.field public static final XCOMMON_INTENT_GET_IP_PUSH_NOTI_RECEIVED:Ljava/lang/String; = "android.intent.action.IP_PUSH_DM_NOTI_RECEIVED"

.field public static final XCOMMON_INTENT_NOTI_CANCELED:Ljava/lang/String; = "android.intent.action.NOTI_CANCELED"

.field public static final XCOMMON_INTENT_SIM_STATE_CHANGE:Ljava/lang/String; = "android.intent.action.SIM_STATE_CHANGED"

.field public static final XCOMMON_INTENT_UNLOCK_ALERT:Ljava/lang/String; = "android.intent.action.UNLOCK_FFM_ALERT"

.field public static final XCOMMON_INTENT_UPDATE_DEVICEID:Ljava/lang/String; = "android.intent.action.pcw.UPDATE_DEVICEID"

.field private static m_InitResumeIntent:Landroid/content/Intent;

.field private static m_InitResumeResult:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static xdmBroadCastNotInitHandle(I)V
    .locals 1
    .param p0, "nResumeIntent"    # I

    .prologue
    .line 161
    const-string v0, "DM Not Init"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 162
    invoke-static {p0}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentSetNotiResumeCase(I)V

    .line 163
    return-void
.end method

.method public static xdmBroadcastGetInitIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/fmm/dm/XDMUnlockBroadcastReceiver;->m_InitResumeIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public static xdmBroadcastGetInitResumeResult()I
    .locals 1

    .prologue
    .line 131
    sget v0, Lcom/fmm/dm/XDMUnlockBroadcastReceiver;->m_InitResumeResult:I

    return v0
.end method

.method private xdmBroadcastIPPushReceived(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 91
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NotiResumeCase : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentGetNotiResumeCase()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 93
    invoke-static {}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentGetNotiResumeCase()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentGetNotiResumeCase()I

    move-result v1

    if-nez v1, :cond_0

    .line 97
    const-string v1, "pdus"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 99
    .local v0, "inbox":[B
    if-nez v0, :cond_1

    .line 101
    const-string v1, "PushData is null."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 127
    .end local v0    # "inbox":[B
    :cond_0
    :goto_0
    return-void

    .line 105
    .restart local v0    # "inbox":[B
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 106
    invoke-static {v0}, Lcom/fmm/dm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v0

    .line 107
    if-eqz v0, :cond_0

    .line 109
    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiAddPushDataQueue(I[B)V

    .line 111
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_2

    .line 113
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/dm/XDMUnlockBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    goto :goto_0

    .line 117
    :cond_2
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiGetNotiProcessing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 119
    const-string v1, "Noti Processing..."

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_3
    invoke-static {}, Lcom/fmm/dm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto :goto_0
.end method

.method public static xdmBroadcastSetInitIntent(Landroid/content/Intent;)V
    .locals 0
    .param p0, "mIntent"    # Landroid/content/Intent;

    .prologue
    .line 141
    sput-object p0, Lcom/fmm/dm/XDMUnlockBroadcastReceiver;->m_InitResumeIntent:Landroid/content/Intent;

    .line 142
    return-void
.end method

.method public static xdmBroadcastSetInitResumeResult(I)V
    .locals 0
    .param p0, "nResumeResult"    # I

    .prologue
    .line 136
    sput p0, Lcom/fmm/dm/XDMUnlockBroadcastReceiver;->m_InitResumeResult:I

    .line 137
    return-void
.end method

.method public static xdmBroadcastSetUpdateURL(Landroid/content/Intent;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 151
    if-nez p0, :cond_0

    .line 153
    sget-object p0, Lcom/fmm/dm/XDMUnlockBroadcastReceiver;->m_InitResumeIntent:Landroid/content/Intent;

    .line 155
    :cond_0
    const-string v1, "DMServer"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "szUrl":Ljava/lang/String;
    invoke-static {v0}, Lcom/fmm/dm/db/file/XDB;->xdbChangeLawmoServerInfo(Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method private xdmBroadcastSimChangeAlertCheck()V
    .locals 3

    .prologue
    .line 78
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetDmAgentType()I

    move-result v0

    .line 79
    .local v0, "nAgentType":I
    invoke-static {}, Lcom/fmm/dm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    .line 80
    .local v1, "nNetworkState":I
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 82
    :cond_0
    const-string v2, "cannot process during"

    invoke-static {v2}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 87
    :goto_0
    return-void

    .line 86
    :cond_1
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiSimChangeAlertReport()Z

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 50
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.pcw.UPDATE_DEVICEID"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.IP_PUSH_DM_NOTI_RECEIVED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    :cond_0
    sget-boolean v1, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    if-nez v1, :cond_1

    invoke-static {}, Lcom/fmm/dm/db/file/XDBLawmo;->xdbLawmoGetPCWDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BroadCast Action Name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XDMApplicationStartFlag : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 56
    const/4 v1, 0x1

    sput-boolean v1, Lcom/fmm/dm/XDMApplication;->g_ServiceStartSte:Z

    .line 57
    const-string v1, "FMMDM SERVICE START Set!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 58
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/fmm/dm/XDMService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 63
    .end local v0    # "cn":Landroid/content/ComponentName;
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.UNLOCK_FFM_ALERT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 65
    const-string v1, "android.intent.action.UNLOCK_FFM_ALERT"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 66
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_3

    .line 68
    const-string v1, "!XDMTask.g_IsDMInitialized"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 69
    const/16 v1, 0x8

    invoke-static {v1}, Lcom/fmm/dm/XDMUnlockBroadcastReceiver;->xdmBroadCastNotInitHandle(I)V

    .line 74
    :cond_2
    :goto_0
    return-void

    .line 72
    :cond_3
    invoke-static {}, Lcom/fmm/dm/ui/XUIAdapter;->xuiUnlockAlertReport()V

    goto :goto_0
.end method
