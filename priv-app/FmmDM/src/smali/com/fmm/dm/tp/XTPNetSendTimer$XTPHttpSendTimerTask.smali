.class public Lcom/fmm/dm/tp/XTPNetSendTimer$XTPHttpSendTimerTask;
.super Ljava/util/TimerTask;
.source "XTPNetSendTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/tp/XTPNetSendTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XTPHttpSendTimerTask"
.end annotation


# instance fields
.field private isCloseTimer:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fmm/dm/tp/XTPNetSendTimer$XTPHttpSendTimerTask;->isCloseTimer:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 63
    # getter for: Lcom/fmm/dm/tp/XTPNetSendTimer;->m_nSendCount:I
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->access$000()I

    move-result v0

    const/16 v1, 0x3c

    if-lt v0, v1, :cond_0

    .line 65
    const/4 v0, 0x0

    # setter for: Lcom/fmm/dm/tp/XTPNetSendTimer;->m_nSendCount:I
    invoke-static {v0}, Lcom/fmm/dm/tp/XTPNetSendTimer;->access$002(I)I

    .line 66
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 67
    const-string v0, "===Send Fail==="

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 68
    sget-object v0, Lcom/fmm/dm/XDMService;->g_Task:Lcom/fmm/dm/agent/XDMTask;

    invoke-virtual {v0}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentDmXXXFail()V

    .line 76
    :goto_0
    return-void

    .line 71
    :cond_0
    const-string v0, "============================="

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "== send Timer["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    # getter for: Lcom/fmm/dm/tp/XTPNetSendTimer;->m_nSendCount:I
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->access$000()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 73
    const-string v0, "============================="

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 75
    # operator++ for: Lcom/fmm/dm/tp/XTPNetSendTimer;->m_nSendCount:I
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->access$008()I

    goto :goto_0
.end method

.method public xtpGetCloseTimer()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/fmm/dm/tp/XTPNetSendTimer$XTPHttpSendTimerTask;->isCloseTimer:Z

    return v0
.end method

.method public xtpSetCloseTimer(Z)V
    .locals 0
    .param p1, "isclose"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/fmm/dm/tp/XTPNetSendTimer$XTPHttpSendTimerTask;->isCloseTimer:Z

    .line 86
    return-void
.end method
