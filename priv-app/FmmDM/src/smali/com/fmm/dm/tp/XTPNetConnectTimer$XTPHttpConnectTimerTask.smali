.class public Lcom/fmm/dm/tp/XTPNetConnectTimer$XTPHttpConnectTimerTask;
.super Ljava/util/TimerTask;
.source "XTPNetConnectTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/tp/XTPNetConnectTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XTPHttpConnectTimerTask"
.end annotation


# instance fields
.field private isCloseTimer:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fmm/dm/tp/XTPNetConnectTimer$XTPHttpConnectTimerTask;->isCloseTimer:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 64
    # getter for: Lcom/fmm/dm/tp/XTPNetConnectTimer;->m_nConnectCount:I
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->access$000()I

    move-result v0

    const/16 v1, 0x3c

    if-lt v0, v1, :cond_0

    .line 66
    const/4 v0, 0x0

    # setter for: Lcom/fmm/dm/tp/XTPNetConnectTimer;->m_nConnectCount:I
    invoke-static {v0}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->access$002(I)I

    .line 67
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 68
    const-string v0, "===Connect Fail==="

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 69
    sget-object v0, Lcom/fmm/dm/XDMService;->g_Task:Lcom/fmm/dm/agent/XDMTask;

    invoke-virtual {v0}, Lcom/fmm/dm/agent/XDMTask;->xdmAgentDmXXXFail()V

    .line 77
    :goto_0
    return-void

    .line 72
    :cond_0
    const-string v0, "=============handshake============="

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "== Connect Timer["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    # getter for: Lcom/fmm/dm/tp/XTPNetConnectTimer;->m_nConnectCount:I
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->access$000()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 74
    const-string v0, "==================================="

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 76
    # operator++ for: Lcom/fmm/dm/tp/XTPNetConnectTimer;->m_nConnectCount:I
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->access$008()I

    goto :goto_0
.end method

.method public xtpHttpConnGetCloseTimer()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/fmm/dm/tp/XTPNetConnectTimer$XTPHttpConnectTimerTask;->isCloseTimer:Z

    return v0
.end method

.method public xtpHttpConnSetCloseTimer(Z)V
    .locals 0
    .param p1, "isclose"    # Z

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/fmm/dm/tp/XTPNetConnectTimer$XTPHttpConnectTimerTask;->isCloseTimer:Z

    .line 87
    return-void
.end method
