.class public Lcom/fmm/dm/tp/XTPNetRecvTimer;
.super Ljava/lang/Object;
.source "XTPNetRecvTimer.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XTPInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;
    }
.end annotation


# static fields
.field private static final XTP_RECEIVE_TIMER:I = 0x3c

.field private static m_RecvTimer:Ljava/util/Timer;

.field private static m_TpRecvTimer:Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

.field private static m_nRecvCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    sput-object v0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    .line 14
    sput-object v0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    .line 15
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    invoke-direct {v0}, Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;-><init>()V

    sput-object v0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    .line 22
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    .line 23
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvStartTimer()V

    .line 24
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 11
    sget v0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 11
    sput p0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    return p0
.end method

.method static synthetic access$008()I
    .locals 2

    .prologue
    .line 11
    sget v0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    return v0
.end method

.method public static xtpNetRecvEndTimer()V
    .locals 2

    .prologue
    .line 35
    const/4 v1, 0x0

    :try_start_0
    sput v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    .line 37
    sget-object v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    if-nez v1, :cond_1

    .line 54
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 40
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v1, "=====================>> endTimer(recv)"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 41
    sget-object v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 42
    sget-object v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    invoke-virtual {v1}, Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;->cancel()Z

    .line 44
    const/4 v1, 0x0

    sput-object v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    .line 45
    const/4 v1, 0x0

    sput-object v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    .line 47
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 48
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 52
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xtpNetRecvStartTimer()V
    .locals 6

    .prologue
    .line 28
    sget-object v0, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    sget-object v1, Lcom/fmm/dm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/fmm/dm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v1, v2, v4, v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;Ljava/util/Date;J)V

    .line 29
    return-void
.end method
