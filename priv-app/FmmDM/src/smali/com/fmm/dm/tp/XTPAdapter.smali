.class public Lcom/fmm/dm/tp/XTPAdapter;
.super Ljava/lang/Object;
.source "XTPAdapter.java"

# interfaces
.implements Lcom/fmm/dm/interfaces/XDMDefInterface;
.implements Lcom/fmm/dm/interfaces/XDMInterface;
.implements Lcom/fmm/dm/interfaces/XEventInterface;
.implements Lcom/fmm/dm/interfaces/XTPInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;,
        Lcom/fmm/dm/tp/XTPAdapter$XTPTrustManager;
    }
.end annotation


# static fields
.field public static g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

.field private static m_bIsConnected:Z

.field private static m_bUserCancel:Z

.field private static m_nHttpDebugCount:I

.field private static m_nTpappid:I

.field private static m_szCookie:Ljava/lang/String;


# instance fields
.field private final RECEIVE_BUFFER_SIZE:I

.field private m_HMacData:Lcom/fmm/dm/eng/core/XDMHmacData;

.field private m_Input:Ljava/io/InputStream;

.field private m_Output:Ljava/io/OutputStream;

.field private m_SSLContext:Ljavax/net/ssl/SSLContext;

.field private m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

.field private m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

.field private m_SSLSocket:Ljavax/net/ssl/SSLSocket;

.field private m_Socket:Ljava/net/Socket;

.field private m_WakeLock:Landroid/os/PowerManager$WakeLock;

.field private m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private m_bIsProxyServer:Z

.field private m_conProxy:Ljava/net/Proxy;

.field private m_nHttpBodyLength:I

.field private m_szHttpHeaderData:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 54
    sput-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    .line 55
    sput-object v1, Lcom/fmm/dm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    .line 60
    sput-boolean v0, Lcom/fmm/dm/tp/XTPAdapter;->m_bUserCancel:Z

    .line 73
    sput-boolean v0, Lcom/fmm/dm/tp/XTPAdapter;->m_bIsConnected:Z

    .line 76
    sput v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nTpappid:I

    .line 77
    sput v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpDebugCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_conProxy:Ljava/net/Proxy;

    .line 58
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_HMacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    .line 64
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    .line 65
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    .line 66
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    .line 67
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 68
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    .line 69
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    .line 70
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    .line 71
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_bIsProxyServer:Z

    .line 79
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 80
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 82
    const/16 v0, 0x1400

    iput v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->RECEIVE_BUFFER_SIZE:I

    .line 86
    sget-object v0, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-direct {v0}, Lcom/fmm/dm/tp/XTPHttpObj;-><init>()V

    sput-object v0, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    .line 88
    :cond_0
    return-void
.end method

.method public static xtpAdpCheckURL(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p0, "szObjectURL"    # Ljava/lang/String;
    .param p1, "szRespURL"    # Ljava/lang/String;

    .prologue
    .line 1792
    const/4 v0, 0x0

    .line 1793
    .local v0, "nRet":I
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1795
    :cond_0
    const-string v3, "Input Uri is NULL"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1796
    const/4 v3, -0x5

    .line 1816
    :goto_0
    return v3

    .line 1800
    :cond_1
    const/4 v1, 0x0

    .line 1801
    .local v1, "parser1":Lcom/fmm/dm/db/file/XDBUrlInfo;
    const/4 v2, 0x0

    .line 1803
    .local v2, "parser2":Lcom/fmm/dm/db/file/XDBUrlInfo;
    invoke-static {p0}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v1

    .line 1804
    invoke-static {p1}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v2

    .line 1806
    iget-object v3, v1, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iget-object v4, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iget-object v4, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, v1, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    iget v4, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    if-eq v3, v4, :cond_3

    .line 1808
    :cond_2
    const-string v3, "different response url!!"

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1809
    const/4 v0, 0x7

    :goto_1
    move v3, v0

    .line 1816
    goto :goto_0

    .line 1813
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private xtpAdpGetHttpInfo(I)I
    .locals 13
    .param p1, "appId"    # I

    .prologue
    const/4 v7, -0x1

    .line 1131
    const/4 v3, 0x0

    .line 1133
    .local v3, "ret":I
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-nez v8, :cond_1

    move v3, v7

    .line 1202
    .end local v3    # "ret":I
    :cond_0
    :goto_0
    return v3

    .line 1136
    .restart local v3    # "ret":I
    :cond_1
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput p1, v8, Lcom/fmm/dm/tp/XTPHttpObj;->appId:I

    .line 1138
    const-string v6, ""

    .line 1139
    .local v6, "szServerURL":Ljava/lang/String;
    const-string v4, ""

    .line 1141
    .local v4, "szProxyAddress":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v6

    .line 1142
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1144
    const-string v8, "ServerURL is null!!"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v7

    .line 1145
    goto :goto_0

    .line 1150
    :cond_2
    new-instance v0, Lcom/fmm/dm/db/file/XDBInfoConRef;

    invoke-direct {v0}, Lcom/fmm/dm/db/file/XDBInfoConRef;-><init>()V

    .line 1151
    .local v0, "Conref":Lcom/fmm/dm/db/file/XDBInfoConRef;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetConRef()Lcom/fmm/dm/db/file/XDBInfoConRef;

    move-result-object v0

    .line 1153
    if-nez v0, :cond_3

    .line 1155
    const-string v8, "Get Conref from DB is failed"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v7

    .line 1156
    goto :goto_0

    .line 1159
    :cond_3
    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetSalesCode()Ljava/lang/String;

    move-result-object v5

    .line 1164
    .local v5, "szSaleCode":Ljava/lang/String;
    :try_start_0
    iget-object v8, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    const-string v9, "0.0.0.0"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    const-string v9, "192.168."

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "CTC"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1166
    :cond_4
    const/4 v8, 0x0

    iput-boolean v8, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 1167
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetIsProxy(Z)V

    .line 1183
    :goto_1
    invoke-static {v6}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v2

    .line 1184
    .local v2, "parser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v9, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v9, v8, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerURL:Ljava/lang/String;

    .line 1185
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v9, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v9, v8, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    .line 1186
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v9, v2, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    iput v9, v8, Lcom/fmm/dm/tp/XTPHttpObj;->nServerPort:I

    .line 1187
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v9, 0x1

    iput-boolean v9, v8, Lcom/fmm/dm/tp/XTPHttpObj;->isServerInfo:Z

    .line 1188
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-static {v6}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpHttpGetConnectType(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    .line 1190
    invoke-virtual {p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1192
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v9, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v9, v9, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    iput-object v9, v8, Lcom/fmm/dm/tp/XTPHttpObj;->m_szProxyAddr:Ljava/lang/String;

    .line 1193
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v9, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v9, v9, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    iput-object v9, v8, Lcom/fmm/dm/tp/XTPHttpObj;->m_szProxyIP:Ljava/lang/String;

    .line 1194
    sget-object v8, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v9, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v9, v9, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    iput v9, v8, Lcom/fmm/dm/tp/XTPHttpObj;->proxyPort:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1197
    .end local v2    # "parser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    :catch_0
    move-exception v1

    .line 1199
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v7

    .line 1200
    goto/16 :goto_0

    .line 1171
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    :try_start_1
    const-string v8, "Proxy Mode"

    invoke-static {v8}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1172
    const/4 v8, 0x1

    iput-boolean v8, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->bProxyUse:Z

    .line 1173
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetIsProxy(Z)V

    .line 1175
    const-string v4, "http://"

    .line 1176
    iget-object v8, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v8, v8, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1177
    const-string v8, ":"

    invoke-virtual {v4, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1178
    iget-object v8, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v8, v8, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1179
    new-instance v8, Ljava/net/Proxy;

    sget-object v9, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v10, Ljava/net/InetSocketAddress;

    iget-object v11, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget-object v11, v11, Lcom/fmm/dm/db/file/XDBConRefPX;->Addr:Ljava/lang/String;

    iget-object v12, v0, Lcom/fmm/dm/db/file/XDBInfoConRef;->PX:Lcom/fmm/dm/db/file/XDBConRefPX;

    iget v12, v12, Lcom/fmm/dm/db/file/XDBConRefPX;->nPortNbr:I

    invoke-direct {v10, v11, v12}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v8, v9, v10}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    iput-object v8, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_conProxy:Ljava/net/Proxy;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public static xtpAdpGetHttpObjPointer()Lcom/fmm/dm/tp/XTPHttpObj;
    .locals 1

    .prologue
    .line 1541
    sget-object v0, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    return-object v0
.end method

.method public static xtpAdpGetIsConnected()Z
    .locals 2

    .prologue
    .line 1491
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connect status is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/tp/XTPAdapter;->m_bIsConnected:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1492
    sget-boolean v0, Lcom/fmm/dm/tp/XTPAdapter;->m_bIsConnected:Z

    return v0
.end method

.method public static xtpAdpGetUserCancel()Z
    .locals 2

    .prologue
    .line 1508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "flag is : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/dm/tp/XTPAdapter;->m_bUserCancel:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1509
    sget-boolean v0, Lcom/fmm/dm/tp/XTPAdapter;->m_bUserCancel:Z

    return v0
.end method

.method private xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "szData1"    # Ljava/lang/String;
    .param p2, "szData2"    # Ljava/lang/String;

    .prologue
    .line 388
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 391
    iget-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 392
    iget-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 393
    iget-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 395
    :cond_0
    return-void
.end method

.method private xtpAdpHttpChunkedHeadRead(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v3, 0x0

    .line 1613
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v4, ""

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1614
    .local v1, "data":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 1618
    .local v0, "c":I
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1619
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 1645
    :goto_0
    return-object v3

    .line 1622
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    .line 1624
    :goto_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ltz v0, :cond_1

    .line 1626
    const/16 v4, 0xd

    if-ne v0, v4, :cond_3

    .line 1628
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1629
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/16 v4, 0xa

    if-ne v0, v4, :cond_2

    .line 1645
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1632
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1639
    :catch_0
    move-exception v2

    .line 1641
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1635
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    int-to-char v4, v0

    :try_start_2
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public static xtpAdpHttpCookieClear()V
    .locals 1

    .prologue
    .line 1502
    const-string v0, ""

    sput-object v0, Lcom/fmm/dm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    .line 1503
    const-string v0, "HttpCookieClear!!"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1504
    return-void
.end method

.method private xtpAdpHttpHeadRead(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 8
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v3, 0x0

    const/16 v7, 0xd

    const/16 v6, 0xa

    .line 1298
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v4, ""

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1299
    .local v1, "data":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 1303
    .local v0, "c":I
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1304
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 1330
    :goto_0
    return-object v3

    .line 1307
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    .line 1309
    :goto_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ltz v0, :cond_1

    .line 1311
    if-eqz v0, :cond_1

    if-eq v0, v6, :cond_1

    if-ne v0, v7, :cond_3

    .line 1317
    :cond_1
    if-ne v0, v7, :cond_2

    .line 1319
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1320
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v4

    if-eq v4, v6, :cond_2

    .line 1321
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1330
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1314
    :cond_3
    int-to-char v4, v0

    :try_start_1
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1324
    :catch_0
    move-exception v2

    .line 1326
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xtpAdpHttpHeaderParser(Ljava/io/InputStream;)I
    .locals 14
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 1335
    const-string v11, ""

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1337
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v11, ""

    invoke-direct {v1, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1338
    .local v1, "header":Ljava/lang/StringBuffer;
    const-string v6, ""

    .line 1339
    .local v6, "szData":Ljava/lang/String;
    const/4 v4, -0x1

    .line 1340
    .local v4, "pos":I
    const-string v8, ""

    .line 1341
    .local v8, "szHmac":Ljava/lang/String;
    const-string v7, ""

    .line 1342
    .local v7, "szEncoding":Ljava/lang/String;
    const-string v5, ""

    .line 1343
    .local v5, "szContentrange":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1344
    .local v3, "nContentLen":I
    const/4 v9, 0x0

    .local v9, "szHost":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1346
    .local v2, "nConnection":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpHeadRead(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    .line 1347
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1350
    invoke-direct {p0, p1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpHeadRead(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    .line 1351
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1353
    const-string v11, "data is null "

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1354
    const/4 v11, -0x4

    .line 1472
    :goto_0
    return v11

    .line 1357
    :cond_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\r\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1358
    const/16 v11, 0x20

    invoke-virtual {v6, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 1359
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "http"

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    if-ltz v4, :cond_1

    const-string v11, " "

    add-int/lit8 v12, v4, 0x1

    invoke-virtual {v6, v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v11

    if-ltz v11, :cond_1

    .line 1361
    add-int/lit8 v11, v4, 0x1

    const-string v12, " "

    add-int/lit8 v13, v4, 0x1

    invoke-virtual {v6, v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v12

    invoke-virtual {v6, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 1364
    .local v10, "szRet":Ljava/lang/String;
    :try_start_0
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    iput v12, v11, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpReturnStatusValue:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1373
    .end local v10    # "szRet":Ljava/lang/String;
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pHttpObj.nHttpReturnStatusValue="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v12, v12, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1376
    :cond_2
    :goto_1
    invoke-direct {p0, p1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpHeadRead(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 1379
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_6

    .line 1422
    :cond_3
    const-string v11, "\r\n"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1424
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v12

    iput v12, v11, Lcom/fmm/dm/tp/XTPHttpObj;->nHeaderLength:I

    .line 1425
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    const/16 v12, 0x400

    if-ge v11, v12, :cond_4

    .line 1426
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "\r\n [_____Receive Header_____]\r\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1428
    :cond_4
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v9, v11, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpHost:Ljava/lang/String;

    .line 1429
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v3, v11, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    .line 1430
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v2, v11, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    .line 1432
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "chunked = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1433
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pHttpObj.nHeaderLength ="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v12, v12, Lcom/fmm/dm/tp/XTPHttpObj;->nHeaderLength:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1434
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pHttpObj.nContentLength ="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v12, v12, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1436
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_e

    .line 1438
    const-string v11, "chunked"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 1439
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v12, 0x1

    iput v12, v11, Lcom/fmm/dm/tp/XTPHttpObj;->nTransferCoding:I

    .line 1446
    :goto_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_f

    .line 1448
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v11, v11, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    invoke-virtual {p0, v8, v11}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpPsrParserHMAC(Ljava/lang/String;I)Lcom/fmm/dm/eng/core/XDMHmacData;

    move-result-object v11

    iput-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_HMacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    .line 1457
    :goto_3
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_10

    .line 1459
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v5, v11, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    .line 1460
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pContentRange"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v12, v12, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1467
    :goto_4
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 1469
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    sget-object v12, Lcom/fmm/dm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    iput-object v12, v11, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    .line 1472
    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 1366
    .restart local v10    # "szRet":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1368
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1369
    const/4 v11, -0x4

    goto/16 :goto_0

    .line 1383
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v10    # "szRet":Ljava/lang/String;
    :cond_6
    const-string v11, "\r\n"

    invoke-virtual {v6, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1384
    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1388
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "host:"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1389
    if-ltz v4, :cond_7

    .line 1390
    add-int/lit8 v11, v4, 0x5

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 1393
    :cond_7
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "content-length:"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1394
    if-ltz v4, :cond_8

    .line 1395
    add-int/lit8 v11, v4, 0xf

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1397
    :cond_8
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "connection:"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1398
    if-ltz v4, :cond_9

    .line 1399
    add-int/lit8 v11, v4, 0xb

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1401
    :cond_9
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "x-syncml-hmac:"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1402
    if-ltz v4, :cond_a

    .line 1403
    add-int/lit8 v11, v4, 0xe

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 1405
    :cond_a
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "transfer-encoding:"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1406
    if-ltz v4, :cond_b

    .line 1407
    add-int/lit8 v11, v4, 0x12

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 1409
    :cond_b
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "content-range:"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1410
    if-ltz v4, :cond_c

    .line 1411
    add-int/lit8 v11, v4, 0xe

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 1413
    :cond_c
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "set-cookie:"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1414
    if-ltz v4, :cond_2

    .line 1416
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    const-string v12, "jsessionid"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1418
    add-int/lit8 v11, v4, 0xb

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    sput-object v11, Lcom/fmm/dm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    goto/16 :goto_1

    .line 1441
    :cond_d
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v12, 0x0

    iput v12, v11, Lcom/fmm/dm/tp/XTPHttpObj;->nTransferCoding:I

    goto/16 :goto_2

    .line 1444
    :cond_e
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v12, 0x0

    iput v12, v11, Lcom/fmm/dm/tp/XTPHttpObj;->nTransferCoding:I

    goto/16 :goto_2

    .line 1453
    :cond_f
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_HMacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    .line 1454
    const-string v11, "szHMAC null"

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1464
    :cond_10
    sget-object v11, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    goto/16 :goto_4
.end method

.method private xtpAdpHttpInit(I)I
    .locals 7
    .param p1, "appId"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 1069
    const/4 v0, 0x0

    .line 1071
    .local v0, "ret":I
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput p1, v1, Lcom/fmm/dm/tp/XTPHttpObj;->appId:I

    .line 1072
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    .line 1073
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v3, v1, Lcom/fmm/dm/tp/XTPHttpObj;->AppSockfd:I

    .line 1074
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v4, v1, Lcom/fmm/dm/tp/XTPHttpObj;->Networkstatus:I

    .line 1075
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-boolean v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->bHttpBufferAlloc:Z

    .line 1076
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-boolean v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->bHttpHeaderParser:Z

    .line 1077
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v3, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    .line 1078
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v3, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nHeaderLength:I

    .line 1079
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v3, v1, Lcom/fmm/dm/tp/XTPHttpObj;->hProtocol:I

    .line 1080
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-boolean v4, v1, Lcom/fmm/dm/tp/XTPHttpObj;->httpObjUseable:Z

    .line 1081
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    .line 1082
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->dnsQueryRetryCount:I

    .line 1084
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTimerID:I

    .line 1085
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTimerWaitCounter:I

    .line 1086
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-boolean v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTimerUsed:Z

    .line 1087
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v5, v1, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    .line 1088
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v6, v1, Lcom/fmm/dm/tp/XTPHttpObj;->cServerType:I

    .line 1089
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-boolean v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nComingRemoteClose:Z

    .line 1091
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTunnelMode:I

    .line 1092
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTunnelConnected:I

    .line 1094
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const-string v2, "POST"

    iput-object v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    .line 1096
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const-string v2, "HTTP/1.1"

    iput-object v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    .line 1097
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v5, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpHost:Ljava/lang/String;

    .line 1098
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v5, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    .line 1099
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v5, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    .line 1100
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v3, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTransferCoding:I

    .line 1102
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-static {}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbGetConnectType()I

    move-result v2

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    .line 1104
    if-nez p1, :cond_0

    .line 1107
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v6, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpConnection:I

    .line 1108
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const-string v2, "Keep-Alive"

    iput-object v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    .line 1109
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const-string v2, "application/vnd.syncml.dm+wbxml"

    iput-object v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    .line 1110
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const-string v2, "application/vnd.syncml.dm+wbxml"

    iput-object v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    .line 1112
    :cond_0
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-static {}, Lcom/fmm/dm/adapter/XDMDevinfAdapter;->xdmDevAdpGetHttpUserAgent()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    .line 1114
    invoke-direct {p0, p1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetHttpInfo(I)I

    move-result v0

    .line 1115
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v1, v1, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    if-ne v1, v4, :cond_2

    invoke-virtual {p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1117
    const-string v1, "XTP_SSL_TUNNEL_MODE_ACTIVE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1118
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v2, 0x3

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTunnelMode:I

    .line 1126
    :cond_1
    :goto_0
    return v0

    .line 1120
    :cond_2
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v1, v1, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    if-ne v1, v4, :cond_1

    invoke-virtual {p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1122
    const-string v1, "XTP_SSL_TUNNEL_MODE_DEACTIVE"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1123
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput v6, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTunnelMode:I

    goto :goto_0
.end method

.method private xtpAdpHttpPsrMakeHeader(I)Ljava/lang/String;
    .locals 6
    .param p1, "conLength"    # I

    .prologue
    const/4 v3, 0x0

    .line 399
    const-string v4, "xtpAdpHttpPsrMakeHeader"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 400
    invoke-virtual {p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetHttpOpenMode()Ljava/lang/String;

    move-result-object v1

    .line 401
    .local v1, "szOpenMode":Ljava/lang/String;
    const-string v0, ""

    .line 402
    .local v0, "szHeaderlog":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 482
    :goto_0
    return-object v3

    .line 405
    :cond_0
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 407
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 409
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 410
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 416
    :goto_1
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 417
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 419
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 421
    :cond_1
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 423
    const-string v4, "no-store, private"

    const-string v5, "Cache-Control"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 427
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    const-string v5, "Connection"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_2
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 432
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    const-string v5, "User-Agent"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_3
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 437
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    const-string v5, "Accept"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :cond_4
    const-string v4, "en"

    const-string v5, "Accept-Language"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const-string v4, "utf-8"

    const-string v5, "Accept-Charset"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 445
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->nServerPort:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 446
    .local v2, "szServerHost":Ljava/lang/String;
    const-string v4, "Host"

    invoke-direct {p0, v2, v4}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    .end local v2    # "szServerHost":Ljava/lang/String;
    :cond_5
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 451
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    const-string v5, "Content-Type"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    :cond_6
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 456
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    const-string v5, "Cookie"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_7
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 461
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bytes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Range"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_8
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, "Content-Length: "

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 465
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 466
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 468
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    if-eqz v4, :cond_9

    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    array-length v4, v4

    if-eqz v4, :cond_9

    .line 470
    new-instance v4, Ljava/lang/String;

    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V

    const-string v5, "x-syncml-hmac"

    invoke-direct {p0, v4, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v3, v4, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    .line 473
    :cond_9
    iget-object v3, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 475
    iget-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 476
    sget-object v3, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v3, v3, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 477
    sget-object v3, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v3, v3, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    const-string v4, "####Hidden#####"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 478
    :cond_a
    sget-object v3, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v3, v3, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 479
    sget-object v3, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v3, v3, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    const-string v4, "####Hidden#####"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 480
    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\r\n [_____Make Header_____]\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 482
    iget-object v3, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    goto/16 :goto_0

    .line 414
    :cond_c
    const-string v4, "PATH is NULL. Checking the pHttpObj->pRequestUri"

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static xtpAdpNetGetProfile(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)V
    .locals 0
    .param p0, "pNetInfo"    # Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    .prologue
    .line 1546
    invoke-virtual {p0}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppGetBootStrapNetInfo()V

    .line 1547
    return-void
.end method

.method public static xtpAdpNetGetProfileData(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)V
    .locals 0
    .param p0, "pNetInfo"    # Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    .prologue
    .line 1551
    invoke-virtual {p0, p0}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppGetNetInfo(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)V

    .line 1552
    return-void
.end method

.method public static xtpAdpNetSaveProfile(Lcom/fmm/dm/agent/XDMAppProtoNetInfo;)I
    .locals 2
    .param p0, "pNetInfo"    # Lcom/fmm/dm/agent/XDMAppProtoNetInfo;

    .prologue
    const/4 v0, 0x0

    .line 1556
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1557
    invoke-virtual {p0, v0, p0}, Lcom/fmm/dm/agent/XDMAppProtoNetInfo;->xdmAgentAppProtoSetAccount(ILcom/fmm/dm/agent/XDMAppProtoNetInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1559
    const-string v0, "The Account Info Change Failed"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1560
    const/4 v0, -0x1

    .line 1562
    :cond_0
    return v0
.end method

.method public static xtpAdpResetWBXMLLog()V
    .locals 1

    .prologue
    .line 1786
    const/4 v0, 0x0

    sput v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpDebugCount:I

    .line 1787
    return-void
.end method

.method public static xtpAdpSetIsConnected(Z)V
    .locals 0
    .param p0, "isCon"    # Z

    .prologue
    .line 1497
    sput-boolean p0, Lcom/fmm/dm/tp/XTPAdapter;->m_bIsConnected:Z

    .line 1498
    return-void
.end method

.method public static xtpAdpSetUserCancel(Z)V
    .locals 2
    .param p0, "nCancelFlag"    # Z

    .prologue
    .line 1514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "flag is : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1515
    sput-boolean p0, Lcom/fmm/dm/tp/XTPAdapter;->m_bUserCancel:Z

    .line 1516
    return-void
.end method

.method private xtpAdpTunnelHandshake(Ljava/net/Socket;I)I
    .locals 13
    .param p1, "tunnel"    # Ljava/net/Socket;
    .param p2, "appId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    invoke-virtual {p0, p2}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpMakeSSLTunneling(I)Ljava/lang/String;

    move-result-object v9

    .line 310
    .local v9, "szMsg":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 311
    const/4 v11, -0x1

    .line 383
    :goto_0
    return v11

    .line 313
    :cond_0
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v11

    iput-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    .line 318
    :try_start_0
    const-string v11, "UTF-8"

    invoke-virtual {v9, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 324
    .local v0, "b":[B
    :goto_1
    iget-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    invoke-virtual {v11, v0}, Ljava/io/OutputStream;->write([B)V

    .line 325
    iget-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    invoke-virtual {v11}, Ljava/io/OutputStream;->flush()V

    .line 327
    const/16 v11, 0xc8

    new-array v6, v11, [B

    .line 328
    .local v6, "reply":[B
    const/4 v7, 0x0

    .line 329
    .local v7, "replyLen":I
    const/4 v5, 0x0

    .line 330
    .local v5, "newlinesSeen":I
    const/4 v2, 0x0

    .line 332
    .local v2, "headerDone":Z
    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    iput-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    move v8, v7

    .line 334
    .end local v7    # "replyLen":I
    .local v8, "replyLen":I
    :goto_2
    const/4 v11, 0x2

    if-ge v5, v11, :cond_3

    .line 336
    iget-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    invoke-virtual {v11}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 337
    .local v3, "i":I
    if-gez v3, :cond_1

    .line 339
    const-string v11, "Unable to tunnel"

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 342
    :try_start_1
    iget-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    .line 343
    iget-object v11, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 349
    :goto_3
    const/4 v11, -0x2

    goto :goto_0

    .line 320
    .end local v0    # "b":[B
    .end local v2    # "headerDone":Z
    .end local v3    # "i":I
    .end local v5    # "newlinesSeen":I
    .end local v6    # "reply":[B
    .end local v8    # "replyLen":I
    :catch_0
    move-exception v4

    .line 322
    .local v4, "ignored":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .restart local v0    # "b":[B
    goto :goto_1

    .line 345
    .end local v4    # "ignored":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "headerDone":Z
    .restart local v3    # "i":I
    .restart local v5    # "newlinesSeen":I
    .restart local v6    # "reply":[B
    .restart local v8    # "replyLen":I
    :catch_1
    move-exception v1

    .line 347
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 351
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    const/16 v11, 0xa

    if-ne v3, v11, :cond_2

    .line 353
    const/4 v2, 0x1

    .line 354
    add-int/lit8 v5, v5, 0x1

    move v7, v8

    .end local v8    # "replyLen":I
    .restart local v7    # "replyLen":I
    :goto_4
    move v8, v7

    .line 364
    .end local v7    # "replyLen":I
    .restart local v8    # "replyLen":I
    goto :goto_2

    .line 356
    :cond_2
    const/16 v11, 0xd

    if-eq v3, v11, :cond_6

    .line 358
    const/4 v5, 0x0

    .line 359
    if-nez v2, :cond_6

    array-length v11, v6

    if-ge v8, v11, :cond_6

    .line 361
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "replyLen":I
    .restart local v7    # "replyLen":I
    int-to-byte v11, v3

    aput-byte v11, v6, v8

    goto :goto_4

    .line 369
    .end local v3    # "i":I
    .end local v7    # "replyLen":I
    .restart local v8    # "replyLen":I
    :cond_3
    :try_start_2
    new-instance v10, Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "UTF-8"

    invoke-direct {v10, v6, v11, v8, v12}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 377
    .local v10, "szReply":Ljava/lang/String;
    :goto_5
    const-string v11, "HTTP/1.1 200"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_4

    const-string v11, "HTTP/1.0 200"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 379
    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 371
    .end local v10    # "szReply":Ljava/lang/String;
    :catch_2
    move-exception v4

    .line 373
    .restart local v4    # "ignored":Ljava/io/UnsupportedEncodingException;
    new-instance v10, Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct {v10, v6, v11, v8}, Ljava/lang/String;-><init>([BII)V

    .restart local v10    # "szReply":Ljava/lang/String;
    goto :goto_5

    .line 382
    .end local v4    # "ignored":Ljava/io/UnsupportedEncodingException;
    :cond_5
    const-string v11, "Unable to tunnel through Proxy"

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 383
    const/4 v11, -0x2

    goto/16 :goto_0

    .end local v10    # "szReply":Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_6
    move v7, v8

    .end local v8    # "replyLen":I
    .restart local v7    # "replyLen":I
    goto :goto_4
.end method

.method private xtpAdpWBXMLLog(I[B)V
    .locals 3
    .param p1, "appId"    # I
    .param p2, "buffer"    # [B

    .prologue
    .line 1765
    invoke-static {}, Lcom/fmm/dm/agent/XDMDebug;->xdmGetWbxmlFileStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    .line 1769
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "data/data/com.fmm.dm/httpdata"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpDebugCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".wbxml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/fmm/dm/agent/XDMDebug;->xdmWriteFile(Ljava/lang/String;[B)V

    .line 1770
    sget v1, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpDebugCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpDebugCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1778
    :cond_0
    :goto_0
    invoke-static {}, Lcom/fmm/dm/agent/XDMDebug;->xdmGetWbxmlDumpStatus()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 1780
    const/4 v1, 0x0

    invoke-static {p2, v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DUMP([BI)V

    .line 1782
    :cond_1
    return-void

    .line 1772
    :catch_0
    move-exception v0

    .line 1774
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public xtpAdpAbort()I
    .locals 1

    .prologue
    .line 1520
    const/4 v0, 0x0

    .line 1521
    .local v0, "rc":I
    return v0
.end method

.method public xtpAdpClose()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1014
    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1015
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 1017
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1018
    iput-object v2, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1020
    :cond_0
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v1, :cond_1

    .line 1022
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 1023
    iput-object v2, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 1026
    :cond_1
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-nez v1, :cond_3

    .line 1065
    :cond_2
    :goto_0
    return-void

    .line 1031
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    if-eqz v1, :cond_4

    .line 1032
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1034
    :cond_4
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    if-eqz v1, :cond_5

    .line 1035
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 1037
    :cond_5
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    if-eqz v1, :cond_6

    .line 1038
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 1040
    :cond_6
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    if-eqz v1, :cond_8

    .line 1042
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    if-eqz v1, :cond_7

    .line 1044
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    iget-object v2, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    invoke-virtual {v1, v2}, Ljavax/net/ssl/SSLSocket;->removeHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 1045
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    .line 1047
    :cond_7
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->close()V

    .line 1050
    :cond_8
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_9

    .line 1051
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 1053
    :cond_9
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    if-eqz v1, :cond_a

    .line 1054
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1062
    :cond_a
    :goto_1
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetIsConnected(Z)V

    goto :goto_0

    .line 1057
    :catch_0
    move-exception v0

    .line 1059
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public xtpAdpCloseNetWork()V
    .locals 1

    .prologue
    .line 1526
    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    .line 1527
    return-void
.end method

.method public xtpAdpGetCurHMACData()Lcom/fmm/dm/eng/core/XDMHmacData;
    .locals 1

    .prologue
    .line 1596
    iget-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_HMacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    return-object v0
.end method

.method public xtpAdpGetHttpEcode()I
    .locals 1

    .prologue
    .line 1531
    sget-object v0, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v0, v0, Lcom/fmm/dm/tp/XTPHttpObj;->eCode:I

    return v0
.end method

.method public xtpAdpGetHttpOpenMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1536
    sget-object v0, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v0, v0, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    return-object v0
.end method

.method public xtpAdpGetIsProxy()Z
    .locals 2

    .prologue
    .line 1478
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmProtoIsWIFIConnected()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1479
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_bIsProxyServer:Z

    .line 1481
    :cond_0
    iget-boolean v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_bIsProxyServer:Z

    return v0
.end method

.method public xtpAdpHttpMakeSSLTunnelHeader()Ljava/lang/String;
    .locals 3

    .prologue
    .line 647
    const/4 v0, 0x0

    .line 648
    .local v0, "szHeader":Ljava/lang/String;
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v2, 0x2

    iput v2, v1, Lcom/fmm/dm/tp/XTPHttpObj;->nTunnelConnected:I

    .line 649
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpPsrMakeSslTunnelHeader(I)Ljava/lang/String;

    move-result-object v0

    .line 650
    return-object v0
.end method

.method public xtpAdpHttpPsrChunkSizeParsing(Ljava/io/InputStream;)I
    .locals 14
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v12, 0x0

    .line 1650
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v11, ""

    invoke-direct {v3, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1651
    .local v3, "data":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 1652
    .local v1, "c":I
    const/4 v7, 0x1

    .line 1653
    .local v7, "p_num":I
    const/4 v0, 0x0

    .line 1654
    .local v0, "ChunkedLen":I
    const/4 v2, 0x0

    .line 1655
    .local v2, "ch":C
    const/16 v9, 0x61

    .line 1656
    .local v9, "small_A":C
    const/16 v6, 0x41

    .line 1660
    .local v6, "large_A":C
    const/4 v11, 0x1

    :try_start_0
    invoke-virtual {p1, v11}, Ljava/io/InputStream;->mark(I)V

    .line 1661
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v11

    const/4 v13, -0x1

    if-ne v11, v13, :cond_0

    move v11, v12

    .line 1708
    :goto_0
    return v11

    .line 1664
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    .line 1666
    :goto_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ltz v1, :cond_1

    .line 1668
    const/16 v11, 0xd

    if-ne v1, v11, :cond_3

    .line 1670
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, Ljava/io/InputStream;->mark(I)V

    .line 1671
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/16 v11, 0xa

    if-ne v1, v11, :cond_2

    .line 1686
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1688
    .local v10, "szSize":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    .line 1689
    .local v8, "size":[C
    array-length v11, v8

    add-int/lit8 v5, v11, -0x1

    .local v5, "j":I
    :goto_2
    if-ltz v5, :cond_7

    .line 1691
    aget-char v11, v8, v5

    invoke-static {v11}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibIsAlphaNum(C)Z

    move-result v11

    if-nez v11, :cond_4

    move v11, v12

    .line 1693
    goto :goto_0

    .line 1674
    .end local v5    # "j":I
    .end local v8    # "size":[C
    .end local v10    # "szSize":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1681
    :catch_0
    move-exception v4

    .line 1683
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v11, v12

    .line 1684
    goto :goto_0

    .line 1677
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_3
    int-to-char v11, v1

    :try_start_2
    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1695
    .restart local v5    # "j":I
    .restart local v8    # "size":[C
    .restart local v10    # "szSize":Ljava/lang/String;
    :cond_4
    aget-char v11, v8, v5

    invoke-static {v11}, Lcom/fmm/dm/eng/core/XDMMem;->xdmLibIsNum(C)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1697
    aget-char v11, v8, v5

    add-int/lit8 v11, v11, -0x30

    mul-int/2addr v11, v7

    add-int/2addr v0, v11

    .line 1705
    :goto_3
    mul-int/lit8 v7, v7, 0x10

    .line 1689
    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    .line 1701
    :cond_5
    aget-char v11, v8, v5

    if-ge v11, v9, :cond_6

    aget-char v11, v8, v5

    add-int/2addr v11, v9

    sub-int/2addr v11, v6

    :goto_4
    int-to-char v2, v11

    .line 1702
    sub-int v11, v2, v9

    add-int/lit8 v11, v11, 0xa

    mul-int/2addr v11, v7

    add-int/2addr v0, v11

    goto :goto_3

    .line 1701
    :cond_6
    aget-char v11, v8, v5

    goto :goto_4

    :cond_7
    move v11, v0

    .line 1708
    goto :goto_0
.end method

.method public xtpAdpHttpPsrGetContentLengthByRange(Ljava/lang/String;)I
    .locals 12
    .param p1, "szContentRange"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 1714
    const/4 v0, 0x0

    .line 1715
    .local v0, "contentlength":I
    const/4 v5, 0x0

    .local v5, "szHttpRange":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1716
    .local v6, "szRetRange":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "pos":I
    const/4 v3, 0x0

    .local v3, "len":I
    const/4 v2, 0x0

    .line 1718
    .local v2, "index":I
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "bytes "

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1719
    if-gez v4, :cond_1

    .line 1760
    :cond_0
    :goto_0
    return v8

    .line 1722
    :cond_1
    add-int/lit8 v9, v4, 0x6

    invoke-virtual {p1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 1723
    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v9

    if-eqz v9, :cond_0

    .line 1726
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    .line 1727
    if-eqz v3, :cond_0

    .line 1731
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 1733
    invoke-virtual {v5, v1}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x2f

    if-ne v9, v10, :cond_3

    .line 1735
    move v2, v1

    .line 1739
    :cond_2
    if-nez v2, :cond_4

    .line 1741
    move-object v6, v5

    .line 1747
    :goto_2
    const-string v9, "-"

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1748
    .local v7, "szTmp":[Ljava/lang/String;
    aget-object v9, v7, v8

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    aget-object v9, v7, v11

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1750
    aget-object v9, v7, v11

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aget-object v10, v7, v8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    sub-int v0, v9, v10

    .line 1751
    if-lez v0, :cond_0

    .line 1755
    add-int/lit8 v0, v0, 0x1

    move v8, v0

    .line 1760
    goto :goto_0

    .line 1731
    .end local v7    # "szTmp":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1745
    :cond_4
    invoke-virtual {v5, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method

.method public xtpAdpHttpPsrMakeSslTunnelHeader(I)Ljava/lang/String;
    .locals 7
    .param p1, "conLength"    # I

    .prologue
    const/4 v4, 0x0

    .line 655
    const-string v5, "xtpHttpPsrMakeSslTunnelHeader"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 656
    const/4 v3, 0x0

    .line 657
    .local v3, "szServerURL":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetHttpOpenMode()Ljava/lang/String;

    move-result-object v1

    .line 658
    .local v1, "szOpenMode":Ljava/lang/String;
    const-string v0, ""

    .line 659
    .local v0, "szHeaderlog":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 772
    :goto_0
    return-object v4

    .line 662
    :cond_0
    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 664
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    const-string v6, "CONNECT"

    if-ne v5, v6, :cond_e

    .line 666
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 668
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 669
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v5}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpHttpParserServerAddrWithPort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 670
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 677
    :goto_1
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 678
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 680
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 682
    :cond_1
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 684
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 686
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->nServerPort:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 687
    .local v2, "szServerHost":Ljava/lang/String;
    const-string v5, "Host"

    invoke-direct {p0, v2, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    .end local v2    # "szServerHost":Ljava/lang/String;
    :cond_2
    :goto_2
    const-string v5, "no-store, private"

    const-string v6, "Cache-Control"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 721
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    const-string v6, "Connection"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :cond_3
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 726
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    const-string v6, "User-Agent"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :cond_4
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 731
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    const-string v6, "Accept"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    :cond_5
    const-string v5, "en"

    const-string v6, "Accept-Language"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    const-string v5, "utf-8"

    const-string v6, "Accept-Charset"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 739
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    const-string v6, "Content-Type"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    :cond_6
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 744
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    const-string v6, "Cookie"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    :cond_7
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 749
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bytes="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Range"

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    :cond_8
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, "Content-Length: "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 753
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 754
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 756
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    if-eqz v5, :cond_9

    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    array-length v5, v5

    if-eqz v5, :cond_9

    .line 758
    new-instance v5, Ljava/lang/String;

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    const-string v6, "x-syncml-hmac: "

    invoke-direct {p0, v5, v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object v4, v5, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    .line 761
    :cond_9
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 763
    iget-object v0, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 764
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 765
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    const-string v5, "####Hidden#####"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 766
    :cond_a
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 767
    const-string v4, "####Hidden#####"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 768
    :cond_b
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 769
    sget-object v4, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v4, v4, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    const-string v5, "####Hidden#####"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 770
    :cond_c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\r\n [_____SSL Proxy Make Header_____]\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 772
    iget-object v4, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    goto/16 :goto_0

    .line 674
    :cond_d
    const-string v5, "PATH is NULL. Checking the pHttpObj->pRequestUri"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 692
    :cond_e
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 694
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 695
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 702
    :goto_3
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 703
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 705
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 707
    :cond_f
    iget-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 709
    sget-object v5, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v5, v5, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 711
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v6, v6, Lcom/fmm/dm/tp/XTPHttpObj;->nServerPort:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 712
    .restart local v2    # "szServerHost":Ljava/lang/String;
    const-string v5, "Host"

    invoke-direct {p0, v2, v5}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 699
    .end local v2    # "szServerHost":Ljava/lang/String;
    :cond_10
    const-string v5, "PATH is NULL. Checking the pHttpObj->pRequestUri"

    invoke-static {v5}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public xtpAdpHttpPsrParserHMAC(Ljava/lang/String;I)Lcom/fmm/dm/eng/core/XDMHmacData;
    .locals 8
    .param p1, "szValue"    # Ljava/lang/String;
    .param p2, "ConLen"    # I

    .prologue
    .line 1567
    new-instance v0, Lcom/fmm/dm/eng/core/XDMHmacData;

    invoke-direct {v0}, Lcom/fmm/dm/eng/core/XDMHmacData;-><init>()V

    .line 1568
    .local v0, "MacData":Lcom/fmm/dm/eng/core/XDMHmacData;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1570
    const/4 v0, 0x0

    .line 1591
    .end local v0    # "MacData":Lcom/fmm/dm/eng/core/XDMHmacData;
    :goto_0
    return-object v0

    .line 1573
    .restart local v0    # "MacData":Lcom/fmm/dm/eng/core/XDMHmacData;
    :cond_0
    const-string v6, "algorithm="

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1574
    .local v1, "nStartAlgorithm":I
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1575
    .local v4, "szData":Ljava/lang/String;
    const/16 v6, 0x2c

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 1576
    .local v5, "token":I
    const-string v6, "algorithm="

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    .line 1577
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "algorithm:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1579
    const-string v6, "username=\""

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1580
    .local v2, "nStartUserName":I
    const-string v6, "username=\""

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1581
    const-string v6, "\""

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1582
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacUserName:Ljava/lang/String;

    .line 1585
    const-string v6, "mac="

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 1586
    .local v3, "nStartmac":I
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1587
    const-string v6, "mac="

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    .line 1588
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "nStartmac:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/fmm/dm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1590
    iput p2, v0, Lcom/fmm/dm/eng/core/XDMHmacData;->httpContentLength:I

    goto/16 :goto_0
.end method

.method public xtpAdpInit(I)I
    .locals 2
    .param p1, "appId"    # I

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 94
    .local v0, "ret":I
    const-string v1, "xtpAdpInit"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 96
    sget-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-nez v1, :cond_0

    .line 97
    new-instance v1, Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-direct {v1}, Lcom/fmm/dm/tp/XTPHttpObj;-><init>()V

    sput-object v1, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    .line 99
    :cond_0
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetProxyData()Ljava/lang/Object;

    .line 100
    invoke-direct {p0, p1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpInit(I)I

    move-result v0

    .line 101
    if-eqz v0, :cond_1

    .line 103
    const-string v1, "xtpAdpHttpInit Fail!!"

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 104
    const/4 v0, -0x1

    .line 107
    .end local v0    # "ret":I
    :cond_1
    return v0
.end method

.method public xtpAdpMakeSSLTunneling(I)Ljava/lang/String;
    .locals 9
    .param p1, "appId"    # I

    .prologue
    .line 627
    const/4 v8, 0x0

    .line 628
    .local v8, "szHeader":Ljava/lang/String;
    const-string v2, ""

    .line 629
    .local v2, "szHmacData":Ljava/lang/String;
    const-string v3, ""

    .line 633
    .local v3, "szContentRange":Ljava/lang/String;
    :try_start_0
    sget-object v0, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v1, v0, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerURL:Ljava/lang/String;

    const-string v4, "CONNECT"

    const/4 v6, 0x0

    move-object v0, p0

    move v5, p1

    invoke-virtual/range {v0 .. v6}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 640
    invoke-virtual {p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpMakeSSLTunnelHeader()Ljava/lang/String;

    move-result-object v8

    move-object v0, v8

    .line 642
    :goto_0
    return-object v0

    .line 635
    :catch_0
    move-exception v7

    .line 637
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 638
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public xtpAdpOpen(I)I
    .locals 20
    .param p1, "appId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 112
    const/4 v7, 0x0

    .line 114
    .local v7, "nRet":I
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-nez v14, :cond_0

    .line 116
    invoke-virtual/range {p0 .. p1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpInit(I)I

    move-result v7

    .line 118
    if-eqz v7, :cond_0

    .line 120
    const-string v14, "xtpAdpInit Fail!!"

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 121
    const/4 v14, -0x1

    .line 304
    :goto_0
    return v14

    .line 125
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v14, :cond_2

    .line 127
    const-string v14, "power"

    invoke-static {v14}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/PowerManager;

    .line 128
    .local v8, "pm":Landroid/os/PowerManager;
    if-nez v8, :cond_1

    .line 130
    const-string v14, "PowerManager is null!!"

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 131
    const/4 v14, -0x2

    goto :goto_0

    .line 134
    :cond_1
    const/4 v14, 0x1

    const-string v15, "wakeLock"

    invoke-virtual {v8, v14, v15}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 135
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 136
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v14}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 139
    .end local v8    # "pm":Landroid/os/PowerManager;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-nez v14, :cond_4

    .line 141
    const-string v14, "wifi"

    invoke-static {v14}, Lcom/fmm/dm/XDMService;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/wifi/WifiManager;

    .line 142
    .local v13, "wifiManager":Landroid/net/wifi/WifiManager;
    if-nez v13, :cond_3

    .line 144
    const-string v14, "WifiManager is null!!"

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 145
    const/4 v14, -0x2

    goto :goto_0

    .line 147
    :cond_3
    const-string v14, "wifilock"

    invoke-virtual {v13, v14}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 148
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 149
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v14}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 153
    .end local v13    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_4
    invoke-static {}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetUserCancel()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 154
    const/4 v14, -0x2

    goto :goto_0

    .line 156
    :cond_5
    new-instance v14, Lcom/fmm/dm/tp/XTPNetConnectTimer;

    invoke-direct {v14}, Lcom/fmm/dm/tp/XTPNetConnectTimer;-><init>()V

    .line 158
    const/4 v9, 0x0

    .line 159
    .local v9, "socketAddress":Ljava/net/SocketAddress;
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-nez v14, :cond_6

    .line 161
    const-string v14, "xtpAdpInit Fail!!"

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 162
    const/4 v14, -0x1

    goto :goto_0

    .line 165
    :cond_6
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v14, v14, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_8

    .line 169
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 171
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_conProxy:Ljava/net/Proxy;

    invoke-virtual {v14}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v9

    .line 179
    :goto_1
    new-instance v14, Ljava/net/Socket;

    invoke-direct {v14}, Ljava/net/Socket;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    .line 180
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const v15, 0xea60

    invoke-virtual {v14, v9, v15}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 182
    new-instance v14, Ljava/io/BufferedInputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    invoke-virtual {v15}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    const/16 v16, 0x1000

    invoke-direct/range {v14 .. v16}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    .line 183
    new-instance v14, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    invoke-virtual {v15}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v15

    const/16 v16, 0x400

    invoke-direct/range {v14 .. v16}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :goto_2
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 303
    const/4 v14, 0x1

    invoke-static {v14}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpSetIsConnected(Z)V

    .line 304
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 176
    :cond_7
    :try_start_1
    new-instance v10, Ljava/net/InetSocketAddress;

    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v14, v14, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    sget-object v15, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v15, v15, Lcom/fmm/dm/tp/XTPHttpObj;->nServerPort:I

    invoke-direct {v10, v14, v15}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .end local v9    # "socketAddress":Ljava/net/SocketAddress;
    .local v10, "socketAddress":Ljava/net/SocketAddress;
    move-object v9, v10

    .end local v10    # "socketAddress":Ljava/net/SocketAddress;
    .restart local v9    # "socketAddress":Ljava/net/SocketAddress;
    goto :goto_1

    .line 185
    :catch_0
    move-exception v5

    .line 187
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 188
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 189
    const/4 v14, -0x2

    goto/16 :goto_0

    .line 192
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_8
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v14, v14, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_d

    .line 194
    const/4 v11, 0x0

    .local v11, "szSSLHost":Ljava/lang/String;
    const/4 v2, 0x0

    .line 195
    .local v2, "SSLPXHost":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "SSLPort":I
    const/4 v3, 0x0

    .line 198
    .local v3, "SSLPXPort":I
    :try_start_2
    const-string v14, "TLS"

    invoke-static {v14}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    .line 199
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    if-eqz v14, :cond_9

    .line 202
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    const/4 v15, 0x0

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    new-instance v18, Lcom/fmm/dm/tp/XTPAdapter$XTPTrustManager;

    const/16 v19, 0x0

    invoke-direct/range {v18 .. v19}, Lcom/fmm/dm/tp/XTPAdapter$XTPTrustManager;-><init>(Ljava/security/KeyStore;)V

    aput-object v18, v16, v17

    new-instance v17, Ljava/security/SecureRandom;

    invoke-direct/range {v17 .. v17}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual/range {v14 .. v17}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLContext;->getServerSessionContext()Ljavax/net/ssl/SSLSessionContext;

    move-result-object v14

    const v15, 0xea60

    invoke-interface {v14, v15}, Ljavax/net/ssl/SSLSessionContext;->setSessionTimeout(I)V

    .line 205
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 215
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v14

    if-eqz v14, :cond_c

    .line 217
    new-instance v14, Ljava/net/Socket;

    invoke-direct {v14}, Ljava/net/Socket;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    .line 218
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v2, v14, Lcom/fmm/dm/tp/XTPHttpObj;->m_szProxyAddr:Ljava/lang/String;

    .line 219
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v3, v14, Lcom/fmm/dm/tp/XTPHttpObj;->proxyPort:I

    .line 221
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v11, v14, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    .line 222
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v4, v14, Lcom/fmm/dm/tp/XTPHttpObj;->nServerPort:I

    .line 226
    :try_start_3
    new-instance v12, Ljava/net/InetSocketAddress;

    invoke-direct {v12, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 227
    .local v12, "tunnelAddr":Ljava/net/InetSocketAddress;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const v15, 0xea60

    invoke-virtual {v14, v12, v15}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 229
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v14, v1}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpTunnelHandshake(Ljava/net/Socket;I)I

    move-result v7

    .line 230
    if-eqz v7, :cond_a

    .line 232
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 233
    const/4 v14, -0x2

    goto/16 :goto_0

    .line 208
    .end local v12    # "tunnelAddr":Ljava/net/InetSocketAddress;
    :catch_1
    move-exception v5

    .line 210
    .restart local v5    # "e":Ljava/lang/Exception;
    const-string v14, "HttpsConnection: failed to initialize the socket factory"

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 211
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 212
    const/4 v14, -0x2

    goto/16 :goto_0

    .line 237
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v12    # "tunnelAddr":Ljava/net/InetSocketAddress;
    :cond_a
    :try_start_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v14, v15, v11, v4, v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v14

    check-cast v14, Ljavax/net/ssl/SSLSocket;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    .line 238
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    const v15, 0xea60

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 240
    new-instance v14, Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    invoke-direct {v14}, Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    .line 241
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 243
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 287
    .end local v12    # "tunnelAddr":Ljava/net/InetSocketAddress;
    :goto_3
    :try_start_5
    new-instance v14, Ljava/io/BufferedInputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v15}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    const/16 v16, 0x1000

    invoke-direct/range {v14 .. v16}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    .line 288
    new-instance v14, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v15}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v15

    const/16 v16, 0x400

    invoke-direct/range {v14 .. v16}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_2

    .line 290
    :catch_2
    move-exception v5

    .line 292
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 293
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 294
    const/4 v14, -0x2

    goto/16 :goto_0

    .line 245
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v5

    .line 247
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 250
    :try_start_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    if-eqz v14, :cond_b

    .line 251
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLSocket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 257
    :cond_b
    :goto_4
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 258
    const/4 v14, -0x2

    goto/16 :goto_0

    .line 253
    :catch_4
    move-exception v6

    .line 255
    .local v6, "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 263
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/io/IOException;
    :cond_c
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v11, v14, Lcom/fmm/dm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    .line 264
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v4, v14, Lcom/fmm/dm/tp/XTPHttpObj;->nServerPort:I

    .line 267
    :try_start_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v14

    check-cast v14, Ljavax/net/ssl/SSLSocket;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    .line 268
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    new-instance v15, Ljava/net/InetSocketAddress;

    invoke-direct {v15, v11, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const v16, 0xea60

    invoke-virtual/range {v14 .. v16}, Ljavax/net/ssl/SSLSocket;->connect(Ljava/net/SocketAddress;I)V

    .line 269
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    const v15, 0xea60

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 272
    new-instance v14, Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    invoke-direct {v14}, Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    .line 273
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLListener:Lcom/fmm/dm/tp/XTPAdapter$smltpHandshakeListener;

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 275
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    goto/16 :goto_3

    .line 277
    :catch_5
    move-exception v5

    .line 279
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 280
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 281
    const/4 v14, -0x2

    goto/16 :goto_0

    .line 299
    .end local v2    # "SSLPXHost":Ljava/lang/String;
    .end local v3    # "SSLPXPort":I
    .end local v4    # "SSLPort":I
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v11    # "szSSLHost":Ljava/lang/String;
    :cond_d
    const-string v14, "Other ProtocolType"

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public xtpAdpReceiveData(Ljava/io/ByteArrayOutputStream;I)I
    .locals 18
    .param p1, "pData"    # Ljava/io/ByteArrayOutputStream;
    .param p2, "appId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 777
    const/4 v12, 0x0

    .line 778
    .local v12, "nRet":I
    const/4 v6, 0x0

    .line 779
    .local v6, "actualBuff":[B
    const/4 v8, 0x0

    .line 780
    .local v8, "chunkedlen":I
    const/4 v3, 0x0

    .line 781
    .local v3, "aBuff":Ljava/io/ByteArrayInputStream;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    .line 783
    .local v10, "in":Ljava/io/InputStream;
    const/4 v7, 0x0

    .line 784
    .local v7, "bytesread":I
    const/4 v2, 0x0

    .line 785
    .local v2, "ContentBytesread":I
    const/4 v5, 0x0

    .line 787
    .local v5, "actual":I
    const-string v13, ""

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 789
    if-nez v10, :cond_0

    .line 791
    const/4 v13, -0x2

    .line 1009
    :goto_0
    return v13

    .line 794
    :cond_0
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_1

    .line 798
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const v14, 0xea60

    invoke-virtual {v13, v14}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 799
    new-instance v13, Lcom/fmm/dm/tp/XTPNetRecvTimer;

    invoke-direct {v13}, Lcom/fmm/dm/tp/XTPNetRecvTimer;-><init>()V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    .line 827
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpHeaderParser(Ljava/io/InputStream;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v12

    .line 835
    :goto_2
    if-eqz v12, :cond_2

    .line 837
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 838
    const/4 v13, -0x4

    goto :goto_0

    .line 801
    :catch_0
    move-exception v9

    .line 803
    .local v9, "e":Ljava/net/SocketException;
    invoke-virtual {v9}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 804
    const-string v13, "Time out"

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 805
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 806
    const/4 v13, -0x4

    goto :goto_0

    .line 813
    .end local v9    # "e":Ljava/net/SocketException;
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    const v14, 0xea60

    invoke-virtual {v13, v14}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 814
    new-instance v13, Lcom/fmm/dm/tp/XTPNetRecvTimer;

    invoke-direct {v13}, Lcom/fmm/dm/tp/XTPNetRecvTimer;-><init>()V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 816
    :catch_1
    move-exception v9

    .line 818
    .restart local v9    # "e":Ljava/net/SocketException;
    invoke-virtual {v9}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 819
    const-string v13, "Time out"

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 820
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 821
    const/4 v13, -0x4

    goto :goto_0

    .line 829
    .end local v9    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v9

    .line 831
    .local v9, "e":Ljava/lang/Exception;
    const/4 v12, -0x4

    .line 832
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 841
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_2
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    const/16 v14, 0xc8

    if-lt v13, v14, :cond_3

    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    const/16 v14, 0x12c

    if-le v13, v14, :cond_5

    .line 843
    :cond_3
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 844
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    const/16 v14, 0x1f7

    if-ne v13, v14, :cond_4

    .line 846
    const-string v13, "nHttpReturnStatusValue is 503, Retry"

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 847
    const/4 v13, -0x4

    goto/16 :goto_0

    .line 851
    :cond_4
    const/4 v13, -0x6

    goto/16 :goto_0

    .line 856
    :cond_5
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    if-nez v13, :cond_b

    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nTransferCoding:I

    const/4 v14, 0x1

    if-eq v13, v14, :cond_b

    .line 858
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_6

    .line 860
    const-string v13, "Content-length 0, Content-Range Use"

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 861
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v14, v14, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpPsrGetContentLengthByRange(Ljava/lang/String;)I

    move-result v14

    iput v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    .line 869
    :cond_6
    :goto_3
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    if-eqz v13, :cond_7

    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nTransferCoding:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_7

    .line 871
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v14, 0x0

    iput v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    .line 874
    :cond_7
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    if-lez v13, :cond_d

    .line 875
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    .line 879
    :goto_4
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nTransferCoding:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_12

    .line 881
    const-string v13, "SYNCMLDM CHUNKED"

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 882
    if-nez p2, :cond_11

    .line 884
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/16 v14, 0x1400

    new-array v14, v14, [B

    iput-object v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->pReceiveBuffer:[B

    .line 887
    :goto_5
    :try_start_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpPsrChunkSizeParsing(Ljava/io/InputStream;)I

    move-result v8

    if-lez v8, :cond_10

    .line 889
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "chunkedlen:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 890
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    new-array v14, v8, [B

    iput-object v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->pChunkBuffer:[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-object v4, v3

    .line 891
    .end local v3    # "aBuff":Ljava/io/ByteArrayInputStream;
    .local v4, "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_8
    :goto_6
    if-eq v8, v2, :cond_f

    .line 893
    :try_start_4
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->pChunkBuffer:[B

    sub-int v14, v8, v2

    invoke-virtual {v10, v13, v2, v14}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    if-lez v5, :cond_8

    .line 895
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "ContentBytesread:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " , actual :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 896
    new-array v6, v5, [B

    .line 897
    new-instance v3, Ljava/io/ByteArrayInputStream;

    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->pChunkBuffer:[B

    invoke-direct {v3, v13, v2, v5}, Ljava/io/ByteArrayInputStream;-><init>([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    .line 901
    .end local v4    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "aBuff":Ljava/io/ByteArrayInputStream;
    :try_start_5
    invoke-virtual {v3, v6}, Ljava/io/ByteArrayInputStream;->read([B)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v11

    .line 902
    .local v11, "nByteSize":I
    const/4 v13, -0x1

    if-ne v11, v13, :cond_9

    .line 915
    :cond_9
    if-eqz v3, :cond_a

    .line 916
    :try_start_6
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 923
    .end local v11    # "nByteSize":I
    :cond_a
    :goto_7
    const/4 v13, 0x0

    :try_start_7
    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v14, v14, Lcom/fmm/dm/tp/XTPHttpObj;->pReceiveBuffer:[B

    array-length v15, v6

    invoke-static {v6, v13, v14, v2, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 924
    add-int/2addr v2, v5

    .line 926
    const/4 v5, -0x1

    move-object v4, v3

    .end local v3    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto :goto_6

    .line 864
    .end local v4    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_b
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    if-eqz v13, :cond_c

    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    int-to-long v14, v13

    const-wide/32 v16, 0x7fffffff

    cmp-long v13, v14, v16

    if-nez v13, :cond_6

    .line 866
    :cond_c
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v14, 0x0

    iput v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nContentLength:I

    goto/16 :goto_3

    .line 877
    :cond_d
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    goto/16 :goto_4

    .line 918
    .restart local v11    # "nByteSize":I
    :catch_3
    move-exception v9

    .line 920
    .local v9, "e":Ljava/io/IOException;
    :try_start_8
    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_7

    .line 933
    .end local v9    # "e":Ljava/io/IOException;
    .end local v11    # "nByteSize":I
    :catch_4
    move-exception v9

    .line 935
    .restart local v9    # "e":Ljava/io/IOException;
    :goto_8
    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 936
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 937
    const/4 v13, -0x4

    goto/16 :goto_0

    .line 907
    .end local v9    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v9

    .line 909
    .restart local v9    # "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 915
    if-eqz v3, :cond_a

    .line 916
    :try_start_a
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_7

    .line 918
    :catch_6
    move-exception v9

    .line 920
    :try_start_b
    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_7

    .line 913
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    .line 915
    if-eqz v3, :cond_e

    .line 916
    :try_start_c
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    .line 921
    :cond_e
    :goto_9
    :try_start_d
    throw v13

    .line 918
    :catch_7
    move-exception v9

    .line 920
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4

    goto :goto_9

    .line 929
    .end local v3    # "aBuff":Ljava/io/ByteArrayInputStream;
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v4    # "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_f
    :try_start_e
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpChunkedHeadRead(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9

    .line 930
    const/4 v8, 0x0

    move-object v3, v4

    .end local v4    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_5

    .line 940
    :cond_10
    move-object/from16 v0, p0

    iput v2, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    .line 941
    const/4 v2, 0x0

    .line 942
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "CHUNKED pHttpObj.pReceiveBuffer.length = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v14, v14, Lcom/fmm/dm/tp/XTPHttpObj;->pReceiveBuffer:[B

    array-length v14, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 943
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "CHUNKED nHttpBodyLength = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 988
    :cond_11
    :goto_a
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1, v13}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpWBXMLLog(I[B)V

    .line 990
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 993
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_18

    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    const-string v14, "Close"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_18

    .line 995
    const-string v13, "_______HTTP_CONNECTION_TYPE_CLOSE MODE_______"

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 996
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v14, 0x1

    iput v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpConnection:I

    .line 997
    invoke-virtual/range {p0 .. p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpClose()V

    .line 1009
    :goto_b
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 950
    :cond_12
    :goto_c
    :try_start_f
    move-object/from16 v0, p0

    iget v13, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    if-eq v2, v13, :cond_16

    const/4 v13, -0x1

    if-eq v5, v13, :cond_16

    .line 952
    move-object/from16 v0, p0

    iget v13, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    if-lez v13, :cond_14

    .line 955
    if-nez v2, :cond_13

    .line 957
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    new-array v14, v14, [B

    iput-object v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->pReceiveBuffer:[B

    .line 959
    :cond_13
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v0, p0

    iget v14, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    sub-int/2addr v14, v2

    invoke-virtual {v10, v13, v2, v14}, Ljava/io/InputStream;->read([BII)I
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    move-result v5

    .line 960
    const/4 v13, -0x1

    if-ne v5, v13, :cond_14

    .line 966
    :cond_14
    if-eqz v6, :cond_15

    .line 967
    const/4 v6, 0x0

    .line 969
    :cond_15
    add-int/2addr v7, v5

    .line 970
    add-int/2addr v2, v5

    goto :goto_c

    .line 974
    :catch_8
    move-exception v9

    .line 976
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 977
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 978
    const/4 v13, -0x4

    goto/16 :goto_0

    .line 981
    .end local v9    # "e":Ljava/io/IOException;
    :cond_16
    invoke-virtual/range {p1 .. p1}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 982
    move-object/from16 v0, p0

    iget v13, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    if-lez v13, :cond_17

    .line 983
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->pReceiveBuffer:[B

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/fmm/dm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v15}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto/16 :goto_a

    .line 985
    :cond_17
    const/16 p1, 0x0

    goto/16 :goto_a

    .line 999
    :cond_18
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_19

    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v13, v13, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    const-string v14, "Keep-Alive"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_19

    .line 1001
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v14, 0x2

    iput v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpConnection:I

    goto :goto_b

    .line 1005
    :cond_19
    const-string v13, "_______HTTP_CONNECTION_TYPE_NONE MODE_______"

    invoke-static {v13}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1006
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v14, 0x2

    iput v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->nHttpConnection:I

    .line 1007
    sget-object v13, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const-string v14, "Keep-Alive"

    iput-object v14, v13, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    goto/16 :goto_b

    .line 933
    .end local v3    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "aBuff":Ljava/io/ByteArrayInputStream;
    :catch_9
    move-exception v9

    move-object v3, v4

    .end local v4    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_8
.end method

.method public xtpAdpSendData([BII)I
    .locals 15
    .param p1, "pData"    # [B
    .param p2, "dataSize"    # I
    .param p3, "appId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 487
    const/4 v5, 0x0

    .line 488
    .local v5, "nRet":I
    const/4 v10, 0x0

    .line 489
    .local v10, "szHttpHeaderData":Ljava/lang/String;
    const/4 v7, 0x0

    .line 490
    .local v7, "pSendBuffer":[B
    const/4 v2, 0x0

    .line 492
    .local v2, "SendDataLen":I
    const-string v12, ""

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 494
    iget-object v12, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    if-eqz v12, :cond_0

    sget-object v12, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-nez v12, :cond_1

    .line 496
    :cond_0
    const/4 v5, -0x2

    .line 622
    .end local v5    # "nRet":I
    :goto_0
    return v5

    .line 501
    .restart local v5    # "nRet":I
    :cond_1
    :try_start_0
    iget-object v12, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    if-eqz v12, :cond_2

    .line 503
    iget-object v12, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Ljava/io/InputStream;->mark(I)V

    .line 504
    iget-object v12, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    invoke-virtual {v12}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 514
    :cond_2
    if-eqz p1, :cond_3

    .line 516
    move-object/from16 v7, p1

    .line 517
    array-length v2, v7

    .line 524
    :goto_1
    sget-object v12, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v12, v12, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_4

    .line 528
    :try_start_1
    iget-object v12, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const v13, 0xea60

    invoke-virtual {v12, v13}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 529
    new-instance v12, Lcom/fmm/dm/tp/XTPNetSendTimer;

    invoke-direct {v12}, Lcom/fmm/dm/tp/XTPNetSendTimer;-><init>()V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1

    .line 537
    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpPsrMakeHeader(I)Ljava/lang/String;

    move-result-object v10

    .line 538
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 540
    const-string v12, "pHttpHeaderData is null"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 541
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 542
    const/4 v5, -0x3

    goto :goto_0

    .line 507
    :catch_0
    move-exception v3

    .line 509
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 510
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 511
    const/4 v5, -0x3

    goto :goto_0

    .line 521
    .end local v3    # "e":Ljava/io/IOException;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 531
    :catch_1
    move-exception v3

    .line 533
    .local v3, "e":Ljava/net/SocketException;
    invoke-virtual {v3}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 534
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 535
    const/4 v5, -0x3

    goto :goto_0

    .line 545
    .end local v3    # "e":Ljava/net/SocketException;
    :cond_4
    sget-object v12, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget v12, v12, Lcom/fmm/dm/tp/XTPHttpObj;->protocol:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    .line 549
    :try_start_2
    iget-object v12, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    const v13, 0xea60

    invoke-virtual {v12, v13}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 550
    new-instance v12, Lcom/fmm/dm/tp/XTPNetSendTimer;

    invoke-direct {v12}, Lcom/fmm/dm/tp/XTPNetSendTimer;-><init>()V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_2

    .line 558
    invoke-virtual {p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 560
    move/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpPsrMakeSslTunnelHeader(I)Ljava/lang/String;

    move-result-object v10

    .line 561
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 563
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 564
    const/4 v5, -0x3

    goto/16 :goto_0

    .line 552
    :catch_2
    move-exception v3

    .line 554
    .restart local v3    # "e":Ljava/net/SocketException;
    invoke-virtual {v3}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 555
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 556
    const/4 v5, -0x3

    goto/16 :goto_0

    .line 569
    .end local v3    # "e":Ljava/net/SocketException;
    :cond_5
    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpHttpPsrMakeHeader(I)Ljava/lang/String;

    move-result-object v10

    .line 570
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 572
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 573
    const/4 v5, -0x3

    goto/16 :goto_0

    .line 579
    :cond_6
    const-string v12, "Other ProtocolType"

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 580
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 581
    const/4 v5, -0x6

    goto/16 :goto_0

    .line 584
    :cond_7
    move/from16 v0, p3

    invoke-direct {p0, v0, v7}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpWBXMLLog(I[B)V

    .line 586
    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 587
    .local v4, "headbyte":[B
    array-length v12, v4

    add-int/2addr v12, v2

    new-array v1, v12, [B

    .line 589
    .local v1, "SendBuf":[B
    const/4 v12, 0x0

    const/4 v13, 0x0

    array-length v14, v4

    invoke-static {v4, v12, v1, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 590
    if-eqz v2, :cond_8

    if-eqz v7, :cond_8

    .line 591
    const/4 v12, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    array-length v13, v13

    invoke-static {v7, v12, v1, v13, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 593
    :cond_8
    iget-object v6, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    .line 594
    .local v6, "out":Ljava/io/OutputStream;
    const/4 v8, 0x0

    .line 595
    .local v8, "position":I
    array-length v11, v1

    .line 598
    .local v11, "totalLength":I
    :goto_2
    if-eq v8, v11, :cond_a

    .line 600
    sub-int v12, v11, v8

    const/16 v13, 0x100

    if-le v12, v13, :cond_9

    const/16 v9, 0x100

    .line 603
    .local v9, "sendLength":I
    :goto_3
    :try_start_3
    invoke-virtual {v6, v1, v8, v9}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 611
    add-int/2addr v8, v9

    .line 612
    goto :goto_2

    .line 600
    .end local v9    # "sendLength":I
    :cond_9
    sub-int v9, v11, v8

    goto :goto_3

    .line 605
    .restart local v9    # "sendLength":I
    :catch_3
    move-exception v3

    .line 607
    .local v3, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 608
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 609
    const/4 v5, -0x3

    goto/16 :goto_0

    .line 613
    .end local v3    # "e":Ljava/io/IOException;
    .end local v9    # "sendLength":I
    :cond_a
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 621
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    goto/16 :goto_0

    .line 615
    :catch_4
    move-exception v3

    .line 617
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 618
    invoke-static {}, Lcom/fmm/dm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 619
    const/4 v5, -0x3

    goto/16 :goto_0
.end method

.method public xtpAdpSetCurHMACData(Lcom/fmm/dm/eng/core/XDMHmacData;)I
    .locals 2
    .param p1, "MacData"    # Lcom/fmm/dm/eng/core/XDMHmacData;

    .prologue
    .line 1601
    const/4 v0, 0x0

    .line 1603
    .local v0, "nRet":I
    iget-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_HMacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    if-nez v1, :cond_0

    .line 1604
    new-instance v1, Lcom/fmm/dm/eng/core/XDMHmacData;

    invoke-direct {v1}, Lcom/fmm/dm/eng/core/XDMHmacData;-><init>()V

    iput-object v1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_HMacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    .line 1606
    :cond_0
    iput-object p1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_HMacData:Lcom/fmm/dm/eng/core/XDMHmacData;

    .line 1608
    return v0
.end method

.method public xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    .locals 11
    .param p1, "szRequest"    # Ljava/lang/String;
    .param p2, "szHmacData"    # Ljava/lang/String;
    .param p3, "szContentRange"    # Ljava/lang/String;
    .param p4, "szHttpOpenMode"    # Ljava/lang/String;
    .param p5, "appId"    # I
    .param p6, "nDownloadMode"    # Z

    .prologue
    .line 1207
    const/4 v6, 0x0

    .line 1208
    .local v6, "ret":I
    const/4 v5, 0x0

    .line 1210
    .local v5, "parser":Lcom/fmm/dm/db/file/XDBUrlInfo;
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    if-nez v9, :cond_0

    .line 1211
    const/4 v6, -0x1

    .line 1293
    .end local v6    # "ret":I
    :goto_0
    return v6

    .line 1213
    .restart local v6    # "ret":I
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 1215
    if-nez p5, :cond_6

    .line 1217
    const/4 v4, 0x0

    .local v4, "nSrcType":I
    const/4 v2, 0x0

    .line 1218
    .local v2, "nDstType":I
    const/4 v3, 0x0

    .line 1221
    .local v3, "nPortOrg":I
    invoke-static {p1}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/fmm/dm/db/file/XDBUrlInfo;

    move-result-object v5

    .line 1222
    invoke-static {}, Lcom/fmm/dm/db/file/XDBAdapter;->xdbGetConnectType()I

    move-result v4

    .line 1223
    iget-object v9, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-static {v9}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpHttpExchangeProtocolType(Ljava/lang/String;)I

    move-result v2

    .line 1224
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerAddress()Ljava/lang/String;

    move-result-object v7

    .line 1225
    .local v7, "szAddressOrg":Ljava/lang/String;
    invoke-static {}, Lcom/fmm/dm/db/file/XDB;->xdbGetServerPort()I

    move-result v3

    .line 1228
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1230
    const-string v9, "AddressOrg is null"

    invoke-static {v9}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1231
    const/4 v6, -0x1

    goto :goto_0

    .line 1234
    :cond_1
    if-ne v4, v2, :cond_2

    iget-object v9, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_2

    iget v9, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    if-eq v3, v9, :cond_5

    .line 1236
    :cond_2
    const-string v8, ""

    .line 1237
    .local v8, "szURL":Ljava/lang/String;
    const/16 v9, 0x3f

    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1238
    .local v1, "firstQuestion":I
    if-lez v1, :cond_4

    .line 1239
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 1242
    :goto_1
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_3

    .line 1244
    invoke-static {v8}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerUrl(Ljava/lang/String;)V

    .line 1247
    :cond_3
    iget-object v9, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-static {v9}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerAddress(Ljava/lang/String;)V

    .line 1248
    iget v9, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->nPort:I

    invoke-static {v9}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerPort(I)V

    .line 1249
    iget-object v9, v5, Lcom/fmm/dm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-static {v9}, Lcom/fmm/dm/db/file/XDB;->xdbSetServerProtocol(Ljava/lang/String;)V

    .line 1250
    const/4 v6, 0x7

    goto :goto_0

    .line 1241
    :cond_4
    move-object v8, p1

    goto :goto_1

    .line 1253
    .end local v1    # "firstQuestion":I
    .end local v8    # "szURL":Ljava/lang/String;
    :cond_5
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 1255
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    sget-object v10, Lcom/fmm/dm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    iput-object v10, v9, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    .line 1259
    .end local v2    # "nDstType":I
    .end local v3    # "nPortOrg":I
    .end local v4    # "nSrcType":I
    .end local v7    # "szAddressOrg":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lcom/fmm/dm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1260
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object p1, v9, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    .line 1267
    :cond_7
    :goto_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_a

    .line 1269
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iget-object v9, v9, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    if-nez v9, :cond_8

    .line 1271
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v10

    new-array v10, v10, [B

    iput-object v10, v9, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    .line 1273
    :cond_8
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    iput-object v10, v9, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    .line 1278
    :goto_3
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 1279
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object p4, v9, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    .line 1283
    :goto_4
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 1284
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    iput-object p3, v9, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    .line 1288
    :goto_5
    if-eqz p6, :cond_d

    .line 1289
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    move/from16 v0, p6

    iput-boolean v0, v9, Lcom/fmm/dm/tp/XTPHttpObj;->nDownloadMode:Z

    goto/16 :goto_0

    .line 1262
    :cond_9
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    invoke-static {p1}, Lcom/fmm/dm/tp/XTPHttpUtil;->xtpHttpParsePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/fmm/dm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    goto :goto_2

    .line 1276
    :cond_a
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/fmm/dm/tp/XTPHttpObj;->pHmacData:[B

    goto :goto_3

    .line 1281
    :cond_b
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/fmm/dm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    goto :goto_4

    .line 1286
    :cond_c
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/fmm/dm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    goto :goto_5

    .line 1291
    :cond_d
    sget-object v9, Lcom/fmm/dm/tp/XTPAdapter;->g_HttpObj:Lcom/fmm/dm/tp/XTPHttpObj;

    const/4 v10, 0x0

    iput-boolean v10, v9, Lcom/fmm/dm/tp/XTPHttpObj;->nDownloadMode:Z

    goto/16 :goto_0
.end method

.method public xtpAdpSetIsProxy(Z)V
    .locals 0
    .param p1, "isProxy"    # Z

    .prologue
    .line 1486
    iput-boolean p1, p0, Lcom/fmm/dm/tp/XTPAdapter;->m_bIsProxyServer:Z

    .line 1487
    return-void
.end method
