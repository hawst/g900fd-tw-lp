.class public final Lcom/fmm/dm/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final WSS_STR_COMMON_Cancel:I = 0x7f060000

.field public static final WSS_STR_COMMON_Edit:I = 0x7f060001

.field public static final WSS_STR_COMMON_No:I = 0x7f060002

.field public static final WSS_STR_COMMON_OK:I = 0x7f060003

.field public static final WSS_STR_COMMON_Select:I = 0x7f060004

.field public static final WSS_STR_COMMON_Yes:I = 0x7f060005

.field public static final WSS_STR_DM_ConnectFail:I = 0x7f060006

.field public static final WSS_STR_DM_ConnectingServer:I = 0x7f060007

.field public static final WSS_STR_DM_Not_Init:I = 0x7f060008

.field public static final WSS_STR_DM_Title2:I = 0x7f060009

.field public static final WSS_STR_DM_UnableNetwork:I = 0x7f06000a

.field public static final WSS_STR_EmergencyMode_Text:I = 0x7f06000b

.field public static final WSS_STR_EmergencyMode_Ticker:I = 0x7f06000c

.field public static final WSS_STR_EmergencyMode_Title:I = 0x7f06000d

.field public static final WSS_STR_FOTA_NoUpdate3:I = 0x7f06000e

.field public static final WSS_STR_FindMyMobile_Text:I = 0x7f06000f

.field public static final WSS_STR_FindMyMobile_Ticker:I = 0x7f060010

.field public static final WSS_STR_FindMyMobile_Title:I = 0x7f060011

.field public static final WSS_STR_MENU_APNType:I = 0x7f060012

.field public static final WSS_STR_MENU_AccessName:I = 0x7f060013

.field public static final WSS_STR_MENU_AuthType:I = 0x7f060014

.field public static final WSS_STR_MENU_NetworkProfile:I = 0x7f060015

.field public static final WSS_STR_MENU_Passwd:I = 0x7f060016

.field public static final WSS_STR_MENU_Port:I = 0x7f060017

.field public static final WSS_STR_MENU_ProfileName:I = 0x7f060018

.field public static final WSS_STR_MENU_Proxy:I = 0x7f060019

.field public static final WSS_STR_MENU_SWUpdate:I = 0x7f06001a

.field public static final WSS_STR_MENU_SWUpdateSummary:I = 0x7f06001b

.field public static final WSS_STR_MENU_ServerAddr:I = 0x7f06001c

.field public static final WSS_STR_MENU_ServerAuthType:I = 0x7f06001d

.field public static final WSS_STR_MENU_ServerID:I = 0x7f06001e

.field public static final WSS_STR_MENU_ServerPWD:I = 0x7f06001f

.field public static final WSS_STR_MENU_SettingDM:I = 0x7f060020

.field public static final WSS_STR_MENU_SettingDMSummary:I = 0x7f060021

.field public static final WSS_STR_MENU_UserID:I = 0x7f060022

.field public static final WSS_STR_MENU_UserName:I = 0x7f060023

.field public static final WSS_STR_MENU_UserPWD:I = 0x7f060024

.field public static final WSS_STR_RemoteControls_Title:I = 0x7f060025

.field public static final WSS_STR_Requested_By:I = 0x7f060026

.field public static final stms_version:I = 0x7f060027


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
