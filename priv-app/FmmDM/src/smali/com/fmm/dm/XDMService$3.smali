.class Lcom/fmm/dm/XDMService$3;
.super Landroid/os/Handler;
.source "XDMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fmm/dm/XDMService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/fmm/dm/XDMService;


# direct methods
.method constructor <init>(Lcom/fmm/dm/XDMService;)V
    .locals 0

    .prologue
    .line 475
    iput-object p1, p0, Lcom/fmm/dm/XDMService$3;->this$0:Lcom/fmm/dm/XDMService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x5

    const/4 v1, 0x3

    .line 479
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 481
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 509
    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device failed - unknown operation requested"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 513
    :goto_0
    sget-object v0, Lcom/fmm/dm/XDMService;->g_hAMTHandler:Landroid/os/Handler;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 514
    return-void

    .line 485
    :pswitch_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is get location"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 487
    const-string v0, "XAMT_OPERATION_GET_LOCATION"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 488
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpOerationGetLocation()Z

    goto :goto_0

    .line 493
    :pswitch_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is mobile tracking"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v0, "XAMT_OPERATION_TRACKING"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 496
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSyncOerationTracking()Z

    goto :goto_0

    .line 501
    :pswitch_2
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "SyncML"

    const-string v5, "Remotely management of device succeeded - operation to be performed is stop mobile tracking"

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string v0, "XAMT_OPERATION_STOP"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 504
    invoke-static {}, Lcom/fmm/dm/agent/amt/XAMTAdapter;->xamtAdpSyncOerationStop()Z

    goto :goto_0

    .line 481
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
