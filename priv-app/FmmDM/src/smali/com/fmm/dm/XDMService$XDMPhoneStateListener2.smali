.class public Lcom/fmm/dm/XDMService$XDMPhoneStateListener2;
.super Landroid/telephony/PhoneStateListener;
.source "XDMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fmm/dm/XDMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XDMPhoneStateListener2"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 934
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyListeners()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1005
    sget-object v1, Lcom/fmm/dm/XDMService;->telephonyManager2:Landroid/telephony/TelephonyManager;

    sget-object v2, Lcom/fmm/dm/XDMService;->serviceState2:Landroid/telephony/ServiceState;

    invoke-static {v1, v2}, Lcom/fmm/dm/adapter/XDMTelephonyData;->xdmGetInstance(Landroid/telephony/TelephonyManager;Landroid/telephony/ServiceState;)Lcom/fmm/dm/adapter/XDMTelephonyData;

    move-result-object v0

    .line 1007
    .local v0, "telephony":Lcom/fmm/dm/adapter/XDMTelephonyData;
    if-eqz v0, :cond_1

    .line 1009
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetServiceType2(I)I

    .line 1010
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->nSimState:I

    invoke-static {v1}, Lcom/fmm/dm/XDMService;->xdmSetSimState2(I)V

    .line 1012
    iget v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->networkType:I

    if-eqz v1, :cond_0

    .line 1014
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    if-nez v1, :cond_0

    .line 1016
    sput-boolean v4, Lcom/fmm/dm/XDMService;->g_bIsInitializing:Z

    .line 1017
    invoke-static {v5, v3, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 1022
    :cond_0
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-eqz v1, :cond_3

    .line 1024
    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v4}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 1026
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "going to Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1036
    :cond_1
    :goto_0
    sget-object v1, Lcom/fmm/dm/XDMService;->serviceState2:Landroid/telephony/ServiceState;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/fmm/dm/XDMService;->serviceState2:Landroid/telephony/ServiceState;

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    if-nez v1, :cond_2

    .line 1038
    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsPhoneBookInitialized:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/fmm/dm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_2

    .line 1040
    invoke-static {v5, v3, v3}, Lcom/fmm/dm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 1043
    :cond_2
    return-void

    .line 1028
    :cond_3
    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/fmm/dm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-nez v1, :cond_1

    .line 1030
    const/4 v1, 0x0

    # setter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {v1}, Lcom/fmm/dm/XDMService;->access$302(Z)Z

    .line 1032
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exit from Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/fmm/dm/XDMService;->m_bIsRoaming:Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$300()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "szIncomingNumber"    # Ljava/lang/String;

    .prologue
    .line 939
    const-string v0, ">>>>>>>>>>> onCallStateChanged"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 940
    sget-object v0, Lcom/fmm/dm/XDMService;->telephonyManager2:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_1

    .line 960
    :cond_0
    :goto_0
    return-void

    .line 943
    :cond_1
    if-nez p1, :cond_3

    .line 945
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_2

    .line 947
    const-string v0, "Run Resume Operation, Resume call CallStateChanged)"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 948
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    .line 950
    :cond_2
    const-string v0, ">>>>>>>>>>> CALL_STATE_IDLE"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 952
    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 954
    const-string v0, ">>>>>>>>>>> CALL_STATE_RINGING"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 956
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 958
    const-string v0, ">>>>>>>>>>> CALL_STATE_OFFHOOK"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDataActivity(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 1001
    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "networkType"    # I

    .prologue
    .line 981
    invoke-static {}, Lcom/fmm/dm/XDMService;->xdmGetDataState2()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 983
    invoke-static {p1}, Lcom/fmm/dm/XDMService;->xdmSetDataState2(I)V

    .line 986
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 988
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener2;->notifyListeners()V

    .line 990
    sget v0, Lcom/fmm/dm/XDMService;->g_nResumeStatus:I

    if-eqz v0, :cond_1

    .line 992
    const-string v0, "Run Resume Operation, Resume call DataConnectionState"

    invoke-static {v0}, Lcom/fmm/dm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 993
    # invokes: Lcom/fmm/dm/XDMService;->xdmResumeOperation()Z
    invoke-static {}, Lcom/fmm/dm/XDMService;->access$200()Z

    .line 996
    :cond_1
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 1
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 971
    sget-object v0, Lcom/fmm/dm/XDMService;->serviceState2:Landroid/telephony/ServiceState;

    if-eq v0, p1, :cond_0

    .line 973
    sput-object p1, Lcom/fmm/dm/XDMService;->serviceState2:Landroid/telephony/ServiceState;

    .line 974
    invoke-virtual {p0}, Lcom/fmm/dm/XDMService$XDMPhoneStateListener2;->notifyListeners()V

    .line 976
    :cond_0
    return-void
.end method

.method public onSignalStrengthChanged(I)V
    .locals 0
    .param p1, "asu"    # I

    .prologue
    .line 966
    return-void
.end method
