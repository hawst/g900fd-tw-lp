.class public Lcom/fmm/ds/XDSBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field a:Lcom/fmm/ds/c;

.field final b:Landroid/os/Handler;

.field private c:Lcom/fmm/ds/d/h;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->f:[B

    new-instance v0, Lcom/fmm/ds/b;

    invoke-direct {v0, p0}, Lcom/fmm/ds/b;-><init>(Lcom/fmm/ds/XDSBroadcastReceiver;)V

    iput-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/fmm/ds/XDSBroadcastReceiver;Lcom/fmm/ds/d/h;)Lcom/fmm/ds/d/h;
    .locals 0

    iput-object p1, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->c:Lcom/fmm/ds/d/h;

    return-object p1
.end method

.method static synthetic a(Lcom/fmm/ds/XDSBroadcastReceiver;)[B
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->f:[B

    return-object v0
.end method

.method static synthetic b(Lcom/fmm/ds/XDSBroadcastReceiver;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/fmm/ds/XDSBroadcastReceiver;)Lcom/fmm/ds/d/h;
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->c:Lcom/fmm/ds/d/h;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.provider.Telephony.WAP_PUSH_DS_NOTI_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "XCOMMON_INTENT_GET_DS_NOTI_RECEIVED"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Syncing... return"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "ds_message"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    array-length v1, v0

    if-lez v1, :cond_2

    new-instance v1, Lcom/fmm/ds/e/b;

    invoke-direct {v1}, Lcom/fmm/ds/e/b;-><init>()V

    array-length v2, v0

    invoke-virtual {v1, v0, v2}, Lcom/fmm/ds/e/b;->a([BI)Z

    goto :goto_0

    :cond_2
    const-string v0, "XCOMMON_INTENT_GET_DS_NOTI_RECEIVED error!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.IP_PUSH_DS_NOTI_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "XCOMMON_INTENT_GET_DS_IP_PUSH_RECEIVED"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Samsung Account is Empty !!!!!!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "push_message"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_6

    array-length v1, v0

    if-lez v1, :cond_6

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/fmm/ds/c/ar;->b([B)[B

    move-result-object v0

    sget-boolean v1, Lcom/fmm/ds/XDSApplication;->a:Z

    if-nez v1, :cond_5

    iput-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->f:[B

    new-instance v0, Lcom/fmm/ds/c;

    iget-object v1, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->b:Landroid/os/Handler;

    invoke-direct {v0, v1, v3}, Lcom/fmm/ds/c;-><init>(Landroid/os/Handler;I)V

    iput-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->a:Lcom/fmm/ds/c;

    iget-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->a:Lcom/fmm/ds/c;

    invoke-virtual {v0}, Lcom/fmm/ds/c;->start()V

    goto :goto_0

    :cond_5
    new-instance v1, Lcom/fmm/ds/e/b;

    invoke-direct {v1}, Lcom/fmm/ds/e/b;-><init>()V

    array-length v2, v0

    invoke-virtual {v1, v0, v2}, Lcom/fmm/ds/e/b;->b([BI)Z

    goto :goto_0

    :cond_6
    const-string v0, "XCOMMON_INTENT_GET_DS_IP_PUSH_RECEIVED error!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SETTING_DS_USERID_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "user_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->e:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XCOMMON_INTENT_SET_DS_USERID_RECEIVED szUserId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    sget-boolean v0, Lcom/fmm/ds/XDSApplication;->a:Z

    if-nez v0, :cond_8

    new-instance v0, Lcom/fmm/ds/c;

    iget-object v1, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/fmm/ds/c;-><init>(Landroid/os/Handler;I)V

    iput-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->a:Lcom/fmm/ds/c;

    iget-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->a:Lcom/fmm/ds/c;

    invoke-virtual {v0}, Lcom/fmm/ds/c;->start()V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->f()V

    goto/16 :goto_0

    :cond_9
    invoke-static {v3}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->e:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/fmm/ds/d/a;->a(ILjava/lang/String;)V

    invoke-static {v0, v3}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SETTING_DS_USERID_SIGNOUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "user_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCOMMON_INTENT_SET_DS_USERID_SIGNOUT szUserId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    sget-boolean v0, Lcom/fmm/ds/XDSApplication;->a:Z

    if-nez v0, :cond_b

    new-instance v0, Lcom/fmm/ds/c;

    iget-object v1, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/fmm/ds/c;-><init>(Landroid/os/Handler;I)V

    iput-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->a:Lcom/fmm/ds/c;

    iget-object v0, p0, Lcom/fmm/ds/XDSBroadcastReceiver;->a:Lcom/fmm/ds/c;

    invoke-virtual {v0}, Lcom/fmm/ds/c;->start()V

    goto/16 :goto_0

    :cond_b
    invoke-static {v3}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v3, v1}, Lcom/fmm/ds/d/a;->a(ILjava/lang/String;)V

    invoke-static {v0, v3}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SETTING_DS_CANCEL_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    sget-boolean v0, Lcom/fmm/ds/XDSApplication;->a:Z

    if-nez v0, :cond_d

    const-string v0, "Service Dead, Don\'t need to cancel."

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const-string v0, "type"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XCOMMON_INTENT_SET_DS_CANCEL_RECEIVED szCancelType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    packed-switch v0, :pswitch_data_0

    const-string v0, "Not support Cancel Type"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Lcom/fmm/ds/d/a;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/fmm/ds/c/ba;->a(Ljava/lang/String;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.FMMDS_ADMIN_SETTING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/fmm/ds/ui/XUITestMenu;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
