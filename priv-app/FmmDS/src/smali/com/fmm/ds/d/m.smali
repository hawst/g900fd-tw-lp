.class public Lcom/fmm/ds/d/m;
.super Ljava/lang/Object;


# direct methods
.method public static a(JLcom/fmm/ds/d/j;)V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    :try_start_0
    const-string v2, "sync"

    iget-boolean v3, p2, Lcom/fmm/ds/d/j;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "last_sync_type"

    iget v3, p2, Lcom/fmm/ds/d/j;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sync_type"

    iget v3, p2, Lcom/fmm/ds/d/j;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "server_id"

    iget v3, p2, Lcom/fmm/ds/d/j;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "real_sync_type"

    iget v3, p2, Lcom/fmm/ds/d/j;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sync_db_result"

    iget v3, p2, Lcom/fmm/ds/d/j;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "is_device_full"

    iget-boolean v3, p2, Lcom/fmm/ds/d/j;->g:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "is_server_full"

    iget-boolean v3, p2, Lcom/fmm/ds/d/j;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "s2p_add_item"

    iget v3, p2, Lcom/fmm/ds/d/j;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_replace_item"

    iget v3, p2, Lcom/fmm/ds/d/j;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_delete_item"

    iget v3, p2, Lcom/fmm/ds/d/j;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_total_item"

    iget v3, p2, Lcom/fmm/ds/d/j;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_add_item"

    iget v3, p2, Lcom/fmm/ds/d/j;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_replace_item"

    iget v3, p2, Lcom/fmm/ds/d/j;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_delete_item"

    iget v3, p2, Lcom/fmm/ds/d/j;->o:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_total_item"

    iget v3, p2, Lcom/fmm/ds/d/j;->p:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_add_item_fail"

    iget v3, p2, Lcom/fmm/ds/d/j;->q:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_replace_item_fail"

    iget v3, p2, Lcom/fmm/ds/d/j;->r:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_delete_item_fail"

    iget v3, p2, Lcom/fmm/ds/d/j;->s:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_add_item_fail"

    iget v3, p2, Lcom/fmm/ds/d/j;->t:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_replace_item_fail"

    iget v3, p2, Lcom/fmm/ds/d/j;->u:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_delete_item_fail"

    iget v3, p2, Lcom/fmm/ds/d/j;->v:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "db_name"

    iget-object v3, p2, Lcom/fmm/ds/d/j;->w:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "db_id"

    iget-object v3, p2, Lcom/fmm/ds/d/j;->x:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "db_password"

    iget-object v3, p2, Lcom/fmm/ds/d/j;->y:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "db_cred_type"

    iget v3, p2, Lcom/fmm/ds/d/j;->z:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "db_next_nonce"

    iget-object v3, p2, Lcom/fmm/ds/d/j;->A:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "last_anchor"

    iget-object v3, p2, Lcom/fmm/ds/d/j;->B:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "last_sync_time"

    iget-object v3, p2, Lcom/fmm/ds/d/j;->C:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "server_support_field_level"

    iget v3, p2, Lcom/fmm/ds/d/j;->F:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "sync_item_calllog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    :try_start_0
    const-string v0, "create table if not exists sync_item_calllog (rowid integer primary key autoincrement, sync boolean, last_sync_type integer, sync_type integer, server_id integer, real_sync_type integer, sync_db_result integer, is_device_full boolean, is_server_full boolean, s2p_add_item integer, s2p_replace_item integer, s2p_delete_item integer, s2p_total_item integer, p2s_add_item integer, p2s_replace_item integer, p2s_delete_item integer, p2s_total_item integer, s2p_add_item_fail integer, s2p_replace_item_fail integer, s2p_delete_item_fail integer, p2s_add_item_fail integer, p2s_replace_item_fail integer, p2s_delete_item_fail integer, db_name text, db_id text, db_password text, db_cred_type integer, db_next_nonce text, last_anchor text, last_sync_time text, server_support_field_level integer);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/d/j;)V
    .locals 4

    const/4 v1, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    :try_start_0
    const-string v2, "sync"

    iget-boolean v3, p0, Lcom/fmm/ds/d/j;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "last_sync_type"

    iget v3, p0, Lcom/fmm/ds/d/j;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sync_type"

    iget v3, p0, Lcom/fmm/ds/d/j;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "server_id"

    iget v3, p0, Lcom/fmm/ds/d/j;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "real_sync_type"

    iget v3, p0, Lcom/fmm/ds/d/j;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sync_db_result"

    iget v3, p0, Lcom/fmm/ds/d/j;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "is_device_full"

    iget-boolean v3, p0, Lcom/fmm/ds/d/j;->g:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "is_server_full"

    iget-boolean v3, p0, Lcom/fmm/ds/d/j;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "s2p_add_item"

    iget v3, p0, Lcom/fmm/ds/d/j;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_replace_item"

    iget v3, p0, Lcom/fmm/ds/d/j;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_delete_item"

    iget v3, p0, Lcom/fmm/ds/d/j;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_total_item"

    iget v3, p0, Lcom/fmm/ds/d/j;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_add_item"

    iget v3, p0, Lcom/fmm/ds/d/j;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_replace_item"

    iget v3, p0, Lcom/fmm/ds/d/j;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_delete_item"

    iget v3, p0, Lcom/fmm/ds/d/j;->o:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_total_item"

    iget v3, p0, Lcom/fmm/ds/d/j;->p:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_add_item_fail"

    iget v3, p0, Lcom/fmm/ds/d/j;->q:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_replace_item_fail"

    iget v3, p0, Lcom/fmm/ds/d/j;->r:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "s2p_delete_item_fail"

    iget v3, p0, Lcom/fmm/ds/d/j;->s:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_add_item_fail"

    iget v3, p0, Lcom/fmm/ds/d/j;->t:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_replace_item_fail"

    iget v3, p0, Lcom/fmm/ds/d/j;->u:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "p2s_delete_item_fail"

    iget v3, p0, Lcom/fmm/ds/d/j;->v:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "db_name"

    iget-object v3, p0, Lcom/fmm/ds/d/j;->w:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "db_id"

    iget-object v3, p0, Lcom/fmm/ds/d/j;->x:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "db_password"

    iget-object v3, p0, Lcom/fmm/ds/d/j;->y:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "db_cred_type"

    iget v3, p0, Lcom/fmm/ds/d/j;->z:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "db_next_nonce"

    iget-object v3, p0, Lcom/fmm/ds/d/j;->A:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "last_anchor"

    iget-object v3, p0, Lcom/fmm/ds/d/j;->B:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "last_sync_time"

    iget-object v3, p0, Lcom/fmm/ds/d/j;->C:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "server_support_field_level"

    iget v3, p0, Lcom/fmm/ds/d/j;->F:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "sync_item_calllog"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public static a(J)Z
    .locals 14

    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/16 v0, 0x1f

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowid"

    aput-object v0, v3, v10

    const-string v0, "sync"

    aput-object v0, v3, v12

    const/4 v0, 0x2

    const-string v1, "last_sync_type"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "sync_type"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "server_id"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "real_sync_type"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "sync_db_result"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "is_device_full"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "is_server_full"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "s2p_add_item"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "s2p_replace_item"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "s2p_delete_item"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "s2p_total_item"

    aput-object v1, v3, v0

    const/16 v0, 0xd

    const-string v1, "p2s_add_item"

    aput-object v1, v3, v0

    const/16 v0, 0xe

    const-string v1, "p2s_replace_item"

    aput-object v1, v3, v0

    const/16 v0, 0xf

    const-string v1, "p2s_delete_item"

    aput-object v1, v3, v0

    const/16 v0, 0x10

    const-string v1, "p2s_total_item"

    aput-object v1, v3, v0

    const/16 v0, 0x11

    const-string v1, "s2p_add_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x12

    const-string v1, "s2p_replace_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x13

    const-string v1, "s2p_delete_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x14

    const-string v1, "p2s_add_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x15

    const-string v1, "p2s_replace_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x16

    const-string v1, "p2s_delete_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x17

    const-string v1, "db_name"

    aput-object v1, v3, v0

    const/16 v0, 0x18

    const-string v1, "db_id"

    aput-object v1, v3, v0

    const/16 v0, 0x19

    const-string v1, "db_password"

    aput-object v1, v3, v0

    const/16 v0, 0x1a

    const-string v1, "db_cred_type"

    aput-object v1, v3, v0

    const/16 v0, 0x1b

    const-string v1, "db_next_nonce"

    aput-object v1, v3, v0

    const/16 v0, 0x1c

    const-string v1, "last_anchor"

    aput-object v1, v3, v0

    const/16 v0, 0x1d

    const-string v1, "last_sync_time"

    aput-object v1, v3, v0

    const/16 v0, 0x1e

    const-string v1, "server_support_field_level"

    aput-object v1, v3, v0

    :try_start_0
    invoke-static {}, Lcom/fmm/ds/XDSApplication;->d()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    :try_start_1
    const-string v2, "sync_item_calllog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-lez v1, :cond_6

    move v1, v12

    :goto_0
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    if-eqz v11, :cond_5

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    move-object v1, v11

    :goto_2
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move v0, v10

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v11

    :goto_3
    if-eqz v1, :cond_2

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_2
    if-eqz v11, :cond_3

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_2

    :cond_4
    move v0, v10

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v1, v10

    goto :goto_0
.end method

.method public static b(JLcom/fmm/ds/d/j;)Ljava/lang/Object;
    .locals 12

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v10, 0x0

    const/16 v0, 0x1f

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowid"

    aput-object v0, v3, v2

    const-string v0, "sync"

    aput-object v0, v3, v1

    const-string v0, "last_sync_type"

    aput-object v0, v3, v4

    const-string v0, "sync_type"

    aput-object v0, v3, v5

    const/4 v0, 0x4

    const-string v1, "server_id"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "real_sync_type"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "sync_db_result"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "is_device_full"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "is_server_full"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "s2p_add_item"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "s2p_replace_item"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "s2p_delete_item"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "s2p_total_item"

    aput-object v1, v3, v0

    const/16 v0, 0xd

    const-string v1, "p2s_add_item"

    aput-object v1, v3, v0

    const/16 v0, 0xe

    const-string v1, "p2s_replace_item"

    aput-object v1, v3, v0

    const/16 v0, 0xf

    const-string v1, "p2s_delete_item"

    aput-object v1, v3, v0

    const/16 v0, 0x10

    const-string v1, "p2s_total_item"

    aput-object v1, v3, v0

    const/16 v0, 0x11

    const-string v1, "s2p_add_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x12

    const-string v1, "s2p_replace_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x13

    const-string v1, "s2p_delete_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x14

    const-string v1, "p2s_add_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x15

    const-string v1, "p2s_replace_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x16

    const-string v1, "p2s_delete_item_fail"

    aput-object v1, v3, v0

    const/16 v0, 0x17

    const-string v1, "db_name"

    aput-object v1, v3, v0

    const/16 v0, 0x18

    const-string v1, "db_id"

    aput-object v1, v3, v0

    const/16 v0, 0x19

    const-string v1, "db_password"

    aput-object v1, v3, v0

    const/16 v0, 0x1a

    const-string v1, "db_cred_type"

    aput-object v1, v3, v0

    const/16 v0, 0x1b

    const-string v1, "db_next_nonce"

    aput-object v1, v3, v0

    const/16 v0, 0x1c

    const-string v1, "last_anchor"

    aput-object v1, v3, v0

    const/16 v0, 0x1d

    const-string v1, "last_sync_time"

    aput-object v1, v3, v0

    const/16 v0, 0x1e

    const-string v1, "server_support_field_level"

    aput-object v1, v3, v0

    :try_start_0
    invoke-static {}, Lcom/fmm/ds/XDSApplication;->d()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    const/4 v1, 0x1

    :try_start_1
    const-string v2, "sync_item_calllog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_7

    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p2, Lcom/fmm/ds/d/j;->a:Z

    :goto_1
    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->b:I

    const/4 v1, 0x3

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->c:I

    const/4 v1, 0x4

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->d:I

    const/4 v1, 0x5

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->e:I

    const/4 v1, 0x6

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->f:I

    const/4 v1, 0x7

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    iput-boolean v1, p2, Lcom/fmm/ds/d/j;->g:Z

    :goto_2
    const/16 v1, 0x8

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x0

    iput-boolean v1, p2, Lcom/fmm/ds/d/j;->h:Z

    :goto_3
    const/16 v1, 0x9

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->i:I

    const/16 v1, 0xa

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->j:I

    const/16 v1, 0xb

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->k:I

    const/16 v1, 0xc

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->l:I

    const/16 v1, 0xd

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->m:I

    const/16 v1, 0xe

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->n:I

    const/16 v1, 0xf

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->o:I

    const/16 v1, 0x10

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->p:I

    const/16 v1, 0x11

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->q:I

    const/16 v1, 0x12

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->r:I

    const/16 v1, 0x13

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->s:I

    const/16 v1, 0x14

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->t:I

    const/16 v1, 0x15

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->u:I

    const/16 v1, 0x16

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->v:I

    const/16 v1, 0x17

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/j;->w:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/j;->x:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/j;->y:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->z:I

    const/16 v1, 0x1b

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/j;->A:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/j;->B:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/j;->C:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/j;->F:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_4
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_5
    return-object p2

    :cond_2
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p2, Lcom/fmm/ds/d/j;->a:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_6
    if-eqz v1, :cond_3

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :cond_5
    const/4 v1, 0x1

    :try_start_4
    iput-boolean v1, p2, Lcom/fmm/ds/d/j;->g:Z

    goto/16 :goto_2

    :cond_6
    const/4 v1, 0x1

    iput-boolean v1, p2, Lcom/fmm/ds/d/j;->h:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3

    :cond_7
    if-eqz v0, :cond_8

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_8
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_5

    :catchall_1
    move-exception v0

    move-object v1, v10

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v1, v10

    goto :goto_4
.end method
