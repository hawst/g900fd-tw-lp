.class public Lcom/fmm/ds/d/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static a:Z

.field public static b:[Lcom/fmm/ds/d/h;

.field public static c:Lcom/fmm/ds/d/b;

.field public static d:Lcom/fmm/ds/d/p;

.field public static e:Lcom/fmm/ds/d/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/fmm/ds/d/a;->a:Z

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/fmm/ds/d/h;

    sput-object v0, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    new-instance v0, Lcom/fmm/ds/d/b;

    invoke-direct {v0}, Lcom/fmm/ds/d/b;-><init>()V

    sput-object v0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    new-instance v0, Lcom/fmm/ds/d/p;

    invoke-direct {v0}, Lcom/fmm/ds/d/p;-><init>()V

    sput-object v0, Lcom/fmm/ds/d/a;->d:Lcom/fmm/ds/d/p;

    new-instance v0, Lcom/fmm/ds/d/q;

    invoke-direct {v0}, Lcom/fmm/ds/d/q;-><init>()V

    sput-object v0, Lcom/fmm/ds/d/a;->e:Lcom/fmm/ds/d/q;

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 3

    const/4 v1, -0x1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "szName is null!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    const/4 v2, 0x1

    if-ge v0, v2, :cond_2

    invoke-static {v0}, Lcom/fmm/ds/d/a;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static a(I)Lcom/fmm/ds/d/h;
    .locals 1

    sget-object v0, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static a()V
    .locals 8

    const-wide/16 v6, 0x1

    const/4 v5, 0x1

    const/4 v0, 0x0

    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    sget-object v2, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    new-instance v3, Lcom/fmm/ds/d/h;

    invoke-direct {v3}, Lcom/fmm/ds/d/h;-><init>()V

    aput-object v3, v2, v1

    add-int/lit8 v2, v1, 0x1

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/fmm/ds/d/i;->a(J)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v2, v2, v1

    invoke-static {v2, v1}, Lcom/fmm/ds/d/a;->b(Lcom/fmm/ds/d/h;I)V

    sget-object v2, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v2, v2, v1

    invoke-static {v2}, Lcom/fmm/ds/d/i;->a(Lcom/fmm/ds/d/h;)V

    :goto_1
    add-int/lit8 v2, v1, 0x1

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/fmm/ds/d/m;->a(J)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v2, v2, v1

    invoke-static {v2, v1}, Lcom/fmm/ds/d/a;->b(Lcom/fmm/ds/d/h;I)V

    sget-object v2, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    invoke-static {v2}, Lcom/fmm/ds/d/m;->a(Lcom/fmm/ds/d/j;)V

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v2, v1, 0x1

    int-to-long v2, v2

    sget-object v4, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v4, v4, v1

    invoke-static {v2, v3, v4}, Lcom/fmm/ds/d/i;->b(JLcom/fmm/ds/d/h;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v1, 0x1

    int-to-long v2, v2

    sget-object v4, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    invoke-static {v2, v3, v4}, Lcom/fmm/ds/d/m;->b(JLcom/fmm/ds/d/j;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    invoke-static {v6, v7}, Lcom/fmm/ds/d/c;->a(J)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    invoke-static {v1, v0}, Lcom/fmm/ds/d/a;->a(Ljava/lang/Object;I)V

    sget-object v1, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    invoke-static {v1}, Lcom/fmm/ds/d/c;->a(Lcom/fmm/ds/d/b;)V

    :goto_3
    if-ge v0, v5, :cond_5

    sget-object v1, Lcom/fmm/ds/d/a;->d:Lcom/fmm/ds/d/p;

    iget-object v1, v1, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    new-instance v2, Lcom/fmm/ds/d/f;

    invoke-direct {v2}, Lcom/fmm/ds/d/f;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v1, v0, 0x1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/fmm/ds/d/g;->a(J)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/fmm/ds/d/a;->d:Lcom/fmm/ds/d/p;

    iget-object v1, v1, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    aget-object v1, v1, v0

    invoke-static {v1, v0}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/f;I)V

    sget-object v1, Lcom/fmm/ds/d/a;->d:Lcom/fmm/ds/d/p;

    iget-object v1, v1, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/fmm/ds/d/g;->a(Lcom/fmm/ds/d/f;)V

    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    sget-object v1, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    invoke-static {v6, v7, v1}, Lcom/fmm/ds/d/c;->b(JLcom/fmm/ds/d/b;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    add-int/lit8 v1, v0, 0x1

    int-to-long v1, v1

    sget-object v3, Lcom/fmm/ds/d/a;->d:Lcom/fmm/ds/d/p;

    iget-object v3, v3, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    aget-object v3, v3, v0

    invoke-static {v1, v2, v3}, Lcom/fmm/ds/d/g;->b(JLcom/fmm/ds/d/f;)Ljava/lang/Object;

    goto :goto_4

    :cond_5
    sput-boolean v5, Lcom/fmm/ds/d/a;->a:Z

    return-void
.end method

.method private static a(ILcom/fmm/ds/d/h;Lcom/fmm/ds/d/r;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p2, Lcom/fmm/ds/d/r;->f:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    iget-object v0, p2, Lcom/fmm/ds/d/r;->g:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/ds/d/h;->i:Ljava/lang/String;

    iput v2, p1, Lcom/fmm/ds/d/h;->f:I

    iput v1, p1, Lcom/fmm/ds/d/h;->n:I

    iget-object v0, p2, Lcom/fmm/ds/d/r;->b:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    iput v1, p1, Lcom/fmm/ds/d/h;->o:I

    iget v0, p2, Lcom/fmm/ds/d/r;->i:I

    iput v0, p1, Lcom/fmm/ds/d/h;->c:I

    if-nez p0, :cond_0

    iget-object v0, p2, Lcom/fmm/ds/d/r;->h:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/ds/d/h;->d:Ljava/lang/String;

    :cond_0
    if-nez p0, :cond_1

    iget-object v0, p1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput-boolean v2, v0, Lcom/fmm/ds/d/j;->a:Z

    :goto_0
    iget-object v0, p1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    const/4 v1, 0x3

    iput v1, v0, Lcom/fmm/ds/d/j;->c:I

    iget-object v0, p1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-object v1, p2, Lcom/fmm/ds/d/r;->k:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/d/j;->w:Ljava/lang/String;

    return-void

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput-boolean v1, v0, Lcom/fmm/ds/d/j;->a:Z

    goto :goto_0
.end method

.method public static a(ILjava/lang/String;)V
    .locals 3

    const-wide/16 v1, 0x1

    sget-object v0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    iget-object v0, v0, Lcom/fmm/ds/d/b;->e:[Ljava/lang/String;

    aput-object p1, v0, p0

    invoke-static {v1, v2}, Lcom/fmm/ds/d/c;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Not Exist ActivedProfile Db !!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    invoke-static {v1, v2, v0}, Lcom/fmm/ds/d/c;->a(JLcom/fmm/ds/d/b;)V

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/d/b;)V
    .locals 3

    const-wide/16 v1, 0x1

    invoke-static {v1, v2}, Lcom/fmm/ds/d/c;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Not Exist ActivedProfile Db !!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sput-object p0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    sget-object v0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    invoke-static {v1, v2, v0}, Lcom/fmm/ds/d/c;->a(JLcom/fmm/ds/d/b;)V

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/d/f;I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/fmm/ds/d/a;->e:Lcom/fmm/ds/d/q;

    iget-object v0, v0, Lcom/fmm/ds/d/q;->a:Lcom/fmm/ds/d/r;

    invoke-static {p0, v0}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/f;Lcom/fmm/ds/d/r;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/fmm/ds/d/f;Lcom/fmm/ds/d/r;)V
    .locals 1

    iget-object v0, p1, Lcom/fmm/ds/d/r;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/f;->a:Ljava/lang/String;

    iget-object v0, p1, Lcom/fmm/ds/d/r;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/f;->b:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/fmm/ds/d/r;->n:Z

    iput-boolean v0, p0, Lcom/fmm/ds/d/f;->c:Z

    iget-boolean v0, p1, Lcom/fmm/ds/d/r;->o:Z

    iput-boolean v0, p0, Lcom/fmm/ds/d/f;->d:Z

    iget-object v0, p1, Lcom/fmm/ds/d/r;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/f;->e:Ljava/lang/String;

    iget-object v0, p1, Lcom/fmm/ds/d/r;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;

    iget-object v0, p1, Lcom/fmm/ds/d/r;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/f;->j:Ljava/lang/String;

    iget-object v0, p1, Lcom/fmm/ds/d/r;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/f;->k:Ljava/lang/String;

    iget-object v0, p1, Lcom/fmm/ds/d/r;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/f;->l:Ljava/lang/String;

    const-string v0, "4"

    iput-object v0, p0, Lcom/fmm/ds/d/f;->n:Ljava/lang/String;

    const-string v0, "4"

    iput-object v0, p0, Lcom/fmm/ds/d/f;->o:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/fmm/ds/d/h;)V
    .locals 5

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/fmm/ds/d/i;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Not Exist Profile[%d] Db !!!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aput-object p0, v1, v0

    add-int/lit8 v1, v0, 0x1

    int-to-long v1, v1

    sget-object v3, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v0, v3, v0

    invoke-static {v1, v2, v0}, Lcom/fmm/ds/d/i;->a(JLcom/fmm/ds/d/h;)V

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/d/h;I)V
    .locals 5

    const/4 v4, 0x0

    add-int/lit8 v0, p1, 0x1

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/fmm/ds/d/i;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Not Exist Profile[%d] Db !!!"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p0, :cond_1

    sget-object v0, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aput-object p0, v0, p1

    add-int/lit8 v0, p1, 0x1

    int-to-long v0, v0

    invoke-static {v0, v1, p0}, Lcom/fmm/ds/d/i;->a(JLcom/fmm/ds/d/h;)V

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ProfileInfo is null!!!"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Lcom/fmm/ds/d/h;Lcom/fmm/ds/d/r;Z)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/d/r;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/d/r;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/h;->i:Ljava/lang/String;

    iput v2, p0, Lcom/fmm/ds/d/h;->f:I

    iput v1, p0, Lcom/fmm/ds/d/h;->n:I

    iget-object v0, p1, Lcom/fmm/ds/d/r;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    iput v1, p0, Lcom/fmm/ds/d/h;->o:I

    iget v0, p1, Lcom/fmm/ds/d/r;->i:I

    iput v0, p0, Lcom/fmm/ds/d/h;->c:I

    iget-object v0, p1, Lcom/fmm/ds/d/r;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/h;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput-boolean v2, v0, Lcom/fmm/ds/d/j;->a:Z

    iget-object v0, p0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    const/4 v1, 0x3

    iput v1, v0, Lcom/fmm/ds/d/j;->c:I

    iget-object v0, p0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-object v1, p1, Lcom/fmm/ds/d/r;->k:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/d/j;->w:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/fmm/ds/d/h;Z)V
    .locals 1

    sget-object v0, Lcom/fmm/ds/d/a;->e:Lcom/fmm/ds/d/q;

    iget-object v0, v0, Lcom/fmm/ds/d/q;->a:Lcom/fmm/ds/d/r;

    invoke-static {p0, v0, p1}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;Lcom/fmm/ds/d/r;Z)V

    return-void
.end method

.method private static a(Lcom/fmm/ds/d/k;Lcom/fmm/ds/d/j;)V
    .locals 2

    iget-boolean v0, p1, Lcom/fmm/ds/d/j;->a:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/fmm/ds/d/k;->e:I

    iget v1, p1, Lcom/fmm/ds/d/j;->i:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fmm/ds/d/k;->e:I

    iget v0, p0, Lcom/fmm/ds/d/k;->f:I

    iget v1, p1, Lcom/fmm/ds/d/j;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fmm/ds/d/k;->f:I

    iget v0, p0, Lcom/fmm/ds/d/k;->g:I

    iget v1, p1, Lcom/fmm/ds/d/j;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fmm/ds/d/k;->g:I

    iget v0, p0, Lcom/fmm/ds/d/k;->h:I

    iget v1, p1, Lcom/fmm/ds/d/j;->l:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fmm/ds/d/k;->h:I

    iget v0, p0, Lcom/fmm/ds/d/k;->i:I

    iget v1, p1, Lcom/fmm/ds/d/j;->m:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fmm/ds/d/k;->i:I

    iget v0, p0, Lcom/fmm/ds/d/k;->j:I

    iget v1, p1, Lcom/fmm/ds/d/j;->n:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fmm/ds/d/k;->j:I

    iget v0, p0, Lcom/fmm/ds/d/k;->k:I

    iget v1, p1, Lcom/fmm/ds/d/j;->o:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fmm/ds/d/k;->k:I

    iget v0, p0, Lcom/fmm/ds/d/k;->l:I

    iget v1, p1, Lcom/fmm/ds/d/j;->p:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fmm/ds/d/k;->l:I

    iget v0, p1, Lcom/fmm/ds/d/j;->c:I

    iput v0, p0, Lcom/fmm/ds/d/k;->a:I

    iget v0, p1, Lcom/fmm/ds/d/j;->d:I

    iput v0, p0, Lcom/fmm/ds/d/k;->b:I

    iget v0, p1, Lcom/fmm/ds/d/j;->e:I

    iput v0, p0, Lcom/fmm/ds/d/k;->c:I

    iget v0, p1, Lcom/fmm/ds/d/j;->f:I

    iput v0, p0, Lcom/fmm/ds/d/k;->d:I

    iget-object v0, p1, Lcom/fmm/ds/d/j;->C:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/d/k;->m:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public static a(Lcom/fmm/ds/d/p;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_1

    add-int/lit8 v2, v0, 0x1

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/fmm/ds/d/g;->a(J)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Not Exist NetworkProfile[%d] Db !!!"

    new-array v4, v6, [Ljava/lang/Object;

    add-int/lit8 v5, v0, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/fmm/ds/d/a;->d:Lcom/fmm/ds/d/p;

    iget-object v2, v2, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    iget-object v3, p0, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    aget-object v3, v3, v0

    aput-object v3, v2, v0

    add-int/lit8 v2, v0, 0x1

    int-to-long v2, v2

    sget-object v4, Lcom/fmm/ds/d/a;->d:Lcom/fmm/ds/d/p;

    iget-object v4, v4, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    aget-object v4, v4, v0

    invoke-static {v2, v3, v4}, Lcom/fmm/ds/d/g;->a(JLcom/fmm/ds/d/f;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Object;I)V
    .locals 3

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/d/b;

    iput v0, p0, Lcom/fmm/ds/d/b;->a:I

    :goto_0
    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/d/b;->d:[Ljava/lang/String;

    sget-object v2, Lcom/fmm/ds/d/a;->e:Lcom/fmm/ds/d/q;

    iget-object v2, v2, Lcom/fmm/ds/d/q;->a:Lcom/fmm/ds/d/r;

    iget-object v2, v2, Lcom/fmm/ds/d/r;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/fmm/ds/d/b;->e:[Ljava/lang/String;

    sget-object v2, Lcom/fmm/ds/d/a;->e:Lcom/fmm/ds/d/q;

    iget-object v2, v2, Lcom/fmm/ds/d/q;->a:Lcom/fmm/ds/d/r;

    iget-object v2, v2, Lcom/fmm/ds/d/r;->f:Ljava/lang/String;

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Z)V
    .locals 3

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "xdbSetCachedMapFlag : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v1, v1, v0

    iput-boolean p0, v1, Lcom/fmm/ds/d/h;->l:Z

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v1, v1, v0

    invoke-static {v1, v0}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    return-void
.end method

.method public static a(Lcom/fmm/ds/d/d;)Z
    .locals 6

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    const-string v1, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nProfileIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v1, v1, v0

    iput-object p0, v1, Lcom/fmm/ds/d/h;->t:Lcom/fmm/ds/d/d;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s%s%d.dat"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "data/data/com.fmm.ds/"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "map_cached.dat"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/fmm/ds/d/d;->a:[Lcom/fmm/ds/c/x;

    invoke-static {v0, v1}, Lcom/fmm/ds/d/o;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b()V
    .locals 5

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/fmm/ds/d/i;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Not Exist Profile[%d] Db !!!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    add-int/lit8 v1, v0, 0x1

    int-to-long v1, v1

    sget-object v3, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v0, v3, v0

    invoke-static {v1, v2, v0}, Lcom/fmm/ds/d/i;->a(JLcom/fmm/ds/d/h;)V

    goto :goto_0
.end method

.method public static b(I)V
    .locals 1

    sget-object v0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    iput p0, v0, Lcom/fmm/ds/d/b;->a:I

    sget-object v0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    invoke-static {v0}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/b;)V

    return-void
.end method

.method public static b(Lcom/fmm/ds/d/h;I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/fmm/ds/d/a;->e:Lcom/fmm/ds/d/q;

    iget-object v0, v0, Lcom/fmm/ds/d/q;->a:Lcom/fmm/ds/d/r;

    invoke-static {p1, p0, v0}, Lcom/fmm/ds/d/a;->a(ILcom/fmm/ds/d/h;Lcom/fmm/ds/d/r;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static c()Lcom/fmm/ds/d/h;
    .locals 3

    sget-boolean v0, Lcom/fmm/ds/d/a;->a:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fmm/ds/d/a;->a()V

    :cond_0
    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nProfileIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v0, v1, v0

    return-object v0
.end method

.method public static c(I)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    iget-object v0, v0, Lcom/fmm/ds/d/b;->d:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static d()I
    .locals 1

    sget-object v0, Lcom/fmm/ds/d/a;->c:Lcom/fmm/ds/d/b;

    iget v0, v0, Lcom/fmm/ds/d/b;->a:I

    return v0
.end method

.method public static d(I)Lcom/fmm/ds/d/j;
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v1

    sget-object v2, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v1, v2, v1

    if-nez p0, :cond_0

    iget-object v0, v1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    :cond_0
    return-object v0
.end method

.method public static e()Lcom/fmm/ds/d/p;
    .locals 1

    sget-object v0, Lcom/fmm/ds/d/a;->d:Lcom/fmm/ds/d/p;

    return-object v0
.end method

.method public static f()Z
    .locals 4

    new-instance v0, Lcom/fmm/ds/d/d;

    invoke-direct {v0}, Lcom/fmm/ds/d/d;-><init>()V

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nProfileIndex : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v2, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/fmm/ds/d/h;->t:Lcom/fmm/ds/d/d;

    sput-object v1, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    const-string v1, "contacts cachedmap info read. and reset"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/d;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public static g()Z
    .locals 3

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v0, v1, v0

    iget-boolean v0, v0, Lcom/fmm/ds/d/h;->l:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "xdbGetCachedMapFlag : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    return v0
.end method

.method public static h()V
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    const-string v0, ""

    invoke-static {}, Lcom/fmm/ds/c/au;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_1

    invoke-static {v0}, Lcom/fmm/ds/d/a;->d(I)Lcom/fmm/ds/d/j;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-boolean v5, v4, Lcom/fmm/ds/d/j;->a:Z

    if-eqz v5, :cond_0

    iput v1, v4, Lcom/fmm/ds/d/j;->l:I

    iput v1, v4, Lcom/fmm/ds/d/j;->i:I

    iput v1, v4, Lcom/fmm/ds/d/j;->j:I

    iput v1, v4, Lcom/fmm/ds/d/j;->k:I

    iput v1, v4, Lcom/fmm/ds/d/j;->p:I

    iput v1, v4, Lcom/fmm/ds/d/j;->m:I

    iput v1, v4, Lcom/fmm/ds/d/j;->n:I

    iput v1, v4, Lcom/fmm/ds/d/j;->o:I

    iput v1, v4, Lcom/fmm/ds/d/j;->q:I

    iput v1, v4, Lcom/fmm/ds/d/j;->r:I

    iput v1, v4, Lcom/fmm/ds/d/j;->s:I

    iput v1, v4, Lcom/fmm/ds/d/j;->t:I

    iput v1, v4, Lcom/fmm/ds/d/j;->u:I

    iput v1, v4, Lcom/fmm/ds/d/j;->v:I

    iput v6, v4, Lcom/fmm/ds/d/j;->f:I

    iput-object v2, v4, Lcom/fmm/ds/d/j;->C:Ljava/lang/String;

    const/16 v5, 0x9

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iput v5, v4, Lcom/fmm/ds/d/j;->D:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/fmm/ds/d/a;->b()V

    return-void
.end method

.method public static i()V
    .locals 4

    const/16 v3, 0x32

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v1, v1, v0

    new-instance v2, Lcom/fmm/ds/d/k;

    invoke-direct {v2}, Lcom/fmm/ds/d/k;-><init>()V

    iget-object v1, v1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    invoke-static {v2, v1}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/k;Lcom/fmm/ds/d/j;)V

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/fmm/ds/d/h;->o:I

    if-lt v1, v3, :cond_0

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/fmm/ds/d/l;->a(I)V

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v0, v1, v0

    iput v3, v0, Lcom/fmm/ds/d/h;->o:I

    :goto_0
    invoke-static {v2}, Lcom/fmm/ds/d/l;->a(Lcom/fmm/ds/d/k;)V

    invoke-static {}, Lcom/fmm/ds/d/a;->b()V

    return-void

    :cond_0
    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v0, v1, v0

    iget v1, v0, Lcom/fmm/ds/d/h;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/fmm/ds/d/h;->o:I

    goto :goto_0
.end method

.method public static j()I
    .locals 2

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    sget-object v1, Lcom/fmm/ds/d/a;->b:[Lcom/fmm/ds/d/h;

    aget-object v0, v1, v0

    iget v0, v0, Lcom/fmm/ds/d/h;->a:I

    return v0
.end method
