.class public Lcom/fmm/ds/d/g;
.super Ljava/lang/Object;


# direct methods
.method public static a(JLcom/fmm/ds/d/f;)V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    if-nez p2, :cond_1

    const-string v0, "NetworkProfileInfo is null!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-string v2, "net_profile_name"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "net_home_url"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "bearer_use"

    iget-boolean v3, p2, Lcom/fmm/ds/d/f;->c:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "proxy_use"

    iget-boolean v3, p2, Lcom/fmm/ds/d/f;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "proxy_address"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "proxy_port"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "proxy_user_id"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "proxy_password"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "proxy_type"

    iget v3, p2, Lcom/fmm/ds/d/f;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "apn"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->j:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "apn_login_id"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->k:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "apn_password"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->l:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "apn_type"

    iget v3, p2, Lcom/fmm/ds/d/f;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "gprs_dns1"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->n:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "gprs_dns2"

    iget-object v3, p2, Lcom/fmm/ds/d/f;->o:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "network_profile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_2
    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    :try_start_0
    const-string v0, "create table if not exists network_profile (rowid integer primary key autoincrement, net_profile_name text, net_home_url text, bearer_use boolean, proxy_use boolean, proxy_address text, proxy_port text, proxy_user_id text, proxy_password text, proxy_type integer, apn text, apn_login_id text, apn_password text, apn_type integer, gprs_dns1 text, gprs_dns2 text);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/d/f;)V
    .locals 4

    const/4 v1, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    :try_start_0
    const-string v2, "net_profile_name"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "net_home_url"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "bearer_use"

    iget-boolean v3, p0, Lcom/fmm/ds/d/f;->c:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "proxy_use"

    iget-boolean v3, p0, Lcom/fmm/ds/d/f;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "proxy_address"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "proxy_port"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "proxy_user_id"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "proxy_password"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "proxy_type"

    iget v3, p0, Lcom/fmm/ds/d/f;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "apn"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->j:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "apn_login_id"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->k:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "apn_password"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->l:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "apn_type"

    iget v3, p0, Lcom/fmm/ds/d/f;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "gprs_dns1"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->n:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "gprs_dns2"

    iget-object v3, p0, Lcom/fmm/ds/d/f;->o:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "network_profile"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public static a(J)Z
    .locals 14

    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/16 v0, 0x10

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowid"

    aput-object v0, v3, v10

    const-string v0, "net_profile_name"

    aput-object v0, v3, v12

    const/4 v0, 0x2

    const-string v1, "net_home_url"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "bearer_use"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "proxy_use"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "proxy_address"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "proxy_port"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "proxy_user_id"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "proxy_password"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "proxy_type"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "apn"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "apn_login_id"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "apn_password"

    aput-object v1, v3, v0

    const/16 v0, 0xd

    const-string v1, "apn_type"

    aput-object v1, v3, v0

    const/16 v0, 0xe

    const-string v1, "gprs_dns1"

    aput-object v1, v3, v0

    const/16 v0, 0xf

    const-string v1, "gprs_dns1"

    aput-object v1, v3, v0

    :try_start_0
    invoke-static {}, Lcom/fmm/ds/XDSApplication;->d()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    :try_start_1
    const-string v2, "network_profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-lez v1, :cond_6

    move v1, v12

    :goto_0
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    if-eqz v11, :cond_5

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    move-object v1, v11

    :goto_2
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move v0, v10

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v11

    :goto_3
    if-eqz v1, :cond_2

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_2
    if-eqz v11, :cond_3

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_2

    :cond_4
    move v0, v10

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v1, v10

    goto :goto_0
.end method

.method public static b(JLcom/fmm/ds/d/f;)Ljava/lang/Object;
    .locals 12

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v10, 0x0

    const/16 v0, 0x10

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowid"

    aput-object v0, v3, v2

    const-string v0, "net_profile_name"

    aput-object v0, v3, v1

    const-string v0, "net_home_url"

    aput-object v0, v3, v4

    const-string v0, "bearer_use"

    aput-object v0, v3, v5

    const/4 v0, 0x4

    const-string v1, "proxy_use"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "proxy_address"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "proxy_port"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "proxy_user_id"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "proxy_password"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "proxy_type"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "apn"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "apn_login_id"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "apn_password"

    aput-object v1, v3, v0

    const/16 v0, 0xd

    const-string v1, "apn_type"

    aput-object v1, v3, v0

    const/16 v0, 0xe

    const-string v1, "gprs_dns1"

    aput-object v1, v3, v0

    const/16 v0, 0xf

    const-string v1, "gprs_dns1"

    aput-object v1, v3, v0

    :try_start_0
    invoke-static {}, Lcom/fmm/ds/XDSApplication;->d()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    const/4 v1, 0x1

    :try_start_1
    const-string v2, "network_profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_6

    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->a:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->b:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p2, Lcom/fmm/ds/d/f;->c:Z

    :goto_1
    const/4 v1, 0x4

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    iput-boolean v1, p2, Lcom/fmm/ds/d/f;->d:Z

    :goto_2
    const/4 v1, 0x5

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->e:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->g:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->h:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/f;->i:I

    const/16 v1, 0xa

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->j:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->k:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->l:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/f;->m:I

    const/16 v1, 0xe

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->n:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/f;->o:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_3
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_4
    return-object p2

    :cond_2
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p2, Lcom/fmm/ds/d/f;->c:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_5
    if-eqz v1, :cond_3

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :cond_5
    const/4 v1, 0x1

    :try_start_4
    iput-boolean v1, p2, Lcom/fmm/ds/d/f;->d:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    :cond_6
    if-eqz v0, :cond_7

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_7
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v1, v10

    goto :goto_5

    :catchall_2
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v1, v10

    goto :goto_3
.end method
