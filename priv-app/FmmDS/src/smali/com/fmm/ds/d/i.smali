.class public Lcom/fmm/ds/d/i;
.super Ljava/lang/Object;


# direct methods
.method public static a(JLcom/fmm/ds/d/h;)V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    :try_start_0
    const-string v2, "obex_type"

    iget v3, p2, Lcom/fmm/ds/d/h;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cred_type"

    iget v3, p2, Lcom/fmm/ds/d/h;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "next_nonce"

    iget-object v3, p2, Lcom/fmm/ds/d/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "old_imsi"

    iget-object v3, p2, Lcom/fmm/ds/d/h;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ui_sync_type"

    iget v3, p2, Lcom/fmm/ds/d/h;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "server_url"

    iget-object v3, p2, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/ds/d/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "server_id"

    iget-object v3, p2, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/ds/d/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "server_pw"

    iget-object v3, p2, Lcom/fmm/ds/d/h;->i:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/ds/d/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "sync_result"

    iget v3, p2, Lcom/fmm/ds/d/h;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "auto_sync"

    iget-boolean v3, p2, Lcom/fmm/ds/d/h;->k:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "http_conn_mode"

    iget v3, p2, Lcom/fmm/ds/d/h;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "network_conn_index"

    iget v3, p2, Lcom/fmm/ds/d/h;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sync_item_log_count"

    iget v3, p2, Lcom/fmm/ds/d/h;->o:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cached_map_flag"

    iget-boolean v3, p2, Lcom/fmm/ds/d/h;->l:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "is_sync"

    iget-boolean v3, p2, Lcom/fmm/ds/d/h;->s:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "magic_number"

    iget v3, p2, Lcom/fmm/ds/d/h;->p:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "interval_start_time"

    iget-wide v3, p2, Lcom/fmm/ds/d/h;->q:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "interval"

    iget v3, p2, Lcom/fmm/ds/d/h;->r:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "profile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p2, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    invoke-static {p0, p1, v0}, Lcom/fmm/ds/d/m;->a(JLcom/fmm/ds/d/j;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    :try_start_0
    const-string v0, "create table if not exists profile (rowid integer primary key autoincrement, obex_type integer, cred_type integer, next_nonce text, old_imsi text, ui_sync_type integer, server_url text, server_id text, server_pw text, sync_result integer, auto_sync boolean, http_conn_mode integer, network_conn_index integer, sync_item_log_count integer, cached_map_flag boolean, is_sync boolean, magic_number integer, interval_start_time long,interval integer);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/fmm/ds/d/m;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/d/h;)V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    :try_start_0
    const-string v2, "obex_type"

    iget v3, p0, Lcom/fmm/ds/d/h;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cred_type"

    iget v3, p0, Lcom/fmm/ds/d/h;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "next_nonce"

    iget-object v3, p0, Lcom/fmm/ds/d/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "old_imsi"

    iget-object v3, p0, Lcom/fmm/ds/d/h;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ui_sync_type"

    iget v3, p0, Lcom/fmm/ds/d/h;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "server_url"

    iget-object v3, p0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/ds/d/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "server_id"

    iget-object v3, p0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/ds/d/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "server_pw"

    iget-object v3, p0, Lcom/fmm/ds/d/h;->i:Ljava/lang/String;

    invoke-static {v3}, Lcom/fmm/ds/d/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "sync_result"

    iget v3, p0, Lcom/fmm/ds/d/h;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "auto_sync"

    iget-boolean v3, p0, Lcom/fmm/ds/d/h;->k:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "http_conn_mode"

    iget v3, p0, Lcom/fmm/ds/d/h;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "network_conn_index"

    iget v3, p0, Lcom/fmm/ds/d/h;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sync_item_log_count"

    iget v3, p0, Lcom/fmm/ds/d/h;->o:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cached_map_flag"

    iget-boolean v3, p0, Lcom/fmm/ds/d/h;->l:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "is_sync"

    iget-boolean v3, p0, Lcom/fmm/ds/d/h;->s:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "magic_number"

    iget v3, p0, Lcom/fmm/ds/d/h;->p:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "interval_start_time"

    iget-wide v3, p0, Lcom/fmm/ds/d/h;->q:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "interval"

    iget v3, p0, Lcom/fmm/ds/d/h;->r:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "profile"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public static a(J)Z
    .locals 14

    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/16 v0, 0x13

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowid"

    aput-object v0, v3, v10

    const-string v0, "obex_type"

    aput-object v0, v3, v12

    const/4 v0, 0x2

    const-string v1, "cred_type"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "next_nonce"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "old_imsi"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "ui_sync_type"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "server_url"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "server_id"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "server_pw"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "sync_result"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "auto_sync"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "http_conn_mode"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "network_conn_index"

    aput-object v1, v3, v0

    const/16 v0, 0xd

    const-string v1, "sync_item_log_count"

    aput-object v1, v3, v0

    const/16 v0, 0xe

    const-string v1, "cached_map_flag"

    aput-object v1, v3, v0

    const/16 v0, 0xf

    const-string v1, "is_sync"

    aput-object v1, v3, v0

    const/16 v0, 0x10

    const-string v1, "magic_number"

    aput-object v1, v3, v0

    const/16 v0, 0x11

    const-string v1, "interval_start_time"

    aput-object v1, v3, v0

    const/16 v0, 0x12

    const-string v1, "interval"

    aput-object v1, v3, v0

    :try_start_0
    invoke-static {}, Lcom/fmm/ds/XDSApplication;->d()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x1

    :try_start_1
    const-string v2, "profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-lez v1, :cond_6

    move v1, v12

    :goto_0
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    if-eqz v11, :cond_5

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    move-object v1, v11

    :goto_2
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move v0, v10

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v11

    :goto_3
    if-eqz v1, :cond_2

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_2
    if-eqz v11, :cond_3

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_2

    :cond_4
    move v0, v10

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v1, v10

    goto :goto_0
.end method

.method public static b(JLcom/fmm/ds/d/h;)Ljava/lang/Object;
    .locals 12

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v10, 0x0

    const/16 v0, 0x13

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "rowid"

    aput-object v0, v3, v2

    const-string v0, "obex_type"

    aput-object v0, v3, v1

    const-string v0, "cred_type"

    aput-object v0, v3, v4

    const-string v0, "next_nonce"

    aput-object v0, v3, v5

    const/4 v0, 0x4

    const-string v1, "old_imsi"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "ui_sync_type"

    aput-object v1, v3, v0

    const/4 v0, 0x6

    const-string v1, "server_url"

    aput-object v1, v3, v0

    const/4 v0, 0x7

    const-string v1, "server_id"

    aput-object v1, v3, v0

    const/16 v0, 0x8

    const-string v1, "server_pw"

    aput-object v1, v3, v0

    const/16 v0, 0x9

    const-string v1, "sync_result"

    aput-object v1, v3, v0

    const/16 v0, 0xa

    const-string v1, "auto_sync"

    aput-object v1, v3, v0

    const/16 v0, 0xb

    const-string v1, "http_conn_mode"

    aput-object v1, v3, v0

    const/16 v0, 0xc

    const-string v1, "network_conn_index"

    aput-object v1, v3, v0

    const/16 v0, 0xd

    const-string v1, "sync_item_log_count"

    aput-object v1, v3, v0

    const/16 v0, 0xe

    const-string v1, "cached_map_flag"

    aput-object v1, v3, v0

    const/16 v0, 0xf

    const-string v1, "is_sync"

    aput-object v1, v3, v0

    const/16 v0, 0x10

    const-string v1, "magic_number"

    aput-object v1, v3, v0

    const/16 v0, 0x11

    const-string v1, "interval_start_time"

    aput-object v1, v3, v0

    const/16 v0, 0x12

    const-string v1, "interval"

    aput-object v1, v3, v0

    :try_start_0
    invoke-static {}, Lcom/fmm/ds/XDSApplication;->d()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    const/4 v1, 0x1

    :try_start_1
    const-string v2, "profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_7

    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->b:I

    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->c:I

    const/4 v1, 0x3

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/h;->d:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/h;->e:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->f:I

    const/4 v1, 0x6

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/d/n;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/d/n;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/d/n;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/fmm/ds/d/h;->i:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->j:I

    const/16 v1, 0xa

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p2, Lcom/fmm/ds/d/h;->k:Z

    :goto_1
    const/16 v1, 0xb

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->m:I

    const/16 v1, 0xc

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->n:I

    const/16 v1, 0xd

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->o:I

    const/16 v1, 0xe

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    iput-boolean v1, p2, Lcom/fmm/ds/d/h;->l:Z

    :goto_2
    const/16 v1, 0xf

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x0

    iput-boolean v1, p2, Lcom/fmm/ds/d/h;->s:Z

    :goto_3
    const/16 v1, 0x10

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->p:I

    const/16 v1, 0x11

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p2, Lcom/fmm/ds/d/h;->q:J

    const/16 v1, 0x12

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/fmm/ds/d/h;->r:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_4
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_5
    return-object p2

    :cond_2
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p2, Lcom/fmm/ds/d/h;->k:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_6
    if-eqz v1, :cond_3

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :cond_5
    const/4 v1, 0x1

    :try_start_4
    iput-boolean v1, p2, Lcom/fmm/ds/d/h;->l:Z

    goto :goto_2

    :cond_6
    const/4 v1, 0x1

    iput-boolean v1, p2, Lcom/fmm/ds/d/h;->s:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :cond_7
    if-eqz v0, :cond_8

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_8
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_5

    :catchall_1
    move-exception v0

    move-object v1, v10

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v1, v10

    goto :goto_4
.end method
