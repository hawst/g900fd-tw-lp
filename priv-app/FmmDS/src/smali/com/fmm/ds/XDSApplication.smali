.class public Lcom/fmm/ds/XDSApplication;
.super Landroid/app/Application;


# static fields
.field public static a:Z

.field public static b:Z

.field public static c:Landroid/os/Handler;

.field public static d:Ljava/lang/String;

.field public static e:Landroid/widget/Toast;

.field private static f:Landroid/content/Context;

.field private static g:Lcom/fmm/ds/b/l;

.field private static h:Lcom/fmm/ds/b/o;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    sput-boolean v0, Lcom/fmm/ds/XDSApplication;->a:Z

    sput-boolean v0, Lcom/fmm/ds/XDSApplication;->b:Z

    sput-object v1, Lcom/fmm/ds/XDSApplication;->c:Landroid/os/Handler;

    const-string v0, ""

    sput-object v0, Lcom/fmm/ds/XDSApplication;->d:Ljava/lang/String;

    sput-object v1, Lcom/fmm/ds/XDSApplication;->e:Landroid/widget/Toast;

    sput-object v1, Lcom/fmm/ds/XDSApplication;->g:Lcom/fmm/ds/b/l;

    sput-object v1, Lcom/fmm/ds/XDSApplication;->h:Lcom/fmm/ds/b/o;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static a()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/fmm/ds/XDSApplication;->f:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    sget-object v0, Lcom/fmm/ds/XDSApplication;->f:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    const-wide/16 v2, 0x3e8

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is null, retry..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v2, Lcom/fmm/ds/XDSApplication;->f:Landroid/content/Context;

    invoke-virtual {v2, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_2
    return-object v0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    :goto_3
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    goto :goto_3
.end method

.method public static a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    new-instance v0, Lcom/fmm/ds/c/bd;

    invoke-direct {v0}, Lcom/fmm/ds/c/bd;-><init>()V

    iput-object p1, v0, Lcom/fmm/ds/c/bd;->a:Ljava/lang/Object;

    iput-object v1, v0, Lcom/fmm/ds/c/bd;->b:Ljava/lang/Object;

    if-eqz p2, :cond_0

    iput-object p2, v0, Lcom/fmm/ds/c/bd;->b:Ljava/lang/Object;

    :cond_0
    :goto_0
    new-instance v3, Lcom/fmm/ds/c/bc;

    invoke-direct {v3}, Lcom/fmm/ds/c/bc;-><init>()V

    sget-object v1, Lcom/fmm/ds/XDSApplication;->c:Landroid/os/Handler;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    const/4 v1, 0x5

    if-ge v2, v1, :cond_1

    :try_start_0
    const-string v1, "Waiting for hAutoConnecting Handler create"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-wide/16 v4, 0x1f4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    sget-object v1, Lcom/fmm/ds/XDSApplication;->c:Landroid/os/Handler;

    if-eqz v1, :cond_2

    :cond_1
    if-eqz v3, :cond_3

    :try_start_1
    sget-object v1, Lcom/fmm/ds/XDSApplication;->c:Landroid/os/Handler;

    if-eqz v1, :cond_3

    iput p0, v3, Lcom/fmm/ds/c/bc;->a:I

    iput-object v0, v3, Lcom/fmm/ds/c/bc;->b:Lcom/fmm/ds/c/bd;

    sget-object v0, Lcom/fmm/ds/XDSApplication;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget-object v1, Lcom/fmm/ds/XDSApplication;->c:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    :try_start_2
    const-string v0, "Can\'t send message"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(ILjava/util/List;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/fmm/ds/XDSApplication;->b(ILjava/util/List;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v0, Lcom/fmm/ds/XDSApplication;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    sget-object v0, Lcom/fmm/ds/XDSApplication;->e:Landroid/widget/Toast;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p0, v0, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/fmm/ds/XDSApplication;->e:Landroid/widget/Toast;

    :cond_0
    sget-object v0, Lcom/fmm/ds/XDSApplication;->e:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/fmm/ds/XDSApplication;->e:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const-string v0, ""

    :try_start_0
    sget-object v1, Lcom/fmm/ds/XDSApplication;->f:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_2

    aget-object v3, v2, v1

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exist AccountType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v1

    invoke-virtual {v4}, Landroid/accounts/Account;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    aget-object v3, v2, v1

    iget-object v0, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "AccountManager is null!!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not Exist AccountType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :cond_3
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b()V
    .locals 1

    sget-object v0, Lcom/fmm/ds/XDSApplication;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/content/Context;)V

    return-void
.end method

.method private static b(ILjava/util/List;)V
    .locals 1

    invoke-static {p0}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "profileInfo is null"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p0, :cond_0

    iget-object v0, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-boolean v0, v0, Lcom/fmm/ds/d/j;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/fmm/ds/XDSApplication;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->b()V

    :cond_0
    sget-object v0, Lcom/fmm/ds/XDSApplication;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static d()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    new-instance v0, Lcom/fmm/ds/d/e;

    sget-object v1, Lcom/fmm/ds/XDSApplication;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/fmm/ds/d/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/fmm/ds/d/e;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static e()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    new-instance v0, Lcom/fmm/ds/d/e;

    sget-object v1, Lcom/fmm/ds/XDSApplication;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/fmm/ds/d/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/fmm/ds/d/e;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static f()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v0

    const-string v1, "com.osp.app.signin"

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "Fmm ServerID is empty!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ServerID is not empty!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v3, v1}, Lcom/fmm/ds/d/a;->a(ILjava/lang/String;)V

    invoke-static {v0, v3}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "Fmm ServerID is not empty!!"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "ServerID is empty!!"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/fmm/ds/d/a;->a(ILjava/lang/String;)V

    invoke-static {v0, v3}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    goto :goto_0
.end method

.method public static g()V
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/fmm/ds/XDSApplication;->d()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v0

    const-string v1, "com.osp.app.signin"

    invoke-static {v1}, Lcom/fmm/ds/XDSApplication;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "Fmm ServerID is empty!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "Fmm ServerID is not empty!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/fmm/ds/XDSApplication;->i()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "NO Update ServerURL"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v2, "Update ServerURL!!"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    goto :goto_0

    :cond_2
    const-string v2, "profileInfo ServerUrl is empty!!"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;Z)V

    :goto_1
    iget-object v2, v0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v1, "NO Update ServerURL"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :cond_3
    :goto_2
    invoke-static {v0, v3}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    goto :goto_0

    :cond_4
    invoke-static {v0, v3}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;Z)V

    goto :goto_1

    :cond_5
    const-string v2, "Update ServerURL!!"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    goto :goto_2
.end method

.method private i()Ljava/lang/String;
    .locals 8

    const/4 v6, 0x0

    const-string v0, "content://com.sec.pcw.device/value"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/XDSApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-nez v2, :cond_3

    :try_start_1
    const-string v0, "xdsGetServerUrlFromDSM FMM db is null!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v6

    :goto_0
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/fmm/ds/a/b;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "CHU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "CHM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "CTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "CHC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    const-string v0, "https://syn.samsungdive.cn/sync"

    :cond_2
    :goto_2
    return-object v0

    :cond_3
    :try_start_2
    const-string v0, "svalue"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    const/16 v3, 0x8

    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "xdsGetServerUrlFromDSM dbDECryptionUrl : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    :goto_3
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v2, v6

    :goto_4
    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :cond_5
    const-string v0, "https://syn.samsungdive.com/sync"

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v2, v6

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    move-object v6, v2

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v6, v2

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_3

    :cond_6
    move-object v0, v6

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {p0}, Lcom/fmm/ds/XDSApplication;->a(Landroid/content/Context;)V

    const-string v0, "FMMDS Application Start"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sput-object p0, Lcom/fmm/ds/XDSApplication;->f:Landroid/content/Context;

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->g()V

    invoke-static {}, Lcom/fmm/ds/d/a;->a()V

    const-string v0, "onCreate Db Initialized"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/XDSApplication;->h:Lcom/fmm/ds/b/o;

    if-nez v0, :cond_0

    new-instance v0, Lcom/fmm/ds/b/o;

    invoke-direct {v0}, Lcom/fmm/ds/b/o;-><init>()V

    sput-object v0, Lcom/fmm/ds/XDSApplication;->h:Lcom/fmm/ds/b/o;

    :cond_0
    sget-object v0, Lcom/fmm/ds/XDSApplication;->g:Lcom/fmm/ds/b/l;

    if-nez v0, :cond_1

    new-instance v0, Lcom/fmm/ds/b/l;

    invoke-direct {v0}, Lcom/fmm/ds/b/l;-><init>()V

    sput-object v0, Lcom/fmm/ds/XDSApplication;->g:Lcom/fmm/ds/b/l;

    :cond_1
    new-instance v0, Lcom/fmm/ds/b/a;

    invoke-direct {v0}, Lcom/fmm/ds/b/a;-><init>()V

    invoke-virtual {v0, p0}, Lcom/fmm/ds/b/a;->a(Landroid/content/Context;)V

    sget-object v1, Lcom/fmm/ds/XDSApplication;->g:Lcom/fmm/ds/b/l;

    iput-object v0, v1, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->f()V

    invoke-direct {p0}, Lcom/fmm/ds/XDSApplication;->h()V

    new-instance v0, Lcom/fmm/ds/a;

    invoke-direct {v0, p0}, Lcom/fmm/ds/a;-><init>(Lcom/fmm/ds/XDSApplication;)V

    sput-object v0, Lcom/fmm/ds/XDSApplication;->c:Landroid/os/Handler;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/fmm/ds/XDSApplication;->a:Z

    return-void
.end method
