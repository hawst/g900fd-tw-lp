.class public Lcom/fmm/ds/b/e;
.super Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:I

.field public k:Z

.field public l:Z

.field public m:[Lcom/fmm/ds/b/g;

.field public n:[Lcom/fmm/ds/b/f;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/fmm/ds/b/e;->j:I

    new-array v0, v1, [Lcom/fmm/ds/b/g;

    iput-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    new-array v0, v1, [Lcom/fmm/ds/b/f;

    iput-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    return-void
.end method

.method public static c(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static d(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v0, "1.2"

    iput-object v0, p0, Lcom/fmm/ds/b/e;->a:Ljava/lang/String;

    const-string v0, "phone"

    iput-object v0, p0, Lcom/fmm/ds/b/e;->i:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->b:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->c:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->d:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->e:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->g:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->f:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->h:Ljava/lang/String;

    iput-boolean v4, p0, Lcom/fmm/ds/b/e;->l:Z

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/fmm/ds/b/e;->k:Z

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    new-instance v1, Lcom/fmm/ds/b/g;

    invoke-direct {v1}, Lcom/fmm/ds/b/g;-><init>()V

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/ds/b/g;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    const-string v1, "Address Book"

    iput-object v1, v0, Lcom/fmm/ds/b/g;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->d:Lcom/fmm/ds/b/h;

    const-string v1, "text/x-vcard"

    iput-object v1, v0, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->d:Lcom/fmm/ds/b/h;

    const-string v1, "2.1"

    iput-object v1, v0, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->e:Lcom/fmm/ds/b/h;

    const-string v1, "text/x-vcard"

    iput-object v1, v0, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->e:Lcom/fmm/ds/b/h;

    const-string v1, "2.1"

    iput-object v1, v0, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    const/16 v1, 0x1f4

    iput v1, v0, Lcom/fmm/ds/b/g;->c:I

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    const/4 v1, 0x2

    aput v1, v0, v5

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    aput v4, v0, v4

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    const/4 v1, 0x2

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    const/4 v1, 0x4

    aput v1, v0, v8

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    const/4 v1, 0x4

    aput v7, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    aput v6, v0, v6

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    const/4 v1, 0x7

    aput v1, v0, v7

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v5

    iput v4, v0, Lcom/fmm/ds/b/g;->i:I

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    new-instance v1, Lcom/fmm/ds/b/f;

    invoke-direct {v1}, Lcom/fmm/ds/b/f;-><init>()V

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    const-string v1, "2.1"

    iput-object v1, v0, Lcom/fmm/ds/b/f;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    const/16 v1, 0x64

    iput v1, v0, Lcom/fmm/ds/b/f;->c:I

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    const-string v1, "text/x-vcard"

    iput-object v1, v0, Lcom/fmm/ds/b/f;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v5

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v4

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/4 v1, 0x2

    const-string v2, "BEGIN"

    aput-object v2, v0, v4

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const-string v2, "VCARD"

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v8

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/4 v1, 0x4

    const-string v2, "0"

    aput-object v2, v0, v8

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const-string v2, "0"

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v6

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const-string v1, "END"

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v7

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/4 v1, 0x7

    const-string v2, "VCARD"

    aput-object v2, v0, v7

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x8

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x9

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0xa

    const-string v3, "VERSION"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0xb

    const-string v3, "2.1"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0xc

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0xd

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0xe

    const-string v3, "N"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0xf

    const-string v3, "chr"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x10

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x11

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x12

    const-string v3, "FN"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x13

    const-string v3, "chr"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x14

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x15

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x16

    const-string v3, "TEL"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x17

    const-string v3, "chr"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/4 v2, 0x7

    aput v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x18

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/4 v1, 0x2

    aput v1, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x19

    const-string v3, "TYPE"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x1a

    const-string v3, "WORK"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x1b

    const-string v3, "HOME"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x1c

    const-string v3, "FAX"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x1d

    const-string v3, "VIDEO"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x1e

    const-string v3, "CELL"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x1f

    const-string v3, "VOICE"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/16 v2, 0x8

    aput v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x20

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x21

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x22

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x23

    const-string v3, "EMAIL"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/4 v2, 0x7

    aput v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x24

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/4 v1, 0x2

    aput v1, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x25

    const-string v3, "TYPE"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x26

    const-string v3, "INTERNET"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x27

    const-string v3, "WORK"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x28

    const-string v3, "HOME"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/16 v1, 0x8

    aput v1, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x29

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x2a

    const-string v3, "chr"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x2b

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x2c

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x2d

    const-string v3, "URL"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x2e

    const-string v3, "chr"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x2f

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x30

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x31

    const-string v3, "TITLE"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x32

    const-string v3, "chr"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x33

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x34

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x35

    const-string v3, "ORG"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x36

    const-string v3, "chr"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x37

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x38

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x39

    const-string v3, "NICKNAME"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x3a

    const-string v3, "chr"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x3b

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x3c

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x3d

    const-string v3, "NOTE"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x3e

    const-string v3, "chr"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x3f

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x40

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x41

    const-string v3, "BDAY"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x42

    const-string v3, "datetime"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x43

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x44

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x45

    const-string v3, "X-ANNIVERSARY"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x46

    const-string v3, "datetime"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x47

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x48

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x49

    const-string v3, "ADR"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x4a

    const-string v3, "chr"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/4 v1, 0x7

    aput v1, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x4b

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/4 v2, 0x2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x4c

    const-string v3, "TYPE"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x4d

    const-string v3, "WORK"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x4e

    const-string v3, "HOME"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/16 v1, 0x8

    aput v1, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x4f

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x50

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x51

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x52

    const-string v3, "X-IM"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x53

    const-string v3, "chr"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x54

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v6, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x55

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v5, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x56

    const-string v3, "PHOTO"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/16 v1, 0x9

    aput v1, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x57

    const-string v3, "1"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v8, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x58

    const-string v3, "bin"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/4 v1, 0x7

    aput v1, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x59

    const-string v3, "0"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/4 v2, 0x2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x5a

    const-string v3, "TYPE"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x5b

    const-string v3, "JPEG"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x5c

    const-string v3, "GIF"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x5d

    const-string v3, "BMP"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x5e

    const-string v3, "PNG"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x5f

    const-string v3, "WBMP"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x60

    const-string v3, "TIFF"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x61

    const-string v3, "AGIF"

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    const/16 v2, 0x8

    aput v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v2, 0x62

    const-string v3, "0"

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->d:[I

    aput v7, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    const/16 v1, 0x63

    const-string v3, "0"

    aput-object v3, v0, v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "contacts deviceinfo type count : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "1.1"

    iput-object v0, p0, Lcom/fmm/ds/b/e;->a:Ljava/lang/String;

    const-string v0, "phone"

    iput-object v0, p0, Lcom/fmm/ds/b/e;->i:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->b:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->c:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->d:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->e:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->g:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->f:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/a/b;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/b/e;->h:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/fmm/ds/b/e;->l:Z

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/fmm/ds/b/e;->k:Z

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    new-instance v1, Lcom/fmm/ds/b/g;

    invoke-direct {v1}, Lcom/fmm/ds/b/g;-><init>()V

    aput-object v1, v0, v4

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/ds/b/g;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    const-string v1, "Address Book"

    iput-object v1, v0, Lcom/fmm/ds/b/g;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->d:Lcom/fmm/ds/b/h;

    const-string v1, "text/x-vcard"

    iput-object v1, v0, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->d:Lcom/fmm/ds/b/h;

    const-string v1, "2.1"

    iput-object v1, v0, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->e:Lcom/fmm/ds/b/h;

    const-string v1, "text/x-vcard"

    iput-object v1, v0, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->e:Lcom/fmm/ds/b/h;

    const-string v1, "2.1"

    iput-object v1, v0, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    const/16 v1, 0x1f4

    iput v1, v0, Lcom/fmm/ds/b/g;->c:I

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    aput v2, v0, v4

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    aput v3, v0, v3

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    aput v5, v0, v2

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    aput v6, v0, v5

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    const/4 v1, 0x6

    aput v1, v0, v6

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    const/4 v1, 0x5

    const/4 v2, 0x5

    aput v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/b/g;->h:[I

    const/4 v1, 0x6

    const/4 v2, 0x7

    aput v2, v0, v1

    iget-object v0, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v0, v0, v4

    iput v3, v0, Lcom/fmm/ds/b/g;->i:I

    return-void
.end method
