.class public Lcom/fmm/ds/b/a;
.super Ljava/lang/Object;


# static fields
.field public static a:Lcom/fmm/ds/c/bi;

.field public static e:Z

.field private static i:Z

.field private static j:Z

.field private static k:Z


# instance fields
.field public b:Lcom/fmm/ds/b/q;

.field public c:Lcom/fmm/ds/f/a;

.field public d:Landroid/content/Context;

.field private final f:I

.field private g:I

.field private h:I

.field private l:J

.field private m:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sput-boolean v1, Lcom/fmm/ds/b/a;->e:Z

    sput-boolean v1, Lcom/fmm/ds/b/a;->i:Z

    sput-boolean v1, Lcom/fmm/ds/b/a;->j:Z

    sput-boolean v1, Lcom/fmm/ds/b/a;->k:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    iput v0, p0, Lcom/fmm/ds/b/a;->f:I

    iput v1, p0, Lcom/fmm/ds/b/a;->g:I

    iput v1, p0, Lcom/fmm/ds/b/a;->h:I

    return-void
.end method

.method public static a(Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_bStopping : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sput-boolean p0, Lcom/fmm/ds/b/a;->j:Z

    return-void
.end method

.method public static c(Z)V
    .locals 2

    sput-boolean p0, Lcom/fmm/ds/b/a;->e:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gSuspendState : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/ds/b/a;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static d(Z)V
    .locals 0

    sput-boolean p0, Lcom/fmm/ds/b/a;->k:Z

    return-void
.end method

.method public static m()Z
    .locals 1

    sget-boolean v0, Lcom/fmm/ds/b/a;->i:Z

    return v0
.end method

.method public static n()Z
    .locals 1

    sget-boolean v0, Lcom/fmm/ds/b/a;->j:Z

    return v0
.end method

.method public static o()Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gSuspendState : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/fmm/ds/b/a;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-boolean v0, Lcom/fmm/ds/b/a;->e:Z

    return v0
.end method

.method public static q()Z
    .locals 1

    sget-boolean v0, Lcom/fmm/ds/b/a;->k:Z

    return v0
.end method

.method private u()I
    .locals 3

    const/16 v0, 0x1d

    const/16 v1, 0x1e

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v2, v2, Lcom/fmm/ds/c/bi;->i:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/16 v0, 0x10

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x16

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x12

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x13

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_6
    move v0, v1

    goto :goto_0

    :pswitch_7
    move v0, v1

    goto :goto_0

    :pswitch_8
    move v0, v1

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x21

    goto :goto_0

    :pswitch_a
    const/16 v0, 0x11

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x12
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(I)I
    .locals 10

    const/4 v9, 0x0

    const/16 v1, 0x1e

    const/16 v0, 0x17

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "abortCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    move v1, v0

    :goto_0
    :sswitch_0
    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    iput v8, v0, Lcom/fmm/ds/d/h;->j:I

    iput-boolean v7, v0, Lcom/fmm/ds/d/h;->s:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aborted in "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "msgid=[%d], ws.state=[%d]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v5, v5, Lcom/fmm/ds/c/bi;->e:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v7

    sget-object v5, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v5, v5, Lcom/fmm/ds/c/bi;->n:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v7}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_1
    if-eqz v0, :cond_1

    const/16 v2, 0xfb

    if-ne p1, v2, :cond_0

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v2, v2, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    const/4 v3, 0x3

    invoke-static {v2, v0, v3, v9}, Lcom/fmm/ds/b/j;->a(Ljava/lang/Object;Lcom/fmm/ds/d/u;ILjava/lang/String;)V

    :goto_2
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_1

    :sswitch_1
    const/16 v0, 0x1b

    move v1, v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1c

    move v1, v0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0xd

    move v1, v0

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    move v1, v0

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x19

    move v1, v0

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x18

    move v1, v0

    goto/16 :goto_0

    :sswitch_7
    move v1, v0

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x1f

    move v1, v0

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x20

    move v1, v0

    goto/16 :goto_0

    :sswitch_a
    invoke-direct {p0}, Lcom/fmm/ds/b/a;->u()I

    move-result v0

    move v1, v0

    goto/16 :goto_0

    :cond_0
    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v2, v2, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v2, v0, v8, v9}, Lcom/fmm/ds/b/j;->a(Ljava/lang/Object;Lcom/fmm/ds/d/u;ILjava/lang/String;)V

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-virtual {v0}, Lcom/fmm/ds/b/q;->c()I

    invoke-virtual {p0, v7, v1}, Lcom/fmm/ds/b/a;->a(ZI)V

    return v7

    :sswitch_data_0
    .sparse-switch
        0xc0 -> :sswitch_7
        0xef -> :sswitch_9
        0xf0 -> :sswitch_0
        0xf1 -> :sswitch_8
        0xf2 -> :sswitch_0
        0xf7 -> :sswitch_a
        0xf9 -> :sswitch_1
        0xfb -> :sswitch_6
        0xfc -> :sswitch_5
        0xfd -> :sswitch_4
        0xfe -> :sswitch_3
        0xff -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/fmm/ds/b/a;->d:Landroid/content/Context;

    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v1, p0, Lcom/fmm/ds/b/a;->h:I

    sput-boolean v1, Lcom/fmm/ds/b/a;->i:Z

    sput-boolean v1, Lcom/fmm/ds/b/a;->j:Z

    sput-boolean v1, Lcom/fmm/ds/b/a;->k:Z

    new-instance v0, Lcom/fmm/ds/b/q;

    invoke-direct {v0, p1}, Lcom/fmm/ds/b/q;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    new-instance v0, Lcom/fmm/ds/f/a;

    invoke-direct {v0, p1}, Lcom/fmm/ds/f/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    return-void
.end method

.method a(ZI)V
    .locals 4

    const/16 v3, 0x33

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->i()V

    invoke-virtual {p0, v1}, Lcom/fmm/ds/b/a;->b(Z)V

    invoke-static {v1}, Lcom/fmm/ds/b/a;->a(Z)V

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->t()V

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->r()V

    if-eqz p1, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/c/ba;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/ds/a/c;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2, v3}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    :cond_0
    :goto_0
    invoke-static {}, Lcom/fmm/ds/c/ba;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/fmm/ds/b/a;->d(Z)V

    const-string v0, "Queue isn\'t empty, Next connect will Start"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x3

    invoke-static {v0, v2, v2}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_1
    invoke-static {v1}, Lcom/fmm/ds/c/ba;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/b/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/ds/a/c;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v2, v3}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    :cond_2
    const/16 v0, 0xc

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v0, p2}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_3
    const-string v0, "Queue is empty"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/fmm/ds/b/a;->d(Z)V

    goto :goto_1
.end method

.method a()Z
    .locals 5

    const-wide/16 v3, 0x0

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    const/4 v2, 0x0

    const-string v0, "called"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v3, v4}, Lcom/fmm/ds/c/av;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/ak;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ak;)I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v3, v4}, Lcom/fmm/ds/c/av;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/ak;

    goto :goto_0

    :cond_0
    return v2
.end method

.method a(Ljava/lang/Object;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->j()V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput-object p1, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v1, v0, Lcom/fmm/ds/c/bi;->n:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v2, v0, Lcom/fmm/ds/c/bi;->f:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const-wide/16 v3, 0x1

    iput-wide v3, v0, Lcom/fmm/ds/c/bi;->e:J

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v1, v0, Lcom/fmm/ds/c/bi;->d:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v1, v0, Lcom/fmm/ds/c/bi;->X:I

    check-cast p1, Lcom/fmm/ds/d/h;

    iget-object v0, p1, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ServerURL is null!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v3}, Lcom/fmm/ds/b/x;->a(Ljava/lang/Object;)I

    move-result v3

    iput v3, v0, Lcom/fmm/ds/c/bi;->m:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v3}, Lcom/fmm/ds/b/x;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/c/bi;->t:Ljava/lang/String;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v3}, Lcom/fmm/ds/b/x;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/c/bi;->u:Ljava/lang/String;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v3}, Lcom/fmm/ds/b/x;->d(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v3}, Lcom/fmm/ds/b/x;->e(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/c/bi;->x:Ljava/lang/String;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v3}, Lcom/fmm/ds/b/x;->f(Ljava/lang/Object;)I

    move-result v3

    iput v3, v0, Lcom/fmm/ds/c/bi;->l:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v3}, Lcom/fmm/ds/b/x;->g(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/c/bi;->y:Ljava/lang/String;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {}, Lcom/fmm/ds/a/b;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/c/bi;->z:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/d/a;->j()I

    move-result v0

    if-lez v0, :cond_4

    invoke-static {}, Lcom/fmm/ds/d/a;->j()I

    move-result v0

    iget-object v3, p0, Lcom/fmm/ds/b/a;->d:Landroid/content/Context;

    invoke-static {v3}, Lcom/fmm/ds/a/c;->b(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "Remote Funtions Checked off."

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    neg-int v0, v0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SAN sessionID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/fmm/ds/c/bi;->B:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    invoke-static {v0}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v0

    iput v1, v0, Lcom/fmm/ds/d/h;->a:I

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    :goto_1
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/b/x;->h(Ljava/lang/Object;)I

    move-result v1

    iput v1, v0, Lcom/fmm/ds/c/bi;->H:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->H:I

    if-ne v0, v2, :cond_3

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->u:Ljava/lang/String;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "value is Default, check again!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {}, Lcom/fmm/ds/a/b;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/ds/c/bi;->u:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    invoke-static {v0}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v0

    invoke-static {}, Lcom/fmm/ds/a/b;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/ds/d/h;->i:Ljava/lang/String;

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    :cond_3
    move v0, v2

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/ds/c/bi;->B:Ljava/lang/String;

    goto :goto_1
.end method

.method public b(I)I
    .locals 2

    const/16 v1, -0xa

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/16 v0, -0x64

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    :goto_0
    return v0

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail by tplCreateException ws.state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, -0x6

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_0

    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail by tplWbxmlParsingException ws.state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, -0x5

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_0

    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail by tplReceiveException ws.state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, -0x4

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_0

    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail by tplSendException ws.state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, -0x3

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_0

    :pswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail by XDS_AGENT_CMDSYNC_FAIL ws.state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, -0x8

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto/16 :goto_0

    :pswitch_6
    const/16 v0, -0xb

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto/16 :goto_0

    :pswitch_7
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->r:I

    packed-switch v0, :pswitch_data_1

    :pswitch_8
    iput v1, p0, Lcom/fmm/ds/b/a;->h:I

    :goto_1
    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto/16 :goto_0

    :pswitch_9
    const/16 v0, -0x9

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_1

    :pswitch_a
    iput v1, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_1

    :pswitch_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail by authFailException ws.state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, -0x2

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xb
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_b
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x5
        :pswitch_a
        :pswitch_8
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public b(Ljava/lang/Object;)I
    .locals 2

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/a;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->O:Lcom/fmm/ds/c/as;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/f/a;->a(Lcom/fmm/ds/c/as;)I

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->d()I

    move-result v0

    goto :goto_0
.end method

.method b()V
    .locals 2

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->M:Lcom/fmm/ds/c/ah;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->M:Lcom/fmm/ds/c/ah;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ah;)I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/fmm/ds/c/bi;->M:Lcom/fmm/ds/c/ah;

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 2

    sput-boolean p1, Lcom/fmm/ds/b/a;->i:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bSync : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/ds/a/c;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    :cond_0
    invoke-static {}, Lcom/fmm/ds/b/i;->a()V

    return-void
.end method

.method c()I
    .locals 12

    const/16 v11, 0x6a

    const/4 v10, 0x2

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v1, 0x0

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    new-instance v2, Lcom/fmm/ds/c/at;

    invoke-direct {v2}, Lcom/fmm/ds/c/at;-><init>()V

    iput-object v2, v0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Start to create init package. "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "msgid=[%d], ws.state=[%d]"

    new-array v5, v10, [Ljava/lang/Object;

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v6, v6, Lcom/fmm/ds/c/bi;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v6, v6, Lcom/fmm/ds/c/bi;->n:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v8, v0, Lcom/fmm/ds/c/bi;->f:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->a()V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->a()V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-static {v0, v3}, Lcom/fmm/ds/b/j;->a(Ljava/lang/Object;Lcom/fmm/ds/c/av;)I

    move-result v0

    if-eq v0, v8, :cond_0

    const/16 v0, -0x64

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_1
    if-eqz v0, :cond_1

    iput-boolean v1, v0, Lcom/fmm/ds/d/u;->k:Z

    iput-boolean v1, v0, Lcom/fmm/ds/d/u;->l:Z

    iput v9, v0, Lcom/fmm/ds/d/u;->n:I

    iput v9, v0, Lcom/fmm/ds/d/u;->o:I

    iput-boolean v1, v0, Lcom/fmm/ds/d/u;->u:Z

    iput-boolean v1, v0, Lcom/fmm/ds/d/u;->v:Z

    iput v1, v0, Lcom/fmm/ds/d/u;->p:I

    iput v1, v0, Lcom/fmm/ds/d/u;->q:I

    iput v1, v0, Lcom/fmm/ds/d/u;->r:I

    new-instance v3, Lcom/fmm/ds/c/av;

    invoke-direct {v3}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v3, v0, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    invoke-virtual {v2, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/as;)V

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "-//SYNCML//DTD SyncML 1.1//EN"

    const-string v3, "-//SYNCML//DTD SyncML 1.1//EN"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v1, v11, v0, v3}, Lcom/fmm/ds/c/at;->a(IILjava/lang/String;I)I

    :goto_2
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v0}, Lcom/fmm/ds/b/b;->c(Lcom/fmm/ds/c/bi;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->D:Lcom/fmm/ds/c/an;

    invoke-virtual {v2, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/an;)I

    invoke-virtual {v2}, Lcom/fmm/ds/c/at;->b()I

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->a()Z

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v0}, Lcom/fmm/ds/b/e;->c(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/fmm/ds/b/e;

    invoke-direct {v0}, Lcom/fmm/ds/b/e;-><init>()V

    const-string v3, "making DEVINF to send.."

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v3

    if-nez v3, :cond_6

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Lcom/fmm/ds/b/e;->b(Ljava/lang/Object;)V

    :goto_3
    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v3, v0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/b/e;)Lcom/fmm/ds/c/ae;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ae;)I

    :cond_2
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v0}, Lcom/fmm/ds/b/e;->d(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "xdsRequestDeviceInfo() == true"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v0}, Lcom/fmm/ds/b/b;->d(Lcom/fmm/ds/c/bi;)Lcom/fmm/ds/c/t;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/t;)I

    :cond_3
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_4
    if-eqz v0, :cond_7

    iget-object v3, v0, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "build alert command source [%s], target [%s]"

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    aput-object v6, v5, v1

    iget-object v6, v0, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :cond_4
    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v3, v0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/f;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/f;)I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_4

    :cond_5
    const-string v0, "-//SYNCML//DTD SyncML 1.2//EN"

    const-string v3, "-//SYNCML//DTD SyncML 1.2//EN"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v1, v11, v0, v3}, Lcom/fmm/ds/c/at;->a(IILjava/lang/String;I)I

    goto/16 :goto_2

    :cond_6
    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Lcom/fmm/ds/b/e;->a(Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_7
    invoke-virtual {v2, v8}, Lcom/fmm/ds/c/at;->a(Z)I

    invoke-virtual {v2}, Lcom/fmm/ds/c/at;->a()I

    move v0, v1

    goto/16 :goto_0
.end method

.method public c(Ljava/lang/Object;)I
    .locals 6

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/fmm/ds/b/c;->a()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/fmm/ds/b/a;->m:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/fmm/ds/b/a;->l:J

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/a;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fail by initFailException 2"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iput v3, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-virtual {v0}, Lcom/fmm/ds/b/q;->b()I

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->c()I

    move-result v0

    if-gez v0, :cond_1

    const-string v0, "fail by initFailException 3"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iput v3, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/fmm/ds/b/a;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "xdsInitSync"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/fmm/ds/b/a;->c(Z)V

    :cond_2
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    iput v5, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v4, v3, Lcom/fmm/ds/c/bi;->d:I

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/fmm/ds/f/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I

    iget-object v0, p0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v1

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v2, v2, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    invoke-virtual {v2}, Lcom/fmm/ds/c/as;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/fmm/ds/f/a;->a([BI)I

    move-result v0

    if-gez v0, :cond_4

    const-string v0, "fail by tplSendException 4"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, -0x3

    iput v0, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_0

    :cond_4
    iput v5, p0, Lcom/fmm/ds/b/a;->h:I

    iget v0, p0, Lcom/fmm/ds/b/a;->h:I

    goto :goto_0
.end method

.method public d()I
    .locals 12

    const/16 v3, -0x64

    const/4 v11, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    sget-object v4, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v4, v4, Lcom/fmm/ds/c/bi;->c:Lcom/fmm/ds/c/bf;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start parsing syncml init package. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "msgid=[%d], ws.state=[%d]"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    sget-object v9, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v9, v9, Lcom/fmm/ds/c/bi;->e:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    sget-object v9, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v9, v9, Lcom/fmm/ds/c/bi;->n:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v5, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v11, v5, Lcom/fmm/ds/c/bi;->f:I

    sget-object v5, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-virtual {v4, v5}, Lcom/fmm/ds/c/bf;->a(Ljava/lang/Object;)V

    sget-object v5, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v5, v5, Lcom/fmm/ds/c/bi;->O:Lcom/fmm/ds/c/as;

    invoke-virtual {v4, v5}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/as;)I

    move-result v4

    iget-object v5, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput-boolean v2, v5, Lcom/fmm/ds/d/j;->a:Z

    if-nez v4, :cond_3

    iget-wide v4, v1, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    invoke-virtual {v1, v2}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/d/u;

    :goto_0
    if-eqz v1, :cond_1

    iget-object v4, v1, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    const-string v5, "./CallLog"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-object v5, v1, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/ds/d/j;->w:Ljava/lang/String;

    iget-object v4, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-boolean v5, v1, Lcom/fmm/ds/d/u;->u:Z

    iput-boolean v5, v4, Lcom/fmm/ds/d/j;->a:Z

    iget-object v4, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v1, v1, Lcom/fmm/ds/d/u;->b:I

    iput v1, v4, Lcom/fmm/ds/d/j;->c:I

    :cond_0
    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/d/u;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    if-ne v0, v11, :cond_2

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v2, v0, Lcom/fmm/ds/c/bi;->n:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "authState ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v0, v2

    :goto_1
    return v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "authState Fail Reason = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v0, v3

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed in xdsParse res = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v0, v3

    goto :goto_1
.end method

.method public e()I
    .locals 9

    const/4 v1, 0x0

    const/4 v8, 0x1

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v2, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->c:Lcom/fmm/ds/c/bf;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Start parsing syncml init package. msgid[%d], ws.state[%d]"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v6, v6, Lcom/fmm/ds/c/bi;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v6, v6, Lcom/fmm/ds/c/bi;->n:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v8, v3, Lcom/fmm/ds/c/bi;->f:I

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/bf;->a(Ljava/lang/Object;)V

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->O:Lcom/fmm/ds/c/as;

    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/as;)I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v2, v1}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/fmm/ds/d/u;->u:Z

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/fmm/ds/c/av;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :cond_0
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_0

    :cond_1
    iget-wide v2, v2, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v2, -0x1

    iput v2, v0, Lcom/fmm/ds/c/bi;->s:I

    :cond_2
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    if-ne v0, v8, :cond_3

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v8, v0, Lcom/fmm/ds/c/bi;->n:I

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authState is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v2, v2, Lcom/fmm/ds/c/bi;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_2
    return v0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authState Fail ... Reason is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v2, v2, Lcom/fmm/ds/c/bi;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed in xdsParse res="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v0, -0x64

    goto :goto_2
.end method

.method f()I
    .locals 16

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    new-instance v7, Lcom/fmm/ds/c/at;

    invoke-direct {v7}, Lcom/fmm/ds/c/at;-><init>()V

    iput-object v7, v0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x1

    const/4 v1, 0x1

    const/4 v2, 0x1

    new-instance v8, Lcom/fmm/ds/b/d;

    invoke-direct {v8}, Lcom/fmm/ds/b/d;-><init>()V

    new-instance v9, Lcom/fmm/ds/b/d;

    invoke-direct {v9}, Lcom/fmm/ds/b/d;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start to create sync package. "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "msgid=[%d], ws.state=[%d]"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    sget-object v13, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v13, v13, Lcom/fmm/ds/c/bi;->e:J

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    sget-object v13, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v13, v13, Lcom/fmm/ds/c/bi;->n:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v6, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    invoke-virtual {v7, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/as;)V

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    const/16 v6, 0x6a

    const-string v10, "-//SYNCML//DTD SyncML 1.1//EN"

    const-string v11, "-//SYNCML//DTD SyncML 1.1//EN"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v7, v0, v6, v10, v11}, Lcom/fmm/ds/c/at;->a(IILjava/lang/String;I)I

    :goto_0
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v0}, Lcom/fmm/ds/b/b;->c(Lcom/fmm/ds/c/bi;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->D:Lcom/fmm/ds/c/an;

    invoke-virtual {v7, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/an;)I

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->b()I

    invoke-virtual/range {p0 .. p0}, Lcom/fmm/ds/b/a;->a()Z

    invoke-virtual/range {p0 .. p0}, Lcom/fmm/ds/b/a;->b()V

    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/fmm/ds/b/a;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[suspend&resume]"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/16 v6, 0x8

    invoke-static {v0, v6}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;I)Lcom/fmm/ds/c/f;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/f;)I

    :cond_0
    invoke-static {}, Lcom/fmm/ds/a/a;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/fmm/ds/d/a;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_1
    if-eqz v0, :cond_3

    invoke-static {}, Lcom/fmm/ds/d/a;->f()Z

    invoke-static {v0}, Lcom/fmm/ds/b/j;->b(Lcom/fmm/ds/d/u;)V

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v6, v0}, Lcom/fmm/ds/b/b;->f(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/v;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v7, v6}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/v;)I

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v6, v0}, Lcom/fmm/ds/b/b;->g(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Z

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->g()I

    :cond_1
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    const/16 v6, 0x6a

    const-string v10, "-//SYNCML//DTD SyncML 1.2//EN"

    const-string v11, "-//SYNCML//DTD SyncML 1.2//EN"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v7, v0, v6, v10, v11}, Lcom/fmm/ds/c/at;->a(IILjava/lang/String;I)I

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/fmm/ds/d/a;->a(Z)V

    :cond_4
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    move-object v6, v0

    :goto_2
    if-eqz v6, :cond_5

    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/fmm/ds/b/a;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_5
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    move v2, v4

    move-object v3, v0

    move v0, v5

    :goto_3
    if-eqz v3, :cond_6

    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-static {}, Lcom/fmm/ds/b/a;->o()Z

    move-result v1

    if-eqz v1, :cond_19

    :cond_6
    invoke-virtual {v7, v0}, Lcom/fmm/ds/c/at;->a(Z)I

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->a()I

    if-eqz v0, :cond_7

    if-eqz v2, :cond_1c

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v1, 0x3

    iput v1, v0, Lcom/fmm/ds/c/bi;->n:I

    :cond_7
    :goto_4
    const/4 v0, 0x0

    return v0

    :cond_8
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-nez v0, :cond_5

    iget v0, v6, Lcom/fmm/ds/d/u;->b:I

    if-eqz v0, :cond_18

    iget-boolean v0, v6, Lcom/fmm/ds/d/u;->u:Z

    if-eqz v0, :cond_c

    iget-boolean v0, v6, Lcom/fmm/ds/d/u;->v:Z

    if-nez v0, :cond_c

    const/4 v0, 0x0

    sget-object v10, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v10, v6}, Lcom/fmm/ds/b/b;->b(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/al;

    move-result-object v10

    if-eqz v10, :cond_c

    invoke-virtual {v7, v10}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/al;)I

    :cond_9
    move v15, v2

    move v2, v3

    move v3, v15

    if-nez v0, :cond_1f

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v10

    if-eqz v10, :cond_d

    move v15, v3

    move v3, v2

    move v2, v15

    :cond_a
    :goto_5
    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->c()I

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v6

    if-nez v6, :cond_b

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v10, 0x3

    invoke-static {v6, v10}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    :cond_b
    if-nez v0, :cond_5

    :cond_c
    :goto_6
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    move-object v6, v0

    goto/16 :goto_2

    :cond_d
    if-nez v0, :cond_f

    sget-object v10, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v10, v6}, Lcom/fmm/ds/b/b;->e(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/m;

    move-result-object v10

    if-eqz v10, :cond_f

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->h()Z

    iget v1, v6, Lcom/fmm/ds/d/u;->r:I

    invoke-virtual {v7, v10}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/m;)I

    iget v0, v6, Lcom/fmm/ds/d/u;->r:I

    iput v0, v8, Lcom/fmm/ds/b/d;->a:I

    iput v2, v9, Lcom/fmm/ds/b/d;->a:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v2, 0x4

    invoke-static {v0, v6, v8, v2, v9}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;Lcom/fmm/ds/b/d;ILcom/fmm/ds/b/d;)Z

    move-result v0

    iget v2, v8, Lcom/fmm/ds/b/d;->a:I

    iput v2, v6, Lcom/fmm/ds/d/u;->r:I

    iget v2, v9, Lcom/fmm/ds/b/d;->a:I

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->f()I

    iget v10, v6, Lcom/fmm/ds/d/u;->r:I

    if-ne v1, v10, :cond_e

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->i()Z

    :cond_e
    iget v1, v6, Lcom/fmm/ds/d/u;->r:I

    if-nez v1, :cond_10

    const/4 v1, 0x1

    :cond_f
    :goto_7
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v10

    if-eqz v10, :cond_11

    move v15, v3

    move v3, v2

    move v2, v15

    goto :goto_5

    :cond_10
    const/4 v1, 0x0

    goto :goto_7

    :cond_11
    if-nez v0, :cond_1e

    if-eqz v1, :cond_1e

    sget-object v10, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v10, v6}, Lcom/fmm/ds/b/b;->d(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/ag;

    move-result-object v10

    if-eqz v10, :cond_1e

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->h()Z

    iget v11, v6, Lcom/fmm/ds/d/u;->q:I

    invoke-virtual {v7, v10}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ag;)I

    iget v0, v6, Lcom/fmm/ds/d/u;->q:I

    iput v0, v8, Lcom/fmm/ds/b/d;->a:I

    iput v2, v9, Lcom/fmm/ds/b/d;->a:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v2, 0x2

    invoke-static {v0, v6, v8, v2, v9}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;Lcom/fmm/ds/b/d;ILcom/fmm/ds/b/d;)Z

    move-result v0

    iget v2, v8, Lcom/fmm/ds/b/d;->a:I

    iput v2, v6, Lcom/fmm/ds/d/u;->q:I

    iget v3, v9, Lcom/fmm/ds/b/d;->a:I

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->e()I

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v2

    if-eqz v2, :cond_14

    iget v2, v6, Lcom/fmm/ds/d/u;->q:I

    if-ne v11, v2, :cond_12

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v2, v2, Lcom/fmm/ds/c/bi;->W:Lcom/fmm/ds/c/bk;

    iget-boolean v2, v2, Lcom/fmm/ds/c/bk;->a:Z

    if-nez v2, :cond_12

    const-string v2, "xdsEncReset at updateListSize == dbInfo.addListCount"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->i()Z

    :cond_12
    :goto_8
    iget v2, v6, Lcom/fmm/ds/d/u;->q:I

    if-nez v2, :cond_15

    const/4 v2, 0x1

    :goto_9
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v10

    if-nez v10, :cond_a

    if-nez v0, :cond_13

    if-eqz v1, :cond_13

    if-eqz v2, :cond_13

    iget-boolean v10, v6, Lcom/fmm/ds/d/u;->l:Z

    if-eqz v10, :cond_16

    iget v10, v6, Lcom/fmm/ds/d/u;->p:I

    if-eqz v10, :cond_16

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "isServerFull [%b] addListCount [%d]"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-boolean v14, v6, Lcom/fmm/ds/d/u;->l:Z

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    iget v14, v6, Lcom/fmm/ds/d/u;->p:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v10, 0x0

    iput v10, v6, Lcom/fmm/ds/d/u;->p:I

    :cond_13
    :goto_a
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "itemCountofMsg : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v10

    if-nez v10, :cond_a

    const/16 v10, 0xa

    if-ge v3, v10, :cond_a

    iget v10, v6, Lcom/fmm/ds/d/u;->p:I

    if-nez v10, :cond_9

    iget v10, v6, Lcom/fmm/ds/d/u;->q:I

    if-nez v10, :cond_9

    iget v10, v6, Lcom/fmm/ds/d/u;->r:I

    if-nez v10, :cond_9

    const/4 v10, 0x1

    iput-boolean v10, v6, Lcom/fmm/ds/d/u;->v:Z

    goto/16 :goto_5

    :cond_14
    iget v2, v6, Lcom/fmm/ds/d/u;->q:I

    if-ne v11, v2, :cond_12

    const-string v2, "xdsEncReset at updateListSize == dbInfo.addListCount"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->i()Z

    goto :goto_8

    :cond_15
    const/4 v2, 0x0

    goto :goto_9

    :cond_16
    sget-object v10, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v10, v6}, Lcom/fmm/ds/b/b;->c(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/e;

    move-result-object v10

    if-eqz v10, :cond_13

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->h()Z

    iget v11, v6, Lcom/fmm/ds/d/u;->p:I

    invoke-virtual {v7, v10}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/e;)I

    iget v0, v6, Lcom/fmm/ds/d/u;->p:I

    iput v0, v8, Lcom/fmm/ds/b/d;->a:I

    iput v3, v9, Lcom/fmm/ds/b/d;->a:I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v3, 0x1

    invoke-static {v0, v6, v8, v3, v9}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;Lcom/fmm/ds/b/d;ILcom/fmm/ds/b/d;)Z

    move-result v0

    iget v3, v8, Lcom/fmm/ds/b/d;->a:I

    iput v3, v6, Lcom/fmm/ds/d/u;->p:I

    iget v3, v9, Lcom/fmm/ds/b/d;->a:I

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->d()I

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v10

    if-eqz v10, :cond_17

    iget v10, v6, Lcom/fmm/ds/d/u;->p:I

    if-ne v11, v10, :cond_13

    sget-object v10, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v10, v10, Lcom/fmm/ds/c/bi;->W:Lcom/fmm/ds/c/bk;

    iget-boolean v10, v10, Lcom/fmm/ds/c/bk;->a:Z

    if-nez v10, :cond_13

    const-string v10, "xdsEncReset at updateListSize == dbInfo.addListCount"

    invoke-static {v10}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->i()Z

    goto :goto_a

    :cond_17
    iget v10, v6, Lcom/fmm/ds/d/u;->p:I

    if-ne v11, v10, :cond_13

    const-string v10, "xdsEncReset at updateListSize == dbInfo.addListCount"

    invoke-static {v10}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/fmm/ds/c/at;->i()Z

    goto/16 :goto_a

    :cond_18
    const/4 v0, 0x1

    iput-boolean v0, v6, Lcom/fmm/ds/d/u;->v:Z

    goto/16 :goto_6

    :cond_19
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    if-nez v1, :cond_6

    iget-boolean v1, v3, Lcom/fmm/ds/d/u;->v:Z

    if-nez v1, :cond_1d

    const/4 v0, 0x0

    move v1, v0

    :goto_b
    iget v0, v3, Lcom/fmm/ds/d/u;->b:I

    const/4 v4, 0x3

    if-eq v0, v4, :cond_1a

    iget v0, v3, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_1b

    :cond_1a
    const/4 v0, 0x0

    move v2, v0

    :cond_1b
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    move-object v3, v0

    move v0, v1

    goto/16 :goto_3

    :cond_1c
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v1, 0x5

    iput v1, v0, Lcom/fmm/ds/c/bi;->n:I

    goto/16 :goto_4

    :cond_1d
    move v1, v0

    goto :goto_b

    :cond_1e
    move v15, v3

    move v3, v2

    move v2, v15

    goto/16 :goto_9

    :cond_1f
    move v15, v3

    move v3, v2

    move v2, v15

    goto/16 :goto_5
.end method

.method g()I
    .locals 14

    const-wide/16 v12, 0x0

    const/16 v11, 0x6a

    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    new-instance v4, Lcom/fmm/ds/c/at;

    invoke-direct {v4}, Lcom/fmm/ds/c/at;-><init>()V

    iput-object v4, v3, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Start to create map package. "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "msgid=[%d], ws.state=[%d]"

    new-array v7, v10, [Ljava/lang/Object;

    sget-object v8, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v8, v8, Lcom/fmm/ds/c/bi;->e:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v2

    sget-object v8, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v8, v8, Lcom/fmm/ds/c/bi;->n:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/as;)V

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "-//SYNCML//DTD SyncML 1.1//EN"

    const-string v5, "-//SYNCML//DTD SyncML 1.1//EN"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v2, v11, v3, v5}, Lcom/fmm/ds/c/at;->a(IILjava/lang/String;I)I

    :goto_0
    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v3}, Lcom/fmm/ds/b/b;->c(Lcom/fmm/ds/c/bi;)V

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v3, v3, Lcom/fmm/ds/c/bi;->D:Lcom/fmm/ds/c/an;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/an;)I

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->b()I

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-boolean v3, v3, Lcom/fmm/ds/c/bi;->E:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v3, v0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/f;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/f;)I

    :cond_0
    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v2}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_1
    if-eqz v0, :cond_4

    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v3, v0}, Lcom/fmm/ds/b/b;->f(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/v;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/v;)I

    invoke-static {}, Lcom/fmm/ds/a/a;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v0, Lcom/fmm/ds/d/u;->b:I

    if-ne v3, v10, :cond_1

    invoke-static {v0}, Lcom/fmm/ds/b/j;->a(Lcom/fmm/ds/d/u;)V

    :cond_1
    sget-object v3, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {v3, v0}, Lcom/fmm/ds/b/b;->g(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Z

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->g()I

    :cond_2
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_1

    :cond_3
    const-string v3, "-//SYNCML//DTD SyncML 1.2//EN"

    const-string v5, "-//SYNCML//DTD SyncML 1.2//EN"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v2, v11, v3, v5}, Lcom/fmm/ds/c/at;->a(IILjava/lang/String;I)I

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->n:I

    const/4 v3, 0x4

    if-ne v0, v3, :cond_5

    :goto_2
    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v2}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v12, v13}, Lcom/fmm/ds/c/av;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/f;

    :goto_3
    if-eqz v0, :cond_7

    invoke-virtual {v4, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/f;)I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v12, v13}, Lcom/fmm/ds/c/av;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/f;

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    const-string v0, "too Many Status Command. so.. try multimessage.."

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v1, v2

    :cond_7
    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/fmm/ds/b/a;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/16 v3, 0x8

    invoke-static {v0, v3}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;I)Lcom/fmm/ds/c/f;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/f;)I

    :cond_8
    invoke-virtual {v4, v1}, Lcom/fmm/ds/c/at;->a(Z)I

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->a()I

    return v2
.end method

.method h()I
    .locals 9

    const/4 v8, 0x1

    const/4 v0, 0x0

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->c:Lcom/fmm/ds/c/bf;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Start parsing syncml non-init package. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "msgid=[%d], ws.state=[%d]"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v6, v6, Lcom/fmm/ds/c/bi;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v0

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v6, v6, Lcom/fmm/ds/c/bi;->n:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v8, v2, Lcom/fmm/ds/c/bi;->f:I

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-virtual {v1, v2}, Lcom/fmm/ds/c/bf;->a(Ljava/lang/Object;)V

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v2, v2, Lcom/fmm/ds/c/bi;->O:Lcom/fmm/ds/c/as;

    invoke-virtual {v1, v2}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/as;)I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed in xdsParse ret : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v0, -0x64

    goto :goto_0
.end method

.method declared-synchronized i()V
    .locals 9

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/fmm/ds/b/a;->m:J

    const-string v0, "===================================="

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "- TotalSyncTime : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%d millsecond"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/fmm/ds/b/a;->m:J

    iget-wide v7, p0, Lcom/fmm/ds/b/a;->l:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v0, "===================================="

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/b/c;->b()V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v1, v1, Lcom/fmm/ds/c/bi;->p:I

    invoke-static {v0, v1}, Lcom/fmm/ds/b/x;->b(Ljava/lang/Object;I)V

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-virtual {v0}, Lcom/fmm/ds/c/bi;->a()V

    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/fmm/ds/b/a;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "suspend&resume"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/fmm/ds/b/a;->c(Z)V

    :cond_0
    const-string v0, "completed"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized j()V
    .locals 1

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-virtual {v0}, Lcom/fmm/ds/c/bi;->a()V

    :cond_0
    new-instance v0, Lcom/fmm/ds/c/bi;

    invoke-direct {v0, p0}, Lcom/fmm/ds/c/bi;-><init>(Lcom/fmm/ds/b/a;)V

    sput-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public k()I
    .locals 11

    const/4 v9, -0x2

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v8, -0x5

    const/4 v5, 0x0

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->n:I

    if-ne v0, v4, :cond_1

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v1, 0x3

    iput v1, v0, Lcom/fmm/ds/c/bi;->n:I

    :goto_0
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->n:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_e

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->n:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x6

    invoke-virtual {p0, v0}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    :cond_0
    :goto_1
    return v5

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->O:Lcom/fmm/ds/c/as;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/f/a;->a(Lcom/fmm/ds/c/as;)I

    move-result v0

    if-gez v0, :cond_2

    const/4 v0, -0x4

    invoke-virtual {p0, v0}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->c:Lcom/fmm/ds/c/bf;

    if-nez v0, :cond_3

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    new-instance v1, Lcom/fmm/ds/c/bf;

    new-instance v3, Lcom/fmm/ds/b/i;

    invoke-direct {v3}, Lcom/fmm/ds/b/i;-><init>()V

    invoke-direct {v1, v3}, Lcom/fmm/ds/c/bf;-><init>(Lcom/fmm/ds/b/i;)V

    iput-object v1, v0, Lcom/fmm/ds/c/bi;->c:Lcom/fmm/ds/c/bf;

    :cond_3
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->n:I

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->e()I

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v8}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/16 v1, -0x13

    if-ne v0, v1, :cond_5

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v0, v0, Lcom/fmm/ds/c/bi;->e:J

    const-wide/16 v6, 0x1

    cmp-long v0, v0, v6

    if-lez v0, :cond_5

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iput v9, v0, Lcom/fmm/ds/c/bi;->i:I

    :cond_5
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    if-eq v0, v8, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/4 v1, -0x6

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/16 v1, -0xa

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/16 v1, -0x12

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    if-eq v0, v9, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/16 v1, -0xc

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/16 v1, -0xd

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/16 v1, -0x11

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/16 v1, -0xe

    if-ne v0, v1, :cond_7

    :cond_6
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    goto/16 :goto_1

    :cond_7
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->s:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_c

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->i:I

    const/16 v1, -0x13

    if-eq v0, v1, :cond_c

    const/16 v0, -0xb

    invoke-virtual {p0, v0}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->h()I

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0, v8}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    goto/16 :goto_1

    :cond_9
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->q:I

    if-eq v0, v2, :cond_a

    const/4 v0, -0x8

    invoke-virtual {p0, v0}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    goto/16 :goto_1

    :cond_a
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->r:I

    if-eq v0, v9, :cond_b

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->r:I

    if-ne v0, v8, :cond_c

    :cond_b
    const/16 v0, -0x9

    invoke-virtual {p0, v0}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    goto/16 :goto_1

    :cond_c
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-wide v6, v0, Lcom/fmm/ds/c/bi;->e:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v0, Lcom/fmm/ds/c/bi;->e:J

    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->g()I

    :goto_2
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-eqz v0, :cond_d

    iput v5, p0, Lcom/fmm/ds/b/a;->h:I

    iget v5, p0, Lcom/fmm/ds/b/a;->h:I

    goto/16 :goto_1

    :pswitch_1
    const-string v0, "XDS_STATE_INIT"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->c()I

    goto :goto_2

    :pswitch_2
    const-string v0, "XDS_STATE_CLIENT_MODIFICATION"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->f()I

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->g()I

    goto :goto_2

    :pswitch_4
    const-string v0, "XDS_STATE_CLIENT_MAP_FINISH"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/fmm/ds/b/a;->g()I

    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v1, 0x6

    iput v1, v0, Lcom/fmm/ds/c/bi;->n:I

    goto :goto_2

    :pswitch_5
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    const/4 v1, 0x6

    iput v1, v0, Lcom/fmm/ds/c/bi;->n:I

    goto :goto_2

    :cond_d
    iget-object v0, p0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v4, v4, Lcom/fmm/ds/c/bi;->d:I

    invoke-virtual/range {v0 .. v5}, Lcom/fmm/ds/f/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I

    const-string v0, "xtpTplSendData"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v1

    sget-object v2, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v2, v2, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    invoke-virtual {v2}, Lcom/fmm/ds/c/as;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/fmm/ds/f/a;->a([BI)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, -0x3

    invoke-virtual {p0, v0}, Lcom/fmm/ds/b/a;->b(I)I

    move-result v5

    goto/16 :goto_1

    :cond_e
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    const-string v1, "XDS_STATE_FINISH sync result is XDS_SYNC_TRUE"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v5}, Lcom/fmm/ds/c/av;->a(I)V

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/d/u;

    if-nez v1, :cond_10

    const-string v3, "dbinfo is null!!"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v3, v2

    :goto_3
    if-eqz v1, :cond_f

    sget-object v6, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v6, v6, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    iget-object v7, v1, Lcom/fmm/ds/d/u;->f:Ljava/lang/String;

    invoke-static {v6, v1, v4, v7}, Lcom/fmm/ds/b/j;->a(Ljava/lang/Object;Lcom/fmm/ds/d/u;ILjava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "call xdsApplySyncResult() "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "DB_ID[%d] dbInfo.alertStatus [%s] XDS_SYNC_TRUE"

    new-array v9, v4, [Ljava/lang/Object;

    iget v10, v1, Lcom/fmm/ds/d/u;->a:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v5

    iget-object v1, v1, Lcom/fmm/ds/d/u;->m:Ljava/lang/String;

    aput-object v1, v9, v2

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget-object v1, v1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/d/u;

    goto :goto_3

    :cond_f
    iput v3, v0, Lcom/fmm/ds/d/h;->j:I

    move v5, v2

    goto/16 :goto_1

    :cond_10
    move v3, v4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public l()I
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-virtual {v0}, Lcom/fmm/ds/b/q;->c()I

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/fmm/ds/b/a;->a(ZI)V

    return v1
.end method

.method public p()Ljava/lang/String;
    .locals 6

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%x%x"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sessionid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    return-object v0
.end method

.method public r()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/b/a;->g:I

    return-void
.end method

.method public s()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/fmm/ds/b/a;->g:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    iget v0, p0, Lcom/fmm/ds/b/a;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fmm/ds/b/a;->g:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m_RetryConnectCount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/fmm/ds/b/a;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v1, "m_RetryConnectCount is XDS_MAX_RETRY_COUNT"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public t()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/ds/f/a;->a:Ljava/lang/String;

    return-void
.end method
