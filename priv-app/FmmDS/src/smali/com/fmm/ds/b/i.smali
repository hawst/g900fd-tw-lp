.class public Lcom/fmm/ds/b/i;
.super Ljava/lang/Object;


# static fields
.field private static a:[I


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    sput-object v0, Lcom/fmm/ds/b/i;->a:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/b/i;->b:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/fmm/ds/b/i;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/fmm/ds/b/i;->d:I

    const/4 v0, -0x2

    iput v0, p0, Lcom/fmm/ds/b/i;->e:I

    const/4 v0, -0x3

    iput v0, p0, Lcom/fmm/ds/b/i;->f:I

    const/4 v0, -0x4

    iput v0, p0, Lcom/fmm/ds/b/i;->g:I

    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x1

    const/16 v1, -0xd

    const-string v2, "212"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "Authentication Accepted ( 212 )"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    return v0

    :cond_1
    const-string v2, "200"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "401"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, -0x2

    goto :goto_0

    :cond_2
    const-string v0, "407"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, -0x3

    goto :goto_0

    :cond_3
    const-string v0, "402"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, -0x4

    goto :goto_0

    :cond_4
    const-string v0, "417"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, -0x5

    goto :goto_0

    :cond_5
    const-string v0, "101"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, -0x6

    goto :goto_0

    :cond_6
    const-string v0, "403"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, -0xc

    goto :goto_0

    :cond_7
    const-string v0, "511"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    const-string v0, "513"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, -0x11

    goto :goto_0

    :cond_9
    const-string v0, "500"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    const-string v0, "510"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, -0xe

    goto/16 :goto_0

    :cond_b
    const-string v0, "506"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, -0xf

    goto/16 :goto_0

    :cond_c
    const-string v0, "404"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, -0xa

    goto/16 :goto_0

    :cond_d
    const-string v0, "406"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, -0x12

    goto/16 :goto_0

    :cond_e
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method public static a()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    sput-object v0, Lcom/fmm/ds/b/i;->a:[I

    return-void
.end method

.method public static a(Lcom/fmm/ds/c/n;)V
    .locals 0

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/n;)Z

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/u;Lcom/fmm/ds/c/z;)I
    .locals 10

    const/4 v9, 0x2

    const/4 v1, -0x1

    const/4 v3, -0x3

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v4, p1, Lcom/fmm/ds/c/bi;->V:Lcom/fmm/ds/c/bj;

    if-nez p2, :cond_1

    const-string v0, "item is null !!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    const/4 v0, -0x4

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez v4, :cond_2

    const-string v0, "ReceiveObj is null !!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    const/4 v0, -0x4

    goto :goto_0

    :cond_2
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "item->moredata [%d], bReceiving [%b]"

    new-array v7, v9, [Ljava/lang/Object;

    iget v8, p2, Lcom/fmm/ds/c/u;->g:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v0

    iget-boolean v8, v4, Lcom/fmm/ds/c/bj;->a:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v5, p2, Lcom/fmm/ds/c/u;->g:I

    if-lez v5, :cond_10

    iget-boolean v3, v4, Lcom/fmm/ds/c/bj;->a:Z

    if-nez v3, :cond_d

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    iput-boolean v2, v4, Lcom/fmm/ds/c/bj;->a:Z

    iget-object v3, p2, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p2, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    iput-object v3, v4, Lcom/fmm/ds/c/bj;->b:Ljava/lang/String;

    :cond_3
    :goto_1
    if-eqz p3, :cond_6

    iget-object v3, p3, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p3, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v4, Lcom/fmm/ds/c/bj;->f:I

    :goto_2
    if-eqz p3, :cond_8

    iget-object v3, p3, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p3, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v3, v4, Lcom/fmm/ds/c/bj;->d:Ljava/lang/String;

    :cond_4
    :goto_3
    iget v3, v4, Lcom/fmm/ds/c/bj;->f:I

    if-gtz v3, :cond_9

    const-string v0, " XDS_LO_UNKNOWNSIZE"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    const/4 v0, -0x2

    goto :goto_0

    :cond_5
    iget-object v3, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v3, :cond_3

    iget-object v3, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v3, v3, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v3, v3, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iput-object v3, v4, Lcom/fmm/ds/c/bj;->b:Ljava/lang/String;

    goto :goto_1

    :cond_6
    if-eqz p2, :cond_7

    iget-object v3, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-eqz v3, :cond_7

    iget-object v3, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v3, v3, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v3, v3, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v4, Lcom/fmm/ds/c/bj;->f:I

    goto :goto_2

    :cond_7
    iput v0, v4, Lcom/fmm/ds/c/bj;->f:I

    goto :goto_2

    :cond_8
    if-eqz p2, :cond_4

    iget-object v3, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-eqz v3, :cond_4

    iget-object v3, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v3, v3, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v3, v3, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v3, v4, Lcom/fmm/ds/c/bj;->d:Ljava/lang/String;

    goto :goto_3

    :cond_9
    if-eqz p2, :cond_a

    iget-object v3, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    if-eqz v3, :cond_a

    iget v3, v4, Lcom/fmm/ds/c/bj;->f:I

    iget-object v5, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v5, v5, Lcom/fmm/ds/c/ab;->c:I

    if-ge v3, v5, :cond_a

    const-string v0, " XDS_LO_SIZEMISMATCH"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    move v0, v1

    goto/16 :goto_0

    :cond_a
    iget v1, v4, Lcom/fmm/ds/c/bj;->f:I

    new-array v1, v1, [C

    iput-object v1, v4, Lcom/fmm/ds/c/bj;->c:[C

    if-eqz p2, :cond_c

    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    if-eqz v1, :cond_c

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "ReceiveObj.totalsize [%d], item.data.size[%d]"

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, v4, Lcom/fmm/ds/c/bj;->f:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    iget-object v6, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v6, v6, Lcom/fmm/ds/c/ab;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v1, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_4
    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v1, v1, Lcom/fmm/ds/c/ab;->c:I

    if-ge v0, v1, :cond_b

    iget-object v1, v4, Lcom/fmm/ds/c/bj;->c:[C

    iget-object v3, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v3, v3, Lcom/fmm/ds/c/ab;->b:[C

    aget-char v3, v3, v0

    aput-char v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    iget v0, v4, Lcom/fmm/ds/c/bj;->e:I

    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v1, v1, Lcom/fmm/ds/c/ab;->c:I

    add-int/2addr v0, v1

    iput v0, v4, Lcom/fmm/ds/c/bj;->e:I

    :goto_5
    move v0, v2

    goto/16 :goto_0

    :cond_c
    const-string v0, "item or ite.data is null!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_5

    :cond_d
    iget v3, v4, Lcom/fmm/ds/c/bj;->e:I

    iget-object v5, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v5, v5, Lcom/fmm/ds/c/ab;->c:I

    add-int/2addr v3, v5

    iget v5, v4, Lcom/fmm/ds/c/bj;->f:I

    if-le v3, v5, :cond_e

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    move v0, v1

    goto/16 :goto_0

    :cond_e
    :goto_6
    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v1, v1, Lcom/fmm/ds/c/ab;->c:I

    if-ge v0, v1, :cond_f

    iget-object v1, v4, Lcom/fmm/ds/c/bj;->c:[C

    iget v3, v4, Lcom/fmm/ds/c/bj;->e:I

    add-int/2addr v3, v0

    iget-object v5, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v5, v5, Lcom/fmm/ds/c/ab;->b:[C

    aget-char v5, v5, v0

    aput-char v5, v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_f
    iget v0, v4, Lcom/fmm/ds/c/bj;->e:I

    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v1, v1, Lcom/fmm/ds/c/ab;->c:I

    add-int/2addr v0, v1

    iput v0, v4, Lcom/fmm/ds/c/bj;->e:I

    move v0, v2

    goto/16 :goto_0

    :cond_10
    iget-boolean v5, v4, Lcom/fmm/ds/c/bj;->a:Z

    if-eqz v5, :cond_15

    iget-object v5, p2, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_11

    iget-object v5, v4, Lcom/fmm/ds/c/bj;->b:Ljava/lang/String;

    iget-object v6, p2, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_13

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    move v0, v3

    goto/16 :goto_0

    :cond_11
    iget-object v5, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v5, :cond_12

    iget-object v5, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v5, v5, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_12

    iget-object v5, v4, Lcom/fmm/ds/c/bj;->b:Ljava/lang/String;

    iget-object v6, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v6, v6, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_13

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    move v0, v3

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    move v0, v3

    goto/16 :goto_0

    :cond_13
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "ReceiveObj.totalsize [%d], item.data.size[%d]"

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, v4, Lcom/fmm/ds/c/bj;->f:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    iget-object v7, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v7, v7, Lcom/fmm/ds/c/ab;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v3, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v2, v4, Lcom/fmm/ds/c/bj;->e:I

    iget-object v3, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v3, v3, Lcom/fmm/ds/c/ab;->c:I

    add-int/2addr v2, v3

    iget v3, v4, Lcom/fmm/ds/c/bj;->f:I

    if-le v2, v3, :cond_14

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    move v0, v1

    goto/16 :goto_0

    :cond_14
    move v1, v0

    :goto_7
    iget-object v2, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v2, v2, Lcom/fmm/ds/c/ab;->c:I

    if-ge v1, v2, :cond_0

    iget-object v2, v4, Lcom/fmm/ds/c/bj;->c:[C

    iget v3, v4, Lcom/fmm/ds/c/bj;->e:I

    add-int/2addr v3, v1

    iget-object v5, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v5, v5, Lcom/fmm/ds/c/ab;->b:[C

    aget-char v5, v5, v1

    aput-char v5, v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_15
    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v1, v1, Lcom/fmm/ds/c/ab;->c:I

    iput v1, v4, Lcom/fmm/ds/c/bj;->f:I

    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v1, v1, Lcom/fmm/ds/c/ab;->b:[C

    iput-object v1, v4, Lcom/fmm/ds/c/bj;->c:[C

    goto/16 :goto_0
.end method

.method public a(Lcom/fmm/ds/c/bi;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->V:Lcom/fmm/ds/c/bj;

    if-eqz v0, :cond_0

    iput-object v2, v0, Lcom/fmm/ds/c/bj;->c:[C

    iput-object v2, v0, Lcom/fmm/ds/c/bj;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/fmm/ds/c/bj;->d:Ljava/lang/String;

    iput-boolean v1, v0, Lcom/fmm/ds/c/bj;->a:Z

    iput v1, v0, Lcom/fmm/ds/c/bj;->e:I

    iput v1, v0, Lcom/fmm/ds/c/bj;->f:I

    iput-object v2, v0, Lcom/fmm/ds/c/bj;->g:[C

    iput v1, v0, Lcom/fmm/ds/c/bj;->h:I

    iput v1, v0, Lcom/fmm/ds/c/bj;->i:I

    iput v1, v0, Lcom/fmm/ds/c/bj;->j:I

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Object;I)V
    .locals 8

    const-wide/16 v5, 0x0

    const/4 v7, 0x6

    const/4 v2, 0x1

    const/4 v1, 0x0

    check-cast p1, Lcom/fmm/ds/c/bi;

    iget v0, p1, Lcom/fmm/ds/c/bi;->n:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    if-eqz p2, :cond_1

    iput v7, p1, Lcom/fmm/ds/c/bi;->n:I

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/fmm/ds/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p1, Lcom/fmm/ds/c/bi;->E:Z

    goto :goto_0

    :pswitch_2
    if-eqz p2, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    iget-wide v3, v0, Lcom/fmm/ds/c/av;->c:J

    cmp-long v0, v3, v5

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->M:Lcom/fmm/ds/c/ah;

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(I)V

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_1
    if-eqz v0, :cond_a

    iget-object v3, v0, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    if-eqz v3, :cond_2

    iget-object v0, v0, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    iget-wide v3, v0, Lcom/fmm/ds/c/av;->c:J

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    iput v7, p1, Lcom/fmm/ds/c/bi;->n:I

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    const/4 v0, 0x4

    iput v0, p1, Lcom/fmm/ds/c/bi;->n:I

    goto :goto_0

    :cond_5
    invoke-static {}, Lcom/fmm/ds/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    iget-wide v3, v0, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v5, 0x2

    cmp-long v0, v3, v5

    if-gtz v0, :cond_8

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->M:Lcom/fmm/ds/c/ah;

    if-nez v0, :cond_8

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(I)V

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_3
    if-eqz v0, :cond_9

    iget v3, v0, Lcom/fmm/ds/d/u;->p:I

    if-nez v3, :cond_6

    iget v3, v0, Lcom/fmm/ds/d/u;->q:I

    if-nez v3, :cond_6

    iget v0, v0, Lcom/fmm/ds/d/u;->r:I

    if-nez v0, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    iget v0, p1, Lcom/fmm/ds/c/bi;->K:I

    const/4 v3, 0x2

    if-le v0, v3, :cond_7

    iput v1, p1, Lcom/fmm/ds/c/bi;->K:I

    iput v7, p1, Lcom/fmm/ds/c/bi;->n:I

    goto/16 :goto_0

    :cond_6
    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_3

    :cond_7
    iget v0, p1, Lcom/fmm/ds/c/bi;->K:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/fmm/ds/c/bi;->K:I

    iput-boolean v2, p1, Lcom/fmm/ds/c/bi;->E:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "session finish check ws.m_SyncFinish="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/fmm/ds/c/bi;->K:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    iput-boolean v2, p1, Lcom/fmm/ds/c/bi;->E:Z

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_4

    :cond_a
    move v0, v2

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Ljava/lang/Object;IZ)V
    .locals 2

    check-cast p1, Lcom/fmm/ds/c/bi;

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(I)V

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    if-nez v0, :cond_1

    const-string v0, "dbinfo is null!!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/ag;)V
    .locals 26

    check-cast p1, Lcom/fmm/ds/c/bi;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v18, v0

    const/16 v16, 0x0

    const-string v15, ""

    const-string v14, ""

    const/4 v10, 0x0

    const/4 v8, 0x0

    const/4 v12, 0x0

    const/4 v9, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v4, Lcom/fmm/ds/d/h;

    const/4 v4, 0x0

    const-string v5, "handle replace cmd"

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-eqz v18, :cond_19

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v5

    if-eqz v5, :cond_f

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/ds/c/ag;->e:Lcom/fmm/ds/c/ax;

    invoke-static {v5}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/fmm/ds/c/u;

    invoke-static {v5}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v5

    move-object/from16 v17, v14

    move/from16 v24, v6

    move v6, v7

    move v7, v12

    move v12, v11

    move-object/from16 v11, v16

    move-object/from16 v16, v4

    move/from16 v4, v24

    move/from16 v25, v9

    move-object v9, v15

    move-object v15, v5

    move/from16 v5, v25

    :goto_0
    if-eqz v16, :cond_0

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v13

    if-eqz v13, :cond_3

    :cond_0
    :goto_1
    if-nez v10, :cond_1

    const/4 v4, 0x1

    if-le v8, v4, :cond_1

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;)V

    :cond_1
    if-eqz v11, :cond_2

    move-object/from16 v0, p2

    iget v4, v0, Lcom/fmm/ds/c/ag;->b:I

    if-nez v4, :cond_2

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v4, :cond_2

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v4, v11}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_2
    return-void

    :cond_3
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/fmm/ds/c/ag;->d:Lcom/fmm/ds/c/z;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2, v13}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/u;Lcom/fmm/ds/c/z;)I

    move-result v13

    if-nez v13, :cond_b

    new-instance v14, Lcom/fmm/ds/c/b;

    invoke-direct {v14}, Lcom/fmm/ds/c/b;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2, v14}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ag;Lcom/fmm/ds/c/u;Lcom/fmm/ds/c/b;)V

    new-instance v13, Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->V:Lcom/fmm/ds/c/bj;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/fmm/ds/c/bj;->c:[C

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v13, v0}, Ljava/lang/String;-><init>([C)V

    iput-object v13, v14, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/fmm/ds/c/bi;->V:Lcom/fmm/ds/c/bj;

    iget v13, v13, Lcom/fmm/ds/c/bj;->f:I

    iput v13, v14, Lcom/fmm/ds/c/b;->g:I

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v13, v13, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/ds/c/bi;->H:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v13, v0, v1, v14, v2}, Lcom/fmm/ds/b/q;->b(Ljava/lang/Object;ILcom/fmm/ds/c/b;I)I

    move-result v13

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "xdsReplaceItem ret : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v19, -0x4

    move/from16 v0, v19

    if-ne v13, v0, :cond_4

    iget-object v12, v14, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v13, v13, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v19, v0

    const/16 v20, -0x4

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v13, v0, v12, v1}, Lcom/fmm/ds/b/q;->b(III)I

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v12, v12, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/ds/c/bi;->H:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v13, v0, v14, v1}, Lcom/fmm/ds/b/q;->a(Ljava/lang/Object;ILcom/fmm/ds/c/b;I)I

    move-result v13

    const/4 v12, 0x1

    :cond_4
    packed-switch v13, :pswitch_data_0

    :pswitch_0
    if-nez v12, :cond_a

    add-int/lit8 v5, v5, 0x1

    :goto_2
    const-string v9, "500"

    move/from16 v24, v6

    move v6, v5

    move/from16 v5, v24

    :goto_3
    invoke-virtual/range {p0 .. p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    move v13, v8

    move-object v14, v9

    move v8, v5

    move v9, v6

    move v6, v12

    move v12, v7

    move v7, v4

    :goto_4
    move-object/from16 v0, v17

    if-eq v14, v0, :cond_1f

    if-eqz v11, :cond_1e

    move-object/from16 v0, p2

    iget v4, v0, Lcom/fmm/ds/c/ag;->b:I

    if-nez v4, :cond_5

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v4, :cond_5

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v4, v11}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_5
    const/4 v4, 0x1

    :goto_5
    move-object/from16 v0, p2

    iget v5, v0, Lcom/fmm/ds/c/ag;->a:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v10, "Replace"

    move-object/from16 v0, p1

    invoke-static {v0, v5, v10, v14}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v5

    move v10, v4

    :goto_6
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "xds_item.source : "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :cond_6
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v4, :cond_7

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v4, v4, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "xds_item.target : "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v11, v11, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    const/4 v4, 0x0

    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v11, v11, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v5, v4, v11}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    invoke-static {v15}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/fmm/ds/c/u;

    invoke-static {v15}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v11

    move-object v15, v11

    move-object/from16 v16, v4

    move-object/from16 v17, v14

    move v4, v7

    move-object v11, v5

    move v5, v9

    move v7, v12

    move-object v9, v14

    move v12, v6

    move v6, v8

    move v8, v13

    goto/16 :goto_0

    :pswitch_1
    if-nez v12, :cond_8

    add-int/lit8 v8, v8, 0x1

    const-string v9, "success process replace cmd"

    invoke-static {v9}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v9, "200"

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v13, v13, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v19, v0

    iget v0, v14, Lcom/fmm/ds/c/b;->a:I

    move/from16 v20, v0

    iget-object v14, v14, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    const/16 v21, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v13, v0, v1, v14, v2}, Lcom/fmm/ds/b/q;->c(IILjava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v7, v7, 0x1

    move/from16 v24, v6

    move v6, v5

    move/from16 v5, v24

    goto/16 :goto_3

    :cond_8
    add-int/lit8 v8, v8, 0x1

    const-string v9, "success process Repalace As Add cmd"

    invoke-static {v9}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v9, "201"

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v13, v13, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v19, v0

    iget v0, v14, Lcom/fmm/ds/c/b;->a:I

    move/from16 v20, v0

    iget-object v0, v14, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/fmm/ds/b/q;->a(IILjava/lang/String;Ljava/lang/String;)I

    iget-object v13, v14, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v13, v14}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/d/u;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    move/from16 v24, v6

    move v6, v5

    move/from16 v5, v24

    goto/16 :goto_3

    :pswitch_2
    const-string v9, "201"

    move/from16 v24, v6

    move v6, v5

    move/from16 v5, v24

    goto/16 :goto_3

    :pswitch_3
    if-nez v12, :cond_9

    add-int/lit8 v5, v5, 0x1

    :goto_7
    const-string v9, "500"

    move/from16 v24, v6

    move v6, v5

    move/from16 v5, v24

    goto/16 :goto_3

    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :pswitch_4
    move/from16 v24, v6

    move v6, v5

    move/from16 v5, v24

    goto/16 :goto_3

    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_b
    const/4 v14, 0x1

    if-ne v13, v14, :cond_c

    const/4 v9, 0x0

    const-string v13, "222"

    move-object/from16 v0, p1

    invoke-static {v0, v9, v13}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/z;Ljava/lang/String;)Lcom/fmm/ds/c/f;

    move-result-object v9

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    invoke-virtual {v13, v9}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    const-string v9, "213"

    move v13, v8

    move-object v14, v9

    move v8, v6

    move v9, v5

    move v6, v12

    move v12, v7

    move v7, v4

    goto/16 :goto_4

    :cond_c
    const/4 v14, -0x1

    if-ne v13, v14, :cond_d

    const-string v9, "XDS_LO_SIZEMISMATCH"

    invoke-static {v9}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v9, "424"

    move v13, v8

    move-object v14, v9

    move v8, v6

    move v9, v5

    move v6, v12

    move v12, v7

    move v7, v4

    goto/16 :goto_4

    :cond_d
    const/4 v14, -0x3

    if-ne v13, v14, :cond_e

    const-string v13, "XDS_LO_LASTCHUNKNORECV"

    invoke-static {v13}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v13, 0x0

    const-string v14, "223"

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/z;Ljava/lang/String;)Lcom/fmm/ds/c/f;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    invoke-virtual {v14, v13}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    move v13, v8

    move-object v14, v9

    move v8, v6

    move v9, v5

    move v6, v12

    move v12, v7

    move v7, v4

    goto/16 :goto_4

    :cond_e
    const/4 v14, -0x2

    if-ne v13, v14, :cond_20

    const-string v9, "XDS_LO_UNKNOWNSIZE"

    invoke-static {v9}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v9, "411"

    move v13, v8

    move-object v14, v9

    move v8, v6

    move v9, v5

    move v6, v12

    move v12, v7

    move v7, v4

    goto/16 :goto_4

    :cond_f
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/fmm/ds/c/ag;->d:Lcom/fmm/ds/c/z;

    if-eqz v5, :cond_1d

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/fmm/ds/c/ag;->d:Lcom/fmm/ds/c/z;

    iget-object v4, v4, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    move-object v5, v4

    :goto_8
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/fmm/ds/c/ag;->e:Lcom/fmm/ds/c/ax;

    invoke-static {v13}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/fmm/ds/c/u;

    invoke-static {v13}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v13

    move-object/from16 v17, v14

    move/from16 v24, v9

    move v9, v8

    move v8, v12

    move v12, v10

    move-object v10, v15

    move-object v15, v13

    move-object/from16 v13, v16

    move-object/from16 v16, v4

    move v4, v6

    move/from16 v6, v24

    :goto_9
    if-eqz v16, :cond_1c

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v14

    if-eqz v14, :cond_10

    move v8, v9

    move v10, v12

    move-object v11, v13

    goto/16 :goto_1

    :cond_10
    new-instance v19, Lcom/fmm/ds/c/b;

    invoke-direct/range {v19 .. v19}, Lcom/fmm/ds/c/b;-><init>()V

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-eqz v14, :cond_14

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v14, v14, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_11

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v14, v14, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v14, v0, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    :cond_11
    :goto_a
    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v14, :cond_15

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v14, v14, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_15

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v14, v14, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v14, v0, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    :goto_b
    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v14, v14, Lcom/fmm/ds/c/ab;->b:[C

    invoke-static {v14}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v19

    iput-object v14, v0, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    const/16 v20, 0x0

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/fmm/ds/c/ab;->b:[C

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v14, v14, Lcom/fmm/ds/c/ab;->c:I

    move-object/from16 v0, v19

    iput v14, v0, Lcom/fmm/ds/c/b;->g:I

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v14, v14, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v21, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/ds/c/bi;->H:I

    move/from16 v22, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v19

    move/from16 v3, v22

    invoke-virtual {v14, v0, v1, v2, v3}, Lcom/fmm/ds/b/q;->b(Ljava/lang/Object;ILcom/fmm/ds/c/b;I)I

    move-result v14

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "xdsReplaceItem ret : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v20, -0x4

    move/from16 v0, v20

    if-ne v14, v0, :cond_1b

    move-object/from16 v0, v19

    iget-object v11, v0, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v14, v14, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v20, v0

    const/16 v21, -0x4

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v14, v0, v11, v1}, Lcom/fmm/ds/b/q;->b(III)I

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v11, v11, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/ds/c/bi;->H:I

    move/from16 v21, v0

    move/from16 v0, v20

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-virtual {v11, v14, v0, v1, v2}, Lcom/fmm/ds/b/q;->a(Ljava/lang/Object;ILcom/fmm/ds/c/b;I)I

    move-result v14

    const/4 v11, 0x1

    move/from16 v24, v11

    move v11, v14

    move/from16 v14, v24

    :goto_c
    packed-switch v11, :pswitch_data_1

    :pswitch_5
    if-nez v14, :cond_18

    add-int/lit8 v6, v6, 0x1

    :goto_d
    const-string v10, "500"

    move-object v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move v6, v4

    :goto_e
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    if-eqz v13, :cond_1a

    move-object/from16 v0, p2

    iget v4, v0, Lcom/fmm/ds/c/ag;->b:I

    if-nez v4, :cond_12

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v4, :cond_12

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v4, v13}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_12
    const/4 v4, 0x1

    :goto_f
    move-object/from16 v0, p2

    iget v12, v0, Lcom/fmm/ds/c/ag;->a:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "Replace"

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13, v11}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v12

    move-object v13, v12

    move v12, v4

    :cond_13
    new-instance v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v13, v4, v1}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v15}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/fmm/ds/c/u;

    invoke-static {v15}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v15

    move-object/from16 v16, v4

    move-object/from16 v17, v11

    move v4, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v14

    goto/16 :goto_9

    :cond_14
    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    goto/16 :goto_a

    :cond_15
    const-string v14, "################## no LocURI ###################!!!!"

    invoke-static {v14}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_b

    :pswitch_6
    if-nez v14, :cond_16

    add-int/lit8 v9, v9, 0x1

    const-string v10, "success process replace cmd"

    invoke-static {v10}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v10, "200"

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v11, v11, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/fmm/ds/c/b;->a:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    invoke-virtual {v11, v0, v1, v2, v3}, Lcom/fmm/ds/b/q;->c(IILjava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v8, v8, 0x1

    move-object v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move v6, v4

    goto/16 :goto_e

    :cond_16
    add-int/lit8 v9, v9, 0x1

    const-string v10, "success process Repalace As Add cmd"

    invoke-static {v10}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v10, "201"

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v11, v11, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/fmm/ds/c/b;->a:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-virtual {v11, v0, v1, v2, v3}, Lcom/fmm/ds/b/q;->a(IILjava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, v19

    iget-object v11, v0, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v11, v1}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/d/u;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v7, v7, 0x1

    move-object v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move v6, v4

    goto/16 :goto_e

    :pswitch_7
    const-string v10, "201"

    move-object v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move v6, v4

    goto/16 :goto_e

    :pswitch_8
    if-nez v14, :cond_17

    add-int/lit8 v6, v6, 0x1

    :goto_10
    const-string v10, "500"

    move-object v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move v6, v4

    goto/16 :goto_e

    :cond_17
    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    :pswitch_9
    move-object v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move v6, v4

    goto/16 :goto_e

    :cond_18
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_d

    :cond_19
    const-string v4, "500"

    move-object/from16 v0, p2

    iget v5, v0, Lcom/fmm/ds/c/ag;->a:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Replace"

    move-object/from16 v0, p1

    invoke-static {v0, v5, v6, v4}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v11

    goto/16 :goto_1

    :cond_1a
    move v4, v12

    goto/16 :goto_f

    :cond_1b
    move/from16 v24, v11

    move v11, v14

    move/from16 v14, v24

    goto/16 :goto_c

    :cond_1c
    move v8, v9

    move v10, v12

    move-object v11, v13

    goto/16 :goto_1

    :cond_1d
    move-object v5, v4

    goto/16 :goto_8

    :cond_1e
    move v4, v10

    goto/16 :goto_5

    :cond_1f
    move-object v5, v11

    goto/16 :goto_6

    :cond_20
    move v13, v8

    move-object v14, v9

    move v8, v6

    move v9, v5

    move v6, v12

    move v12, v7

    move v7, v4

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x4
        :pswitch_9
        :pswitch_5
        :pswitch_5
        :pswitch_8
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/ai;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/ak;)V
    .locals 11

    check-cast p1, Lcom/fmm/ds/c/bi;

    iget-object v7, p2, Lcom/fmm/ds/c/ak;->d:Ljava/lang/String;

    iget-object v1, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    iget-object v0, p2, Lcom/fmm/ds/c/ak;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p2, Lcom/fmm/ds/c/ak;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2, v3}, Lcom/fmm/ds/c/bi;->a(IILjava/lang/String;)Lcom/fmm/ds/c/d;

    move-result-object v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handle Status cmd : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-nez v6, :cond_1

    const-string v0, "WARNING !! Can not find action(null).. it may cause a serious error"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "msgref[%s] cmdref[%s] cmd[%s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "SyncHdr"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Authentication Response : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/fmm/ds/b/i;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p1, Lcom/fmm/ds/c/bi;->i:I

    iget-object v0, p2, Lcom/fmm/ds/c/ak;->h:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/fmm/ds/c/bi;->i:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_2

    const/16 v0, -0x13

    iput v0, p1, Lcom/fmm/ds/c/bi;->i:I

    :cond_2
    iget-object v0, p2, Lcom/fmm/ds/c/ak;->h:Lcom/fmm/ds/c/z;

    iget-object v0, v0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    const-string v1, "syncml:auth-md5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/fmm/ds/b/x;->a(Ljava/lang/Object;I)V

    new-instance v0, Ljava/lang/String;

    iget-object v1, p2, Lcom/fmm/ds/c/ak;->h:Lcom/fmm/ds/c/z;

    iget-object v1, v1, Lcom/fmm/ds/c/z;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/c/ar;->b([B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    iget-object v1, p1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/fmm/ds/b/x;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput v0, p1, Lcom/fmm/ds/c/bi;->m:I

    :cond_3
    :goto_1
    if-eqz v6, :cond_0

    invoke-static {p1, v6}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/d;)V

    goto :goto_0

    :cond_4
    iget-object v0, p2, Lcom/fmm/ds/c/ak;->h:Lcom/fmm/ds/c/z;

    iget-object v0, v0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    const-string v1, "syncml:auth-basic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/fmm/ds/b/x;->a(Ljava/lang/Object;I)V

    const/4 v0, 0x0

    iput v0, p1, Lcom/fmm/ds/c/bi;->m:I

    goto :goto_1

    :cond_5
    const-string v0, "Sync"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Status != 200"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v0, "500"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, -0x1

    iput v0, p1, Lcom/fmm/ds/c/bi;->q:I

    goto :goto_1

    :cond_6
    const-string v0, "508"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    iput v0, p1, Lcom/fmm/ds/c/bi;->q:I

    goto :goto_1

    :cond_7
    const-string v0, "512"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, -0x1

    iput v0, p1, Lcom/fmm/ds/c/bi;->q:I

    goto :goto_1

    :cond_8
    const-string v0, "406"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    iput v0, p1, Lcom/fmm/ds/c/bi;->q:I

    goto :goto_1

    :cond_9
    const-string v0, "Add"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "Replace"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "Delete"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    :cond_a
    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v8, v0, Lcom/fmm/ds/d/u;->a:I

    const/4 v1, 0x0

    iget-object v9, v6, Lcom/fmm/ds/c/d;->e:Lcom/fmm/ds/c/av;

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "XX"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v3

    if-eqz v3, :cond_13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Status Response : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v3, "424"

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_c

    const-string v1, "Add"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    :goto_2
    const-string v3, "4XX"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "5XX"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_b
    const/4 v2, 0x1

    if-ne v1, v2, :cond_11

    packed-switch v8, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_c
    :goto_3
    const-string v0, "201"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    move v2, v0

    :goto_4
    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_2b

    const-string v0, "424"

    iget-object v3, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_d

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x7

    if-eq v0, v3, :cond_d

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_d

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x1

    if-eq v0, v3, :cond_d

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_3

    :cond_d
    const/4 v3, -0x1

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    if-eqz v4, :cond_25

    invoke-static {v4}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v4}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v4

    move-object v5, v4

    move-object v4, v0

    move v0, v3

    :goto_5
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2a

    const/4 v3, 0x1

    if-ne v1, v3, :cond_23

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v8, v3, v2}, Lcom/fmm/ds/b/q;->a(III)I

    move-result v3

    :goto_6
    if-nez v3, :cond_e

    const-string v0, "Delete"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v10, v0, Lcom/fmm/ds/d/u;->s:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v0, Lcom/fmm/ds/d/u;->s:I

    :cond_e
    const/4 v0, 0x0

    invoke-virtual {v9, v4, v0}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v5}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v5}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v4

    move-object v5, v4

    move-object v4, v0

    move v0, v3

    goto :goto_5

    :cond_f
    const-string v1, "Replace"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x2

    goto/16 :goto_2

    :cond_10
    const/4 v1, 0x4

    goto/16 :goto_2

    :pswitch_0
    iget-object v0, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v2, v0, Lcom/fmm/ds/d/j;->t:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/fmm/ds/d/j;->t:I

    goto/16 :goto_3

    :cond_11
    const/4 v2, 0x2

    if-ne v1, v2, :cond_12

    const-string v2, "CMD_REPLACE"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    packed-switch v8, :pswitch_data_1

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_1
    iget-object v0, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v2, v0, Lcom/fmm/ds/d/j;->u:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/fmm/ds/d/j;->u:I

    goto/16 :goto_3

    :cond_12
    packed-switch v8, :pswitch_data_2

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_2
    iget-object v0, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v2, v0, Lcom/fmm/ds/d/j;->v:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/fmm/ds/d/j;->v:I

    goto/16 :goto_3

    :cond_13
    const-string v1, "Add"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    const/4 v1, 0x1

    :goto_7
    const-string v3, "4XX"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string v3, "5XX"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_14
    const/4 v2, 0x1

    if-ne v1, v2, :cond_17

    packed-switch v8, :pswitch_data_3

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_15
    const-string v1, "Replace"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    const/4 v1, 0x2

    goto :goto_7

    :cond_16
    const/4 v1, 0x4

    goto :goto_7

    :pswitch_3
    iget-object v0, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v2, v0, Lcom/fmm/ds/d/j;->t:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/fmm/ds/d/j;->t:I

    goto/16 :goto_3

    :cond_17
    const/4 v2, 0x2

    if-ne v1, v2, :cond_18

    const-string v2, "CMD_REPLACE"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    packed-switch v8, :pswitch_data_4

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_4
    iget-object v0, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v2, v0, Lcom/fmm/ds/d/j;->u:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/fmm/ds/d/j;->u:I

    goto/16 :goto_3

    :cond_18
    packed-switch v8, :pswitch_data_5

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_5
    iget-object v0, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v2, v0, Lcom/fmm/ds/d/j;->v:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/fmm/ds/d/j;->v:I

    goto/16 :goto_3

    :cond_19
    const-string v0, "200"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_4

    :cond_1a
    const-string v0, "404"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_4

    :cond_1b
    const-string v0, "211"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_4

    :cond_1c
    const-string v0, "420"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const/4 v0, -0x2

    iput v0, p1, Lcom/fmm/ds/c/bi;->r:I

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v8, v2}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;IZ)V

    move v2, v0

    goto/16 :goto_4

    :cond_1d
    const-string v0, "418"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, -0x3

    move v2, v0

    goto/16 :goto_4

    :cond_1e
    const-string v0, "208"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_4

    :cond_1f
    const-string v0, "213"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x1

    invoke-static {p1, p2}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;)V

    move v2, v0

    goto/16 :goto_4

    :cond_20
    const-string v0, "409"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_4

    :cond_21
    const-string v0, "510"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 v0, -0x5

    iput v0, p1, Lcom/fmm/ds/c/bi;->r:I

    move v2, v0

    goto/16 :goto_4

    :cond_22
    const/4 v0, -0x1

    move v2, v0

    goto/16 :goto_4

    :cond_23
    const/4 v3, 0x2

    if-ne v1, v3, :cond_24

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v8, v3, v2}, Lcom/fmm/ds/b/q;->c(III)I

    move-result v3

    goto/16 :goto_6

    :cond_24
    const/4 v3, 0x4

    if-ne v1, v3, :cond_42

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v8, v3, v2}, Lcom/fmm/ds/b/q;->b(III)I

    move-result v3

    goto/16 :goto_6

    :cond_25
    invoke-virtual {v9}, Lcom/fmm/ds/c/av;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_8
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    const/4 v4, 0x1

    if-ne v1, v4, :cond_28

    iget-object v3, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v8, v0, v2}, Lcom/fmm/ds/b/q;->a(III)I

    move-result v3

    :cond_26
    :goto_9
    if-nez v3, :cond_27

    const-string v0, "Delete"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v4, v0, Lcom/fmm/ds/d/u;->s:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lcom/fmm/ds/d/u;->s:I

    :cond_27
    invoke-virtual {v9}, Lcom/fmm/ds/c/av;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_8

    :cond_28
    const/4 v4, 0x2

    if-ne v1, v4, :cond_29

    iget-object v3, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v8, v0, v2}, Lcom/fmm/ds/b/q;->c(III)I

    move-result v3

    goto :goto_9

    :cond_29
    const/4 v4, 0x4

    if-ne v1, v4, :cond_26

    iget-object v3, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v8, v0, v2}, Lcom/fmm/ds/b/q;->b(III)I

    move-result v3

    goto :goto_9

    :cond_2a
    iget-wide v0, v9, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_41

    const/4 v0, 0x0

    :goto_a
    move-object v6, v0

    goto/16 :goto_1

    :cond_2b
    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_2c

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x7

    if-eq v0, v3, :cond_2c

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_2c

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x1

    if-eq v0, v3, :cond_2c

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->b:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_3

    :cond_2c
    const/4 v3, -0x1

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    if-eqz v4, :cond_30

    invoke-static {v4}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v4}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v4

    move-object v5, v4

    move-object v4, v0

    move v0, v3

    :goto_b
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_35

    const/4 v3, 0x1

    if-ne v1, v3, :cond_2e

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v8, v3, v2}, Lcom/fmm/ds/b/q;->a(III)I

    move-result v3

    :goto_c
    if-nez v3, :cond_2d

    const-string v0, "Delete"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v10, v0, Lcom/fmm/ds/d/u;->s:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v0, Lcom/fmm/ds/d/u;->s:I

    :cond_2d
    const/4 v0, 0x0

    invoke-virtual {v9, v4, v0}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v5}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v5}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v4

    move-object v5, v4

    move-object v4, v0

    move v0, v3

    goto :goto_b

    :cond_2e
    const/4 v3, 0x2

    if-ne v1, v3, :cond_2f

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v8, v3, v2}, Lcom/fmm/ds/b/q;->c(III)I

    move-result v3

    goto :goto_c

    :cond_2f
    const/4 v3, 0x4

    if-ne v1, v3, :cond_40

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v8, v3, v2}, Lcom/fmm/ds/b/q;->b(III)I

    move-result v3

    goto :goto_c

    :cond_30
    invoke-virtual {v9}, Lcom/fmm/ds/c/av;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_d
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_35

    const/4 v4, 0x1

    if-ne v1, v4, :cond_33

    iget-object v3, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v8, v0, v2}, Lcom/fmm/ds/b/q;->a(III)I

    move-result v3

    :cond_31
    :goto_e
    if-nez v3, :cond_32

    const-string v0, "Delete"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    iget v4, v0, Lcom/fmm/ds/d/u;->s:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lcom/fmm/ds/d/u;->s:I

    :cond_32
    invoke-virtual {v9}, Lcom/fmm/ds/c/av;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_d

    :cond_33
    const/4 v4, 0x2

    if-ne v1, v4, :cond_34

    iget-object v3, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v8, v0, v2}, Lcom/fmm/ds/b/q;->c(III)I

    move-result v3

    goto :goto_e

    :cond_34
    const/4 v4, 0x4

    if-ne v1, v4, :cond_31

    iget-object v3, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v8, v0, v2}, Lcom/fmm/ds/b/q;->b(III)I

    move-result v3

    goto :goto_e

    :cond_35
    iget-wide v0, v9, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    const/4 v6, 0x0

    goto/16 :goto_1

    :cond_36
    const-string v0, "Map"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    const-string v0, "CMD_MAP"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_37
    const-string v0, "Alert"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-static {}, Lcom/fmm/ds/b/a;->o()Z

    move-result v0

    if-eqz v0, :cond_38

    const/4 v0, 0x6

    iput v0, p1, Lcom/fmm/ds/c/bi;->n:I

    const-string v0, "200"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    const-string v0, "[suspend&resume] supported in server"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_38
    :goto_f
    const-string v0, "508"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/fmm/ds/c/av;->a(I)V

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_10
    if-eqz v0, :cond_3a

    const/4 v2, 0x1

    iput v2, v0, Lcom/fmm/ds/d/u;->b:I

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_10

    :cond_39
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/fmm/ds/b/a;->c(Z)V

    const-string v0, "[suspend&resume] notsupported in server"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_f

    :cond_3a
    const-string v0, "406"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v0, -0x1

    iput v0, p1, Lcom/fmm/ds/c/bi;->s:I

    :cond_3b
    :goto_11
    const-string v0, "CMD_ALERT"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p2, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    const-string v1, "200"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3c

    iget-object v0, p2, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    const-string v1, "202"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3c

    iget-object v0, p2, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    const-string v1, "204"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3c

    iget-object v0, p2, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    const-string v1, "203"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3c

    iget-object v0, p2, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    const-string v1, "205"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_3c
    if-eqz v6, :cond_3

    iget-object v0, v6, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    if-eqz v0, :cond_3

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Alert command status[%s] DB_ID[%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p2, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    iget-object v4, v4, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(I)V

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_12
    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    iget-object v2, v2, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3f

    const-string v1, "400"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3d

    const-string v1, "401"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3d

    const-string v1, "403"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3d

    const-string v1, "404"

    iget-object v2, p2, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_3d
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/fmm/ds/d/u;->u:Z

    goto/16 :goto_1

    :cond_3e
    const-string v0, "200"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    const/4 v0, 0x1

    iput v0, p1, Lcom/fmm/ds/c/bi;->s:I

    goto/16 :goto_11

    :cond_3f
    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_12

    :cond_40
    move v3, v0

    goto/16 :goto_c

    :cond_41
    move-object v0, v6

    goto/16 :goto_a

    :cond_42
    move v3, v0

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/al;)V
    .locals 4

    const/4 v1, 0x0

    check-cast p1, Lcom/fmm/ds/c/bi;

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    const-string v0, "start to handle sync cmd"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/fmm/ds/c/bi;->I:Z

    iget v0, p2, Lcom/fmm/ds/c/al;->g:I

    if-eqz v0, :cond_3

    iget v0, p2, Lcom/fmm/ds/c/al;->g:I

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nNumOfChanges : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(I)V

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p2, Lcom/fmm/ds/c/al;->f:Ljava/lang/String;

    iget-object v2, v0, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "ws.dbInfo setted by db"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iput-object v0, p1, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    if-eqz v0, :cond_5

    const-string v0, "SYNC status OK"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v0, "200"

    :goto_2
    iget v1, p2, Lcom/fmm/ds/c/al;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Sync"

    invoke-static {p1, v1, v2, v0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v0

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/fmm/ds/c/ax;

    new-instance v2, Ljava/lang/String;

    iget-object v3, p2, Lcom/fmm/ds/c/al;->f:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    new-instance v1, Lcom/fmm/ds/c/ax;

    new-instance v2, Ljava/lang/String;

    iget-object v3, p2, Lcom/fmm/ds/c/al;->e:Lcom/fmm/ds/c/ao;

    iget-object v3, v3, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v1, p2, Lcom/fmm/ds/c/al;->b:Z

    if-nez v1, :cond_2

    iget-boolean v1, p1, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    invoke-virtual {v0}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/d/u;

    goto :goto_1

    :cond_5
    const-string v0, "SYNC status FAIL"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v0, "500"

    goto :goto_2
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/an;)V
    .locals 8

    const-wide/16 v6, 0x1400

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x100000

    check-cast p1, Lcom/fmm/ds/c/bi;

    iget v0, p2, Lcom/fmm/ds/c/an;->d:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/ds/c/bi;->C:Ljava/lang/String;

    iget-object v0, p2, Lcom/fmm/ds/c/an;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/fmm/ds/c/an;->h:Ljava/lang/String;

    iput-object v0, p1, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    :cond_0
    iget-object v0, p2, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_7

    iget-object v0, p2, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    iget-wide v0, v0, Lcom/fmm/ds/c/z;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    iget-object v0, p2, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    iget-wide v0, v0, Lcom/fmm/ds/c/z;->i:J

    iput-wide v0, p1, Lcom/fmm/ds/c/bi;->j:J

    iget-wide v0, p1, Lcom/fmm/ds/c/bi;->j:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-wide v0, p1, Lcom/fmm/ds/c/bi;->j:J

    const-wide/32 v2, 0x500000

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    :cond_1
    iput-wide v4, p1, Lcom/fmm/ds/c/bi;->j:J

    :cond_2
    :goto_0
    iget-object v0, p2, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    iget v0, v0, Lcom/fmm/ds/c/z;->h:I

    if-lez v0, :cond_6

    iget-object v0, p2, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    iget v0, v0, Lcom/fmm/ds/c/z;->h:I

    int-to-long v0, v0

    iput-wide v0, p1, Lcom/fmm/ds/c/bi;->k:J

    :goto_1
    const-string v0, "0"

    const-string v1, "SyncHdr"

    const-string v2, "200"

    invoke-static {p1, v0, v1, v2}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v1

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/fmm/ds/c/an;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Lcom/fmm/ds/c/ax;

    iget-object v2, p2, Lcom/fmm/ds/c/an;->h:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    :goto_2
    iput-object v0, v1, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    new-instance v0, Lcom/fmm/ds/c/ax;

    iget-object v2, p1, Lcom/fmm/ds/c/bi;->z:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v0, v1, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    :cond_3
    if-eqz v1, :cond_4

    iget v0, p2, Lcom/fmm/ds/c/an;->i:I

    if-nez v0, :cond_9

    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/fmm/ds/c/bi;->Y:Z

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_4
    :goto_3
    return-void

    :cond_5
    iput-wide v4, p1, Lcom/fmm/ds/c/bi;->j:J

    goto :goto_0

    :cond_6
    iput-wide v6, p1, Lcom/fmm/ds/c/bi;->k:J

    goto :goto_1

    :cond_7
    iput-wide v6, p1, Lcom/fmm/ds/c/bi;->k:J

    iput-wide v4, p1, Lcom/fmm/ds/c/bi;->j:J

    goto :goto_1

    :cond_8
    new-instance v0, Lcom/fmm/ds/c/ax;

    iget-object v2, p1, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_2

    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/fmm/ds/c/bi;->Y:Z

    goto :goto_3
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/c;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/e;)V
    .locals 20

    check-cast p1, Lcom/fmm/ds/c/bi;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    const/4 v10, 0x0

    const-string v8, ""

    const-string v9, ""

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v2, Lcom/fmm/ds/d/h;

    const/4 v2, 0x0

    const-string v3, "handle add cmd"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-eqz v13, :cond_10

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/ds/c/e;->e:Lcom/fmm/ds/c/ax;

    invoke-static {v3}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/ds/c/u;

    invoke-static {v3}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v3

    move-object v11, v2

    move-object v12, v9

    move v2, v5

    move-object v5, v8

    move/from16 v19, v7

    move-object v7, v10

    move-object v10, v3

    move/from16 v3, v19

    :goto_0
    if-eqz v11, :cond_0

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_0
    :goto_1
    if-nez v6, :cond_1

    const/4 v2, 0x1

    if-le v4, v2, :cond_1

    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;)V

    :cond_1
    if-eqz v7, :cond_2

    move-object/from16 v0, p2

    iget v2, v0, Lcom/fmm/ds/c/e;->b:I

    if-nez v2, :cond_2

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v2, v7}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_2
    return-void

    :cond_3
    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/fmm/ds/c/e;->d:Lcom/fmm/ds/c/z;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v11, v8}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/u;Lcom/fmm/ds/c/z;)I

    move-result v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "retlo  : "

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-nez v8, :cond_5

    new-instance v5, Lcom/fmm/ds/c/b;

    invoke-direct {v5}, Lcom/fmm/ds/c/b;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v11, v5}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/e;Lcom/fmm/ds/c/u;Lcom/fmm/ds/c/b;)Lcom/fmm/ds/c/b;

    move-result-object v8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ws.m_dbInfo.id : "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v9, v9, Lcom/fmm/ds/d/u;->a:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/fmm/ds/c/bi;->V:Lcom/fmm/ds/c/bj;

    iget-object v9, v9, Lcom/fmm/ds/c/bj;->c:[C

    invoke-direct {v5, v9}, Ljava/lang/String;-><init>([C)V

    iput-object v5, v8, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/ds/c/bi;->V:Lcom/fmm/ds/c/bj;

    iget v5, v5, Lcom/fmm/ds/c/bj;->f:I

    iput v5, v8, Lcom/fmm/ds/c/b;->g:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v5, v5, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v14, v14, Lcom/fmm/ds/d/u;->a:I

    move-object/from16 v0, p1

    iget v15, v0, Lcom/fmm/ds/c/bi;->H:I

    invoke-virtual {v5, v9, v14, v8, v15}, Lcom/fmm/ds/b/q;->a(Ljava/lang/Object;ILcom/fmm/ds/c/b;I)I

    move-result v5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "xdsAddItem ret : "

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    const-string v5, "510"

    add-int/lit8 v2, v2, 0x1

    :goto_2
    invoke-virtual/range {p0 .. p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/bi;)V

    move v8, v4

    move-object v9, v5

    move v4, v2

    move v5, v3

    :goto_3
    if-eq v9, v12, :cond_15

    if-eqz v7, :cond_14

    move-object/from16 v0, p2

    iget v2, v0, Lcom/fmm/ds/c/e;->b:I

    if-nez v2, :cond_4

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v2, :cond_4

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v2, v7}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_4
    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p2

    iget v3, v0, Lcom/fmm/ds/c/e;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v6, "Add"

    move-object/from16 v0, p1

    invoke-static {v0, v3, v6, v9}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v3

    move v6, v2

    :goto_5
    new-instance v2, Ljava/lang/String;

    iget-object v7, v11, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3, v2, v7}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v10}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/ds/c/u;

    invoke-static {v10}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v7

    move-object v10, v7

    move-object v11, v2

    move-object v12, v9

    move v2, v4

    move-object v7, v3

    move v4, v8

    move v3, v5

    move-object v5, v9

    goto/16 :goto_0

    :pswitch_1
    const-string v5, "success process add cmd"

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v5, "201"

    add-int/lit8 v4, v4, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "addItem luid : "

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v14, v8, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v9, v9, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v14, v14, Lcom/fmm/ds/d/u;->a:I

    iget v15, v8, Lcom/fmm/ds/c/b;->a:I

    iget-object v0, v8, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v9, v14, v15, v0, v1}, Lcom/fmm/ds/b/q;->a(IILjava/lang/String;Ljava/lang/String;)I

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "xdsAddItem item.luid : "

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v14, v8, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v8, v8, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    iget-object v9, v11, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-static {v13, v8, v9}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/d/u;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :pswitch_2
    const-string v5, "500"

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :pswitch_3
    const-string v5, "500"

    goto/16 :goto_2

    :pswitch_4
    const-string v5, "420"

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v8, v8, Lcom/fmm/ds/d/u;->a:I

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v8, v9}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;IZ)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :pswitch_5
    const-string v5, "418"

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_5
    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    const/4 v5, 0x0

    const-string v8, "222"

    move-object/from16 v0, p1

    invoke-static {v0, v5, v8}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/z;Ljava/lang/String;)Lcom/fmm/ds/c/f;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    invoke-virtual {v8, v5}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    const-string v5, "213"

    move v8, v4

    move-object v9, v5

    move v4, v2

    move v5, v3

    goto/16 :goto_3

    :cond_6
    const/4 v9, -0x1

    if-ne v8, v9, :cond_7

    const-string v5, "424"

    move v8, v4

    move-object v9, v5

    move v4, v2

    move v5, v3

    goto/16 :goto_3

    :cond_7
    const/4 v9, -0x3

    if-ne v8, v9, :cond_8

    const/4 v8, 0x0

    const-string v9, "223"

    move-object/from16 v0, p1

    invoke-static {v0, v8, v9}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/z;Ljava/lang/String;)Lcom/fmm/ds/c/f;

    move-result-object v8

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    invoke-virtual {v9, v8}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    move v8, v4

    move-object v9, v5

    move v4, v2

    move v5, v3

    goto/16 :goto_3

    :cond_8
    const/4 v9, -0x2

    if-ne v8, v9, :cond_16

    const-string v5, "411"

    move v8, v4

    move-object v9, v5

    move v4, v2

    move v5, v3

    goto/16 :goto_3

    :cond_9
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/fmm/ds/c/e;->d:Lcom/fmm/ds/c/z;

    if-eqz v3, :cond_13

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/fmm/ds/c/e;->d:Lcom/fmm/ds/c/z;

    iget-object v2, v2, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    move-object v3, v2

    :goto_6
    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/fmm/ds/c/e;->e:Lcom/fmm/ds/c/ax;

    invoke-static {v8}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/ds/c/u;

    invoke-static {v8}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v8

    move-object v11, v2

    move-object v12, v9

    move v2, v5

    move-object v9, v10

    move-object v10, v8

    move v5, v4

    move v4, v7

    move v8, v6

    :goto_7
    if-eqz v11, :cond_12

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v6

    if-eqz v6, :cond_a

    move v4, v5

    move v6, v8

    move-object v7, v9

    goto/16 :goto_1

    :cond_a
    new-instance v7, Lcom/fmm/ds/c/b;

    invoke-direct {v7}, Lcom/fmm/ds/c/b;-><init>()V

    iget-object v6, v11, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-eqz v6, :cond_e

    iget-object v6, v11, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v6, v6, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, v11, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v6, v6, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v6, v7, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    :cond_b
    :goto_8
    iget-object v6, v11, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v6, v6, Lcom/fmm/ds/c/ab;->b:[C

    invoke-static {v6}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    iget-object v6, v11, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    const/4 v14, 0x0

    iput-object v14, v6, Lcom/fmm/ds/c/ab;->b:[C

    iget-object v6, v11, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v6, v6, Lcom/fmm/ds/c/ab;->c:I

    iput v6, v7, Lcom/fmm/ds/c/b;->g:I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v6, v6, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v15, v15, Lcom/fmm/ds/d/u;->a:I

    move-object/from16 v0, p1

    iget v0, v0, Lcom/fmm/ds/c/bi;->H:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v6, v14, v15, v7, v0}, Lcom/fmm/ds/b/q;->a(Ljava/lang/Object;ILcom/fmm/ds/c/b;I)I

    move-result v6

    packed-switch v6, :pswitch_data_1

    :pswitch_6
    const-string v6, "510"

    add-int/lit8 v2, v2, 0x1

    move-object v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    :goto_9
    invoke-virtual {v7, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    if-eqz v9, :cond_11

    move-object/from16 v0, p2

    iget v2, v0, Lcom/fmm/ds/c/e;->b:I

    if-nez v2, :cond_c

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v2, :cond_c

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v2, v9}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_c
    const/4 v2, 0x1

    :goto_a
    move-object/from16 v0, p2

    iget v8, v0, Lcom/fmm/ds/c/e;->a:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "Add"

    move-object/from16 v0, p1

    invoke-static {v0, v8, v9, v7}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v8

    move-object v9, v8

    move v8, v2

    :cond_d
    new-instance v2, Ljava/lang/String;

    iget-object v11, v11, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-direct {v2, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v9, v2, v11}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v10}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/ds/c/u;

    invoke-static {v10}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v10

    move-object v11, v2

    move-object v12, v7

    move v2, v4

    move v4, v5

    move v5, v6

    goto/16 :goto_7

    :cond_e
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_f

    iput-object v3, v7, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    goto/16 :goto_8

    :cond_f
    const-string v6, " WARNING !!  Server didnot send MIME Type."

    invoke-static {v6}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_8

    :pswitch_7
    const-string v6, " success process add cmd."

    invoke-static {v6}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v6, "201"

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v14, v14, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v15, v15, Lcom/fmm/ds/d/u;->a:I

    iget v0, v7, Lcom/fmm/ds/c/b;->a:I

    move/from16 v16, v0

    iget-object v0, v7, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v14 .. v18}, Lcom/fmm/ds/b/q;->a(IILjava/lang/String;Ljava/lang/String;)I

    iget-object v7, v7, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    iget-object v14, v11, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-static {v13, v7, v14}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/d/u;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    move-object v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    goto/16 :goto_9

    :pswitch_8
    const-string v6, "500"

    add-int/lit8 v2, v2, 0x1

    move-object v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    goto/16 :goto_9

    :pswitch_9
    const-string v6, "500"

    move-object v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    goto/16 :goto_9

    :pswitch_a
    const-string v6, " DBG_XDS_DEVICE_FULL."

    invoke-static {v6}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v6, "420"

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v7, v7, Lcom/fmm/ds/d/u;->a:I

    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v7, v14}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;IZ)V

    add-int/lit8 v2, v2, 0x1

    move-object v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    goto/16 :goto_9

    :pswitch_b
    const-string v6, "418"

    add-int/lit8 v2, v2, 0x1

    move-object v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    goto/16 :goto_9

    :pswitch_c
    const-string v6, "406"

    add-int/lit8 v2, v2, 0x1

    move-object v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    goto/16 :goto_9

    :cond_10
    const-string v2, "500"

    move-object/from16 v0, p2

    iget v3, v0, Lcom/fmm/ds/c/e;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "Add"

    move-object/from16 v0, p1

    invoke-static {v0, v3, v5, v2}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v7

    goto/16 :goto_1

    :cond_11
    move v2, v8

    goto/16 :goto_a

    :cond_12
    move v4, v5

    move v6, v8

    move-object v7, v9

    goto/16 :goto_1

    :cond_13
    move-object v3, v2

    goto/16 :goto_6

    :cond_14
    move v2, v6

    goto/16 :goto_4

    :cond_15
    move-object v3, v7

    goto/16 :goto_5

    :cond_16
    move v8, v4

    move-object v9, v5

    move v4, v2

    move v5, v3

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch -0x5
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x6
        :pswitch_c
        :pswitch_9
        :pswitch_6
        :pswitch_b
        :pswitch_a
        :pswitch_8
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/f;)V
    .locals 11

    check-cast p1, Lcom/fmm/ds/c/bi;

    iget-object v4, p1, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    const-string v1, "handle Alert cmd"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v1, p2, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    const-string v2, "222"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v0, p2, Lcom/fmm/ds/c/f;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Alert"

    const-string v2, "200"

    invoke-static {p1, v0, v1, v2}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p2, Lcom/fmm/ds/c/f;->b:I

    if-nez v1, :cond_0

    iget-boolean v1, p1, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p2, Lcom/fmm/ds/c/f;->f:Lcom/fmm/ds/c/ax;

    if-eqz v1, :cond_f

    iget-object v2, p2, Lcom/fmm/ds/c/f;->f:Lcom/fmm/ds/c/ax;

    invoke-static {v2}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/c/u;

    invoke-static {v2}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lcom/fmm/ds/c/av;->a(I)V

    invoke-virtual {v4}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/ds/d/u;

    move-object v10, v3

    move-object v3, v2

    move-object v2, v10

    :goto_1
    if-eqz v3, :cond_d

    iget-object v5, v1, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    iget-object v6, v3, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    iget-object v5, v1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v5, v5, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iget-object v6, v3, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    const-string v2, "target and sorce are same"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "target.pLocURI [%s], sorce [%s]"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v8, v8, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v3, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v2, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v2, v1, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v2, v2, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    iget-object v2, v2, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "item.meta.anchor.next is empty !!"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_2
    iget v2, v3, Lcom/fmm/ds/d/u;->b:I

    const/16 v5, 0x9

    if-eq v2, v5, :cond_3

    iget-object v2, p2, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    const-string v5, "201"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    iput v2, v3, Lcom/fmm/ds/d/u;->b:I

    :cond_3
    :goto_2
    iget v2, v3, Lcom/fmm/ds/d/u;->b:I

    const/4 v5, 0x1

    if-eq v2, v5, :cond_4

    iget v2, v3, Lcom/fmm/ds/d/u;->b:I

    const/4 v5, 0x6

    if-eq v2, v5, :cond_4

    iget v2, v3, Lcom/fmm/ds/d/u;->b:I

    const/4 v5, 0x5

    if-ne v2, v5, :cond_5

    :cond_4
    const-string v2, "00000000T000000Z"

    iput-object v2, v3, Lcom/fmm/ds/d/u;->e:Ljava/lang/String;

    :cond_5
    iget v2, v3, Lcom/fmm/ds/d/u;->b:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    iget v2, p2, Lcom/fmm/ds/c/f;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v5, "Alert"

    const-string v6, "500"

    invoke-static {p1, v2, v5, v6}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v2

    const/4 v5, 0x0

    iput-boolean v5, v3, Lcom/fmm/ds/d/u;->u:Z

    move-object v3, v2

    :goto_3
    invoke-virtual {v4}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/fmm/ds/d/u;

    move-object v10, v3

    move-object v3, v2

    move-object v2, v10

    goto/16 :goto_1

    :cond_6
    iget-object v2, p2, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    const-string v5, "200"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x2

    iput v2, v3, Lcom/fmm/ds/d/u;->b:I

    goto :goto_2

    :cond_7
    iget-object v2, p2, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    const-string v5, "202"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x3

    iput v2, v3, Lcom/fmm/ds/d/u;->b:I

    goto :goto_2

    :cond_8
    iget-object v2, p2, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    const-string v5, "203"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x6

    iput v2, v3, Lcom/fmm/ds/d/u;->b:I

    goto :goto_2

    :cond_9
    iget-object v2, p2, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    const-string v5, "204"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x4

    iput v2, v3, Lcom/fmm/ds/d/u;->b:I

    goto :goto_2

    :cond_a
    iget-object v2, p2, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    const-string v5, "205"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x5

    iput v2, v3, Lcom/fmm/ds/d/u;->b:I

    goto :goto_2

    :cond_b
    const/4 v2, 0x0

    iput v2, v3, Lcom/fmm/ds/d/u;->b:I

    goto :goto_2

    :pswitch_1
    iget v2, p2, Lcom/fmm/ds/c/f;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v5, "Alert"

    const-string v6, "200"

    invoke-static {p1, v2, v5, v6}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v2

    new-instance v5, Lcom/fmm/ds/c/ax;

    iget-object v6, v1, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v5, v2, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    new-instance v5, Lcom/fmm/ds/c/ax;

    iget-object v6, v1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v6, v6, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v5, v2, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    new-instance v5, Lcom/fmm/ds/c/u;

    invoke-direct {v5}, Lcom/fmm/ds/c/u;-><init>()V

    new-instance v6, Lcom/fmm/ds/c/aa;

    invoke-direct {v6}, Lcom/fmm/ds/c/aa;-><init>()V

    new-instance v7, Lcom/fmm/ds/c/ab;

    invoke-direct {v7}, Lcom/fmm/ds/c/ab;-><init>()V

    new-instance v8, Ljava/lang/String;

    iget-object v9, v1, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v9, v9, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    iget-object v9, v9, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v8, v6, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    iput-object v7, v5, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v7, v5, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    const/4 v8, 0x2

    iput v8, v7, Lcom/fmm/ds/c/ab;->a:I

    iget-object v7, v5, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iput-object v6, v7, Lcom/fmm/ds/c/ab;->d:Lcom/fmm/ds/c/aa;

    new-instance v6, Lcom/fmm/ds/c/ax;

    invoke-direct {v6, v5}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v6, v2, Lcom/fmm/ds/c/ak;->j:Lcom/fmm/ds/c/ax;

    new-instance v5, Lcom/fmm/ds/b/d;

    invoke-direct {v5}, Lcom/fmm/ds/b/d;-><init>()V

    iget-object v6, p1, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v6, v6, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-virtual {v6, v3, v5}, Lcom/fmm/ds/b/q;->a(Lcom/fmm/ds/d/u;Lcom/fmm/ds/b/d;)V

    iget v5, v5, Lcom/fmm/ds/b/d;->a:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "count : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v6, v3, Lcom/fmm/ds/d/u;->a:I

    packed-switch v6, :pswitch_data_1

    const-string v5, "not support id"

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_4
    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/fmm/ds/d/u;->u:Z

    move-object v3, v2

    goto/16 :goto_3

    :pswitch_2
    iget-object v6, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v7, v3, Lcom/fmm/ds/d/u;->b:I

    iput v7, v6, Lcom/fmm/ds/d/j;->c:I

    iget-object v6, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput v5, v6, Lcom/fmm/ds/d/j;->p:I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CallLog_SyncInfo.P2STotalItem  : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v6, v6, Lcom/fmm/ds/d/j;->p:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_c
    const-string v3, "target and sorce are not same"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move-object v3, v2

    goto/16 :goto_3

    :cond_d
    move-object v0, v2

    invoke-static {}, Lcom/fmm/ds/d/a;->b()V

    :goto_5
    if-nez v0, :cond_e

    iget v0, p2, Lcom/fmm/ds/c/f;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Alert"

    const-string v2, "500"

    invoke-static {p1, v0, v1, v2}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v0

    :cond_e
    iget v1, p2, Lcom/fmm/ds/c/f;->b:I

    if-nez v1, :cond_0

    iget-boolean v1, p1, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_f
    move-object v0, v3

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/i;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/m;)V
    .locals 16

    check-cast p1, Lcom/fmm/ds/c/bi;

    const/4 v8, 0x0

    const-string v1, ""

    const-string v7, ""

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v1, Lcom/fmm/ds/d/h;

    const-string v1, "handle delete cmd"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    if-eqz v1, :cond_6

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/fmm/ds/c/m;->g:Lcom/fmm/ds/c/ax;

    invoke-static {v6}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/c/u;

    invoke-static {v6}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v6

    move-object v9, v1

    move-object v10, v7

    move v1, v3

    move v3, v2

    move-object v2, v8

    move-object v8, v6

    :goto_0
    if-eqz v9, :cond_8

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v6

    if-eqz v6, :cond_2

    move v15, v3

    move-object v3, v2

    move v2, v1

    move v1, v15

    :goto_1
    if-eqz v3, :cond_1

    if-nez v2, :cond_0

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;)V

    :cond_0
    move-object/from16 v0, p2

    iget v1, v0, Lcom/fmm/ds/c/m;->b:I

    if-nez v1, :cond_1

    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v1, :cond_1

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v3}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v7, v3, 0x1

    iget-object v3, v9, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v3, v3, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v6, 0x3f

    if-ne v3, v6, :cond_5

    const-string v3, "200"

    move-object v6, v3

    :goto_2
    if-eq v6, v10, :cond_7

    if-eqz v2, :cond_4

    move-object/from16 v0, p2

    iget v3, v0, Lcom/fmm/ds/c/m;->b:I

    if-nez v3, :cond_4

    move-object/from16 v0, p2

    iget v1, v0, Lcom/fmm/ds/c/m;->b:I

    if-nez v1, :cond_3

    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v1, :cond_3

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v2}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_3
    const/4 v1, 0x1

    :cond_4
    move-object/from16 v0, p2

    iget v2, v0, Lcom/fmm/ds/c/m;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Delete"

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v6}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v2

    move-object v3, v2

    move v2, v1

    :goto_3
    const/4 v1, 0x0

    new-instance v10, Ljava/lang/String;

    iget-object v9, v9, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v9, v9, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-direct {v10, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-static {v0, v3, v1, v10}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v8}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/c/u;

    invoke-static {v8}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v8

    move-object v9, v1

    move-object v10, v6

    move v1, v2

    move-object v2, v3

    move v3, v7

    goto/16 :goto_0

    :cond_5
    new-instance v3, Lcom/fmm/ds/c/b;

    invoke-direct {v3}, Lcom/fmm/ds/c/b;-><init>()V

    iget-object v6, v9, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v6, v6, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iput-object v6, v3, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v6, v6, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v12, v12, Lcom/fmm/ds/d/u;->a:I

    invoke-virtual {v6, v11, v12, v3}, Lcom/fmm/ds/b/q;->a(Ljava/lang/Object;ILcom/fmm/ds/c/b;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    add-int/lit8 v3, v4, 0x1

    const-string v4, "500"

    move-object v6, v4

    move v4, v3

    goto :goto_2

    :pswitch_1
    const-string v6, "success process delete cmd"

    invoke-static {v6}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v6, "200"

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v11, v11, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v12, v12, Lcom/fmm/ds/d/u;->a:I

    iget v13, v3, Lcom/fmm/ds/c/b;->a:I

    iget-object v3, v3, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v3, v14}, Lcom/fmm/ds/b/q;->b(IILjava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_2

    :pswitch_2
    add-int/lit8 v3, v4, 0x1

    const-string v4, "500"

    move-object v6, v4

    move v4, v3

    goto/16 :goto_2

    :pswitch_3
    add-int/lit8 v3, v4, 0x1

    const-string v4, "211"

    move-object v6, v4

    move v4, v3

    goto/16 :goto_2

    :cond_6
    const-string v1, "500"

    move-object/from16 v0, p2

    iget v4, v0, Lcom/fmm/ds/c/m;->a:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "Delete"

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v1}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v1

    move v15, v2

    move v2, v3

    move-object v3, v1

    move v1, v15

    goto/16 :goto_1

    :cond_7
    move-object v3, v2

    move v2, v1

    goto/16 :goto_3

    :cond_8
    move v15, v3

    move-object v3, v2

    move v2, v1

    move v1, v15

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/t;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p1, Lcom/fmm/ds/c/bi;

    const-string v0, "handle Get cmd."

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v3, p2, Lcom/fmm/ds/c/t;->f:Lcom/fmm/ds/c/ax;

    invoke-static {v3}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/u;

    invoke-static {v3}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v0, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v0, v0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    const-string v3, "./devinf11"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    new-instance v0, Lcom/fmm/ds/b/e;

    invoke-direct {v0}, Lcom/fmm/ds/b/e;-><init>()V

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/b/e;->b(Ljava/lang/Object;)V

    :goto_1
    iget v1, p2, Lcom/fmm/ds/c/t;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Lcom/fmm/ds/b/e;)Lcom/fmm/ds/c/ah;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/ds/c/bi;->M:Lcom/fmm/ds/c/ah;

    iget v0, p2, Lcom/fmm/ds/c/t;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Get"

    const-string v2, "200"

    invoke-static {p1, v0, v1, v2}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    iget v1, p2, Lcom/fmm/ds/c/t;->b:I

    if-nez v1, :cond_0

    iget-boolean v1, p1, Lcom/fmm/ds/c/bi;->Y:Z

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v0, v0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    const-string v3, "./devinf12"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    move v0, v1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_3

    :cond_4
    iget-object v1, p1, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/b/e;->a(Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    iget v0, p2, Lcom/fmm/ds/c/t;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Get"

    const-string v2, "404"

    invoke-static {p1, v0, v1, v2}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;

    move-result-object v0

    goto :goto_2
.end method

.method public b(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public b(Ljava/lang/Object;Lcom/fmm/ds/c/al;)V
    .locals 1

    check-cast p1, Lcom/fmm/ds/c/bi;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/fmm/ds/c/bi;->I:Z

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    :cond_0
    return-void
.end method
