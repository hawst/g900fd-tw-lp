.class public Lcom/fmm/ds/b/l;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static a:Z

.field public static c:Landroid/os/Handler;


# instance fields
.field public b:Lcom/fmm/ds/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/fmm/ds/b/l;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    sget-boolean v0, Lcom/fmm/ds/b/l;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcom/fmm/ds/d/a;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Lcom/fmm/ds/d/a;->b(I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "XDS_PROFILE_NOT_FOUND"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a()Z
    .locals 3

    invoke-static {}, Lcom/fmm/ds/c/ba;->b()Lcom/fmm/ds/c/be;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    const-string v1, "queueData is null"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profilename is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/fmm/ds/c/be;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/fmm/ds/c/be;->a:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/fmm/ds/b/l;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/fmm/ds/c/ba;->b(Lcom/fmm/ds/c/be;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/fmm/ds/c/ba;->c()Lcom/fmm/ds/c/be;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v3, v2, Lcom/fmm/ds/c/be;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/fmm/ds/c/be;->b:Ljava/util/List;

    invoke-static {v3}, Lcom/fmm/ds/d/a;->a(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    const-string v1, "XDS_PROFILE_NOT_FOUND"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {v3}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v4

    if-eqz v4, :cond_2

    if-nez v2, :cond_3

    :cond_2
    const-string v1, "profileInfo or account_category is null!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v5, v4, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    if-eqz v5, :cond_5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, v4, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput-boolean v1, v0, Lcom/fmm/ds/d/j;->a:Z

    :goto_1
    invoke-static {v4, v3}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, v4, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput-boolean v0, v2, Lcom/fmm/ds/d/j;->a:Z

    goto :goto_1

    :cond_5
    const-string v0, "CallLog_SyncInfo is null!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method a(Landroid/os/Message;)Z
    .locals 10

    const/4 v9, 0x7

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/c/bc;

    iget-object v3, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    if-nez v3, :cond_1

    new-instance v3, Lcom/fmm/ds/b/a;

    invoke-direct {v3}, Lcom/fmm/ds/b/a;-><init>()V

    iput-object v3, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    :cond_1
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Message Type [%s] (0x%02X)"

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, v0, Lcom/fmm/ds/c/bc;->a:I

    invoke-static {v6}, Lcom/fmm/ds/c/ba;->a(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    iget v6, v0, Lcom/fmm/ds/c/bc;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v3, v0, Lcom/fmm/ds/c/bc;->a:I

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    move v1, v2

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/fmm/ds/d/h;

    invoke-direct {v0}, Lcom/fmm/ds/d/h;-><init>()V

    invoke-static {}, Lcom/fmm/ds/d/a;->c()Lcom/fmm/ds/d/h;

    move-result-object v0

    iget-object v3, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-virtual {v3, v0}, Lcom/fmm/ds/b/a;->b(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/fmm/ds/d/a;->b()V

    invoke-static {v2}, Lcom/fmm/ds/b/a;->a(Z)V

    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/b/a;->b(Z)V

    const/4 v0, 0x5

    invoke-static {v0, v7, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    const/16 v0, 0xfd

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    invoke-static {v9, v0, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_2
    invoke-static {}, Lcom/fmm/ds/c/ba;->d()V

    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v0}, Lcom/fmm/ds/f/a;->c()V

    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    const/16 v1, 0xfe

    invoke-virtual {v0, v1}, Lcom/fmm/ds/b/a;->a(I)I

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/fmm/ds/b/l;->a()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v8, v7, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "xdsInSyncMode() : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0xf

    invoke-static {v7, v0}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    invoke-static {v8, v7, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-virtual {v0, v1}, Lcom/fmm/ds/b/a;->b(Z)V

    invoke-static {}, Lcom/fmm/ds/d/a;->h()V

    invoke-direct {p0}, Lcom/fmm/ds/b/l;->b()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {v8, v7, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v0}, Lcom/fmm/ds/f/a;->a()V

    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v0}, Lcom/fmm/ds/f/a;->b()V

    invoke-static {v7, v1}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    goto/16 :goto_1

    :pswitch_4
    iget-object v3, v0, Lcom/fmm/ds/c/bc;->b:Lcom/fmm/ds/c/bd;

    if-eqz v3, :cond_12

    iget-object v0, v0, Lcom/fmm/ds/c/bc;->b:Lcom/fmm/ds/c/bd;

    iget-object v0, v0, Lcom/fmm/ds/c/bd;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    iget-object v3, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-virtual {v3, v2}, Lcom/fmm/ds/b/a;->b(Z)V

    if-ne v0, v1, :cond_7

    iget-object v1, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v1, v1, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v1}, Lcom/fmm/ds/f/a;->c()V

    :cond_7
    iget-object v1, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-virtual {v1}, Lcom/fmm/ds/b/a;->s()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/fmm/ds/c/ba;->c()Lcom/fmm/ds/c/be;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/c/ba;->a(Lcom/fmm/ds/c/be;)V

    const/4 v0, 0x3

    invoke-static {v0, v7, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_8
    iget-object v1, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v1, v1, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v1}, Lcom/fmm/ds/f/a;->c()V

    packed-switch v0, :pswitch_data_1

    :goto_3
    invoke-static {}, Lcom/fmm/ds/c/ba;->d()V

    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    const/16 v1, 0xfe

    invoke-virtual {v0, v1}, Lcom/fmm/ds/b/a;->a(I)I

    goto/16 :goto_1

    :pswitch_5
    const/16 v0, 0xd

    invoke-static {v7, v0}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    goto :goto_3

    :pswitch_6
    invoke-static {}, Lcom/fmm/ds/d/a;->c()Lcom/fmm/ds/d/h;

    move-result-object v0

    if-eqz v0, :cond_9

    iput-boolean v1, v0, Lcom/fmm/ds/d/h;->s:Z

    invoke-static {v0}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;)V

    invoke-static {v7, v8}, Lcom/fmm/ds/b/n;->a(Ljava/lang/Object;I)V

    iget-object v1, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-virtual {v1, v0}, Lcom/fmm/ds/b/a;->c(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    sget-object v1, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v3

    iput v3, v1, Lcom/fmm/ds/c/bi;->p:I

    if-eqz v0, :cond_2

    packed-switch v0, :pswitch_data_2

    :pswitch_7
    const/16 v0, 0xfe

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    :goto_4
    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    invoke-static {v9, v0, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_9
    const-string v0, "profileInfo is null"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v0, 0xc0

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    invoke-static {v9, v0, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    :pswitch_8
    const/16 v0, 0x1a

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_4

    :pswitch_9
    const/16 v0, 0xfd

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_4

    :pswitch_a
    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-virtual {v0}, Lcom/fmm/ds/b/a;->k()I

    move-result v0

    if-ne v0, v1, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XEVENT_DS_CONTINUE xdsGetIsStopping() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x8

    invoke-static {v0, v7, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_a
    if-eqz v0, :cond_2

    packed-switch v0, :pswitch_data_3

    :pswitch_b
    const/16 v0, 0xfe

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    :goto_5
    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    invoke-static {v9, v0, v7}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    :pswitch_c
    const/16 v0, 0xff

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_5

    :pswitch_d
    const/16 v0, 0xfd

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_5

    :pswitch_e
    const/16 v0, 0xfc

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_5

    :pswitch_f
    const/16 v0, 0xf7

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_5

    :pswitch_10
    const/16 v0, 0xf2

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_5

    :pswitch_11
    const/16 v0, 0xf0

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_5

    :pswitch_12
    const/16 v0, 0xf1

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_5

    :pswitch_13
    const/16 v0, 0xef

    invoke-static {v0, v2}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    goto :goto_5

    :pswitch_14
    iget-object v0, v0, Lcom/fmm/ds/c/bc;->b:Lcom/fmm/ds/c/bd;

    iget-object v0, v0, Lcom/fmm/ds/c/bd;->a:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/c/bb;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "abortCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/fmm/ds/c/bb;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v3, v0, Lcom/fmm/ds/c/bb;->a:I

    sparse-switch v3, :sswitch_data_0

    :goto_6
    iget v1, v0, Lcom/fmm/ds/c/bb;->a:I

    const/16 v3, 0xfb

    if-eq v1, v3, :cond_b

    invoke-static {}, Lcom/fmm/ds/c/ba;->d()V

    :cond_b
    iget-object v1, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v1, v1, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v1}, Lcom/fmm/ds/f/a;->c()V

    iget-object v1, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget v0, v0, Lcom/fmm/ds/c/bb;->a:I

    invoke-virtual {v1, v0}, Lcom/fmm/ds/b/a;->a(I)I

    goto/16 :goto_1

    :sswitch_0
    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "receive fail. so suspending set"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/fmm/ds/b/a;->c(Z)V

    :cond_c
    const/16 v1, 0xfc

    iput v1, v0, Lcom/fmm/ds/c/bb;->a:I

    goto :goto_6

    :sswitch_1
    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "send fail. so suspending set"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/fmm/ds/b/a;->c(Z)V

    :cond_d
    const/16 v1, 0xfd

    iput v1, v0, Lcom/fmm/ds/c/bb;->a:I

    goto :goto_6

    :sswitch_2
    const-string v1, "close network layer when XEVENT_DS_ABORT_AUTH_FAIL"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_6

    :pswitch_15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XEVENT_DS_FINISH xdsGetIsStopping() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v0}, Lcom/fmm/ds/f/a;->c()V

    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    invoke-virtual {v0}, Lcom/fmm/ds/b/a;->l()I

    goto/16 :goto_1

    :pswitch_16
    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "START SuspendSync "

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/fmm/ds/b/a;->c(Z)V

    goto/16 :goto_1

    :pswitch_17
    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v0}, Lcom/fmm/ds/f/a;->b()V

    goto/16 :goto_1

    :pswitch_18
    iget-object v0, p0, Lcom/fmm/ds/b/l;->b:Lcom/fmm/ds/b/a;

    iget-object v0, v0, Lcom/fmm/ds/b/a;->c:Lcom/fmm/ds/f/a;

    invoke-virtual {v0}, Lcom/fmm/ds/f/a;->c()V

    goto/16 :goto_1

    :pswitch_19
    const-string v1, "XEVENT_NOTI_RECEIVED"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v3, Lcom/fmm/ds/e/c;

    invoke-direct {v3}, Lcom/fmm/ds/e/c;-><init>()V

    new-instance v1, Lcom/fmm/ds/e/a;

    invoke-direct {v1}, Lcom/fmm/ds/e/a;-><init>()V

    iget-object v0, v0, Lcom/fmm/ds/c/bc;->b:Lcom/fmm/ds/c/bd;

    iget-object v0, v0, Lcom/fmm/ds/c/bd;->a:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/e/d;

    invoke-virtual {v3, v0}, Lcom/fmm/ds/e/c;->a(Ljava/lang/Object;)Lcom/fmm/ds/e/d;

    move-result-object v0

    if-nez v0, :cond_e

    const-string v0, "NotiErr: pPushMsg is NULL"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_e
    invoke-virtual {v3, v0}, Lcom/fmm/ds/e/c;->b(Lcom/fmm/ds/e/d;)Lcom/fmm/ds/e/a;

    move-result-object v4

    if-eqz v4, :cond_2

    iget v0, v4, Lcom/fmm/ds/e/a;->a:I

    packed-switch v0, :pswitch_data_4

    const-string v0, "Not Support AppID"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :cond_f
    :goto_7
    invoke-virtual {v3, v4}, Lcom/fmm/ds/e/c;->a(Lcom/fmm/ds/e/a;)V

    goto/16 :goto_1

    :pswitch_1a
    iget-object v0, v4, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    if-eqz v0, :cond_11

    iget-object v0, v4, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    invoke-static {v0}, Lcom/fmm/ds/e/b;->a(Lcom/fmm/ds/e/g;)Z

    move-result v0

    :goto_8
    iget-object v1, v4, Lcom/fmm/ds/e/a;->g:Lcom/fmm/ds/e/f;

    if-eqz v1, :cond_10

    iget-object v1, v4, Lcom/fmm/ds/e/a;->g:Lcom/fmm/ds/e/f;

    invoke-static {v1}, Lcom/fmm/ds/e/b;->a(Lcom/fmm/ds/e/f;)Z

    move-result v1

    :goto_9
    if-eqz v0, :cond_f

    if-eqz v1, :cond_f

    iget-object v0, v4, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget v0, v0, Lcom/fmm/ds/e/g;->b:I

    invoke-static {v0}, Lcom/fmm/ds/e/b;->a(I)V

    goto :goto_7

    :cond_10
    move v1, v2

    goto :goto_9

    :cond_11
    move v0, v2

    goto :goto_8

    :cond_12
    move v0, v1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_a
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_0
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x3
        :pswitch_9
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0xb
        :pswitch_13
        :pswitch_11
        :pswitch_12
        :pswitch_10
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_e
        :pswitch_d
        :pswitch_f
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0xf7 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_1a
    .end packed-switch
.end method

.method public run()V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/fmm/ds/b/m;

    invoke-direct {v0, p0}, Lcom/fmm/ds/b/m;-><init>(Lcom/fmm/ds/b/l;)V

    sput-object v0, Lcom/fmm/ds/b/l;->c:Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
