.class public Lcom/fmm/ds/b/a/b;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Ljava/util/Hashtable;

.field public h:Ljava/util/Vector;

.field public i:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/b/a/b;->g:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/b/a/b;->i:Ljava/util/Vector;

    invoke-virtual {p0}, Lcom/fmm/ds/b/a/b;->a()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/fmm/ds/b/a/b;->g:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    iget-object v0, p0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iget-object v0, p0, Lcom/fmm/ds/b/a/b;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iput v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    iput v1, p0, Lcom/fmm/ds/b/a/b;->b:I

    iput v1, p0, Lcom/fmm/ds/b/a/b;->c:I

    iput v1, p0, Lcom/fmm/ds/b/a/b;->d:I

    iput v1, p0, Lcom/fmm/ds/b/a/b;->e:I

    iput v1, p0, Lcom/fmm/ds/b/a/b;->f:I

    return-void
.end method

.method public b()Lcom/fmm/ds/b/a/c;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_1

    :goto_0
    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    iget-object v2, p0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-gt v1, v2, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/a/c;

    if-eqz v0, :cond_2

    iget v1, v0, Lcom/fmm/ds/b/a/c;->g:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    :cond_0
    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    :cond_1
    return-object v0

    :cond_2
    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    goto :goto_0
.end method

.method public c()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    :cond_0
    :goto_0
    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/a/c;

    if-eqz v0, :cond_2

    iget v1, v0, Lcom/fmm/ds/b/a/c;->g:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    :cond_1
    if-eqz v0, :cond_3

    iget v0, v0, Lcom/fmm/ds/b/a/c;->a:I

    :goto_1
    return v0

    :cond_2
    iget v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/fmm/ds/b/a/b;->a:I

    goto :goto_0

    :cond_3
    const-string v0, "CallLogUpdateItem is null"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d()Lcom/fmm/ds/b/a/c;
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/fmm/ds/b/a/b;->b:I

    iget-object v2, p0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-gt v1, v2, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    iget v1, p0, Lcom/fmm/ds/b/a/b;->b:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/a/c;

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/fmm/ds/b/a/c;->g:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    iget v1, p0, Lcom/fmm/ds/b/a/b;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/fmm/ds/b/a/b;->b:I

    return-object v0

    :cond_1
    iget v1, p0, Lcom/fmm/ds/b/a/b;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/fmm/ds/b/a/b;->b:I

    goto :goto_0
.end method

.method public e()I
    .locals 2

    iget-object v0, p0, Lcom/fmm/ds/b/a/b;->i:Ljava/util/Vector;

    iget v1, p0, Lcom/fmm/ds/b/a/b;->c:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lcom/fmm/ds/b/a/b;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/fmm/ds/b/a/b;->c:I

    return v0
.end method
