.class public Lcom/fmm/ds/b/v;
.super Ljava/lang/Object;


# instance fields
.field public a:Z

.field public b:[Lcom/fmm/ds/b/t;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/fmm/ds/b/v;->a:Z

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/fmm/ds/b/t;

    iput-object v0, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    iput-boolean v1, p0, Lcom/fmm/ds/b/v;->c:Z

    return-void
.end method

.method static synthetic a(Lcom/fmm/ds/b/v;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/fmm/ds/b/v;->c:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    new-instance v2, Lcom/fmm/ds/b/t;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/fmm/ds/b/t;-><init>(Lcom/fmm/ds/b/r;)V

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(II)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fmm/ds/b/v;->a:Z

    iget-object v0, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/fmm/ds/b/t;->d(I)Z

    return-void
.end method

.method public a(IIJ)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fmm/ds/b/v;->a:Z

    iget-object v0, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, p4, v1}, Lcom/fmm/ds/b/t;->a(IJI)Z

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "data/data/com.fmm.ds/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/fmm/ds/b/v;->a:Z

    invoke-static {v0}, Lcom/fmm/ds/d/o;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v0}, Lcom/fmm/ds/d/o;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/u;

    if-eqz v0, :cond_3

    move v1, v2

    :goto_0
    if-ge v1, v8, :cond_1

    iget-object v3, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/fmm/ds/b/t;->b()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v2

    :goto_1
    if-ge v3, v8, :cond_3

    move v1, v2

    :goto_2
    iget-object v4, v0, Lcom/fmm/ds/b/u;->a:[I

    aget v4, v4, v3

    if-ge v1, v4, :cond_2

    iget-object v4, v0, Lcom/fmm/ds/b/u;->b:[[I

    aget-object v4, v4, v3

    aget v4, v4, v1

    iget-object v5, v0, Lcom/fmm/ds/b/u;->c:[[J

    aget-object v5, v5, v3

    aget-wide v5, v5, v1

    iget-object v7, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v7, v7, v3

    invoke-virtual {v7, v4, v5, v6, v2}, Lcom/fmm/ds/b/t;->a(IJI)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_3
    iput-boolean v8, p0, Lcom/fmm/ds/b/v;->c:Z

    return-void
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/fmm/ds/b/v;->a:Z

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v1, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/fmm/ds/b/t;->b()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public b(IIJ)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fmm/ds/b/v;->a:Z

    iget-object v0, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, p4, v1}, Lcom/fmm/ds/b/t;->b(IJI)Z

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 10

    const/4 v2, 0x0

    const/4 v9, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "data/data/com.fmm.ds/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/fmm/ds/b/u;

    invoke-direct {v5}, Lcom/fmm/ds/b/u;-><init>()V

    iput-boolean v2, p0, Lcom/fmm/ds/b/v;->a:Z

    iget-object v0, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/fmm/ds/b/t;->a()I

    move-result v1

    move v3, v2

    :goto_0
    if-ge v3, v9, :cond_2

    iget-object v0, v5, Lcom/fmm/ds/b/u;->a:[I

    iget-object v6, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Lcom/fmm/ds/b/t;->a()I

    move-result v6

    aput v6, v0, v3

    move v0, v1

    move v1, v2

    :goto_1
    if-ge v1, v9, :cond_1

    if-eq v3, v1, :cond_0

    iget-object v6, v5, Lcom/fmm/ds/b/u;->a:[I

    iget-object v7, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v7, v7, v1

    invoke-virtual {v7}, Lcom/fmm/ds/b/t;->a()I

    move-result v7

    aput v7, v6, v1

    iget-object v6, v5, Lcom/fmm/ds/b/u;->a:[I

    aget v6, v6, v3

    iget-object v7, v5, Lcom/fmm/ds/b/u;->a:[I

    aget v7, v7, v1

    if-ge v6, v7, :cond_0

    iget-object v6, v5, Lcom/fmm/ds/b/u;->a:[I

    aget v6, v6, v1

    if-ge v0, v6, :cond_0

    iget-object v0, v5, Lcom/fmm/ds/b/u;->a:[I

    aget v0, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_2
    if-nez v1, :cond_4

    filled-new-array {v9, v9}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, v5, Lcom/fmm/ds/b/u;->b:[[I

    filled-new-array {v9, v9}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[J

    iput-object v0, v5, Lcom/fmm/ds/b/u;->c:[[J

    :goto_2
    move v3, v2

    :goto_3
    if-ge v3, v9, :cond_6

    iget-object v0, v5, Lcom/fmm/ds/b/u;->a:[I

    iget-object v1, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/fmm/ds/b/t;->a()I

    move-result v1

    aput v1, v0, v3

    move v1, v2

    :goto_4
    iget-object v0, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v0, v0, v3

    invoke-static {v0}, Lcom/fmm/ds/b/t;->a(Lcom/fmm/ds/b/t;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v0, v0, v3

    invoke-static {v0}, Lcom/fmm/ds/b/t;->a(Lcom/fmm/ds/b/t;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/s;

    if-eqz v0, :cond_3

    iget-object v6, v5, Lcom/fmm/ds/b/u;->b:[[I

    aget-object v6, v6, v3

    iget v7, v0, Lcom/fmm/ds/b/s;->a:I

    aput v7, v6, v1

    iget-object v6, v5, Lcom/fmm/ds/b/u;->c:[[J

    aget-object v6, v6, v3

    iget-wide v7, v0, Lcom/fmm/ds/b/s;->b:J

    aput-wide v7, v6, v1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_4
    filled-new-array {v9, v1}, [I

    move-result-object v0

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, v5, Lcom/fmm/ds/b/u;->b:[[I

    filled-new-array {v9, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[J

    iput-object v0, v5, Lcom/fmm/ds/b/u;->c:[[J

    goto :goto_2

    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_6
    invoke-static {v4, v5}, Lcom/fmm/ds/d/o;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    iput-boolean v9, p0, Lcom/fmm/ds/b/v;->c:Z

    return-void
.end method
