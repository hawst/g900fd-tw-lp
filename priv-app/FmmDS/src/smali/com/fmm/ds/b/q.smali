.class public Lcom/fmm/ds/b/q;
.super Ljava/lang/Object;


# static fields
.field private static c:Landroid/content/ContentResolver;


# instance fields
.field private a:Lcom/fmm/ds/b/a/b;

.field private b:Ljava/util/Hashtable;

.field private d:Lcom/fmm/ds/b/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/fmm/ds/b/a/b;

    invoke-direct {v0}, Lcom/fmm/ds/b/a/b;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/b/q;->b:Ljava/util/Hashtable;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/fmm/ds/b/q;->c:Landroid/content/ContentResolver;

    return-void
.end method

.method private d()V
    .locals 9

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    invoke-virtual {v0}, Lcom/fmm/ds/b/a/b;->a()V

    const-string v3, "logtype=100 or logtype=500 or logtype=1000"

    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v0, "number"

    aput-object v0, v2, v1

    const-string v0, "date"

    aput-object v0, v2, v4

    const-string v0, "duration"

    aput-object v0, v2, v5

    const/4 v0, 0x4

    const-string v1, "name"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "type"

    aput-object v1, v2, v0

    :try_start_0
    sget-object v0, Lcom/fmm/ds/b/q;->c:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_3

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    move v1, v6

    :cond_0
    :try_start_2
    new-instance v0, Lcom/fmm/ds/b/a/c;

    invoke-direct {v0}, Lcom/fmm/ds/b/a/c;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lcom/fmm/ds/b/a/c;->a:I

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/b/a/c;->b:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v0, Lcom/fmm/ds/b/a/c;->c:J

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lcom/fmm/ds/b/a/c;->d:I

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/b/a/c;->e:Ljava/lang/String;

    const/4 v3, 0x5

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lcom/fmm/ds/b/a/c;->f:I

    iget-object v3, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v3, v3, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v3, v3, Lcom/fmm/ds/b/a/b;->g:Ljava/util/Hashtable;

    iget v0, v0, Lcom/fmm/ds/b/a/c;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v4, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v4, v4, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "total counts : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v2, v7

    move v0, v6

    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v2, v7

    :goto_3
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v0

    move v0, v6

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v8, v0

    move v0, v1

    move-object v1, v8

    goto :goto_2

    :cond_3
    move v0, v6

    goto :goto_0
.end method


# virtual methods
.method public a(III)I
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "dbid [%d] luid [%d] results [%d]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, -0x2

    if-ne p3, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    if-eq p3, v7, :cond_2

    const/4 v2, -0x4

    if-ne p3, v2, :cond_1

    if-eq p3, v0, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DB Access Error. Result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v2, v0, Lcom/fmm/ds/c/bi;->p:I

    packed-switch p1, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_3
    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/fmm/ds/b/q;->b:Ljava/util/Hashtable;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v2, p2, v4, v5}, Lcom/fmm/ds/b/v;->a(IIJ)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method a(IILjava/lang/String;)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(IILjava/lang/String;Ljava/lang/String;)I
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "dbid[%d], address[%d], luid[%s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, v5}, Lcom/fmm/ds/b/q;->a(III)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "map update fail"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    return v4
.end method

.method public a(Ljava/lang/Object;ILcom/fmm/ds/c/b;)I
    .locals 1

    packed-switch p2, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;ILcom/fmm/ds/c/b;I)I
    .locals 1

    iget-object v0, p3, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x5

    :goto_0
    return v0

    :cond_0
    packed-switch p2, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;Lcom/fmm/ds/c/b;Lcom/fmm/ds/b/w;)I
    .locals 3

    check-cast p1, Lcom/fmm/ds/c/bi;

    iget-object v0, p1, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget v0, v0, Lcom/fmm/ds/d/u;->a:I

    const/4 v1, -0x5

    packed-switch v0, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p3, Lcom/fmm/ds/b/w;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object v0, p2, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    iget-object v0, p2, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p2, Lcom/fmm/ds/c/b;->g:I

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/fmm/ds/d/u;I)Lcom/fmm/ds/b/w;
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget v2, p1, Lcom/fmm/ds/d/u;->a:I

    invoke-virtual {p0, v2, v1, v0}, Lcom/fmm/ds/b/q;->a(IILjava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "########## not supported dbid : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    packed-switch v2, :pswitch_data_0

    const-string v2, "not support id"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v2, v1

    :goto_1
    if-eqz v2, :cond_2

    new-instance v1, Lcom/fmm/ds/b/w;

    invoke-direct {v1, v2, p2, v0}, Lcom/fmm/ds/b/w;-><init>(IILjava/lang/Object;)V

    :goto_2
    if-nez v1, :cond_1

    const-string v1, "null"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    packed-switch p2, :pswitch_data_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not support itemType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v2, v1

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    invoke-virtual {v1}, Lcom/fmm/ds/b/a/b;->c()I

    move-result v1

    move v2, v1

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Luid : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v1, Lcom/fmm/ds/b/w;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/fmm/ds/d/u;II)Lcom/fmm/ds/b/w;
    .locals 4

    const/4 v1, 0x0

    iget v0, p1, Lcom/fmm/ds/d/u;->a:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/fmm/ds/b/q;->a(IILjava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "########## not supported dbid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    packed-switch v0, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_1
    :goto_1
    move-object v0, v1

    :goto_2
    if-nez v0, :cond_2

    const-string v0, "null"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    packed-switch p2, :pswitch_data_1

    :pswitch_1
    const-string v0, "not support type"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    invoke-virtual {v0}, Lcom/fmm/ds/b/a/b;->b()Lcom/fmm/ds/b/a/c;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/fmm/ds/b/a/a;

    invoke-direct {v3, v2}, Lcom/fmm/ds/b/a/a;-><init>(Lcom/fmm/ds/b/a/c;)V

    new-instance v0, Lcom/fmm/ds/b/w;

    iget v2, v2, Lcom/fmm/ds/b/a/c;->a:I

    invoke-static {v3}, Lcom/fmm/ds/b/a/a;->b(Lcom/fmm/ds/b/a/a;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, p2, v3}, Lcom/fmm/ds/b/w;-><init>(IILjava/lang/Object;)V

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    invoke-virtual {v0}, Lcom/fmm/ds/b/a/b;->d()Lcom/fmm/ds/b/a/c;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/fmm/ds/b/a/a;

    invoke-direct {v3, v2}, Lcom/fmm/ds/b/a/a;-><init>(Lcom/fmm/ds/b/a/c;)V

    new-instance v0, Lcom/fmm/ds/b/w;

    iget v2, v2, Lcom/fmm/ds/b/a/c;->a:I

    invoke-static {v3}, Lcom/fmm/ds/b/a/a;->b(Lcom/fmm/ds/b/a/a;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, p2, v3}, Lcom/fmm/ds/b/w;-><init>(IILjava/lang/Object;)V

    goto :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    invoke-virtual {v0}, Lcom/fmm/ds/b/a/b;->e()I

    move-result v2

    if-lez v2, :cond_1

    new-instance v0, Lcom/fmm/ds/b/w;

    invoke-direct {v0, v2, p2, v1}, Lcom/fmm/ds/b/w;-><init>(IILjava/lang/Object;)V

    goto :goto_2

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Luid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/fmm/ds/b/w;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/b/q;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    iget-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    if-nez v0, :cond_0

    new-instance v0, Lcom/fmm/ds/b/v;

    invoke-direct {v0}, Lcom/fmm/ds/b/v;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    :cond_0
    return-void
.end method

.method public a(Lcom/fmm/ds/d/u;Lcom/fmm/ds/b/d;)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget v0, p1, Lcom/fmm/ds/d/u;->a:I

    iget v1, p1, Lcom/fmm/ds/d/u;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nSyncType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v4, v2}, Lcom/fmm/ds/b/q;->a(IILjava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not support dbid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget v2, p1, Lcom/fmm/ds/d/u;->a:I

    packed-switch v2, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v5}, Lcom/fmm/ds/b/q;->a(Z)V

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v0, v0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    iput v0, p2, Lcom/fmm/ds/b/d;->a:I

    iput v0, p1, Lcom/fmm/ds/d/u;->p:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XDS_DBID_CALLLOG add : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-ne v1, v6, :cond_2

    invoke-virtual {p0, v4}, Lcom/fmm/ds/b/q;->a(Z)V

    iget-object v1, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v1, v1, Lcom/fmm/ds/b/a/b;->d:I

    iget-object v2, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v2, v2, Lcom/fmm/ds/b/a/b;->e:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v2, v2, Lcom/fmm/ds/b/a/b;->f:I

    add-int/2addr v1, v2

    iput v1, p2, Lcom/fmm/ds/b/d;->a:I

    iget-object v1, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v1, v1, Lcom/fmm/ds/b/a/b;->d:I

    iput v1, p1, Lcom/fmm/ds/d/u;->p:I

    iget-object v1, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v1, v1, Lcom/fmm/ds/b/a/b;->e:I

    iput v1, p1, Lcom/fmm/ds/d/u;->q:I

    iget-object v1, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v1, v1, Lcom/fmm/ds/b/a/b;->f:I

    iput v1, p1, Lcom/fmm/ds/d/u;->r:I

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "dbid [%d] add [%d] rep [%d] del [%d]"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v0, v0, Lcom/fmm/ds/b/a/b;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v4, v4, Lcom/fmm/ds/b/a/b;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v0, v0, Lcom/fmm/ds/b/a/b;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not support nSyncType : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Z)V
    .locals 14

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/fmm/ds/b/q;->d()V

    if-eqz p1, :cond_0

    const-string v0, "All"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    invoke-virtual {v0}, Lcom/fmm/ds/b/v;->b()V

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v1, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v1, v1, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    iput v1, v0, Lcom/fmm/ds/b/a/b;->d:I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v0, v0, Lcom/fmm/ds/b/a/b;->d:I

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v0, v0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/a/c;

    iput v12, v0, Lcom/fmm/ds/b/a/c;->g:I

    invoke-virtual {v0}, Lcom/fmm/ds/b/a/c;->a()J

    move-result-wide v3

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "All crc [%d], callLogUpdate.m_nId [%d]"

    new-array v7, v13, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v2

    iget v8, v0, Lcom/fmm/ds/b/a/c;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/fmm/ds/b/q;->b:Ljava/util/Hashtable;

    iget v0, v0, Lcom/fmm/ds/b/a/c;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v3, v0, Lcom/fmm/ds/c/bi;->p:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Index : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v0, v2

    :goto_1
    iget-object v1, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    iget-object v1, v1, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/fmm/ds/b/t;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    iget-object v1, v1, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v1, v1, v3

    invoke-virtual {v1, v0}, Lcom/fmm/ds/b/t;->b(I)Lcom/fmm/ds/b/s;

    move-result-object v1

    iget-object v4, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v4, v4, Lcom/fmm/ds/b/a/b;->g:Ljava/util/Hashtable;

    iget v5, v1, Lcom/fmm/ds/b/s;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v4, v4, Lcom/fmm/ds/b/a/b;->i:Ljava/util/Vector;

    iget v1, v1, Lcom/fmm/ds/b/s;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v1, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v1, v1, Lcom/fmm/ds/b/a/b;->i:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    iput v1, v0, Lcom/fmm/ds/b/a/b;->f:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Delete count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v1, v1, Lcom/fmm/ds/b/a/b;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v0, v0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget-object v0, v0, Lcom/fmm/ds/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/a/c;

    invoke-virtual {v0}, Lcom/fmm/ds/b/a/c;->a()J

    move-result-wide v4

    iget-object v6, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    iget-object v6, v6, Lcom/fmm/ds/b/v;->b:[Lcom/fmm/ds/b/t;

    aget-object v6, v6, v3

    iget v7, v0, Lcom/fmm/ds/b/a/c;->a:I

    invoke-virtual {v6, v7}, Lcom/fmm/ds/b/t;->c(I)Lcom/fmm/ds/b/s;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-wide v7, v6, Lcom/fmm/ds/b/s;->b:J

    cmp-long v7, v7, v4

    if-eqz v7, :cond_3

    iput v13, v0, Lcom/fmm/ds/b/a/c;->g:I

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Replaced callLogUpdate.m_nId [%d] crc [%d]!=[%d]"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    iget v10, v0, Lcom/fmm/ds/b/a/c;->a:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v12

    iget-wide v10, v6, Lcom/fmm/ds/b/s;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v9, v13

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/fmm/ds/b/q;->b:Ljava/util/Hashtable;

    iget v0, v0, Lcom/fmm/ds/b/a/c;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v4, v0, Lcom/fmm/ds/b/a/b;->e:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/fmm/ds/b/a/b;->e:I

    :cond_3
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    iput v12, v0, Lcom/fmm/ds/b/a/c;->g:I

    iget-object v6, p0, Lcom/fmm/ds/b/q;->b:Ljava/util/Hashtable;

    iget v0, v0, Lcom/fmm/ds/b/a/c;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v4, v0, Lcom/fmm/ds/b/a/b;->d:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/fmm/ds/b/a/b;->d:I

    goto :goto_3

    :cond_5
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Add count [%d], Replace count [%d]"

    new-array v3, v13, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v4, v4, Lcom/fmm/ds/b/a/b;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v2, p0, Lcom/fmm/ds/b/q;->a:Lcom/fmm/ds/b/a/b;

    iget v2, v2, Lcom/fmm/ds/b/a/b;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v12

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_6
    return-void
.end method

.method public b()I
    .locals 2

    const-string v0, "[xdsInitMapDB]"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/fmm/ds/b/q;->a()V

    iget-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    invoke-virtual {v0}, Lcom/fmm/ds/b/v;->a()V

    iget-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    invoke-static {v0}, Lcom/fmm/ds/b/v;->a(Lcom/fmm/ds/b/v;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    const-string v1, "map_calllog.dat"

    invoke-virtual {v0, v1}, Lcom/fmm/ds/b/v;->a(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public b(III)I
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "dbid [%d] luid [%d] results [%d]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, -0x2

    if-ne p3, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    if-eq p3, v7, :cond_1

    const/4 v2, -0x4

    if-eq p3, v2, :cond_1

    if-eq p3, v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DB Access Error. Result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v0, v0, Lcom/fmm/ds/c/bi;->p:I

    packed-switch p1, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    invoke-virtual {v2, v0, p2}, Lcom/fmm/ds/b/v;->a(II)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b(IILjava/lang/String;Ljava/lang/String;)I
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "dbid[%d], address[%d], luid[%s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, v5}, Lcom/fmm/ds/b/q;->b(III)I

    return v4
.end method

.method public b(Ljava/lang/Object;ILcom/fmm/ds/c/b;I)I
    .locals 1

    if-eqz p3, :cond_0

    iget-object v0, p3, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, -0x5

    :goto_0
    return v0

    :cond_1
    packed-switch p2, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public c()I
    .locals 2

    invoke-static {}, Lcom/fmm/ds/d/a;->b()V

    const-string v0, "[xdsSaveMapTable]"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    iget-boolean v0, v0, Lcom/fmm/ds/b/v;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    const-string v1, "map_calllog.dat"

    invoke-virtual {v0, v1}, Lcom/fmm/ds/b/v;->b(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public c(III)I
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "dbid [%d] luid [%d] results [%d]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, -0x2

    if-ne p3, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    if-eq p3, v7, :cond_1

    const/4 v2, -0x4

    if-eq p3, v2, :cond_1

    if-eq p3, v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DB Access Error. Result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/fmm/ds/b/a;->a:Lcom/fmm/ds/c/bi;

    iget v2, v0, Lcom/fmm/ds/c/bi;->p:I

    packed-switch p1, :pswitch_data_0

    const-string v0, "not support id"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_2
    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/fmm/ds/b/q;->b:Ljava/util/Hashtable;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/fmm/ds/b/q;->d:Lcom/fmm/ds/b/v;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v2, p2, v4, v5}, Lcom/fmm/ds/b/v;->b(IIJ)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public c(IILjava/lang/String;Ljava/lang/String;)I
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "dbid[%d], address[%d], luid[%s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, v5}, Lcom/fmm/ds/b/q;->c(III)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "map update fail"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    return v4
.end method
