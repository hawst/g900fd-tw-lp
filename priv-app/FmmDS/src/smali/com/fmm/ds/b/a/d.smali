.class public Lcom/fmm/ds/b/a/d;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lcom/fmm/ds/b/a/e;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "encodeVCallLog"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/fmm/ds/b/a/f;->a(I)Lcom/fmm/ds/b/a/i;

    move-result-object v1

    invoke-static {}, Lcom/fmm/ds/b/a/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/fmm/ds/b/a/e;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/fmm/ds/b/a/e;->b:Ljava/lang/String;

    invoke-static {v3, v2, v1}, Lcom/fmm/ds/b/a/f;->a(ILjava/lang/String;Lcom/fmm/ds/b/a/i;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v2, p0, Lcom/fmm/ds/b/a/e;->c:Landroid/text/format/Time;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/fmm/ds/b/a/e;->c:Landroid/text/format/Time;

    invoke-static {v2, v3}, Lcom/fmm/ds/b/a/f;->a(ILandroid/text/format/Time;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/fmm/ds/b/a/e;->d:I

    invoke-static {v2, v3}, Lcom/fmm/ds/b/a/f;->a(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/fmm/ds/b/a/e;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/fmm/ds/b/a/e;->e:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/fmm/ds/b/a/f;->a(ILjava/lang/String;Lcom/fmm/ds/b/a/i;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x4

    iget v2, p0, Lcom/fmm/ds/b/a/e;->f:I

    invoke-static {v1, v2}, Lcom/fmm/ds/b/a/f;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/fmm/ds/b/a/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
