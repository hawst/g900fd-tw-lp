.class public Lcom/fmm/ds/b/a/c;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:J

.field public d:I

.field public e:Ljava/lang/String;

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/util/zip/CRC32;Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/zip/CRC32;->update([B)V

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 5

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    const-string v1, "a"

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/b/a/c;->a(Ljava/util/zip/CRC32;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fmm/ds/b/a/c;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/b/a/c;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/b/a/c;->a(Ljava/util/zip/CRC32;Ljava/lang/String;)V

    :cond_0
    iget-wide v1, p0, Lcom/fmm/ds/b/a/c;->c:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iget-wide v1, p0, Lcom/fmm/ds/b/a/c;->c:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/b/a/c;->a(Ljava/util/zip/CRC32;Ljava/lang/String;)V

    :cond_1
    iget v1, p0, Lcom/fmm/ds/b/a/c;->d:I

    if-lez v1, :cond_2

    iget v1, p0, Lcom/fmm/ds/b/a/c;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/b/a/c;->a(Ljava/util/zip/CRC32;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    return-wide v0
.end method
