.class public Lcom/fmm/ds/b/j;
.super Ljava/lang/Object;


# static fields
.field public static a:Lcom/fmm/ds/d/d;


# direct methods
.method public static a(Ljava/lang/Object;Lcom/fmm/ds/c/av;)I
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    check-cast p0, Lcom/fmm/ds/c/bi;

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    const-string v1, ""

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-boolean v2, v1, Lcom/fmm/ds/d/j;->a:Z

    if-eqz v2, :cond_0

    new-instance v2, Lcom/fmm/ds/d/u;

    invoke-direct {v2}, Lcom/fmm/ds/d/u;-><init>()V

    iput v5, v2, Lcom/fmm/ds/d/u;->a:I

    const-string v3, "./CallLog"

    iput-object v3, v2, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    iget-object v3, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-object v3, v3, Lcom/fmm/ds/d/j;->w:Ljava/lang/String;

    iput-object v3, v2, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    const-string v3, "text/x-calllog"

    iput-object v3, v2, Lcom/fmm/ds/d/u;->g:Ljava/lang/String;

    const-string v3, "text/x-calllog"

    iput-object v3, v2, Lcom/fmm/ds/d/u;->h:Ljava/lang/String;

    iget-object v1, v1, Lcom/fmm/ds/d/j;->B:Ljava/lang/String;

    iput-object v1, v2, Lcom/fmm/ds/d/u;->e:Ljava/lang/String;

    iput v5, v2, Lcom/fmm/ds/d/u;->i:I

    const/4 v1, -0x1

    iput v1, v2, Lcom/fmm/ds/d/u;->j:I

    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1, v5}, Lcom/fmm/ds/c/av;->a(I)V

    invoke-virtual {p1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/d/u;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    iget v1, v2, Lcom/fmm/ds/d/u;->a:I

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    const-string v3, "not support id"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_1
    if-eqz v1, :cond_1

    iget v3, v1, Lcom/fmm/ds/d/j;->c:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    iget-object v3, v2, Lcom/fmm/ds/d/u;->e:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput v6, v2, Lcom/fmm/ds/d/u;->b:I

    :goto_2
    iput v5, v1, Lcom/fmm/ds/d/j;->i:I

    iput v5, v1, Lcom/fmm/ds/d/j;->j:I

    iput v5, v1, Lcom/fmm/ds/d/j;->k:I

    iput v5, v1, Lcom/fmm/ds/d/j;->l:I

    iput v5, v1, Lcom/fmm/ds/d/j;->m:I

    iput v5, v1, Lcom/fmm/ds/d/j;->n:I

    iput v5, v1, Lcom/fmm/ds/d/j;->o:I

    iput v5, v1, Lcom/fmm/ds/d/j;->p:I

    iget-object v3, v1, Lcom/fmm/ds/d/j;->x:Ljava/lang/String;

    iput-object v3, v2, Lcom/fmm/ds/d/u;->x:Ljava/lang/String;

    iget-object v3, v1, Lcom/fmm/ds/d/j;->y:Ljava/lang/String;

    iput-object v3, v2, Lcom/fmm/ds/d/u;->y:Ljava/lang/String;

    iget-object v3, v1, Lcom/fmm/ds/d/j;->A:Ljava/lang/String;

    iput-object v3, v2, Lcom/fmm/ds/d/u;->z:Ljava/lang/String;

    iget v3, v1, Lcom/fmm/ds/d/j;->z:I

    iput v3, v2, Lcom/fmm/ds/d/u;->w:I

    iput v5, v1, Lcom/fmm/ds/d/j;->F:I

    :cond_1
    invoke-virtual {p1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fmm/ds/d/u;

    move-object v2, v1

    goto :goto_0

    :pswitch_0
    iget-object v1, v0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    goto :goto_1

    :cond_2
    iget v3, v1, Lcom/fmm/ds/d/j;->c:I

    iput v3, v2, Lcom/fmm/ds/d/u;->b:I

    goto :goto_2

    :cond_3
    return v6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static a()V
    .locals 3

    new-instance v0, Lcom/fmm/ds/d/d;

    invoke-direct {v0}, Lcom/fmm/ds/d/d;-><init>()V

    sput-object v0, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x32

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    iget-object v1, v1, Lcom/fmm/ds/d/d;->a:[Lcom/fmm/ds/c/x;

    new-instance v2, Lcom/fmm/ds/c/x;

    invoke-direct {v2}, Lcom/fmm/ds/c/x;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a(Lcom/fmm/ds/d/u;)V
    .locals 6

    const/4 v1, 0x0

    const-string v0, "xdsCacheMapItem"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    iget-wide v2, v0, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    invoke-virtual {v2, v1}, Lcom/fmm/ds/c/av;->a(I)V

    invoke-virtual {v2}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/x;

    invoke-static {}, Lcom/fmm/ds/b/j;->a()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v3, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    iget-object v3, v3, Lcom/fmm/ds/d/d;->a:[Lcom/fmm/ds/c/x;

    aget-object v3, v3, v1

    iget-object v4, v0, Lcom/fmm/ds/c/x;->b:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/ds/c/x;->b:Ljava/lang/String;

    sget-object v3, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    iget-object v3, v3, Lcom/fmm/ds/d/d;->a:[Lcom/fmm/ds/c/x;

    aget-object v3, v3, v1

    iget-object v0, v0, Lcom/fmm/ds/c/x;->a:Ljava/lang/String;

    iput-object v0, v3, Lcom/fmm/ds/c/x;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/x;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Lcom/fmm/ds/d/u;ILjava/lang/String;)V
    .locals 6

    const/16 v5, 0x9

    const/4 v4, 0x3

    const/4 v3, 0x2

    check-cast p0, Lcom/fmm/ds/d/h;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Lcom/fmm/ds/d/u;->a:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    const-string v1, "not support id"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_1
    if-nez v0, :cond_1

    const-string v0, "itemInfo is null!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/fmm/ds/c/au;->a()Ljava/lang/String;

    move-result-object v1

    iget v2, v0, Lcom/fmm/ds/d/j;->c:I

    iput v2, v0, Lcom/fmm/ds/d/j;->b:I

    if-ne p2, v3, :cond_5

    iput-object p3, v0, Lcom/fmm/ds/d/j;->B:Ljava/lang/String;

    const/16 v2, 0xc8

    iput v2, v0, Lcom/fmm/ds/d/j;->E:I

    :cond_2
    :goto_2
    iget v2, p1, Lcom/fmm/ds/d/u;->a:I

    if-nez v2, :cond_3

    if-ne p2, v3, :cond_3

    iput v4, v0, Lcom/fmm/ds/d/j;->c:I

    iput v4, p0, Lcom/fmm/ds/d/h;->f:I

    :cond_3
    invoke-static {}, Lcom/fmm/ds/a/a;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/fmm/ds/b/a;->o()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "[suspend&resume] xdsApplySyncResult"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iput v5, v0, Lcom/fmm/ds/d/j;->c:I

    iput v5, v0, Lcom/fmm/ds/d/j;->b:I

    iget v2, p1, Lcom/fmm/ds/d/u;->b:I

    iput v2, p0, Lcom/fmm/ds/d/h;->f:I

    iget-object v2, p1, Lcom/fmm/ds/d/u;->e:Ljava/lang/String;

    iput-object v2, v0, Lcom/fmm/ds/d/j;->B:Ljava/lang/String;

    :cond_4
    iput-object v1, v0, Lcom/fmm/ds/d/j;->C:Ljava/lang/String;

    iput p2, v0, Lcom/fmm/ds/d/j;->f:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/d/h;->a:I

    goto :goto_0

    :cond_5
    const/4 v2, 0x1

    if-ne p2, v2, :cond_2

    const/16 v2, 0x1f4

    iput v2, v0, Lcom/fmm/ds/d/j;->E:I

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Lcom/fmm/ds/d/u;)V
    .locals 3

    const/4 v0, 0x0

    const-string v1, "xdsCreateCachedMap"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    sget-object v1, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    iget-object v1, v1, Lcom/fmm/ds/d/d;->a:[Lcom/fmm/ds/c/x;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/fmm/ds/c/x;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    iget-object v1, v1, Lcom/fmm/ds/d/d;->a:[Lcom/fmm/ds/c/x;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/fmm/ds/c/x;->a:Ljava/lang/String;

    sget-object v2, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    iget-object v2, v2, Lcom/fmm/ds/d/d;->a:[Lcom/fmm/ds/c/x;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/fmm/ds/c/x;->b:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/d/u;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static b()Z
    .locals 1

    const-string v0, "[cachedmap]xdsSaveCachedMap"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/fmm/ds/b/j;->a:Lcom/fmm/ds/d/d;

    invoke-static {v0}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/d;)Z

    move-result v0

    goto :goto_0
.end method
