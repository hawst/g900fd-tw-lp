.class public Lcom/fmm/ds/b/n;
.super Ljava/lang/Object;


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const-string v0, "####### !!!XEVENT_NOT_DEFINED!!! #######"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "XEVENT_UI_SYNC_START"

    goto :goto_0

    :pswitch_2
    const-string v0, "XEVENT_UI_SERVER_CONNECT"

    goto :goto_0

    :pswitch_3
    const-string v0, "XEVENT_UI_SERVER_PROGRESS"

    goto :goto_0

    :pswitch_4
    const-string v0, "XEVENT_UI_PHONE_PROGRESS"

    goto :goto_0

    :pswitch_5
    const-string v0, "XEVENT_UI_SYNC_FINISH"

    goto :goto_0

    :pswitch_6
    const-string v0, "XEVENT_UI_CANNOT_START_NET_INIT"

    goto :goto_0

    :pswitch_7
    const-string v0, "XEVENT_UI_CANNOT_START_TP_INIT"

    goto :goto_0

    :pswitch_8
    const-string v0, "XEVENT_UI_SYNC_RESTART"

    goto :goto_0

    :pswitch_9
    const-string v0, "XEVENT_UI_SERVER_CONNECT_FAIL"

    goto :goto_0

    :pswitch_a
    const-string v0, "XEVENT_UI_SERVER_DNS_FAIL"

    goto :goto_0

    :pswitch_b
    const-string v0, "XEVENT_UI_NETWORK_BUSY"

    goto :goto_0

    :pswitch_c
    const-string v0, "XEVENT_UI_IN_PROGRESS"

    goto :goto_0

    :pswitch_d
    const-string v0, "XEVENT_UI_SERVER_BUSY"

    goto :goto_0

    :pswitch_e
    const-string v0, "XEVENT_UI_INVALID_CREDENTIALS"

    goto :goto_0

    :pswitch_f
    const-string v0, "XEVENT_UI_MISSING_CREDENTIALS"

    goto :goto_0

    :pswitch_10
    const-string v0, "XEVENT_UI_PAYMENT_REQIRED"

    goto :goto_0

    :pswitch_11
    const-string v0, "XEVENT_UI_FORBIDDEN_SESSION"

    goto :goto_0

    :pswitch_12
    const-string v0, "XEVENT_UI_SERVER_NOT_SUPPORTED"

    goto :goto_0

    :pswitch_13
    const-string v0, "XEVENT_UI_NETWORK_ERR"

    goto :goto_0

    :pswitch_14
    const-string v0, "XEVENT_UI_ABORT_BYUSER"

    goto :goto_0

    :pswitch_15
    const-string v0, "XEVENT_UI_RECV_FAIL"

    goto :goto_0

    :pswitch_16
    const-string v0, "XEVENT_UI_SEND_FAIL"

    goto :goto_0

    :pswitch_17
    const-string v0, "XEVENT_UI_HTTP_INTERNAL_ERROR"

    goto :goto_0

    :pswitch_18
    const-string v0, "XEVENT_UI_WBXML_ERROR"

    goto :goto_0

    :pswitch_19
    const-string v0, "XEVENT_UI_UNKNOWN_ERROR"

    goto :goto_0

    :pswitch_1a
    const-string v0, "XEVENT_UI_SYNCHRONIZATION_ERROR"

    goto :goto_0

    :pswitch_1b
    const-string v0, "XEVENT_UI_DEVICE_FULL_ERROR"

    goto :goto_0

    :pswitch_1c
    const-string v0, "XEVENT_UI_ONEWAYSYNC_NOTSUPPORTED"

    goto :goto_0

    :pswitch_1d
    const-string v0, "XEVENT_UI_DATASTORE_ERROR"

    goto :goto_0

    :pswitch_1e
    const-string v0, "XEVENT_UI_SYNC_START_ACTIVITY"

    goto :goto_0

    :pswitch_1f
    const-string v0, "XEVENT_UI_SYNC_CALLLOG_UPLOAD_COMPLETE"

    goto :goto_0

    :pswitch_20
    const-string v0, "XEVENT_UI_NOTI_NOT_SPECIFIED"

    goto :goto_0

    :pswitch_21
    const-string v0, "XEVENT_UI_NOTI_BACKGROUND"

    goto :goto_0

    :pswitch_22
    const-string v0, "XEVENT_UI_NOTI_INFORMATIVE"

    goto :goto_0

    :pswitch_23
    const-string v0, "XEVENT_UI_NOTI_INTERACTIVE"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
    .end packed-switch
.end method

.method public static a(Ljava/lang/Object;I)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "xdsSetEvent() %s(%d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/fmm/ds/b/n;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xdsGetIsStopping() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v0, 0x18

    if-eq p1, v0, :cond_0

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, v6, v6}, Lcom/fmm/ds/c/ba;->b(ILjava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    :goto_1
    return-void

    :sswitch_0
    const-string v0, "XEVENT_UI_SERVER_PROGRESS"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "XEVENT_UI_PHONE_PROGRESS"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    invoke-static {v5}, Lcom/fmm/ds/ui/a;->a(I)V

    goto :goto_1

    :sswitch_3
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/fmm/ds/ui/a;->a(I)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x32 -> :sswitch_2
        0x33 -> :sswitch_3
    .end sparse-switch
.end method
