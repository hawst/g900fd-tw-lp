.class public Lcom/fmm/ds/b/k;
.super Ljava/lang/Object;


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/fmm/ds/b/k;->a:[B

    const-string v0, "=\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/fmm/ds/b/k;->b:[B

    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/fmm/ds/b/k;->c:[B

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
    .end array-data
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)J
    .locals 11

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/16 v5, 0x50

    new-array v7, v5, [B

    :goto_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    if-lez v3, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v7, v0, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_0
    int-to-long v0, v2

    return-wide v0

    :cond_1
    const/16 v6, 0x4a

    if-le v3, v6, :cond_2

    const/4 v4, 0x0

    invoke-virtual {p1, v7, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    sget-object v3, Lcom/fmm/ds/b/k;->b:[B

    invoke-virtual {p1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    add-int/lit8 v2, v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    :cond_2
    int-to-byte v5, v5

    if-nez v5, :cond_3

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    move v4, v5

    goto :goto_0

    :cond_3
    if-lez v0, :cond_12

    const/4 v4, 0x1

    move v6, v4

    move v4, v3

    :goto_1
    if-gt v6, v0, :cond_5

    add-int/lit8 v3, v4, 0x1

    const/16 v8, 0x3d

    aput-byte v8, v7, v4

    add-int/lit8 v4, v3, 0x1

    sget-object v8, Lcom/fmm/ds/b/k;->c:[B

    const/4 v9, 0x0

    aget-byte v8, v8, v9

    aput-byte v8, v7, v3

    add-int/lit8 v3, v4, 0x1

    sget-object v8, Lcom/fmm/ds/b/k;->c:[B

    const/4 v9, 0x0

    aget-byte v8, v8, v9

    aput-byte v8, v7, v4

    add-int/lit8 v2, v2, 0x3

    const/16 v4, 0x4a

    if-le v3, v4, :cond_4

    const/4 v4, 0x0

    invoke-virtual {p1, v7, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    sget-object v3, Lcom/fmm/ds/b/k;->b:[B

    invoke-virtual {p1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    add-int/lit8 v2, v2, 0x3

    const/4 v3, 0x0

    :cond_4
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_2
    const/16 v6, 0x20

    if-le v5, v6, :cond_6

    const/16 v6, 0x7f

    if-ge v5, v6, :cond_6

    const/16 v6, 0x3d

    if-eq v5, v6, :cond_6

    const/16 v6, 0x28

    if-eq v5, v6, :cond_6

    const/16 v6, 0x29

    if-eq v5, v6, :cond_6

    const/16 v6, 0x3b

    if-eq v5, v6, :cond_6

    const/16 v6, 0x3a

    if-eq v5, v6, :cond_6

    add-int/lit8 v3, v4, 0x1

    aput-byte v5, v7, v4

    add-int/lit8 v2, v2, 0x1

    const/4 v1, 0x0

    move v4, v5

    goto/16 :goto_0

    :cond_6
    const/16 v6, 0x20

    if-eq v5, v6, :cond_7

    const/16 v6, 0x9

    if-ne v5, v6, :cond_8

    :cond_7
    add-int/lit8 v3, v4, 0x1

    aput-byte v5, v7, v4

    add-int/lit8 v2, v2, 0x1

    const/4 v1, 0x1

    move v4, v5

    goto/16 :goto_0

    :cond_8
    const/16 v6, 0xa

    if-ne v5, v6, :cond_9

    const/16 v6, 0xd

    if-ne v3, v6, :cond_9

    const/4 v3, 0x0

    move v10, v4

    move v4, v3

    move v3, v10

    goto/16 :goto_0

    :cond_9
    const/16 v6, 0xa

    if-ne v5, v6, :cond_c

    const/4 v5, 0x1

    if-eq v1, v5, :cond_a

    const/16 v1, 0x2e

    if-ne v3, v1, :cond_b

    const/4 v1, 0x1

    if-ne v4, v1, :cond_b

    :cond_a
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    add-int/lit8 v3, v1, 0x1

    const/16 v4, 0xd

    aput-byte v4, v7, v1

    add-int/lit8 v4, v3, 0x1

    const/16 v1, 0xa

    aput-byte v1, v7, v3

    add-int/lit8 v2, v2, 0x3

    :cond_b
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    add-int/lit8 v3, v1, 0x1

    const/16 v4, 0x30

    aput-byte v4, v7, v1

    add-int/lit8 v4, v3, 0x1

    const/16 v1, 0x41

    aput-byte v1, v7, v3

    const/4 v1, 0x0

    add-int/lit8 v2, v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {p1, v7, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    const/4 v4, 0x0

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_c
    const/16 v6, 0xd

    if-ne v5, v6, :cond_f

    const/4 v5, 0x1

    if-eq v1, v5, :cond_d

    const/16 v1, 0x2e

    if-ne v3, v1, :cond_e

    const/4 v1, 0x1

    if-ne v4, v1, :cond_e

    :cond_d
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    add-int/lit8 v3, v1, 0x1

    const/16 v4, 0xd

    aput-byte v4, v7, v1

    add-int/lit8 v4, v3, 0x1

    const/16 v1, 0xa

    aput-byte v1, v7, v3

    add-int/lit8 v2, v2, 0x3

    :cond_e
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0xd

    aput-byte v3, v7, v4

    add-int/lit8 v3, v1, 0x1

    const/16 v4, 0xa

    aput-byte v4, v7, v1

    const/4 v1, 0x0

    add-int/lit8 v2, v2, 0x2

    const/4 v4, 0x0

    invoke-virtual {p1, v7, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    const/4 v4, 0x0

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_f
    const/16 v1, 0x20

    if-lt v5, v1, :cond_10

    const/16 v1, 0x3d

    if-eq v5, v1, :cond_10

    const/16 v1, 0x7f

    if-ge v5, v1, :cond_10

    const/16 v1, 0x28

    if-eq v5, v1, :cond_10

    const/16 v1, 0x29

    if-eq v5, v1, :cond_10

    const/16 v1, 0x3b

    if-eq v5, v1, :cond_10

    const/16 v1, 0x3a

    if-ne v5, v1, :cond_11

    :cond_10
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    add-int/lit8 v4, v1, 0x1

    sget-object v3, Lcom/fmm/ds/b/k;->c:[B

    ushr-int/lit8 v6, v5, 0x4

    and-int/lit8 v6, v6, 0xf

    aget-byte v3, v3, v6

    aput-byte v3, v7, v1

    add-int/lit8 v3, v4, 0x1

    sget-object v1, Lcom/fmm/ds/b/k;->c:[B

    and-int/lit8 v6, v5, 0xf

    aget-byte v1, v1, v6

    aput-byte v1, v7, v4

    const/4 v1, 0x0

    add-int/lit8 v2, v2, 0x3

    move v4, v5

    goto/16 :goto_0

    :cond_11
    add-int/lit8 v3, v4, 0x1

    aput-byte v5, v7, v4

    const/4 v1, 0x0

    add-int/lit8 v2, v2, 0x1

    move v4, v5

    goto/16 :goto_0

    :cond_12
    move v10, v3

    move v3, v4

    move v4, v10

    goto/16 :goto_2
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-static {v0, v1}, Lcom/fmm/ds/b/k;->a(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
