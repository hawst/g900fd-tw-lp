.class public Lcom/fmm/ds/b/b;
.super Ljava/lang/Object;


# static fields
.field public static a:Lcom/fmm/ds/c/n;

.field public static b:Lcom/fmm/ds/c/ad;

.field public static c:Lcom/fmm/ds/c/av;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/ds/b/b;->a:Lcom/fmm/ds/c/n;

    sput-object v0, Lcom/fmm/ds/b/b;->b:Lcom/fmm/ds/c/ad;

    sput-object v0, Lcom/fmm/ds/b/b;->c:Lcom/fmm/ds/c/av;

    return-void
.end method

.method public static a(Lcom/fmm/ds/c/bi;)I
    .locals 2

    iget v0, p0, Lcom/fmm/ds/c/bi;->f:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/fmm/ds/c/bi;->f:I

    return v0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/u;)I
    .locals 11

    const-wide/16 v0, 0x0

    const/16 v10, 0x1400

    const/4 v4, 0x4

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/fmm/ds/c/bi;->W:Lcom/fmm/ds/c/bk;

    const-wide/16 v2, 0x1400

    iget-object v8, p0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    invoke-static {v8}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;)I

    move-result v8

    int-to-long v8, v8

    sub-long/2addr v2, v8

    const-wide/16 v8, 0x40

    sub-long/2addr v2, v8

    cmp-long v8, v2, v0

    if-gez v8, :cond_7

    :goto_0
    iget-boolean v2, v7, Lcom/fmm/ds/c/bk;->a:Z

    if-eqz v2, :cond_3

    iget v2, v7, Lcom/fmm/ds/c/bk;->d:I

    iget v3, v7, Lcom/fmm/ds/c/bk;->b:I

    sub-int/2addr v2, v3

    int-to-long v8, v2

    cmp-long v3, v8, v0

    if-gtz v3, :cond_0

    int-to-long v0, v2

    iput-boolean v6, v7, Lcom/fmm/ds/c/bk;->a:Z

    move v2, v4

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "retlo : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    new-array v5, v10, [C

    iput-object v5, v3, Lcom/fmm/ds/c/ab;->b:[C

    iget-object v3, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    long-to-int v5, v0

    iput v5, v3, Lcom/fmm/ds/c/ab;->c:I

    move v3, v6

    :goto_2
    int-to-long v8, v3

    cmp-long v5, v8, v0

    if-gez v5, :cond_1

    iget-object v5, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v5, v5, Lcom/fmm/ds/c/ab;->b:[C

    iget-object v8, v7, Lcom/fmm/ds/c/bk;->c:[C

    iget v9, v7, Lcom/fmm/ds/c/bk;->b:I

    add-int/2addr v9, v3

    aget-char v8, v8, v9

    aput-char v8, v5, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_0
    iput v5, p1, Lcom/fmm/ds/c/u;->g:I

    const/4 v2, 0x2

    goto :goto_1

    :cond_1
    iget v3, v7, Lcom/fmm/ds/c/bk;->b:I

    int-to-long v8, v3

    add-long/2addr v0, v8

    long-to-int v0, v0

    iput v0, v7, Lcom/fmm/ds/c/bk;->b:I

    if-ne v2, v4, :cond_2

    const/4 v0, 0x0

    iput-object v0, v7, Lcom/fmm/ds/c/bk;->c:[C

    iput v6, v7, Lcom/fmm/ds/c/bk;->b:I

    iput v6, v7, Lcom/fmm/ds/c/bk;->d:I

    :cond_2
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ret : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    return v2

    :cond_3
    iget-object v2, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v2, v2, Lcom/fmm/ds/c/ab;->c:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_6

    const-wide/16 v2, 0x400

    cmp-long v2, v0, v2

    if-gez v2, :cond_4

    const-string v0, "nFreesize < 1024 "

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x3

    goto :goto_3

    :cond_4
    const-string v2, "nFreesize >= 1024 "

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    long-to-int v2, v0

    iput v2, v7, Lcom/fmm/ds/c/bk;->b:I

    iget-object v2, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v2, v2, Lcom/fmm/ds/c/ab;->c:I

    iput v2, v7, Lcom/fmm/ds/c/bk;->d:I

    iput-boolean v5, v7, Lcom/fmm/ds/c/bk;->a:Z

    iget-object v2, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v2, v2, Lcom/fmm/ds/c/ab;->b:[C

    iput-object v2, v7, Lcom/fmm/ds/c/bk;->c:[C

    iget-object v2, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    new-array v3, v10, [C

    iput-object v3, v2, Lcom/fmm/ds/c/ab;->b:[C

    :goto_4
    int-to-long v2, v6

    cmp-long v2, v2, v0

    if-gez v2, :cond_5

    iget-object v2, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v2, v2, Lcom/fmm/ds/c/ab;->b:[C

    iget-object v3, v7, Lcom/fmm/ds/c/bk;->c:[C

    aget-char v3, v3, v6

    aput-char v3, v2, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_5
    iget-object v2, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    long-to-int v0, v0

    iput v0, v2, Lcom/fmm/ds/c/ab;->c:I

    iput v5, p1, Lcom/fmm/ds/c/u;->g:I

    move v2, v5

    goto :goto_3

    :cond_6
    const-string v0, "LARGE_OBJECT_NOT "

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v2, v6

    goto :goto_3

    :cond_7
    move-wide v0, v2

    goto/16 :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/b/e;)Lcom/fmm/ds/c/ae;
    .locals 5

    new-instance v0, Lcom/fmm/ds/c/ae;

    invoke-direct {v0}, Lcom/fmm/ds/c/ae;-><init>()V

    new-instance v1, Lcom/fmm/ds/c/z;

    invoke-direct {v1}, Lcom/fmm/ds/c/z;-><init>()V

    new-instance v2, Lcom/fmm/ds/c/u;

    invoke-direct {v2}, Lcom/fmm/ds/c/u;-><init>()V

    new-instance v3, Lcom/fmm/ds/c/ab;

    invoke-direct {v3}, Lcom/fmm/ds/c/ab;-><init>()V

    const-string v4, "build put cmd"

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v4

    iput v4, v0, Lcom/fmm/ds/c/ae;->a:I

    const-string v4, "application/vnd.syncml-devinf+wbxml"

    iput-object v4, v1, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/ae;->e:Lcom/fmm/ds/c/z;

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "./devinf11"

    iput-object v1, v2, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    :goto_0
    invoke-static {p1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/b/e;)Lcom/fmm/ds/c/as;

    move-result-object v1

    const/4 v4, 0x1

    iput v4, v3, Lcom/fmm/ds/c/ab;->a:I

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->e()I

    move-result v4

    iput v4, v3, Lcom/fmm/ds/c/ab;->c:I

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->g()[C

    move-result-object v1

    iput-object v1, v3, Lcom/fmm/ds/c/ab;->b:[C

    :cond_0
    iput-object v3, v2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    new-instance v1, Lcom/fmm/ds/c/ax;

    invoke-direct {v1, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/fmm/ds/c/ae;->f:Lcom/fmm/ds/c/ax;

    const-string v1, "Put"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    return-object v0

    :cond_1
    const-string v1, "./devinf12"

    iput-object v1, v2, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Lcom/fmm/ds/b/e;)Lcom/fmm/ds/c/ah;
    .locals 5

    new-instance v0, Lcom/fmm/ds/c/ah;

    invoke-direct {v0}, Lcom/fmm/ds/c/ah;-><init>()V

    new-instance v1, Lcom/fmm/ds/c/z;

    invoke-direct {v1}, Lcom/fmm/ds/c/z;-><init>()V

    new-instance v2, Lcom/fmm/ds/c/u;

    invoke-direct {v2}, Lcom/fmm/ds/c/u;-><init>()V

    new-instance v3, Lcom/fmm/ds/c/ab;

    invoke-direct {v3}, Lcom/fmm/ds/c/ab;-><init>()V

    const-string v4, "build result cmd."

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v4

    iput v4, v0, Lcom/fmm/ds/c/ah;->a:I

    iget-object v4, p0, Lcom/fmm/ds/c/bi;->C:Ljava/lang/String;

    iput-object v4, v0, Lcom/fmm/ds/c/ah;->b:Ljava/lang/String;

    iput-object p1, v0, Lcom/fmm/ds/c/ah;->c:Ljava/lang/String;

    const-string v4, "application/vnd.syncml-devinf+wbxml"

    iput-object v4, v1, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/ah;->d:Lcom/fmm/ds/c/z;

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "./devinf11"

    iput-object v1, v2, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    :goto_0
    invoke-static {p2}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/b/e;)Lcom/fmm/ds/c/as;

    move-result-object v1

    const/4 v4, 0x1

    iput v4, v3, Lcom/fmm/ds/c/ab;->a:I

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->e()I

    move-result v4

    iput v4, v3, Lcom/fmm/ds/c/ab;->c:I

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->g()[C

    move-result-object v1

    iput-object v1, v3, Lcom/fmm/ds/c/ab;->b:[C

    :cond_0
    iput-object v3, v2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    new-instance v1, Lcom/fmm/ds/c/ax;

    invoke-direct {v1, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/fmm/ds/c/ah;->g:Lcom/fmm/ds/c/ax;

    const-string v1, "Results"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    return-object v0

    :cond_1
    const-string v1, "./devinf12"

    iput-object v1, v2, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fmm/ds/c/ak;
    .locals 3

    new-instance v0, Lcom/fmm/ds/c/ak;

    invoke-direct {v0}, Lcom/fmm/ds/c/ak;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "build status cmd : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v1

    iput v1, v0, Lcom/fmm/ds/c/ak;->a:I

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->C:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/ak;->b:Ljava/lang/String;

    iput-object p1, v0, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    iput-object p2, v0, Lcom/fmm/ds/c/ak;->d:Ljava/lang/String;

    iput-object p3, v0, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/e;Lcom/fmm/ds/c/u;Lcom/fmm/ds/c/b;)Lcom/fmm/ds/c/b;
    .locals 2

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->V:Lcom/fmm/ds/c/bj;

    iget-object v1, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-eqz v1, :cond_5

    iget-object v0, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v0, v0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v0, v0, Lcom/fmm/ds/c/ab;->b:[C

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/String;

    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v1, v1, Lcom/fmm/ds/c/ab;->b:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    iput-object v0, p3, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    iget-object v0, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v0, v0, Lcom/fmm/ds/c/ab;->c:I

    iput v0, p3, Lcom/fmm/ds/c/b;->g:I

    :cond_1
    iget-object v0, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v0, v0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v0, v0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    :cond_2
    iget-object v0, p2, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    iget-object v0, v0, Lcom/fmm/ds/c/ap;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p2, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    iget-object v0, v0, Lcom/fmm/ds/c/ap;->a:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->c:Ljava/lang/String;

    :cond_3
    iget-object v0, p2, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    iget-object v0, v0, Lcom/fmm/ds/c/aj;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p2, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    iget-object v0, v0, Lcom/fmm/ds/c/aj;->a:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->d:Ljava/lang/String;

    :cond_4
    return-object p3

    :cond_5
    iget-object v1, p1, Lcom/fmm/ds/c/e;->d:Lcom/fmm/ds/c/z;

    if-eqz v1, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/e;->d:Lcom/fmm/ds/c/z;

    iget-object v0, v0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    goto :goto_0

    :cond_6
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/fmm/ds/c/bj;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/fmm/ds/c/bj;->d:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/bi;I)Lcom/fmm/ds/c/f;
    .locals 4

    new-instance v0, Lcom/fmm/ds/c/f;

    invoke-direct {v0}, Lcom/fmm/ds/c/f;-><init>()V

    const-string v1, "build alert suspend resume cmd"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v1

    iput v1, v0, Lcom/fmm/ds/c/f;->a:I

    packed-switch p1, :pswitch_data_0

    :goto_0
    new-instance v1, Lcom/fmm/ds/c/u;

    invoke-direct {v1}, Lcom/fmm/ds/c/u;-><init>()V

    new-instance v2, Lcom/fmm/ds/c/ao;

    invoke-direct {v2}, Lcom/fmm/ds/c/ao;-><init>()V

    iput-object v2, v1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v2, v1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v3, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    iput-object v3, v2, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->z:Ljava/lang/String;

    iput-object v2, v1, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    new-instance v2, Lcom/fmm/ds/c/ax;

    invoke-direct {v2, v1}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, Lcom/fmm/ds/c/f;->f:Lcom/fmm/ds/c/ax;

    const-string v1, "Alert"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    return-object v0

    :pswitch_0
    const-string v1, "224"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    const-string v1, "225"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/z;Ljava/lang/String;)Lcom/fmm/ds/c/f;
    .locals 4

    new-instance v0, Lcom/fmm/ds/c/u;

    invoke-direct {v0}, Lcom/fmm/ds/c/u;-><init>()V

    new-instance v1, Lcom/fmm/ds/c/f;

    invoke-direct {v1}, Lcom/fmm/ds/c/f;-><init>()V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v2

    iput v2, v1, Lcom/fmm/ds/c/f;->a:I

    iput-object p2, v1, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    new-instance v2, Lcom/fmm/ds/c/ao;

    invoke-direct {v2}, Lcom/fmm/ds/c/ao;-><init>()V

    iput-object v2, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v2, v0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v3, p0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget-object v3, v3, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    iput-object v3, v2, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget-object v2, v2, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    iput-object v2, v0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, v0, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    :cond_0
    new-instance v2, Lcom/fmm/ds/c/ax;

    invoke-direct {v2, v0}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, Lcom/fmm/ds/c/f;->f:Lcom/fmm/ds/c/ax;

    return-object v1
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/f;
    .locals 10

    const/4 v9, 0x1

    new-instance v0, Lcom/fmm/ds/c/f;

    invoke-direct {v0}, Lcom/fmm/ds/c/f;-><init>()V

    const-string v1, "build alert cmd."

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v1

    iput v1, v0, Lcom/fmm/ds/c/f;->a:I

    iget-boolean v1, p0, Lcom/fmm/ds/c/bi;->E:Z

    if-eqz v1, :cond_0

    const-string v1, "222"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/fmm/ds/c/bi;->E:Z

    const-string v1, "Alert"

    invoke-static {p0, v1, v9}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    :goto_0
    return-object v0

    :cond_0
    iget v1, p1, Lcom/fmm/ds/d/u;->b:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const-string v1, "201"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    :goto_1
    new-instance v1, Lcom/fmm/ds/c/u;

    invoke-direct {v1}, Lcom/fmm/ds/c/u;-><init>()V

    new-instance v2, Lcom/fmm/ds/c/z;

    invoke-direct {v2}, Lcom/fmm/ds/c/z;-><init>()V

    new-instance v3, Lcom/fmm/ds/c/aa;

    invoke-direct {v3}, Lcom/fmm/ds/c/aa;-><init>()V

    new-instance v4, Lcom/fmm/ds/c/ao;

    invoke-direct {v4}, Lcom/fmm/ds/c/ao;-><init>()V

    iput-object v4, v1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v4, v1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v5, p1, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    iput-object v5, v4, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    iput-object v4, v1, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    iget v4, p1, Lcom/fmm/ds/d/u;->b:I

    packed-switch v4, :pswitch_data_1

    :pswitch_1
    iget-object v4, p1, Lcom/fmm/ds/d/u;->e:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    iget-object v4, v3, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "00000000T000000Z"

    iput-object v4, v3, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    :cond_1
    :goto_2
    invoke-static {}, Lcom/fmm/ds/c/au;->b()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p1, Lcom/fmm/ds/d/u;->f:Ljava/lang/String;

    iget-object v4, p1, Lcom/fmm/ds/d/u;->f:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    iput-object v3, v2, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    iget-object v3, p1, Lcom/fmm/ds/d/u;->x:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p1, Lcom/fmm/ds/d/u;->y:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Lcom/fmm/ds/c/j;

    invoke-direct {v3}, Lcom/fmm/ds/c/j;-><init>()V

    new-instance v4, Lcom/fmm/ds/c/z;

    invoke-direct {v4}, Lcom/fmm/ds/c/z;-><init>()V

    iget v5, p0, Lcom/fmm/ds/c/bi;->m:I

    invoke-static {v5}, Lcom/fmm/ds/c/aq;->a(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    const-string v5, "b64"

    iput-object v5, v4, Lcom/fmm/ds/c/z;->b:Ljava/lang/String;

    iput-object v4, v3, Lcom/fmm/ds/c/j;->a:Lcom/fmm/ds/c/z;

    iget v4, p1, Lcom/fmm/ds/d/u;->w:I

    iget-object v5, p0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    iget-object v6, p1, Lcom/fmm/ds/d/u;->x:Ljava/lang/String;

    iget-object v7, p1, Lcom/fmm/ds/d/u;->y:Ljava/lang/String;

    iget-object v8, p1, Lcom/fmm/ds/d/u;->z:Ljava/lang/String;

    invoke-static {v4, v5, v6, v7, v8}, Lcom/fmm/ds/b/b;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/fmm/ds/c/j;->b:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/ds/c/f;->d:Lcom/fmm/ds/c/j;

    :goto_3
    iput-object v2, v1, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    new-instance v2, Lcom/fmm/ds/c/ax;

    invoke-direct {v2, v1}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, Lcom/fmm/ds/c/f;->f:Lcom/fmm/ds/c/ax;

    const-string v1, "Alert"

    invoke-static {p0, v1, v9}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    goto/16 :goto_0

    :pswitch_2
    const-string v1, "201"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_3
    const-string v1, "200"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_4
    const-string v1, "202"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_5
    const-string v1, "203"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_6
    const-string v1, "204"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_7
    const-string v1, "205"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_8
    const-string v1, "224"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_9
    const-string v1, "225"

    iput-object v1, v0, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_a
    const-string v4, "00000000T000000Z"

    iput-object v4, v3, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    goto/16 :goto_2

    :cond_2
    const/4 v3, 0x0

    iput-object v3, v0, Lcom/fmm/ds/c/f;->d:Lcom/fmm/ds/c/j;

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_0
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_a
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_a
        :pswitch_a
    .end packed-switch
.end method

.method public static a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const-string v0, ""

    packed-switch p0, :pswitch_data_0

    const-string v0, "Not Support Auth Type"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/c/ar;->a([B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "userName[%s] passWord[%s] creddata[%s]"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/fmm/ds/c/ay;

    invoke-direct {v0}, Lcom/fmm/ds/c/ay;-><init>()V

    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, p2, p3, v1}, Lcom/fmm/ds/c/ay;->a(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v1

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    const-string v1, "FIXME:MD5 Auth"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ag;Lcom/fmm/ds/c/u;Lcom/fmm/ds/c/b;)V
    .locals 2

    iget-object v0, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v0, v0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v0, v0, Lcom/fmm/ds/c/ab;->b:[C

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/String;

    iget-object v1, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v1, v1, Lcom/fmm/ds/c/ab;->b:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    iput-object v0, p3, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    iget-object v0, p2, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v0, v0, Lcom/fmm/ds/c/ab;->c:I

    iput v0, p3, Lcom/fmm/ds/c/b;->g:I

    :cond_1
    iget-object v0, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v0, v0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    :cond_2
    return-void

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/ag;->d:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/c/ag;->d:Lcom/fmm/ds/c/z;

    iget-object v0, v0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v0, p3, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;)V
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object v0, p1, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    iput-object v0, p1, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/ak;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    if-nez v0, :cond_1

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, p2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v0, p1, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    invoke-virtual {v0, p2}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0

    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    if-nez v0, :cond_3

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, p2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v0, p1, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    goto :goto_0

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    invoke-virtual {v0, p2}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;Lcom/fmm/ds/b/d;ILcom/fmm/ds/b/d;)Z
    .locals 14

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->L:Lcom/fmm/ds/c/d;

    iget-object v5, v1, Lcom/fmm/ds/c/d;->e:Lcom/fmm/ds/c/av;

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v1, Lcom/fmm/ds/d/h;

    iget-object v6, p0, Lcom/fmm/ds/c/bi;->W:Lcom/fmm/ds/c/bk;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ws.m_serverType : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v7, p0, Lcom/fmm/ds/c/bi;->H:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v7, Lcom/fmm/ds/c/u;

    invoke-direct {v7}, Lcom/fmm/ds/c/u;-><init>()V

    new-instance v8, Lcom/fmm/ds/c/z;

    invoke-direct {v8}, Lcom/fmm/ds/c/z;-><init>()V

    new-instance v9, Lcom/fmm/ds/c/b;

    invoke-direct {v9}, Lcom/fmm/ds/c/b;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ItemType : "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v4, 0x1

    move/from16 v0, p3

    if-eq v0, v4, :cond_0

    const/4 v4, 0x2

    move/from16 v0, p3

    if-ne v0, v4, :cond_1

    :cond_0
    iget-object v4, p1, Lcom/fmm/ds/d/u;->g:Ljava/lang/String;

    iput-object v4, v9, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    new-instance v4, Lcom/fmm/ds/c/ab;

    invoke-direct {v4}, Lcom/fmm/ds/c/ab;-><init>()V

    iput-object v4, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v4, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    const/4 v10, 0x0

    iput v10, v4, Lcom/fmm/ds/c/ab;->c:I

    iget-object v4, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    const/4 v10, 0x1

    iput v10, v4, Lcom/fmm/ds/c/ab;->a:I

    :cond_1
    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v4

    if-eqz v4, :cond_1c

    :cond_2
    :goto_0
    move-object/from16 v0, p2

    iget v4, v0, Lcom/fmm/ds/b/d;->a:I

    if-lez v4, :cond_3

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_3
    :goto_1
    move-object/from16 v0, p4

    iget v4, v0, Lcom/fmm/ds/b/d;->a:I

    add-int/2addr v4, v2

    move-object/from16 v0, p4

    iput v4, v0, Lcom/fmm/ds/b/d;->a:I

    const/4 v4, 0x1

    move/from16 v0, p3

    if-ne v0, v4, :cond_26

    iget v4, p1, Lcom/fmm/ds/d/u;->a:I

    packed-switch v4, :pswitch_data_0

    const-string v1, "not support id"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_4
    :goto_2
    const/4 v1, 0x1

    move/from16 v0, p3

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    move/from16 v0, p3

    if-ne v0, v1, :cond_6

    :cond_5
    if-eqz v7, :cond_6

    const/4 v1, 0x0

    iput-object v1, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    :cond_6
    if-eqz v7, :cond_28

    const/4 v1, 0x0

    iput-object v1, v7, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    :goto_3
    return v3

    :cond_7
    iget-object v4, p0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    invoke-static {v4}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;)I

    move-result v4

    add-int/lit8 v4, v4, 0x40

    add-int/lit16 v4, v4, 0x200

    int-to-long v10, v4

    const-wide/16 v12, 0x1400

    cmp-long v4, v10, v12

    if-ltz v4, :cond_8

    const/4 v3, 0x1

    goto :goto_1

    :cond_8
    invoke-static {}, Lcom/fmm/ds/a/a;->c()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0x32

    if-lt v2, v4, :cond_a

    const/4 v3, 0x1

    goto :goto_1

    :cond_9
    const/4 v4, 0x1

    if-ge v2, v4, :cond_3

    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[xdsBuildAndEncodeItemList]Count : "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    iget v10, v0, Lcom/fmm/ds/b/d;->a:I

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget v10, v0, Lcom/fmm/ds/b/d;->a:I

    if-lez v10, :cond_b

    move-object/from16 v0, p2

    iget v10, v0, Lcom/fmm/ds/b/d;->a:I

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, p2

    iput v10, v0, Lcom/fmm/ds/b/d;->a:I

    :cond_b
    iget-object v10, p0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v10, v10, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    iget v11, p0, Lcom/fmm/ds/c/bi;->H:I

    move/from16 v0, p3

    invoke-virtual {v10, p1, v0, v11}, Lcom/fmm/ds/b/q;->a(Lcom/fmm/ds/d/u;II)Lcom/fmm/ds/b/w;

    move-result-object v10

    if-nez v10, :cond_c

    const-string v4, "nextItem is null"

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    iget v11, v10, Lcom/fmm/ds/b/w;->a:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v9, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    iget-object v11, v9, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    iput-object v11, v7, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    const/4 v11, 0x1

    move/from16 v0, p3

    if-eq v0, v11, :cond_d

    const/4 v11, 0x2

    move/from16 v0, p3

    if-ne v0, v11, :cond_12

    :cond_d
    iput-object p1, p0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    if-eqz v8, :cond_e

    iget-object v4, v9, Lcom/fmm/ds/c/b;->e:Ljava/lang/String;

    iput-object v4, v8, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v8, v7, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    :cond_e
    iget-object v4, p0, Lcom/fmm/ds/c/bi;->W:Lcom/fmm/ds/c/bk;

    iget-boolean v4, v4, Lcom/fmm/ds/c/bk;->a:Z

    if-nez v4, :cond_10

    iget-object v4, p0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v4, v4, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-virtual {v4, p0, v9, v10}, Lcom/fmm/ds/b/q;->a(Ljava/lang/Object;Lcom/fmm/ds/c/b;Lcom/fmm/ds/b/w;)I

    iget v4, v9, Lcom/fmm/ds/c/b;->g:I

    if-nez v4, :cond_f

    const-string v4, " xdsBuildAndEncodeItemList() return no data"

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    iget-object v4, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v10, v9, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    iput-object v10, v4, Lcom/fmm/ds/c/ab;->b:[C

    iget-object v4, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v10, v9, Lcom/fmm/ds/c/b;->g:I

    iput v10, v4, Lcom/fmm/ds/c/ab;->c:I

    :cond_10
    invoke-static {p0, v7}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/u;)I

    move-result v4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "retlo : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v10, 0x1

    if-ne v4, v10, :cond_17

    const-string v10, "retlo == LARGE_OBJECT_START"

    invoke-static {v10}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v10, v6, Lcom/fmm/ds/c/bk;->d:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    iget-object v11, v7, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-nez v11, :cond_11

    if-eqz v8, :cond_16

    iput-object v8, v7, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    :cond_11
    :goto_4
    iget-object v11, v7, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iput-object v10, v11, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    :cond_12
    const/4 v10, 0x1

    if-eq v4, v10, :cond_13

    const/4 v10, 0x2

    if-ne v4, v10, :cond_14

    :cond_13
    move-object/from16 v0, p2

    iget v3, v0, Lcom/fmm/ds/b/d;->a:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/ds/b/d;->a:I

    iget-object v3, p0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move/from16 v0, p3

    invoke-virtual {v3, p1, v0}, Lcom/fmm/ds/b/q;->a(Lcom/fmm/ds/d/u;I)Lcom/fmm/ds/b/w;

    const/4 v3, 0x1

    :cond_14
    iget-object v10, p0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    invoke-virtual {v10, v7}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/u;)I

    const/4 v10, 0x1

    if-eq v4, v10, :cond_15

    const/4 v10, 0x2

    if-ne v4, v10, :cond_18

    :cond_15
    const-string v4, "break while. retlo == LARGE_OBJECT_START or LARGE_OBJECT_CONTINUE"

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_16
    new-instance v11, Lcom/fmm/ds/c/z;

    invoke-direct {v11}, Lcom/fmm/ds/c/z;-><init>()V

    iput-object v11, v7, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    goto :goto_4

    :cond_17
    const/4 v10, 0x3

    if-ne v4, v10, :cond_12

    const-string v3, "retlo == LARGE_OBJECT_NEXT_MESSAGE"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget v3, v0, Lcom/fmm/ds/b/d;->a:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/ds/b/d;->a:I

    iget-object v3, p0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move/from16 v0, p3

    invoke-virtual {v3, p1, v0}, Lcom/fmm/ds/b/q;->a(Lcom/fmm/ds/d/u;I)Lcom/fmm/ds/b/w;

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_18
    const/4 v10, 0x4

    if-eq v4, v10, :cond_19

    if-nez v4, :cond_2

    :cond_19
    iget-object v4, v9, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1a
    iget v6, v4, Lcom/fmm/ds/b/w;->a:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v9, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    if-eqz v7, :cond_21

    iget-object v6, v9, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    iput-object v6, v7, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    :goto_5
    const/4 v6, 0x1

    move/from16 v0, p3

    if-eq v0, v6, :cond_1b

    const/4 v6, 0x2

    move/from16 v0, p3

    if-ne v0, v6, :cond_23

    :cond_1b
    iput-object p1, p0, Lcom/fmm/ds/c/bi;->G:Lcom/fmm/ds/d/u;

    iget-object v6, p0, Lcom/fmm/ds/c/bi;->W:Lcom/fmm/ds/c/bk;

    iget-boolean v6, v6, Lcom/fmm/ds/c/bk;->a:Z

    if-nez v6, :cond_23

    iget-object v6, p0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v6, v6, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    invoke-virtual {v6, p0, v9, v4}, Lcom/fmm/ds/b/q;->a(Ljava/lang/Object;Lcom/fmm/ds/c/b;Lcom/fmm/ds/b/w;)I

    iget v4, v9, Lcom/fmm/ds/c/b;->g:I

    if-nez v4, :cond_22

    :cond_1c
    :goto_6
    move-object/from16 v0, p2

    iget v4, v0, Lcom/fmm/ds/b/d;->a:I

    if-lez v4, :cond_3

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    invoke-static {v4}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;)I

    move-result v4

    add-int/lit8 v4, v4, 0x40

    add-int/lit16 v4, v4, 0x200

    int-to-long v10, v4

    const-wide/16 v12, 0x1400

    cmp-long v4, v10, v12

    if-ltz v4, :cond_1d

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_1d
    invoke-static {}, Lcom/fmm/ds/a/a;->c()Z

    move-result v4

    if-eqz v4, :cond_1e

    const/16 v4, 0x32

    if-lt v2, v4, :cond_1f

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_1e
    const/4 v4, 0x1

    if-ge v2, v4, :cond_3

    :cond_1f
    move-object/from16 v0, p2

    iget v4, v0, Lcom/fmm/ds/b/d;->a:I

    if-lez v4, :cond_20

    move-object/from16 v0, p2

    iget v4, v0, Lcom/fmm/ds/b/d;->a:I

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p2

    iput v4, v0, Lcom/fmm/ds/b/d;->a:I

    :cond_20
    iget-object v4, p0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v4, v4, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    iget v6, p0, Lcom/fmm/ds/c/bi;->H:I

    move/from16 v0, p3

    invoke-virtual {v4, p1, v0, v6}, Lcom/fmm/ds/b/q;->a(Lcom/fmm/ds/d/u;II)Lcom/fmm/ds/b/w;

    move-result-object v4

    if-nez v4, :cond_1a

    goto/16 :goto_1

    :cond_21
    const-string v6, "xds_item is null!!"

    invoke-static {v6}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_5

    :cond_22
    if-eqz v7, :cond_24

    iget-object v4, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    if-eqz v4, :cond_24

    iget-object v4, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v6, v9, Lcom/fmm/ds/c/b;->f:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    iput-object v6, v4, Lcom/fmm/ds/c/ab;->b:[C

    iget-object v4, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v6, v9, Lcom/fmm/ds/c/b;->g:I

    iput v6, v4, Lcom/fmm/ds/c/ab;->c:I

    :cond_23
    :goto_7
    const/4 v4, 0x4

    move/from16 v0, p3

    if-eq v0, v4, :cond_25

    if-eqz v7, :cond_25

    iget-object v4, p0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    invoke-static {v4}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;)I

    move-result v4

    add-int/lit8 v4, v4, 0x40

    iget-object v6, v7, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v6, v6, Lcom/fmm/ds/c/ab;->c:I

    add-int/2addr v4, v6

    int-to-long v10, v4

    const-wide/16 v12, 0x1400

    cmp-long v4, v10, v12

    if-ltz v4, :cond_25

    move-object/from16 v0, p2

    iget v3, v0, Lcom/fmm/ds/b/d;->a:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p2

    iput v3, v0, Lcom/fmm/ds/b/d;->a:I

    iget-object v3, p0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    iget-object v3, v3, Lcom/fmm/ds/b/a;->b:Lcom/fmm/ds/b/q;

    move/from16 v0, p3

    invoke-virtual {v3, p1, v0}, Lcom/fmm/ds/b/q;->a(Lcom/fmm/ds/d/u;I)Lcom/fmm/ds/b/w;

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_24
    const-string v4, "xds_item or xds_item.data is null!!"

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_7

    :cond_25
    iget-object v4, p0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    invoke-virtual {v4, v7}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/u;)I

    iget-object v4, v9, Lcom/fmm/ds/c/b;->b:Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6

    :pswitch_0
    iget-object v4, v1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-object v5, v1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v5, v5, Lcom/fmm/ds/d/j;->m:I

    add-int/2addr v2, v5

    iput v2, v4, Lcom/fmm/ds/d/j;->m:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CallLog_SyncInfo.P2SAddItem : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v1, v1, Lcom/fmm/ds/d/j;->m:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_26
    const/4 v1, 0x2

    move/from16 v0, p3

    if-ne v0, v1, :cond_27

    const-string v1, "not support id"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_27
    const/4 v1, 0x4

    move/from16 v0, p3

    if-ne v0, v1, :cond_4

    const-string v1, "not support id"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_28
    const-string v1, "xds_item is null!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/fmm/ds/c/n;)Z
    .locals 1

    sput-object p0, Lcom/fmm/ds/b/b;->a:Lcom/fmm/ds/c/n;

    const/4 v0, 0x1

    return v0
.end method

.method public static b(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/al;
    .locals 4

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/fmm/ds/d/u;->v:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const-string v0, "build sync cmd"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v2, Lcom/fmm/ds/c/al;

    invoke-direct {v2}, Lcom/fmm/ds/c/al;-><init>()V

    new-instance v0, Lcom/fmm/ds/c/ao;

    invoke-direct {v0}, Lcom/fmm/ds/c/ao;-><init>()V

    iput-object v0, v2, Lcom/fmm/ds/c/al;->e:Lcom/fmm/ds/c/ao;

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v0

    iput v0, v2, Lcom/fmm/ds/c/al;->a:I

    iget-object v0, v2, Lcom/fmm/ds/c/al;->e:Lcom/fmm/ds/c/ao;

    iget-object v3, p1, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iget-object v0, p1, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    iput-object v0, v2, Lcom/fmm/ds/c/al;->f:Ljava/lang/String;

    if-eqz p1, :cond_2

    iget v0, p1, Lcom/fmm/ds/d/u;->p:I

    iget v3, p1, Lcom/fmm/ds/d/u;->q:I

    add-int/2addr v0, v3

    iget v3, p1, Lcom/fmm/ds/d/u;->r:I

    add-int/2addr v0, v3

    :goto_1
    iput v0, v2, Lcom/fmm/ds/c/al;->g:I

    const-string v0, "Sync"

    invoke-static {p0, v0, v1}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    move-object v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static b(Lcom/fmm/ds/c/bi;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->y:Ljava/lang/String;

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v4, :cond_0

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-eq v1, v4, :cond_2

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v4, :cond_1

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/ds/c/bi;->x:Ljava/lang/String;

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/fmm/ds/c/bi;->l:I

    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->y:Ljava/lang/String;

    :goto_0
    return-void

    :cond_2
    iput-object v0, p0, Lcom/fmm/ds/c/bi;->x:Ljava/lang/String;

    goto :goto_0
.end method

.method public static c(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/e;
    .locals 3

    iget v0, p1, Lcom/fmm/ds/d/u;->p:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "build add cmd"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/fmm/ds/c/e;

    invoke-direct {v0}, Lcom/fmm/ds/c/e;-><init>()V

    new-instance v1, Lcom/fmm/ds/c/z;

    invoke-direct {v1}, Lcom/fmm/ds/c/z;-><init>()V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v2

    iput v2, v0, Lcom/fmm/ds/c/e;->a:I

    iget-object v2, p1, Lcom/fmm/ds/d/u;->g:Ljava/lang/String;

    iput-object v2, v1, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/e;->d:Lcom/fmm/ds/c/z;

    const-string v1, "Add"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "WARNING!!! xdsCreateAction Failed"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iput-object p1, v1, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    goto :goto_0
.end method

.method public static c(Lcom/fmm/ds/c/bi;)V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v0, "SyncHdr"

    invoke-static {p0, v0, v6}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    move-result-object v0

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/fmm/ds/c/d;->b:J

    const-string v0, "build synchdr cmd"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->D:Lcom/fmm/ds/c/an;

    if-nez v0, :cond_8

    new-instance v0, Lcom/fmm/ds/c/an;

    invoke-direct {v0}, Lcom/fmm/ds/c/an;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->D:Lcom/fmm/ds/c/an;

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->D:Lcom/fmm/ds/c/an;

    const-string v1, ""

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    const-string v2, "wap"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "http"

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    iget v1, p0, Lcom/fmm/ds/c/bi;->l:I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/fmm/ds/c/bi;->l:I

    const/16 v2, 0x50

    if-eq v1, v2, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, ":%d"

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lcom/fmm/ds/c/bi;->l:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/fmm/ds/c/bi;->y:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    :cond_2
    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "1.1"

    iput-object v1, v0, Lcom/fmm/ds/c/an;->a:Ljava/lang/String;

    const-string v1, "SyncML/1.1"

    iput-object v1, v0, Lcom/fmm/ds/c/an;->b:Ljava/lang/String;

    :goto_2
    iget-object v1, p0, Lcom/fmm/ds/c/bi;->B:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/an;->c:Ljava/lang/String;

    iget-wide v1, p0, Lcom/fmm/ds/c/bi;->e:J

    long-to-int v1, v1

    iput v1, v0, Lcom/fmm/ds/c/an;->d:I

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->z:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/an;->f:Ljava/lang/String;

    new-instance v1, Lcom/fmm/ds/c/ao;

    invoke-direct {v1}, Lcom/fmm/ds/c/ao;-><init>()V

    iput-object v1, v0, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    iget-object v1, v0, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    iput-object v2, v1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->t:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/an;->g:Ljava/lang/String;

    new-instance v1, Lcom/fmm/ds/c/z;

    invoke-direct {v1}, Lcom/fmm/ds/c/z;-><init>()V

    iget-wide v2, p0, Lcom/fmm/ds/c/bi;->g:J

    long-to-int v2, v2

    iput v2, v1, Lcom/fmm/ds/c/z;->h:I

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/fmm/ds/c/bi;->h:J

    iput-wide v2, v1, Lcom/fmm/ds/c/z;->i:J

    :cond_3
    iput-object v1, v0, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    move-object v1, v0

    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "targetURI : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget v0, p0, Lcom/fmm/ds/c/bi;->i:I

    if-eq v0, v7, :cond_c

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_b

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_b

    new-instance v2, Lcom/fmm/ds/c/j;

    invoke-direct {v2}, Lcom/fmm/ds/c/j;-><init>()V

    new-instance v0, Lcom/fmm/ds/c/z;

    invoke-direct {v0}, Lcom/fmm/ds/c/z;-><init>()V

    iget v3, p0, Lcom/fmm/ds/c/bi;->m:I

    invoke-static {v3}, Lcom/fmm/ds/c/aq;->a(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    const-string v3, "b64"

    iput-object v3, v0, Lcom/fmm/ds/c/z;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/fmm/ds/c/j;->a:Lcom/fmm/ds/c/z;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "userID[%s] password[%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/fmm/ds/c/bi;->t:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/fmm/ds/c/bi;->u:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-static {v0, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    iget-object v0, v0, Lcom/fmm/ds/d/h;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nonce : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    iget-object v0, v0, Lcom/fmm/ds/d/h;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :cond_4
    iget v3, p0, Lcom/fmm/ds/c/bi;->m:I

    iget-object v4, p0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    iget-object v5, p0, Lcom/fmm/ds/c/bi;->t:Ljava/lang/String;

    iget-object v6, p0, Lcom/fmm/ds/c/bi;->u:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/bi;->o:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/d/h;

    iget-object v0, v0, Lcom/fmm/ds/d/h;->d:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6, v0}, Lcom/fmm/ds/b/b;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/fmm/ds/c/j;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    :goto_4
    return-void

    :cond_5
    iget-object v1, p0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/fmm/ds/c/bi;->l:I

    const/16 v2, 0x1bb

    if-eq v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, ":%d"

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lcom/fmm/ds/c/bi;->l:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    const-string v1, "1.2"

    iput-object v1, v0, Lcom/fmm/ds/c/an;->a:Ljava/lang/String;

    const-string v1, "SyncML/1.2"

    iput-object v1, v0, Lcom/fmm/ds/c/an;->b:Ljava/lang/String;

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/fmm/ds/c/bi;->D:Lcom/fmm/ds/c/an;

    iget-wide v1, p0, Lcom/fmm/ds/c/bi;->e:J

    long-to-int v1, v1

    iput v1, v0, Lcom/fmm/ds/c/an;->d:I

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, ""

    iput-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    :cond_9
    iget-object v1, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    iget-object v2, v0, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    iget-object v2, v2, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    iget-object v2, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    iput-object v2, v1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {p0}, Lcom/fmm/ds/b/b;->b(Lcom/fmm/ds/c/bi;)V

    :cond_a
    move-object v1, v0

    goto/16 :goto_3

    :cond_b
    iput-object v8, v1, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    goto :goto_4

    :cond_c
    iput-object v8, v1, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    goto :goto_4
.end method

.method public static d(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/ag;
    .locals 4

    const/4 v3, 0x1

    iget v0, p1, Lcom/fmm/ds/d/u;->q:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "build replace cmd"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/fmm/ds/c/ag;

    invoke-direct {v0}, Lcom/fmm/ds/c/ag;-><init>()V

    new-instance v1, Lcom/fmm/ds/c/z;

    invoke-direct {v1}, Lcom/fmm/ds/c/z;-><init>()V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v2

    iput v2, v0, Lcom/fmm/ds/c/ag;->a:I

    invoke-static {}, Lcom/fmm/ds/a/a;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/fmm/ds/a/a;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    iput-boolean v3, v1, Lcom/fmm/ds/c/z;->g:Z

    :cond_1
    iget-object v2, p1, Lcom/fmm/ds/d/u;->g:Ljava/lang/String;

    iput-object v2, v1, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/ag;->d:Lcom/fmm/ds/c/z;

    const-string v1, "Replace"

    invoke-static {p0, v1, v3}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    move-result-object v1

    iput-object p1, v1, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    goto :goto_0
.end method

.method public static d(Lcom/fmm/ds/c/bi;)Lcom/fmm/ds/c/t;
    .locals 4

    new-instance v0, Lcom/fmm/ds/c/t;

    invoke-direct {v0}, Lcom/fmm/ds/c/t;-><init>()V

    new-instance v1, Lcom/fmm/ds/c/z;

    invoke-direct {v1}, Lcom/fmm/ds/c/z;-><init>()V

    new-instance v2, Lcom/fmm/ds/c/u;

    invoke-direct {v2}, Lcom/fmm/ds/c/u;-><init>()V

    const-string v3, "build get cmd"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v3

    iput v3, v0, Lcom/fmm/ds/c/t;->a:I

    const-string v3, "application/vnd.syncml-devinf+wbxml"

    iput-object v3, v1, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/t;->e:Lcom/fmm/ds/c/z;

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    const-string v3, "./devinf11"

    iput-object v3, v1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    :goto_0
    new-instance v1, Lcom/fmm/ds/c/ax;

    invoke-direct {v1, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/fmm/ds/c/t;->f:Lcom/fmm/ds/c/ax;

    const-string v1, "Get"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    return-object v0

    :cond_0
    iget-object v1, v2, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    const-string v3, "./devinf12"

    iput-object v3, v1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static e(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/m;
    .locals 3

    iget v0, p1, Lcom/fmm/ds/d/u;->r:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "build delete cmd"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/fmm/ds/c/m;

    invoke-direct {v0}, Lcom/fmm/ds/c/m;-><init>()V

    new-instance v1, Lcom/fmm/ds/c/z;

    invoke-direct {v1}, Lcom/fmm/ds/c/z;-><init>()V

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v2

    iput v2, v0, Lcom/fmm/ds/c/m;->a:I

    iget-object v2, p1, Lcom/fmm/ds/d/u;->g:Ljava/lang/String;

    iput-object v2, v1, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/m;->f:Lcom/fmm/ds/c/z;

    const-string v1, "Delete"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    move-result-object v1

    iput-object p1, v1, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    goto :goto_0
.end method

.method public static f(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Lcom/fmm/ds/c/v;
    .locals 4

    iget-object v0, p1, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    iget-wide v0, v0, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "build map cmd"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/fmm/ds/c/v;

    invoke-direct {v0}, Lcom/fmm/ds/c/v;-><init>()V

    new-instance v1, Lcom/fmm/ds/c/ao;

    invoke-direct {v1}, Lcom/fmm/ds/c/ao;-><init>()V

    iput-object v1, v0, Lcom/fmm/ds/c/v;->b:Lcom/fmm/ds/c/ao;

    invoke-static {p0}, Lcom/fmm/ds/b/b;->a(Lcom/fmm/ds/c/bi;)I

    move-result v1

    iput v1, v0, Lcom/fmm/ds/c/v;->a:I

    iget-object v1, v0, Lcom/fmm/ds/c/v;->b:Lcom/fmm/ds/c/ao;

    iget-object v2, p1, Lcom/fmm/ds/d/u;->d:Ljava/lang/String;

    iput-object v2, v1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/fmm/ds/d/u;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/c/v;->c:Ljava/lang/String;

    const-string v1, "Map"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/fmm/ds/c/bi;->a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;

    goto :goto_0
.end method

.method public static g(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/d/u;)Z
    .locals 7

    const-wide/16 v5, 0x0

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->b:Lcom/fmm/ds/c/at;

    new-instance v2, Lcom/fmm/ds/c/w;

    invoke-direct {v2}, Lcom/fmm/ds/c/w;-><init>()V

    new-instance v0, Lcom/fmm/ds/c/ao;

    invoke-direct {v0}, Lcom/fmm/ds/c/ao;-><init>()V

    iput-object v0, v2, Lcom/fmm/ds/c/w;->a:Lcom/fmm/ds/c/ao;

    iget-object v3, p1, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    invoke-virtual {v3, v5, v6}, Lcom/fmm/ds/c/av;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/x;

    :goto_0
    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/fmm/ds/c/x;->a:Ljava/lang/String;

    iput-object v4, v2, Lcom/fmm/ds/c/w;->b:Ljava/lang/String;

    iget-object v4, v2, Lcom/fmm/ds/c/w;->a:Lcom/fmm/ds/c/ao;

    iget-object v0, v0, Lcom/fmm/ds/c/x;->b:Ljava/lang/String;

    iput-object v0, v4, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/w;)I

    invoke-virtual {v3, v5, v6}, Lcom/fmm/ds/c/av;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/x;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
