.class Lcom/fmm/ds/b/t;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/Vector;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    return-void
.end method

.method synthetic constructor <init>(Lcom/fmm/ds/b/r;)V
    .locals 0

    invoke-direct {p0}, Lcom/fmm/ds/b/t;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/fmm/ds/b/t;)Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public a(I)I
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/s;

    iget v0, v0, Lcom/fmm/ds/b/s;->a:I

    if-ne v0, p1, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public a(IJI)Z
    .locals 2

    new-instance v0, Lcom/fmm/ds/b/s;

    invoke-direct {v0}, Lcom/fmm/ds/b/s;-><init>()V

    iput p1, v0, Lcom/fmm/ds/b/s;->a:I

    iput-wide p2, v0, Lcom/fmm/ds/b/s;->b:J

    iput p4, v0, Lcom/fmm/ds/b/s;->c:I

    iget-object v1, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public b(I)Lcom/fmm/ds/b/s;
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/s;

    return-object v0
.end method

.method b()V
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void
.end method

.method public b(IJI)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/t;->c(I)Lcom/fmm/ds/b/s;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-wide p2, v0, Lcom/fmm/ds/b/s;->b:J

    iput p4, v0, Lcom/fmm/ds/b/s;->c:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(I)Lcom/fmm/ds/b/s;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/b/s;

    iget v2, v0, Lcom/fmm/ds/b/s;->a:I

    if-ne v2, p1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d(I)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/fmm/ds/b/t;->a(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/b/t;->a:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
