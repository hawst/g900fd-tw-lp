.class public Lcom/fmm/ds/b/a/a;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/fmm/ds/b/a/c;


# direct methods
.method public constructor <init>(Lcom/fmm/ds/b/a/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    return-void
.end method

.method public static a(Lcom/fmm/ds/b/a/a;)Lcom/fmm/ds/b/a/e;
    .locals 7

    new-instance v0, Lcom/fmm/ds/b/a/e;

    invoke-direct {v0}, Lcom/fmm/ds/b/a/e;-><init>()V

    iget-object v1, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    iget v1, v1, Lcom/fmm/ds/b/a/c;->a:I

    iput v1, v0, Lcom/fmm/ds/b/a/e;->a:I

    iget-object v1, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    iget-object v1, v1, Lcom/fmm/ds/b/a/c;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/b/a/e;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    iget-wide v1, v1, Lcom/fmm/ds/b/a/c;->c:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iget-object v1, v0, Lcom/fmm/ds/b/a/e;->c:Landroid/text/format/Time;

    if-nez v1, :cond_0

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput-object v1, v0, Lcom/fmm/ds/b/a/e;->c:Landroid/text/format/Time;

    :cond_0
    iget-object v1, v0, Lcom/fmm/ds/b/a/e;->c:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    iget-object v2, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    iget-wide v2, v2, Lcom/fmm/ds/b/a/c;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    iget-object v2, v0, Lcom/fmm/ds/b/a/e;->c:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    iget-wide v3, v3, Lcom/fmm/ds/b/a/c;->c:J

    int-to-long v5, v1

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Landroid/text/format/Time;->set(J)V

    :cond_1
    iget-object v1, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    iget v1, v1, Lcom/fmm/ds/b/a/c;->d:I

    iput v1, v0, Lcom/fmm/ds/b/a/e;->d:I

    iget-object v1, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    iget-object v1, v1, Lcom/fmm/ds/b/a/c;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/fmm/ds/b/a/e;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/fmm/ds/b/a/a;->a:Lcom/fmm/ds/b/a/c;

    iget v1, v1, Lcom/fmm/ds/b/a/c;->f:I

    iput v1, v0, Lcom/fmm/ds/b/a/e;->f:I

    return-object v0
.end method

.method public static b(Lcom/fmm/ds/b/a/a;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/fmm/ds/b/a/a;->a(Lcom/fmm/ds/b/a/a;)Lcom/fmm/ds/b/a/e;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/a/d;->a(Lcom/fmm/ds/b/a/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
