.class public Lcom/fmm/ds/e/c;
.super Ljava/lang/Object;


# static fields
.field public static a:[Lcom/fmm/ds/e/a;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v3, [Lcom/fmm/ds/e/a;

    sput-object v0, Lcom/fmm/ds/e/c;->a:[Lcom/fmm/ds/e/a;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    sget-object v1, Lcom/fmm/ds/e/c;->a:[Lcom/fmm/ds/e/a;

    new-instance v2, Lcom/fmm/ds/e/a;

    invoke-direct {v2}, Lcom/fmm/ds/e/a;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a([BI)I
    .locals 2

    const/4 v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    aget-byte v0, p0, v0

    add-int/lit8 v0, v0, 0x3

    if-ge v0, p1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/e/d;)V
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object v0, p0, Lcom/fmm/ds/e/d;->d:[B

    iput-object v0, p0, Lcom/fmm/ds/e/d;->e:[B

    iput-object v0, p0, Lcom/fmm/ds/e/d;->f:[B

    goto :goto_0
.end method

.method public static a([BILcom/fmm/ds/e/a;)V
    .locals 8

    const/4 v0, 0x0

    new-instance v1, Lcom/fmm/ds/e/f;

    invoke-direct {v1}, Lcom/fmm/ds/e/f;-><init>()V

    const/4 v2, 0x5

    if-eqz p0, :cond_0

    iget-object v3, p2, Lcom/fmm/ds/e/a;->g:Lcom/fmm/ds/e/f;

    aget-byte v1, p0, v0

    shr-int/lit8 v1, v1, 0x4

    iput v1, v3, Lcom/fmm/ds/e/f;->a:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NotiBody.number_Of_Syncs="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v3, Lcom/fmm/ds/e/f;->a:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v1, v3, Lcom/fmm/ds/e/f;->a:I

    new-array v1, v1, [Lcom/fmm/ds/e/e;

    iput-object v1, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    move v1, v0

    :goto_0
    iget v4, v3, Lcom/fmm/ds/e/f;->a:I

    if-ge v1, v4, :cond_1

    iget-object v4, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    new-instance v5, Lcom/fmm/ds/e/e;

    invoke-direct {v5}, Lcom/fmm/ds/e/e;-><init>()V

    aput-object v5, v4, v1

    iget-object v4, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v4, v4, v1

    add-int/lit8 v5, v0, 0x1

    aget-byte v5, p0, v5

    shr-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0xf

    add-int/lit16 v5, v5, 0xc8

    iput v5, v4, Lcom/fmm/ds/e/e;->a:I

    iget-object v4, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v4, v4, v1

    add-int/lit8 v5, v0, 0x1

    aget-byte v5, p0, v5

    and-int/lit8 v5, v5, 0xf

    iput v5, v4, Lcom/fmm/ds/e/e;->b:I

    iget-object v4, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v4, v4, v1

    add-int/lit8 v5, v0, 0x2

    aget-byte v5, p0, v5

    shl-int/lit8 v5, v5, 0x10

    add-int/lit8 v6, v0, 0x3

    aget-byte v6, p0, v6

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    add-int/lit8 v6, v0, 0x4

    aget-byte v6, p0, v6

    or-int/2addr v5, v6

    iput v5, v4, Lcom/fmm/ds/e/e;->c:I

    iget-object v4, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v4, v4, v1

    add-int/lit8 v5, v0, 0x5

    aget-byte v5, p0, v5

    iput v5, v4, Lcom/fmm/ds/e/e;->d:I

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p0}, Ljava/lang/String;-><init>([B)V

    iget-object v5, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v5, v5, v1

    add-int/lit8 v6, v0, 0x6

    iget-object v7, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v7, v7, v1

    iget v7, v7, Lcom/fmm/ds/e/e;->d:I

    add-int/2addr v7, v0

    add-int/lit8 v7, v7, 0x6

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/fmm/ds/e/e;->e:Ljava/lang/String;

    add-int/2addr v0, v2

    iget-object v4, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/fmm/ds/e/e;->d:I

    add-int/2addr v0, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/fmm/ds/e/e;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "future : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/fmm/ds/e/e;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "contentType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/fmm/ds/e/e;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serverUriLen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/fmm/ds/e/e;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serverUri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/fmm/ds/e/e;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_0
    const-string v0, "pPushBody is null"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public static a([BII)Z
    .locals 7

    const/4 v1, 0x0

    sget-object v0, Lcom/fmm/ds/e/c;->a:[Lcom/fmm/ds/e/a;

    aget-object v2, v0, p2

    invoke-static {p0, v2}, Lcom/fmm/ds/e/c;->a([BLcom/fmm/ds/e/a;)[B

    move-result-object v0

    iput-object v0, v2, Lcom/fmm/ds/e/a;->e:[B

    array-length v0, p0

    add-int/lit8 v0, v0, -0x10

    new-array v3, v0, [B

    move v0, v1

    :goto_0
    array-length v4, p0

    add-int/lit8 v4, v4, -0x10

    if-ge v0, v4, :cond_0

    add-int/lit8 v4, v0, 0x10

    aget-byte v4, p0, v4

    aput-byte v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v3, v2}, Lcom/fmm/ds/e/c;->b([BLcom/fmm/ds/e/a;)I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bodySize["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], notiHeaderLen["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-nez v0, :cond_1

    const-string v0, "nNotiHeaderLen is 0"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_1
    return v1

    :cond_1
    add-int/lit8 v3, v0, 0x10

    if-ge v3, p1, :cond_3

    add-int/lit8 v3, v0, 0x10

    sub-int v3, p1, v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nNotiBodyLen ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    array-length v4, p0

    add-int/lit8 v5, v0, 0x10

    sub-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nLen ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-array v5, v4, [B

    :goto_2
    if-ge v1, v4, :cond_2

    add-int/lit8 v6, v0, 0x10

    add-int/2addr v6, v1

    aget-byte v6, p0, v6

    aput-byte v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-static {v5, v3, v2}, Lcom/fmm/ds/e/c;->a([BILcom/fmm/ds/e/a;)V

    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static a([BLcom/fmm/ds/e/a;)[B
    .locals 3

    const/16 v2, 0x10

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    new-array v0, v2, [B

    invoke-static {p0, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "pPushBody is null"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b([BLcom/fmm/ds/e/a;)I
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz p0, :cond_2

    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    aget-byte v3, p0, v2

    shl-int/lit8 v3, v3, 0x8

    aget-byte v4, p0, v1

    and-int/lit16 v4, v4, 0xc0

    or-int/2addr v3, v4

    shr-int/lit8 v3, v3, 0x6

    iput v3, v0, Lcom/fmm/ds/e/g;->a:I

    aget-byte v0, p0, v1

    and-int/lit8 v0, v0, 0x30

    shr-int/lit8 v0, v0, 0x4

    packed-switch v0, :pswitch_data_0

    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iput v1, v0, Lcom/fmm/ds/e/g;->b:I

    :goto_0
    aget-byte v0, p0, v1

    and-int/lit8 v0, v0, 0x8

    shr-int/lit8 v0, v0, 0x3

    iget-object v3, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    if-lez v0, :cond_0

    move v0, v1

    :goto_1
    iput v0, v3, Lcom/fmm/ds/e/g;->c:I

    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    aget-byte v1, p0, v1

    and-int/lit8 v1, v1, 0x7

    aget-byte v3, p0, v5

    or-int/2addr v1, v3

    aget-byte v3, p0, v6

    or-int/2addr v1, v3

    aget-byte v3, p0, v7

    or-int/2addr v1, v3

    iput v1, v0, Lcom/fmm/ds/e/g;->d:I

    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    const/4 v1, 0x5

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x8

    const v3, 0xff00

    and-int/2addr v1, v3

    const/4 v3, 0x6

    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    iput v1, v0, Lcom/fmm/ds/e/g;->e:I

    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    const/4 v1, 0x7

    aget-byte v1, p0, v1

    iput v1, v0, Lcom/fmm/ds/e/g;->f:I

    const/4 v0, 0x7

    aget-byte v0, p0, v0

    new-array v1, v0, [B

    :goto_2
    if-ge v2, v0, :cond_1

    add-int/lit8 v3, v2, 0x8

    aget-byte v3, p0, v3

    aput-byte v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :pswitch_0
    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iput v1, v0, Lcom/fmm/ds/e/g;->b:I

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iput v5, v0, Lcom/fmm/ds/e/g;->b:I

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iput v6, v0, Lcom/fmm/ds/e/g;->b:I

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iput v7, v0, Lcom/fmm/ds/e/g;->b:I

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    iput-object v2, v0, Lcom/fmm/ds/e/g;->g:Ljava/lang/String;

    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget v0, v0, Lcom/fmm/ds/e/g;->f:I

    add-int/lit8 v2, v0, 0x8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "version ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget v1, v1, Lcom/fmm/ds/e/g;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "uiMode    ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget v1, v1, Lcom/fmm/ds/e/g;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initiator ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget v1, v1, Lcom/fmm/ds/e/g;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "future    ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget v1, v1, Lcom/fmm/ds/e/g;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sessionID ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget v1, v1, Lcom/fmm/ds/e/g;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "lenServerID ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget v1, v1, Lcom/fmm/ds/e/g;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pServerID ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    iget-object v1, v1, Lcom/fmm/ds/e/g;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :goto_3
    return v2

    :cond_2
    const-string v0, "NotiHeader is null"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b([BI)Lcom/fmm/ds/e/h;
    .locals 9

    const/16 v8, 0x35

    const/4 v1, 0x3

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "pData is NULL"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move-object v0, v3

    :goto_0
    return-object v0

    :cond_1
    aget-byte v0, p0, v7

    and-int/lit16 v0, v0, 0xff

    const/4 v4, 0x6

    if-eq v0, v4, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not Support PDU Type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move-object v0, v3

    goto :goto_0

    :cond_2
    new-instance v5, Lcom/fmm/ds/e/h;

    invoke-direct {v5}, Lcom/fmm/ds/e/h;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WAP Push Msg Len ="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    iget-object v4, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    aget-byte v0, p0, v0

    iput v0, v4, Lcom/fmm/ds/e/i;->b:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WAP Push Header Len : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v4, v4, Lcom/fmm/ds/e/i;->b:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nLoc:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", ["

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v4, p0, v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    aget-byte v0, p0, v1

    and-int/lit16 v0, v0, 0xff

    const/16 v4, 0x1f

    if-ne v0, v4, :cond_19

    const/4 v0, 0x5

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nLoc:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", ["

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v6, p0, v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    aget-byte v4, p0, v0

    and-int/lit16 v4, v4, 0xff

    const/16 v6, 0x20

    if-lt v4, v6, :cond_9

    const/16 v6, 0x80

    if-ge v4, v6, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nLoc ="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v1, v2

    :goto_2
    add-int v4, v0, v1

    aget-byte v4, p0, v4

    if-eqz v4, :cond_3

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    new-array v6, v1, [B

    move v4, v2

    :goto_3
    if-ge v4, v1, :cond_4

    add-int v7, v0, v4

    aget-byte v7, p0, v7

    aput-byte v7, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_4
    add-int/2addr v0, v1

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/lang/String;-><init>([B)V

    const-string v4, "application/vnd.wap.connectivity-wbxml"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v1, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    const/16 v3, 0x36

    iput v3, v1, Lcom/fmm/ds/e/i;->a:I

    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Content Type : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v3, v3, Lcom/fmm/ds/e/i;->a:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    :goto_5
    aget-byte v1, p0, v0

    if-nez v1, :cond_5

    add-int/lit8 v0, v0, 0x1

    :cond_5
    const-string v1, "SEC"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    new-array v3, v1, [B

    move v1, v2

    :goto_6
    const-string v4, "SEC"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_d

    add-int v4, v0, v1

    aget-byte v4, p0, v4

    aput-byte v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    const-string v4, "application/vnd.wap.connectivity-xml"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v1, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iput v8, v1, Lcom/fmm/ds/e/i;->a:I

    goto :goto_4

    :cond_7
    const-string v4, "application/vnd.syncml.ds.notification"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    const/16 v3, 0x4e

    iput v3, v1, Lcom/fmm/ds/e/i;->a:I

    goto :goto_4

    :cond_8
    const-string v0, "Not Support Content Type"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move-object v0, v3

    goto/16 :goto_0

    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Content Value ["

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v6, p0, v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "], Mask Value ["

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v1, p0, v1

    and-int/lit8 v1, v1, 0x7f

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "]"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    aget-byte v1, p0, v0

    and-int/lit8 v1, v1, 0x7f

    iget-object v4, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iput v1, v4, Lcom/fmm/ds/e/i;->a:I

    const/16 v4, 0x36

    if-ne v1, v4, :cond_a

    const-string v1, "Content Type: XNOTI_MIMETYPE_WAP_CONNECTIVITY_WBXML"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Content Type : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%x"

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v7, v7, Lcom/fmm/ds/e/i;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v3, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_5

    :cond_a
    if-ne v1, v8, :cond_b

    const-string v1, "Content Type: XNOTI_MIMETYPE_WAP_CONNECTIVITY_XML"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_7

    :cond_b
    const/16 v4, 0x4e

    if-ne v1, v4, :cond_c

    const-string v1, "Content Type: XNOTI_MIMETYPE_SYNCML_DS_NOTI"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_7

    :cond_c
    const-string v0, "Not Support Content Type"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move-object v0, v3

    goto/16 :goto_0

    :cond_d
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0x91

    if-ne v3, v4, :cond_10

    add-int/lit8 v0, v0, 0x1

    iget-object v1, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    iput v3, v1, Lcom/fmm/ds/e/i;->e:I

    add-int/lit8 v0, v0, 0x1

    :cond_e
    :goto_8
    aget-byte v1, p0, v0

    if-nez v1, :cond_f

    add-int/lit8 v0, v0, 0x1

    :cond_f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "szMsg[nLoc]="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v3, p0, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v1, "MAC"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    new-array v3, v1, [B

    move v1, v2

    :goto_9
    const-string v4, "MAC"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_11

    add-int v4, v0, v1

    aget-byte v4, p0, v4

    aput-byte v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_10
    const-string v3, "SEC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SEC: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v1, "SEC"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget-object v1, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    aget-byte v3, p0, v0

    and-int/lit8 v3, v3, 0xf

    or-int/lit16 v3, v3, 0x80

    iput v3, v1, Lcom/fmm/ds/e/i;->e:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_11
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0x92

    if-ne v3, v4, :cond_15

    add-int/lit8 v3, v0, 0x1

    move v0, v2

    move v1, v3

    :goto_a
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, p0, v1

    if-eqz v1, :cond_12

    add-int/lit8 v0, v0, 0x1

    move v1, v4

    goto :goto_a

    :cond_12
    new-array v6, v0, [B

    move v0, v2

    :goto_b
    add-int/lit8 v1, v3, 0x1

    aget-byte v3, p0, v3

    if-eqz v3, :cond_13

    aput-byte v3, v6, v0

    add-int/lit8 v0, v0, 0x1

    move v3, v1

    goto :goto_b

    :cond_13
    iget-object v0, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    array-length v1, v6

    iput v1, v0, Lcom/fmm/ds/e/i;->d:I

    iget-object v0, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iput-object v6, v0, Lcom/fmm/ds/e/i;->f:[B

    add-int/lit8 v0, v4, 0x1

    :cond_14
    :goto_c
    iget-object v0, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    add-int/lit8 v1, p1, -0x2

    iget-object v3, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v3, v3, Lcom/fmm/ds/e/i;->b:I

    sub-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/fmm/ds/e/i;->c:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Body length : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v1, v1, Lcom/fmm/ds/e/i;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v0, v0, Lcom/fmm/ds/e/i;->b:I

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    iget-object v1, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v1, v1, Lcom/fmm/ds/e/i;->c:I

    new-array v1, v1, [B

    iput-object v1, v5, Lcom/fmm/ds/e/h;->b:[B

    :goto_d
    iget-object v1, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v1, v1, Lcom/fmm/ds/e/i;->c:I

    if-ge v2, v1, :cond_18

    iget-object v3, v5, Lcom/fmm/ds/e/h;->b:[B

    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p0, v0

    aput-byte v0, v3, v2

    add-int/lit8 v2, v2, 0x1

    move v0, v1

    goto :goto_d

    :cond_15
    const-string v3, "MAC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MAC : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    const-string v1, "MAC"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v3, v0, 0x1

    move v0, v2

    move v1, v3

    :goto_e
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, p0, v1

    if-eqz v1, :cond_16

    add-int/lit8 v0, v0, 0x1

    move v1, v4

    goto :goto_e

    :cond_16
    new-array v6, v0, [B

    move v0, v2

    :goto_f
    add-int/lit8 v1, v3, 0x1

    aget-byte v3, p0, v3

    if-eqz v3, :cond_17

    aput-byte v3, v6, v0

    add-int/lit8 v0, v0, 0x1

    move v3, v1

    goto :goto_f

    :cond_17
    iget-object v0, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    array-length v1, v6

    iput v1, v0, Lcom/fmm/ds/e/i;->d:I

    iget-object v0, v5, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iput-object v6, v0, Lcom/fmm/ds/e/i;->f:[B

    add-int/lit8 v0, v4, 0x1

    goto/16 :goto_c

    :cond_18
    move-object v0, v5

    goto/16 :goto_0

    :cond_19
    move v0, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/fmm/ds/e/d;
    .locals 2

    check-cast p1, Lcom/fmm/ds/e/d;

    new-instance v0, Lcom/fmm/ds/e/d;

    invoke-direct {v0}, Lcom/fmm/ds/e/d;-><init>()V

    if-nez p1, :cond_0

    const-string v0, "pSrc is NULL"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p1, Lcom/fmm/ds/e/d;->d:[B

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/fmm/ds/e/d;->d:[B

    iput-object v1, v0, Lcom/fmm/ds/e/d;->d:[B

    iget v1, p1, Lcom/fmm/ds/e/d;->g:I

    iput v1, v0, Lcom/fmm/ds/e/d;->g:I

    :cond_1
    :goto_1
    iget v1, p1, Lcom/fmm/ds/e/d;->c:I

    iput v1, v0, Lcom/fmm/ds/e/d;->c:I

    iget v1, p1, Lcom/fmm/ds/e/d;->b:I

    iput v1, v0, Lcom/fmm/ds/e/d;->b:I

    iget v1, p1, Lcom/fmm/ds/e/d;->i:I

    iput v1, v0, Lcom/fmm/ds/e/d;->i:I

    iget v1, p1, Lcom/fmm/ds/e/d;->a:I

    iput v1, v0, Lcom/fmm/ds/e/d;->a:I

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lcom/fmm/ds/e/d;->f:[B

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/fmm/ds/e/d;->f:[B

    iput-object v1, v0, Lcom/fmm/ds/e/d;->f:[B

    iget v1, p1, Lcom/fmm/ds/e/d;->h:I

    iput v1, v0, Lcom/fmm/ds/e/d;->h:I

    goto :goto_1
.end method

.method public a(Lcom/fmm/ds/e/a;)V
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_1

    const-string v0, "pNotiMsg is NULL"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/ds/e/g;->g:Ljava/lang/String;

    iput-object v3, p1, Lcom/fmm/ds/e/a;->f:Lcom/fmm/ds/e/g;

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/e/a;->g:Lcom/fmm/ds/e/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p1, Lcom/fmm/ds/e/a;->g:Lcom/fmm/ds/e/f;

    iget v1, v1, Lcom/fmm/ds/e/f;->a:I

    if-ge v0, v1, :cond_3

    iget-object v1, p1, Lcom/fmm/ds/e/a;->g:Lcom/fmm/ds/e/f;

    iget-object v1, v1, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v1, v1, v0

    const-string v2, ""

    iput-object v2, v1, Lcom/fmm/ds/e/e;->e:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iput-object v3, p1, Lcom/fmm/ds/e/a;->g:Lcom/fmm/ds/e/f;

    goto :goto_0
.end method

.method public b(Lcom/fmm/ds/e/d;)Lcom/fmm/ds/e/a;
    .locals 2

    new-instance v0, Lcom/fmm/ds/e/a;

    invoke-direct {v0}, Lcom/fmm/ds/e/a;-><init>()V

    iget v1, p1, Lcom/fmm/ds/e/d;->b:I

    packed-switch v1, :pswitch_data_0

    const-string v1, "Not Support Push Type"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    invoke-static {p1}, Lcom/fmm/ds/e/c;->a(Lcom/fmm/ds/e/d;)V

    return-object v0

    :pswitch_0
    const/4 v0, 0x0

    iput v0, p1, Lcom/fmm/ds/e/d;->a:I

    const/4 v0, 0x1

    iput v0, p1, Lcom/fmm/ds/e/d;->c:I

    invoke-virtual {p0, p1}, Lcom/fmm/ds/e/c;->c(Lcom/fmm/ds/e/d;)Lcom/fmm/ds/e/a;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public c(Lcom/fmm/ds/e/d;)Lcom/fmm/ds/e/a;
    .locals 9

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string v1, "pPushMsg is NULL"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    iget v2, p1, Lcom/fmm/ds/e/d;->a:I

    sget-object v3, Lcom/fmm/ds/e/c;->a:[Lcom/fmm/ds/e/a;

    aget-object v3, v3, v2

    iput v2, v3, Lcom/fmm/ds/e/a;->a:I

    sget-object v3, Lcom/fmm/ds/e/c;->a:[Lcom/fmm/ds/e/a;

    aget-object v3, v3, v2

    iget v4, p1, Lcom/fmm/ds/e/d;->c:I

    iput v4, v3, Lcom/fmm/ds/e/a;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pHeader["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/fmm/ds/e/d;->e:[B

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "pBody["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/fmm/ds/e/d;->f:[B

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "],"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "pData["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/fmm/ds/e/d;->d:[B

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/fmm/ds/e/d;->d:[B

    if-eqz v3, :cond_4

    iget-object v3, p1, Lcom/fmm/ds/e/d;->d:[B

    aget-byte v3, v3, v1

    if-eqz v3, :cond_3

    iget v3, p1, Lcom/fmm/ds/e/d;->g:I

    new-array v3, v3, [B

    iget-object v3, p1, Lcom/fmm/ds/e/d;->d:[B

    iget v4, p1, Lcom/fmm/ds/e/d;->g:I

    invoke-static {v3, v4}, Lcom/fmm/ds/e/c;->a([BI)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    sub-int v6, v4, v5

    new-array v7, v6, [B

    :goto_1
    if-ge v1, v6, :cond_1

    add-int v8, v5, v1

    aget-byte v8, v3, v8

    aput-byte v8, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    sub-int v1, v4, v5

    iput v6, p1, Lcom/fmm/ds/e/d;->h:I

    iput-object v7, p1, Lcom/fmm/ds/e/d;->f:[B

    iput-object v0, p1, Lcom/fmm/ds/e/d;->d:[B

    :cond_2
    :goto_2
    iget-object v1, p1, Lcom/fmm/ds/e/d;->f:[B

    iget v3, p1, Lcom/fmm/ds/e/d;->h:I

    invoke-static {v1, v3, v2}, Lcom/fmm/ds/e/c;->a([BII)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/fmm/ds/e/c;->a:[Lcom/fmm/ds/e/a;

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/fmm/ds/e/c;->a(Lcom/fmm/ds/e/a;)V

    goto/16 :goto_0

    :cond_3
    const-string v1, "pPushMsg.pData[0] is 0"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    const-string v1, "pPushMsg.pData is NULL"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/fmm/ds/e/c;->a:[Lcom/fmm/ds/e/a;

    aget-object v0, v0, v2

    goto/16 :goto_0
.end method
