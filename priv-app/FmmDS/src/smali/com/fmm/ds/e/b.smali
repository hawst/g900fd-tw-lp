.class public Lcom/fmm/ds/e/b;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)V
    .locals 3

    const/4 v2, 0x0

    const/16 v0, 0x28

    packed-switch p0, :pswitch_data_0

    const-string v1, "default"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    invoke-static {v0, v2, v2}, Lcom/fmm/ds/c/ba;->b(ILjava/lang/Object;Ljava/lang/Object;)V

    return-void

    :pswitch_0
    const-string v1, "XNOTI_MODE_NOT_SPECIFIED"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "XNOTI_MODE_BACKGROUND"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v0, 0x29

    goto :goto_0

    :pswitch_2
    const-string v0, "XNOTI_MODE_INFORMATIVE"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v0, 0x2a

    goto :goto_0

    :pswitch_3
    const-string v0, "XNOTI_MODE_INTERACTIVE"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v0, 0x2b

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/fmm/ds/e/f;)Z
    .locals 8

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v5

    iget v0, p0, Lcom/fmm/ds/e/f;->a:I

    if-nez v0, :cond_0

    const-string v0, "Number of sync is 0, bRet = false"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    move v0, v1

    move v2, v1

    move v3, v1

    :goto_1
    iget v6, p0, Lcom/fmm/ds/e/f;->a:I

    if-ge v0, v6, :cond_2

    iget-object v6, p0, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/fmm/ds/e/e;->e:Ljava/lang/String;

    iget-object v7, v5, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-object v7, v7, Lcom/fmm/ds/d/j;->w:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    iget-object v2, v5, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput-boolean v4, v2, Lcom/fmm/ds/d/j;->a:Z

    iget-object v2, v5, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget-object v3, p0, Lcom/fmm/ds/e/f;->b:[Lcom/fmm/ds/e/e;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/fmm/ds/e/e;->a:I

    invoke-static {v3}, Lcom/fmm/ds/e/b;->b(I)I

    move-result v3

    iput v3, v2, Lcom/fmm/ds/d/j;->c:I

    iget-object v2, v5, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iget v2, v2, Lcom/fmm/ds/d/j;->c:I

    const-string v3, "Selected CALLLOG"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v3, v4

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v6, v5, Lcom/fmm/ds/d/h;->u:Lcom/fmm/ds/d/j;

    iput-boolean v1, v6, Lcom/fmm/ds/d/j;->a:Z

    goto :goto_2

    :cond_2
    packed-switch v2, :pswitch_data_0

    const-string v0, "UISyncType is not exist"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v3, v1

    :goto_3
    invoke-static {v5, v1}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    move v1, v3

    goto :goto_0

    :pswitch_0
    iput v4, v5, Lcom/fmm/ds/d/h;->f:I

    goto :goto_3

    :pswitch_1
    const/4 v0, 0x2

    iput v0, v5, Lcom/fmm/ds/d/h;->f:I

    goto :goto_3

    :pswitch_2
    const/4 v0, 0x3

    iput v0, v5, Lcom/fmm/ds/d/h;->f:I

    goto :goto_3

    :pswitch_3
    const/4 v0, 0x6

    iput v0, v5, Lcom/fmm/ds/d/h;->f:I

    goto :goto_3

    :pswitch_4
    const/4 v0, 0x4

    iput v0, v5, Lcom/fmm/ds/d/h;->f:I

    goto :goto_3

    :pswitch_5
    const/4 v0, 0x5

    iput v0, v5, Lcom/fmm/ds/d/h;->f:I

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/fmm/ds/e/g;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    iget-object v1, p0, Lcom/fmm/ds/e/g;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "headerMsg.pServerID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/fmm/ds/e/g;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fmm/ds/e/g;->g:Ljava/lang/String;

    const-string v2, "samsungdive"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v1

    iget v2, p0, Lcom/fmm/ds/e/g;->e:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/fmm/ds/e/g;->e:I

    iput v2, v1, Lcom/fmm/ds/d/h;->a:I

    :cond_0
    invoke-static {v1, v0}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const-string v1, "headerMsg.pServerID is null"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v1, "Undefine server Id !!!!!!!!!!!!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(I)I
    .locals 2

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "206"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "TWO-WAY BY SERVER"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const-string v1, "207"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "ONE-WAY FROM CLIENT BY SERVER"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const-string v1, "208"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "REFRESH FROM CLIENT BY SERVER"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x6

    goto :goto_0

    :cond_2
    const-string v1, "209"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    const-string v0, "ONE-WAY FROM SERVER BY SERVER"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const-string v1, "210"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "REFRESH FROM SERVER BY SERVER"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x5

    goto :goto_0

    :cond_4
    const-string v0, "XDS_SYNC_TYPE_UNDEFINE"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a([BI)Z
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, p2}, Lcom/fmm/ds/e/c;->b([BI)Lcom/fmm/ds/e/h;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v1, "xnotiPushHdrParsingWSPHeader Error"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    iget-object v3, v2, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v3, v3, Lcom/fmm/ds/e/i;->a:I

    packed-switch v3, :pswitch_data_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not Support Content Type :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "0x%x"

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/fmm/ds/e/h;->a:Lcom/fmm/ds/e/i;

    iget v2, v2, Lcom/fmm/ds/e/i;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    new-instance v0, Lcom/fmm/ds/e/d;

    invoke-direct {v0}, Lcom/fmm/ds/e/d;-><init>()V

    iput v1, v0, Lcom/fmm/ds/e/d;->b:I

    iput p2, v0, Lcom/fmm/ds/e/d;->g:I

    iput-object p1, v0, Lcom/fmm/ds/e/d;->d:[B

    const/16 v2, 0x14

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x4e
        :pswitch_0
    .end packed-switch
.end method

.method public b([BI)Z
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/fmm/ds/e/d;

    invoke-direct {v0}, Lcom/fmm/ds/e/d;-><init>()V

    iput v3, v0, Lcom/fmm/ds/e/d;->b:I

    iput-object p1, v0, Lcom/fmm/ds/e/d;->f:[B

    iput p2, v0, Lcom/fmm/ds/e/d;->h:I

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    return v3
.end method
