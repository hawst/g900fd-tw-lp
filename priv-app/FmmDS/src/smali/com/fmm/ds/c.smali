.class Lcom/fmm/ds/c;
.super Ljava/lang/Thread;


# instance fields
.field a:Landroid/os/Handler;

.field public b:I


# direct methods
.method constructor <init>(Landroid/os/Handler;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p1, p0, Lcom/fmm/ds/c;->a:Landroid/os/Handler;

    iput p2, p0, Lcom/fmm/ds/c;->b:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-string v0, "Run Start"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/16 v0, 0x14

    if-ge v1, v0, :cond_0

    sget-boolean v0, Lcom/fmm/ds/XDSApplication;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c;->a:Landroid/os/Handler;

    iget v1, p0, Lcom/fmm/ds/c;->b:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_1
    :try_start_0
    const-string v0, "Waiting for DS Service Start"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
