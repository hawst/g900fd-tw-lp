.class Lcom/fmm/ds/b;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lcom/fmm/ds/XDSBroadcastReceiver;


# direct methods
.method constructor <init>(Lcom/fmm/ds/XDSBroadcastReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "SERVICE INIT FINISHED"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Lcom/fmm/ds/e/b;

    invoke-direct {v0}, Lcom/fmm/ds/e/b;-><init>()V

    iget-object v1, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v1}, Lcom/fmm/ds/XDSBroadcastReceiver;->a(Lcom/fmm/ds/XDSBroadcastReceiver;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v2}, Lcom/fmm/ds/XDSBroadcastReceiver;->a(Lcom/fmm/ds/XDSBroadcastReceiver;)[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/fmm/ds/e/b;->b([BI)Z

    goto :goto_0

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XCOMMON_INTENT_SET_DS_USERID_RECEIVED szUserId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v1}, Lcom/fmm/ds/XDSBroadcastReceiver;->b(Lcom/fmm/ds/XDSBroadcastReceiver;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->b(Lcom/fmm/ds/XDSBroadcastReceiver;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->f()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v2}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fmm/ds/XDSBroadcastReceiver;->a(Lcom/fmm/ds/XDSBroadcastReceiver;Lcom/fmm/ds/d/h;)Lcom/fmm/ds/d/h;

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->c(Lcom/fmm/ds/XDSBroadcastReceiver;)Lcom/fmm/ds/d/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->c(Lcom/fmm/ds/XDSBroadcastReceiver;)Lcom/fmm/ds/d/h;

    move-result-object v0

    iget-object v1, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v1}, Lcom/fmm/ds/XDSBroadcastReceiver;->b(Lcom/fmm/ds/XDSBroadcastReceiver;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->b(Lcom/fmm/ds/XDSBroadcastReceiver;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/fmm/ds/d/a;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->c(Lcom/fmm/ds/XDSBroadcastReceiver;)Lcom/fmm/ds/d/h;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "SIGN OUT INTENT Received!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v2}, Lcom/fmm/ds/d/a;->a(I)Lcom/fmm/ds/d/h;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fmm/ds/XDSBroadcastReceiver;->a(Lcom/fmm/ds/XDSBroadcastReceiver;Lcom/fmm/ds/d/h;)Lcom/fmm/ds/d/h;

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->c(Lcom/fmm/ds/XDSBroadcastReceiver;)Lcom/fmm/ds/d/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->c(Lcom/fmm/ds/XDSBroadcastReceiver;)Lcom/fmm/ds/d/h;

    move-result-object v0

    iget-object v0, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->c(Lcom/fmm/ds/XDSBroadcastReceiver;)Lcom/fmm/ds/d/h;

    move-result-object v0

    const-string v1, ""

    iput-object v1, v0, Lcom/fmm/ds/d/h;->h:Ljava/lang/String;

    const-string v0, ""

    invoke-static {v2, v0}, Lcom/fmm/ds/d/a;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/b;->a:Lcom/fmm/ds/XDSBroadcastReceiver;

    invoke-static {v0}, Lcom/fmm/ds/XDSBroadcastReceiver;->c(Lcom/fmm/ds/XDSBroadcastReceiver;)Lcom/fmm/ds/d/h;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/h;I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
