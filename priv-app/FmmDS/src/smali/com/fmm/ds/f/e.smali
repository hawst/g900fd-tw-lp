.class public Lcom/fmm/ds/f/e;
.super Ljava/lang/Object;


# static fields
.field public static a:Ljava/util/Timer;

.field public static b:I

.field private static c:Lcom/fmm/ds/f/g;

.field private static d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-object v1, Lcom/fmm/ds/f/e;->c:Lcom/fmm/ds/f/g;

    sput-boolean v0, Lcom/fmm/ds/f/e;->d:Z

    sput-object v1, Lcom/fmm/ds/f/e;->a:Ljava/util/Timer;

    sput v0, Lcom/fmm/ds/f/e;->b:I

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/fmm/ds/f/g;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/fmm/ds/f/g;-><init>(Lcom/fmm/ds/f/f;)V

    sput-object v0, Lcom/fmm/ds/f/e;->c:Lcom/fmm/ds/f/g;

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/fmm/ds/f/e;->a:Ljava/util/Timer;

    invoke-static {}, Lcom/fmm/ds/f/e;->a()V

    sput-boolean p1, Lcom/fmm/ds/f/e;->d:Z

    return-void
.end method

.method public static a()V
    .locals 5

    sget-object v0, Lcom/fmm/ds/f/e;->a:Ljava/util/Timer;

    sget-object v1, Lcom/fmm/ds/f/e;->c:Lcom/fmm/ds/f/g;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    const-wide/16 v3, 0x1388

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;Ljava/util/Date;J)V

    return-void
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, Lcom/fmm/ds/f/e;->d:Z

    return p0
.end method

.method public static b()V
    .locals 1

    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/fmm/ds/f/e;->b:I

    const/4 v0, 0x0

    sput-boolean v0, Lcom/fmm/ds/f/e;->d:Z

    sget-object v0, Lcom/fmm/ds/f/e;->a:Ljava/util/Timer;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fmm/ds/f/e;->c:Lcom/fmm/ds/f/g;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "=====================>> endTimer(connect)"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/f/e;->a:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    sget-object v0, Lcom/fmm/ds/f/e;->c:Lcom/fmm/ds/f/g;

    invoke-virtual {v0}, Lcom/fmm/ds/f/g;->cancel()Z

    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/ds/f/e;->a:Ljava/util/Timer;

    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/ds/f/e;->c:Lcom/fmm/ds/f/g;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c()Z
    .locals 1

    sget-boolean v0, Lcom/fmm/ds/f/e;->d:Z

    return v0
.end method
