.class public Lcom/fmm/ds/f/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Landroid/content/Context;

.field private E:Landroid/net/wifi/WifiManager$WifiLock;

.field private F:Landroid/os/PowerManager$WakeLock;

.field private G:Landroid/content/ContentResolver;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:Ljava/net/Socket;

.field private g:Ljavax/net/ssl/SSLSocket;

.field private h:Ljavax/net/ssl/SSLContext;

.field private i:Ljava/io/InputStream;

.field private j:Ljava/io/OutputStream;

.field private k:Ljava/lang/Thread;

.field private l:Landroid/os/Handler;

.field private m:Ljava/io/ByteArrayOutputStream;

.field private n:Lcom/fmm/ds/c/as;

.field private o:Lcom/fmm/ds/c/as;

.field private p:Lcom/fmm/ds/c/as;

.field private q:I

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljavax/net/ssl/SSLSocketFactory;

.field private u:Lcom/fmm/ds/f/d;

.field private v:Lcom/fmm/ds/d/h;

.field private w:Lcom/fmm/ds/d/p;

.field private x:I

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/fmm/ds/f/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/16 v1, 0x1400

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x15f90

    iput v0, p0, Lcom/fmm/ds/f/a;->b:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/fmm/ds/f/a;->c:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/fmm/ds/f/a;->d:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/fmm/ds/f/a;->e:I

    iput-object v2, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    iput-object v2, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    iput-object v2, p0, Lcom/fmm/ds/f/a;->k:Ljava/lang/Thread;

    new-instance v0, Lcom/fmm/ds/c/as;

    invoke-direct {v0, v1}, Lcom/fmm/ds/c/as;-><init>(I)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    new-instance v0, Lcom/fmm/ds/c/as;

    invoke-direct {v0, v1}, Lcom/fmm/ds/c/as;-><init>(I)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    new-instance v0, Lcom/fmm/ds/c/as;

    invoke-direct {v0, v1}, Lcom/fmm/ds/c/as;-><init>(I)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/fmm/ds/a/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/fmm/ds/a/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SyncML_DS Client"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->s:Ljava/lang/String;

    iput-object v2, p0, Lcom/fmm/ds/f/a;->t:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v2, p0, Lcom/fmm/ds/f/a;->u:Lcom/fmm/ds/f/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fmm/ds/f/a;->C:Z

    iput-object v2, p0, Lcom/fmm/ds/f/a;->D:Landroid/content/Context;

    iput-object v2, p0, Lcom/fmm/ds/f/a;->E:Landroid/net/wifi/WifiManager$WifiLock;

    iput-object v2, p0, Lcom/fmm/ds/f/a;->F:Landroid/os/PowerManager$WakeLock;

    iput-object p1, p0, Lcom/fmm/ds/f/a;->D:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->G:Landroid/content/ContentResolver;

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->k:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->k:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private a(Ljava/net/Socket;)I
    .locals 8

    const/4 v4, -0x2

    const/4 v2, 0x0

    const-string v0, "[xtpTpTunnelHandshake]"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    invoke-virtual {p0}, Lcom/fmm/ds/f/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, -0x1

    :cond_0
    :goto_0
    return v2

    :cond_1
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    const/16 v0, 0xc8

    new-array v5, v0, [B

    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    move v0, v2

    move v1, v2

    move v3, v2

    :cond_2
    :goto_2
    const/4 v6, 0x2

    if-ge v1, v6, :cond_5

    iget-object v6, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    if-gez v6, :cond_3

    const-string v0, "Unable to tunnel"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v2, v4

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_1

    :cond_3
    const/16 v7, 0xa

    if-ne v6, v7, :cond_4

    const/4 v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/16 v7, 0xd

    if-eq v6, v7, :cond_2

    if-nez v0, :cond_6

    array-length v1, v5

    if-ge v3, v1, :cond_6

    add-int/lit8 v1, v3, 0x1

    int-to-byte v6, v6

    aput-byte v6, v5, v3

    move v3, v1

    move v1, v2

    goto :goto_2

    :cond_5
    const-string v1, ""

    :try_start_1
    new-instance v0, Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "UTF-8"

    invoke-direct {v0, v5, v6, v3, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Proxy returns \""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    const-string v1, "HTTP/1.1 200"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "HTTP/1.0 200"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Unable to tunnel through "

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5, v2, v3}, Ljava/lang/String;-><init>([BII)V

    goto :goto_3

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_3

    :cond_6
    move v1, v2

    goto/16 :goto_2
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    const-string v1, "\\."

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/4 v3, 0x0

    aget-char v2, v2, v3

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_1

    array-length v2, v1

    new-array v2, v2, [I

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s.%s.%s.%s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    aget v5, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x1

    aget v5, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x2

    aget v5, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x3

    aget v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "szValidAddress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p0, v0

    :cond_1
    :goto_1
    return-object p0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(ILjava/lang/Object;)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    new-instance v0, Lcom/fmm/ds/c/bd;

    invoke-direct {v0}, Lcom/fmm/ds/c/bd;-><init>()V

    iput-object p2, v0, Lcom/fmm/ds/c/bd;->a:Ljava/lang/Object;

    :cond_0
    new-instance v1, Lcom/fmm/ds/c/bc;

    invoke-direct {v1}, Lcom/fmm/ds/c/bc;-><init>()V

    if-eqz v1, :cond_1

    iput p1, v1, Lcom/fmm/ds/c/bc;->a:I

    iput-object v0, v1, Lcom/fmm/ds/c/bc;->b:Lcom/fmm/ds/c/bd;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->l:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/fmm/ds/f/a;->l:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_1
    const-string v0, "Can\'t send message"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/os/Message;)V
    .locals 7

    const/4 v6, 0x7

    const/4 v5, 0x0

    const/4 v4, 0x5

    const/4 v3, 0x0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "Stopping..."

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/fmm/ds/c/bc;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Message Type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/fmm/ds/c/bc;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget v0, v0, Lcom/fmm/ds/c/bc;->a:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    :try_start_0
    invoke-direct {p0}, Lcom/fmm/ds/f/a;->j()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MSG_TP_OPEN"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-nez v0, :cond_4

    const/16 v0, 0xd

    invoke-static {v0, v3, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v4, v3, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MSG_TP_CLOSE"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/16 v0, 0xe

    invoke-static {v0, v3, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    const/4 v0, -0x2

    goto :goto_1

    :cond_4
    const-string v0, "connect fail"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_3
    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(I)V

    :try_start_1
    invoke-direct {p0}, Lcom/fmm/ds/f/a;->l()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :goto_2
    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "MSG_TP_SEND"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_6

    const-string v0, "send fail"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x3

    invoke-static {v0, v5}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    invoke-static {v6, v0, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    :cond_5
    :goto_3
    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(I)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    const/4 v0, -0x3

    goto :goto_2

    :cond_6
    invoke-direct {p0}, Lcom/fmm/ds/f/a;->m()I

    move-result v0

    invoke-static {}, Lcom/fmm/ds/b/a;->n()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v1

    if-eqz v1, :cond_5

    if-nez v0, :cond_7

    const/16 v0, 0xf

    invoke-static {v0, v3, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    const/4 v0, 0x6

    invoke-static {v0, v3, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3

    :cond_7
    const-string v0, "receive fail"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-static {v0, v5}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v0

    invoke-static {v6, v0, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/fmm/ds/f/a;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/fmm/ds/f/a;->a(Landroid/os/Message;)V

    return-void
.end method

.method private i()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "xtpSslInit()"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :try_start_0
    const-string v2, "TLS"

    invoke-static {v2}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/f/a;->h:Ljavax/net/ssl/SSLContext;

    iget-object v2, p0, Lcom/fmm/ds/f/a;->h:Ljavax/net/ssl/SSLContext;

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljavax/net/ssl/TrustManager;

    const/4 v5, 0x0

    new-instance v6, Lcom/fmm/ds/f/c;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/fmm/ds/f/c;-><init>(Ljava/security/KeyStore;)V

    aput-object v6, v4, v5

    new-instance v5, Ljava/security/SecureRandom;

    invoke-direct {v5}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v2, v3, v4, v5}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    iget-object v2, p0, Lcom/fmm/ds/f/a;->h:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLContext;->getServerSessionContext()Ljavax/net/ssl/SSLSessionContext;

    move-result-object v2

    const v3, 0x15f90

    invoke-interface {v2, v3}, Ljavax/net/ssl/SSLSessionContext;->setSessionTimeout(I)V

    iget-object v2, p0, Lcom/fmm/ds/f/a;->h:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/f/a;->t:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "xtpSslInit"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method private j()I
    .locals 1

    const-string v0, "[xtpTpOpenInternal]"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/fmm/ds/f/a;->g()V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->D:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/ds/a/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fmm/ds/f/a;->e()V

    :cond_0
    invoke-direct {p0}, Lcom/fmm/ds/f/a;->k()I

    move-result v0

    return v0
.end method

.method private k()I
    .locals 7

    const/4 v3, 0x1

    const/4 v1, -0x2

    const/4 v2, 0x0

    const-string v0, "xtpTpConnect()"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/fmm/ds/f/e;

    invoke-direct {v0, v3}, Lcom/fmm/ds/f/e;-><init>(Z)V

    :try_start_0
    iget v0, p0, Lcom/fmm/ds/f/a;->z:I

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "sslSocket != null && sslSocket.isConnected()"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    :cond_0
    invoke-direct {p0}, Lcom/fmm/ds/f/a;->i()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    invoke-static {}, Lcom/fmm/ds/f/e;->b()V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/f/a;->D:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/ds/a/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/fmm/ds/f/a;->w:Lcom/fmm/ds/d/p;

    iget-object v0, v0, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    iget-boolean v0, v0, Lcom/fmm/ds/d/f;->d:Z

    if-eqz v0, :cond_4

    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->w:Lcom/fmm/ds/d/p;

    iget-object v0, v0, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    iget-object v3, v0, Lcom/fmm/ds/d/f;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->w:Lcom/fmm/ds/d/p;

    iget-object v0, v0, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    new-instance v4, Ljava/net/InetSocketAddress;

    invoke-direct {v4, v3, v0}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/fmm/ds/f/e;->b()V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    const v3, 0x15f90

    invoke-virtual {v0, v4, v3}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    invoke-direct {p0, v0}, Lcom/fmm/ds/f/a;->a(Ljava/net/Socket;)I

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    invoke-static {}, Lcom/fmm/ds/f/e;->b()V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/fmm/ds/f/a;->t:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    iget-object v4, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    iget v5, p0, Lcom/fmm/ds/f/a;->x:I

    const/4 v6, 0x1

    invoke-virtual {v0, v3, v4, v5, v6}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iput-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->setUseClientMode(Z)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    const v3, 0x15f90

    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    new-instance v0, Lcom/fmm/ds/f/d;

    invoke-direct {v0}, Lcom/fmm/ds/f/d;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->u:Lcom/fmm/ds/f/d;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->u:Lcom/fmm/ds/f/d;

    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    :try_start_2
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-direct {v0, v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    new-instance v0, Ljava/io/BufferedOutputStream;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-direct {v0, v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    invoke-static {}, Lcom/fmm/ds/f/e;->b()V

    move v0, v2

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {}, Lcom/fmm/ds/f/e;->b()V

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/fmm/ds/f/a;->t:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    iget v4, p0, Lcom/fmm/ds/f/a;->x:I

    invoke-virtual {v0, v3, v4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iput-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->setUseClientMode(Z)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    const v3, 0x15f90

    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "m_csHost : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "m_nPort : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/fmm/ds/f/a;->x:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    new-instance v0, Lcom/fmm/ds/f/d;

    invoke-direct {v0}, Lcom/fmm/ds/f/d;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->u:Lcom/fmm/ds/f/d;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->u:Lcom/fmm/ds/f/d;

    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/fmm/ds/f/e;->b()V

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {}, Lcom/fmm/ds/f/e;->b()V

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "m_socket != null && m_socket.isConnected()"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    :cond_6
    iget-object v0, p0, Lcom/fmm/ds/f/a;->D:Landroid/content/Context;

    invoke-static {v0}, Lcom/fmm/ds/a/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/fmm/ds/f/a;->w:Lcom/fmm/ds/d/p;

    iget-object v0, v0, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    iget-boolean v0, v0, Lcom/fmm/ds/d/f;->d:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/fmm/ds/f/a;->w:Lcom/fmm/ds/d/p;

    iget-object v0, v0, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    iget-object v3, v0, Lcom/fmm/ds/d/f;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->w:Lcom/fmm/ds/d/p;

    iget-object v0, v0, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_4
    new-instance v4, Ljava/net/Proxy;

    sget-object v5, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v6, Ljava/net/InetSocketAddress;

    invoke-direct {v6, v3, v0}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v4, v5, v6}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    invoke-virtual {v4}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "conProxy.address() : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :goto_5
    new-instance v3, Ljava/net/Socket;

    invoke-direct {v3}, Ljava/net/Socket;-><init>()V

    iput-object v3, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    const v4, 0x15f90

    invoke-virtual {v3, v0, v4}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-direct {v0, v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    new-instance v0, Ljava/io/BufferedOutputStream;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-direct {v0, v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    goto/16 :goto_3

    :cond_7
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    iget v4, p0, Lcom/fmm/ds/f/a;->x:I

    invoke-direct {v0, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "m_csHost : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "m_nPort : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/fmm/ds/f/a;->x:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_5

    :cond_8
    move v0, v2

    goto/16 :goto_4

    :cond_9
    move v0, v2

    goto/16 :goto_1
.end method

.method private l()I
    .locals 8

    const/4 v2, -0x3

    const/4 v0, -0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    const-string v3, "xtpTplSendDataInternal()"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    invoke-virtual {v4}, Lcom/fmm/ds/c/as;->e()I

    move-result v4

    const/4 v5, 0x5

    if-ge v4, v5, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget-boolean v4, p0, Lcom/fmm/ds/f/a;->C:Z

    if-eqz v4, :cond_3

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->k()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEND exception"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :try_start_1
    iput-boolean v4, p0, Lcom/fmm/ds/f/a;->C:Z

    :cond_3
    iget v4, p0, Lcom/fmm/ds/f/a;->z:I

    if-ne v4, v6, :cond_6

    iget-object v4, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    if-nez v4, :cond_4

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    const v4, 0x15f90

    invoke-virtual {v0, v4}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    :goto_1
    iget-object v0, p0, Lcom/fmm/ds/f/a;->w:Lcom/fmm/ds/d/p;

    iget-object v0, v0, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    iget-boolean v0, v0, Lcom/fmm/ds/d/f;->d:Z

    if-eqz v0, :cond_8

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s %s %s\r\n"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "POST"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/fmm/ds/f/a;->B:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "HTTP/1.1"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Cache-Control"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "no-store, private"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/fmm/ds/f/a;->C:Z

    if-eqz v0, :cond_9

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Connection"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "Close"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "User-Agent"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/fmm/ds/f/a;->s:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Accept"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "application/vnd.syncml+wbxml"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Accept-Language"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "en"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Accept-Charset"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "UTF-8"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Host"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Content-Type"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "application/vnd.syncml+wbxml"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%d\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Content-Length"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    invoke-virtual {v7}, Lcom/fmm/ds/c/as;->e()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/fmm/ds/f/a;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Cookie"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lcom/fmm/ds/f/a;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v0, "\r\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Send Header:"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/OutputStream;->write([B)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    invoke-virtual {v3}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    invoke-virtual {v5}, Lcom/fmm/ds/c/as;->e()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Ljava/io/OutputStream;->write([BII)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->m:Ljava/io/ByteArrayOutputStream;

    iget-object v3, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    invoke-virtual {v3}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    invoke-virtual {v5}, Lcom/fmm/ds/c/as;->e()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    move v0, v1

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    if-nez v4, :cond_7

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    const v4, 0x15f90

    invoke-virtual {v0, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    goto/16 :goto_1

    :cond_8
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s %s %s\r\n"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "POST"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/fmm/ds/f/a;->A:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "HTTP/1.1"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_9
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s:%s\r\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Connection"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "Keep-Alive"

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method

.method private m()I
    .locals 6

    const/4 v4, 0x1

    const/4 v1, -0x2

    const/4 v2, 0x0

    const/4 v0, -0x4

    const-string v3, "\n"

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    if-nez v3, :cond_1

    const-string v1, "input stream is null !!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget v3, p0, Lcom/fmm/ds/f/a;->z:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    if-nez v3, :cond_2

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    const v3, 0x15f90

    invoke-virtual {v1, v3}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    :goto_1
    iget-object v1, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->b()V

    iget-object v1, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    invoke-virtual {p0, v1}, Lcom/fmm/ds/f/a;->b(Ljava/io/InputStream;)I

    move-result v1

    const/16 v3, 0xc8

    if-ne v1, v3, :cond_0

    iget v1, p0, Lcom/fmm/ds/f/a;->q:I

    if-gtz v1, :cond_5

    const-string v1, "Content Length undefined. Try later."

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_2
    move v0, v2

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    if-nez v3, :cond_4

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    const v3, 0x15f90

    invoke-virtual {v1, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/fmm/ds/f/a;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/fmm/ds/f/a;->r:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v3, "close"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/fmm/ds/f/a;->C:Z

    :goto_3
    iget-object v1, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->e()I

    move-result v1

    iget-object v3, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    invoke-virtual {v3}, Lcom/fmm/ds/c/as;->c()I

    move-result v3

    sub-int/2addr v1, v3

    iget-object v3, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    invoke-virtual {v3}, Lcom/fmm/ds/c/as;->b()V

    if-lez v1, :cond_6

    iget-object v3, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    iget-object v4, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    invoke-virtual {v4}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v4

    iget-object v5, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    invoke-virtual {v5}, Lcom/fmm/ds/c/as;->c()I

    move-result v5

    invoke-virtual {v3, v4, v5, v1}, Lcom/fmm/ds/c/as;->a([BII)Lcom/fmm/ds/c/as;

    :cond_6
    iget-object v3, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    iget v4, p0, Lcom/fmm/ds/f/a;->q:I

    invoke-virtual {v3, v4}, Lcom/fmm/ds/c/as;->c(I)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "m_recvBodyLength = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/fmm/ds/f/a;->q:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_4
    iget v3, p0, Lcom/fmm/ds/f/a;->q:I

    if-ge v1, v3, :cond_a

    iget-object v3, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    if-nez v3, :cond_8

    const-string v1, "input stream is null !"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/fmm/ds/f/a;->C:Z

    goto :goto_3

    :cond_8
    iget-object v3, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    iget-object v4, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    invoke-virtual {v4}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v4

    iget v5, p0, Lcom/fmm/ds/f/a;->q:I

    sub-int/2addr v5, v1

    invoke-virtual {v3, v4, v1, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_9

    const-string v1, "unexpected end of stream"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    add-int/2addr v1, v3

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lcom/fmm/ds/f/a;->m:Ljava/io/ByteArrayOutputStream;

    iget-object v1, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    invoke-virtual {v4}, Lcom/fmm/ds/c/as;->e()I

    move-result v4

    invoke-virtual {v0, v1, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method private n()V
    .locals 3

    const-string v0, "[xtpTpCloseInternal]"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/fmm/ds/f/a;->f()V

    invoke-virtual {p0}, Lcom/fmm/ds/f/a;->h()V

    :try_start_0
    iget-object v0, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->i:Ljava/io/InputStream;

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->j:Ljava/io/OutputStream;

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->f:Ljava/net/Socket;

    :cond_2
    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/fmm/ds/f/a;->u:Lcom/fmm/ds/f/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    iget-object v1, p0, Lcom/fmm/ds/f/a;->u:Lcom/fmm/ds/f/d;

    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLSocket;->removeHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->u:Lcom/fmm/ds/f/d;

    :cond_3
    iget-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->g:Ljavax/net/ssl/SSLSocket;

    :cond_4
    iget-object v0, p0, Lcom/fmm/ds/f/a;->t:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->t:Ljavax/net/ssl/SSLSocketFactory;

    :cond_5
    iget-object v0, p0, Lcom/fmm/ds/f/a;->h:Ljavax/net/ssl/SSLContext;

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->h:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fmm/ds/f/a;->C:Z

    :try_start_1
    iget-object v0, p0, Lcom/fmm/ds/f/a;->m:Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/fmm/ds/f/a;->m:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    const-string v0, "data/data/com.fmm.ds/httpdata.wbxml"

    iget-object v1, p0, Lcom/fmm/ds/f/a;->m:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;[B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_7
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Debug close error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private o()V
    .locals 11

    const/4 v6, 0x0

    const/4 v2, 0x0

    new-instance v7, Lcom/fmm/ds/d/p;

    invoke-direct {v7}, Lcom/fmm/ds/d/p;-><init>()V

    iget-object v0, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    new-instance v1, Lcom/fmm/ds/d/f;

    invoke-direct {v1}, Lcom/fmm/ds/d/f;-><init>()V

    aput-object v1, v0, v2

    const-string v0, "content://telephony/carriers/preferapn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lcom/fmm/ds/f/a;->G:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    if-eqz v1, :cond_7

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "No enabled APN. Use default."

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const-string v2, "apn"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const-string v3, "authtype"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    const-string v4, "user"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    const-string v5, "password"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    const-string v6, "proxy"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    const-string v8, "port"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    iget-object v9, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/fmm/ds/d/f;->a:Ljava/lang/String;

    iget-object v9, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v9, Lcom/fmm/ds/d/f;->j:Ljava/lang/String;

    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v9, 0x0

    aget-object v2, v2, v9

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/fmm/ds/d/f;->m:I

    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/ds/d/f;->k:Ljava/lang/String;

    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/ds/d/f;->l:Ljava/lang/String;

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "0.0.0.0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/fmm/ds/d/f;->d:Z

    :cond_4
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "APN name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/fmm/ds/d/a;->a(Lcom/fmm/ds/d/p;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :goto_3
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_5
    :try_start_4
    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/fmm/ds/d/f;->d:Z

    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/f/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/ds/d/f;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProxyAddress is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/fmm/ds/d/f;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;

    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v7, Lcom/fmm/ds/d/p;->a:[Lcom/fmm/ds/d/f;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const-string v3, "80"

    iput-object v3, v2, Lcom/fmm/ds/d/f;->f:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    :cond_7
    :try_start_5
    const-string v0, "cursor is null !!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v1, v6

    goto :goto_3
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/as;)I
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/f/a;->k:Ljava/lang/Thread;

    if-nez v0, :cond_0

    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    invoke-virtual {v0}, Lcom/fmm/ds/c/as;->e()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_1
    const/4 v0, -0x4

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/fmm/ds/c/as;->b()V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->o:Lcom/fmm/ds/c/as;

    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/as;->a(Lcom/fmm/ds/c/as;)Lcom/fmm/ds/c/as;

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    .locals 5

    const/4 v4, 0x0

    const/4 v1, -0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URL : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    :goto_0
    if-eq v0, v1, :cond_4

    iput-object p1, p0, Lcom/fmm/ds/f/a;->B:Ljava/lang/String;

    iget-object v2, p0, Lcom/fmm/ds/f/a;->v:Lcom/fmm/ds/d/h;

    iget-object v2, v2, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/fmm/ds/b/x;->a(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/fmm/ds/f/a;->z:I

    const-string v2, "/"

    add-int/lit8 v3, v0, 0x3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v1, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/f/a;->A:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x3

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    :goto_1
    iget-object v0, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/fmm/ds/f/a;->z:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const/16 v0, 0x50

    iput v0, p0, Lcom/fmm/ds/f/a;->x:I

    :cond_0
    :goto_2
    return v4

    :cond_1
    const-string v2, "/"

    iput-object v2, p0, Lcom/fmm/ds/f/a;->A:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x3

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/fmm/ds/f/a;->z:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x1bb

    iput v0, p0, Lcom/fmm/ds/f/a;->x:I

    goto :goto_2

    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/fmm/ds/f/a;->x:I

    iget-object v1, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "URL fail : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method public a([BI)I
    .locals 3

    const/4 v1, 0x0

    const/4 v0, -0x3

    iget-object v2, p0, Lcom/fmm/ds/f/a;->k:Ljava/lang/Thread;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x5

    if-le p2, v2, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    invoke-virtual {v0}, Lcom/fmm/ds/c/as;->b()V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->n:Lcom/fmm/ds/c/as;

    invoke-virtual {v0, p1, v1, p2}, Lcom/fmm/ds/c/as;->a([BII)Lcom/fmm/ds/c/as;

    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/fmm/ds/f/a;->a(ILjava/lang/Object;)V

    move v0, v1

    goto :goto_0
.end method

.method protected a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 5

    const/4 v4, -0x1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->d()I

    move-result v1

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v1

    iget-object v2, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    invoke-virtual {v2, v1}, Lcom/fmm/ds/c/as;->c(I)V

    iget-object v2, p0, Lcom/fmm/ds/f/a;->p:Lcom/fmm/ds/c/as;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/fmm/ds/c/as;->b(I)V

    if-ne v1, v4, :cond_0

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    const/16 v2, 0xa

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public a()V
    .locals 6

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->m:Ljava/io/ByteArrayOutputStream;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->m:Ljava/io/ByteArrayOutputStream;

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/f/a;->m:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->o()V

    iput-boolean v4, p0, Lcom/fmm/ds/f/a;->C:Z

    invoke-static {}, Lcom/fmm/ds/d/a;->c()Lcom/fmm/ds/d/h;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->v:Lcom/fmm/ds/d/h;

    invoke-static {}, Lcom/fmm/ds/d/a;->e()Lcom/fmm/ds/d/p;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->w:Lcom/fmm/ds/d/p;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->v:Lcom/fmm/ds/d/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/f/a;->v:Lcom/fmm/ds/d/h;

    iget-object v1, v0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    move-object v0, p0

    move-object v3, v2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/fmm/ds/f/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I

    iget-object v0, p0, Lcom/fmm/ds/f/a;->v:Lcom/fmm/ds/d/h;

    iget-object v0, v0, Lcom/fmm/ds/d/h;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/fmm/ds/b/x;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/fmm/ds/f/a;->z:I

    :goto_0
    return-void

    :cond_1
    const-string v0, "m_profileInfo is null"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected b(Ljava/io/InputStream;)I
    .locals 5

    const/4 v0, -0x1

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/fmm/ds/f/a;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "read data is null"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "header : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, p0, Lcom/fmm/ds/f/a;->q:I

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "HTTP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    :goto_1
    invoke-virtual {p0, p1}, Lcom/fmm/ds/f/a;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "header : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    const-string v3, "Content-Length"

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/fmm/ds/f/a;->q:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v3, "Connection"

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/fmm/ds/f/a;->r:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connection = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/fmm/ds/f/a;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string v3, "set-cookie: jsessionid="

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v2

    if-nez v2, :cond_5

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/fmm/ds/f/a;->a:Ljava/lang/String;

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0
.end method

.method public b()V
    .locals 2

    const-string v0, ""

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/f/a;->a(ILjava/lang/Object;)V

    return-void
.end method

.method public c()V
    .locals 1

    const-string v0, "xtpTpClose()"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/f/e;->b()V

    invoke-direct {p0}, Lcom/fmm/ds/f/a;->n()V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->k:Ljava/lang/Thread;

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 9

    const/4 v5, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v0, "[xtpTpMakeSSLTunneling]"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-string v0, ""

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s %s:%d %s\r\n"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "CONNECT"

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    aput-object v4, v3, v7

    iget v4, p0, Lcom/fmm/ds/f/a;->x:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    const-string v4, "HTTP/1.1"

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s:%s:%d\r\n"

    new-array v4, v5, [Ljava/lang/Object;

    const-string v5, "Host"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/fmm/ds/f/a;->y:Ljava/lang/String;

    aput-object v5, v4, v7

    iget v5, p0, Lcom/fmm/ds/f/a;->x:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s:%s\r\n"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "User-Agent"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/fmm/ds/f/a;->s:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s:%s\r\n"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "Cache-Control"

    aput-object v5, v4, v6

    const-string v5, "no-store, private"

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/fmm/ds/f/a;->C:Z

    if-eqz v1, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s:%s\r\n"

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "Connection"

    aput-object v4, v3, v6

    const-string v4, "Close"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s:%s\r\n"

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "Accept"

    aput-object v4, v3, v6

    const-string v4, "application/vnd.syncml+wbxml"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s:%s\r\n"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "Accept-Language"

    aput-object v5, v4, v6

    const-string v5, "en"

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s:%s\r\n"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "Accept-Charset"

    aput-object v5, v4, v6

    const-string v5, "UTF-8"

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s:%s\r\n"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "Content-Type"

    aput-object v5, v4, v6

    const-string v5, "application/vnd.syncml+wbxml"

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s:%s\r\n"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "Content-Length"

    aput-object v5, v4, v6

    const-string v5, "0"

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "header message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    return-object v0

    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s:%s\r\n"

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "Connection"

    aput-object v4, v3, v6

    const-string v4, "Keep-Alive"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/fmm/ds/f/a;->E:Landroid/net/wifi/WifiManager$WifiLock;

    if-nez v0, :cond_0

    const-string v0, "wifi"

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_1

    const-string v0, "wifiManager is null!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "wifilock"

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->E:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->E:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->E:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/f/a;->E:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/f/a;->E:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->E:Landroid/net/wifi/WifiManager$WifiLock;

    :cond_0
    return-void
.end method

.method public g()V
    .locals 3

    iget-object v0, p0, Lcom/fmm/ds/f/a;->F:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    const-string v0, "power"

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    if-nez v0, :cond_1

    const-string v0, "PowerManager pm is null!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    const-string v2, "DSService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->F:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/fmm/ds/f/a;->F:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iget-object v0, p0, Lcom/fmm/ds/f/a;->F:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/f/a;->F:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/f/a;->F:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/f/a;->F:Landroid/os/PowerManager$WakeLock;

    :cond_0
    return-void
.end method

.method public run()V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/fmm/ds/f/b;

    invoke-direct {v0, p0}, Lcom/fmm/ds/f/b;-><init>(Lcom/fmm/ds/f/a;)V

    iput-object v0, p0, Lcom/fmm/ds/f/a;->l:Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
