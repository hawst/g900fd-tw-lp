.class public Lcom/fmm/ds/ui/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Landroid/content/Context;


# direct methods
.method private static a(ILjava/lang/String;I)Landroid/app/Notification;
    .locals 6

    const v5, 0x7f030002

    const/4 v4, 0x0

    new-instance v0, Landroid/app/Notification;

    sget-object v1, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iput p2, v0, Landroid/app/Notification;->flags:I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    sget-object v2, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    invoke-static {v2, v4, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    sget-object v2, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    sget-object v3, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, p1, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public static a(I)V
    .locals 5

    const/high16 v4, 0x7f020000

    const/16 v3, 0x2244

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nIndicatorState : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fmm/ds/XDSApplication;->a()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    sget-object v0, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v0, "mContext is null, return"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "notification"

    invoke-static {v0}, Lcom/fmm/ds/XDSApplication;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    if-nez v0, :cond_1

    const-string v0, "NotificationManager is null"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/fmm/ds/ui/a;->a(I)V

    goto :goto_0

    :cond_2
    const-string v0, "indicator not support!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    const/high16 v2, 0x7f030000

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x20

    invoke-static {v4, v1, v2}, Lcom/fmm/ds/ui/a;->a(ILjava/lang/String;I)Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    :pswitch_2
    sget-object v1, Lcom/fmm/ds/ui/a;->a:Landroid/content/Context;

    const v2, 0x7f030001

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v4, v1, v2}, Lcom/fmm/ds/ui/a;->a(ILjava/lang/String;I)Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
