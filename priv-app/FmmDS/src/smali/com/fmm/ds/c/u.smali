.class public Lcom/fmm/ds/c/u;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/fmm/ds/c/ao;

.field public b:Lcom/fmm/ds/c/ap;

.field public c:Lcom/fmm/ds/c/aj;

.field public d:Ljava/lang/String;

.field public e:Lcom/fmm/ds/c/z;

.field public f:Lcom/fmm/ds/c/ab;

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bf;)I
    .locals 3

    const/4 v1, -0x1

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "xdsParseItem res is not ok!!"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    iget-object v1, p0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "target.pLocURI : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v2, v2, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "source : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "res  : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    :goto_2
    if-eqz v0, :cond_0

    goto :goto_0

    :sswitch_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->e()Lcom/fmm/ds/c/ao;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    goto :goto_2

    :sswitch_1
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->f()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->g()Lcom/fmm/ds/c/ap;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    goto :goto_2

    :sswitch_3
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->h()Lcom/fmm/ds/c/aj;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    goto :goto_2

    :sswitch_4
    new-instance v2, Lcom/fmm/ds/c/z;

    invoke-direct {v2}, Lcom/fmm/ds/c/z;-><init>()V

    iput-object v2, p0, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    iget-object v2, p0, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/z;->a(Lcom/fmm/ds/c/bf;)Lcom/fmm/ds/c/z;

    goto :goto_2

    :sswitch_5
    new-instance v0, Lcom/fmm/ds/c/ab;

    invoke-direct {v0}, Lcom/fmm/ds/c/ab;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v0, p0, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    invoke-virtual {v0, p1, v1}, Lcom/fmm/ds/c/ab;->a(Lcom/fmm/ds/c/bf;I)I

    move-result v0

    goto :goto_2

    :sswitch_6
    invoke-virtual {p1, v1}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v2

    iput v2, p0, Lcom/fmm/ds/c/u;->g:I

    goto :goto_2

    :sswitch_7
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    move-result v1

    iput v1, p1, Lcom/fmm/ds/c/bf;->e:I

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_7
        0xf -> :sswitch_5
        0x1a -> :sswitch_4
        0x27 -> :sswitch_1
        0x2e -> :sswitch_0
        0x34 -> :sswitch_6
        0x39 -> :sswitch_3
        0x3a -> :sswitch_2
    .end sparse-switch
.end method
