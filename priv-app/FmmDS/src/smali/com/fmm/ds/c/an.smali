.class public Lcom/fmm/ds/c/an;
.super Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Lcom/fmm/ds/c/ao;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public j:Lcom/fmm/ds/c/j;

.field public k:Lcom/fmm/ds/c/z;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bf;)I
    .locals 5

    const/4 v1, 0x2

    const/4 v2, -0x1

    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "not XDS_PARSING_OK"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "verproto : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fmm/ds/c/an;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sessionid : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fmm/ds/c/an;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "target.pLocURI : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    iget-object v1, v1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "msgid : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/fmm/ds/c/an;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "source : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fmm/ds/c/an;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "respuri : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fmm/ds/c/an;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "is_noresp : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/fmm/ds/c/an;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->c(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v1, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/an;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_2
    sparse-switch v2, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "XDS_PARSING_UNKNOWN_ELEMENT : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    const-string v1, "res not  XDS_PARSING_OK"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_0
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/an;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/an;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/an;->c:Ljava/lang/String;

    goto :goto_2

    :sswitch_3
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/fmm/ds/c/an;->d:I

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_2

    :sswitch_4
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/an;->h:Ljava/lang/String;

    goto :goto_2

    :sswitch_5
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->f()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/an;->f:Ljava/lang/String;

    goto :goto_2

    :sswitch_6
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->e()Lcom/fmm/ds/c/ao;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    goto :goto_2

    :sswitch_7
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v3

    iput v3, p0, Lcom/fmm/ds/c/an;->i:I

    goto :goto_2

    :sswitch_8
    new-instance v3, Lcom/fmm/ds/c/z;

    invoke-direct {v3}, Lcom/fmm/ds/c/z;-><init>()V

    iput-object v3, p0, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    iget-object v3, p0, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    invoke-virtual {v3, p1}, Lcom/fmm/ds/c/z;->a(Lcom/fmm/ds/c/bf;)Lcom/fmm/ds/c/z;

    goto :goto_2

    :sswitch_9
    new-instance v0, Lcom/fmm/ds/c/j;

    invoke-direct {v0}, Lcom/fmm/ds/c/j;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    iget-object v0, p0, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    invoke-virtual {v0, p1}, Lcom/fmm/ds/c/j;->a(Lcom/fmm/ds/c/bf;)I

    move-result v0

    goto :goto_2

    :sswitch_a
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    move-result v2

    iput v2, p1, Lcom/fmm/ds/c/bf;->e:I

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_a
        0xe -> :sswitch_9
        0x1a -> :sswitch_8
        0x1b -> :sswitch_3
        0x1d -> :sswitch_7
        0x21 -> :sswitch_4
        0x25 -> :sswitch_2
        0x27 -> :sswitch_5
        0x2e -> :sswitch_6
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
    .end sparse-switch
.end method
