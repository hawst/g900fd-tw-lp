.class public Lcom/fmm/ds/c/bf;
.super Lcom/fmm/ds/c/bg;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Ljava/lang/Object;

.field public g:Lcom/fmm/ds/b/i;


# direct methods
.method public constructor <init>(Lcom/fmm/ds/b/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/fmm/ds/c/bg;-><init>()V

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->a()V

    iput-object p1, p0, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    return-void
.end method

.method public static A(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    iget-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->z(Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static B(Ljava/lang/Object;)V
    .locals 2

    const/4 v1, 0x0

    check-cast p0, Lcom/fmm/ds/c/ad;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v1, p0, Lcom/fmm/ds/c/ad;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/fmm/ds/c/ad;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/fmm/ds/c/ad;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/fmm/ds/c/ad;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/ad;->f:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/ad;->f:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->k(Ljava/lang/Object;)V

    :cond_2
    iput-object v1, p0, Lcom/fmm/ds/c/ad;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/ad;->h:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/ad;->h:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->A(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static C(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    iget-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->B(Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static D(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/h;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v0, p0, Lcom/fmm/ds/c/h;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/h;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/h;->d:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/h;->d:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->C(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static E(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    iget-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->D(Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p0, Lcom/fmm/ds/c/z;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->c(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/c/z;->j:Lcom/fmm/ds/c/y;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/z;->j:Lcom/fmm/ds/c/y;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->d(Ljava/lang/Object;)V

    :cond_2
    iput-object v2, p0, Lcom/fmm/ds/c/z;->k:Ljava/lang/String;

    iput-object v2, p0, Lcom/fmm/ds/c/z;->b:Ljava/lang/String;

    iput-object v2, p0, Lcom/fmm/ds/c/z;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/z;->h:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/fmm/ds/c/z;->i:J

    iput-object v2, p0, Lcom/fmm/ds/c/z;->e:Ljava/lang/String;

    iput-object v2, p0, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    iput-object v2, p0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    iput-object v2, p0, Lcom/fmm/ds/c/z;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public static c(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/aa;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object v0, p0, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static d(Ljava/lang/Object;)V
    .locals 2

    const-wide/16 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/y;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-wide v0, p0, Lcom/fmm/ds/c/y;->b:J

    iput-wide v0, p0, Lcom/fmm/ds/c/y;->c:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/y;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static e(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    iget-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->h(Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static f(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/ap;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/ap;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static g(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/aj;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/aj;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static h(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/u;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->i(Ljava/lang/Object;)V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->j(Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->b(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->f(Ljava/lang/Object;)V

    :cond_5
    iget-object v0, p0, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->g(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static i(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/ab;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/ab;->b:[C

    iget-object v0, p0, Lcom/fmm/ds/c/ab;->d:Lcom/fmm/ds/c/aa;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/ab;->d:Lcom/fmm/ds/c/aa;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->c(Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/fmm/ds/c/ab;->e:Lcom/fmm/ds/c/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/ab;->e:Lcom/fmm/ds/c/ad;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->B(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static j(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/ao;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v0, p0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/ao;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/ao;->c:Lcom/fmm/ds/c/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/ao;->c:Lcom/fmm/ds/c/r;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->n(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static k(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    move-object p0, v0

    goto :goto_0
.end method

.method public static l(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/af;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/c/af;->a:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/af;->a:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static m(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/q;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/c/q;->a:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/q;->a:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static n(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/r;

    if-eqz p0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/r;->a:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/r;->a:Lcom/fmm/ds/c/z;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->b(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/r;->b:Lcom/fmm/ds/c/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/c/r;->b:Lcom/fmm/ds/c/q;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->m(Ljava/lang/Object;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/r;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/r;->c:Lcom/fmm/ds/c/af;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/r;->c:Lcom/fmm/ds/c/af;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->l(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public static o(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/p;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/p;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/p;->b:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/p;->b:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->k(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static p(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    iget-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->o(Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static q(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/s;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v0, p0, Lcom/fmm/ds/c/s;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/s;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/s;->c:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/s;->c:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->k(Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/fmm/ds/c/s;->d:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/s;->d:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->k(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static r(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    iget-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->q(Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static s(Ljava/lang/Object;)V
    .locals 1

    check-cast p0, Lcom/fmm/ds/c/am;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/fmm/ds/c/am;->a:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/am;->a:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->k(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static t(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/o;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object v0, p0, Lcom/fmm/ds/c/o;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/o;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public static u(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/k;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object v0, p0, Lcom/fmm/ds/c/k;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/k;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static v(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    iget-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->u(Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static w(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/l;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v0, p0, Lcom/fmm/ds/c/l;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/l;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/l;->d:Lcom/fmm/ds/c/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/l;->d:Lcom/fmm/ds/c/k;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->u(Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/fmm/ds/c/l;->e:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/fmm/ds/c/l;->e:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->v(Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lcom/fmm/ds/c/l;->f:Lcom/fmm/ds/c/k;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/fmm/ds/c/l;->f:Lcom/fmm/ds/c/k;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->u(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcom/fmm/ds/c/l;->g:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/fmm/ds/c/l;->g:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->v(Ljava/lang/Object;)V

    :cond_5
    iget-object v0, p0, Lcom/fmm/ds/c/l;->h:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/fmm/ds/c/l;->h:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->v(Ljava/lang/Object;)V

    :cond_6
    iget-object v0, p0, Lcom/fmm/ds/c/l;->i:Lcom/fmm/ds/c/o;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/fmm/ds/c/l;->i:Lcom/fmm/ds/c/o;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->t(Ljava/lang/Object;)V

    :cond_7
    iget-object v0, p0, Lcom/fmm/ds/c/l;->k:Lcom/fmm/ds/c/am;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/fmm/ds/c/l;->k:Lcom/fmm/ds/c/am;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->s(Ljava/lang/Object;)V

    :cond_8
    iget-object v0, p0, Lcom/fmm/ds/c/l;->l:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/fmm/ds/c/l;->l:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->v(Ljava/lang/Object;)V

    :cond_9
    iget-object v0, p0, Lcom/fmm/ds/c/l;->m:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/l;->m:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->r(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static x(Ljava/lang/Object;)V
    .locals 2

    check-cast p0, Lcom/fmm/ds/c/ax;

    :goto_0
    if-nez p0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/ax;->b:Lcom/fmm/ds/c/ax;

    iget-object v1, p0, Lcom/fmm/ds/c/ax;->a:Ljava/lang/Object;

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->w(Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static y(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    check-cast p0, Lcom/fmm/ds/c/n;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v0, p0, Lcom/fmm/ds/c/n;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/n;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/n;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/n;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/n;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/n;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/n;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/n;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/n;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/n;->m:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fmm/ds/c/n;->m:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->x(Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/fmm/ds/c/n;->n:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/n;->n:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->p(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static z(Ljava/lang/Object;)V
    .locals 2

    const/4 v1, 0x0

    check-cast p0, Lcom/fmm/ds/c/ac;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object v1, p0, Lcom/fmm/ds/c/ac;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/fmm/ds/c/ac;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/fmm/ds/c/ac;->c:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fmm/ds/c/ac;->c:Lcom/fmm/ds/c/ax;

    invoke-static {v0}, Lcom/fmm/ds/c/bf;->k(Ljava/lang/Object;)V

    :cond_1
    iput-object v1, p0, Lcom/fmm/ds/c/ac;->d:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/as;)I
    .locals 3

    const/4 v0, 0x2

    const/4 v1, -0x1

    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bf;->b(Lcom/fmm/ds/c/as;)V

    invoke-virtual {p0, p0}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/bf;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const/16 v2, 0x2d

    if-eq v1, v2, :cond_0

    const-string v1, "not WBXML_TAG_SYNCML"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_1
    return v0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->b()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lcom/fmm/ds/c/n;)I
    .locals 4

    const/4 v1, -0x1

    if-nez p1, :cond_0

    const/4 v0, 0x3

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->y(Ljava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->y(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->l()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->y(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    if-eqz p1, :cond_3

    invoke-static {p1}, Lcom/fmm/ds/b/i;->a(Lcom/fmm/ds/c/n;)V

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    :goto_2
    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->y(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_0
    const/16 v2, 0x25

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    const/16 v2, 0x11

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    const/16 v2, 0x15

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->c:Ljava/lang/String;

    goto :goto_2

    :sswitch_3
    const/16 v2, 0x16

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->d:Ljava/lang/String;

    goto :goto_2

    :sswitch_4
    const/16 v2, 0xf

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->e:Ljava/lang/String;

    goto :goto_2

    :sswitch_5
    const/16 v2, 0x1e

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->f:Ljava/lang/String;

    goto :goto_2

    :sswitch_6
    const/16 v2, 0x10

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->g:Ljava/lang/String;

    goto :goto_2

    :sswitch_7
    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->h:Ljava/lang/String;

    goto :goto_2

    :sswitch_8
    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->i:Ljava/lang/String;

    goto :goto_2

    :sswitch_9
    const/16 v2, 0x28

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p1, Lcom/fmm/ds/c/n;->j:J

    goto :goto_2

    :sswitch_a
    const/16 v2, 0x2a

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p1, Lcom/fmm/ds/c/n;->k:J

    goto :goto_2

    :sswitch_b
    const/16 v2, 0x29

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p1, Lcom/fmm/ds/c/n;->l:J

    goto :goto_2

    :sswitch_c
    iget-object v2, p1, Lcom/fmm/ds/c/n;->m:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->c(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->m:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_d
    iget-object v2, p1, Lcom/fmm/ds/c/n;->n:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v2

    iput-object v2, p1, Lcom/fmm/ds/c/n;->n:Lcom/fmm/ds/c/ax;

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_c
        0x9 -> :sswitch_7
        0xb -> :sswitch_8
        0xe -> :sswitch_d
        0xf -> :sswitch_4
        0x10 -> :sswitch_6
        0x11 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x1e -> :sswitch_5
        0x25 -> :sswitch_0
        0x28 -> :sswitch_9
        0x29 -> :sswitch_b
        0x2a -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Lcom/fmm/ds/c/ac;)Lcom/fmm/ds/c/ac;
    .locals 5

    const/4 v0, 0x0

    const/4 v3, -0x1

    new-instance v1, Lcom/fmm/ds/c/ac;

    invoke-direct {v1}, Lcom/fmm/ds/c/ac;-><init>()V

    const/16 v2, 0x2c

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->z(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->z(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    move-object v0, v1

    goto :goto_0

    :cond_2
    sparse-switch v3, :sswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x2

    :goto_2
    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->z(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_0
    const/16 v4, 0x17

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ac;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ac;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(I)Lcom/fmm/ds/c/ax;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ac;->c:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_3
    const/16 v4, 0xc

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ac;->d:Ljava/lang/String;

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0xc -> :sswitch_3
        0x17 -> :sswitch_0
        0x23 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/fmm/ds/c/ad;)Lcom/fmm/ds/c/ad;
    .locals 6

    const/4 v0, 0x0

    const/4 v3, -0x1

    new-instance v1, Lcom/fmm/ds/c/ad;

    invoke-direct {v1}, Lcom/fmm/ds/c/ad;-><init>()V

    const/16 v2, 0x2b

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->B(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sparse-switch v3, :sswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x2

    :goto_2
    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->B(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_0
    const/16 v4, 0x18

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ad;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ad;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    const/16 v4, 0x2d

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ad;->c:Ljava/lang/String;

    goto :goto_2

    :sswitch_3
    const/16 v4, 0x1c

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ad;->d:Ljava/lang/String;

    goto :goto_2

    :sswitch_4
    const/16 v4, 0x2e

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, v1, Lcom/fmm/ds/c/ad;->e:J

    goto :goto_2

    :sswitch_5
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(I)Lcom/fmm/ds/c/ax;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ad;->f:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_6
    const/16 v4, 0xc

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ad;->g:Ljava/lang/String;

    goto :goto_2

    :sswitch_7
    iget-object v4, v1, Lcom/fmm/ds/c/ad;->h:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v4, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/ad;->h:Lcom/fmm/ds/c/ax;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0xc -> :sswitch_6
        0x18 -> :sswitch_0
        0x1c -> :sswitch_3
        0x23 -> :sswitch_5
        0x2c -> :sswitch_7
        0x2d -> :sswitch_2
        0x2e -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/fmm/ds/c/am;)Lcom/fmm/ds/c/am;
    .locals 4

    const/4 v0, 0x0

    const/4 v2, -0x1

    if-nez p1, :cond_0

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->s(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1

    :cond_0
    const/16 v1, 0x1f

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->s(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    packed-switch v2, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v1, 0x2

    :goto_2
    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->s(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->a(I)Lcom/fmm/ds/c/ax;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/am;->a:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_0
    .end packed-switch
.end method

.method public a(I)Lcom/fmm/ds/c/ax;
    .locals 4

    const/4 v1, -0x1

    const/4 v0, 0x0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    if-eq v0, p1, :cond_0

    return-object v1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    if-nez v1, :cond_1

    new-instance v1, Lcom/fmm/ds/c/ax;

    invoke-direct {v1, v2}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v2}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;
    .locals 3

    const/4 v0, 0x0

    :goto_0
    new-instance v1, Lcom/fmm/ds/c/u;

    invoke-direct {v1}, Lcom/fmm/ds/c/u;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    const/16 v2, 0x14

    if-eq v0, v2, :cond_0

    :goto_2
    return-object p1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v1, p0}, Lcom/fmm/ds/c/u;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 p1, 0x0

    goto :goto_2

    :cond_1
    if-nez p1, :cond_2

    new-instance p1, Lcom/fmm/ds/c/ax;

    invoke-direct {p1, v1}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v1}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, -0x1

    move-object v3, v1

    move-object v0, p1

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    if-eq v2, p2, :cond_0

    :goto_2
    return-object v0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/ac;)Lcom/fmm/ds/c/ac;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->A(Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, v3}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public a(ILcom/fmm/ds/c/h;)Lcom/fmm/ds/c/h;
    .locals 6

    const/4 v0, 0x0

    new-instance v1, Lcom/fmm/ds/c/h;

    invoke-direct {v1}, Lcom/fmm/ds/c/h;-><init>()V

    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->D(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    move v3, p1

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->D(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    move-object v0, v1

    goto :goto_0

    :cond_3
    sparse-switch v3, :sswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x2

    :goto_2
    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->D(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_0
    const/4 v4, 0x6

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/h;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    const/16 v4, 0x24

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/h;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    const/16 v4, 0x33

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, v1, Lcom/fmm/ds/c/h;->c:J

    goto :goto_2

    :sswitch_3
    iget-object v4, v1, Lcom/fmm/ds/c/h;->d:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v4, v3}, Lcom/fmm/ds/c/bf;->b(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/h;->d:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x24 -> :sswitch_1
        0x2b -> :sswitch_3
        0x33 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(ILcom/fmm/ds/c/k;)Lcom/fmm/ds/c/k;
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Lcom/fmm/ds/c/k;

    invoke-direct {v1}, Lcom/fmm/ds/c/k;-><init>()V

    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->u(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    move v3, p1

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sparse-switch v3, :sswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x2

    :goto_2
    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->u(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_0
    const/4 v4, 0x6

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/k;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    const/16 v4, 0x24

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/k;->b:Ljava/lang/String;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x24 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lcom/fmm/ds/c/l;)Lcom/fmm/ds/c/l;
    .locals 5

    const/4 v0, 0x0

    const/4 v2, -0x1

    if-nez p1, :cond_0

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->w(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1

    :cond_0
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->w(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sparse-switch v2, :sswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v1, 0x2

    :goto_2
    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->w(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :sswitch_0
    const/16 v3, 0x1d

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    const/16 v3, 0xc

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    const/16 v3, 0x12

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-long v3, v3

    iput-wide v3, p1, Lcom/fmm/ds/c/l;->c:J

    goto :goto_2

    :sswitch_3
    iget-object v3, p1, Lcom/fmm/ds/c/l;->d:Lcom/fmm/ds/c/k;

    invoke-virtual {p0, v2, v3}, Lcom/fmm/ds/c/bf;->a(ILcom/fmm/ds/c/k;)Lcom/fmm/ds/c/k;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->d:Lcom/fmm/ds/c/k;

    goto :goto_2

    :sswitch_4
    iget-object v3, p1, Lcom/fmm/ds/c/l;->e:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v3, v2}, Lcom/fmm/ds/c/bf;->d(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->e:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_5
    iget-object v3, p1, Lcom/fmm/ds/c/l;->f:Lcom/fmm/ds/c/k;

    invoke-virtual {p0, v2, v3}, Lcom/fmm/ds/c/bf;->a(ILcom/fmm/ds/c/k;)Lcom/fmm/ds/c/k;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->f:Lcom/fmm/ds/c/k;

    goto :goto_2

    :sswitch_6
    iget-object v3, p1, Lcom/fmm/ds/c/l;->g:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v3, v2}, Lcom/fmm/ds/c/bf;->d(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->g:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_7
    iget-object v3, p1, Lcom/fmm/ds/c/l;->h:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v3, v2}, Lcom/fmm/ds/c/bf;->c(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->h:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_8
    iget-object v3, p1, Lcom/fmm/ds/c/l;->i:Lcom/fmm/ds/c/o;

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/o;)Lcom/fmm/ds/c/o;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->i:Lcom/fmm/ds/c/o;

    goto :goto_2

    :sswitch_9
    const/16 v3, 0x34

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v3

    int-to-long v3, v3

    iput-wide v3, p1, Lcom/fmm/ds/c/l;->j:J

    goto :goto_2

    :sswitch_a
    iget-object v3, p1, Lcom/fmm/ds/c/l;->k:Lcom/fmm/ds/c/am;

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/am;)Lcom/fmm/ds/c/am;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->k:Lcom/fmm/ds/c/am;

    goto :goto_2

    :sswitch_b
    iget-object v3, p1, Lcom/fmm/ds/c/l;->l:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v3, v2}, Lcom/fmm/ds/c/bf;->d(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->l:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_c
    iget-object v3, p1, Lcom/fmm/ds/c/l;->m:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v3, v2}, Lcom/fmm/ds/c/bf;->e(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/l;->m:Lcom/fmm/ds/c/ax;

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_7
        0xc -> :sswitch_1
        0xd -> :sswitch_8
        0x12 -> :sswitch_2
        0x19 -> :sswitch_4
        0x1a -> :sswitch_3
        0x1d -> :sswitch_0
        0x1f -> :sswitch_a
        0x21 -> :sswitch_6
        0x22 -> :sswitch_5
        0x30 -> :sswitch_b
        0x31 -> :sswitch_c
        0x34 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Lcom/fmm/ds/c/o;)Lcom/fmm/ds/c/o;
    .locals 5

    const/4 v0, 0x0

    const/4 v2, -0x1

    if-nez p1, :cond_0

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->t(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1

    :cond_0
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->t(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sparse-switch v2, :sswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v1, 0x2

    :goto_2
    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->t(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :sswitch_0
    const/16 v3, 0x1b

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v3

    int-to-long v3, v3

    iput-wide v3, p1, Lcom/fmm/ds/c/o;->a:J

    goto :goto_2

    :sswitch_1
    const/16 v3, 0x14

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/o;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    const/16 v3, 0x13

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/o;->c:Ljava/lang/String;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
        0x1b -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/fmm/ds/c/p;)Lcom/fmm/ds/c/p;
    .locals 4

    const/4 v0, 0x0

    const/4 v2, -0x1

    if-nez p1, :cond_0

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->o(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1

    :cond_0
    const/16 v1, 0xe

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->o(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->o(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    goto :goto_0

    :cond_3
    packed-switch v2, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v1, 0x2

    :goto_2
    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->o(Ljava/lang/Object;)V

    move-object p1, v0

    goto :goto_0

    :pswitch_0
    const/16 v3, 0x26

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/p;->a:Ljava/lang/String;

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->a(I)Lcom/fmm/ds/c/ax;

    move-result-object v3

    iput-object v3, p1, Lcom/fmm/ds/c/p;->b:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x26
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/fmm/ds/c/s;)Lcom/fmm/ds/c/s;
    .locals 5

    const/4 v0, 0x0

    const/4 v3, -0x1

    new-instance v1, Lcom/fmm/ds/c/s;

    invoke-direct {v1}, Lcom/fmm/ds/c/s;-><init>()V

    const/16 v2, 0x31

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->q(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sparse-switch v3, :sswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x2

    :goto_2
    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->q(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_0
    const/4 v4, 0x6

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/s;->a:Ljava/lang/String;

    goto :goto_2

    :sswitch_1
    const/16 v4, 0x24

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/s;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(I)Lcom/fmm/ds/c/ax;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/s;->c:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_3
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(I)Lcom/fmm/ds/c/ax;

    move-result-object v4

    iput-object v4, v1, Lcom/fmm/ds/c/s;->d:Lcom/fmm/ds/c/ax;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x18 -> :sswitch_3
        0x24 -> :sswitch_1
        0x32 -> :sswitch_2
    .end sparse-switch
.end method

.method public a()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/bf;->a:I

    iput v0, p0, Lcom/fmm/ds/c/bf;->b:I

    iput v0, p0, Lcom/fmm/ds/c/bf;->c:I

    iput-object v1, p0, Lcom/fmm/ds/c/bf;->d:Ljava/lang/String;

    iput v0, p0, Lcom/fmm/ds/c/bf;->e:I

    iput-object v1, p0, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->a()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/bf;->e:I

    iput-object p1, p0, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    return-void
.end method

.method public b()I
    .locals 3

    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "not XDS_PARSING_OK"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    :goto_1
    if-eqz v0, :cond_0

    goto :goto_0

    :sswitch_0
    new-instance v0, Lcom/fmm/ds/c/an;

    invoke-direct {v0}, Lcom/fmm/ds/c/an;-><init>()V

    invoke-virtual {v0, p0}, Lcom/fmm/ds/c/an;->a(Lcom/fmm/ds/c/bf;)I

    move-result v0

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->d()I

    move-result v0

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x2b -> :sswitch_1
        0x2c -> :sswitch_0
    .end sparse-switch
.end method

.method public b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;
    .locals 3

    const/4 v0, -0x1

    :goto_0
    new-instance v1, Lcom/fmm/ds/c/w;

    invoke-direct {v1}, Lcom/fmm/ds/c/w;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    const/16 v2, 0x19

    if-eq v0, v2, :cond_1

    :cond_0
    return-object p1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1, p0}, Lcom/fmm/ds/c/w;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    if-nez v2, :cond_0

    if-nez p1, :cond_2

    new-instance p1, Lcom/fmm/ds/c/ax;

    invoke-direct {p1, v1}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, p1}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public b(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, -0x1

    move-object v3, v1

    move-object v0, p1

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    if-eq v2, p2, :cond_0

    :goto_2
    return-object v0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/ad;)Lcom/fmm/ds/c/ad;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->C(Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, v3}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public b(I)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->l()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->i()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method public c()I
    .locals 5

    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    and-int/lit8 v0, v2, 0x3f

    and-int/lit8 v0, v0, 0x7f

    and-int/lit16 v2, v2, 0x80

    if-lez v2, :cond_1

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-ne v2, v1, :cond_2

    const/4 v0, 0x4

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    :goto_1
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public c(I)I
    .locals 4

    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I

    move-result v3

    and-int/lit8 v3, v3, 0x40

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    if-eqz v2, :cond_3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    :goto_1
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public c(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    move-object v0, p1

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    const/4 v4, 0x7

    if-eq v2, v4, :cond_0

    :goto_2
    return-object v0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/l;)Lcom/fmm/ds/c/l;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->x(Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, v3}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public c(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, -0x1

    move-object v3, v1

    move-object v0, p1

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    if-eq v2, p2, :cond_0

    :goto_2
    return-object v0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v2, v3}, Lcom/fmm/ds/c/bf;->a(ILcom/fmm/ds/c/h;)Lcom/fmm/ds/c/h;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->E(Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, v3}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public d()I
    .locals 6

    const/4 v1, 0x0

    const/4 v3, -0x1

    const/16 v0, 0x2b

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    return v0

    :cond_0
    move v5, v2

    move v2, v0

    move v0, v5

    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_2
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    iget-object v2, p0, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v3, p0, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v2, v3, v0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;I)V

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    sparse-switch v3, :sswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x2

    move v5, v0

    move v0, v2

    move v2, v5

    :goto_3
    if-eqz v0, :cond_0

    goto :goto_0

    :sswitch_0
    new-instance v2, Lcom/fmm/ds/c/f;

    invoke-direct {v2}, Lcom/fmm/ds/c/f;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/f;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_1
    new-instance v2, Lcom/fmm/ds/c/e;

    invoke-direct {v2}, Lcom/fmm/ds/c/e;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/e;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_2
    new-instance v2, Lcom/fmm/ds/c/t;

    invoke-direct {v2}, Lcom/fmm/ds/c/t;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/t;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_3
    new-instance v2, Lcom/fmm/ds/c/v;

    invoke-direct {v2}, Lcom/fmm/ds/c/v;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/v;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_4
    new-instance v2, Lcom/fmm/ds/c/ae;

    invoke-direct {v2}, Lcom/fmm/ds/c/ae;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/ae;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_5
    new-instance v2, Lcom/fmm/ds/c/ah;

    invoke-direct {v2}, Lcom/fmm/ds/c/ah;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/ah;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_6
    new-instance v2, Lcom/fmm/ds/c/ak;

    invoke-direct {v2}, Lcom/fmm/ds/c/ak;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/ak;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_7
    new-instance v2, Lcom/fmm/ds/c/g;

    invoke-direct {v2}, Lcom/fmm/ds/c/g;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/g;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_8
    new-instance v2, Lcom/fmm/ds/c/ai;

    invoke-direct {v2}, Lcom/fmm/ds/c/ai;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/ai;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto :goto_3

    :sswitch_9
    new-instance v2, Lcom/fmm/ds/c/al;

    invoke-direct {v2}, Lcom/fmm/ds/c/al;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/al;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto/16 :goto_3

    :sswitch_a
    new-instance v2, Lcom/fmm/ds/c/m;

    invoke-direct {v2}, Lcom/fmm/ds/c/m;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/m;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto/16 :goto_3

    :sswitch_b
    new-instance v2, Lcom/fmm/ds/c/i;

    invoke-direct {v2}, Lcom/fmm/ds/c/i;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/i;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto/16 :goto_3

    :sswitch_c
    new-instance v2, Lcom/fmm/ds/c/c;

    invoke-direct {v2}, Lcom/fmm/ds/c/c;-><init>()V

    invoke-virtual {v2, p0}, Lcom/fmm/ds/c/c;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v5, v0

    move v0, v2

    move v2, v5

    goto/16 :goto_3

    :sswitch_d
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v0

    move v5, v0

    move v0, v2

    move v2, v5

    goto/16 :goto_3

    :sswitch_e
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    move-result v3

    iput v3, p0, Lcom/fmm/ds/c/bf;->e:I

    move v5, v0

    move v0, v2

    move v2, v5

    goto/16 :goto_3

    :cond_2
    move v2, v0

    move v0, v1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_e
        0x5 -> :sswitch_1
        0x6 -> :sswitch_0
        0x8 -> :sswitch_7
        0xd -> :sswitch_b
        0x10 -> :sswitch_a
        0x11 -> :sswitch_c
        0x12 -> :sswitch_d
        0x13 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1f -> :sswitch_4
        0x22 -> :sswitch_5
        0x24 -> :sswitch_8
        0x29 -> :sswitch_6
        0x2a -> :sswitch_9
    .end sparse-switch
.end method

.method public d(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->c()I

    move-result v0

    if-eq p1, v0, :cond_0

    const-string v0, "xdsParseCheckElement is XDS_PARSING_UNKNOWN_ELEMENT"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, -0x1

    move-object v3, v1

    move-object v0, p1

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    const/16 v4, 0xe

    if-eq v2, v4, :cond_0

    :goto_2
    return-object v0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/p;)Lcom/fmm/ds/c/p;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->p(Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, v3}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public d(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    move-object v0, p1

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    if-eq v2, p2, :cond_0

    :goto_2
    return-object v0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v2, v3}, Lcom/fmm/ds/c/bf;->a(ILcom/fmm/ds/c/k;)Lcom/fmm/ds/c/k;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->v(Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, v3}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public e(I)I
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :cond_1
    return v1

    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    :try_start_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I

    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_0

    :goto_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I

    goto :goto_1

    :cond_3
    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    const/16 v3, 0x83

    if-eq v2, v3, :cond_4

    const/16 v3, 0xc3

    if-ne v2, v3, :cond_5

    :cond_4
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->i()Ljava/lang/String;

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public e()Lcom/fmm/ds/c/ao;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v2, -0x1

    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/fmm/ds/c/ao;

    invoke-direct {v1}, Lcom/fmm/ds/c/ao;-><init>()V

    :goto_1
    :sswitch_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_2
    if-ne v2, v5, :cond_1

    invoke-virtual {p0, v5}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->j(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    sparse-switch v2, :sswitch_data_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_1

    :sswitch_1
    const/16 v3, 0x17

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    goto :goto_1

    :sswitch_2
    const/16 v3, 0x16

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/fmm/ds/c/ao;->b:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_2
        0x17 -> :sswitch_1
        0x36 -> :sswitch_0
    .end sparse-switch
.end method

.method public e(Lcom/fmm/ds/c/ax;I)Lcom/fmm/ds/c/ax;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, -0x1

    move-object v3, v1

    move-object v0, p1

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    if-eq v2, p2, :cond_0

    :goto_2
    return-object v0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/s;)Lcom/fmm/ds/c/s;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {p1}, Lcom/fmm/ds/c/bf;->r(Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/fmm/ds/c/ax;

    invoke-direct {v0, v3}, Lcom/fmm/ds/c/ax;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Lcom/fmm/ds/c/ax;->a(Ljava/lang/Object;)Lcom/fmm/ds/c/ax;

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    const/16 v1, 0x27

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/16 v1, 0x17

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I

    move-result v2

    const/16 v3, 0x16

    if-ne v2, v3, :cond_2

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->e(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public g()Lcom/fmm/ds/c/ap;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v2, -0x1

    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/fmm/ds/c/ap;

    invoke-direct {v1}, Lcom/fmm/ds/c/ap;-><init>()V

    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_2
    if-ne v2, v5, :cond_1

    invoke-virtual {p0, v5}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->f(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    packed-switch v2, :pswitch_data_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_0
    const/16 v3, 0x17

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/fmm/ds/c/ap;->a:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
    .end packed-switch
.end method

.method public h()Lcom/fmm/ds/c/aj;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v2, -0x1

    const/16 v1, 0x39

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/fmm/ds/c/aj;

    invoke-direct {v1}, Lcom/fmm/ds/c/aj;-><init>()V

    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_2
    if-ne v2, v5, :cond_1

    invoke-virtual {p0, v5}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/fmm/ds/c/bf;->g(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    packed-switch v2, :pswitch_data_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_0
    const/16 v3, 0x17

    invoke-virtual {p0, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/fmm/ds/c/aj;->a:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
    .end packed-switch
.end method

.method public i()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->n()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/16 v2, 0x83

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v2, 0xc3

    if-ne v1, v2, :cond_3

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->f(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/fmm/ds/c/bf;->i:Lcom/fmm/ds/c/as;

    invoke-virtual {v2}, Lcom/fmm/ds/c/as;->c()I

    move-result v2

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bf;->e(I)I

    move-result v1

    iget-object v3, p0, Lcom/fmm/ds/c/bf;->i:Lcom/fmm/ds/c/as;

    invoke-virtual {v3, v2}, Lcom/fmm/ds/c/as;->b(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public j()Lcom/fmm/ds/c/z;
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Lcom/fmm/ds/c/z;

    invoke-direct {v1}, Lcom/fmm/ds/c/z;-><init>()V

    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1, p0}, Lcom/fmm/ds/c/z;->a(Lcom/fmm/ds/c/bf;)Lcom/fmm/ds/c/z;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method public k()I
    .locals 3

    iget-object v0, p0, Lcom/fmm/ds/c/bf;->i:Lcom/fmm/ds/c/as;

    invoke-virtual {v0}, Lcom/fmm/ds/c/as;->c()I

    move-result v0

    iget-object v1, p0, Lcom/fmm/ds/c/bf;->i:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->d()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexpected EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v1, v1, 0x3f

    and-int/lit8 v1, v1, 0x7f

    iget-object v2, p0, Lcom/fmm/ds/c/bf;->i:Lcom/fmm/ds/c/as;

    invoke-virtual {v2, v0}, Lcom/fmm/ds/c/as;->b(I)V

    return v1
.end method

.method public l()I
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->k()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bf;->q()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
