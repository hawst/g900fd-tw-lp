.class public Lcom/fmm/ds/c/z;
.super Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:I

.field public i:J

.field public j:Lcom/fmm/ds/c/y;

.field public k:Ljava/lang/String;

.field public l:Lcom/fmm/ds/c/aa;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fmm/ds/c/z;->g:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bf;)Lcom/fmm/ds/c/z;
    .locals 7

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v3, -0x1

    const/16 v1, 0x1a

    invoke-virtual {p1, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_0

    move-object p0, v0

    :goto_0
    return-object p0

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    if-ne v3, v6, :cond_1

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    move-object p0, v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_2

    move-object p0, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v6}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v1

    if-eqz v1, :cond_3

    move-object p0, v0

    goto :goto_0

    :cond_3
    iput v6, p1, Lcom/fmm/ds/c/bf;->e:I

    :cond_4
    :try_start_1
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    :goto_2
    if-ne v3, v6, :cond_5

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    goto :goto_0

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    packed-switch v3, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "XDS_PARSING_UNKNOWN_ELEMENT : "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v1, v2

    :goto_3
    if-eqz v1, :cond_4

    move-object p0, v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    move-result v3

    iput v3, p1, Lcom/fmm/ds/c/bf;->e:I

    goto :goto_3

    :pswitch_2
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    goto :goto_3

    :pswitch_3
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/ds/c/z;->b:Ljava/lang/String;

    goto :goto_3

    :pswitch_4
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/ds/c/z;->c:Ljava/lang/String;

    goto :goto_3

    :pswitch_5
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    goto :goto_3

    :pswitch_6
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/ds/c/z;->e:Ljava/lang/String;

    goto :goto_3

    :pswitch_7
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/ds/c/z;->f:Ljava/lang/String;

    goto :goto_3

    :pswitch_8
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/fmm/ds/c/z;->h:I

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_3

    :pswitch_9
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/fmm/ds/c/z;->i:J

    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_3

    :pswitch_a
    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/fmm/ds/c/z;->k:Ljava/lang/String;

    goto :goto_3

    :pswitch_b
    new-instance v4, Lcom/fmm/ds/c/y;

    invoke-direct {v4}, Lcom/fmm/ds/c/y;-><init>()V

    iput-object v4, p0, Lcom/fmm/ds/c/z;->j:Lcom/fmm/ds/c/y;

    iget-object v4, p0, Lcom/fmm/ds/c/z;->j:Lcom/fmm/ds/c/y;

    invoke-virtual {v4, p1}, Lcom/fmm/ds/c/y;->a(Lcom/fmm/ds/c/bf;)I

    goto :goto_3

    :pswitch_c
    new-instance v4, Lcom/fmm/ds/c/aa;

    invoke-direct {v4}, Lcom/fmm/ds/c/aa;-><init>()V

    iput-object v4, p0, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    iget-object v4, p0, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    invoke-virtual {v4, p1}, Lcom/fmm/ds/c/aa;->a(Lcom/fmm/ds/c/bf;)I

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_a
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method
