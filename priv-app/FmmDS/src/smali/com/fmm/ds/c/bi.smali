.class public Lcom/fmm/ds/c/bi;
.super Ljava/lang/Object;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Lcom/fmm/ds/c/an;

.field public E:Z

.field public F:Lcom/fmm/ds/c/av;

.field public G:Lcom/fmm/ds/d/u;

.field public H:I

.field public I:Z

.field public J:Z

.field public K:I

.field public L:Lcom/fmm/ds/c/d;

.field public M:Lcom/fmm/ds/c/ah;

.field public N:Lcom/fmm/ds/c/as;

.field public O:Lcom/fmm/ds/c/as;

.field public P:Lcom/fmm/ds/c/av;

.field public Q:Lcom/fmm/ds/c/av;

.field public R:Lcom/fmm/ds/c/av;

.field public S:Lcom/fmm/ds/c/av;

.field public T:Lcom/fmm/ds/c/av;

.field public U:Lcom/fmm/ds/c/av;

.field public V:Lcom/fmm/ds/c/bj;

.field public W:Lcom/fmm/ds/c/bk;

.field public X:I

.field public Y:Z

.field public a:Lcom/fmm/ds/b/a;

.field public b:Lcom/fmm/ds/c/at;

.field public c:Lcom/fmm/ds/c/bf;

.field public d:I

.field public e:J

.field public f:I

.field public g:J

.field public h:J

.field public i:I

.field public j:J

.field public k:J

.field public l:I

.field public m:I

.field public n:I

.field public o:Ljava/lang/Object;

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/fmm/ds/b/a;)V
    .locals 7

    const-wide/32 v5, 0x100000

    const-wide/16 v3, 0x1400

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/fmm/ds/c/bi;->a:Lcom/fmm/ds/b/a;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->t:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->u:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->v:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->w:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->x:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->y:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->z:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->A:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->B:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->C:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/fmm/ds/c/bi;->i:I

    iput v2, p0, Lcom/fmm/ds/c/bi;->q:I

    iput v2, p0, Lcom/fmm/ds/c/bi;->r:I

    iput v2, p0, Lcom/fmm/ds/c/bi;->s:I

    new-instance v0, Lcom/fmm/ds/c/av;

    invoke-direct {v0}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    new-instance v0, Lcom/fmm/ds/c/av;

    invoke-direct {v0}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->T:Lcom/fmm/ds/c/av;

    new-instance v0, Lcom/fmm/ds/c/av;

    invoke-direct {v0}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    new-instance v0, Lcom/fmm/ds/c/bj;

    invoke-direct {v0}, Lcom/fmm/ds/c/bj;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->V:Lcom/fmm/ds/c/bj;

    new-instance v0, Lcom/fmm/ds/c/bk;

    invoke-direct {v0}, Lcom/fmm/ds/c/bk;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->W:Lcom/fmm/ds/c/bk;

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/fmm/ds/c/av;

    invoke-direct {v0}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->P:Lcom/fmm/ds/c/av;

    new-instance v0, Lcom/fmm/ds/c/av;

    invoke-direct {v0}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->Q:Lcom/fmm/ds/c/av;

    new-instance v0, Lcom/fmm/ds/c/av;

    invoke-direct {v0}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->R:Lcom/fmm/ds/c/av;

    new-instance v0, Lcom/fmm/ds/c/av;

    invoke-direct {v0}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->M:Lcom/fmm/ds/c/ah;

    iput-boolean v1, p0, Lcom/fmm/ds/c/bi;->J:Z

    iput-boolean v1, p0, Lcom/fmm/ds/c/bi;->I:Z

    iput v1, p0, Lcom/fmm/ds/c/bi;->p:I

    new-instance v0, Lcom/fmm/ds/c/as;

    invoke-direct {v0}, Lcom/fmm/ds/c/as;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->O:Lcom/fmm/ds/c/as;

    new-instance v0, Lcom/fmm/ds/c/as;

    invoke-direct {v0}, Lcom/fmm/ds/c/as;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    iput-wide v3, p0, Lcom/fmm/ds/c/bi;->g:J

    invoke-static {}, Lcom/fmm/ds/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-wide v5, p0, Lcom/fmm/ds/c/bi;->h:J

    iput-wide v3, p0, Lcom/fmm/ds/c/bi;->k:J

    iput-wide v5, p0, Lcom/fmm/ds/c/bi;->j:J

    :cond_1
    return-void
.end method

.method public static a(Lcom/fmm/ds/c/bi;Ljava/lang/String;Z)Lcom/fmm/ds/c/d;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/fmm/ds/c/d;

    invoke-direct {v0}, Lcom/fmm/ds/c/d;-><init>()V

    iget-wide v1, p0, Lcom/fmm/ds/c/bi;->e:J

    iput-wide v1, v0, Lcom/fmm/ds/c/d;->a:J

    iget v1, p0, Lcom/fmm/ds/c/bi;->f:I

    add-int/lit8 v1, v1, -0x1

    int-to-long v1, v1

    iput-wide v1, v0, Lcom/fmm/ds/c/d;->b:J

    iput-object p1, v0, Lcom/fmm/ds/c/d;->c:Ljava/lang/String;

    iput-object v3, v0, Lcom/fmm/ds/c/d;->d:Lcom/fmm/ds/d/u;

    if-eqz p2, :cond_0

    new-instance v1, Lcom/fmm/ds/c/av;

    invoke-direct {v1}, Lcom/fmm/ds/c/av;-><init>()V

    iput-object v1, v0, Lcom/fmm/ds/c/d;->e:Lcom/fmm/ds/c/av;

    :goto_0
    iget-object v1, p0, Lcom/fmm/ds/c/bi;->T:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->L:Lcom/fmm/ds/c/d;

    return-object v0

    :cond_0
    iput-object v3, v0, Lcom/fmm/ds/c/d;->e:Lcom/fmm/ds/c/av;

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/bi;Lcom/fmm/ds/c/d;)V
    .locals 2

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->T:Lcom/fmm/ds/c/av;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/av;->a(I)V

    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/d;

    :goto_1
    if-eqz v0, :cond_0

    if-ne v0, p1, :cond_2

    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/d;

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/d;

    goto :goto_1
.end method

.method public static a(Lcom/fmm/ds/d/u;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/fmm/ds/c/x;

    invoke-direct {v0}, Lcom/fmm/ds/c/x;-><init>()V

    iput-object p1, v0, Lcom/fmm/ds/c/x;->a:Ljava/lang/String;

    iput-object p2, v0, Lcom/fmm/ds/c/x;->b:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/fmm/ds/d/u;->t:Lcom/fmm/ds/c/av;

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/av;->a(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(IILjava/lang/String;)Lcom/fmm/ds/c/d;
    .locals 6

    iget-object v1, p0, Lcom/fmm/ds/c/bi;->T:Lcom/fmm/ds/c/av;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/av;->a(I)V

    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/d;

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    iget-wide v2, v0, Lcom/fmm/ds/c/d;->a:J

    int-to-long v4, p1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, v0, Lcom/fmm/ds/c/d;->b:J

    int-to-long v4, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    invoke-virtual {v1}, Lcom/fmm/ds/c/av;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/d;

    goto :goto_0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->U:Lcom/fmm/ds/c/av;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->T:Lcom/fmm/ds/c/av;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->F:Lcom/fmm/ds/c/av;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->M:Lcom/fmm/ds/c/ah;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->D:Lcom/fmm/ds/c/an;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->O:Lcom/fmm/ds/c/as;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->N:Lcom/fmm/ds/c/as;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->P:Lcom/fmm/ds/c/av;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->Q:Lcom/fmm/ds/c/av;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->R:Lcom/fmm/ds/c/av;

    iput-object v0, p0, Lcom/fmm/ds/c/bi;->S:Lcom/fmm/ds/c/av;

    return-void
.end method
