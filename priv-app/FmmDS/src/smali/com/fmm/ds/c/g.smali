.class public Lcom/fmm/ds/c/g;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/fmm/ds/c/z;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bf;)I
    .locals 9

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_d

    move v1, v0

    :goto_0
    return v1

    :cond_0
    move v8, v2

    move v2, v0

    move v0, v8

    :goto_1
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :goto_2
    if-ne v4, v5, :cond_2

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    sparse-switch v4, :sswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "XDS_PARSING_UNKNOWN_ELEMENT : "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v2, v0

    move v0, v3

    :goto_3
    if-eqz v0, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_0
    invoke-virtual {p1, v4}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/fmm/ds/c/g;->a:I

    move v8, v0

    move v0, v2

    move v2, v8

    goto :goto_3

    :cond_3
    move v2, v0

    move v0, v3

    goto :goto_3

    :sswitch_1
    invoke-virtual {p1, v4}, Lcom/fmm/ds/c/bf;->c(I)I

    move-result v6

    iput v6, p0, Lcom/fmm/ds/c/g;->b:I

    move v8, v0

    move v0, v2

    move v2, v8

    goto :goto_3

    :sswitch_2
    new-instance v6, Lcom/fmm/ds/c/z;

    invoke-direct {v6}, Lcom/fmm/ds/c/z;-><init>()V

    iput-object v6, p0, Lcom/fmm/ds/c/g;->c:Lcom/fmm/ds/c/z;

    iget-object v6, p0, Lcom/fmm/ds/c/g;->c:Lcom/fmm/ds/c/z;

    invoke-virtual {v6, p1}, Lcom/fmm/ds/c/z;->a(Lcom/fmm/ds/c/bf;)Lcom/fmm/ds/c/z;

    move v8, v0

    move v0, v2

    move v2, v8

    goto :goto_3

    :sswitch_3
    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_4
    new-instance v2, Lcom/fmm/ds/c/e;

    invoke-direct {v2}, Lcom/fmm/ds/c/e;-><init>()V

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/e;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto :goto_3

    :sswitch_4
    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_5
    new-instance v2, Lcom/fmm/ds/c/m;

    invoke-direct {v2}, Lcom/fmm/ds/c/m;-><init>()V

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/m;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto :goto_3

    :sswitch_5
    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_6
    new-instance v2, Lcom/fmm/ds/c/c;

    invoke-direct {v2}, Lcom/fmm/ds/c/c;-><init>()V

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/c;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto :goto_3

    :sswitch_6
    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_7
    new-instance v2, Lcom/fmm/ds/c/i;

    invoke-direct {v2}, Lcom/fmm/ds/c/i;-><init>()V

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/i;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto/16 :goto_3

    :sswitch_7
    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_8
    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/g;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto/16 :goto_3

    :sswitch_8
    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_9
    new-instance v2, Lcom/fmm/ds/c/v;

    invoke-direct {v2}, Lcom/fmm/ds/c/v;-><init>()V

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/v;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto/16 :goto_3

    :sswitch_9
    if-eqz v0, :cond_a

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_a
    new-instance v2, Lcom/fmm/ds/c/ag;

    invoke-direct {v2}, Lcom/fmm/ds/c/ag;-><init>()V

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/ag;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto/16 :goto_3

    :sswitch_a
    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_b
    new-instance v2, Lcom/fmm/ds/c/ai;

    invoke-direct {v2}, Lcom/fmm/ds/c/ai;-><init>()V

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/ai;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto/16 :goto_3

    :sswitch_b
    if-eqz v0, :cond_c

    iget-object v0, p1, Lcom/fmm/ds/c/bf;->g:Lcom/fmm/ds/b/i;

    iget-object v2, p1, Lcom/fmm/ds/c/bf;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2, p0}, Lcom/fmm/ds/b/i;->a(Ljava/lang/Object;Lcom/fmm/ds/c/g;)V

    move v0, v1

    :cond_c
    new-instance v2, Lcom/fmm/ds/c/al;

    invoke-direct {v2}, Lcom/fmm/ds/c/al;-><init>()V

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/al;->a(Lcom/fmm/ds/c/bf;)I

    move-result v2

    move v8, v0

    move v0, v2

    move v2, v8

    goto/16 :goto_3

    :sswitch_c
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    move-result v4

    iput v4, p1, Lcom/fmm/ds/c/bf;->e:I

    move v8, v0

    move v0, v2

    move v2, v8

    goto/16 :goto_3

    :cond_d
    move v2, v0

    move v0, v5

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_c
        0x5 -> :sswitch_3
        0x8 -> :sswitch_7
        0xb -> :sswitch_0
        0xd -> :sswitch_6
        0x10 -> :sswitch_4
        0x11 -> :sswitch_5
        0x18 -> :sswitch_8
        0x1a -> :sswitch_2
        0x1d -> :sswitch_1
        0x20 -> :sswitch_9
        0x24 -> :sswitch_a
        0x2a -> :sswitch_b
    .end sparse-switch
.end method
