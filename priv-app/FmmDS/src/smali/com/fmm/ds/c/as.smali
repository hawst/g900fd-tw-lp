.class public Lcom/fmm/ds/c/as;
.super Ljava/lang/Object;


# instance fields
.field private a:[B

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x100

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    iput v1, p0, Lcom/fmm/ds/c/as;->b:I

    iput v2, p0, Lcom/fmm/ds/c/as;->c:I

    iput v1, p0, Lcom/fmm/ds/c/as;->d:I

    invoke-direct {p0, v2}, Lcom/fmm/ds/c/as;->e(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    iput v1, p0, Lcom/fmm/ds/c/as;->b:I

    const/16 v0, 0x100

    iput v0, p0, Lcom/fmm/ds/c/as;->c:I

    iput v1, p0, Lcom/fmm/ds/c/as;->d:I

    invoke-direct {p0, p1}, Lcom/fmm/ds/c/as;->e(I)V

    return-void
.end method

.method private e(I)V
    .locals 1

    if-eqz p1, :cond_0

    new-array v0, p1, [B

    iput-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/as;->b:I

    return-void
.end method


# virtual methods
.method public a(B)Lcom/fmm/ds/c/as;
    .locals 3

    iget v0, p0, Lcom/fmm/ds/c/as;->b:I

    iget-object v1, p0, Lcom/fmm/ds/c/as;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/fmm/ds/c/as;->c:I

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/as;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    iget v1, p0, Lcom/fmm/ds/c/as;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fmm/ds/c/as;->b:I

    aput-byte p1, v0, v1

    return-object p0
.end method

.method public a(C)Lcom/fmm/ds/c/as;
    .locals 1

    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/as;->a(B)Lcom/fmm/ds/c/as;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/fmm/ds/c/as;)Lcom/fmm/ds/c/as;
    .locals 3

    invoke-virtual {p1}, Lcom/fmm/ds/c/as;->f()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/fmm/ds/c/as;->e()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/fmm/ds/c/as;->a([BII)Lcom/fmm/ds/c/as;

    move-result-object v0

    return-object v0
.end method

.method public a([B)Lcom/fmm/ds/c/as;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/fmm/ds/c/as;->a([BII)Lcom/fmm/ds/c/as;

    move-result-object v0

    return-object v0
.end method

.method public a([BII)Lcom/fmm/ds/c/as;
    .locals 2

    iget v0, p0, Lcom/fmm/ds/c/as;->b:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/fmm/ds/c/as;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/fmm/ds/c/as;->c:I

    add-int/2addr v0, p3

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/as;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    iget v1, p0, Lcom/fmm/ds/c/as;->b:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/fmm/ds/c/as;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/fmm/ds/c/as;->b:I

    return-object p0
.end method

.method public a()V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/fmm/ds/c/as;->c:I

    iget-object v1, p0, Lcom/fmm/ds/c/as;->a:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/c/as;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    :cond_0
    new-array v0, v0, [B

    iget v1, p0, Lcom/fmm/ds/c/as;->b:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/fmm/ds/c/as;->a:[B

    iget v2, p0, Lcom/fmm/ds/c/as;->b:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    iput-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    return-void
.end method

.method public a(I)V
    .locals 1

    const/16 v0, 0x64

    if-ge p1, v0, :cond_0

    move p1, v0

    :cond_0
    iput p1, p0, Lcom/fmm/ds/c/as;->c:I

    invoke-virtual {p0}, Lcom/fmm/ds/c/as;->a()V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/as;->b:I

    iput v0, p0, Lcom/fmm/ds/c/as;->d:I

    return-void
.end method

.method public b(I)V
    .locals 1

    iget v0, p0, Lcom/fmm/ds/c/as;->b:I

    if-le p1, v0, :cond_0

    iget v0, p0, Lcom/fmm/ds/c/as;->b:I

    iput v0, p0, Lcom/fmm/ds/c/as;->d:I

    :goto_0
    return-void

    :cond_0
    if-gez p1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/as;->d:I

    goto :goto_0

    :cond_1
    iput p1, p0, Lcom/fmm/ds/c/as;->d:I

    goto :goto_0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/fmm/ds/c/as;->d:I

    return v0
.end method

.method public c(I)V
    .locals 4

    const/4 v3, 0x0

    if-gez p1, :cond_0

    iput v3, p0, Lcom/fmm/ds/c/as;->b:I

    iput v3, p0, Lcom/fmm/ds/c/as;->d:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    array-length v0, v0

    if-le p1, v0, :cond_2

    new-array v0, p1, [B

    iget v1, p0, Lcom/fmm/ds/c/as;->b:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/fmm/ds/c/as;->a:[B

    iget v2, p0, Lcom/fmm/ds/c/as;->b:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    iput-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    iput p1, p0, Lcom/fmm/ds/c/as;->b:I

    goto :goto_0

    :cond_2
    iput p1, p0, Lcom/fmm/ds/c/as;->b:I

    goto :goto_0
.end method

.method public d()I
    .locals 3

    iget v0, p0, Lcom/fmm/ds/c/as;->b:I

    iget v1, p0, Lcom/fmm/ds/c/as;->d:I

    if-gt v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    iget v1, p0, Lcom/fmm/ds/c/as;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fmm/ds/c/as;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public d(I)Lcom/fmm/ds/c/as;
    .locals 1

    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/as;->a(B)Lcom/fmm/ds/c/as;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/fmm/ds/c/as;->b:I

    return v0
.end method

.method public f()[B
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/c/as;->a:[B

    return-object v0
.end method

.method public g()[C
    .locals 3

    iget v0, p0, Lcom/fmm/ds/c/as;->b:I

    new-array v1, v0, [C

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/fmm/ds/c/as;->b:I

    if-lt v0, v2, :cond_0

    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/fmm/ds/c/as;->a:[B

    aget-byte v2, v2, v0

    int-to-char v2, v2

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
