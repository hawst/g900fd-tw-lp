.class public Lcom/fmm/ds/c/ba;
.super Ljava/lang/Object;


# static fields
.field static a:Ljava/util/Vector;

.field static b:Ljava/util/Vector;

.field public static c:Ljava/util/LinkedList;

.field public static d:Lcom/fmm/ds/c/be;

.field public static e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/fmm/ds/c/ba;->a:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/fmm/ds/c/ba;->b:Ljava/util/Vector;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    new-instance v0, Lcom/fmm/ds/c/be;

    invoke-direct {v0}, Lcom/fmm/ds/c/be;-><init>()V

    sput-object v0, Lcom/fmm/ds/c/ba;->d:Lcom/fmm/ds/c/be;

    const-string v0, ""

    sput-object v0, Lcom/fmm/ds/c/ba;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(II)Lcom/fmm/ds/c/bb;
    .locals 1

    new-instance v0, Lcom/fmm/ds/c/bb;

    invoke-direct {v0}, Lcom/fmm/ds/c/bb;-><init>()V

    iput p0, v0, Lcom/fmm/ds/c/bb;->a:I

    iput p1, v0, Lcom/fmm/ds/c/bb;->b:I

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string v0, "XEVENT_NOT_DEFINED"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "XEVENT_DS_PKG0"

    goto :goto_0

    :pswitch_1
    const-string v0, "XEVENT_DS_CANNOT_CONNECT"

    goto :goto_0

    :pswitch_2
    const-string v0, "XEVENT_DS_CONNECT"

    goto :goto_0

    :pswitch_3
    const-string v0, "XEVENT_DS_CONNECTFAIL"

    goto :goto_0

    :pswitch_4
    const-string v0, "XEVENT_DS_START"

    goto :goto_0

    :pswitch_5
    const-string v0, "XEVENT_DS_CONTINUE"

    goto :goto_0

    :pswitch_6
    const-string v0, "XEVENT_DS_ABORT"

    goto :goto_0

    :pswitch_7
    const-string v0, "XEVENT_DS_FINISH"

    goto :goto_0

    :pswitch_8
    const-string v0, "XEVENT_DS_SUSPEND"

    goto :goto_0

    :pswitch_9
    const-string v0, "XEVENT_TCPIP_OPEN"

    goto :goto_0

    :pswitch_a
    const-string v0, "XEVENT_TCPIP_SEND"

    goto :goto_0

    :pswitch_b
    const-string v0, "XEVENT_TCPIP_CLOSE"

    goto :goto_0

    :pswitch_c
    const-string v0, "XEVENT_SOCKET_CONNECTED"

    goto :goto_0

    :pswitch_d
    const-string v0, "XEVENT_SOCKET_DISCONNECTED"

    goto :goto_0

    :pswitch_e
    const-string v0, "XEVENT_SOCKET_DATA_RECEIVED"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    new-instance v0, Lcom/fmm/ds/c/bd;

    invoke-direct {v0}, Lcom/fmm/ds/c/bd;-><init>()V

    iput-object p1, v0, Lcom/fmm/ds/c/bd;->a:Ljava/lang/Object;

    iput-object v1, v0, Lcom/fmm/ds/c/bd;->b:Ljava/lang/Object;

    if-eqz p2, :cond_0

    iput-object p2, v0, Lcom/fmm/ds/c/bd;->b:Ljava/lang/Object;

    :cond_0
    :goto_0
    new-instance v3, Lcom/fmm/ds/c/bc;

    invoke-direct {v3}, Lcom/fmm/ds/c/bc;-><init>()V

    sget-object v1, Lcom/fmm/ds/b/l;->c:Landroid/os/Handler;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    const/4 v1, 0x5

    if-lt v2, v1, :cond_2

    :cond_1
    if-eqz v3, :cond_3

    :try_start_0
    iput p0, v3, Lcom/fmm/ds/c/bc;->a:I

    iput-object v0, v3, Lcom/fmm/ds/c/bc;->b:Lcom/fmm/ds/c/bd;

    sget-object v0, Lcom/fmm/ds/b/l;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget-object v1, Lcom/fmm/ds/b/l;->c:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    return-void

    :cond_2
    :try_start_1
    const-string v1, "Waiting for hTask Handler create"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-wide/16 v4, 0x1f4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_3
    sget-object v1, Lcom/fmm/ds/b/l;->c:Landroid/os/Handler;

    if-nez v1, :cond_1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    :try_start_2
    const-string v0, "Can\'t send message"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/be;)V
    .locals 3

    sget-object v1, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, p0}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v3}, Lcom/fmm/ds/b/a;->a(Z)V

    const/4 v0, 0x7

    const/16 v1, 0xfb

    invoke-static {v1, v3}, Lcom/fmm/ds/c/ba;->a(II)Lcom/fmm/ds/c/bb;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v0, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/be;

    if-eqz v0, :cond_2

    iget-object v2, v0, Lcom/fmm/ds/c/be;->a:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/fmm/ds/c/be;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;Landroid/content/SyncResult;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/fmm/ds/c/be;

    invoke-direct {v0}, Lcom/fmm/ds/c/be;-><init>()V

    iput-object p0, v0, Lcom/fmm/ds/c/be;->a:Ljava/lang/String;

    iput-object p1, v0, Lcom/fmm/ds/c/be;->b:Ljava/util/List;

    iput-object p2, v0, Lcom/fmm/ds/c/be;->c:Landroid/content/SyncResult;

    sget-object v1, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    const-string v0, "Added data to Queue"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/fmm/ds/b/a;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "xdsInSyncMode() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/b/a;->m()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "xdsGetSyncStart() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/fmm/ds/b/a;->q()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/fmm/ds/b/a;->d(Z)V

    const/4 v0, 0x3

    invoke-static {v0, v3, v3}, Lcom/fmm/ds/c/ba;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a()Z
    .locals 1

    sget-object v0, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public static b()Lcom/fmm/ds/c/be;
    .locals 1

    sget-object v0, Lcom/fmm/ds/c/ba;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/be;

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const-string v0, "XEVENT_UI_NOT_DEFINED"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "XEVENT_UI_SYNC_START"

    goto :goto_0

    :pswitch_2
    const-string v0, "XEVENT_UI_SERVER_CONNECT"

    goto :goto_0

    :pswitch_3
    const-string v0, "XEVENT_UI_SERVER_PROGRESS"

    goto :goto_0

    :pswitch_4
    const-string v0, "XEVENT_UI_PHONE_PROGRESS"

    goto :goto_0

    :pswitch_5
    const-string v0, "XEVENT_UI_SYNC_FINISH"

    goto :goto_0

    :pswitch_6
    const-string v0, "XEVENT_UI_SERVER_CONNECT_FAIL"

    goto :goto_0

    :pswitch_7
    const-string v0, "XEVENT_UI_SERVER_DNS_FAIL"

    goto :goto_0

    :pswitch_8
    const-string v0, "XEVENT_UI_NETWORK_BUSY"

    goto :goto_0

    :pswitch_9
    const-string v0, "XEVENT_UI_SERVER_BUSY"

    goto :goto_0

    :pswitch_a
    const-string v0, "XEVENT_UI_INVALID_CREDENTIALS"

    goto :goto_0

    :pswitch_b
    const-string v0, "XEVENT_UI_MISSING_CREDENTIALS"

    goto :goto_0

    :pswitch_c
    const-string v0, "XEVENT_UI_PAYMENT_REQIRED"

    goto :goto_0

    :pswitch_d
    const-string v0, "XEVENT_UI_FORBIDDEN_SESSION"

    goto :goto_0

    :pswitch_e
    const-string v0, "XEVENT_UI_SERVER_NOT_SUPPORTED"

    goto :goto_0

    :pswitch_f
    const-string v0, "XEVENT_UI_NETWORK_ERR"

    goto :goto_0

    :pswitch_10
    const-string v0, "XEVENT_UI_ABORT_BYUSER"

    goto :goto_0

    :pswitch_11
    const-string v0, "XEVENT_UI_RECV_FAIL"

    goto :goto_0

    :pswitch_12
    const-string v0, "XEVENT_UI_SEND_FAIL"

    goto :goto_0

    :pswitch_13
    const-string v0, "XEVENT_UI_HTTP_INTERNAL_ERROR"

    goto :goto_0

    :pswitch_14
    const-string v0, "XEVENT_UI_WBXML_ERROR"

    goto :goto_0

    :pswitch_15
    const-string v0, "XEVENT_UI_UNKNOWN_ERROR"

    goto :goto_0

    :pswitch_16
    const-string v0, "XEVENT_UI_SYNCHRONIZATION_ERROR"

    goto :goto_0

    :pswitch_17
    const-string v0, "XEVENT_UI_DEVICE_FULL_ERROR"

    goto :goto_0

    :pswitch_18
    const-string v0, "XEVENT_UI_ONEWAYSYNC_NOTSUPPORTED"

    goto :goto_0

    :pswitch_19
    const-string v0, "XEVENT_UI_DATASTORE_ERROR"

    goto :goto_0

    :pswitch_1a
    const-string v0, "XEVENT_UI_NOTI_NOT_SPECIFIED"

    goto :goto_0

    :pswitch_1b
    const-string v0, "XEVENT_UI_NOTI_BACKGROUND"

    goto :goto_0

    :pswitch_1c
    const-string v0, "XEVENT_UI_NOTI_INFORMATIVE"

    goto :goto_0

    :pswitch_1d
    const-string v0, "XEVENT_UI_NOTI_INTERACTIVE"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public static b(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    new-instance v0, Lcom/fmm/ds/c/bd;

    invoke-direct {v0}, Lcom/fmm/ds/c/bd;-><init>()V

    iput-object p1, v0, Lcom/fmm/ds/c/bd;->a:Ljava/lang/Object;

    iput-object v1, v0, Lcom/fmm/ds/c/bd;->b:Ljava/lang/Object;

    if-eqz p2, :cond_0

    iput-object p2, v0, Lcom/fmm/ds/c/bd;->b:Ljava/lang/Object;

    :cond_0
    :goto_0
    new-instance v3, Lcom/fmm/ds/c/bc;

    invoke-direct {v3}, Lcom/fmm/ds/c/bc;-><init>()V

    sget-object v1, Lcom/fmm/ds/b/o;->b:Landroid/os/Handler;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    const/4 v1, 0x5

    if-lt v2, v1, :cond_2

    :cond_1
    if-eqz v3, :cond_3

    :try_start_0
    iput p0, v3, Lcom/fmm/ds/c/bc;->a:I

    iput-object v0, v3, Lcom/fmm/ds/c/bc;->b:Lcom/fmm/ds/c/bd;

    sget-object v0, Lcom/fmm/ds/b/o;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget-object v1, Lcom/fmm/ds/b/o;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    return-void

    :cond_2
    :try_start_1
    const-string v1, "Waiting for hUITask Handler create"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const-wide/16 v4, 0x1f4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_3
    sget-object v1, Lcom/fmm/ds/b/o;->b:Landroid/os/Handler;

    if-nez v1, :cond_1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    :try_start_2
    const-string v0, "Can\'t send message"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static b(Lcom/fmm/ds/c/be;)V
    .locals 0

    sput-object p0, Lcom/fmm/ds/c/ba;->d:Lcom/fmm/ds/c/be;

    return-void
.end method

.method public static c()Lcom/fmm/ds/c/be;
    .locals 1

    sget-object v0, Lcom/fmm/ds/c/ba;->d:Lcom/fmm/ds/c/be;

    return-object v0
.end method

.method public static c(I)Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/fmm/ds/c/ba;->c()Lcom/fmm/ds/c/be;

    move-result-object v1

    iget-object v1, v1, Lcom/fmm/ds/c/be;->b:Ljava/util/List;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static d()V
    .locals 5

    sget-object v0, Lcom/fmm/ds/c/ba;->d:Lcom/fmm/ds/c/be;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fmm/ds/c/ba;->d:Lcom/fmm/ds/c/be;

    iget-object v0, v0, Lcom/fmm/ds/c/be;->c:Landroid/content/SyncResult;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fmm/ds/c/ba;->d:Lcom/fmm/ds/c/be;

    iget-object v0, v0, Lcom/fmm/ds/c/be;->c:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    :cond_0
    return-void
.end method
