.class public Lcom/fmm/ds/c/bh;
.super Ljava/lang/Object;


# instance fields
.field protected a:Lcom/fmm/ds/c/as;

.field b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/bh;->b:I

    return-void
.end method


# virtual methods
.method public a(IZ)Z
    .locals 1

    if-eqz p2, :cond_0

    or-int/lit8 p1, p1, 0x40

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bh;->c(I)Z

    move-result v0

    return v0
.end method

.method public a([CI)Z
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0xc3

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bh;->c(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p2}, Lcom/fmm/ds/c/bh;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    if-lt v0, p2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    aget-char v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/fmm/ds/c/as;->a(C)Lcom/fmm/ds/c/as;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public b(Lcom/fmm/ds/c/as;)V
    .locals 1

    iput-object p1, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    iget-object v0, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    invoke-virtual {v0}, Lcom/fmm/ds/c/as;->b()V

    return-void
.end method

.method public b(I)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/bh;->c(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bh;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(IILjava/lang/String;I)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bh;->c(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bh;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_2

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/bh;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {p0, p2}, Lcom/fmm/ds/c/bh;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p4}, Lcom/fmm/ds/c/bh;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p3}, Lcom/fmm/ds/c/bh;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(I)Z
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    invoke-virtual {v0, p1}, Lcom/fmm/ds/c/as;->d(I)Lcom/fmm/ds/c/as;

    const/4 v0, 0x1

    return v0
.end method

.method public d(I)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x5

    new-array v3, v0, [B

    move v0, v1

    :goto_0
    add-int/lit8 v2, v0, 0x1

    and-int/lit8 v4, p1, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    shr-int/lit8 p1, p1, 0x7

    if-nez p1, :cond_1

    :goto_1
    if-gt v2, v5, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    aget-byte v1, v3, v1

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/as;->a(B)Lcom/fmm/ds/c/as;

    return v5

    :cond_0
    iget-object v0, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    add-int/lit8 v2, v2, -0x1

    aget-byte v4, v3, v2

    or-int/lit16 v4, v4, 0x80

    invoke-virtual {v0, v4}, Lcom/fmm/ds/c/as;->d(I)Lcom/fmm/ds/c/as;

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/bh;->c(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/bh;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    invoke-virtual {v1, v0}, Lcom/fmm/ds/c/as;->d(I)Lcom/fmm/ds/c/as;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/as;->a([B)Lcom/fmm/ds/c/as;

    const/4 v0, 0x1

    return v0
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    invoke-virtual {v0}, Lcom/fmm/ds/c/as;->e()I

    move-result v0

    iput v0, p0, Lcom/fmm/ds/c/bh;->b:I

    const/4 v0, 0x1

    return v0
.end method

.method public i()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/fmm/ds/c/bh;->b:I

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/fmm/ds/c/bh;->b:I

    iget-object v2, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    invoke-virtual {v2}, Lcom/fmm/ds/c/as;->e()I

    move-result v2

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/fmm/ds/c/bh;->a:Lcom/fmm/ds/c/as;

    iget v2, p0, Lcom/fmm/ds/c/bh;->b:I

    invoke-virtual {v1, v2}, Lcom/fmm/ds/c/as;->c(I)V

    iput v0, p0, Lcom/fmm/ds/c/bh;->b:I

    const-string v0, "*******CAREFUL*** NOT TEST xdsEncReset by DK"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/bh;->c(I)Z

    move-result v0

    return v0
.end method
