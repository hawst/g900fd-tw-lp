.class public Lcom/fmm/ds/c/bg;
.super Ljava/lang/Object;


# instance fields
.field public h:Ljava/lang/String;

.field protected i:Lcom/fmm/ds/c/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/bg;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/fmm/ds/c/bg;->i:Lcom/fmm/ds/c/as;

    return-void
.end method


# virtual methods
.method a(Lcom/fmm/ds/c/bf;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->q()I

    move-result v0

    iput v0, p1, Lcom/fmm/ds/c/bf;->a:I

    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->p()I

    move-result v0

    iput v0, p1, Lcom/fmm/ds/c/bf;->b:I

    iget v0, p1, Lcom/fmm/ds/c/bf;->b:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->p()I

    :cond_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->p()I

    move-result v0

    iput v0, p1, Lcom/fmm/ds/c/bf;->c:I

    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/fmm/ds/c/bf;->d:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/fmm/ds/c/bf;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/fmm/ds/c/bg;->h:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Lcom/fmm/ds/c/as;)V
    .locals 2

    iput-object p1, p0, Lcom/fmm/ds/c/bg;->i:Lcom/fmm/ds/c/as;

    iget-object v0, p0, Lcom/fmm/ds/c/bg;->i:Lcom/fmm/ds/c/as;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fmm/ds/c/as;->b(I)V

    return-void
.end method

.method public f(I)Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0xc3

    if-ne p1, v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->p()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_1

    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->q()I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public m()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->p()I

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/fmm/ds/c/bg;->h:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/fmm/ds/c/bg;->h:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public n()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    iget-object v1, p0, Lcom/fmm/ds/c/bg;->i:Lcom/fmm/ds/c/as;

    invoke-virtual {v1}, Lcom/fmm/ds/c/as;->d()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexpected EOF wbxdec_parse_str_i"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public o()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->p()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->q()I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public p()I
    .locals 3

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {p0}, Lcom/fmm/ds/c/bg;->q()I

    move-result v1

    shl-int/lit8 v0, v0, 0x7

    and-int/lit8 v2, v1, 0x7f

    or-int/2addr v0, v2

    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_0

    return v0
.end method

.method public q()I
    .locals 2

    iget-object v0, p0, Lcom/fmm/ds/c/bg;->i:Lcom/fmm/ds/c/as;

    invoke-virtual {v0}, Lcom/fmm/ds/c/as;->d()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexpected EOF wbxdec_buffer_read_byte"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return v0
.end method
