.class public Lcom/fmm/ds/c/ah;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/fmm/ds/c/z;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/fmm/ds/c/ax;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bf;)I
    .locals 4

    const/4 v1, 0x2

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sparse-switch v2, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "XDS_PARSING_UNKNOWN_ELEMENT : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    goto :goto_0

    :sswitch_0
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/fmm/ds/c/ah;->a:I

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_2

    :sswitch_1
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/ah;->b:Ljava/lang/String;

    goto :goto_2

    :sswitch_2
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/ah;->c:Ljava/lang/String;

    goto :goto_2

    :sswitch_3
    new-instance v2, Lcom/fmm/ds/c/z;

    invoke-direct {v2}, Lcom/fmm/ds/c/z;-><init>()V

    iput-object v2, p0, Lcom/fmm/ds/c/ah;->d:Lcom/fmm/ds/c/z;

    iget-object v2, p0, Lcom/fmm/ds/c/ah;->d:Lcom/fmm/ds/c/z;

    invoke-virtual {v2, p1}, Lcom/fmm/ds/c/z;->a(Lcom/fmm/ds/c/bf;)Lcom/fmm/ds/c/z;

    goto :goto_2

    :sswitch_4
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/ah;->e:Ljava/lang/String;

    goto :goto_2

    :sswitch_5
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/ah;->f:Ljava/lang/String;

    goto :goto_2

    :sswitch_6
    iget-object v2, p0, Lcom/fmm/ds/c/ah;->g:Lcom/fmm/ds/c/ax;

    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/ah;->g:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_7
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    move-result v2

    iput v2, p1, Lcom/fmm/ds/c/bf;->e:I

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_7
        0xb -> :sswitch_0
        0xc -> :sswitch_2
        0x14 -> :sswitch_6
        0x1a -> :sswitch_3
        0x1c -> :sswitch_1
        0x28 -> :sswitch_5
        0x2f -> :sswitch_4
    .end sparse-switch
.end method
