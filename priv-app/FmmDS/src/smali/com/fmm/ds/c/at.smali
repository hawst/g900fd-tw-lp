.class public Lcom/fmm/ds/c/at;
.super Lcom/fmm/ds/c/bh;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/fmm/ds/c/bh;-><init>()V

    return-void
.end method

.method public static a(Lcom/fmm/ds/c/at;)I
    .locals 1

    iget-object v0, p0, Lcom/fmm/ds/c/at;->a:Lcom/fmm/ds/c/as;

    invoke-virtual {v0}, Lcom/fmm/ds/c/as;->e()I

    move-result v0

    return v0
.end method

.method public static a(Lcom/fmm/ds/b/e;)Lcom/fmm/ds/c/as;
    .locals 8

    const/16 v7, 0x6a

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    new-instance v1, Lcom/fmm/ds/c/as;

    invoke-direct {v1}, Lcom/fmm/ds/c/as;-><init>()V

    if-nez p0, :cond_1

    const-string v1, "xdsEncDevinf2Opaque devinf null"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v4, Lcom/fmm/ds/c/at;

    invoke-direct {v4}, Lcom/fmm/ds/c/at;-><init>()V

    invoke-virtual {v4, v1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/as;)V

    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "-//SYNCML//DTD DevInf 1.1//EN"

    const-string v5, "-//SYNCML//DTD DevInf 1.1//EN"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v2, v7, v3, v5}, Lcom/fmm/ds/c/at;->b(IILjava/lang/String;I)Z

    move-result v3

    :goto_1
    if-nez v3, :cond_3

    const-string v1, "xdsEncDevinf2Opaque return null"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v3, "-//SYNCML//DTD DevInf 1.2//EN"

    const-string v5, "-//SYNCML//DTD DevInf 1.2//EN"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v2, v7, v3, v5}, Lcom/fmm/ds/c/at;->b(IILjava/lang/String;I)Z

    move-result v3

    goto :goto_1

    :cond_3
    const/16 v3, 0xa

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    const/16 v3, 0x25

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_4
    iget-object v3, p0, Lcom/fmm/ds/b/e;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const/16 v3, 0x11

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->b:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_5
    iget-object v3, p0, Lcom/fmm/ds/b/e;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    const/16 v3, 0x15

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->c:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_6
    iget-object v3, p0, Lcom/fmm/ds/b/e;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    const/16 v3, 0x16

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->d:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_7
    iget-object v3, p0, Lcom/fmm/ds/b/e;->e:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const/16 v3, 0xf

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->e:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_8
    iget-object v3, p0, Lcom/fmm/ds/b/e;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    const/16 v3, 0x1e

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->f:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_9
    iget-object v3, p0, Lcom/fmm/ds/b/e;->g:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    const/16 v3, 0x10

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->g:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_a
    iget-object v3, p0, Lcom/fmm/ds/b/e;->h:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    const/16 v3, 0x9

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->h:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_b
    iget-object v3, p0, Lcom/fmm/ds/b/e;->i:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const/16 v3, 0xb

    invoke-virtual {v4, v3, v6}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/fmm/ds/b/e;->i:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_c
    iget-boolean v3, p0, Lcom/fmm/ds/b/e;->l:Z

    if-eqz v3, :cond_d

    const/16 v3, 0x29

    invoke-virtual {v4, v3, v2}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_d
    iget-boolean v3, p0, Lcom/fmm/ds/b/e;->k:Z

    if-eqz v3, :cond_e

    const/16 v3, 0x2a

    invoke-virtual {v4, v3, v2}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_e
    iget v3, p0, Lcom/fmm/ds/b/e;->j:I

    if-eqz v3, :cond_f

    const/16 v3, 0x28

    invoke-virtual {v4, v3, v2}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_f
    move v3, v2

    :goto_2
    if-lt v3, v6, :cond_12

    :cond_10
    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v2

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    invoke-static {v4, v2}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;[Lcom/fmm/ds/b/f;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_11
    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Lcom/fmm/ds/c/at;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto/16 :goto_0

    :cond_12
    invoke-static {}, Lcom/fmm/ds/a/a;->a()I

    move-result v2

    if-nez v2, :cond_13

    iget-object v2, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v2, v2, v3

    invoke-static {v4, v2}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/g;)Z

    move-result v2

    :goto_3
    if-eqz v2, :cond_0

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_13
    if-nez v3, :cond_14

    invoke-static {}, Lcom/fmm/ds/d/a;->d()I

    move-result v2

    if-nez v2, :cond_10

    :cond_14
    iget-object v2, p0, Lcom/fmm/ds/b/e;->m:[Lcom/fmm/ds/b/g;

    aget-object v2, v2, v3

    iget-object v5, p0, Lcom/fmm/ds/b/e;->n:[Lcom/fmm/ds/b/f;

    aget-object v5, v5, v3

    invoke-static {v4, v2, v5}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/g;Lcom/fmm/ds/b/f;)Z

    move-result v2

    goto :goto_3
.end method

.method private a(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/fmm/ds/c/at;->e(I)I

    invoke-direct {p0, p2}, Lcom/fmm/ds/c/at;->g(Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    return-void
.end method

.method public static a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/f;)Z
    .locals 4

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/b/f;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x6

    iget-object v1, p1, Lcom/fmm/ds/b/f;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/b/f;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x24

    iget-object v1, p1, Lcom/fmm/ds/b/f;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    iget v1, p1, Lcom/fmm/ds/b/f;->c:I

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_2

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x1

    return v0

    :cond_2
    iget-object v2, p1, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p1, Lcom/fmm/ds/b/f;->d:[I

    aget v2, v2, v0

    packed-switch v2, :pswitch_data_0

    const-string v2, "not support type"

    invoke-static {v2}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    const/16 v2, 0x2b

    invoke-direct {p0, v2}, Lcom/fmm/ds/c/at;->e(I)I

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    goto :goto_1

    :pswitch_2
    const/16 v2, 0x18

    iget-object v3, p1, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_3
    const/16 v2, 0x23

    iget-object v3, p1, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_4
    const/16 v2, 0x2c

    invoke-direct {p0, v2}, Lcom/fmm/ds/c/at;->e(I)I

    goto :goto_1

    :pswitch_5
    const/16 v2, 0x17

    iget-object v3, p1, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_6
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    goto :goto_1

    :pswitch_7
    const/16 v2, 0x8

    iget-object v3, p1, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_8
    const/16 v2, 0x1c

    iget-object v3, p1, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_9
    const/16 v2, 0x2d

    iget-object v3, p1, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_a
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/at;->a(I)I

    goto :goto_1

    :pswitch_b
    const/16 v2, 0xc

    iget-object v3, p1, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_6
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/g;)Z
    .locals 2

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/b/g;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x1d

    iget-object v1, p1, Lcom/fmm/ds/b/g;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/b/g;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0xc

    iget-object v1, p1, Lcom/fmm/ds/b/g;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_3
    const/16 v0, 0x12

    iget v1, p1, Lcom/fmm/ds/b/g;->c:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lcom/fmm/ds/b/g;->e:Lcom/fmm/ds/b/h;

    invoke-static {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/b/g;->g:Lcom/fmm/ds/b/h;

    invoke-static {p0, v0}, Lcom/fmm/ds/c/at;->b(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/b/g;->d:Lcom/fmm/ds/b/h;

    invoke-static {p0, v0}, Lcom/fmm/ds/c/at;->c(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/b/g;->f:Lcom/fmm/ds/b/h;

    invoke-static {p0, v0}, Lcom/fmm/ds/c/at;->d(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/b/g;->h:[I

    invoke-static {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;[I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/g;Lcom/fmm/ds/b/f;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v1, p1, Lcom/fmm/ds/b/g;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x1d

    iget-object v2, p1, Lcom/fmm/ds/b/g;->a:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v1, p1, Lcom/fmm/ds/b/g;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/fmm/ds/b/g;->b:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    const/16 v1, 0x12

    iget v2, p1, Lcom/fmm/ds/b/g;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lcom/fmm/ds/b/g;->e:Lcom/fmm/ds/b/h;

    invoke-static {p0, v1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v1, p1, Lcom/fmm/ds/b/g;->g:Lcom/fmm/ds/b/h;

    invoke-static {p0, v1}, Lcom/fmm/ds/c/at;->b(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v1, p1, Lcom/fmm/ds/b/g;->d:Lcom/fmm/ds/b/h;

    invoke-static {p0, v1}, Lcom/fmm/ds/c/at;->c(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z

    move-result v1

    if-nez v1, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v1, p1, Lcom/fmm/ds/b/g;->f:Lcom/fmm/ds/b/h;

    invoke-static {p0, v1}, Lcom/fmm/ds/c/at;->d(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z

    move-result v1

    if-nez v1, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v1, p1, Lcom/fmm/ds/b/g;->h:[I

    invoke-static {p0, v1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;[I)Z

    move-result v1

    if-nez v1, :cond_9

    move v0, v1

    goto :goto_0

    :cond_7
    invoke-static {p0, p2}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/f;)Z

    move-result v1

    if-nez v1, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    add-int/lit8 v0, v0, 0x1

    :cond_9
    iget v1, p1, Lcom/fmm/ds/b/g;->i:I

    if-lt v0, v1, :cond_7

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x6

    iget-object v1, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x24

    iget-object v1, p1, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/at;[I)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x1f

    invoke-direct {p0, v1}, Lcom/fmm/ds/c/at;->e(I)I

    :goto_1
    const/16 v1, 0x8

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    aget v1, p1, v0

    if-lez v1, :cond_2

    aget v1, p1, v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x20

    invoke-direct {p0, v2, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static a(Lcom/fmm/ds/c/at;[Lcom/fmm/ds/b/f;)Z
    .locals 6

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    move v0, v1

    :goto_1
    array-length v2, p1

    if-lt v0, v2, :cond_1

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    aget-object v2, p1, v0

    iget-object v2, v2, Lcom/fmm/ds/b/f;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x6

    aget-object v3, p1, v0

    iget-object v3, v3, Lcom/fmm/ds/b/f;->b:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    aget-object v2, p1, v0

    iget v3, v2, Lcom/fmm/ds/b/f;->c:I

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_2

    aget-object v4, p1, v0

    iget-object v4, v4, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    aget-object v4, p1, v0

    iget-object v4, v4, Lcom/fmm/ds/b/f;->d:[I

    aget v4, v4, v2

    packed-switch v4, :pswitch_data_0

    const-string v4, "not support type"

    invoke-static {v4}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :pswitch_0
    const/16 v4, 0x18

    aget-object v5, p1, v0

    iget-object v5, v5, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_3

    :pswitch_1
    const/16 v4, 0x23

    aget-object v5, p1, v0

    iget-object v5, v5, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_3

    :pswitch_2
    const/16 v4, 0x17

    aget-object v5, p1, v0

    iget-object v5, v5, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_3

    :pswitch_3
    const/16 v4, 0x8

    aget-object v5, p1, v0

    iget-object v5, v5, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_3

    :pswitch_4
    const/16 v4, 0x1c

    aget-object v5, p1, v0

    iget-object v5, v5, Lcom/fmm/ds/b/f;->e:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x6

    iget-object v1, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x24

    iget-object v1, p1, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x6

    iget-object v1, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x24

    iget-object v1, p1, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(Lcom/fmm/ds/c/at;Lcom/fmm/ds/b/h;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x6

    iget-object v1, p1, Lcom/fmm/ds/b/h;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x24

    iget-object v1, p1, Lcom/fmm/ds/b/h;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e(I)I
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/at;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()I
    .locals 1

    invoke-virtual {p0}, Lcom/fmm/ds/c/at;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/fmm/ds/c/at;->k()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/fmm/ds/c/at;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/fmm/ds/c/at;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x5

    :cond_0
    return v0
.end method

.method public a(IILjava/lang/String;I)I
    .locals 1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fmm/ds/c/at;->b(IILjava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2d

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/aa;)I
    .locals 2

    const/4 v0, 0x6

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0xa

    iget-object v1, p1, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0xf

    iget-object v1, p1, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_4
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/ae;)I
    .locals 3

    const-string v0, "xdsEncAddPut"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, Lcom/fmm/ds/c/ae;->f:Lcom/fmm/ds/c/ax;

    const/16 v0, 0x1f

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/ae;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v2, p1, Lcom/fmm/ds/c/ae;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/ae;->d:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/ae;->d:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    iget v0, p1, Lcom/fmm/ds/c/ae;->b:I

    if-lez v0, :cond_4

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_4
    iget v0, p1, Lcom/fmm/ds/c/ae;->c:I

    if-lez v0, :cond_5

    const/16 v0, 0x15

    iget v2, p1, Lcom/fmm/ds/c/ae;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/ae;->e:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/ae;->e:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    invoke-static {v1}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/u;

    :goto_1
    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0

    :cond_7
    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/u;)I

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    goto :goto_0

    :cond_8
    invoke-static {v1}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/u;

    goto :goto_1
.end method

.method public a(Lcom/fmm/ds/c/ag;)I
    .locals 2

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/ag;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v1, p1, Lcom/fmm/ds/c/ag;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget v0, p1, Lcom/fmm/ds/c/ag;->b:I

    if-lez v0, :cond_3

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/ag;->c:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/fmm/ds/c/ag;->c:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/ag;->d:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/ag;->d:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/ah;)I
    .locals 2

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/ah;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v1, p1, Lcom/fmm/ds/c/ah;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/ah;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x1c

    iget-object v1, p1, Lcom/fmm/ds/c/ah;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/ah;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0xc

    iget-object v1, p1, Lcom/fmm/ds/c/ah;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/ah;->d:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/ah;->d:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/ah;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x2f

    iget-object v1, p1, Lcom/fmm/ds/c/ah;->e:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_6
    iget-object v0, p1, Lcom/fmm/ds/c/ah;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x28

    iget-object v1, p1, Lcom/fmm/ds/c/ah;->f:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_7
    iget-object v0, p1, Lcom/fmm/ds/c/ah;->g:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_8

    const-string v0, "results.itemlist:"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/fmm/ds/c/ah;->g:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ax;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_8
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/ak;)I
    .locals 2

    const/4 v0, 0x6

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Lcom/fmm/ds/c/ak;->a:I

    if-ltz v1, :cond_0

    iget-object v1, p1, Lcom/fmm/ds/c/ak;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v0, 0x29

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/ak;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v1, p1, Lcom/fmm/ds/c/ak;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x1c

    iget-object v1, p1, Lcom/fmm/ds/c/ak;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0xc

    iget-object v1, p1, Lcom/fmm/ds/c/ak;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0xa

    iget-object v1, p1, Lcom/fmm/ds/c/ak;->d:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/ak;->e:Lcom/fmm/ds/c/ax;

    const/16 v1, 0x2f

    invoke-virtual {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ax;I)I

    :cond_6
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/fmm/ds/c/ak;->f:Lcom/fmm/ds/c/ax;

    const/16 v1, 0x28

    invoke-virtual {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ax;I)I

    :cond_7
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->g:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/fmm/ds/c/ak;->g:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_8
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->h:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_9

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/c/ak;->h:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    :cond_9
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    const/16 v0, 0xf

    iget-object v1, p1, Lcom/fmm/ds/c/ak;->i:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_a
    iget-object v0, p1, Lcom/fmm/ds/c/ak;->j:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/fmm/ds/c/ak;->j:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ax;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_b
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public a(Lcom/fmm/ds/c/al;)I
    .locals 2

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x2a

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/al;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v1, p1, Lcom/fmm/ds/c/al;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/al;->d:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/al;->d:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    iget-boolean v0, p1, Lcom/fmm/ds/c/al;->b:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_4
    iget-boolean v0, p1, Lcom/fmm/ds/c/al;->c:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/al;->h:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/al;->h:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    iget-object v0, p1, Lcom/fmm/ds/c/al;->e:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/fmm/ds/c/al;->e:Lcom/fmm/ds/c/ao;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ao;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_7
    iget-object v0, p1, Lcom/fmm/ds/c/al;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p1, Lcom/fmm/ds/c/al;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->b(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_8
    iget v0, p1, Lcom/fmm/ds/c/al;->g:I

    if-ltz v0, :cond_9

    const/16 v0, 0x33

    iget v1, p1, Lcom/fmm/ds/c/al;->g:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/an;)I
    .locals 2

    const/16 v0, 0x2c

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/c/an;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x31

    iget-object v1, p1, Lcom/fmm/ds/c/an;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/c/an;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x32

    iget-object v1, p1, Lcom/fmm/ds/c/an;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/c/an;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x25

    iget-object v1, p1, Lcom/fmm/ds/c/an;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget v0, p1, Lcom/fmm/ds/c/an;->d:I

    if-lez v0, :cond_3

    const/16 v0, 0x1b

    iget v1, p1, Lcom/fmm/ds/c/an;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/an;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0x21

    iget-object v1, p1, Lcom/fmm/ds/c/an;->h:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_4
    iget v0, p1, Lcom/fmm/ds/c/an;->i:I

    if-lez v0, :cond_5

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/fmm/ds/c/an;->e:Lcom/fmm/ds/c/ao;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ao;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    :goto_0
    return v0

    :cond_7
    iget-object v0, p1, Lcom/fmm/ds/c/an;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p1, Lcom/fmm/ds/c/an;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p1, Lcom/fmm/ds/c/an;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->b(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    :cond_8
    iget-object v0, p1, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    iget-object v0, v0, Lcom/fmm/ds/c/j;->a:Lcom/fmm/ds/c/z;

    if-nez v0, :cond_9

    iget-object v0, p1, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    iget-object v0, v0, Lcom/fmm/ds/c/j;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    :cond_9
    iget-object v0, p1, Lcom/fmm/ds/c/an;->j:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_6

    :cond_a
    iget-object v0, p1, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/fmm/ds/c/an;->k:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_6

    :cond_b
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0

    :cond_c
    iget-object v0, p1, Lcom/fmm/ds/c/an;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/fmm/ds/c/an;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/ao;)I
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2e

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x17

    iget-object v1, p1, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/fmm/ds/c/ao;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x16

    iget-object v1, p1, Lcom/fmm/ds/c/ao;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/ax;)I
    .locals 1

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/u;

    :goto_1
    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/u;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object p1

    invoke-static {p1}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/u;

    goto :goto_1
.end method

.method public a(Lcom/fmm/ds/c/ax;I)I
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2, v0}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    invoke-static {p1}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object p1

    invoke-static {p1}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Lcom/fmm/ds/c/e;)I
    .locals 2

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/e;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v1, p1, Lcom/fmm/ds/c/e;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget v0, p1, Lcom/fmm/ds/c/e;->b:I

    if-lez v0, :cond_3

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/e;->c:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/fmm/ds/c/e;->c:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/e;->d:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/e;->d:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/f;)I
    .locals 2

    const/4 v0, 0x6

    const-string v1, "xdsEncAddAlert"

    invoke-static {v1}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Lcom/fmm/ds/c/f;->a:I

    if-ltz v1, :cond_0

    iget-object v1, p1, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/f;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v1, p1, Lcom/fmm/ds/c/f;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget v0, p1, Lcom/fmm/ds/c/f;->c:I

    if-lez v0, :cond_3

    const/16 v0, 0x3c

    iget v1, p1, Lcom/fmm/ds/c/f;->c:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0xf

    iget-object v1, p1, Lcom/fmm/ds/c/f;->e:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/f;->d:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/f;->d:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    iget v0, p1, Lcom/fmm/ds/c/f;->b:I

    if-lez v0, :cond_6

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_6
    iget-object v0, p1, Lcom/fmm/ds/c/f;->f:Lcom/fmm/ds/c/ax;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/fmm/ds/c/f;->f:Lcom/fmm/ds/c/ax;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ax;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_7
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/j;)I
    .locals 2

    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/c/j;->a:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/c/j;->a:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/fmm/ds/c/j;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0xf

    iget-object v1, p1, Lcom/fmm/ds/c/j;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/m;)I
    .locals 2

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/m;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v1, p1, Lcom/fmm/ds/c/m;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget v0, p1, Lcom/fmm/ds/c/m;->b:I

    if-lez v0, :cond_3

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_3
    iget v0, p1, Lcom/fmm/ds/c/m;->c:I

    if-lez v0, :cond_4

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_4
    iget v0, p1, Lcom/fmm/ds/c/m;->d:I

    if-lez v0, :cond_5

    const/16 v0, 0x26

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/m;->e:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/m;->e:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    iget-object v0, p1, Lcom/fmm/ds/c/m;->f:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/fmm/ds/c/m;->f:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/t;)I
    .locals 3

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, Lcom/fmm/ds/c/t;->f:Lcom/fmm/ds/c/ax;

    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/t;->a:I

    if-lez v0, :cond_2

    const/16 v0, 0xb

    iget v2, p1, Lcom/fmm/ds/c/t;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/t;->d:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/t;->d:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    iget v0, p1, Lcom/fmm/ds/c/t;->b:I

    if-lez v0, :cond_4

    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_4
    iget v0, p1, Lcom/fmm/ds/c/t;->c:I

    if-lez v0, :cond_5

    const/16 v0, 0x15

    iget v2, p1, Lcom/fmm/ds/c/t;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/t;->e:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/t;->e:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    invoke-static {v1}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/u;

    :goto_1
    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0

    :cond_7
    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/u;)I

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    goto :goto_0

    :cond_8
    invoke-static {v1}, Lcom/fmm/ds/c/ax;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v1

    invoke-static {v1}, Lcom/fmm/ds/c/ax;->a(Lcom/fmm/ds/c/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fmm/ds/c/u;

    goto :goto_1
.end method

.method public a(Lcom/fmm/ds/c/u;)I
    .locals 6

    const/4 v2, 0x0

    const/16 v5, 0xf

    const/4 v1, 0x5

    const/4 v4, 0x1

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    iget-object v0, v0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/fmm/ds/c/u;->a:Lcom/fmm/ds/c/ao;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ao;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    iget-object v0, v0, Lcom/fmm/ds/c/ap;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/u;->b:Lcom/fmm/ds/c/ap;

    iget-object v0, v0, Lcom/fmm/ds/c/ap;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/fmm/ds/c/u;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->b(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    iget-object v0, v0, Lcom/fmm/ds/c/aj;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/u;->c:Lcom/fmm/ds/c/aj;

    iget-object v0, v0, Lcom/fmm/ds/c/aj;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->c(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/u;->e:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    iget-object v0, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v0, v0, Lcom/fmm/ds/c/ab;->a:I

    if-nez v0, :cond_9

    iget-object v0, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ab;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v5, v0}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_7
    :goto_1
    iget v0, p1, Lcom/fmm/ds/c/u;->g:I

    if-ne v0, v4, :cond_8

    const/16 v0, 0x34

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_8
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    move v0, v2

    goto/16 :goto_0

    :cond_9
    iget-object v0, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v0, v0, Lcom/fmm/ds/c/ab;->a:I

    if-ne v0, v4, :cond_b

    invoke-direct {p0, v5}, Lcom/fmm/ds/c/at;->e(I)I

    :try_start_0
    iget-object v0, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v0, v0, Lcom/fmm/ds/c/ab;->b:[C

    iget-object v3, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v3, v3, Lcom/fmm/ds/c/ab;->c:I

    invoke-virtual {p0, v0, v3}, Lcom/fmm/ds/c/at;->a([CI)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :cond_a
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    goto :goto_1

    :cond_b
    iget-object v0, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget v0, v0, Lcom/fmm/ds/c/ab;->a:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_7

    invoke-direct {p0, v5}, Lcom/fmm/ds/c/at;->e(I)I

    invoke-virtual {p0, v4}, Lcom/fmm/ds/c/at;->b(I)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    goto/16 :goto_0

    :cond_c
    iget-object v0, p1, Lcom/fmm/ds/c/u;->f:Lcom/fmm/ds/c/ab;

    iget-object v0, v0, Lcom/fmm/ds/c/ab;->d:Lcom/fmm/ds/c/aa;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/aa;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/at;->b(I)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    goto/16 :goto_0

    :cond_d
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    goto :goto_1
.end method

.method public a(Lcom/fmm/ds/c/v;)I
    .locals 2

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget v0, p1, Lcom/fmm/ds/c/v;->a:I

    if-ltz v0, :cond_2

    const/16 v0, 0xb

    iget v1, p1, Lcom/fmm/ds/c/v;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/v;->d:Lcom/fmm/ds/c/j;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/v;->d:Lcom/fmm/ds/c/j;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/j;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    iget-object v0, p1, Lcom/fmm/ds/c/v;->e:Lcom/fmm/ds/c/z;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/fmm/ds/c/v;->e:Lcom/fmm/ds/c/z;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/z;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/v;->b:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/fmm/ds/c/v;->b:Lcom/fmm/ds/c/ao;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ao;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    iget-object v0, p1, Lcom/fmm/ds/c/v;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p1, Lcom/fmm/ds/c/v;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->b(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/w;)I
    .locals 1

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-object v0, p1, Lcom/fmm/ds/c/w;->a:Lcom/fmm/ds/c/ao;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/fmm/ds/c/w;->a:Lcom/fmm/ds/c/ao;

    iget-object v0, v0, Lcom/fmm/ds/c/ao;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/fmm/ds/c/w;->a:Lcom/fmm/ds/c/ao;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/ao;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-object v0, p1, Lcom/fmm/ds/c/w;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/fmm/ds/c/w;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->b(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/y;)I
    .locals 5

    const/4 v0, 0x6

    const-wide/16 v3, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p1, Lcom/fmm/ds/c/y;->b:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_2

    iget-wide v1, p1, Lcom/fmm/ds/c/y;->c:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_2

    iget-object v1, p1, Lcom/fmm/ds/c/y;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    iget-wide v0, p1, Lcom/fmm/ds/c/y;->b:J

    cmp-long v0, v0, v3

    if-ltz v0, :cond_3

    const/16 v0, 0x9

    iget-wide v1, p1, Lcom/fmm/ds/c/y;->b:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_3
    iget-wide v0, p1, Lcom/fmm/ds/c/y;->c:J

    cmp-long v0, v0, v3

    if-ltz v0, :cond_4

    const/16 v0, 0x8

    iget-wide v1, p1, Lcom/fmm/ds/c/y;->c:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p1, Lcom/fmm/ds/c/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0x11

    iget-object v1, p1, Lcom/fmm/ds/c/y;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_5
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/z;)I
    .locals 7

    const/4 v0, 0x5

    const/4 v2, 0x0

    if-nez p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x1a

    invoke-direct {p0, v1}, Lcom/fmm/ds/c/at;->e(I)I

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/at;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/fmm/ds/c/z;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x7

    iget-object v3, p1, Lcom/fmm/ds/c/z;->b:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v1, p1, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v1, 0x13

    iget-object v3, p1, Lcom/fmm/ds/c/z;->a:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v1, p1, Lcom/fmm/ds/c/z;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const/16 v1, 0xb

    iget-object v3, p1, Lcom/fmm/ds/c/z;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v1, p1, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const/16 v1, 0x12

    iget-object v3, p1, Lcom/fmm/ds/c/z;->d:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v1, p1, Lcom/fmm/ds/c/z;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/16 v1, 0x14

    iget-object v3, p1, Lcom/fmm/ds/c/z;->f:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_6
    iget-object v1, p1, Lcom/fmm/ds/c/z;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0x10

    iget-object v3, p1, Lcom/fmm/ds/c/z;->e:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_7
    iget v1, p1, Lcom/fmm/ds/c/z;->h:I

    if-lez v1, :cond_8

    const/16 v1, 0xc

    iget v3, p1, Lcom/fmm/ds/c/z;->h:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_8
    iget-wide v3, p1, Lcom/fmm/ds/c/z;->i:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-lez v1, :cond_9

    const/16 v1, 0x15

    iget-wide v3, p1, Lcom/fmm/ds/c/z;->i:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_9
    iget-object v1, p1, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    if-eqz v1, :cond_a

    iget-object v1, p1, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    iget-object v1, v1, Lcom/fmm/ds/c/aa;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    iget-object v1, v1, Lcom/fmm/ds/c/aa;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/fmm/ds/c/z;->l:Lcom/fmm/ds/c/aa;

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/aa;)I

    move-result v1

    if-eqz v1, :cond_a

    move v0, v1

    goto/16 :goto_0

    :cond_a
    iget-object v1, p1, Lcom/fmm/ds/c/z;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p1, Lcom/fmm/ds/c/z;->k:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/at;->d(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_b

    move v0, v1

    goto/16 :goto_0

    :cond_b
    iget-object v1, p1, Lcom/fmm/ds/c/z;->j:Lcom/fmm/ds/c/y;

    if-eqz v1, :cond_c

    iget-object v1, p1, Lcom/fmm/ds/c/z;->j:Lcom/fmm/ds/c/y;

    invoke-virtual {p0, v1}, Lcom/fmm/ds/c/at;->a(Lcom/fmm/ds/c/y;)I

    move-result v1

    if-eqz v1, :cond_c

    move v0, v1

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0, v2}, Lcom/fmm/ds/c/at;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    move v0, v2

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)I
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3a

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x17

    invoke-direct {p0, v0, p1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    const/16 v0, 0x17

    invoke-direct {p0, v0, p1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    const/16 v0, 0x16

    invoke-direct {p0, v0, p2}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)I
    .locals 1

    if-eqz p1, :cond_0

    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/at;->a(I)I

    :cond_0
    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/fmm/ds/c/ab;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget v1, p1, Lcom/fmm/ds/c/ab;->a:I

    if-nez v1, :cond_0

    iget-object v0, p1, Lcom/fmm/ds/c/ab;->b:[C

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/fmm/ds/c/as;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/fmm/ds/c/at;->b(Lcom/fmm/ds/c/as;)V

    return-void
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x2b

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/String;)I
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    const/16 v0, 0x17

    invoke-direct {p0, v0, p1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    return v0
.end method

.method public c(Ljava/lang/String;)I
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x39

    invoke-direct {p0, v0}, Lcom/fmm/ds/c/at;->e(I)I

    const/16 v0, 0x17

    invoke-direct {p0, v0, p1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    return v0
.end method

.method public d(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/fmm/ds/c/at;->a(ILjava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    return v0
.end method

.method public f()I
    .locals 1

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    return v0
.end method

.method public g()I
    .locals 1

    invoke-direct {p0}, Lcom/fmm/ds/c/at;->l()I

    const/4 v0, 0x0

    return v0
.end method
