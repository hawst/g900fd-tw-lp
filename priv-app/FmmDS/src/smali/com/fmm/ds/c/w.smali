.class public Lcom/fmm/ds/c/w;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/fmm/ds/c/ao;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bf;)I
    .locals 3

    const/4 v1, -0x1

    const/16 v0, 0x19

    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "XDS_PARSING_UNKNOWN_ELEMENT="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    :goto_2
    if-eqz v0, :cond_0

    goto :goto_0

    :sswitch_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->e()Lcom/fmm/ds/c/ao;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/w;->a:Lcom/fmm/ds/c/ao;

    goto :goto_2

    :sswitch_1
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->f()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/fmm/ds/c/w;->b:Ljava/lang/String;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_1
        0x2e -> :sswitch_0
    .end sparse-switch
.end method
