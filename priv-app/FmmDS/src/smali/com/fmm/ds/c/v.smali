.class public Lcom/fmm/ds/c/v;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:Lcom/fmm/ds/c/ao;

.field public c:Ljava/lang/String;

.field public d:Lcom/fmm/ds/c/j;

.field public e:Lcom/fmm/ds/c/z;

.field public f:Lcom/fmm/ds/c/ax;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bf;)I
    .locals 5

    const/4 v1, 0x2

    const/4 v2, -0x1

    const/16 v0, 0x18

    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p0}, Lcom/fmm/ds/c/v;->a()V

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sparse-switch v2, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "XDS_PARSING_UNKNOWN_ELEMENT : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_2
    if-eqz v0, :cond_1

    goto :goto_0

    :sswitch_0
    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/fmm/ds/c/v;->a:I

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_2

    :sswitch_1
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->e()Lcom/fmm/ds/c/ao;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/v;->b:Lcom/fmm/ds/c/ao;

    goto :goto_2

    :sswitch_2
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->f()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/v;->c:Ljava/lang/String;

    goto :goto_2

    :sswitch_3
    iget-object v0, p0, Lcom/fmm/ds/c/v;->d:Lcom/fmm/ds/c/j;

    if-nez v0, :cond_4

    new-instance v0, Lcom/fmm/ds/c/j;

    invoke-direct {v0}, Lcom/fmm/ds/c/j;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/v;->d:Lcom/fmm/ds/c/j;

    :cond_4
    iget-object v0, p0, Lcom/fmm/ds/c/v;->d:Lcom/fmm/ds/c/j;

    invoke-virtual {v0, p1}, Lcom/fmm/ds/c/j;->a(Lcom/fmm/ds/c/bf;)I

    move-result v0

    goto :goto_2

    :sswitch_4
    iget-object v3, p0, Lcom/fmm/ds/c/v;->e:Lcom/fmm/ds/c/z;

    if-nez v3, :cond_5

    new-instance v3, Lcom/fmm/ds/c/z;

    invoke-direct {v3}, Lcom/fmm/ds/c/z;-><init>()V

    iput-object v3, p0, Lcom/fmm/ds/c/v;->e:Lcom/fmm/ds/c/z;

    :cond_5
    iget-object v3, p0, Lcom/fmm/ds/c/v;->e:Lcom/fmm/ds/c/z;

    invoke-virtual {v3, p1}, Lcom/fmm/ds/c/z;->a(Lcom/fmm/ds/c/bf;)Lcom/fmm/ds/c/z;

    goto :goto_2

    :sswitch_5
    iget-object v3, p0, Lcom/fmm/ds/c/v;->f:Lcom/fmm/ds/c/ax;

    invoke-virtual {p1, v3}, Lcom/fmm/ds/c/bf;->b(Lcom/fmm/ds/c/ax;)Lcom/fmm/ds/c/ax;

    move-result-object v3

    iput-object v3, p0, Lcom/fmm/ds/c/v;->f:Lcom/fmm/ds/c/ax;

    goto :goto_2

    :sswitch_6
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    move-result v2

    if-nez v0, :cond_0

    iput v2, p1, Lcom/fmm/ds/c/bf;->e:I

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_6
        0xb -> :sswitch_0
        0xe -> :sswitch_3
        0x19 -> :sswitch_5
        0x1a -> :sswitch_4
        0x27 -> :sswitch_2
        0x2e -> :sswitch_1
    .end sparse-switch
.end method

.method public a()V
    .locals 0

    return-void
.end method
