.class public Lcom/fmm/ds/c/av;
.super Ljava/lang/Object;


# instance fields
.field public a:Lcom/fmm/ds/c/aw;

.field public b:Lcom/fmm/ds/c/aw;

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/fmm/ds/c/av;->b()Lcom/fmm/ds/c/aw;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "xdsCreateNodeFromMemory Fail !!!"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput-object v0, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iput-object v0, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iput-object v0, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/fmm/ds/c/av;->c:J

    iput-object v0, p0, Lcom/fmm/ds/c/av;->b:Lcom/fmm/ds/c/aw;

    goto :goto_0
.end method

.method public static a(Lcom/fmm/ds/c/aw;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/aw;->a:Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public static a(Lcom/fmm/ds/c/aw;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/fmm/ds/c/aw;->a:Ljava/lang/Object;

    iput-object p1, p0, Lcom/fmm/ds/c/aw;->a:Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public static b()Lcom/fmm/ds/c/aw;
    .locals 1

    new-instance v0, Lcom/fmm/ds/c/aw;

    invoke-direct {v0}, Lcom/fmm/ds/c/aw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(J)Ljava/lang/Object;
    .locals 8

    const-wide/16 v6, 0x1

    const-wide/16 v4, 0x0

    iget-object v0, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    iget-wide v1, p0, Lcom/fmm/ds/c/av;->c:J

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    cmp-long v1, p1, v4

    if-gez v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    move-wide p1, v1

    :cond_2
    sub-long v1, p1, v6

    cmp-long v3, p1, v4

    if-gez v3, :cond_1

    iget-object v1, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-object v2, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iput-object v2, v1, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iget-object v1, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iget-object v2, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iput-object v2, v1, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    invoke-static {v0}, Lcom/fmm/ds/c/av;->a(Lcom/fmm/ds/c/aw;)Ljava/lang/Object;

    move-result-object v0

    iget-wide v1, p0, Lcom/fmm/ds/c/av;->c:J

    sub-long/2addr v1, v6

    iput-wide v1, p0, Lcom/fmm/ds/c/av;->c:J

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 5

    iget-object v2, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    iget-object v1, v2, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    const/4 v0, 0x0

    :goto_0
    if-ne v1, v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    iget-object v3, v1, Lcom/fmm/ds/c/aw;->a:Ljava/lang/Object;

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, v1, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-object v2, v1, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iput-object v2, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iget-object v0, v1, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iget-object v2, v1, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iput-object v2, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    invoke-static {v1}, Lcom/fmm/ds/c/av;->a(Lcom/fmm/ds/c/aw;)Ljava/lang/Object;

    move-result-object v0

    iget-wide v1, p0, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/fmm/ds/c/av;->c:J

    goto :goto_1

    :cond_1
    iget-object v1, v1, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    goto :goto_0
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    iget-object v1, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    iput-object v1, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iget-object v0, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    iget-object v1, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    iput-object v1, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-object v0, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    iput-object v0, p0, Lcom/fmm/ds/c/av;->b:Lcom/fmm/ds/c/aw;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/fmm/ds/c/av;->c:J

    return-void
.end method

.method public a(I)V
    .locals 5

    iget-object v0, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    if-ltz p1, :cond_0

    int-to-long v1, p1

    iget-wide v3, p0, Lcom/fmm/ds/c/av;->c:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    :goto_0
    add-int/lit8 v1, p1, -0x1

    if-gez p1, :cond_1

    :cond_0
    iput-object v0, p0, Lcom/fmm/ds/c/av;->b:Lcom/fmm/ds/c/aw;

    return-void

    :cond_1
    iget-object v0, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    move p1, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 4

    iget-object v0, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    new-instance v1, Lcom/fmm/ds/c/aw;

    invoke-direct {v1}, Lcom/fmm/ds/c/aw;-><init>()V

    invoke-static {v1, p1}, Lcom/fmm/ds/c/av;->a(Lcom/fmm/ds/c/aw;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_0

    iput-object v0, v1, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iget-object v2, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iput-object v2, v1, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-object v2, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iput-object v1, v2, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iput-object v1, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-wide v0, p0, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/fmm/ds/c/av;->c:J

    :cond_0
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 2

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/fmm/ds/c/av;->a(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/fmm/ds/c/av;->b:Lcom/fmm/ds/c/aw;

    iget-object v1, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iput-object v0, p0, Lcom/fmm/ds/c/av;->b:Lcom/fmm/ds/c/aw;

    iget-object v0, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-object v0, v0, Lcom/fmm/ds/c/aw;->a:Ljava/lang/Object;

    goto :goto_0
.end method

.method public e()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/fmm/ds/c/av;->b:Lcom/fmm/ds/c/aw;

    iget-object v1, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-object v2, p0, Lcom/fmm/ds/c/av;->a:Lcom/fmm/ds/c/aw;

    if-eq v1, v2, :cond_0

    iget-object v0, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-object v1, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-object v2, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iput-object v2, v1, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iget-object v1, v0, Lcom/fmm/ds/c/aw;->b:Lcom/fmm/ds/c/aw;

    iget-object v2, v0, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iput-object v2, v1, Lcom/fmm/ds/c/aw;->c:Lcom/fmm/ds/c/aw;

    iget-wide v1, p0, Lcom/fmm/ds/c/av;->c:J

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/fmm/ds/c/av;->c:J

    invoke-static {v0}, Lcom/fmm/ds/c/av;->a(Lcom/fmm/ds/c/aw;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
