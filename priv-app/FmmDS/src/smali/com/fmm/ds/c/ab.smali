.class public Lcom/fmm/ds/c/ab;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:[C

.field public c:I

.field public d:Lcom/fmm/ds/c/aa;

.field public e:Lcom/fmm/ds/c/ad;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/ab;->e:Lcom/fmm/ds/c/ad;

    return-void
.end method


# virtual methods
.method public a(Lcom/fmm/ds/c/bf;I)I
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x1

    const-string v0, "xdsParsePcdata"

    invoke-static {v0}, Lcom/fmm/ds/b/c;->a(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->q()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/ab;->a(Ljava/lang/String;)V

    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/bf;->d(I)I

    move-result v0

    if-nez v0, :cond_0

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v2, 0x83

    if-ne v0, v2, :cond_3

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/ab;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fmm/ds/b/c;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const/16 v2, 0xc3

    if-ne v0, v2, :cond_4

    :try_start_1
    invoke-virtual {p1, v0}, Lcom/fmm/ds/c/bf;->f(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    iput v2, p0, Lcom/fmm/ds/c/ab;->a:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iput v2, p0, Lcom/fmm/ds/c/ab;->c:I

    invoke-virtual {p0, v0}, Lcom/fmm/ds/c/ab;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    if-nez v0, :cond_a

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    move-result v0

    iput v0, p1, Lcom/fmm/ds/c/bf;->e:I

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I

    move-result v0

    :cond_5
    iget v2, p1, Lcom/fmm/ds/c/bf;->e:I

    if-ne v2, v3, :cond_7

    const/4 v2, 0x5

    if-ne v0, v2, :cond_7

    new-instance v0, Lcom/fmm/ds/c/aa;

    invoke-direct {v0}, Lcom/fmm/ds/c/aa;-><init>()V

    iput-object v0, p0, Lcom/fmm/ds/c/ab;->d:Lcom/fmm/ds/c/aa;

    iget-object v0, p0, Lcom/fmm/ds/c/ab;->d:Lcom/fmm/ds/c/aa;

    invoke-virtual {v0, p1}, Lcom/fmm/ds/c/aa;->a(Lcom/fmm/ds/c/bf;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    :goto_3
    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->k()I

    move-result v0

    if-ne v0, v3, :cond_5

    goto :goto_1

    :cond_7
    iget v2, p1, Lcom/fmm/ds/c/bf;->e:I

    if-ne v2, v4, :cond_8

    const/16 v2, 0xa

    if-ne v0, v2, :cond_8

    new-instance v2, Lcom/fmm/ds/c/n;

    invoke-direct {v2}, Lcom/fmm/ds/c/n;-><init>()V

    invoke-virtual {p1, v2}, Lcom/fmm/ds/c/bf;->a(Lcom/fmm/ds/c/n;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, Lcom/fmm/ds/c/bf;->y(Ljava/lang/Object;)V

    goto :goto_3

    :cond_8
    iget v2, p1, Lcom/fmm/ds/c/bf;->e:I

    if-ne v2, v3, :cond_9

    const/16 v2, 0xd

    if-ne v0, v2, :cond_9

    new-instance v0, Lcom/fmm/ds/c/y;

    invoke-direct {v0}, Lcom/fmm/ds/c/y;-><init>()V

    invoke-virtual {v0, p1}, Lcom/fmm/ds/c/y;->a(Lcom/fmm/ds/c/bf;)I

    move-result v0

    if-eqz v0, :cond_6

    goto/16 :goto_0

    :cond_9
    if-nez v0, :cond_6

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    invoke-virtual {p1}, Lcom/fmm/ds/c/bf;->c()I

    goto :goto_3

    :cond_a
    invoke-virtual {p1, p2}, Lcom/fmm/ds/c/bf;->e(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/fmm/ds/c/ab;->a:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/ab;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fmm/ds/c/ab;->b:[C
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/fmm/ds/c/ab;->a:I

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v0, v0

    iput v0, p0, Lcom/fmm/ds/c/ab;->c:I

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/fmm/ds/c/ab;->b:[C

    return-void
.end method
