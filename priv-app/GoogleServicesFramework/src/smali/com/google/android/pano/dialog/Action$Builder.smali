.class public Lcom/google/android/pano/dialog/Action$Builder;
.super Ljava/lang/Object;
.source "Action.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/pano/dialog/Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCheckSetId:I

.field private mChecked:Z

.field private mDescription:Ljava/lang/String;

.field private mDrawableResource:I

.field private mEnabled:Z

.field private mHasNext:Z

.field private mIconUri:Landroid/net/Uri;

.field private mInfoOnly:Z

.field private mIntent:Landroid/content/Intent;

.field private mKey:Ljava/lang/String;

.field private mMultilineDescription:Z

.field private mResourcePackageName:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput v0, p0, Lcom/google/android/pano/dialog/Action$Builder;->mDrawableResource:I

    .line 62
    iput v0, p0, Lcom/google/android/pano/dialog/Action$Builder;->mCheckSetId:I

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/pano/dialog/Action$Builder;->mEnabled:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/pano/dialog/Action;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/pano/dialog/Action;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/pano/dialog/Action;-><init>(Lcom/google/android/pano/dialog/Action$1;)V

    .line 67
    .local v0, "action":Lcom/google/android/pano/dialog/Action;
    iget-object v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mKey:Ljava/lang/String;

    # setter for: Lcom/google/android/pano/dialog/Action;->mKey:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$102(Lcom/google/android/pano/dialog/Action;Ljava/lang/String;)Ljava/lang/String;

    .line 68
    iget-object v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mTitle:Ljava/lang/String;

    # setter for: Lcom/google/android/pano/dialog/Action;->mTitle:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$202(Lcom/google/android/pano/dialog/Action;Ljava/lang/String;)Ljava/lang/String;

    .line 69
    iget-object v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mDescription:Ljava/lang/String;

    # setter for: Lcom/google/android/pano/dialog/Action;->mDescription:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$302(Lcom/google/android/pano/dialog/Action;Ljava/lang/String;)Ljava/lang/String;

    .line 70
    iget-object v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mIntent:Landroid/content/Intent;

    # setter for: Lcom/google/android/pano/dialog/Action;->mIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$402(Lcom/google/android/pano/dialog/Action;Landroid/content/Intent;)Landroid/content/Intent;

    .line 71
    iget-object v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mResourcePackageName:Ljava/lang/String;

    # setter for: Lcom/google/android/pano/dialog/Action;->mResourcePackageName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$502(Lcom/google/android/pano/dialog/Action;Ljava/lang/String;)Ljava/lang/String;

    .line 72
    iget v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mDrawableResource:I

    # setter for: Lcom/google/android/pano/dialog/Action;->mDrawableResource:I
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$602(Lcom/google/android/pano/dialog/Action;I)I

    .line 73
    iget-object v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mIconUri:Landroid/net/Uri;

    # setter for: Lcom/google/android/pano/dialog/Action;->mIconUri:Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$702(Lcom/google/android/pano/dialog/Action;Landroid/net/Uri;)Landroid/net/Uri;

    .line 74
    iget-boolean v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mChecked:Z

    # setter for: Lcom/google/android/pano/dialog/Action;->mChecked:Z
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$802(Lcom/google/android/pano/dialog/Action;Z)Z

    .line 75
    iget-boolean v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mMultilineDescription:Z

    # setter for: Lcom/google/android/pano/dialog/Action;->mMultilineDescription:Z
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$902(Lcom/google/android/pano/dialog/Action;Z)Z

    .line 76
    iget-boolean v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mHasNext:Z

    # setter for: Lcom/google/android/pano/dialog/Action;->mHasNext:Z
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$1002(Lcom/google/android/pano/dialog/Action;Z)Z

    .line 77
    iget-boolean v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mInfoOnly:Z

    # setter for: Lcom/google/android/pano/dialog/Action;->mInfoOnly:Z
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$1102(Lcom/google/android/pano/dialog/Action;Z)Z

    .line 78
    iget v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mCheckSetId:I

    # setter for: Lcom/google/android/pano/dialog/Action;->mCheckSetId:I
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$1202(Lcom/google/android/pano/dialog/Action;I)I

    .line 79
    iget-boolean v1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mEnabled:Z

    # setter for: Lcom/google/android/pano/dialog/Action;->mEnabled:Z
    invoke-static {v0, v1}, Lcom/google/android/pano/dialog/Action;->access$1302(Lcom/google/android/pano/dialog/Action;Z)Z

    .line 80
    return-object v0
.end method

.method public checkSetId(I)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "checkSetId"    # I

    .prologue
    .line 139
    iput p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mCheckSetId:I

    .line 140
    return-object p0
.end method

.method public checked(Z)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mChecked:Z

    .line 120
    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mDescription:Ljava/lang/String;

    .line 95
    return-object p0
.end method

.method public drawableResource(I)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "drawableResource"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mDrawableResource:I

    .line 110
    return-object p0
.end method

.method public enabled(Z)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mEnabled:Z

    .line 145
    return-object p0
.end method

.method public iconUri(Landroid/net/Uri;)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "iconUri"    # Landroid/net/Uri;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mIconUri:Landroid/net/Uri;

    .line 115
    return-object p0
.end method

.method public intent(Landroid/content/Intent;)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mIntent:Landroid/content/Intent;

    .line 100
    return-object p0
.end method

.method public key(Ljava/lang/String;)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mKey:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public multilineDescription(Z)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "multilineDescription"    # Z

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mMultilineDescription:Z

    .line 125
    return-object p0
.end method

.method public resourcePackageName(Ljava/lang/String;)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "resourcePackageName"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mResourcePackageName:Ljava/lang/String;

    .line 105
    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/google/android/pano/dialog/Action$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/pano/dialog/Action$Builder;->mTitle:Ljava/lang/String;

    .line 90
    return-object p0
.end method
