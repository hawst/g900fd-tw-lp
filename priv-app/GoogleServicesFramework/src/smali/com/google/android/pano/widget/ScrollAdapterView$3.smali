.class Lcom/google/android/pano/widget/ScrollAdapterView$3;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ScrollAdapterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/pano/widget/ScrollAdapterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private downTimeOnExpandedView:J

.field final synthetic this$0:Lcom/google/android/pano/widget/ScrollAdapterView;


# direct methods
.method constructor <init>(Lcom/google/android/pano/widget/ScrollAdapterView;)V
    .locals 0

    .prologue
    .line 1820
    iput-object p1, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1825
    iget-object v3, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v3}, Lcom/google/android/pano/widget/ScrollAdapterView;->requestFocus()Z

    .line 1826
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    # getter for: Lcom/google/android/pano/widget/ScrollAdapterView;->mExpandedViews:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/pano/widget/ScrollAdapterView;->access$100(Lcom/google/android/pano/widget/ScrollAdapterView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "size":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 1827
    iget-object v3, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    # getter for: Lcom/google/android/pano/widget/ScrollAdapterView;->mExpandedViews:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/pano/widget/ScrollAdapterView;->access$100(Lcom/google/android/pano/widget/ScrollAdapterView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/pano/widget/ScrollAdapterView$ExpandedView;

    .line 1828
    .local v2, "view":Lcom/google/android/pano/widget/ScrollAdapterView$ExpandedView;
    iget v3, v2, Lcom/google/android/pano/widget/ScrollAdapterView$ExpandedView;->progress:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 1829
    iget-object v3, v2, Lcom/google/android/pano/widget/ScrollAdapterView$ExpandedView;->expandedView:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v4, v4, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1830
    iget-object v3, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v3, v3, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v4}, Lcom/google/android/pano/widget/ScrollAdapterView;->getScrollX()I

    move-result v4

    neg-int v4, v4

    iget-object v5, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v5}, Lcom/google/android/pano/widget/ScrollAdapterView;->getScrollY()I

    move-result v5

    neg-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 1831
    iget-object v3, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v3, v3, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1833
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->downTimeOnExpandedView:J

    .line 1834
    const/4 v3, 0x0

    .line 1839
    .end local v2    # "view":Lcom/google/android/pano/widget/ScrollAdapterView$ExpandedView;
    :goto_1
    return v3

    .line 1826
    .restart local v2    # "view":Lcom/google/android/pano/widget/ScrollAdapterView$ExpandedView;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1838
    .end local v2    # "view":Lcom/google/android/pano/widget/ScrollAdapterView$ExpandedView;
    :cond_1
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->downTimeOnExpandedView:J

    .line 1839
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 1884
    iget-object v0, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    neg-float v1, p3

    neg-float v2, p4

    # invokes: Lcom/google/android/pano/widget/ScrollAdapterView;->doFling(FF)Z
    invoke-static {v0, v1, v2}, Lcom/google/android/pano/widget/ScrollAdapterView;->access$1300(Lcom/google/android/pano/widget/ScrollAdapterView;FF)Z

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 1889
    iget-object v0, p0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/pano/widget/ScrollAdapterView;->dragBy(FF)V

    .line 1890
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 18
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1844
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->downTimeOnExpandedView:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    .line 1846
    const/4 v2, 0x0

    .line 1878
    :goto_0
    return v2

    .line 1848
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v2}, Lcom/google/android/pano/widget/ScrollAdapterView;->getScrollX()I

    move-result v2

    neg-int v15, v2

    .line 1849
    .local v15, "xOffset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v2}, Lcom/google/android/pano/widget/ScrollAdapterView;->getScrollY()I

    move-result v2

    neg-int v0, v2

    move/from16 v17, v0

    .line 1850
    .local v17, "yOffset":I
    const/4 v10, 0x0

    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v2}, Lcom/google/android/pano/widget/ScrollAdapterView;->getChildCount()I

    move-result v12

    .local v12, "size":I
    :goto_1
    if-ge v10, v12, :cond_4

    .line 1851
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v2, v10}, Lcom/google/android/pano/widget/ScrollAdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1852
    .local v4, "child":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v14, v2

    .line 1853
    .local v14, "x":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v0, v2

    move/from16 v16, v0

    .line 1854
    .local v16, "y":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v2, v2, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1855
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v2, v2, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    move/from16 v0, v17

    invoke-virtual {v2, v15, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1856
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v2, v2, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    move/from16 v0, v16

    invoke-virtual {v2, v14, v0}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1857
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v2}, Lcom/google/android/pano/widget/ScrollAdapterView;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1858
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    # invokes: Lcom/google/android/pano/widget/ScrollAdapterView;->getAdapterIndex(I)I
    invoke-static {v2, v10}, Lcom/google/android/pano/widget/ScrollAdapterView;->access$000(Lcom/google/android/pano/widget/ScrollAdapterView;I)I

    move-result v5

    .line 1859
    .local v5, "adapterIndex":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v2}, Lcom/google/android/pano/widget/ScrollAdapterView;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    # getter for: Lcom/google/android/pano/widget/ScrollAdapterView;->mAdapter:Lcom/google/android/pano/widget/ScrollAdapter;
    invoke-static {v6}, Lcom/google/android/pano/widget/ScrollAdapterView;->access$1200(Lcom/google/android/pano/widget/ScrollAdapterView;)Lcom/google/android/pano/widget/ScrollAdapter;

    move-result-object v6

    invoke-interface {v6, v5}, Lcom/google/android/pano/widget/ScrollAdapter;->getItemId(I)J

    move-result-wide v6

    invoke-interface/range {v2 .. v7}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 1861
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1863
    .end local v5    # "adapterIndex":I
    :cond_1
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1864
    .local v13, "touchables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4, v13}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 1865
    const/4 v11, 0x0

    .local v11, "j":I
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v8

    .local v8, "childSize":I
    :goto_2
    if-ge v11, v8, :cond_3

    .line 1866
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 1867
    .local v9, "grandchild":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v2, v2, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v9, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1868
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v6, v6, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v2, v6}, Lcom/google/android/pano/widget/ScrollAdapterView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1870
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v2, v2, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    move/from16 v0, v17

    invoke-virtual {v2, v15, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1871
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/widget/ScrollAdapterView$3;->this$0:Lcom/google/android/pano/widget/ScrollAdapterView;

    iget-object v2, v2, Lcom/google/android/pano/widget/ScrollAdapterView;->mTempRect:Landroid/graphics/Rect;

    move/from16 v0, v16

    invoke-virtual {v2, v14, v0}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1872
    invoke-virtual {v9}, Landroid/view/View;->performClick()Z

    .line 1873
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1865
    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1850
    .end local v8    # "childSize":I
    .end local v9    # "grandchild":Landroid/view/View;
    .end local v11    # "j":I
    .end local v13    # "touchables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 1878
    .end local v4    # "child":Landroid/view/View;
    .end local v14    # "x":I
    .end local v16    # "y":I
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0
.end method
