.class public Lcom/google/android/pano/dialog/BaseActionFragment;
.super Lcom/google/android/pano/dialog/BaseScrollAdapterFragment;
.source "BaseActionFragment.java"

# interfaces
.implements Lcom/google/android/pano/dialog/ActionAdapter$Listener;
.implements Lcom/google/android/pano/dialog/ActionAdapter$OnFocusListener;
.implements Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;


# instance fields
.field private mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

.field private mAddedSavedActions:Z

.field private final mFragment:Lcom/google/android/pano/dialog/LiteFragment;

.field private mIndexToSelect:I

.field private mListener:Lcom/google/android/pano/dialog/ActionAdapter$Listener;

.field private mName:Ljava/lang/String;

.field private mSelectFirstChecked:Z


# direct methods
.method public constructor <init>(Lcom/google/android/pano/dialog/LiteFragment;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/android/pano/dialog/LiteFragment;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/pano/dialog/BaseScrollAdapterFragment;-><init>(Lcom/google/android/pano/dialog/LiteFragment;)V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mListener:Lcom/google/android/pano/dialog/ActionAdapter$Listener;

    .line 59
    iput-object p1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mIndexToSelect:I

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mSelectFirstChecked:Z

    .line 62
    return-void
.end method

.method public static buildArgs(Ljava/util/ArrayList;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/pano/dialog/Action;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/pano/dialog/Action;>;"
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/pano/dialog/BaseActionFragment;->buildArgs(Ljava/util/ArrayList;Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static buildArgs(Ljava/util/ArrayList;Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/pano/dialog/Action;",
            ">;",
            "Ljava/lang/String;",
            "I)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/pano/dialog/Action;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 83
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "actions"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 84
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v1, "index"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    return-object v0
.end method

.method private loadActionsFromArgumentsIfNecessary()V
    .locals 5

    .prologue
    .line 218
    iget-object v3, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v3}, Lcom/google/android/pano/dialog/LiteFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAddedSavedActions:Z

    if-nez v3, :cond_1

    .line 219
    iget-object v3, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v3}, Lcom/google/android/pano/dialog/LiteFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "actions"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 221
    .local v0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/pano/dialog/Action;>;"
    if-eqz v0, :cond_1

    .line 222
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 223
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 224
    iget-boolean v3, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mSelectFirstChecked:Z

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/pano/dialog/Action;

    invoke-virtual {v3}, Lcom/google/android/pano/dialog/Action;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mIndexToSelect:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 226
    iput v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mIndexToSelect:I

    .line 228
    :cond_0
    iget-object v4, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/pano/dialog/Action;

    invoke-virtual {v4, v3}, Lcom/google/android/pano/dialog/ActionAdapter;->addAction(Lcom/google/android/pano/dialog/Action;)V

    .line 223
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 232
    .end local v0    # "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/pano/dialog/Action;>;"
    .end local v1    # "index":I
    .end local v2    # "size":I
    :cond_1
    return-void
.end method


# virtual methods
.method public hasListener()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mListener:Lcom/google/android/pano/dialog/ActionAdapter$Listener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActionClicked(Lcom/google/android/pano/dialog/Action;)V
    .locals 2
    .param p1, "action"    # Lcom/google/android/pano/dialog/Action;

    .prologue
    .line 152
    invoke-virtual {p1}, Lcom/google/android/pano/dialog/Action;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/pano/dialog/Action;->infoOnly()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mListener:Lcom/google/android/pano/dialog/ActionAdapter$Listener;

    if-eqz v1, :cond_2

    .line 157
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mListener:Lcom/google/android/pano/dialog/ActionAdapter$Listener;

    invoke-interface {v1, p1}, Lcom/google/android/pano/dialog/ActionAdapter$Listener;->onActionClicked(Lcom/google/android/pano/dialog/Action;)V

    goto :goto_0

    .line 158
    :cond_2
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v1}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/pano/dialog/ActionAdapter$Listener;

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v1}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/pano/dialog/ActionAdapter$Listener;

    .line 160
    .local v0, "listener":Lcom/google/android/pano/dialog/ActionAdapter$Listener;
    invoke-interface {v0, p1}, Lcom/google/android/pano/dialog/ActionAdapter$Listener;->onActionClicked(Lcom/google/android/pano/dialog/Action;)V

    goto :goto_0
.end method

.method public onActionFocused(Lcom/google/android/pano/dialog/Action;)V
    .locals 2
    .param p1, "action"    # Lcom/google/android/pano/dialog/Action;

    .prologue
    .line 166
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v1}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/pano/dialog/ActionAdapter$OnFocusListener;

    if-eqz v1, :cond_0

    .line 167
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v1}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/pano/dialog/ActionAdapter$OnFocusListener;

    .line 169
    .local v0, "listener":Lcom/google/android/pano/dialog/ActionAdapter$OnFocusListener;
    invoke-interface {v0, p1}, Lcom/google/android/pano/dialog/ActionAdapter$OnFocusListener;->onActionFocused(Lcom/google/android/pano/dialog/Action;)V

    .line 171
    .end local v0    # "listener":Lcom/google/android/pano/dialog/ActionAdapter$OnFocusListener;
    :cond_0
    return-void
.end method

.method public onActionSelect(Lcom/google/android/pano/dialog/Action;)V
    .locals 2
    .param p1, "action"    # Lcom/google/android/pano/dialog/Action;

    .prologue
    .line 175
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v1}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v1}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;

    .line 178
    .local v0, "listener":Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;
    invoke-interface {v0, p1}, Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;->onActionSelect(Lcom/google/android/pano/dialog/Action;)V

    .line 180
    .end local v0    # "listener":Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;
    :cond_0
    return-void
.end method

.method public onActionUnselect(Lcom/google/android/pano/dialog/Action;)V
    .locals 2
    .param p1, "action"    # Lcom/google/android/pano/dialog/Action;

    .prologue
    .line 184
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v1}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v1}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;

    .line 187
    .local v0, "listener":Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;
    invoke-interface {v0, p1}, Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;->onActionUnselect(Lcom/google/android/pano/dialog/Action;)V

    .line 189
    .end local v0    # "listener":Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, -0x1

    .line 90
    new-instance v5, Lcom/google/android/pano/dialog/ActionAdapter;

    iget-object v6, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v6}, Lcom/google/android/pano/dialog/LiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/pano/dialog/ActionAdapter;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    .line 91
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAddedSavedActions:Z

    .line 92
    if-eqz p1, :cond_3

    .line 93
    const-string v5, "actions"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 94
    .local v1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/pano/dialog/Action;>;"
    const-string v5, "index"

    invoke-virtual {p1, v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 95
    .local v3, "savedIndex":I
    if-eqz v1, :cond_2

    .line 96
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/pano/dialog/Action;

    .line 97
    .local v0, "action":Lcom/google/android/pano/dialog/Action;
    iget-object v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {v5, v0}, Lcom/google/android/pano/dialog/ActionAdapter;->addAction(Lcom/google/android/pano/dialog/Action;)V

    goto :goto_0

    .line 99
    .end local v0    # "action":Lcom/google/android/pano/dialog/Action;
    :cond_0
    if-ltz v3, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 100
    iput v3, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mIndexToSelect:I

    .line 102
    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAddedSavedActions:Z

    .line 112
    .end local v1    # "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/pano/dialog/Action;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "savedIndex":I
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v5}, Lcom/google/android/pano/dialog/LiteFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "name"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mName:Ljava/lang/String;

    .line 113
    invoke-direct {p0}, Lcom/google/android/pano/dialog/BaseActionFragment;->loadActionsFromArgumentsIfNecessary()V

    .line 114
    iget-object v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {v5, p0}, Lcom/google/android/pano/dialog/ActionAdapter;->setListener(Lcom/google/android/pano/dialog/ActionAdapter$Listener;)V

    .line 115
    iget-object v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {v5, p0}, Lcom/google/android/pano/dialog/ActionAdapter;->setOnFocusListener(Lcom/google/android/pano/dialog/ActionAdapter$OnFocusListener;)V

    .line 116
    iget-object v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {v5, p0}, Lcom/google/android/pano/dialog/ActionAdapter;->setOnKeyListener(Lcom/google/android/pano/dialog/ActionAdapter$OnKeyListener;)V

    .line 117
    return-void

    .line 105
    :cond_3
    iget-object v5, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mFragment:Lcom/google/android/pano/dialog/LiteFragment;

    invoke-interface {v5}, Lcom/google/android/pano/dialog/LiteFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "index"

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 106
    .local v4, "startIndex":I
    if-eq v4, v7, :cond_2

    .line 109
    iput v4, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mIndexToSelect:I

    goto :goto_1
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 121
    invoke-virtual {p0}, Lcom/google/android/pano/dialog/BaseActionFragment;->getScrollAdapterView()Lcom/google/android/pano/widget/ScrollAdapterView;

    move-result-object v0

    .line 123
    .local v0, "sav":Lcom/google/android/pano/widget/ScrollAdapterView;
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/pano/widget/ScrollAdapterView;->addOnScrollListener(Lcom/google/android/pano/widget/ScrollAdapterView$OnScrollListener;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/pano/dialog/BaseActionFragment;->getAdapter()Lcom/google/android/pano/widget/ScrollAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    if-eq v1, v2, :cond_0

    .line 125
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/pano/dialog/ActionAdapter;->setScrollAdapterView(Lcom/google/android/pano/widget/ScrollAdapterView;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {p0, v1}, Lcom/google/android/pano/dialog/BaseActionFragment;->setAdapter(Lcom/google/android/pano/widget/ScrollAdapter;)V

    .line 128
    :cond_0
    iget v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mIndexToSelect:I

    if-eq v1, v3, :cond_1

    .line 129
    invoke-virtual {p0}, Lcom/google/android/pano/dialog/BaseActionFragment;->getScrollAdapterView()Lcom/google/android/pano/widget/ScrollAdapterView;

    move-result-object v1

    iget v2, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mIndexToSelect:I

    invoke-virtual {v1, v2}, Lcom/google/android/pano/widget/ScrollAdapterView;->setSelection(I)V

    .line 130
    iput v3, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mIndexToSelect:I

    .line 132
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/pano/dialog/BaseActionFragment;->hasCreatedView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "actions"

    iget-object v1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mAdapter:Lcom/google/android/pano/dialog/ActionAdapter;

    invoke-virtual {v1}, Lcom/google/android/pano/dialog/ActionAdapter;->getActions()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 138
    const-string v0, "index"

    invoke-virtual {p0}, Lcom/google/android/pano/dialog/BaseActionFragment;->getScrollAdapterView()Lcom/google/android/pano/widget/ScrollAdapterView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/pano/widget/ScrollAdapterView;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/pano/dialog/ActionAdapter$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/pano/dialog/ActionAdapter$Listener;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/android/pano/dialog/BaseActionFragment;->mListener:Lcom/google/android/pano/dialog/ActionAdapter$Listener;

    .line 204
    return-void
.end method
