.class public Lcom/google/android/pano/setup/SteppedProgressBar;
.super Landroid/view/View;
.source "SteppedProgressBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;,
        Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;
    }
.end annotation


# static fields
.field private static final DEFAULT_ALPHA_CURRENT_STEP:I

.field private static final DEFAULT_TYPEFACE:Landroid/graphics/Typeface;

.field private static final KEY_BUNDLE:Ljava/lang/String;

.field private static final SUPPORTED_VERSIONS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

.field private final DEFAULT_ALPHA_OTHER_STEP:I

.field private mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

.field private mCurrentStep:I

.field private mCutPaint:Landroid/graphics/Paint;

.field private mDesiredHeight:F

.field private mDesiredWidth:F

.field private mDontDisplay:Z

.field final mDotRect:Landroid/graphics/RectF;

.field private mInactiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMeasuredHeight:I

.field private mMeasuredWidth:I

.field final mScratchRect:Landroid/graphics/Rect;

.field private mStepLabels:[Ljava/lang/String;

.field private mTextHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/pano/setup/SteppedProgressBar;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".stateBundle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/pano/setup/SteppedProgressBar;->KEY_BUNDLE:Ljava/lang/String;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/pano/setup/SteppedProgressBar;->SUPPORTED_VERSIONS:Ljava/util/ArrayList;

    .line 81
    sget-object v0, Lcom/google/android/pano/setup/SteppedProgressBar;->SUPPORTED_VERSIONS:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v0, Lcom/google/android/pano/setup/SteppedProgressBar;->SUPPORTED_VERSIONS:Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    const/high16 v0, 0x424c0000    # 51.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULT_ALPHA_CURRENT_STEP:I

    .line 95
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULT_TYPEFACE:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/pano/setup/SteppedProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 127
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/pano/setup/SteppedProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, 0x0

    .line 131
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    const/high16 v0, 0x414c0000    # 12.75f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULT_ALPHA_OTHER_STEP:I

    .line 99
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    .line 105
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    .line 106
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mScratchRect:Landroid/graphics/Rect;

    .line 112
    iput v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredWidth:F

    .line 113
    iput v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredHeight:F

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mTextHeight:I

    .line 118
    iput v1, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mCurrentStep:I

    .line 120
    iput-boolean v1, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDontDisplay:Z

    .line 132
    invoke-direct {p0}, Lcom/google/android/pano/setup/SteppedProgressBar;->loadValuesFromResources()Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    .line 133
    invoke-direct {p0, p1, p2}, Lcom/google/android/pano/setup/SteppedProgressBar;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 134
    invoke-direct {p0}, Lcom/google/android/pano/setup/SteppedProgressBar;->initPaints()V

    .line 135
    return-void
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 352
    if-nez p2, :cond_0

    .line 402
    :goto_0
    return-void

    .line 356
    :cond_0
    sget-object v6, Lcom/google/android/pano/setup/R$styleable;->SteppedProgressBar:[I

    invoke-virtual {p1, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 358
    .local v5, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 360
    .local v2, "res":Landroid/content/res/Resources;
    sget v6, Lcom/google/android/pano/setup/R$styleable;->SteppedProgressBar_textAppearanceActive:I

    invoke-virtual {v5, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 363
    sget v6, Lcom/google/android/pano/setup/R$styleable;->SteppedProgressBar_textAppearanceActive:I

    invoke-virtual {v5, v6, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 365
    .local v3, "styleId":I
    if-eqz v3, :cond_3

    .line 369
    sget-object v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias:[I

    invoke-virtual {p1, v3, v6}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 371
    .local v4, "textAttributes":Landroid/content/res/TypedArray;
    invoke-direct {p0, p1, v4}, Lcom/google/android/pano/setup/SteppedProgressBar;->initPaintFromTextAppearance(Landroid/content/Context;Landroid/content/res/TypedArray;)Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    .line 372
    iget-object v6, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v6, v6, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    invoke-virtual {v6, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 373
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 379
    .end local v3    # "styleId":I
    .end local v4    # "textAttributes":Landroid/content/res/TypedArray;
    :cond_1
    sget v6, Lcom/google/android/pano/setup/R$styleable;->SteppedProgressBar_textAppearanceCompleted:I

    invoke-virtual {v5, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 380
    sget v6, Lcom/google/android/pano/setup/R$styleable;->SteppedProgressBar_textAppearanceCompleted:I

    invoke-virtual {v5, v6, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 382
    .restart local v3    # "styleId":I
    if-eqz v3, :cond_2

    .line 383
    sget-object v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias:[I

    invoke-virtual {p1, v3, v6}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 385
    .restart local v4    # "textAttributes":Landroid/content/res/TypedArray;
    invoke-direct {p0, p1, v4}, Lcom/google/android/pano/setup/SteppedProgressBar;->initPaintFromTextAppearance(Landroid/content/Context;Landroid/content/res/TypedArray;)Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mInactiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    .line 386
    iget-object v6, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mInactiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v6, v6, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    invoke-virtual {v6, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 387
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 391
    .end local v3    # "styleId":I
    .end local v4    # "textAttributes":Landroid/content/res/TypedArray;
    :cond_2
    sget v6, Lcom/google/android/pano/setup/R$styleable;->SteppedProgressBar_labels:I

    invoke-virtual {v5, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 392
    sget v6, Lcom/google/android/pano/setup/R$styleable;->SteppedProgressBar_labels:I

    invoke-virtual {v5, v6}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 394
    .local v1, "labels":[Ljava/lang/CharSequence;
    if-eqz v1, :cond_4

    .line 395
    array-length v6, v1

    new-array v6, v6, [Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    .line 396
    const/4 v0, 0x0

    .local v0, "copyPtr":I
    :goto_1
    array-length v6, v1

    if-ge v0, v6, :cond_4

    .line 397
    iget-object v6, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    aget-object v7, v1, v0

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    .line 396
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 375
    .end local v0    # "copyPtr":I
    .end local v1    # "labels":[Ljava/lang/CharSequence;
    .restart local v3    # "styleId":I
    :cond_3
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "textAppearanceActive must reference a style."

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 401
    .end local v3    # "styleId":I
    :cond_4
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_0
.end method

.method private initPaintFromTextAppearance(Landroid/content/Context;Landroid/content/res/TypedArray;)Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v8, 0x0

    .line 318
    new-instance v2, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-direct {v2, v6, v8}, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;-><init>(Landroid/graphics/Paint;Z)V

    .line 320
    .local v2, "record":Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;
    sget v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias_android_fontFamily:I

    invoke-virtual {p2, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "fontFamily":Ljava/lang/String;
    sget v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias_android_textAllCaps:I

    invoke-virtual {p2, v6, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    iput-boolean v6, v2, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->allCapsOn:Z

    .line 325
    sget v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias_android_textColor:I

    invoke-virtual {p2, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 326
    sget v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias_android_textColor:I

    invoke-virtual {p2, v6, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 327
    .local v3, "textColor":I
    iget-object v6, v2, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 330
    .end local v3    # "textColor":I
    :cond_0
    sget v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias_android_textSize:I

    invoke-virtual {p2, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 331
    sget v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias_android_textSize:I

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    .line 333
    .local v4, "textSize":F
    iget-object v6, v2, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    invoke-virtual {v6, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 336
    .end local v4    # "textSize":F
    :cond_1
    sget v6, Lcom/google/android/pano/setup/R$styleable;->TextAppearanceAlias_android_textStyle:I

    invoke-virtual {p2, v6, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 340
    .local v1, "fontStyle":I
    if-nez v0, :cond_2

    .line 341
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-static {v6, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v5

    .line 346
    .local v5, "typeface":Landroid/graphics/Typeface;
    :goto_0
    iget-object v6, v2, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 347
    iget-object v6, v2, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    invoke-virtual {v6, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 348
    return-object v2

    .line 343
    .end local v5    # "typeface":Landroid/graphics/Typeface;
    :cond_2
    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    .restart local v5    # "typeface":Landroid/graphics/Typeface;
    goto :goto_0
.end method

.method private initPaints()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 405
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    if-nez v0, :cond_0

    .line 406
    new-instance v0, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-direct {v0, v1, v3}, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;-><init>(Landroid/graphics/Paint;Z)V

    iput-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    .line 407
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v0, v0, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 408
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v0, v0, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    sget-object v1, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULT_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 409
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v0, v0, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULT_ALPHA_CURRENT_STEP:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 410
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v0, v0, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {p0}, Lcom/google/android/pano/setup/SteppedProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v4, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 412
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v0, v0, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mInactiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    if-nez v0, :cond_1

    .line 416
    new-instance v0, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v2, v2, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    invoke-direct {v0, v1, v3}, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;-><init>(Landroid/graphics/Paint;Z)V

    iput-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mInactiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    .line 417
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mInactiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v0, v0, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULT_ALPHA_OTHER_STEP:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 420
    :cond_1
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v1, v1, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mLinePaint:Landroid/graphics/Paint;

    .line 421
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/pano/setup/SteppedProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/pano/setup/R$integer;->progress_bar_line_alpha:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 423
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mCutPaint:Landroid/graphics/Paint;

    .line 424
    iget-object v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mCutPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 425
    return-void
.end method

.method private loadValuesFromResources()Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;
    .locals 9

    .prologue
    .line 429
    invoke-virtual {p0}, Lcom/google/android/pano/setup/SteppedProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 430
    .local v8, "res":Landroid/content/res/Resources;
    new-instance v0, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    sget v1, Lcom/google/android/pano/setup/R$dimen;->progress_bar_step_separation:I

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sget v2, Lcom/google/android/pano/setup/R$dimen;->active_indicator_radius:I

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lcom/google/android/pano/setup/R$dimen;->inactive_indicator_radius:I

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/google/android/pano/setup/R$dimen;->text_to_line_distance:I

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/pano/setup/R$dimen;->progress_bar_line_width:I

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    sget v6, Lcom/google/android/pano/setup/R$dimen;->active_indicator_center_cut_radius:I

    invoke-virtual {v8, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    sget v7, Lcom/google/android/pano/setup/R$dimen;->label_width_max:I

    invoke-virtual {v8, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;-><init>(FFFFFFF)V

    return-object v0
.end method

.method private measureHeight(I)V
    .locals 5
    .param p1, "heightMeasureSpec"    # I

    .prologue
    .line 471
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 472
    .local v2, "suggestedHeight":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 474
    .local v1, "mode":I
    sparse-switch v1, :sswitch_data_0

    .line 500
    :goto_0
    return-void

    .line 476
    :sswitch_0
    iput v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mMeasuredHeight:I

    goto :goto_0

    .line 479
    :sswitch_1
    const v2, 0x7fffffff

    .line 483
    :sswitch_2
    iget v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredHeight:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    .line 484
    iget v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mTextHeight:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 485
    invoke-direct {p0}, Lcom/google/android/pano/setup/SteppedProgressBar;->measureText()V

    .line 488
    :cond_0
    iget-object v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v3, v3, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->ACTIVE_INDICATOR_RADIUS:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v0, v3, v4

    .line 491
    .local v0, "activeIndicatorDiameter":F
    iget v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mTextHeight:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v4, v4, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->DISTANCE_TEXT_TO_LINE:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v4, v4, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->LINE_WIDTH:F

    cmpl-float v4, v4, v0

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v0, v4, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->LINE_WIDTH:F

    .end local v0    # "activeIndicatorDiameter":F
    :cond_1
    add-float/2addr v3, v0

    iput v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredHeight:F

    .line 496
    :cond_2
    iget v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredHeight:F

    int-to-float v4, v2

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    iget v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredHeight:F

    :goto_1
    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mMeasuredHeight:I

    goto :goto_0

    :cond_3
    int-to-float v3, v2

    goto :goto_1

    .line 474
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private measureText()V
    .locals 7

    .prologue
    .line 504
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 505
    .local v1, "allLabelsBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "ptr":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 506
    iget-object v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 508
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 510
    .local v0, "allLabels":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    iget-object v3, v3, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mScratchRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 511
    iget-object v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mScratchRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mScratchRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mTextHeight:I

    .line 512
    return-void
.end method

.method private measureWidth(I)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I

    .prologue
    .line 440
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 441
    .local v1, "suggestedWidth":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 443
    .local v0, "mode":I
    sparse-switch v0, :sswitch_data_0

    .line 468
    :goto_0
    return-void

    .line 446
    :sswitch_0
    iput v1, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mMeasuredWidth:I

    goto :goto_0

    .line 450
    :sswitch_1
    const v1, 0x7fffffff

    .line 453
    :sswitch_2
    iget v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredWidth:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 458
    iget-object v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v3, v3, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->STEP_SEPARATION:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v3, v3, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->LABEL_WIDTH_MAX:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredWidth:F

    .line 464
    :cond_0
    iget v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredWidth:F

    int-to-float v3, v1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    iget v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDesiredWidth:F

    :goto_1
    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mMeasuredWidth:I

    goto :goto_0

    :cond_1
    int-to-float v2, v1

    goto :goto_1

    .line 443
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 22
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v2, v2, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->STEP_SEPARATION:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v15, v2, v4

    .line 173
    .local v15, "halfStep":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v2, v2, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->LABEL_WIDTH_MAX:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v13, v2, v4

    .line 174
    .local v13, "halfLabelWidth":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v2, v2, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->LINE_WIDTH:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v14, v2, v4

    .line 175
    .local v14, "halfLineWidth":F
    move v3, v13

    .line 177
    .local v3, "offset":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mTextHeight:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_0

    .line 179
    invoke-direct/range {p0 .. p0}, Lcom/google/android/pano/setup/SteppedProgressBar;->measureText()V

    .line 181
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mTextHeight:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v4, v4, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->DISTANCE_TEXT_TO_LINE:F

    add-float/2addr v2, v4

    add-float v12, v2, v14

    .line 182
    .local v12, "distanceToLineCenter":F
    sub-float v4, v12, v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/pano/setup/SteppedProgressBar;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float v5, v2, v3

    add-float v6, v12, v14

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 185
    const/16 v17, 0x0

    .local v17, "ptr":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    array-length v2, v2

    move/from16 v0, v17

    if-ge v0, v2, :cond_9

    .line 186
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mCurrentStep:I

    move/from16 v0, v17

    if-ne v0, v2, :cond_3

    const/16 v16, 0x1

    .line 187
    .local v16, "isActive":Z
    :goto_1
    if-eqz v16, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mActiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    .line 189
    .local v11, "currentPaint":Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;
    :goto_2
    iget-boolean v2, v11, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->allCapsOn:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    aget-object v2, v2, v17

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    .line 191
    .local v19, "text":Ljava/lang/String;
    :goto_3
    iget-object v2, v11, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float v21, v2, v4

    .line 192
    .local v21, "textWidth":F
    sub-float v10, v3, v13

    .line 195
    .local v10, "clipLeft":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 196
    const/4 v2, 0x0

    add-float v4, v3, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mTextHeight:I

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v2, v4, v5}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 197
    sub-float v20, v3, v21

    .line 198
    .local v20, "textLeft":F
    cmpg-float v2, v20, v10

    if-gez v2, :cond_6

    .end local v10    # "clipLeft":F
    :goto_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mTextHeight:I

    int-to-float v2, v2

    iget-object v4, v11, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 201
    .end local v20    # "textLeft":F
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 204
    if-eqz v16, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v0, v2, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->ACTIVE_INDICATOR_RADIUS:F

    move/from16 v18, v0

    .line 206
    .local v18, "radius":F
    :goto_5
    if-nez v16, :cond_8

    const/16 v16, 0x1

    .line 207
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    sub-float v4, v12, v18

    iput v4, v2, Landroid/graphics/RectF;->top:F

    .line 208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    sub-float v4, v3, v18

    iput v4, v2, Landroid/graphics/RectF;->left:F

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float v4, v4, v18

    add-float v4, v4, v18

    iput v4, v2, Landroid/graphics/RectF;->bottom:F

    .line 210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    add-float v4, v3, v18

    iput v4, v2, Landroid/graphics/RectF;->right:F

    .line 211
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    const/4 v6, 0x0

    const/high16 v7, 0x43b40000    # 360.0f

    const/4 v8, 0x1

    iget-object v9, v11, Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;->paint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 213
    if-nez v16, :cond_2

    .line 215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v4, v4, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->ACTIVE_INDICATOR_CENTER_CUT_RADIUS:F

    sub-float v4, v12, v4

    iput v4, v2, Landroid/graphics/RectF;->top:F

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v4, v4, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->ACTIVE_INDICATOR_CENTER_CUT_RADIUS:F

    sub-float v4, v3, v4

    iput v4, v2, Landroid/graphics/RectF;->left:F

    .line 217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v4, v4, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->ACTIVE_INDICATOR_CENTER_CUT_RADIUS:F

    add-float/2addr v4, v12

    iput v4, v2, Landroid/graphics/RectF;->bottom:F

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v4, v4, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->ACTIVE_INDICATOR_CENTER_CUT_RADIUS:F

    add-float/2addr v4, v3

    iput v4, v2, Landroid/graphics/RectF;->right:F

    .line 220
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDotRect:Landroid/graphics/RectF;

    const/4 v6, 0x0

    const/high16 v7, 0x43b40000    # 360.0f

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mCutPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 185
    :cond_2
    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v2, v2, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->STEP_SEPARATION:F

    add-float/2addr v3, v2

    goto/16 :goto_0

    .line 186
    .end local v11    # "currentPaint":Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;
    .end local v16    # "isActive":Z
    .end local v18    # "radius":F
    .end local v19    # "text":Ljava/lang/String;
    .end local v21    # "textWidth":F
    :cond_3
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 187
    .restart local v16    # "isActive":Z
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mInactiveStepText:Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;

    goto/16 :goto_2

    .line 189
    .restart local v11    # "currentPaint":Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->mStepLabels:[Ljava/lang/String;

    aget-object v19, v2, v17

    goto/16 :goto_3

    .restart local v10    # "clipLeft":F
    .restart local v19    # "text":Ljava/lang/String;
    .restart local v20    # "textLeft":F
    .restart local v21    # "textWidth":F
    :cond_6
    move/from16 v10, v20

    .line 198
    goto/16 :goto_4

    .line 204
    .end local v10    # "clipLeft":F
    .end local v20    # "textLeft":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/pano/setup/SteppedProgressBar;->DEFAULTS:Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;

    iget v0, v2, Lcom/google/android/pano/setup/SteppedProgressBar$Defaults;->INACTIVE_INDICATOR_RADIUS:F

    move/from16 v18, v0

    goto/16 :goto_5

    .line 206
    .restart local v18    # "radius":F
    :cond_8
    const/16 v16, 0x0

    goto/16 :goto_6

    .line 223
    .end local v11    # "currentPaint":Lcom/google/android/pano/setup/SteppedProgressBar$PaintRecord;
    .end local v16    # "isActive":Z
    .end local v18    # "radius":F
    .end local v19    # "text":Ljava/lang/String;
    .end local v21    # "textWidth":F
    :cond_9
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/google/android/pano/setup/SteppedProgressBar;->measureWidth(I)V

    .line 140
    invoke-direct {p0, p2}, Lcom/google/android/pano/setup/SteppedProgressBar;->measureHeight(I)V

    .line 141
    iget v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mMeasuredWidth:I

    iget v1, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mMeasuredHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/pano/setup/SteppedProgressBar;->setMeasuredDimension(II)V

    .line 142
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/google/android/pano/setup/SteppedProgressBar;->mDontDisplay:Z

    if-nez v0, :cond_0

    .line 164
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 166
    :cond_0
    return-void
.end method
