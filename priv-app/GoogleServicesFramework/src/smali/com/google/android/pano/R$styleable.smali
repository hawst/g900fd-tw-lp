.class public final Lcom/google/android/pano/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/pano/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AutoScaleImageView:[I

.field public static final ConstrainedLinearLayout:[I

.field public static final FrameLayoutWithShadows:[I

.field public static final FrameLayoutWithShadows_defaultShadow:I = 0x0

.field public static final FrameLayoutWithShadows_drawableBottom:I = 0x1

.field public static final InlineKeyboard:[I

.field public static final PlaybackControllerView:[I

.field public static final PlaybackControllerView_allowFastForward:I = 0x0

.field public static final PlaybackControllerView_allowNext:I = 0x1

.field public static final PlaybackControllerView_allowPause:I = 0x2

.field public static final PlaybackControllerView_allowPlay:I = 0x3

.field public static final PlaybackControllerView_allowPrevious:I = 0x4

.field public static final PlaybackControllerView_allowRewind:I = 0x5

.field public static final PlaybackControllerView_fastforwardIcon:I = 0x8

.field public static final PlaybackControllerView_fastforwardStretchIcon:I = 0xa

.field public static final PlaybackControllerView_ffwRwActiveAlpha:I = 0x10

.field public static final PlaybackControllerView_ffwRwAlpha:I = 0xf

.field public static final PlaybackControllerView_nextIcon:I = 0x6

.field public static final PlaybackControllerView_pauseIcon:I = 0xd

.field public static final PlaybackControllerView_playIcon:I = 0xc

.field public static final PlaybackControllerView_prevIcon:I = 0x7

.field public static final PlaybackControllerView_retryIcon:I = 0xe

.field public static final PlaybackControllerView_rewindIcon:I = 0x9

.field public static final PlaybackControllerView_rewindStretchIcon:I = 0xb

.field public static final PlaybackOverlay:[I

.field public static final RefcountImageView:[I

.field public static final ScrollAdapterView:[I

.field public static final ScrollAdapterView_dragOperationMode:I = 0xb

.field public static final ScrollAdapterView_expandedItemInAnim:I = 0x15

.field public static final ScrollAdapterView_expandedItemOutAnim:I = 0x16

.field public static final ScrollAdapterView_flingOperationMode:I = 0xa

.field public static final ScrollAdapterView_gridSetting:I = 0x2

.field public static final ScrollAdapterView_highItemTransform:I = 0x14

.field public static final ScrollAdapterView_lerperDivisor:I = 0xd

.field public static final ScrollAdapterView_lowItemTransform:I = 0x13

.field public static final ScrollAdapterView_navigateInAnimationAllowed:I = 0x19

.field public static final ScrollAdapterView_navigateOutAllowed:I = 0x17

.field public static final ScrollAdapterView_navigateOutOfOffAxisAllowed:I = 0x18

.field public static final ScrollAdapterView_orientation:I = 0x0

.field public static final ScrollAdapterView_scrollCenterDrawable:I = 0xc

.field public static final ScrollAdapterView_scrollCenterOffset:I = 0x7

.field public static final ScrollAdapterView_scrollCenterOffsetPercent:I = 0x8

.field public static final ScrollAdapterView_scrollCenterStrategy:I = 0x6

.field public static final ScrollAdapterView_scrollItemAlign:I = 0x9

.field public static final ScrollAdapterView_selectedSize:I = 0x4

.field public static final ScrollAdapterView_selectedTakesMoreSpace:I = 0x5

.field public static final ScrollAdapterView_space:I = 0x3

.field public static final ScrollAdapterView_trackpadLockAxis:I = 0x12

.field public static final ScrollAdapterView_trackpadNavigationEnabled:I = 0x1a

.field public static final ScrollAdapterView_trackpadNavigationTiltEnabled:I = 0x1b

.field public static final ScrollAdapterView_trackpadOvershootProtection:I = 0x11

.field public static final ScrollAdapterView_trackpadSensitivityX:I = 0xf

.field public static final ScrollAdapterView_trackpadSensitivityY:I = 0x10

.field public static final ScrollAdapterView_trackpadThreshold:I = 0xe

.field public static final SeekButton:[I

.field public static final SeekButton_inactiveScale:I = 0x7

.field public static final SeekButton_seekActiveAlpha:I = 0x1

.field public static final SeekButton_seekAlpha:I = 0x0

.field public static final SeekButton_seekDirection:I = 0x6

.field public static final SeekButton_seekIcon:I = 0x2

.field public static final SeekButton_seekStretchIcon:I = 0x3

.field public static final SeekButton_seekTextColor:I = 0x4

.field public static final SeekButton_seekTextSize:I = 0x5

.field public static final SteppedProgressBar:[I

.field public static final TextAppearanceAlias:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 2344
    new-array v0, v4, [I

    const v1, 0x7f010027

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/pano/R$styleable;->AutoScaleImageView:[I

    .line 2378
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/pano/R$styleable;->ConstrainedLinearLayout:[I

    .line 2431
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/pano/R$styleable;->FrameLayoutWithShadows:[I

    .line 2472
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/pano/R$styleable;->InlineKeyboard:[I

    .line 2545
    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/pano/R$styleable;->PlaybackControllerView:[I

    .line 2785
    new-array v0, v5, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/pano/R$styleable;->PlaybackOverlay:[I

    .line 2842
    new-array v0, v4, [I

    const v1, 0x7f01004c

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/pano/R$styleable;->RefcountImageView:[I

    .line 2936
    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/pano/R$styleable;->ScrollAdapterView:[I

    .line 3462
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/pano/R$styleable;->SeekButton:[I

    .line 3592
    new-array v0, v5, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/pano/R$styleable;->SteppedProgressBar:[I

    .line 3661
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/pano/R$styleable;->TextAppearanceAlias:[I

    return-void

    .line 2378
    nop

    :array_0
    .array-data 4
        0x7f010028
        0x7f010029
    .end array-data

    .line 2431
    :array_1
    .array-data 4
        0x7f01002a
        0x7f01002b
    .end array-data

    .line 2472
    :array_2
    .array-data 4
        0x7f01002c
        0x7f01002d
    .end array-data

    .line 2545
    :array_3
    .array-data 4
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
    .end array-data

    .line 2785
    :array_4
    .array-data 4
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
    .end array-data

    .line 2936
    :array_5
    .array-data 4
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
    .end array-data

    .line 3462
    :array_6
    .array-data 4
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
    .end array-data

    .line 3592
    :array_7
    .array-data 4
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
    .end array-data

    .line 3661
    :array_8
    .array-data 4
        0x1010095
        0x1010096
        0x1010097
        0x1010098
        0x101038c
        0x10103ac
    .end array-data
.end method
