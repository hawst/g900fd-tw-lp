.class public Lcom/google/android/pano/widget/PlaybackControlsView;
.super Lcom/google/android/pano/widget/AbsControlsView;
.source "PlaybackControlsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;,
        Lcom/google/android/pano/widget/PlaybackControlsView$Listener;
    }
.end annotation


# static fields
.field private static final ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;


# instance fields
.field protected mCapabilities:I

.field protected mDuration:I

.field protected mDurationView:Landroid/widget/TextView;

.field protected mFfwView:Lcom/google/android/pano/widget/SeekButton;

.field protected mHandledDown:Z

.field protected mHandler:Landroid/os/Handler;

.field protected mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

.field protected mJoystickTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

.field protected mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

.field protected mNextDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

.field protected mNextView:Landroid/widget/ImageButton;

.field protected mOldSeekValue:F

.field protected mPadTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

.field protected mPauseDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

.field protected mPlayDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

.field protected mPlayView:Landroid/widget/ImageButton;

.field protected mPlaystate:I

.field protected mPosition:I

.field protected mPositionUpdateTime:J

.field protected mPositionView:Landroid/widget/TextView;

.field protected mPrevBehavior:I

.field protected mPrevDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

.field protected mPrevSeconds:I

.field protected mPrevView:Landroid/widget/ImageButton;

.field protected mProgressDrawable:Landroid/graphics/drawable/Drawable;

.field protected mRes:Landroid/content/res/Resources;

.field protected mResumed:Z

.field protected mRetryDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

.field protected mRwView:Lcom/google/android/pano/widget/SeekButton;

.field protected mSeekBar:Landroid/widget/SeekBar;

.field private mSeekButtonPosition:F

.field protected mSeekHasFocus:Z

.field protected mSupportsFfw:Z

.field private mSupportsNext:Z

.field private mSupportsPrev:Z

.field protected mSupportsRw:Z

.field protected mThumbDrawable:Landroid/graphics/drawable/Drawable;

.field protected mUpdatePeriod:I

.field private mUpdatePlayPauseRunnable:Ljava/lang/Runnable;

.field private mUpdatePositionRunnable:Ljava/lang/Runnable;

.field private mUpdateViewsRunnable:Ljava/lang/Runnable;

.field protected mUpdatingPositionText:Z

.field protected mViewsReady:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 159
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    sput-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    .line 160
    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    new-instance v1, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v1}, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;-><init>()V

    aput-object v1, v0, v2

    .line 161
    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    aget-object v0, v0, v2

    const/16 v1, 0xff

    iput v1, v0, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;->alpha:I

    .line 162
    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    aget-object v0, v0, v2

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;->scale:F

    .line 163
    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    aget-object v1, v0, v2

    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ENABLED_FOCUSED_STATE_SET:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, v1, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;->state:[I

    .line 164
    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    new-instance v1, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v1}, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;-><init>()V

    aput-object v1, v0, v3

    .line 165
    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    aget-object v0, v0, v3

    const/16 v1, 0x64

    iput v1, v0, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;->alpha:I

    .line 166
    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    aget-object v0, v0, v3

    const v1, 0x3f3ae148    # 0.73f

    iput v1, v0, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;->scale:F

    .line 167
    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    aget-object v1, v0, v3

    sget-object v0, Lcom/google/android/pano/widget/PlaybackControlsView;->EMPTY_STATE_SET:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, v1, Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;->state:[I

    .line 168
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/pano/widget/PlaybackControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 263
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 266
    invoke-direct {p0, p1, p2}, Lcom/google/android/pano/widget/AbsControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 192
    new-instance v0, Lcom/google/android/pano/widget/PlaybackControlsView$1;

    invoke-direct {v0, p0}, Lcom/google/android/pano/widget/PlaybackControlsView$1;-><init>(Lcom/google/android/pano/widget/PlaybackControlsView;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatePositionRunnable:Ljava/lang/Runnable;

    .line 199
    new-instance v0, Lcom/google/android/pano/widget/PlaybackControlsView$2;

    invoke-direct {v0, p0}, Lcom/google/android/pano/widget/PlaybackControlsView$2;-><init>(Lcom/google/android/pano/widget/PlaybackControlsView;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdateViewsRunnable:Ljava/lang/Runnable;

    .line 206
    new-instance v0, Lcom/google/android/pano/widget/PlaybackControlsView$3;

    invoke-direct {v0, p0}, Lcom/google/android/pano/widget/PlaybackControlsView$3;-><init>(Lcom/google/android/pano/widget/PlaybackControlsView;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatePlayPauseRunnable:Ljava/lang/Runnable;

    .line 233
    iput v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlaystate:I

    .line 234
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mOldSeekValue:F

    .line 236
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatePeriod:I

    .line 237
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionUpdateTime:J

    .line 238
    iput v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPosition:I

    .line 239
    iput v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    .line 240
    iput v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevSeconds:I

    .line 242
    new-instance v0, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    invoke-direct {v0, p0}, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;-><init>(Lcom/google/android/pano/widget/PlaybackControlsView;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mJoystickTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    .line 243
    new-instance v0, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    invoke-direct {v0, p0}, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;-><init>(Lcom/google/android/pano/widget/PlaybackControlsView;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPadTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    .line 244
    new-instance v0, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    invoke-direct {v0, p0}, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;-><init>(Lcom/google/android/pano/widget/PlaybackControlsView;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    .line 248
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandler:Landroid/os/Handler;

    .line 254
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandledDown:Z

    .line 255
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekHasFocus:Z

    .line 256
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mResumed:Z

    .line 259
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatingPositionText:Z

    .line 267
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->setupViews()V

    .line 268
    return-void
.end method

.method private cancelJoystickSeek()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 710
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mJoystickTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget-boolean v1, v1, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    if-eqz v1, :cond_0

    .line 711
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->onSeek(IF)V

    .line 712
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mJoystickTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iput-boolean v0, v1, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    .line 713
    const/4 v0, 0x1

    .line 715
    :cond_0
    return v0
.end method

.method private sendOnSeek(IF)V
    .locals 4
    .param p1, "direction"    # I
    .param p2, "magnitude"    # F

    .prologue
    .line 772
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mListeners:Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 774
    .local v2, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/pano/widget/AbsControlsView$Listener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/pano/widget/AbsControlsView$Listener;

    .line 775
    .local v1, "listener":Lcom/google/android/pano/widget/AbsControlsView$Listener;
    instance-of v3, v1, Lcom/google/android/pano/widget/PlaybackControlsView$Listener;

    if-eqz v3, :cond_0

    .line 776
    check-cast v1, Lcom/google/android/pano/widget/PlaybackControlsView$Listener;

    .end local v1    # "listener":Lcom/google/android/pano/widget/AbsControlsView$Listener;
    invoke-interface {v1, p1, p2}, Lcom/google/android/pano/widget/PlaybackControlsView$Listener;->onSeek(IF)V

    goto :goto_0

    .line 779
    :cond_1
    return-void
.end method


# virtual methods
.method protected cancelPadSeek()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 719
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPadTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget-boolean v1, v1, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    if-eqz v1, :cond_0

    .line 720
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->onSeek(IF)V

    .line 721
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPadTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iput-boolean v0, v1, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    .line 722
    const/4 v0, 0x1

    .line 724
    :cond_0
    return v0
.end method

.method public cancelSeek()Z
    .locals 2

    .prologue
    .line 704
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->cancelPadSeek()Z

    move-result v0

    .line 705
    .local v0, "result":Z
    invoke-direct {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->cancelJoystickSeek()Z

    move-result v1

    or-int/2addr v0, v1

    .line 706
    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v8, -0x40800000    # -1.0f

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 615
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getSource()I

    move-result v5

    const/high16 v6, 0x200000

    and-int/2addr v5, v6

    const/high16 v6, 0x200000

    if-ne v5, v6, :cond_0

    .line 619
    invoke-super {p0, p1}, Lcom/google/android/pano/widget/AbsControlsView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    .line 666
    :goto_0
    return v4

    .line 621
    :cond_0
    const/4 v0, 0x0

    .line 622
    .local v0, "diff":F
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-ne v5, v4, :cond_1

    iget-boolean v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandledDown:Z

    if-eqz v5, :cond_1

    .line 623
    iput-boolean v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandledDown:Z

    goto :goto_0

    .line 626
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayView:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->hasFocus()Z

    move-result v5

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekHasFocus:Z

    if-eqz v5, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->handlesHorizontalGestures()Z

    move-result v5

    if-nez v5, :cond_4

    .line 629
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/pano/widget/AbsControlsView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    goto :goto_0

    .line 631
    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 632
    .local v1, "keyCode":I
    const/16 v5, 0x15

    if-ne v1, v5, :cond_6

    .line 633
    const v0, -0x41b33333    # -0.2f

    .line 637
    :cond_5
    :goto_1
    cmpl-float v5, v0, v7

    if-eqz v5, :cond_c

    .line 638
    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v6, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    add-float/2addr v6, v0

    iput v6, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    .line 639
    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v5, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    cmpg-float v5, v5, v7

    if-gez v5, :cond_7

    iget-boolean v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsRw:Z

    if-nez v5, :cond_7

    .line 640
    iget-object v4, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget-boolean v2, v4, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    .line 641
    .local v2, "wasSeeking":Z
    invoke-virtual {p0, v3, v7}, Lcom/google/android/pano/widget/PlaybackControlsView;->onSeek(IF)V

    .line 642
    invoke-virtual {p0, v7}, Lcom/google/android/pano/widget/PlaybackControlsView;->setSeekPosition(F)V

    .line 643
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandledDown:Z

    move v4, v2

    .line 644
    goto :goto_0

    .line 634
    .end local v2    # "wasSeeking":Z
    :cond_6
    const/16 v5, 0x16

    if-ne v1, v5, :cond_5

    .line 635
    const v0, 0x3e4ccccd    # 0.2f

    goto :goto_1

    .line 646
    :cond_7
    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v5, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_8

    iget-boolean v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsFfw:Z

    if-nez v5, :cond_8

    .line 647
    iget-object v4, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget-boolean v2, v4, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    .line 648
    .restart local v2    # "wasSeeking":Z
    invoke-virtual {p0, v3, v7}, Lcom/google/android/pano/widget/PlaybackControlsView;->onSeek(IF)V

    .line 649
    invoke-virtual {p0, v7}, Lcom/google/android/pano/widget/PlaybackControlsView;->setSeekPosition(F)V

    .line 650
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandledDown:Z

    move v4, v2

    .line 651
    goto :goto_0

    .line 654
    .end local v2    # "wasSeeking":Z
    :cond_8
    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v5, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    cmpl-float v5, v5, v9

    if-lez v5, :cond_b

    .line 655
    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iput v9, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    .line 659
    :cond_9
    :goto_2
    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget-object v6, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v6, v6, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_a

    move v3, v4

    :cond_a
    iput-boolean v3, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    .line 660
    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v3, v3, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    float-to-int v3, v3

    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v5, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-virtual {p0, v3, v5}, Lcom/google/android/pano/widget/PlaybackControlsView;->onSeek(IF)V

    .line 661
    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v3, v3, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    invoke-virtual {p0, v3}, Lcom/google/android/pano/widget/PlaybackControlsView;->setSeekPosition(F)V

    .line 662
    iput-boolean v4, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandledDown:Z

    goto/16 :goto_0

    .line 656
    :cond_b
    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v5, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    cmpg-float v5, v5, v8

    if-gez v5, :cond_9

    .line 657
    iget-object v5, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iput v8, v5, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    goto :goto_2

    .line 665
    :cond_c
    iput-boolean v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandledDown:Z

    .line 666
    invoke-super {p0, p1}, Lcom/google/android/pano/widget/AbsControlsView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    goto/16 :goto_0
.end method

.method protected focusDefaultView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 907
    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekHasFocus:Z

    if-eqz v0, :cond_2

    .line 908
    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsRw:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekButtonPosition:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 909
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0}, Lcom/google/android/pano/widget/SeekButton;->requestFocus()Z

    .line 916
    :cond_0
    :goto_0
    return-void

    .line 910
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsFfw:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekButtonPosition:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 911
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0}, Lcom/google/android/pano/widget/SeekButton;->requestFocus()Z

    goto :goto_0

    .line 914
    :cond_2
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayView:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0
.end method

.method protected handleJoystickEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 728
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    .line 729
    .local v1, "newJoystickX":F
    iget-object v4, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mJoystickTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iget v4, v4, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    cmpl-float v4, v1, v4

    if-eqz v4, :cond_1

    .line 730
    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mJoystickTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iput v1, v3, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    .line 731
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 732
    .local v0, "absJoystickX":F
    const v3, 0x3e4ccccd    # 0.2f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_0

    .line 733
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/pano/widget/PlaybackControlsView;->setSeekPosition(F)V

    .line 734
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->cancelSeek()Z

    move-result v2

    .line 742
    .end local v0    # "absJoystickX":F
    :goto_0
    return v2

    .line 736
    .restart local v0    # "absJoystickX":F
    :cond_0
    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p0, v3, v0}, Lcom/google/android/pano/widget/PlaybackControlsView;->onSeek(IF)V

    .line 737
    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mJoystickTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    iput-boolean v2, v3, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    .line 738
    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->setSeekPosition(F)V

    goto :goto_0

    .end local v0    # "absJoystickX":F
    :cond_1
    move v2, v3

    .line 742
    goto :goto_0
.end method

.method public handlesHorizontalGestures()Z
    .locals 1

    .prologue
    .line 523
    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsRw:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsFfw:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initDefaults()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 783
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    .line 784
    iput v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mCapabilities:I

    .line 785
    iput v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevBehavior:I

    .line 786
    new-instance v0, Lcom/google/android/pano/widget/StateScaledDrawable;

    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/pano/R$drawable;->ic_playback_rwd:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v0, v1, v2}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 788
    new-instance v0, Lcom/google/android/pano/widget/StateScaledDrawable;

    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/pano/R$drawable;->ic_playback_fwd:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v0, v1, v2}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 790
    new-instance v0, Lcom/google/android/pano/widget/StateScaledDrawable;

    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/pano/R$drawable;->ic_playback_pause:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v0, v1, v2}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPauseDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 792
    new-instance v0, Lcom/google/android/pano/widget/StateScaledDrawable;

    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/pano/R$drawable;->ic_playback_play:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v0, v1, v2}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 794
    new-instance v0, Lcom/google/android/pano/widget/StateScaledDrawable;

    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/pano/R$drawable;->ic_playback_replay:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v0, v1, v2}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    iput-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRetryDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 796
    return-void
.end method

.method protected initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 800
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 801
    .local v10, "res":Landroid/content/res/Resources;
    sget-object v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView:[I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 803
    .local v2, "a":Landroid/content/res/TypedArray;
    const/4 v5, 0x0

    .line 804
    .local v5, "capabilities":I
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_allowFastForward:I

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 805
    or-int/lit8 v5, v5, 0x40

    .line 808
    :cond_0
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_allowNext:I

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 809
    or-int/lit16 v5, v5, 0x80

    .line 812
    :cond_1
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_allowPlay:I

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 813
    or-int/lit8 v5, v5, 0x4

    .line 816
    :cond_2
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_allowPause:I

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 817
    or-int/lit8 v5, v5, 0x10

    .line 820
    :cond_3
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_allowPrevious:I

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 821
    or-int/lit8 v5, v5, 0x1

    .line 824
    :cond_4
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_allowRewind:I

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 825
    or-int/lit8 v5, v5, 0x2

    .line 828
    :cond_5
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_nextIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 829
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_nextIcon:I

    sget v13, Lcom/google/android/pano/R$drawable;->ic_playback_fwd:I

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    .line 830
    .local v6, "nextRes":I
    new-instance v12, Lcom/google/android/pano/widget/StateScaledDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v13, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    sget-object v14, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v12, v13, v14}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 834
    .end local v6    # "nextRes":I
    :cond_6
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_prevIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 835
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_prevIcon:I

    sget v13, Lcom/google/android/pano/R$drawable;->ic_playback_rwd:I

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    .line 836
    .local v9, "prevRes":I
    new-instance v12, Lcom/google/android/pano/widget/StateScaledDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v13, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    sget-object v14, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v12, v13, v14}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 840
    .end local v9    # "prevRes":I
    :cond_7
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_fastforwardIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 841
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    const/4 v13, 0x0

    sget v14, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_fastforwardIcon:I

    sget v15, Lcom/google/android/pano/R$drawable;->ic_playback_scrub_fwd:I

    invoke-virtual {v2, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    invoke-virtual {v12, v13, v14}, Lcom/google/android/pano/widget/SeekButton;->setResource(II)V

    .line 846
    :cond_8
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_rewindIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 847
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    const/4 v13, 0x0

    sget v14, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_rewindIcon:I

    sget v15, Lcom/google/android/pano/R$drawable;->ic_playback_scrub_rwd:I

    invoke-virtual {v2, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    invoke-virtual {v12, v13, v14}, Lcom/google/android/pano/widget/SeekButton;->setResource(II)V

    .line 852
    :cond_9
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_playIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 853
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_playIcon:I

    sget v13, Lcom/google/android/pano/R$drawable;->ic_playback_play:I

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    .line 855
    .local v8, "playRes":I
    new-instance v12, Lcom/google/android/pano/widget/StateScaledDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v13, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    sget-object v14, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v12, v13, v14}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 859
    .end local v8    # "playRes":I
    :cond_a
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_pauseIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 860
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_pauseIcon:I

    sget v13, Lcom/google/android/pano/R$drawable;->ic_playback_pause:I

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 862
    .local v7, "pauseRes":I
    new-instance v12, Lcom/google/android/pano/widget/StateScaledDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v13, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    sget-object v14, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v12, v13, v14}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPauseDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 866
    .end local v7    # "pauseRes":I
    :cond_b
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_ffwRwAlpha:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 867
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_ffwRwAlpha:I

    const/16 v13, 0x66

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    .line 869
    .local v4, "alpha":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v12, v4}, Lcom/google/android/pano/widget/SeekButton;->setButtonAlpha(I)V

    .line 870
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v12, v4}, Lcom/google/android/pano/widget/SeekButton;->setButtonAlpha(I)V

    .line 873
    .end local v4    # "alpha":I
    :cond_c
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_ffwRwActiveAlpha:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 874
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_ffwRwActiveAlpha:I

    const/16 v13, 0xff

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    .line 876
    .local v3, "activeAlpha":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v12, v3}, Lcom/google/android/pano/widget/SeekButton;->setButtonActiveAlpha(I)V

    .line 877
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v12, v3}, Lcom/google/android/pano/widget/SeekButton;->setButtonActiveAlpha(I)V

    .line 880
    .end local v3    # "activeAlpha":I
    :cond_d
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_fastforwardStretchIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 881
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    const/4 v13, 0x1

    sget v14, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_fastforwardStretchIcon:I

    sget v15, Lcom/google/android/pano/R$drawable;->ic_playback_scrubber_line:I

    invoke-virtual {v2, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    invoke-virtual {v12, v13, v14}, Lcom/google/android/pano/widget/SeekButton;->setResource(II)V

    .line 886
    :cond_e
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_rewindStretchIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 887
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    const/4 v13, 0x1

    sget v14, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_rewindStretchIcon:I

    sget v15, Lcom/google/android/pano/R$drawable;->ic_playback_scrubber_line_reverse:I

    invoke-virtual {v2, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    invoke-virtual {v12, v13, v14}, Lcom/google/android/pano/widget/SeekButton;->setResource(II)V

    .line 892
    :cond_f
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_retryIcon:I

    invoke-virtual {v2, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 893
    sget v12, Lcom/google/android/pano/R$styleable;->PlaybackControllerView_retryIcon:I

    sget v13, Lcom/google/android/pano/R$drawable;->ic_playback_replay:I

    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    .line 895
    .local v11, "retryRes":I
    new-instance v12, Lcom/google/android/pano/widget/StateScaledDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v13, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    sget-object v14, Lcom/google/android/pano/widget/PlaybackControlsView;->ICON_STATE_PROPS:[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;

    invoke-direct {v12, v13, v14}, Lcom/google/android/pano/widget/StateScaledDrawable;-><init>(Landroid/graphics/drawable/Drawable;[Lcom/google/android/pano/widget/StateScaledDrawable$StateProperties;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRetryDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 899
    .end local v11    # "retryRes":I
    :cond_10
    if-eqz v5, :cond_11

    .line 900
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/pano/widget/PlaybackControlsView;->setCapabilities(I)V

    .line 902
    :cond_11
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 903
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 506
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/pano/widget/PlaybackControlsView;->sendOnClick(I)V

    .line 507
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->handlesHorizontalGestures()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 513
    invoke-virtual {p0, p1}, Lcom/google/android/pano/widget/PlaybackControlsView;->handleJoystickEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    const/4 v0, 0x1

    .line 518
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/pano/widget/AbsControlsView;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSeek(IF)V
    .locals 9
    .param p1, "direction"    # I
    .param p2, "magnitude"    # F

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 746
    int-to-float v4, p1

    mul-float v0, v4, p2

    .line 747
    .local v0, "seekValue":F
    iget v4, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mOldSeekValue:F

    sub-float v4, v0, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    cmpl-double v4, v4, v6

    if-lez v4, :cond_0

    move v1, v2

    .line 748
    .local v1, "significantMove":Z
    :goto_0
    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    .line 769
    :goto_1
    return-void

    .end local v1    # "significantMove":Z
    :cond_0
    move v1, v3

    .line 747
    goto :goto_0

    .line 753
    .restart local v1    # "significantMove":Z
    :cond_1
    iget v4, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mOldSeekValue:F

    cmpl-float v4, v4, v8

    if-nez v4, :cond_3

    cmpl-float v4, v0, v8

    if-eqz v4, :cond_3

    .line 754
    invoke-virtual {p0, v2}, Lcom/google/android/pano/widget/PlaybackControlsView;->sendOnActiveChange(Z)V

    .line 758
    :cond_2
    :goto_2
    iput v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mOldSeekValue:F

    .line 759
    invoke-direct {p0, p1, p2}, Lcom/google/android/pano/widget/PlaybackControlsView;->sendOnSeek(IF)V

    .line 763
    iget-object v4, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    if-eqz p1, :cond_4

    :goto_3
    iput-boolean v2, v4, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->seeking:Z

    .line 764
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mKeyboardTracker:Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;

    const/high16 v3, 0x40a00000    # 5.0f

    mul-float/2addr v3, p2

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4014000000000000L    # 5.0

    div-double/2addr v4, v6

    int-to-double v6, p1

    mul-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, v2, Lcom/google/android/pano/widget/PlaybackControlsView$SeekTracker;->value:F

    goto :goto_1

    .line 755
    :cond_3
    iget v4, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mOldSeekValue:F

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_2

    cmpl-float v4, v0, v8

    if-nez v4, :cond_2

    .line 756
    invoke-virtual {p0, v3}, Lcom/google/android/pano/widget/PlaybackControlsView;->sendOnActiveChange(Z)V

    goto :goto_2

    :cond_4
    move v2, v3

    .line 763
    goto :goto_3
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 920
    if-ne p0, p1, :cond_0

    if-eqz p2, :cond_0

    .line 921
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->cancelSeek()Z

    .line 923
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/pano/widget/AbsControlsView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 924
    return-void
.end method

.method protected remeasurePositionView()V
    .locals 11

    .prologue
    const/high16 v10, -0x80000000

    .line 1073
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getPaddingLeft()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getPaddingRight()I

    move-result v9

    sub-int v7, v8, v9

    .line 1074
    .local v7, "width":I
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getPaddingTop()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getPaddingBottom()I

    move-result v9

    sub-int v1, v8, v9

    .line 1075
    .local v1, "childHeight":I
    int-to-float v8, v7

    const v9, 0x3f4ccccd    # 0.8f

    mul-float/2addr v8, v9

    float-to-int v8, v8

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1077
    .local v3, "childWidthSpec":I
    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1078
    .local v2, "childHeightSpec":I
    iget-object v8, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    invoke-virtual {v8, v3, v2}, Landroid/widget/TextView;->measure(II)V

    .line 1079
    iget-object v8, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getLeft()I

    move-result v4

    .line 1080
    .local v4, "left":I
    iget-object v8, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getBottom()I

    move-result v0

    .line 1081
    .local v0, "bottom":I
    iget-object v8, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v8

    add-int v5, v4, v8

    .line 1082
    .local v5, "right":I
    iget-object v8, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    sub-int v6, v0, v8

    .line 1083
    .local v6, "top":I
    iget-object v8, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    invoke-virtual {v8, v4, v6, v5, v0}, Landroid/widget/TextView;->layout(IIII)V

    .line 1084
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 678
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->focusDefaultView()V

    .line 679
    const/4 v0, 0x1

    return v0
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 671
    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatingPositionText:Z

    if-nez v0, :cond_0

    .line 672
    invoke-super {p0}, Lcom/google/android/pano/widget/AbsControlsView;->requestLayout()V

    .line 674
    :cond_0
    return-void
.end method

.method public setCapabilities(I)V
    .locals 2
    .param p1, "capabilities"    # I

    .prologue
    .line 286
    iput p1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mCapabilities:I

    .line 287
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdateViewsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 288
    return-void
.end method

.method public setSeekPosition(F)V
    .locals 4
    .param p1, "position"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 470
    const v0, 0x3e19999a    # 0.15f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsFfw:Z

    if-eqz v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0, v1}, Lcom/google/android/pano/widget/SeekButton;->setFocusable(Z)V

    .line 473
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0}, Lcom/google/android/pano/widget/SeekButton;->requestFocus()Z

    .line 474
    iput-boolean v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekHasFocus:Z

    .line 487
    :cond_0
    :goto_0
    iput p1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekButtonPosition:F

    .line 488
    iget v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekButtonPosition:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3da3d70a    # 0.08f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 489
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0, v2}, Lcom/google/android/pano/widget/SeekButton;->updatePosition(F)V

    .line 490
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0, v2}, Lcom/google/android/pano/widget/SeekButton;->updatePosition(F)V

    .line 498
    :goto_1
    return-void

    .line 475
    :cond_1
    const v0, -0x41e66666    # -0.15f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsRw:Z

    if-eqz v0, :cond_2

    .line 477
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0, v1}, Lcom/google/android/pano/widget/SeekButton;->setFocusable(Z)V

    .line 478
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0}, Lcom/google/android/pano/widget/SeekButton;->requestFocus()Z

    .line 479
    iput-boolean v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekHasFocus:Z

    goto :goto_0

    .line 480
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekHasFocus:Z

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0, v3}, Lcom/google/android/pano/widget/SeekButton;->setFocusable(Z)V

    .line 483
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0, v3}, Lcom/google/android/pano/widget/SeekButton;->setFocusable(Z)V

    .line 484
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayView:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 485
    iput-boolean v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekHasFocus:Z

    goto :goto_0

    .line 491
    :cond_3
    iget v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekButtonPosition:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    .line 492
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    iget v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekButtonPosition:F

    invoke-virtual {v0, v1}, Lcom/google/android/pano/widget/SeekButton;->updatePosition(F)V

    .line 493
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0, v2}, Lcom/google/android/pano/widget/SeekButton;->updatePosition(F)V

    goto :goto_1

    .line 495
    :cond_4
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v0, v2}, Lcom/google/android/pano/widget/SeekButton;->updatePosition(F)V

    .line 496
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    iget v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekButtonPosition:F

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/pano/widget/SeekButton;->updatePosition(F)V

    goto :goto_1
.end method

.method protected setupViews()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 927
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 928
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v1, Lcom/google/android/pano/R$layout;->default_controller_view:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 930
    sget v1, Lcom/google/android/pano/R$id;->controller_prev:I

    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevView:Landroid/widget/ImageButton;

    .line 931
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevView:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 932
    sget v1, Lcom/google/android/pano/R$id;->controller_playpause:I

    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayView:Landroid/widget/ImageButton;

    .line 933
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayView:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 934
    sget v1, Lcom/google/android/pano/R$id;->controller_next:I

    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextView:Landroid/widget/ImageButton;

    .line 935
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextView:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 936
    sget v1, Lcom/google/android/pano/R$id;->controller_rw:I

    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/pano/widget/SeekButton;

    iput-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    .line 937
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v1, p0}, Lcom/google/android/pano/widget/SeekButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 938
    sget v1, Lcom/google/android/pano/R$id;->controller_ffw:I

    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/pano/widget/SeekButton;

    iput-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    .line 939
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    invoke-virtual {v1, p0}, Lcom/google/android/pano/widget/SeekButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 941
    sget v1, Lcom/google/android/pano/R$id;->controller_seekBar:I

    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    .line 942
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 943
    sget v1, Lcom/google/android/pano/R$id;->controller_time:I

    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    .line 944
    sget v1, Lcom/google/android/pano/R$id;->controller_duration:I

    invoke-virtual {p0, v1}, Lcom/google/android/pano/widget/PlaybackControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDurationView:Landroid/widget/TextView;

    .line 946
    iput-boolean v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mViewsReady:Z

    .line 947
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->updateViews()V

    .line 948
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->updatePlayPause()V

    .line 949
    invoke-virtual {p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->updatePosition()V

    .line 950
    return-void
.end method

.method protected updatePlayPause()V
    .locals 2

    .prologue
    .line 953
    iget-boolean v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mViewsReady:Z

    if-nez v1, :cond_1

    .line 975
    :cond_0
    :goto_0
    return-void

    .line 956
    :cond_1
    const/4 v0, 0x0

    .line 958
    .local v0, "drawable":Lcom/google/android/pano/widget/StateScaledDrawable;
    iget v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlaystate:I

    packed-switch v1, :pswitch_data_0

    .line 972
    :goto_1
    :pswitch_0
    if-eqz v0, :cond_0

    .line 973
    iget-object v1, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayView:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 960
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 961
    goto :goto_1

    .line 963
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPauseDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 964
    goto :goto_1

    .line 966
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRetryDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    .line 967
    goto :goto_1

    .line 969
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlayDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    goto :goto_1

    .line 958
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected updatePosition()V
    .locals 22

    .prologue
    .line 982
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mViewsReady:Z

    move/from16 v17, v0

    if-nez v17, :cond_1

    .line 1070
    :cond_0
    :goto_0
    return-void

    .line 985
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDurationView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    const/16 v19, -0x1

    move/from16 v0, v17

    move/from16 v1, v19

    if-le v0, v1, :cond_8

    const/16 v17, 0x0

    :goto_1
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 986
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    const/16 v19, -0x1

    move/from16 v0, v17

    move/from16 v1, v19

    if-le v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPosition:I

    move/from16 v17, v0

    const/16 v19, -0x1

    move/from16 v0, v17

    move/from16 v1, v19

    if-le v0, v1, :cond_9

    const/16 v17, 0x0

    :goto_2
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 987
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPosition:I

    move/from16 v17, v0

    const/16 v19, -0x1

    move/from16 v0, v17

    move/from16 v1, v19

    if-le v0, v1, :cond_a

    const/16 v17, 0x0

    :goto_3
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 989
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/SeekBar;->getMax()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    .line 990
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/32 v20, 0x36ee80

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v6, v0

    .line 991
    .local v6, "hours":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/32 v20, 0x36ee80

    rem-long v18, v18, v20

    const-wide/32 v20, 0xea60

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v8, v0

    .line 993
    .local v8, "minutes":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/32 v20, 0xea60

    rem-long v18, v18, v20

    const-wide/16 v20, 0x3e8

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v13, v0

    .line 995
    .local v13, "seconds":I
    if-nez v6, :cond_b

    .line 996
    const-string v17, "%d:%02d"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1000
    .local v5, "duration":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDurationView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1001
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1004
    .end local v5    # "duration":Ljava/lang/String;
    .end local v6    # "hours":I
    .end local v8    # "minutes":I
    .end local v13    # "seconds":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatePositionRunnable:Ljava/lang/Runnable;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1005
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPosition:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_0

    .line 1006
    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPosition:I

    .line 1007
    .local v10, "position":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlaystate:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionUpdateTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-lez v17, :cond_3

    .line 1008
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionUpdateTime:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v17, v0

    add-int v10, v10, v17

    .line 1010
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v10, v0, :cond_4

    .line 1011
    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    .line 1014
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/SeekBar;->getProgress()I

    move-result v17

    move/from16 v0, v17

    if-eq v10, v0, :cond_7

    .line 1015
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1016
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatingPositionText:Z

    .line 1017
    div-int/lit16 v14, v10, 0x3e8

    .line 1019
    .local v14, "totalSeconds":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevSeconds:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v14, :cond_5

    .line 1020
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevSeconds:I

    .line 1021
    int-to-long v0, v10

    move-wide/from16 v18, v0

    const-wide/32 v20, 0x36ee80

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v6, v0

    .line 1022
    .restart local v6    # "hours":I
    int-to-long v0, v10

    move-wide/from16 v18, v0

    const-wide/32 v20, 0x36ee80

    rem-long v18, v18, v20

    const-wide/32 v20, 0xea60

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v8, v0

    .line 1024
    .restart local v8    # "minutes":I
    int-to-long v0, v10

    move-wide/from16 v18, v0

    const-wide/32 v20, 0xea60

    rem-long v18, v18, v20

    const-wide/16 v20, 0x3e8

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v13, v0

    .line 1026
    .restart local v13    # "seconds":I
    if-nez v6, :cond_c

    .line 1027
    const-string v17, "%d:%02d"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 1032
    .local v11, "positionString":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1033
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->remeasurePositionView()V

    .line 1036
    .end local v6    # "hours":I
    .end local v8    # "minutes":I
    .end local v11    # "positionString":Ljava/lang/String;
    .end local v13    # "seconds":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/SeekBar;->getWidth()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v18

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v16, v0

    .line 1038
    .local v16, "width":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/SeekBar;->getLeft()I

    move-result v18

    add-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v9, v0

    .line 1039
    .local v9, "offset":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getWidth()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v18

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v12, v0

    .line 1042
    .local v12, "positionWidth":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_d

    .line 1043
    int-to-float v0, v10

    move/from16 v17, v0

    mul-float v17, v17, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mDuration:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    const/high16 v18, 0x40000000    # 2.0f

    div-float v18, v12, v18

    sub-float v17, v17, v18

    add-float v15, v17, v9

    .line 1047
    .local v15, "translation":F
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getWidth()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getWidth()I

    move-result v18

    sub-int v17, v17, v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getPaddingLeft()I

    move-result v18

    sub-int v17, v17, v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/pano/widget/PlaybackControlsView;->getPaddingRight()I

    move-result v18

    sub-int v7, v17, v18

    .line 1049
    .local v7, "maxTranslation":I
    cmpg-float v17, v15, v9

    if-gez v17, :cond_e

    .line 1050
    move v15, v9

    .line 1054
    :cond_6
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 1055
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatingPositionText:Z

    .line 1058
    .end local v7    # "maxTranslation":I
    .end local v9    # "offset":F
    .end local v12    # "positionWidth":F
    .end local v14    # "totalSeconds":I
    .end local v15    # "translation":F
    .end local v16    # "width":F
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mResumed:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPlaystate:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPositionUpdateTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-lez v17, :cond_0

    .line 1065
    rem-int/lit16 v0, v10, 0x3e8

    move/from16 v17, v0

    move/from16 v0, v17

    rsub-int v0, v0, 0x3e8

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatePeriod:I

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1067
    .local v4, "delay":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/pano/widget/PlaybackControlsView;->mUpdatePositionRunnable:Ljava/lang/Runnable;

    move-object/from16 v18, v0

    int-to-long v0, v4

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 985
    .end local v4    # "delay":I
    .end local v10    # "position":I
    :cond_8
    const/16 v17, 0x4

    goto/16 :goto_1

    .line 986
    :cond_9
    const/16 v17, 0x4

    goto/16 :goto_2

    .line 987
    :cond_a
    const/16 v17, 0x4

    goto/16 :goto_3

    .line 998
    .restart local v6    # "hours":I
    .restart local v8    # "minutes":I
    .restart local v13    # "seconds":I
    :cond_b
    const-string v17, "%d:%02d:%02d"

    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "duration":Ljava/lang/String;
    goto/16 :goto_4

    .line 1029
    .end local v5    # "duration":Ljava/lang/String;
    .restart local v10    # "position":I
    .restart local v14    # "totalSeconds":I
    :cond_c
    const-string v17, "%d:%02d:%02d"

    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .restart local v11    # "positionString":Ljava/lang/String;
    goto/16 :goto_5

    .line 1045
    .end local v6    # "hours":I
    .end local v8    # "minutes":I
    .end local v11    # "positionString":Ljava/lang/String;
    .end local v13    # "seconds":I
    .restart local v9    # "offset":F
    .restart local v12    # "positionWidth":F
    .restart local v16    # "width":F
    :cond_d
    sub-float v17, v16, v12

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    add-float v15, v17, v9

    .restart local v15    # "translation":F
    goto/16 :goto_6

    .line 1051
    .restart local v7    # "maxTranslation":I
    :cond_e
    int-to-float v0, v7

    move/from16 v17, v0

    cmpl-float v17, v15, v17

    if-lez v17, :cond_6

    .line 1052
    int-to-float v15, v7

    goto/16 :goto_7
.end method

.method protected updateViews()V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1087
    iget-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mViewsReady:Z

    if-nez v2, :cond_1

    .line 1118
    :cond_0
    :goto_0
    return-void

    .line 1090
    :cond_1
    iget v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mCapabilities:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsRw:Z

    .line 1091
    iget v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mCapabilities:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    move v2, v3

    :goto_2
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsPrev:Z

    .line 1092
    iget-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsPrev:Z

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsRw:Z

    if-nez v2, :cond_6

    move v1, v3

    .line 1093
    .local v1, "usePrev":Z
    :goto_3
    iget-object v6, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevView:Landroid/widget/ImageButton;

    if-eqz v1, :cond_7

    move v2, v4

    :goto_4
    invoke-virtual {v6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1094
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevView:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1095
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevView:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1096
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevView:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mPrevDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1098
    iget v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mCapabilities:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_8

    move v2, v3

    :goto_5
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsFfw:Z

    .line 1099
    iget v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mCapabilities:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_9

    move v2, v3

    :goto_6
    iput-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsNext:Z

    .line 1100
    iget-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsNext:Z

    if-eqz v2, :cond_a

    iget-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsFfw:Z

    if-nez v2, :cond_a

    move v0, v3

    .line 1101
    .local v0, "useNext":Z
    :goto_7
    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextView:Landroid/widget/ImageButton;

    if-eqz v0, :cond_b

    move v2, v4

    :goto_8
    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1102
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextView:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1103
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextView:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1104
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextView:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mNextDrawable:Lcom/google/android/pano/widget/StateScaledDrawable;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1106
    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mRwView:Lcom/google/android/pano/widget/SeekButton;

    iget-boolean v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsRw:Z

    if-eqz v2, :cond_c

    move v2, v4

    :goto_9
    invoke-virtual {v3, v2}, Lcom/google/android/pano/widget/SeekButton;->setVisibility(I)V

    .line 1107
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mFfwView:Lcom/google/android/pano/widget/SeekButton;

    iget-boolean v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSupportsFfw:Z

    if-eqz v3, :cond_d

    :goto_a
    invoke-virtual {v2, v4}, Lcom/google/android/pano/widget/SeekButton;->setVisibility(I)V

    .line 1109
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    .line 1110
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 1112
    :cond_2
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_3

    .line 1113
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1115
    :cond_3
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 1116
    iget-object v2, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/google/android/pano/widget/PlaybackControlsView;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .end local v0    # "useNext":Z
    .end local v1    # "usePrev":Z
    :cond_4
    move v2, v4

    .line 1090
    goto/16 :goto_1

    :cond_5
    move v2, v4

    .line 1091
    goto/16 :goto_2

    :cond_6
    move v1, v4

    .line 1092
    goto/16 :goto_3

    .restart local v1    # "usePrev":Z
    :cond_7
    move v2, v5

    .line 1093
    goto/16 :goto_4

    :cond_8
    move v2, v4

    .line 1098
    goto :goto_5

    :cond_9
    move v2, v4

    .line 1099
    goto :goto_6

    :cond_a
    move v0, v4

    .line 1100
    goto :goto_7

    .restart local v0    # "useNext":Z
    :cond_b
    move v2, v5

    .line 1101
    goto :goto_8

    :cond_c
    move v2, v5

    .line 1106
    goto :goto_9

    :cond_d
    move v4, v5

    .line 1107
    goto :goto_a
.end method
