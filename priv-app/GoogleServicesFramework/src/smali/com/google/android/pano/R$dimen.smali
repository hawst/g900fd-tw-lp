.class public final Lcom/google/android/pano/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/pano/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_fragment_selector_min_height:I = 0x7f0900a3

.field public static final cursor_touch_size:I = 0x7f0900c8

.field public static final cursor_web_view_accel_padding:I = 0x7f0900c7

.field public static final cursor_web_view_bounce_rate:I = 0x7f0900c6

.field public static final cursor_web_view_draw_margin:I = 0x7f0900c5

.field public static final cursor_web_view_scroll_margin:I = 0x7f0900c4

.field public static final key_circle_size:I = 0x7f0900c3

.field public static final list_item_disabled_chevron_background_alpha:I = 0x7f090074

.field public static final list_item_disabled_description_text_alpha:I = 0x7f090073

.field public static final list_item_disabled_title_text_alpha:I = 0x7f090072

.field public static final list_item_selected_chevron_background_alpha:I = 0x7f09006e

.field public static final list_item_selected_description_text_alpha:I = 0x7f09006d

.field public static final list_item_selected_title_text_alpha:I = 0x7f09006c

.field public static final list_item_unselected_description_text_alpha:I = 0x7f090070

.field public static final list_item_unselected_text_alpha:I = 0x7f09006f

.field public static final list_item_vertical_padding:I = 0x7f090081

.field public static final pixels_per_mm:I = 0x7f0900c9

.field public static final playback_rate_text_padding:I = 0x7f0900c1

.field public static final playback_rate_text_size:I = 0x7f0900c0
