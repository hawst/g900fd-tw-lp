.class public Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;
.super Landroid/app/Fragment;
.source "OtaPanoSetupUpdateReady.java"


# instance fields
.field private mDescriptionText:Landroid/widget/TextView;

.field private mRestartingFormat:Ljava/lang/String;

.field private mSeconds:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mSeconds:I

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    if-eqz p1, :cond_0

    .line 42
    const-string v0, "seconds"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mSeconds:I

    .line 44
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "helium"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 54
    const v4, 0x7f03001e

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 55
    .local v1, "content":Landroid/view/View;
    const v4, 0x7f0f001e

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 56
    .local v0, "actionArea":Landroid/widget/FrameLayout;
    const v4, 0x7f0f001d

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 59
    .local v2, "descArea":Landroid/widget/FrameLayout;
    const v4, 0x7f030026

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 60
    .local v3, "description":Landroid/view/View;
    const v4, 0x7f0f001a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0a00f8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 62
    const v4, 0x7f0f001b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0a00f9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00fa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mRestartingFormat:Ljava/lang/String;

    .line 65
    const v4, 0x7f0f006b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mDescriptionText:Landroid/widget/TextView;

    .line 66
    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->setSecondsView()V

    .line 69
    return-object v1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    const-string v0, "seconds"

    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mSeconds:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 49
    return-void
.end method

.method public setSeconds(I)V
    .locals 0
    .param p1, "seconds"    # I

    .prologue
    .line 73
    if-gez p1, :cond_0

    .line 74
    const/4 p1, 0x0

    .line 75
    :cond_0
    iput p1, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mSeconds:I

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->setSecondsView()V

    .line 77
    return-void
.end method

.method public setSecondsView()V
    .locals 5

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mDescriptionText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mRestartingFormat:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mDescriptionText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mRestartingFormat:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->mSeconds:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :cond_0
    return-void
.end method
