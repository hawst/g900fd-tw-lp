.class public Lcom/google/android/gsf/update/OtaPanoSetupVerifying;
.super Landroid/app/Fragment;
.source "OtaPanoSetupVerifying.java"


# instance fields
.field private mPercent:I

.field private mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mPercent:I

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 50
    if-eqz p1, :cond_0

    .line 51
    const-string v0, "percent"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mPercent:I

    .line 53
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "helium"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 63
    const v3, 0x7f03001e

    invoke-virtual {p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 64
    .local v0, "content":Landroid/view/View;
    const v3, 0x7f0f001d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 67
    .local v1, "descArea":Landroid/widget/FrameLayout;
    const v3, 0x7f030027

    invoke-virtual {p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 68
    .local v2, "description":Landroid/view/View;
    const v3, 0x7f0f001a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0a00ef

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 70
    const v3, 0x7f0f001b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0a00fb

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 72
    const v3, 0x7f0f006e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0a00fc

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 74
    const v3, 0x7f0f0064

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mProgressBar:Landroid/widget/ProgressBar;

    .line 75
    iget-object v3, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 76
    iget-object v3, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mProgressBar:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mPercent:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 77
    iget-object v3, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 78
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 80
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    const-string v0, "percent"

    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mPercent:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    return-void
.end method

.method public setProgress(I)V
    .locals 2
    .param p1, "percent"    # I

    .prologue
    .line 84
    if-gez p1, :cond_2

    .line 85
    const/4 p1, 0x0

    .line 88
    :cond_0
    :goto_0
    iput p1, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mPercent:I

    .line 89
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mProgressBar:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;->mPercent:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 92
    :cond_1
    return-void

    .line 86
    :cond_2
    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    .line 87
    const/16 p1, 0x64

    goto :goto_0
.end method
