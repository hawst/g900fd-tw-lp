.class Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;
.super Landroid/os/AsyncTask;
.source "SystemUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/update/SystemUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mLastDownloadStatus:I

.field private mProvisioned:Z

.field private mRequiredSetup:Z

.field final synthetic this$0:Lcom/google/android/gsf/update/SystemUpdateService;


# direct methods
.method private constructor <init>(Lcom/google/android/gsf/update/SystemUpdateService;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 379
    iput-object p1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 381
    iput-boolean v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mProvisioned:Z

    .line 382
    iput-boolean v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mRequiredSetup:Z

    .line 1043
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;
    .param p2, "x1"    # Lcom/google/android/gsf/update/SystemUpdateService$1;

    .prologue
    .line 379
    invoke-direct {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;-><init>(Lcom/google/android/gsf/update/SystemUpdateService;)V

    return-void
.end method

.method private cancelUpdate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 938
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 939
    .local v0, "am":Landroid/app/AlarmManager;
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1500(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 941
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 942
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 943
    const-string v1, "SystemUpdateService"

    const-string v3, "cancelUpdate: cancelling verifier"

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;->cancel(Z)Z

    .line 945
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v1, v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1202(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    .line 947
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 949
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 950
    const-string v1, "SystemUpdateService"

    const-string v2, "cancelling current attempt"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 951
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/gsf/update/DownloadAttempt;->cancel(Z)Z

    .line 952
    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {v5}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;

    .line 954
    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gsf/update/DownloadAttempt;->clear(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 956
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_approved"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "install_approved"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "url"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "url_change"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_mobile"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "filename"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_id"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "verified"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "install_time"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "started_download"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "pending_filename"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "success_message"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "failure_message"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "verify_progress"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "status"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 974
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->cancelNotifications(Landroid/content/Context;)V

    .line 975
    return-void

    .line 947
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private checkDownload(Ljava/lang/String;I)Z
    .locals 12
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "urgency"    # I

    .prologue
    .line 691
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-static {v6, v7}, Lcom/google/android/gsf/update/SystemUpdateService;->whenMobileAllowed(Landroid/content/SharedPreferences;Landroid/content/Context;)J

    move-result-wide v4

    .line 693
    .local v4, "whenMobileAllowed":J
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-string v7, "connectivity"

    invoke-virtual {v6, v7}, Lcom/google/android/gsf/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 695
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 696
    .local v1, "ni":Landroid/net/NetworkInfo;
    const-string v7, "SystemUpdateService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "network: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "; metered: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "; mobile allowed: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v6, v4, v10

    if-gtz v6, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-lez v6, :cond_3

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v6

    if-eqz v6, :cond_3

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    if-eqz v6, :cond_0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/update/DownloadAttempt;->getCurrentStatus()I

    move-result v6

    if-eqz v6, :cond_3

    .line 706
    :cond_0
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 707
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/google/android/gsf/update/DownloadAttempt;->cancel(Z)Z

    .line 708
    const/4 v6, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;

    .line 710
    :cond_1
    const/4 v6, 0x1

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    .line 711
    const/4 v6, 0x0

    .line 773
    :goto_1
    return v6

    .line 696
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 714
    :cond_3
    const/4 v2, -0x1

    .line 715
    .local v2, "status":I
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    if-nez v6, :cond_8

    .line 716
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "update_token"

    invoke-static {v6, v7}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 717
    .local v3, "update_token":Ljava/lang/String;
    new-instance v6, Lcom/google/android/gsf/update/DownloadAttempt;

    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    iget-object v8, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-direct {v6, v7, v8, p1, v3}, Lcom/google/android/gsf/update/DownloadAttempt;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;

    .line 718
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/update/DownloadAttempt;->getCurrentStatus()I

    move-result v2

    .line 719
    const-string v6, "SystemUpdateService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "status is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    const/4 v6, 0x1

    if-ne v2, v6, :cond_5

    .line 721
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-nez v6, :cond_6

    .line 722
    :cond_4
    const-string v6, "SystemUpdateService"

    const-string v7, "network down"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    const/4 v2, 0x5

    .line 724
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v7, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mNetworkWasUp:Z
    invoke-static {v6, v7}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1602(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z

    .line 743
    .end local v3    # "update_token":Ljava/lang/String;
    :cond_5
    :goto_2
    const-string v6, "SystemUpdateService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "now status is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    packed-switch v2, :pswitch_data_0

    .line 769
    :pswitch_0
    const-string v6, "SystemUpdateService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "checkDownload unexpected state "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/common/OperationScheduler;->onTransientError()V

    .line 771
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v7}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1000(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-direct {p0, v6, v7, v8}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    .line 772
    const/4 v6, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;

    .line 773
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 726
    .restart local v3    # "update_token":Ljava/lang/String;
    :cond_6
    const-string v6, "SystemUpdateService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "starting download of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mNetworkWasUp:Z
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1600(Lcom/google/android/gsf/update/SystemUpdateService;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 728
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/common/OperationScheduler;->resetTransientError()V

    .line 730
    :cond_7
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/update/DownloadAttempt;->start()V

    .line 731
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "status"

    const/4 v8, 0x2

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "verified"

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 735
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v7, 0x1

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mNetworkWasUp:Z
    invoke-static {v6, v7}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1602(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z

    .line 736
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/update/DownloadAttempt;->getCurrentStatus()I

    move-result v2

    goto/16 :goto_2

    .line 740
    .end local v3    # "update_token":Ljava/lang/String;
    :cond_8
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/update/DownloadAttempt;->getCurrentStatus()I

    move-result v2

    goto/16 :goto_2

    .line 747
    :pswitch_1
    const/4 v6, 0x1

    goto/16 :goto_1

    .line 750
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->downloadFailedSpace()V

    .line 751
    const/4 v6, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;

    .line 752
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 755
    :pswitch_3
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/common/OperationScheduler;->resetTransientError()V

    .line 759
    :pswitch_4
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/common/OperationScheduler;->onTransientError()V

    .line 760
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v7}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1000(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-direct {p0, v6, v7, v8}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    .line 761
    const/4 v6, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;

    .line 762
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 765
    :pswitch_5
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1400()Lcom/google/android/gsf/update/DownloadAttempt;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gsf/update/DownloadAttempt;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, p2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->processDownloadedFile(Ljava/lang/String;I)Z

    move-result v6

    goto/16 :goto_1

    .line 745
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private checkFile()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 879
    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v5}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "filename"

    invoke-interface {v5, v6, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 880
    .local v1, "filename":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 881
    const-string v5, "SystemUpdateService"

    const-string v6, "OTA package filename empty"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 891
    .end local v1    # "filename":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 884
    .restart local v1    # "filename":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 885
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 886
    const-string v5, "SystemUpdateService"

    const-string v6, "OTA package doesn\'t exist!"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 887
    goto :goto_0

    .line 889
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 890
    .local v2, "size":J
    const-string v4, "SystemUpdateService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "OTA package size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private checkPostInstallIntents()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 590
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "pending_filename"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 591
    .local v4, "pending":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    const/4 v2, 0x0

    .line 594
    .local v2, "installFilename":Ljava/lang/String;
    const/4 v5, 0x0

    .line 596
    .local v5, "success":Z
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    const-string v7, "/cache/recovery/last_install"

    invoke-direct {v6, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 597
    .local v1, "in":Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 598
    const-string v6, "1"

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 599
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 604
    if-nez v2, :cond_2

    .line 605
    const-string v6, "SystemUpdateService"

    const-string v7, "can\'t determine last-installed OTA package name"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 600
    .end local v1    # "in":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 601
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "SystemUpdateService"

    const-string v7, "failed to read last_install"

    invoke-static {v6, v7, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 608
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    :cond_2
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 610
    const-string v6, "SystemUpdateService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "first boot since "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " install of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    const/4 v3, 0x0

    .line 613
    .local v3, "message":Ljava/lang/String;
    if-eqz v5, :cond_5

    .line 614
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "success_message"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 619
    :goto_1
    if-eqz v3, :cond_4

    if-eqz v5, :cond_3

    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # invokes: Lcom/google/android/gsf/update/SystemUpdateService;->isRunningOnTv()Z
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 620
    :cond_3
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-class v9, Lcom/google/android/gsf/update/CompleteDialog;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v8, 0x10040000

    invoke-virtual {v7, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "message"

    invoke-virtual {v7, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/gsf/update/SystemUpdateService;->startActivity(Landroid/content/Intent;)V

    .line 626
    :cond_4
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "pending_filename"

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 616
    :cond_5
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "failure_message"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private checkUpdateState(Landroid/content/Intent;)Z
    .locals 28
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 434
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v19

    if-lez v19, :cond_0

    const/16 v19, 0x0

    .line 585
    :goto_0
    return v19

    .line 436
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 437
    .local v10, "now":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "provisioned"

    const-wide/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-wide/from16 v2, v24

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    .line 438
    .local v14, "provisionedTime":J
    const-wide/16 v22, 0x0

    cmp-long v19, v14, v22

    if-eqz v19, :cond_1

    cmp-long v19, v14, v10

    if-lez v19, :cond_2

    .line 442
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v22, "device_provisioned"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v12

    .line 444
    .local v12, "provisioned":I
    if-nez v12, :cond_4

    const-wide/16 v14, 0x0

    .line 445
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    const-string v22, "provisioned"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-interface {v0, v1, v14, v15}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 447
    .end local v12    # "provisioned":I
    :cond_2
    const-wide/16 v22, 0x0

    cmp-long v19, v14, v22

    if-lez v19, :cond_5

    const/16 v19, 0x1

    :goto_2
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mProvisioned:Z

    .line 449
    if-eqz p1, :cond_3

    const-string v19, "boot"

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 450
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkPostInstallIntents()V

    .line 453
    :cond_3
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadStatus:I

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v22, "update_url"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 455
    .local v17, "update_url":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 456
    const-string v19, "SystemUpdateService"

    const-string v22, "cancelUpdate (empty URL)"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->cancelUpdate()V

    .line 458
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    .line 459
    const/16 v19, 0x0

    goto/16 :goto_0

    .end local v17    # "update_url":Ljava/lang/String;
    .restart local v12    # "provisioned":I
    :cond_4
    move-wide v14, v10

    .line 444
    goto :goto_1

    .line 447
    .end local v12    # "provisioned":I
    :cond_5
    const/16 v19, 0x0

    goto :goto_2

    .line 462
    .restart local v17    # "update_url":Ljava/lang/String;
    :cond_6
    if-eqz p1, :cond_7

    const-string v19, "download_now"

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 463
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->clearDownloadBackoffs()V

    .line 466
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v22, "update_required_setup"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 467
    .local v13, "required_setup":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_8

    .line 468
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mRequiredSetup:Z

    .line 469
    const-string v19, "SystemUpdateService"

    const-string v22, "update required during setup wizard"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "status"

    const/16 v23, -0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v19

    const/16 v22, 0x5

    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "url"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "install_time"

    const-wide/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-wide/from16 v2, v24

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v24, "update_retry_delay_sec"

    const v25, 0x3f480

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x3e8

    mul-long v24, v24, v26

    add-long v22, v22, v24

    cmp-long v19, v10, v22

    if-gtz v19, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "install_time"

    const-wide/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-wide/from16 v2, v24

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v22

    const-wide/32 v24, 0x36ee80

    sub-long v22, v22, v24

    cmp-long v19, v10, v22

    if-gez v19, :cond_a

    .line 485
    :cond_9
    const-string v19, "SystemUpdateService"

    const-string v22, "cancelUpdate (willing to retry now)"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->cancelUpdate()V

    .line 489
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v22, "update_urgency"

    const/16 v23, 0x2

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v18

    .line 491
    .local v18, "urgency":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "url"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_b

    .line 493
    const-string v19, "SystemUpdateService"

    const-string v22, "cancelUpdate (update URL has changed)"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->cancelUpdate()V

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 496
    .local v5, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v19, "url"

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    const-string v22, "status"

    const/16 v23, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    const-string v22, "url_change"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-wide/from16 v2, v24

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    const-string v22, "required_setup"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mRequiredSetup:Z

    move/from16 v23, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 500
    packed-switch v18, :pswitch_data_0

    .line 513
    :pswitch_0
    const-string v19, "download_approved"

    move-object/from16 v0, v19

    invoke-interface {v5, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    const-string v22, "install_approved"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 515
    const/16 v18, 0x2

    .line 518
    :goto_3
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 519
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->clearDownloadBackoffs()V

    .line 522
    .end local v5    # "edit":Landroid/content/SharedPreferences$Editor;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v22, "update_provisioning_delay_sec"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 524
    .local v4, "delay":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mRequiredSetup:Z

    move/from16 v19, v0

    if-nez v19, :cond_e

    if-ltz v4, :cond_e

    const-wide/16 v22, 0x0

    cmp-long v19, v14, v22

    if-eqz v19, :cond_c

    mul-int/lit16 v0, v4, 0x3e8

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v22, v22, v14

    cmp-long v19, v10, v22

    if-gez v19, :cond_e

    .line 527
    :cond_c
    const-wide/16 v22, 0x0

    cmp-long v19, v14, v22

    if-nez v19, :cond_d

    .line 531
    const/16 v19, 0x384

    move/from16 v0, v19

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v19

    move/from16 v0, v19

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v20, v10, v22

    .line 540
    .local v20, "when":J
    :goto_4
    const-string v19, "SystemUpdateService"

    const-string v22, "cancelUpdate (not provisioned)"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->cancelUpdate()V

    .line 542
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    .line 543
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    .line 544
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 503
    .end local v4    # "delay":I
    .end local v20    # "when":J
    .restart local v5    # "edit":Landroid/content/SharedPreferences$Editor;
    :pswitch_1
    const-string v19, "download_approved"

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    const-string v22, "install_approved"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_3

    .line 508
    :pswitch_2
    const-string v19, "download_approved"

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    const-string v22, "install_approved"

    const/16 v23, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_3

    .line 538
    .end local v5    # "edit":Landroid/content/SharedPreferences$Editor;
    .restart local v4    # "delay":I
    :cond_d
    mul-int/lit16 v0, v4, 0x3e8

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v20, v14, v22

    .restart local v20    # "when":J
    goto :goto_4

    .line 547
    .end local v20    # "when":J
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "status"

    const/16 v23, -0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v19

    const/16 v22, 0x6

    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isAutomaticallyDownloaded(I)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 549
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static/range {v22 .. v22}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1000(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v6

    .line 550
    .local v6, "nextTime":J
    cmp-long v19, v10, v6

    if-gez v19, :cond_f

    .line 551
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    .line 552
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 556
    .end local v6    # "nextTime":J
    :cond_f
    const/16 v16, 0x0

    .line 558
    .local v16, "sticky":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "status"

    const/16 v23, -0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v19

    const/16 v22, 0x5

    move/from16 v0, v19

    move/from16 v1, v22

    if-eq v0, v1, :cond_10

    .line 559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v22, "download_approved"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    if-eqz v19, :cond_12

    .line 560
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkDownload(Ljava/lang/String;I)Z

    move-result v16

    .line 583
    :cond_10
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    .line 584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v22

    monitor-enter v22

    .line 585
    if-nez v16, :cond_11

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v19

    if-eqz v19, :cond_15

    :cond_11
    const/16 v19, 0x1

    :goto_6
    monitor-exit v22

    goto/16 :goto_0

    .line 586
    :catchall_0
    move-exception v19

    monitor-exit v22
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v19

    .line 562
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v22, "update_download_notify_time"

    const-wide/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-wide/from16 v2, v24

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v22

    const-wide/16 v24, 0x3e8

    mul-long v8, v22, v24

    .line 564
    .local v8, "notifyTime":J
    sub-long v22, v8, v10

    const-wide v24, 0x9a7ec800L

    cmp-long v19, v22, v24

    if-lez v19, :cond_13

    .line 565
    const-string v19, "SystemUpdateService"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "download_notify_time too far in future; ignoring ("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    const-wide/16 v8, 0x0

    .line 569
    :cond_13
    cmp-long v19, v10, v8

    if-ltz v19, :cond_14

    .line 570
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    .line 571
    const v19, 0x7f0a0035

    const v22, 0x7f0a0037

    const v23, 0x7f020081

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->notifyWithActivity(III)V

    .line 575
    const-wide/32 v22, 0x36ee80

    add-long v22, v22, v10

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto/16 :goto_5

    .line 577
    :cond_14
    const-string v19, "SystemUpdateService"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "deferring notification for "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sub-long v24, v8, v10

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ms"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v8, v9, v1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto/16 :goto_5

    .line 585
    .end local v8    # "notifyTime":J
    :cond_15
    const/16 v19, 0x0

    goto/16 :goto_6

    .line 500
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private clearDownloadBackoffs()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 423
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/common/OperationScheduler;->resetTransientError()V

    .line 424
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 425
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 426
    return-void
.end method

.method private downloadFailed(II)V
    .locals 8
    .param p1, "urgency"    # I
    .param p2, "minRetryDelaySec"    # I

    .prologue
    const/4 v6, 0x6

    .line 630
    invoke-direct {p0, v6}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    .line 631
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/common/OperationScheduler;->onTransientError()V

    .line 632
    const-string v1, "SystemUpdateService"

    const-string v4, "download failed; clearing attempt"

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    const/4 v1, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;

    .line 634
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 635
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "status"

    const/4 v5, -0x1

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 636
    const-string v1, "status"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 638
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isAutomaticallyDownloaded(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 639
    const-string v1, "download_approved"

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 640
    const v1, 0x7f0a0039

    const v4, 0x7f0a003a

    const v5, 0x7f020082

    invoke-direct {p0, v1, v4, v5}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->notifyWithActivity(III)V

    .line 651
    :goto_0
    return-void

    .line 645
    :cond_1
    if-lez p2, :cond_2

    .line 646
    mul-int/lit16 v1, p2, 0x3e8

    int-to-long v4, v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long v2, v4, v6

    .line 647
    .local v2, "when":J
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 649
    .end local v2    # "when":J
    :cond_2
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1000(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v4

    const/4 v1, 0x1

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto :goto_0
.end method

.method private downloadFailedSpace()V
    .locals 4

    .prologue
    const/16 v3, 0x9

    .line 654
    const-string v0, "SystemUpdateService"

    const-string v1, "download failed due to space; clearing attempt"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    invoke-direct {p0, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    .line 656
    const/4 v0, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;

    .line 657
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "status"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 658
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "status"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 660
    :cond_0
    const v0, 0x7f0a0039

    const v1, 0x7f0a003b

    const v2, 0x7f020082

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->notifyWithActivity(III)V

    .line 664
    return-void
.end method

.method private getMaintenanceWindow()Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;
    .locals 13

    .prologue
    const/16 v12, 0x3b

    const/16 v11, 0x17

    const/4 v8, 0x0

    .line 1049
    iget-object v9, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v9}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "maintenance_window"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1050
    .local v7, "window":Ljava/lang/String;
    if-eqz v7, :cond_0

    const-string v9, ""

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1053
    :cond_0
    new-instance v6, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;

    invoke-direct {v6, p0, v8}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;-><init>(Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;Lcom/google/android/gsf/update/SystemUpdateService$1;)V

    .line 1084
    :goto_0
    return-object v6

    .line 1056
    :cond_1
    const-string v9, "none"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1057
    const-string v9, "SystemUpdateService"

    const-string v10, "maintenance window set to \"none\""

    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v8

    .line 1058
    goto :goto_0

    .line 1061
    :cond_2
    sget-object v9, Lcom/google/android/gsf/update/SystemUpdateService;->MAINTENANCE_WINDOW_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 1062
    .local v3, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-nez v9, :cond_3

    .line 1063
    const-string v9, "SystemUpdateService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "malformed maintenance window \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v8

    .line 1064
    goto :goto_0

    .line 1068
    :cond_3
    const/4 v9, 0x1

    :try_start_0
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1069
    .local v4, "sHour":I
    const/4 v9, 0x2

    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1070
    .local v5, "sMin":I
    const/4 v9, 0x3

    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1071
    .local v1, "eHour":I
    const/4 v9, 0x4

    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1077
    .local v2, "eMin":I
    if-gt v4, v11, :cond_4

    if-gt v5, v12, :cond_4

    if-gt v1, v11, :cond_4

    if-le v2, v12, :cond_5

    .line 1078
    :cond_4
    const-string v9, "SystemUpdateService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "malformed maintenance window \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v8

    .line 1079
    goto/16 :goto_0

    .line 1072
    .end local v1    # "eHour":I
    .end local v2    # "eMin":I
    .end local v4    # "sHour":I
    .end local v5    # "sMin":I
    :catch_0
    move-exception v0

    .line 1074
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 1081
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v1    # "eHour":I
    .restart local v2    # "eMin":I
    .restart local v4    # "sHour":I
    .restart local v5    # "sMin":I
    :cond_5
    new-instance v6, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;

    invoke-direct {v6, p0, v8}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;-><init>(Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;Lcom/google/android/gsf/update/SystemUpdateService$1;)V

    .line 1082
    .local v6, "w":Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;
    mul-int/lit8 v8, v4, 0x3c

    add-int/2addr v8, v5

    iput v8, v6, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    .line 1083
    mul-int/lit8 v8, v1, 0x3c

    add-int/2addr v8, v2

    iput v8, v6, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    goto/16 :goto_0
.end method

.method private install()V
    .locals 8

    .prologue
    .line 895
    const-string v4, "SystemUpdateService"

    const-string v5, "called install()"

    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkFile()Ljava/lang/String;

    move-result-object v2

    .line 898
    .local v2, "filename":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 935
    :goto_0
    return-void

    .line 900
    :cond_0
    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 901
    .local v1, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v4, "status"

    const/4 v5, 0x5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "install_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "pending_filename"

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 911
    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "update_install_success_message"

    invoke-static {v4, v5}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 912
    .local v3, "message":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 913
    const-string v4, "success_message"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 915
    :cond_1
    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "update_install_failure_message"

    invoke-static {v4, v5}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 916
    if-eqz v3, :cond_2

    .line 917
    const-string v4, "failure_message"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 920
    :cond_2
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 921
    invoke-virtual {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->logState()V

    .line 923
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    .line 925
    .local v0, "context":Landroid/content/Context;
    const-string v4, "SystemUpdateService"

    const-string v5, "calling installPackage()"

    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    new-instance v4, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$1;

    invoke-direct {v4, p0, v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$1;-><init>(Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$1;->start()V

    goto :goto_0
.end method

.method private isAutomaticallyDownloaded(I)Z
    .locals 1
    .param p1, "urgency"    # I

    .prologue
    .line 868
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInMaintenanceWindow(J)Z
    .locals 7
    .param p1, "when"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1094
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->getMaintenanceWindow()Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;

    move-result-object v2

    .line 1095
    .local v2, "window":Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;
    if-nez v2, :cond_1

    .line 1108
    :cond_0
    :goto_0
    return v3

    .line 1096
    :cond_1
    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    iget v6, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-ne v5, v6, :cond_2

    move v3, v4

    .line 1101
    goto :goto_0

    .line 1104
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1105
    .local v1, "query":Ljava/util/Calendar;
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1106
    const/16 v5, 0xb

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    mul-int/lit8 v5, v5, 0x3c

    const/16 v6, 0xc

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int v0, v5, v6

    .line 1108
    .local v0, "now":I
    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    if-gt v5, v0, :cond_3

    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-le v0, v5, :cond_4

    :cond_3
    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    iget v6, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-le v5, v6, :cond_0

    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    if-le v5, v0, :cond_4

    iget v5, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-gt v0, v5, :cond_0

    :cond_4
    move v3, v4

    goto :goto_0
.end method

.method private isOkToInstallNow(IJ)Z
    .locals 4
    .param p1, "urgency"    # I
    .param p2, "now"    # J

    .prologue
    const/4 v0, 0x0

    .line 858
    const/4 v1, 0x6

    if-ne p1, v1, :cond_1

    .line 859
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1700()J

    move-result-wide v2

    cmp-long v1, p2, v2

    if-lez v1, :cond_0

    invoke-direct {p0, p2, p3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isInMaintenanceWindow(J)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 861
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "install_approved"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private msUntilMaintenanceWindow(J)J
    .locals 9
    .param p1, "when"    # J

    .prologue
    .line 1123
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->getMaintenanceWindow()Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;

    move-result-object v2

    .line 1124
    .local v2, "window":Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;
    if-nez v2, :cond_0

    const-wide v4, 0x7fffffffffffffffL

    .line 1134
    :goto_0
    return-wide v4

    .line 1125
    :cond_0
    iget v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    iget v4, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->end:I

    if-ne v3, v4, :cond_1

    const-wide/16 v4, 0x0

    goto :goto_0

    .line 1127
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1128
    .local v1, "query":Ljava/util/Calendar;
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1129
    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x3c

    const/16 v4, 0xc

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int v0, v3, v4

    .line 1131
    .local v0, "now":I
    iget v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    if-le v0, v3, :cond_2

    .line 1132
    iget v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    add-int/lit16 v3, v3, 0x5a0

    iput v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    .line 1134
    :cond_2
    iget v3, v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask$Window;->start:I

    sub-int/2addr v3, v0

    mul-int/lit8 v3, v3, 0x3c

    int-to-long v4, v3

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    goto :goto_0
.end method

.method private notifyWithActivity(III)V
    .locals 12
    .param p1, "titleResId"    # I
    .param p2, "messageResId"    # I
    .param p3, "iconResId"    # I

    .prologue
    .line 978
    sget-boolean v0, Lcom/google/android/gsf/update/SystemUpdateActivity;->sIsActivityUp:Z

    if-eqz v0, :cond_0

    .line 980
    const-string v0, "SystemUpdateService"

    const-string v1, "skipping notification"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1011
    :goto_0
    return-void

    .line 984
    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 985
    .local v10, "resources":Landroid/content/res/Resources;
    invoke-virtual {v10, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 986
    .local v11, "title":Ljava/lang/CharSequence;
    invoke-virtual {v10, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 988
    .local v6, "message":Ljava/lang/CharSequence;
    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-class v1, Lcom/google/android/gsf/update/SystemUpdateActivity;

    invoke-direct {v2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 989
    .local v2, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    move-result-object v9

    .line 993
    .local v9, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v11}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    .line 1008
    .local v7, "n":Landroid/app/Notification;
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    .line 1010
    .local v8, "nm":Landroid/app/NotificationManager;
    const/4 v0, 0x0

    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v8, v0, p3, v7, v1}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method private processDownloadedFile(Ljava/lang/String;I)Z
    .locals 11
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "urgency"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 778
    const-string v6, "SystemUpdateService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processDownloadedFile "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 781
    const-string v5, "SystemUpdateService"

    const-string v6, "download completed but no filename available"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    invoke-direct {p0, p2, v4}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->downloadFailed(II)V

    .line 853
    :cond_0
    :goto_0
    return v4

    .line 786
    :cond_1
    if-eqz p1, :cond_3

    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "filename"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 787
    const-string v6, "SystemUpdateService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "download filename now "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "filename"

    invoke-interface {v6, v7, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "verified"

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 792
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 793
    :try_start_0
    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v7}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 794
    const-string v7, "SystemUpdateService"

    const-string v8, "checkDownload: cancelling verifier"

    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v7}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;->cancel(Z)Z

    .line 796
    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v8, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v7, v8}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1202(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    .line 798
    :cond_2
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 801
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 803
    .local v0, "now":J
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "verified"

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 804
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 805
    :try_start_1
    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v8, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v7, v8}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1202(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    .line 806
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 807
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "verified"

    invoke-interface {v6, v7, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 808
    const-string v6, "SystemUpdateService"

    const-string v7, "file has been verified"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkFile()Ljava/lang/String;

    .line 810
    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->isOkToInstallNow(IJ)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 811
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->install()V

    move v4, v5

    .line 812
    goto/16 :goto_0

    .line 798
    .end local v0    # "now":J
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 806
    .restart local v0    # "now":J
    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4

    .line 814
    :cond_4
    const/4 v6, 0x4

    invoke-direct {p0, v6}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    .line 815
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/common/OperationScheduler;->onSuccess()V

    .line 817
    const/4 v6, 0x6

    if-ne p2, v6, :cond_5

    .line 818
    invoke-direct {p0, v0, v1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->msUntilMaintenanceWindow(J)J

    move-result-wide v6

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1700()J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 820
    .local v2, "wait":J
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v6, v2, v6

    if-eqz v6, :cond_0

    .line 821
    add-long v6, v0, v2

    invoke-direct {p0, v6, v7, v5}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->retryAt(JZ)V

    goto/16 :goto_0

    .line 823
    .end local v2    # "wait":J
    :cond_5
    sget-boolean v5, Lcom/google/android/gsf/update/SystemUpdateActivity;->sIsActivityUp:Z

    if-nez v5, :cond_0

    .line 824
    const v5, 0x7f0a0036

    const v6, 0x7f0a0038

    const v7, 0x7f020081

    invoke-direct {p0, v5, v6, v7}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->notifyWithActivity(III)V

    goto/16 :goto_0

    .line 831
    :cond_6
    const-string v5, "SystemUpdateService"

    const-string v6, "verification of system update package failed"

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v5}, Lcom/google/android/gsf/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "update_verify_redownload_delay_sec"

    const v7, 0xa8c0

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-direct {p0, p2, v5}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->downloadFailed(II)V

    .line 835
    const/4 v5, 0x7

    invoke-direct {p0, v5}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    goto/16 :goto_0

    .line 840
    :cond_7
    iget-object v6, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1100(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 841
    :try_start_4
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkFile()Ljava/lang/String;

    .line 842
    iget-object v7, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v7}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 844
    const-string v5, "SystemUpdateService"

    const-string v7, "verification already in progress"

    invoke-static {v5, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    monitor-exit v6

    goto/16 :goto_0

    .line 854
    :catchall_2
    move-exception v4

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v4

    .line 848
    :cond_8
    :try_start_5
    const-string v4, "SystemUpdateService"

    const-string v7, "starting package verification"

    invoke-static {v4, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    const/4 v4, 0x3

    invoke-direct {p0, v4}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->setStatus(I)V

    .line 850
    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    new-instance v7, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    iget-object v8, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v10}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v10

    invoke-direct {v7, v8, v9, v10}, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;-><init>(Landroid/content/Context;Ljava/io/File;Landroid/content/SharedPreferences;)V

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v4, v7}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1202(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    .line 852
    iget-object v4, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    invoke-static {v4}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    move-result-object v4

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->EXECUTOR:Ljava/util/concurrent/Executor;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$600()Ljava/util/concurrent/Executor;

    move-result-object v7

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Void;

    invoke-virtual {v4, v7, v8}, Lcom/google/android/gsf/update/SystemUpdateVerifierTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 853
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move v4, v5

    goto/16 :goto_0
.end method

.method private retryAt(JZ)V
    .locals 7
    .param p1, "when"    # J
    .param p3, "wakeup"    # Z

    .prologue
    .line 667
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 668
    .local v0, "am":Landroid/app/AlarmManager;
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1500(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 669
    if-eqz p3, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;
    invoke-static {v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$1500(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 670
    const-string v1, "SystemUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "retry (wakeup: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, p1, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    return-void

    .line 669
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private setStatus(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 873
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "status"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 874
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "status"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 876
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "intents"    # [Landroid/content/Intent;

    .prologue
    .line 386
    array-length v0, p1

    if-lez v0, :cond_0

    .line 387
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkUpdateState(Landroid/content/Intent;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 389
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->checkUpdateState(Landroid/content/Intent;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 379
    check-cast p1, [Landroid/content/Intent;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->doInBackground([Landroid/content/Intent;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method logState()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1015
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "status"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1017
    .local v0, "status":I
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "download_approved"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1018
    or-int/lit8 v0, v0, 0x20

    .line 1021
    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "install_approved"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1022
    or-int/lit8 v0, v0, 0x40

    .line 1026
    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "verified"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1027
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "verified"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_0
    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    .line 1030
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mProvisioned:Z

    if-eqz v1, :cond_3

    .line 1031
    or-int/lit16 v0, v0, 0x200

    .line 1035
    :cond_3
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "download_mobile"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1036
    or-int/lit16 v0, v0, 0x400

    .line 1038
    :cond_4
    const v1, 0x31129

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->mLastDownloadStatus:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v3

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/google/android/gsf/update/SystemUpdateService;->access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v5, "url"

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 1041
    return-void

    :cond_5
    move v1, v3

    .line 1027
    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 6
    .param p1, "keepRunning"    # Ljava/lang/Boolean;

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mPendingLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$300(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 395
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mRunning:Z
    invoke-static {v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$402(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z

    .line 397
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z
    invoke-static {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->access$500(Lcom/google/android/gsf/update/SystemUpdateService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z
    invoke-static {v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$502(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z

    .line 401
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/gsf/update/SystemUpdateService;->mRunning:Z
    invoke-static {v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->access$402(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z

    .line 402
    new-instance v0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, v2}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;-><init>(Lcom/google/android/gsf/update/SystemUpdateService;)V

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->EXECUTOR:Ljava/util/concurrent/Executor;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$600()Ljava/util/concurrent/Executor;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/Intent;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->mIntentPending:Landroid/content/Intent;
    invoke-static {v5}, Lcom/google/android/gsf/update/SystemUpdateService;->access$700(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/Intent;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 415
    :cond_0
    :goto_0
    monitor-exit v1

    .line 416
    return-void

    .line 404
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 407
    :cond_2
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->this$0:Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/SystemUpdateService;->stopSelf()V

    .line 411
    :cond_3
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$100()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$100()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    # getter for: Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/google/android/gsf/update/SystemUpdateService;->access$100()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 379
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
