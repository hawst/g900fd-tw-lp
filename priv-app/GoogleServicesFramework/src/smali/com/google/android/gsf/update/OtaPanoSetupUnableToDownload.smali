.class public Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;
.super Landroid/app/Fragment;
.source "OtaPanoSetupUnableToDownload.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 39
    return-void
.end method

.method public static createInstance(Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;)Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;
    .locals 1
    .param p0, "listener"    # Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;

    invoke-direct {v0}, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;-><init>()V

    .line 50
    .local v0, "fragment":Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;
    invoke-virtual {v0, p0}, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;->setListener(Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;)V

    .line 51
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 57
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "helium"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 62
    const v8, 0x7f03001e

    invoke-virtual {p1, v8, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 63
    .local v3, "content":Landroid/view/View;
    const v8, 0x7f0f001e

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 64
    .local v0, "actionArea":Landroid/widget/FrameLayout;
    const v8, 0x7f0f001d

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    .line 67
    .local v4, "descArea":Landroid/widget/FrameLayout;
    const v8, 0x7f030026

    invoke-virtual {p1, v8, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 68
    .local v5, "description":Landroid/view/View;
    const v8, 0x7f0f001a

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v9, 0x7f0a00f3

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 69
    const v8, 0x7f0f001b

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v9, 0x7f0a00f4

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 71
    const v8, 0x7f0f006b

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v9, 0x7f0a00f5

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 72
    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 74
    const v8, 0x7f030024

    invoke-virtual {p1, v8, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/pano/widget/ScrollAdapterView;

    .line 77
    .local v1, "actionList":Lcom/google/android/pano/widget/ScrollAdapterView;
    new-instance v6, Ljava/util/ArrayList;

    const/4 v8, 0x2

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 78
    .local v6, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 79
    .local v7, "res":Landroid/content/res/Resources;
    const v8, 0x7f0a00f6

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    const v8, 0x7f0a00f7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    new-instance v2, Lcom/google/android/pano/widget/ScrollArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f030022

    const v10, 0x7f0f0067

    invoke-direct {v2, v8, v9, v10, v6}, Lcom/google/android/pano/widget/ScrollArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 85
    .local v2, "adapter":Lcom/google/android/pano/widget/ScrollArrayAdapter;, "Lcom/google/android/pano/widget/ScrollArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v1, v2}, Lcom/google/android/pano/widget/ScrollAdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 86
    invoke-virtual {v1, p0}, Lcom/google/android/pano/widget/ScrollAdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 87
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 88
    return-object v3
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "adapter":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;->mListener:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;

    if-eqz v1, :cond_0

    .line 95
    packed-switch p3, :pswitch_data_0

    .line 98
    const/4 v0, 0x0

    .line 100
    .local v0, "action":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;->mListener:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;

    invoke-interface {v1, v0}, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;->onCorrectionAction(I)V

    .line 102
    .end local v0    # "action":I
    :cond_0
    return-void

    .line 96
    :pswitch_0
    const/16 v0, 0x9

    .restart local v0    # "action":I
    goto :goto_0

    .line 97
    .end local v0    # "action":I
    :pswitch_1
    const/16 v0, 0xa

    .restart local v0    # "action":I
    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setListener(Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;->mListener:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;

    .line 106
    return-void
.end method
