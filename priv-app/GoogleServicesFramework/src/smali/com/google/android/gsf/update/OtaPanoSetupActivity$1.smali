.class Lcom/google/android/gsf/update/OtaPanoSetupActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "OtaPanoSetupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/update/OtaPanoSetupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$1;->this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$1;->this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    # getter for: Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J
    invoke-static {v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->access$000(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$1;->this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    # getter for: Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownStopTime:J
    invoke-static {v2}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->access$100(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 89
    :cond_0
    const-string v0, "OtaPanoSetupActivity"

    const-string v1, "screen turned off during countdown; installing immediately"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$1;->this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    # invokes: Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startUpdate()V
    invoke-static {v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->access$200(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V

    .line 92
    :cond_1
    return-void
.end method
