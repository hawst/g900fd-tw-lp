.class public Lcom/google/android/gsf/checkin/CheckinService;
.super Landroid/app/Service;
.source "CheckinService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/checkin/CheckinService$SecretCodeReceiver;,
        Lcom/google/android/gsf/checkin/CheckinService$TriggerReceiver;,
        Lcom/google/android/gsf/checkin/CheckinService$Receiver;
    }
.end annotation


# static fields
.field private static sHandoffWakeLock:Landroid/os/PowerManager$WakeLock;

.field private static volatile sSystemWasUpgraded:Z


# instance fields
.field private mServiceWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mTask:Lcom/google/android/gsf/checkin/CheckinTask;

.field private mTaskStartedUptime:J

.field private mTaskTriggerCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gsf/checkin/CheckinService;->sHandoffWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 93
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/checkin/CheckinService;->sSystemWasUpgraded:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 84
    iput-object v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mServiceWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 89
    iput-object v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTask:Lcom/google/android/gsf/checkin/CheckinTask;

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTaskStartedUptime:J

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTaskTriggerCount:I

    .line 135
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinService;->launchService(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gsf/checkin/CheckinService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/checkin/CheckinService;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/gsf/checkin/CheckinService;->checkSchedule()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/gsf/checkin/CheckinService;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/checkin/CheckinService;
    .param p1, "x1"    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;
    .param p2, "x2"    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/checkin/CheckinService;->handleResponse(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;)V

    return-void
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 52
    sput-boolean p0, Lcom/google/android/gsf/checkin/CheckinService;->sSystemWasUpgraded:Z

    return p0
.end method

.method static synthetic access$402(Lcom/google/android/gsf/checkin/CheckinService;Lcom/google/android/gsf/checkin/CheckinTask;)Lcom/google/android/gsf/checkin/CheckinTask;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/checkin/CheckinService;
    .param p1, "x1"    # Lcom/google/android/gsf/checkin/CheckinTask;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTask:Lcom/google/android/gsf/checkin/CheckinTask;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/gsf/checkin/CheckinService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/checkin/CheckinService;

    .prologue
    .line 52
    iget v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTaskTriggerCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/gsf/checkin/CheckinService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/checkin/CheckinService;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/gsf/checkin/CheckinService;->launchTask()V

    return-void
.end method

.method private checkSchedule()Z
    .locals 26

    .prologue
    .line 318
    const-string v5, "CheckinService"

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/google/android/gsf/checkin/CheckinService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v21

    .line 319
    .local v21, "sp":Landroid/content/SharedPreferences;
    new-instance v19, Lcom/android/common/OperationScheduler;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    .line 320
    .local v19, "scheduler":Lcom/android/common/OperationScheduler;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/checkin/CheckinService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    .line 321
    .local v18, "resolver":Landroid/content/ContentResolver;
    const-string v5, "CheckinService_ignore_net"

    const/4 v8, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 322
    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/android/common/OperationScheduler;->setEnabledState(Z)V

    .line 333
    :cond_0
    :goto_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/gsf/checkin/CheckinService;->wasSystemUpgraded(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 334
    const-string v5, "CheckinService"

    const/4 v8, 0x2

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "CheckinService"

    const-string v8, "system was upgraded"

    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_1
    const-wide/16 v8, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8, v9}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 339
    :cond_2
    const-string v5, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/gsf/checkin/CheckinService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/telephony/TelephonyManager;

    .line 340
    .local v22, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v22 .. v22}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v20

    .line 341
    .local v20, "sim":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v13

    .line 342
    .local v13, "imsi":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v20, :cond_3

    const-string v20, "no-sim"

    .end local v20    # "sim":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\n"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v13, :cond_4

    const-string v13, "no-imsi"

    .end local v13    # "imsi":Ljava/lang/String;
    :cond_4
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 343
    .local v12, "id":Ljava/lang/String;
    const-string v5, "CheckinService_lastSim"

    const/4 v8, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v5, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 344
    const-string v5, "CheckinService"

    const/4 v8, 0x2

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "CheckinService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "subscriberid changed to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_5
    const-wide/16 v8, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8, v9}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 346
    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v8, "CheckinService_lastSim"

    invoke-interface {v5, v8, v12}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 349
    :cond_6
    new-instance v15, Lcom/android/common/OperationScheduler$Options;

    invoke-direct {v15}, Lcom/android/common/OperationScheduler$Options;-><init>()V

    .line 350
    .local v15, "options":Lcom/android/common/OperationScheduler$Options;
    const-wide/16 v8, 0x7530

    iput-wide v8, v15, Lcom/android/common/OperationScheduler$Options;->minTriggerMillis:J

    .line 351
    const-wide/16 v8, 0x3e8

    const-string v5, "checkin_interval"

    const-wide/32 v24, 0xa8c0

    move-object/from16 v0, v18

    move-wide/from16 v1, v24

    invoke-static {v0, v5, v1, v2}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v24

    mul-long v8, v8, v24

    iput-wide v8, v15, Lcom/android/common/OperationScheduler$Options;->periodicIntervalMillis:J

    .line 353
    const/16 v5, 0x1388

    iput v5, v15, Lcom/android/common/OperationScheduler$Options;->backoffExponentialMillis:I

    .line 357
    const-string v5, "CheckinService"

    const/4 v8, 0x2

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 358
    const-string v5, "CheckinService"

    invoke-virtual/range {v19 .. v19}, Lcom/android/common/OperationScheduler;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 362
    .local v16, "now":J
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v6

    .line 363
    .local v6, "next":J
    cmp-long v5, v6, v16

    if-gtz v5, :cond_a

    const/4 v5, 0x1

    .line 381
    :goto_1
    return v5

    .line 324
    .end local v6    # "next":J
    .end local v12    # "id":Ljava/lang/String;
    .end local v15    # "options":Lcom/android/common/OperationScheduler$Options;
    .end local v16    # "now":J
    .end local v22    # "tm":Landroid/telephony/TelephonyManager;
    :cond_8
    const-string v5, "connectivity"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/gsf/checkin/CheckinService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/ConnectivityManager;

    .line 325
    .local v11, "cm":Landroid/net/ConnectivityManager;
    if-eqz v11, :cond_0

    .line 326
    invoke-virtual {v11}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v14

    .line 327
    .local v14, "ni":Landroid/net/NetworkInfo;
    if-eqz v14, :cond_9

    invoke-virtual {v14}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {v11}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v5, 0x1

    :goto_2
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/android/common/OperationScheduler;->setEnabledState(Z)V

    goto/16 :goto_0

    :cond_9
    const/4 v5, 0x0

    goto :goto_2

    .line 365
    .end local v11    # "cm":Landroid/net/ConnectivityManager;
    .end local v14    # "ni":Landroid/net/NetworkInfo;
    .restart local v6    # "next":J
    .restart local v12    # "id":Ljava/lang/String;
    .restart local v15    # "options":Lcom/android/common/OperationScheduler$Options;
    .restart local v16    # "now":J
    .restart local v22    # "tm":Landroid/telephony/TelephonyManager;
    :cond_a
    const/4 v5, 0x0

    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/google/android/gsf/checkin/CheckinService$Receiver;

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v8, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 366
    .local v10, "pi":Landroid/app/PendingIntent;
    const-wide v8, 0x7fffffffffffffffL

    cmp-long v5, v6, v8

    if-nez v5, :cond_c

    .line 367
    const-string v5, "CheckinService"

    const/4 v8, 0x2

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_b

    const-string v5, "CheckinService"

    const-string v8, "Checkin disabled"

    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :cond_b
    invoke-virtual {v10}, Landroid/app/PendingIntent;->cancel()V

    .line 369
    const/4 v5, 0x0

    goto :goto_1

    .line 372
    :cond_c
    const-string v5, "CheckinService"

    const/4 v8, 0x2

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 373
    const-string v5, "CheckinService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Checkin scheduled at "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_d
    const-string v5, "alarm"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/gsf/checkin/CheckinService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/AlarmManager;

    .line 376
    .local v4, "am":Landroid/app/AlarmManager;
    iget-wide v8, v15, Lcom/android/common/OperationScheduler$Options;->periodicIntervalMillis:J

    const-wide/16 v24, 0x0

    cmp-long v5, v8, v24

    if-lez v5, :cond_e

    .line 377
    const/4 v5, 0x0

    iget-wide v8, v15, Lcom/android/common/OperationScheduler$Options;->periodicIntervalMillis:J

    invoke-virtual/range {v4 .. v10}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 378
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 380
    :cond_e
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v6, v7, v10}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 381
    const/4 v5, 0x0

    goto/16 :goto_1
.end method

.method public static getLastCheckinSuccessTime(Landroid/content/Context;)J
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 412
    new-instance v0, Lcom/android/common/OperationScheduler;

    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinService;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    invoke-virtual {v0}, Lcom/android/common/OperationScheduler;->getLastSuccessTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 386
    const-string v0, "CheckinService"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private handleResponse(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;)V
    .locals 13
    .param p1, "request"    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;
    .param p2, "response"    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    .prologue
    const v12, 0x108008a

    const/4 v11, 0x0

    .line 279
    const-string v8, "CheckinService"

    invoke-virtual {p0, v8, v11}, Lcom/google/android/gsf/checkin/CheckinService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 280
    .local v7, "sp":Landroid/content/SharedPreferences;
    if-eqz p2, :cond_2

    .line 281
    invoke-static {p2}, Lcom/google/android/gsf/checkin/CheckinResponseProcessor;->getIntents(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;)[Landroid/content/Intent;

    move-result-object v0

    .local v0, "arr$":[Landroid/content/Intent;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 282
    .local v2, "intent":Landroid/content/Intent;
    const-string v8, "CheckinService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "From server: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.google.android.checkin.INVALIDATE"

    if-ne v8, v9, :cond_0

    .line 284
    invoke-virtual {p1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->getAccountCookieList()Ljava/util/List;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->invalidateAuthTokens(Landroid/content/Context;Ljava/util/List;)V

    .line 281
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 286
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gsf/checkin/CheckinService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 289
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    const-string v8, "CheckinService_ignore_net"

    invoke-interface {v7, v8, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 290
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "CheckinService_ignore_net"

    invoke-interface {v8, v9, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 295
    .end local v0    # "arr$":[Landroid/content/Intent;
    .end local v1    # "i$":I
    .end local v3    # "len$":I
    :cond_2
    const-string v8, "CheckinService_notify"

    invoke-interface {v7, v8, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 296
    if-eqz p2, :cond_4

    const-string v4, "checkin succeeded"

    .line 297
    .local v4, "message":Ljava/lang/String;
    :goto_2
    new-instance v5, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v5, v12, v4, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 299
    .local v5, "n":Landroid/app/Notification;
    iget v8, v5, Landroid/app/Notification;->flags:I

    or-int/lit8 v8, v8, 0x10

    iput v8, v5, Landroid/app/Notification;->flags:I

    .line 300
    const/4 v8, 0x0

    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v11, v9, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v5, p0, v4, v8, v9}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 303
    const-string v8, "notification"

    invoke-virtual {p0, v8}, Lcom/google/android/gsf/checkin/CheckinService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    .line 304
    .local v6, "nm":Landroid/app/NotificationManager;
    invoke-virtual {v6, v12, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 305
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "CheckinService_notify"

    invoke-interface {v8, v9}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 307
    .end local v4    # "message":Ljava/lang/String;
    .end local v5    # "n":Landroid/app/Notification;
    .end local v6    # "nm":Landroid/app/NotificationManager;
    :cond_3
    return-void

    .line 296
    :cond_4
    const-string v4, "checkin failed"

    goto :goto_2
.end method

.method private static launchService(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 180
    const-class v2, Lcom/google/android/gsf/checkin/CheckinService;

    monitor-enter v2

    .line 181
    :try_start_0
    sget-object v1, Lcom/google/android/gsf/checkin/CheckinService;->sHandoffWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 182
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 183
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string v3, "Checkin Handoff"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/google/android/gsf/checkin/CheckinService;->sHandoffWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 184
    sget-object v1, Lcom/google/android/gsf/checkin/CheckinService;->sHandoffWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 186
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gsf/checkin/CheckinService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 188
    return-void

    .line 186
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private launchTask()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x0

    .line 196
    iget v7, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTaskTriggerCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTaskTriggerCount:I

    .line 198
    const-string v7, "CheckinService"

    invoke-static {v7, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 199
    const-string v7, "CheckinService"

    const-string v8, "launchTask"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_0
    iget-object v7, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTask:Lcom/google/android/gsf/checkin/CheckinTask;

    if-eqz v7, :cond_4

    .line 204
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/CheckinService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 205
    .local v6, "resolver":Landroid/content/ContentResolver;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTaskStartedUptime:J

    sub-long v4, v8, v10

    .line 206
    .local v4, "millisRunning":J
    const-wide/16 v8, 0x3e8

    const-string v7, "checkin_watchdog_seconds"

    const-wide/16 v10, 0xe10

    invoke-static {v6, v7, v10, v11}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    mul-long v2, v8, v10

    .line 209
    .local v2, "millisMax":J
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-lez v7, :cond_2

    cmp-long v7, v4, v2

    if-lez v7, :cond_2

    .line 212
    const-string v7, "wtf_is_fatal"

    invoke-static {v6, v7, v12}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_1

    .line 213
    const-wide/32 v8, 0xea60

    invoke-static {v8, v9}, Landroid/os/SystemClock;->sleep(J)V

    .line 217
    :cond_1
    const-string v7, "CheckinService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Checkin still running after "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "ms"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v7

    invoke-static {v7}, Landroid/os/Process;->killProcess(I)V

    .line 219
    const/16 v7, 0xa

    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    .line 222
    :cond_2
    const-string v7, "CheckinService"

    invoke-static {v7, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 223
    const-string v7, "CheckinService"

    const-string v8, "checkinRunning"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    .end local v2    # "millisMax":J
    .end local v4    # "millisRunning":J
    .end local v6    # "resolver":Landroid/content/ContentResolver;
    :cond_3
    :goto_0
    return-void

    .line 228
    :cond_4
    iget v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTaskTriggerCount:I

    .line 229
    .local v0, "lastTriggerCount":I
    new-instance v1, Lcom/google/android/gsf/checkin/CheckinTask$Params;

    invoke-direct {v1}, Lcom/google/android/gsf/checkin/CheckinTask$Params;-><init>()V

    .line 230
    .local v1, "params":Lcom/google/android/gsf/checkin/CheckinTask$Params;
    iput-object p0, v1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    .line 231
    const-string v7, "dropbox"

    invoke-virtual {p0, v7}, Lcom/google/android/gsf/checkin/CheckinService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/DropBoxManager;

    iput-object v7, v1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->dropbox:Landroid/os/DropBoxManager;

    .line 232
    const-string v7, "CheckinService"

    invoke-virtual {p0, v7, v12}, Lcom/google/android/gsf/checkin/CheckinService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, v1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    .line 233
    new-instance v7, Lcom/android/common/OperationScheduler;

    iget-object v8, v1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    invoke-direct {v7, v8}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v7, v1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    .line 235
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTaskStartedUptime:J

    .line 236
    new-instance v7, Lcom/google/android/gsf/checkin/CheckinService$1;

    invoke-direct {v7, p0, v0}, Lcom/google/android/gsf/checkin/CheckinService$1;-><init>(Lcom/google/android/gsf/checkin/CheckinService;I)V

    iput-object v7, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTask:Lcom/google/android/gsf/checkin/CheckinTask;

    .line 267
    iget-object v7, p0, Lcom/google/android/gsf/checkin/CheckinService;->mTask:Lcom/google/android/gsf/checkin/CheckinTask;

    const/4 v8, 0x1

    new-array v8, v8, [Lcom/google/android/gsf/checkin/CheckinTask$Params;

    aput-object v1, v8, v12

    invoke-virtual {v7, v8}, Lcom/google/android/gsf/checkin/CheckinTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static wasSystemUpgraded(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 392
    sget-boolean v3, Lcom/google/android/gsf/checkin/CheckinService;->sSystemWasUpgraded:Z

    if-eqz v3, :cond_0

    .line 408
    :goto_0
    return v2

    .line 396
    :cond_0
    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinService;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 398
    .local v1, "sp":Landroid/content/SharedPreferences;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->RADIO:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "build":Ljava/lang/String;
    const-string v3, "CheckinService_lastBuild"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 400
    const/4 v2, 0x0

    goto :goto_0

    .line 405
    :cond_1
    sput-boolean v2, Lcom/google/android/gsf/checkin/CheckinService;->sSystemWasUpgraded:Z

    .line 407
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "CheckinService_lastBuild"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 149
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mServiceWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mServiceWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/checkin/CheckinService;->mServiceWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 156
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x1

    .line 159
    iget-object v1, p0, Lcom/google/android/gsf/checkin/CheckinService;->mServiceWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 160
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/CheckinService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 161
    .local v0, "pm":Landroid/os/PowerManager;
    const-string v1, "Checkin Service"

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/checkin/CheckinService;->mServiceWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 162
    iget-object v1, p0, Lcom/google/android/gsf/checkin/CheckinService;->mServiceWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 165
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    const-class v2, Lcom/google/android/gsf/checkin/CheckinService;

    monitor-enter v2

    .line 166
    :try_start_0
    sget-object v1, Lcom/google/android/gsf/checkin/CheckinService;->sHandoffWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 167
    sget-object v1, Lcom/google/android/gsf/checkin/CheckinService;->sHandoffWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 168
    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/gsf/checkin/CheckinService;->sHandoffWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 170
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    invoke-direct {p0}, Lcom/google/android/gsf/checkin/CheckinService;->launchTask()V

    .line 173
    return v3

    .line 170
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
