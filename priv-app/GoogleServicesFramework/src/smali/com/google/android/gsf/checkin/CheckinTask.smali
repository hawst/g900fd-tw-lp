.class public Lcom/google/android/gsf/checkin/CheckinTask;
.super Landroid/os/AsyncTask;
.source "CheckinTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/checkin/CheckinTask$Params;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/gsf/checkin/CheckinTask$Params;",
        "Ljava/lang/Void;",
        "Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected request:Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 94
    return-void
.end method

.method private static combineResponses(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 9
    .param p0, "older"    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .param p1, "newer"    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntentCount()I

    move-result v8

    .line 374
    .local v8, "oldNum":I
    invoke-virtual {p1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntentCount()I

    move-result v5

    .line 375
    .local v5, "newNum":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v8, :cond_2

    .line 376
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntent(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    move-result-object v7

    .line 377
    .local v7, "oldIntent":Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;
    invoke-virtual {v7}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 381
    .local v6, "oldAction":Ljava/lang/String;
    const/4 v1, 0x0

    .line 382
    .local v1, "is_dupe":Z
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-ge v2, v5, :cond_0

    if-nez v1, :cond_0

    .line 383
    invoke-virtual {p1, v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntent(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    move-result-object v4

    .line 384
    .local v4, "newIntent":Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;
    invoke-virtual {v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 385
    .local v3, "newAction":Ljava/lang/String;
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 382
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 388
    .end local v3    # "newAction":Ljava/lang/String;
    .end local v4    # "newIntent":Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;
    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {p1, v7}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->addIntent(Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    .line 375
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 391
    .end local v1    # "is_dupe":Z
    .end local v2    # "j":I
    .end local v6    # "oldAction":Ljava/lang/String;
    .end local v7    # "oldIntent":Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;
    :cond_2
    return-object p1
.end method

.method private static haveExpDetectUserConsent(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 403
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_user_consent"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_enable"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static makeRequest(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;J)J
    .locals 20
    .param p0, "params"    # Lcom/google/android/gsf/checkin/CheckinTask$Params;
    .param p1, "request"    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;
    .param p2, "bookmark"    # J

    .prologue
    .line 203
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    .line 205
    .local v4, "cm":Landroid/net/ConnectivityManager;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v6, "phone"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/telephony/TelephonyManager;

    .line 207
    .local v18, "tm":Landroid/telephony/TelephonyManager;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v6, "wifi"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/net/wifi/WifiManager;

    .line 210
    .local v19, "wm":Landroid/net/wifi/WifiManager;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addBuildProperties(Landroid/content/SharedPreferences;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addPackageProperties(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 212
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v4, v2}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addNetworkProperties(Landroid/telephony/TelephonyManager;Landroid/net/wifi/WifiManager;Landroid/net/ConnectivityManager;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 213
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addLocaleProperty(Ljava/util/Locale;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 214
    const-wide/16 v16, 0x0

    .line 215
    .local v16, "securityToken":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    if-eqz v5, :cond_0

    .line 216
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    const-string v6, "CheckinTask_securityToken"

    const-wide/16 v8, 0x0

    invoke-interface {v5, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    .line 218
    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v5, v16, v6

    if-nez v5, :cond_1

    .line 220
    :try_start_0
    new-instance v14, Ljava/io/DataInputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v6, "security_token"

    invoke-virtual {v5, v6}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v5

    invoke-direct {v14, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 222
    .local v14, "dis":Ljava/io/DataInputStream;
    invoke-virtual {v14}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v16

    .line 223
    invoke-virtual {v14}, Ljava/io/DataInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    .end local v14    # "dis":Ljava/io/DataInputStream;
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-wide/from16 v0, v16

    move-object/from16 v2, p1

    invoke-static {v5, v0, v1, v2}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addIdProperties(Landroid/content/Context;JLcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 231
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addAccountInfo(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 232
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addTimeZone(Ljava/util/TimeZone;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 233
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addDeviceConfiguration(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 234
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addRequestedGroups(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    .line 236
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->dropbox:Landroid/os/DropBoxManager;

    if-eqz v5, :cond_2

    .line 237
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    .line 238
    .local v13, "cr":Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gsf/checkin/CheckinTask;->haveExpDetectUserConsent(Landroid/content/Context;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->dropbox:Landroid/os/DropBoxManager;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->maxRequestBytes:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->maxEventBytes:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "checkin_dropbox_upload"

    aput-object v11, v9, v10

    invoke-static {v13, v9}, Lcom/google/android/gsf/Gservices;->getStringsByPrefix(Landroid/content/ContentResolver;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v9

    move-wide/from16 v10, p2

    move-object/from16 v12, p1

    invoke-static/range {v5 .. v12}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addEvents(ZLandroid/os/DropBoxManager;IILjava/util/Map;JLcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)J

    move-result-wide p2

    .line 244
    .end local v13    # "cr":Landroid/content/ContentResolver;
    :cond_2
    return-wide p2

    .line 226
    :catch_0
    move-exception v15

    .line 227
    .local v15, "e":Ljava/io/IOException;
    const-string v5, "CheckinTask"

    const-string v6, "Error reading backup security token file"

    invoke-static {v5, v6, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 224
    .end local v15    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    goto :goto_0
.end method

.method private static maybeSetTime(Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/checkin/CheckinTask$Params;)Z
    .locals 12
    .param p0, "client"    # Lorg/apache/http/client/HttpClient;
    .param p1, "params"    # Lcom/google/android/gsf/checkin/CheckinTask$Params;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 283
    iget-object v8, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    const-string v9, "https:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 309
    :goto_0
    return v3

    .line 284
    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "http:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    const/4 v10, 0x6

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 285
    .local v1, "request":Lorg/apache/http/client/methods/HttpPost;
    const-string v8, "Content-type"

    const-string v9, "application/x-protobuffer"

    invoke-virtual {v1, v8, v9}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    new-instance v8, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-static {v3}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->newRequest(I)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->toByteArray()[B

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v1, v8}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 288
    invoke-interface {p0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/google/android/gsf/checkin/CheckinTask;->parseResponse(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/HttpResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v2

    .line 289
    .local v2, "response":Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    invoke-virtual {v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasTimeMsec()Z

    move-result v8

    if-nez v8, :cond_1

    .line 290
    const-string v8, "CheckinTask"

    const-string v9, "No time of day in checkin server response"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 294
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 295
    .local v6, "systemTime":J
    invoke-virtual {v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getTimeMsec()J

    move-result-wide v4

    .line 299
    .local v4, "serverTime":J
    sub-long v8, v4, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    iget-wide v10, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->minTimeAdjustmentMillis:J

    cmp-long v8, v8, v10

    if-gez v8, :cond_2

    .line 300
    const-string v8, "CheckinTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Server time agrees: delta "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sub-long v10, v4, v6

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " msec"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 302
    :cond_2
    iget-wide v8, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->minTimeSettingMillis:J

    cmp-long v8, v4, v8

    if-gez v8, :cond_3

    .line 303
    const-string v8, "CheckinTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Server time is curiously old: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 306
    :cond_3
    const-string v3, "CheckinTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Setting time from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v3, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v8, "alarm"

    invoke-virtual {v3, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 308
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-virtual {v0, v4, v5}, Landroid/app/AlarmManager;->setTime(J)V

    .line 309
    const/4 v3, 0x1

    goto/16 :goto_0
.end method

.method private static parseResponse(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/HttpResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 11
    .param p0, "params"    # Lcom/google/android/gsf/checkin/CheckinTask$Params;
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    const-string v8, "Retry-After"

    invoke-interface {p1, v8}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v6

    .line 316
    .local v6, "retryAfter":Lorg/apache/http/Header;
    if-eqz v6, :cond_0

    iget-object v8, p0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    if-eqz v8, :cond_0

    .line 317
    iget-object v8, p0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeHttp(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 318
    const-string v8, "CheckinTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Got Retry-After: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :cond_0
    :goto_0
    const-string v8, "Content-Type"

    invoke-interface {p1, v8}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 329
    .local v0, "contentType":Lorg/apache/http/Header;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_3

    .line 330
    :cond_1
    new-instance v8, Ljava/io/IOException;

    const-string v9, "No Content-Type header"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 320
    .end local v0    # "contentType":Lorg/apache/http/Header;
    :cond_2
    const-string v8, "CheckinTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Can\'t parse Retry-After: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 332
    .restart local v0    # "contentType":Lorg/apache/http/Header;
    :cond_3
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    const-string v9, "application/x-protobuffer"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 333
    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bad Content-Type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 336
    :cond_4
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    .line 337
    .local v7, "status":Lorg/apache/http/StatusLine;
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 338
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    invoke-interface {v7}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    const/16 v9, 0xc8

    if-eq v8, v9, :cond_6

    .line 339
    if-eqz v2, :cond_5

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 340
    :cond_5
    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Rejected response from server: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 342
    :cond_6
    if-nez v2, :cond_7

    .line 343
    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Empty response from server: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 346
    :cond_7
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 347
    .local v3, "input":Ljava/io/InputStream;
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v1

    .line 348
    .local v1, "encoding":Lorg/apache/http/Header;
    if-eqz v1, :cond_8

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    const-string v9, "gzip"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 349
    new-instance v4, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v4, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .end local v3    # "input":Ljava/io/InputStream;
    .local v4, "input":Ljava/io/InputStream;
    move-object v3, v4

    .line 352
    .end local v4    # "input":Ljava/io/InputStream;
    .restart local v3    # "input":Ljava/io/InputStream;
    :cond_8
    new-instance v5, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    invoke-direct {v5}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;-><init>()V

    .line 354
    .local v5, "parsed":Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    :try_start_0
    invoke-static {v3}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 356
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 359
    invoke-virtual {v5}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasStatsOk()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-virtual {v5}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getStatsOk()Z

    move-result v8

    if-nez v8, :cond_a

    .line 360
    :cond_9
    new-instance v8, Ljava/io/IOException;

    const-string v9, "Server refused checkin"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 356
    :catchall_0
    move-exception v8

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v8

    .line 363
    :cond_a
    return-object v5
.end method

.method private static sendRequest(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 12
    .param p0, "params"    # Lcom/google/android/gsf/checkin/CheckinTask$Params;
    .param p1, "client"    # Lorg/apache/http/client/HttpClient;
    .param p2, "request"    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    iget-object v7, p0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    invoke-direct {v4, v7}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 251
    .local v4, "httpRequest":Lorg/apache/http/client/methods/HttpPost;
    const-string v7, "Content-type"

    const-string v8, "application/x-protobuffer"

    invoke-virtual {v4, v7, v8}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 254
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v3, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 255
    .local v3, "gzipos":Ljava/util/zip/GZIPOutputStream;
    invoke-static {v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->newInstance(Ljava/io/OutputStream;)Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    move-result-object v6

    .line 256
    .local v6, "micro":Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    invoke-virtual {p2, v6}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V

    .line 257
    invoke-virtual {v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->flush()V

    .line 258
    invoke-virtual {v3}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 260
    new-instance v2, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-direct {v2, v7}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 261
    .local v2, "entity":Lorg/apache/http/entity/ByteArrayEntity;
    const-string v7, "gzip"

    invoke-virtual {v2, v7}, Lorg/apache/http/entity/ByteArrayEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 262
    invoke-virtual {v4, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 263
    const-string v7, "Accept-Encoding"

    const-string v8, "gzip"

    invoke-virtual {v4, v7, v8}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    :try_start_0
    const-string v7, "CheckinTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sending checkin request ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lorg/apache/http/entity/ByteArrayEntity;->getContentLength()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bytes)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-interface {p1, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 275
    .local v5, "httpResponse":Lorg/apache/http/HttpResponse;
    :goto_0
    invoke-static {p0, v5}, Lcom/google/android/gsf/checkin/CheckinTask;->parseResponse(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/HttpResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v7

    return-object v7

    .line 269
    .end local v5    # "httpResponse":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e":Ljavax/net/ssl/SSLException;
    const-string v7, "CheckinTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SSL error, attempting time correction: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-static {p1, p0}, Lcom/google/android/gsf/checkin/CheckinTask;->maybeSetTime(Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/checkin/CheckinTask$Params;)Z

    move-result v7

    if-nez v7, :cond_0

    throw v1

    .line 272
    :cond_0
    invoke-interface {p1, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .restart local v5    # "httpResponse":Lorg/apache/http/HttpResponse;
    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/google/android/gsf/checkin/CheckinTask$Params;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 26
    .param p1, "args"    # [Lcom/google/android/gsf/checkin/CheckinTask$Params;

    .prologue
    .line 127
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    const/16 v22, 0x1

    move/from16 v0, v19

    move/from16 v1, v22

    if-eq v0, v1, :cond_0

    new-instance v19, Ljava/lang/IllegalArgumentException;

    const-string v22, "Must be one Params object"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 128
    :cond_0
    const/16 v19, 0x0

    aget-object v16, p1, v19

    .line 130
    .local v16, "params":Lcom/google/android/gsf/checkin/CheckinTask$Params;
    const/4 v6, 0x0

    .line 131
    .local v6, "client":Lcom/google/android/common/http/GoogleHttpClient;
    const/4 v8, 0x0

    .line 132
    .local v8, "combined":Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    const/4 v9, 0x0

    .line 135
    .local v9, "count":I
    const-wide/16 v4, 0x0

    .line 136
    .local v4, "bookmark":J
    :try_start_0
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v22, "CheckinTask_bookmark"

    const-wide/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-wide/from16 v2, v24

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 138
    :cond_1
    new-instance v7, Lcom/google/android/common/http/GoogleHttpClient;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v22, "Android-Checkin/2.0"

    const/16 v23, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v7, v0, v1, v2}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    .end local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .local v7, "client":Lcom/google/android/common/http/GoogleHttpClient;
    :goto_0
    :try_start_1
    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->maxRequests:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v9, v0, :cond_2

    .line 141
    move-wide v14, v4

    .line 142
    .local v14, "oldBookmark":J
    invoke-static {v9}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->newRequest(I)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gsf/checkin/CheckinTask;->request:Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask;->request:Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/gsf/checkin/CheckinTask;->makeRequest(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;J)J

    move-result-wide v4

    .line 144
    cmp-long v19, v14, v4

    if-nez v19, :cond_5

    if-lez v9, :cond_5

    .line 188
    .end local v14    # "oldBookmark":J
    :cond_2
    const-string v19, "CheckinTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Checkin success: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " requests sent)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/common/OperationScheduler;->onSuccess()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 195
    :cond_3
    if-eqz v7, :cond_e

    invoke-virtual {v7}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    move-object v6, v7

    .line 198
    .end local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    :cond_4
    :goto_1
    return-object v8

    .line 146
    .end local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v14    # "oldBookmark":J
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask;->request:Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-static {v0, v7, v1}, Lcom/google/android/gsf/checkin/CheckinTask;->sendRequest(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v17

    .line 147
    .local v17, "response":Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/common/OperationScheduler;->resetTransientError()V

    .line 148
    :cond_6
    if-nez v8, :cond_b

    move-object/from16 v8, v17

    .line 150
    :goto_2
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    if-eqz v19, :cond_9

    .line 151
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v13

    .line 153
    .local v13, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v19, "CheckinTask_bookmark"

    move-object/from16 v0, v19

    invoke-interface {v13, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 155
    if-eqz v17, :cond_8

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSecurityToken()Z

    move-result v19

    if-eqz v19, :cond_8

    .line 158
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSecurityToken()J

    move-result-wide v20

    .line 159
    .local v20, "value":J
    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-eqz v19, :cond_8

    .line 161
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v22, "CheckinTask_securityToken"

    const-wide/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-wide/from16 v2, v24

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-eqz v19, :cond_7

    .line 162
    const-string v19, "CheckinTask_securityToken"

    move-object/from16 v0, v19

    move-wide/from16 v1, v20

    invoke-interface {v13, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 166
    :cond_7
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    .line 167
    .local v10, "dir":Ljava/io/File;
    new-instance v18, Ljava/io/File;

    const-string v19, "security_token"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v10, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 168
    .local v18, "tokenFile":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v19

    if-nez v19, :cond_8

    .line 169
    new-instance v11, Ljava/io/DataOutputStream;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v22, "security_token"

    const/16 v23, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 173
    .local v11, "dos":Ljava/io/DataOutputStream;
    move-wide/from16 v0, v20

    invoke-virtual {v11, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 174
    invoke-virtual {v11}, Ljava/io/DataOutputStream;->close()V

    .line 179
    .end local v10    # "dir":Ljava/io/File;
    .end local v11    # "dos":Ljava/io/DataOutputStream;
    .end local v18    # "tokenFile":Ljava/io/File;
    .end local v20    # "value":J
    :cond_8
    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 182
    .end local v13    # "edit":Landroid/content/SharedPreferences$Editor;
    :cond_9
    if-eqz v17, :cond_a

    .line 183
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/gsf/checkin/CheckinResponseProcessor;->updateGservices(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;Landroid/content/ContentResolver;)V

    .line 140
    :cond_a
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 148
    :cond_b
    move-object/from16 v0, v17

    invoke-static {v8, v0}, Lcom/google/android/gsf/checkin/CheckinTask;->combineResponses(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v8

    goto/16 :goto_2

    .line 190
    .end local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .end local v14    # "oldBookmark":J
    .end local v17    # "response":Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .restart local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    :catch_0
    move-exception v12

    .line 191
    .local v12, "e":Ljava/io/IOException;
    :goto_3
    :try_start_3
    const-string v19, "CheckinTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Checkin failed: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " (request #"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "): "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v19, v0

    if-eqz v19, :cond_c

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/common/OperationScheduler;->onTransientError()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 195
    :cond_c
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    goto/16 :goto_1

    .end local v12    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v19

    :goto_4
    if-eqz v6, :cond_d

    invoke-virtual {v6}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    :cond_d
    throw v19

    .end local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    :catchall_1
    move-exception v19

    move-object v6, v7

    .end local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    goto :goto_4

    .line 190
    .end local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    :catch_1
    move-exception v12

    move-object v6, v7

    .end local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    goto :goto_3

    .end local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    :cond_e
    move-object v6, v7

    .end local v7    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v6    # "client":Lcom/google/android/common/http/GoogleHttpClient;
    goto/16 :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 84
    check-cast p1, [Lcom/google/android/gsf/checkin/CheckinTask$Params;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/checkin/CheckinTask;->doInBackground([Lcom/google/android/gsf/checkin/CheckinTask$Params;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v0

    return-object v0
.end method
