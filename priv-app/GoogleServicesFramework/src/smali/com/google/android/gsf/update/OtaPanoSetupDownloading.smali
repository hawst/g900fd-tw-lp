.class public Lcom/google/android/gsf/update/OtaPanoSetupDownloading;
.super Landroid/app/Fragment;
.source "OtaPanoSetupDownloading.java"


# instance fields
.field private mBytesDownloaded:I

.field private mDownloadSize:I

.field private mMbRemainingFormat:Ljava/lang/String;

.field private mPercentProgress:Ljava/lang/String;

.field private mPercentText:Landroid/widget/TextView;

.field private mProgress:I

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mRemainingText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method private setProgressView()V
    .locals 5

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgressBar:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgress:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mPercentText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mPercentText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mPercentProgress:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgress:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :cond_1
    return-void
.end method

.method private setRemainingTextView()V
    .locals 6

    .prologue
    .line 121
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mRemainingText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 122
    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mDownloadSize:I

    iget v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mBytesDownloaded:I

    sub-int/2addr v1, v2

    const v2, 0xf4240

    div-int v0, v1, v2

    .line 123
    .local v0, "rmb":I
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mRemainingText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mMbRemainingFormat:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    .end local v0    # "rmb":I
    :cond_0
    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mBytesDownloaded:I

    int-to-double v2, v1

    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mDownloadSize:I

    int-to-double v4, v1

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->setProgress(I)V

    .line 127
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 46
    if-eqz p1, :cond_0

    .line 47
    const-string v0, "download_size"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mDownloadSize:I

    .line 48
    const-string v0, "progress"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgress:I

    .line 49
    const-string v0, "remaining_size"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mBytesDownloaded:I

    .line 52
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "helium"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 67
    const v8, 0x7f03001e

    invoke-virtual {p1, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 68
    .local v0, "content":Landroid/view/View;
    const v8, 0x7f0f001d

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 69
    .local v2, "descArea":Landroid/widget/FrameLayout;
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 70
    .local v6, "res":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 72
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v8, "update_title"

    invoke-static {v1, v8}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 73
    .local v7, "title":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 74
    const v8, 0x7f0a00ef

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 77
    :cond_0
    const-string v8, "update_description"

    invoke-static {v1, v8}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 78
    .local v5, "html":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 79
    const v8, 0x7f0a00f0

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 81
    :cond_1
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 84
    .local v4, "descriptionMarkup":Landroid/text/Spanned;
    const v8, 0x7f030027

    invoke-virtual {p1, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 85
    .local v3, "description":Landroid/view/View;
    const v8, 0x7f0a00f1

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mMbRemainingFormat:Ljava/lang/String;

    .line 86
    const v8, 0x7f0a00f2

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mPercentProgress:Ljava/lang/String;

    .line 87
    const v8, 0x7f0f001a

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    const v8, 0x7f0f001b

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    const v8, 0x7f0f006f

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mPercentText:Landroid/widget/TextView;

    .line 90
    const v8, 0x7f0f0064

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ProgressBar;

    iput-object v8, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgressBar:Landroid/widget/ProgressBar;

    .line 91
    iget-object v8, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v9, 0x64

    invoke-virtual {v8, v9}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 92
    iget-object v8, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v8, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 93
    iget-object v8, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v8, v10}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 94
    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 96
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->setRemainingTextView()V

    .line 97
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->setProgressView()V

    .line 99
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    const-string v0, "download_size"

    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mDownloadSize:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    const-string v0, "progress"

    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgress:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 60
    const-string v0, "remaining_size"

    iget v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mBytesDownloaded:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 61
    return-void
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "percent"    # I

    .prologue
    .line 130
    if-gez p1, :cond_1

    .line 131
    const/4 p1, 0x0

    .line 135
    :cond_0
    :goto_0
    iput p1, p0, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->mProgress:I

    .line 136
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->setProgressView()V

    .line 137
    return-void

    .line 132
    :cond_1
    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    .line 133
    const/16 p1, 0x64

    goto :goto_0
.end method
