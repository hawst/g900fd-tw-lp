.class public Lcom/google/android/gsf/update/DownloadAttempt;
.super Landroid/os/AsyncTask;
.source "DownloadAttempt.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field DISPOSITION_MATCH:Ljava/util/regex/Pattern;

.field mAuthToken:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mCurrentFile:Ljava/io/File;

.field mCurrentUrl:Ljava/lang/String;

.field mDownloaded:J

.field mLastProgress:I

.field mSharedPrefs:Landroid/content/SharedPreferences;

.field mSize:J

.field volatile mStartPending:Z

.field volatile mStatus:I

.field mUrl:Ljava/net/URL;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sharedPrefs"    # Landroid/content/SharedPreferences;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "authToken"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const-wide/16 v8, -0x1

    const/4 v6, 0x0

    .line 85
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 51
    const/4 v4, 0x1

    iput v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 52
    iput-boolean v10, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mStartPending:Z

    .line 176
    const-string v4, ".*filename=\"([a-zA-Z0-9_.-]+)\""

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->DISPOSITION_MATCH:Ljava/util/regex/Pattern;

    .line 341
    const/4 v4, -0x1

    iput v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mLastProgress:I

    .line 86
    iput-object p1, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mContext:Landroid/content/Context;

    .line 87
    iput-object p2, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 89
    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 91
    .local v1, "e":Landroid/content/SharedPreferences$Editor;
    const-string v4, "dl.url"

    invoke-interface {p2, v4, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentUrl:Ljava/lang/String;

    .line 92
    iget-object v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentUrl:Ljava/lang/String;

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 93
    const-string v4, "DownloadAttempt"

    const-string v5, "URL changed from last attempt; resetting"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-static {p1, p2}, Lcom/google/android/gsf/update/DownloadAttempt;->clear(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 96
    const-string v4, "dl.url"

    invoke-interface {v1, v4, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 97
    if-eqz p4, :cond_1

    .line 98
    const-string v4, "dl.authtoken"

    invoke-interface {v1, v4, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 103
    :goto_0
    iput-object p3, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentUrl:Ljava/lang/String;

    .line 104
    iput-object p4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mAuthToken:Ljava/lang/String;

    .line 105
    iput-object v6, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    .line 106
    iput-wide v8, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    .line 107
    iput-wide v8, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    .line 115
    :goto_1
    :try_start_0
    new-instance v4, Ljava/net/URL;

    iget-object v5, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentUrl:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mUrl:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    const-string v4, "dl.filename"

    invoke-interface {p2, v4, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 123
    .local v3, "filename":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 124
    const-string v4, "download"

    invoke-virtual {p1, v4, v10}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 125
    .local v0, "dir":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    .line 126
    const-string v4, "DownloadAttempt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "current file is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    .end local v0    # "dir":Ljava/io/File;
    :goto_2
    const-string v4, "DownloadAttempt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mSize "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mDownloaded "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    if-eqz v4, :cond_0

    iget-wide v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    iget-wide v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    iget-wide v6, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 134
    iput v10, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 137
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 138
    .end local v3    # "filename":Ljava/lang/String;
    :goto_3
    return-void

    .line 100
    :cond_1
    const-string v4, "dl.authtoken"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0

    .line 109
    :cond_2
    const-string v4, "dl.size"

    invoke-interface {p2, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    .line 110
    const-string v4, "dl.downloaded"

    invoke-interface {p2, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    .line 111
    iput-object p4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mAuthToken:Ljava/lang/String;

    goto/16 :goto_1

    .line 116
    :catch_0
    move-exception v2

    .line 117
    .local v2, "ex":Ljava/net/MalformedURLException;
    invoke-static {p1, p2}, Lcom/google/android/gsf/update/DownloadAttempt;->clear(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 118
    const/4 v4, 0x5

    iput v4, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    goto :goto_3

    .line 128
    .end local v2    # "ex":Ljava/net/MalformedURLException;
    .restart local v3    # "filename":Ljava/lang/String;
    :cond_3
    iput-object v6, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    .line 129
    const-string v4, "DownloadAttempt"

    const-string v5, "current file is null"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static clear(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sharedPrefs"    # Landroid/content/SharedPreferences;

    .prologue
    .line 141
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "dl.url"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "dl.authtoken"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "dl.filename"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "dl.size"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "dl.downloaded"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 149
    const-string v5, "download"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 150
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    .line 151
    .local v2, "f":Ljava/io/File;
    const-string v5, "DownloadAttempt"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " deleting "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 150
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 154
    .end local v2    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private extend()Z
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 354
    const/4 v4, 0x0

    .line 356
    .local v4, "raf":Ljava/io/RandomAccessFile;
    :try_start_0
    iget-wide v8, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-lez v7, :cond_2

    iget-object v7, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    cmp-long v7, v8, v10

    if-gez v7, :cond_2

    .line 360
    :cond_0
    const-string v7, "DownloadAttempt"

    const-string v8, "writing zero file"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    new-instance v5, Ljava/io/RandomAccessFile;

    iget-object v7, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    const-string v8, "rw"

    invoke-direct {v5, v7, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 362
    .end local v4    # "raf":Ljava/io/RandomAccessFile;
    .local v5, "raf":Ljava/io/RandomAccessFile;
    const/16 v7, 0x1000

    :try_start_1
    new-array v0, v7, [B

    .line 363
    .local v0, "buffer":[B
    const-wide/16 v2, 0x0

    .local v2, "i":J
    :goto_0
    iget-wide v8, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    const-wide/16 v10, 0x1000

    div-long/2addr v8, v10

    cmp-long v7, v2, v8

    if-gez v7, :cond_1

    .line 364
    invoke-virtual {v5, v0}, Ljava/io/RandomAccessFile;->write([B)V

    .line 363
    const-wide/16 v8, 0x1

    add-long/2addr v2, v8

    goto :goto_0

    .line 366
    :cond_1
    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    const-wide/16 v10, 0x1000

    rem-long/2addr v8, v10

    long-to-int v8, v8

    invoke-virtual {v5, v0, v7, v8}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 367
    const-string v7, "DownloadAttempt"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file extended to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v4, v5

    .line 369
    .end local v0    # "buffer":[B
    .end local v2    # "i":J
    .end local v5    # "raf":Ljava/io/RandomAccessFile;
    .restart local v4    # "raf":Ljava/io/RandomAccessFile;
    :cond_2
    const/4 v6, 0x1

    .line 377
    if-eqz v4, :cond_3

    :try_start_2
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 379
    :cond_3
    :goto_1
    return v6

    .line 370
    :catch_0
    move-exception v1

    .line 371
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    iget-object v7, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    if-eqz v7, :cond_4

    .line 372
    iget-object v7, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 377
    :cond_4
    if-eqz v4, :cond_3

    :try_start_4
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 378
    :catch_1
    move-exception v7

    goto :goto_1

    .line 376
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 377
    :goto_3
    if-eqz v4, :cond_5

    :try_start_5
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 379
    :cond_5
    :goto_4
    throw v6

    .line 378
    :catch_2
    move-exception v7

    goto :goto_1

    :catch_3
    move-exception v7

    goto :goto_4

    .line 376
    .end local v4    # "raf":Ljava/io/RandomAccessFile;
    .restart local v5    # "raf":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "raf":Ljava/io/RandomAccessFile;
    .restart local v4    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 370
    .end local v4    # "raf":Ljava/io/RandomAccessFile;
    .restart local v5    # "raf":Ljava/io/RandomAccessFile;
    :catch_4
    move-exception v1

    move-object v4, v5

    .end local v5    # "raf":Ljava/io/RandomAccessFile;
    .restart local v4    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_2
.end method

.method private setProgress(I)V
    .locals 3
    .param p1, "percent"    # I

    .prologue
    .line 344
    iget v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mLastProgress:I

    if-le p1, v0, :cond_0

    .line 345
    iput p1, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mLastProgress:I

    .line 346
    iget-object v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "download_progress"

    iget v2, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mLastProgress:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 348
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mLastProgress:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/DownloadAttempt;->publishProgress([Ljava/lang/Object;)V

    .line 349
    const-string v0, "DownloadAttempt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "progress now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mLastProgress:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 32
    .param p1, "v"    # [Ljava/lang/Void;

    .prologue
    .line 182
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStartPending:Z

    .line 183
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    move/from16 v27, v0

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 184
    const-string v27, "DownloadAttempt"

    const-string v28, "attempting to start download from non-ready state"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/16 v27, 0x5

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    .line 330
    :cond_0
    :goto_0
    return-object v27

    .line 187
    :cond_1
    const/16 v27, 0x2

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    const-string v28, "wifi"

    invoke-virtual/range {v27 .. v28}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/net/wifi/WifiManager;

    .line 190
    .local v26, "wm":Landroid/net/wifi/WifiManager;
    const/16 v23, 0x0

    .line 191
    .local v23, "wlock":Landroid/net/wifi/WifiManager$WifiLock;
    if-eqz v26, :cond_2

    .line 192
    const/16 v27, 0x1

    const-string v28, "system update download"

    invoke-virtual/range {v26 .. v28}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v23

    .line 195
    :cond_2
    const/4 v5, 0x0

    .line 196
    .local v5, "conn":Ljava/net/HttpURLConnection;
    const/16 v20, 0x0

    .line 198
    .local v20, "progress":Z
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/DownloadAttempt;->extend()Z

    move-result v27

    if-nez v27, :cond_4

    .line 199
    const/16 v27, 0x3

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 200
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v27

    .line 327
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_3
    if-eqz v23, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v28

    if-eqz v28, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    .line 203
    :cond_4
    if-eqz v23, :cond_5

    :try_start_1
    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 205
    :cond_5
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "mUrl is "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mUrl:Ljava/net/URL;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mUrl:Ljava/net/URL;

    move-object/from16 v27, v0

    if-nez v27, :cond_7

    .line 207
    const-string v27, "DownloadAttempt"

    const-string v28, "no url to download"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/16 v27, 0x5

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 209
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v27

    .line 327
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_6
    if-eqz v23, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v28

    if-eqz v28, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 212
    :cond_7
    :try_start_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x0

    cmp-long v27, v28, v30

    if-lez v27, :cond_9

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v30, v0

    cmp-long v27, v28, v30

    if-nez v27, :cond_9

    .line 213
    const-string v27, "DownloadAttempt"

    const-string v28, "already completed"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 215
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v27

    .line 327
    if-eqz v5, :cond_8

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_8
    if-eqz v23, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v28

    if-eqz v28, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 218
    :cond_9
    const/16 v12, 0xc8

    .line 219
    .local v12, "expectStatus":I
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mUrl:Ljava/net/URL;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v27

    move-object/from16 v0, v27

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v5, v0

    .line 220
    const-string v27, "Accept-Encoding"

    const-string v28, "identity"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v5, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mAuthToken:Ljava/lang/String;

    move-object/from16 v27, v0

    if-eqz v27, :cond_c

    .line 222
    const-string v27, "Authorization"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mAuthToken:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v5, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, " including auth header "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mAuthToken:Ljava/lang/String;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    const/16 v31, 0x10

    invoke-virtual/range {v29 .. v31}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :goto_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x0

    cmp-long v27, v28, v30

    if-lez v27, :cond_a

    .line 228
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "bytes="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "-"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 229
    .local v14, "foo":Ljava/lang/String;
    const-string v27, "Range"

    move-object/from16 v0, v27

    invoke-virtual {v5, v0, v14}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "sending range request: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    const/16 v12, 0xce

    .line 233
    .end local v14    # "foo":Ljava/lang/String;
    :cond_a
    const/16 v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 235
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v15

    .line 237
    .local v15, "httpStatus":I
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "response is "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    if-eq v15, v12, :cond_f

    .line 239
    const/16 v27, 0x5

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 240
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v27

    .line 327
    if-eqz v5, :cond_b

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_b
    if-eqz v23, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v28

    if-eqz v28, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 225
    .end local v15    # "httpStatus":I
    :cond_c
    :try_start_4
    const-string v27, "DownloadAttempt"

    const-string v28, " including no auth header"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 323
    .end local v12    # "expectStatus":I
    :catch_0
    move-exception v10

    .line 324
    .local v10, "e":Ljava/io/IOException;
    :try_start_5
    const-string v27, "DownloadAttempt"

    const-string v28, "caught ioexception"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-static {v0, v1, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 325
    if-eqz v20, :cond_20

    const/16 v27, 0x4

    :goto_2
    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 327
    if-eqz v5, :cond_d

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_d
    if-eqz v23, :cond_e

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v27

    if-eqz v27, :cond_e

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 330
    .end local v10    # "e":Ljava/io/IOException;
    :cond_e
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    goto/16 :goto_0

    .line 243
    .restart local v12    # "expectStatus":I
    .restart local v15    # "httpStatus":I
    :cond_f
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    move-object/from16 v27, v0

    if-nez v27, :cond_11

    .line 244
    const-string v13, "update.zip"

    .line 245
    .local v13, "fn":Ljava/lang/String;
    const-string v27, "Content-Disposition"

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 246
    .local v9, "disp":Ljava/lang/String;
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "disposition header: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    if-eqz v9, :cond_10

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->DISPOSITION_MATCH:Ljava/util/regex/Pattern;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v17

    .line 249
    .local v17, "m":Ljava/util/regex/Matcher;
    invoke-virtual/range {v17 .. v17}, Ljava/util/regex/Matcher;->matches()Z

    move-result v27

    if-eqz v27, :cond_10

    .line 250
    const/16 v27, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v13

    .line 254
    .end local v17    # "m":Ljava/util/regex/Matcher;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    const-string v28, "dl.filename"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v13}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    const-string v28, "download"

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v8

    .line 257
    .local v8, "dir":Ljava/io/File;
    new-instance v27, Ljava/io/File;

    move-object/from16 v0, v27

    invoke-direct {v0, v8, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    .line 258
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "current file now "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    .end local v8    # "dir":Ljava/io/File;
    .end local v9    # "disp":Ljava/lang/String;
    .end local v13    # "fn":Ljava/lang/String;
    :cond_11
    const-string v27, "ETag"

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 263
    .local v11, "etag":Ljava/lang/String;
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "etag is "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v27

    move/from16 v0, v27

    int-to-long v6, v0

    .line 266
    .local v6, "contentLength":J
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "contentLength is "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v28, v0

    const-wide/16 v30, -0x1

    cmp-long v27, v28, v30

    if-nez v27, :cond_14

    .line 268
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    .line 269
    const-wide/16 v28, 0x0

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    .line 270
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/update/DownloadAttempt;->extend()Z

    move-result v27

    if-nez v27, :cond_13

    .line 271
    const/16 v27, 0x3

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 272
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v27

    .line 327
    if-eqz v5, :cond_12

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_12
    if-eqz v23, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v28

    if-eqz v28, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 275
    :cond_13
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    const-string v28, "dl.size"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-wide/from16 v2, v30

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    const-string v28, "dl.downloaded"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-wide/from16 v2, v30

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 281
    :cond_14
    const-string v27, "DownloadAttempt"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "downloaded "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " / "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " bytes"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x0

    cmp-long v27, v28, v30

    if-lez v27, :cond_19

    .line 284
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x64

    mul-long v28, v28, v30

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v30, v0

    div-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/google/android/gsf/update/DownloadAttempt;->setProgress(I)V

    .line 288
    :goto_4
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    const-wide/32 v30, 0x20000

    add-long v18, v28, v30

    .line 290
    .local v18, "next_update":J
    new-instance v22, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    move-object/from16 v27, v0

    const-string v28, "rw"

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 292
    .local v22, "raf":Ljava/io/RandomAccessFile;
    :try_start_8
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v16

    .line 293
    .local v16, "ins":Ljava/io/InputStream;
    const/16 v27, 0x1000

    move/from16 v0, v27

    new-array v4, v0, [B

    .line 294
    .local v4, "buffer":[B
    :cond_15
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v30, v0

    cmp-long v27, v28, v30

    if-gez v27, :cond_17

    .line 295
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v30, v0

    sub-long v24, v28, v30

    .line 296
    .local v24, "to_read":J
    array-length v0, v4

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v28, v0

    cmp-long v27, v24, v28

    if-lez v27, :cond_16

    array-length v0, v4

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 297
    :cond_16
    const/16 v27, 0x0

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, v16

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v4, v1, v2}, Ljava/io/InputStream;->read([BII)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v21

    .line 298
    .local v21, "r":I
    if-gez v21, :cond_1c

    .line 319
    .end local v21    # "r":I
    .end local v24    # "to_read":J
    :cond_17
    :try_start_9
    invoke-virtual/range {v22 .. v22}, Ljava/io/RandomAccessFile;->close()V

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    const-string v28, "dl.downloaded"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-wide/from16 v2, v30

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 322
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 327
    if-eqz v5, :cond_18

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_18
    if-eqz v23, :cond_e

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v27

    if-eqz v27, :cond_e

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_3

    .line 286
    .end local v4    # "buffer":[B
    .end local v16    # "ins":Ljava/io/InputStream;
    .end local v18    # "next_update":J
    .end local v22    # "raf":Ljava/io/RandomAccessFile;
    :cond_19
    const/16 v27, 0x0

    :try_start_a
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/google/android/gsf/update/DownloadAttempt;->setProgress(I)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_4

    .line 327
    .end local v6    # "contentLength":J
    .end local v11    # "etag":Ljava/lang/String;
    .end local v12    # "expectStatus":I
    .end local v15    # "httpStatus":I
    :catchall_0
    move-exception v27

    if-eqz v5, :cond_1a

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_1a
    if-eqz v23, :cond_1b

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v28

    if-eqz v28, :cond_1b

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    :cond_1b
    throw v27

    .line 299
    .restart local v4    # "buffer":[B
    .restart local v6    # "contentLength":J
    .restart local v11    # "etag":Ljava/lang/String;
    .restart local v12    # "expectStatus":I
    .restart local v15    # "httpStatus":I
    .restart local v16    # "ins":Ljava/io/InputStream;
    .restart local v18    # "next_update":J
    .restart local v21    # "r":I
    .restart local v22    # "raf":Ljava/io/RandomAccessFile;
    .restart local v24    # "to_read":J
    :cond_1c
    :try_start_b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 300
    const/16 v27, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v27

    move/from16 v2, v21

    invoke-virtual {v0, v4, v1, v2}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 302
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v28, v28, v30

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    .line 303
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    cmp-long v27, v28, v18

    if-ltz v27, :cond_1d

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    const-string v28, "dl.downloaded"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-wide/from16 v2, v30

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 305
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    const-wide/32 v30, 0x20000

    add-long v18, v28, v30

    .line 306
    const/16 v20, 0x1

    .line 309
    :cond_1d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x0

    cmp-long v27, v28, v30

    if-lez v27, :cond_1e

    .line 310
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mDownloaded:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x64

    mul-long v28, v28, v30

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mSize:J

    move-wide/from16 v30, v0

    div-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/google/android/gsf/update/DownloadAttempt;->setProgress(I)V

    .line 313
    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/update/DownloadAttempt;->isCancelled()Z

    move-result v27

    if-eqz v27, :cond_15

    .line 314
    const/16 v27, 0x6

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    .line 315
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v27

    .line 319
    :try_start_c
    invoke-virtual/range {v22 .. v22}, Ljava/io/RandomAccessFile;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 327
    if-eqz v5, :cond_1f

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 328
    :cond_1f
    if-eqz v23, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v28

    if-eqz v28, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 319
    .end local v4    # "buffer":[B
    .end local v16    # "ins":Ljava/io/InputStream;
    .end local v21    # "r":I
    .end local v24    # "to_read":J
    :catchall_1
    move-exception v27

    :try_start_d
    invoke-virtual/range {v22 .. v22}, Ljava/io/RandomAccessFile;->close()V

    throw v27
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 325
    .end local v6    # "contentLength":J
    .end local v11    # "etag":Ljava/lang/String;
    .end local v12    # "expectStatus":I
    .end local v15    # "httpStatus":I
    .end local v18    # "next_update":J
    .end local v22    # "raf":Ljava/io/RandomAccessFile;
    .restart local v10    # "e":Ljava/io/IOException;
    :cond_20
    const/16 v27, 0x5

    goto/16 :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/DownloadAttempt;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentStatus()I
    .locals 2

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mStartPending:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 166
    const/4 v0, 0x2

    .line 168
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mStatus:I

    goto :goto_0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    if-nez v0, :cond_0

    .line 158
    const/4 v0, 0x0

    .line 160
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mCurrentFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onCancelled(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 339
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/DownloadAttempt;->onCancelled(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 335
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/DownloadAttempt;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/update/DownloadAttempt;->mStartPending:Z

    .line 173
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/DownloadAttempt;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 174
    return-void
.end method
