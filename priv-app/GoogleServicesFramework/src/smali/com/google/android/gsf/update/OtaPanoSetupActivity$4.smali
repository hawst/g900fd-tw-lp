.class Lcom/google/android/gsf/update/OtaPanoSetupActivity$4;
.super Ljava/lang/Object;
.source "OtaPanoSetupActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/update/OtaPanoSetupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$4;->this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 270
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$4;->this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    # getter for: Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->access$400(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "download_progress"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 271
    .local v0, "percent":I
    # getter for: Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I
    invoke-static {}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->access$500()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 273
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$4;->this$0:Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    # getter for: Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloading:Lcom/google/android/gsf/update/OtaPanoSetupDownloading;
    invoke-static {v1}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->access$700(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)Lcom/google/android/gsf/update/OtaPanoSetupDownloading;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;->setProgress(I)V

    .line 275
    :cond_0
    return-void
.end method
