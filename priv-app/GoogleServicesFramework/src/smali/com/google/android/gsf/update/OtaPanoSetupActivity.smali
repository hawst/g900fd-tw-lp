.class public Lcom/google/android/gsf/update/OtaPanoSetupActivity;
.super Landroid/app/Activity;
.source "OtaPanoSetupActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;


# static fields
.field private static mFragmentShowing:I

.field static volatile sIsActivityUp:Z


# instance fields
.field private mCountdownEnd:J

.field private mCountdownStopTime:J

.field private mCountdownUrl:Ljava/lang/String;

.field private mDownloadUpdate:Ljava/lang/Runnable;

.field private mDownloading:Lcom/google/android/gsf/update/OtaPanoSetupDownloading;

.field private mHandler:Landroid/os/Handler;

.field private mInstallPending:Z

.field private mLastBatteryState:I

.field private mLastCheckinTime:J

.field private mLastMobile:Z

.field private mLastStatus:I

.field private mScreenReceiver:Landroid/content/BroadcastReceiver;

.field private mSharedPrefs:Landroid/content/SharedPreferences;

.field private mUnableToDownload:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;

.field private mUpdateReady:Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;

.field private mVerifyUpdate:Ljava/lang/Runnable;

.field private mVerifying:Lcom/google/android/gsf/update/OtaPanoSetupVerifying;

.field private mWatcher:Lcom/google/android/gsf/update/StateWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    sput v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    .line 82
    sput-boolean v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->sIsActivityUp:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    iput-boolean v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastMobile:Z

    .line 55
    iput-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 70
    iput-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownUrl:Ljava/lang/String;

    .line 79
    iput-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    .line 80
    iput-boolean v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mInstallPending:Z

    .line 84
    new-instance v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity$1;-><init>(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    .line 259
    new-instance v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity$3;-><init>(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mVerifyUpdate:Ljava/lang/Runnable;

    .line 268
    new-instance v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity$4;-><init>(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloadUpdate:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownStopTime:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startUpdate()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/gsf/update/OtaPanoSetupActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/update/OtaPanoSetupActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->refreshStatus(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)Lcom/google/android/gsf/update/OtaPanoSetupVerifying;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mVerifying:Lcom/google/android/gsf/update/OtaPanoSetupVerifying;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)Lcom/google/android/gsf/update/OtaPanoSetupDownloading;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/OtaPanoSetupActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloading:Lcom/google/android/gsf/update/OtaPanoSetupDownloading;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/gsf/update/OtaPanoSetupActivity;JZ)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/OtaPanoSetupActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->updateCountdownMessage(JZ)V

    return-void
.end method

.method private launchWifiSetup(I)V
    .locals 4
    .param p1, "request"    # I

    .prologue
    const/4 v1, 0x1

    .line 455
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.net.wifi.CANVAS_SETUP_WIFI_NETWORK"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 456
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "firstRun"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 457
    const-string v2, "extra_prefs_show_button_bar"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 458
    const-string v2, "extra_show_summary"

    const/4 v3, 0x2

    if-ne p1, v3, :cond_0

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 459
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startActivity(Landroid/content/Intent;)V

    .line 460
    return-void

    .line 458
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private refreshStatus(Z)V
    .locals 11
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v10, 0x0

    .line 178
    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v6, "status"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 180
    .local v4, "status":I
    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v6, "download_mobile"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 181
    .local v1, "mobile":Z
    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    invoke-virtual {v5}, Lcom/google/android/gsf/update/StateWatcher;->getBatteryState()I

    move-result v0

    .line 182
    .local v0, "batteryState":I
    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinService;->getLastCheckinSuccessTime(Landroid/content/Context;)J

    move-result-wide v2

    .line 186
    .local v2, "lastCheckinTime":J
    if-nez p1, :cond_0

    iget v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastStatus:I

    if-ne v4, v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastMobile:Z

    if-ne v1, v5, :cond_0

    iget v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastBatteryState:I

    if-ne v0, v5, :cond_0

    iget-wide v6, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastCheckinTime:J

    cmp-long v5, v2, v6

    if-nez v5, :cond_0

    .line 257
    :goto_0
    return-void

    .line 191
    :cond_0
    iput v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastStatus:I

    .line 192
    iput-boolean v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastMobile:Z

    .line 193
    iput v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastBatteryState:I

    .line 194
    iput-wide v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mLastCheckinTime:J

    .line 201
    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    iget-boolean v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mInstallPending:Z

    if-eqz v5, :cond_1

    .line 202
    const-string v5, "OtaPanoSetupActivity"

    const-string v6, "skipping refresh; about to reboot"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 206
    :cond_1
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 216
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->resetUi()V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->finish()V

    goto :goto_0

    .line 222
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startDownload()V

    goto :goto_0

    .line 227
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->showDownloading()V

    .line 228
    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloadUpdate:Ljava/lang/Runnable;

    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 234
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->showUnableToDownload()V

    goto :goto_0

    .line 239
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->showVerifyingUpdate()V

    .line 240
    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mVerifyUpdate:Ljava/lang/Runnable;

    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 245
    :pswitch_5
    iget-wide v6, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    .line 246
    iget-wide v6, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    invoke-direct {p0, v6, v7, v10}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->updateCountdownMessage(JZ)V

    goto :goto_0

    .line 248
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->showUpdateReady()V

    .line 249
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startCountdown()V

    goto :goto_0

    .line 254
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->showUnableToDownload()V

    goto :goto_0

    .line 206
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_3
    .end packed-switch
.end method

.method private resetUi()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 278
    sget v1, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    if-eqz v1, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 280
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    sget v1, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    packed-switch v1, :pswitch_data_0

    .line 298
    :goto_0
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 299
    const/4 v1, 0x0

    sput v1, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    .line 301
    .end local v0    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void

    .line 282
    .restart local v0    # "transaction":Landroid/app/FragmentTransaction;
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloading:Lcom/google/android/gsf/update/OtaPanoSetupDownloading;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 283
    iput-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloading:Lcom/google/android/gsf/update/OtaPanoSetupDownloading;

    goto :goto_0

    .line 286
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mVerifying:Lcom/google/android/gsf/update/OtaPanoSetupVerifying;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 287
    iput-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mVerifying:Lcom/google/android/gsf/update/OtaPanoSetupVerifying;

    goto :goto_0

    .line 290
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUpdateReady:Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 291
    iput-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUpdateReady:Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;

    goto :goto_0

    .line 294
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUnableToDownload:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 295
    iput-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUnableToDownload:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;

    goto :goto_0

    .line 280
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private resumeCountdown()V
    .locals 3

    .prologue
    .line 375
    iget-wide v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->updateCountdownMessage(JZ)V

    .line 376
    return-void
.end method

.method private showDownloading()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 316
    sget v1, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    if-eq v1, v2, :cond_0

    .line 318
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->resetUi()V

    .line 319
    sput v2, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    .line 320
    new-instance v1, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;

    invoke-direct {v1}, Lcom/google/android/gsf/update/OtaPanoSetupDownloading;-><init>()V

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloading:Lcom/google/android/gsf/update/OtaPanoSetupDownloading;

    .line 321
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 322
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    const v1, 0x7f0f0062

    iget-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloading:Lcom/google/android/gsf/update/OtaPanoSetupDownloading;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 323
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 325
    .end local v0    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method private showUnableToDownload()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 304
    sget v1, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    if-eq v1, v2, :cond_0

    .line 306
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->resetUi()V

    .line 307
    sput v2, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    .line 308
    invoke-static {p0}, Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;->createInstance(Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload$OnCorrectionActionListener;)Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUnableToDownload:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;

    .line 309
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 310
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    const v1, 0x7f0f0062

    iget-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUnableToDownload:Lcom/google/android/gsf/update/OtaPanoSetupUnableToDownload;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 311
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 313
    .end local v0    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method private showUpdateReady()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 340
    sget v1, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    if-eq v1, v2, :cond_0

    .line 342
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->resetUi()V

    .line 343
    sput v2, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    .line 344
    new-instance v1, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;

    invoke-direct {v1}, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;-><init>()V

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUpdateReady:Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;

    .line 345
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 346
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    const v1, 0x7f0f0062

    iget-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUpdateReady:Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 347
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 349
    .end local v0    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method private showVerifyingUpdate()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 328
    sget v1, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    if-eq v1, v2, :cond_0

    .line 330
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->resetUi()V

    .line 331
    sput v2, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    .line 332
    new-instance v1, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;

    invoke-direct {v1}, Lcom/google/android/gsf/update/OtaPanoSetupVerifying;-><init>()V

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mVerifying:Lcom/google/android/gsf/update/OtaPanoSetupVerifying;

    .line 333
    invoke-virtual {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 334
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    const v1, 0x7f0f0062

    iget-object v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mVerifying:Lcom/google/android/gsf/update/OtaPanoSetupVerifying;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 335
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 337
    .end local v0    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method private startCountdown()V
    .locals 6

    .prologue
    .line 367
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.server.checkin.CHECKIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 368
    const/16 v0, 0x2710

    .line 369
    .local v0, "delay":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    int-to-long v4, v0

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    .line 370
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v2, "url"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownUrl:Ljava/lang/String;

    .line 371
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->resumeCountdown()V

    .line 372
    return-void
.end method

.method private startDownload()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 415
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->showDownloading()V

    .line 417
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_approved"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 420
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 421
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "download_now"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 422
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 423
    return-void
.end method

.method private startUpdate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 426
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "install_approved"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 429
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->sIsActivityUp:Z

    .line 430
    iput-boolean v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mInstallPending:Z

    .line 437
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 438
    return-void
.end method

.method private stopCountdown()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 379
    iget-wide v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 380
    iput-wide v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    .line 381
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownStopTime:J

    .line 383
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownUrl:Ljava/lang/String;

    .line 384
    return-void
.end method

.method private updateCountdownMessage(JZ)V
    .locals 11
    .param p1, "endTime"    # J
    .param p3, "loop"    # Z

    .prologue
    .line 387
    iget-wide v6, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    cmp-long v5, p1, v6

    if-eqz v5, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 392
    :cond_1
    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v6, "url"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 393
    .local v4, "url":Ljava/lang/String;
    if-eqz v4, :cond_2

    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 394
    :cond_2
    const-string v5, "OtaPanoSetupActivity"

    const-string v6, "URL changed during countdown; aborting"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->stopCountdown()V

    .line 396
    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->refreshStatus(Z)V

    goto :goto_0

    .line 400
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 401
    .local v2, "now":J
    iget-wide v6, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    cmp-long v5, v2, v6

    if-ltz v5, :cond_4

    .line 402
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startUpdate()V

    goto :goto_0

    .line 404
    :cond_4
    iget-wide v6, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    sub-long/2addr v6, v2

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v5, v6

    add-int/lit8 v1, v5, 0x1

    .line 405
    .local v1, "secs":I
    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mUpdateReady:Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;

    invoke-virtual {v5, v1}, Lcom/google/android/gsf/update/OtaPanoSetupUpdateReady;->setSeconds(I)V

    .line 406
    iget-wide v6, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    add-int/lit8 v5, v1, -0x1

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v8, v5

    sub-long/2addr v6, v8

    sub-long/2addr v6, v2

    long-to-int v0, v6

    .line 407
    .local v0, "delay":I
    if-eqz p3, :cond_0

    .line 408
    iget-object v5, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/gsf/update/OtaPanoSetupActivity$6;

    invoke-direct {v6, p0, p1, p2, p3}, Lcom/google/android/gsf/update/OtaPanoSetupActivity$6;-><init>(Lcom/google/android/gsf/update/OtaPanoSetupActivity;JZ)V

    int-to-long v8, v0

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 466
    return-void
.end method

.method public onCorrectionAction(I)V
    .locals 2
    .param p1, "correctionAction"    # I

    .prologue
    .line 442
    sget v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mFragmentShowing:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 443
    packed-switch p1, :pswitch_data_0

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 445
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startDownload()V

    goto :goto_0

    .line 448
    :pswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->launchWifiSetup(I)V

    goto :goto_0

    .line 443
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    .line 97
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    const v1, 0x7f03001b

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->setContentView(I)V

    .line 101
    new-instance v1, Lcom/google/android/gsf/update/StateWatcher;

    new-instance v2, Lcom/google/android/gsf/update/OtaPanoSetupActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity$2;-><init>(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V

    invoke-direct {v1, p0, v2}, Lcom/google/android/gsf/update/StateWatcher;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    .line 107
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mHandler:Landroid/os/Handler;

    .line 108
    const-string v1, "update"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 111
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 112
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 114
    if-eqz p1, :cond_0

    .line 115
    const-string v1, "countdown_end"

    invoke-virtual {p1, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    .line 116
    iput-wide v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownStopTime:J

    .line 117
    const-string v1, "countdown_url"

    invoke-virtual {p1, v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownUrl:Ljava/lang/String;

    .line 123
    :goto_0
    return-void

    .line 119
    :cond_0
    iput-wide v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    .line 120
    iput-wide v4, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownStopTime:J

    .line 121
    iput-object v6, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 128
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-lez v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 134
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 135
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->setIntent(Landroid/content/Intent;)V

    .line 136
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "out"    # Landroid/os/Bundle;

    .prologue
    .line 170
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 171
    const-string v0, "countdown_end"

    iget-wide v2, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 172
    const-string v0, "countdown_url"

    iget-object v1, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPrefs"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 352
    const-string v0, "status"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "download_mobile"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    :cond_0
    new-instance v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity$5;-><init>(Lcom/google/android/gsf/update/OtaPanoSetupActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 364
    :cond_1
    :goto_0
    return-void

    .line 359
    :cond_2
    const-string v0, "verify_progress"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mVerifyUpdate:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 361
    :cond_3
    const-string v0, "download_progress"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mDownloadUpdate:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 140
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 143
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 144
    sput-boolean v1, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->sIsActivityUp:Z

    .line 145
    invoke-static {p0}, Lcom/google/android/gsf/update/SystemUpdateService;->cancelNotifications(Landroid/content/Context;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/StateWatcher;->start()V

    .line 147
    invoke-direct {p0, v1}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->refreshStatus(Z)V

    .line 149
    iget-wide v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mCountdownEnd:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 150
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->resumeCountdown()V

    .line 152
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 158
    invoke-direct {p0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->stopCountdown()V

    .line 159
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mWatcher:Lcom/google/android/gsf/update/StateWatcher;

    invoke-virtual {v0}, Lcom/google/android/gsf/update/StateWatcher;->stop()V

    .line 161
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->sIsActivityUp:Z

    .line 162
    iget-object v0, p0, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 165
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/OtaPanoSetupActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 166
    return-void
.end method
