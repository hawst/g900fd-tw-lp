.class public Lcom/google/android/gsf/settings/GoogleSettingsProvider;
.super Landroid/content/ContentProvider;
.source "GoogleSettingsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;
    }
.end annotation


# instance fields
.field private mLinkAssistedGps:Z

.field private mOpenHelper:Lcom/google/android/gsf/settings/DatabaseHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mLinkAssistedGps:Z

    .line 54
    return-void
.end method

.method private checkNetworkLocationOptIn(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 231
    iget-boolean v6, p0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mLinkAssistedGps:Z

    if-nez v6, :cond_1

    .line 256
    :cond_0
    return-void

    .line 234
    :cond_1
    const-string v6, "network_location_opt_in"

    const-string v7, "name"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 236
    const-string v6, "value"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 237
    .local v5, "value":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "assisted_gps_enabled"

    const-string v6, "1"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    :goto_0
    invoke-static {v7, v8, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 241
    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 242
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getManagedProfiles(Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 243
    .local v4, "profileUserId":I
    invoke-static {v0, v4}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->createUserContext(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v3

    .line 246
    .local v3, "profileContext":Landroid/content/Context;
    if-eqz v3, :cond_2

    .line 248
    :try_start_0
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 249
    :catch_0
    move-exception v1

    .line 250
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "GoogleSettingsProvider"

    const-string v7, "Caught exception trying to set NETWORK_LOCATION_OPT_IN to a managed profile"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 237
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "profileContext":Landroid/content/Context;
    .end local v4    # "profileUserId":I
    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private checkWritePermissions(Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;)V
    .locals 2
    .param p1, "args"    # Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;

    .prologue
    .line 133
    const-string v0, "partner"

    iget-object v1, p1, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->table:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.google.android.providers.settings.permission.WRITE_GSETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cannot write to Google settings table"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    return-void
.end method

.method private static createUserContext(Landroid/content/Context;I)Landroid/content/Context;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "targetUserId"    # I

    .prologue
    .line 324
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Landroid/os/UserHandle;

    invoke-direct {v3, p1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 328
    :goto_0
    return-object v1

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "GoogleSettingsProvider"

    const-string v2, "Could not find a package Context"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 328
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getManagedProfiles(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v4

    .line 338
    .local v4, "um":Landroid/os/UserManager;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v2

    .line 339
    .local v2, "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 340
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    .line 341
    .local v5, "userId":I
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/UserInfo;

    .line 343
    .local v1, "profile":Landroid/content/pm/UserInfo;
    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, v1, Landroid/content/pm/UserInfo;->id:I

    if-eq v6, v5, :cond_0

    .line 344
    iget v6, v1, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 347
    .end local v1    # "profile":Landroid/content/pm/UserInfo;
    :cond_1
    return-object v3
.end method

.method private getUriFor(Landroid/net/Uri;Landroid/content/ContentValues;J)Landroid/net/Uri;
    .locals 5
    .param p1, "tableUri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "rowId"    # J

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 102
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 104
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 105
    .local v1, "table":Ljava/lang/String;
    const-string v2, "partner"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 106
    const-string v2, "name"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    .local v0, "name":Ljava/lang/String;
    invoke-static {p1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 109
    .end local v0    # "name":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_1
    invoke-static {p1, p3, p4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method private sendNotify(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 118
    const-string v1, "notify"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "notify":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 125
    :cond_1
    return-void
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 260
    new-instance v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;

    invoke-direct {v0, p1}, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;-><init>(Landroid/net/Uri;)V

    .line 261
    .local v0, "args":Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;
    invoke-direct {p0, v0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->checkWritePermissions(Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;)V

    .line 263
    iget-object v4, p0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mOpenHelper:Lcom/google/android/gsf/settings/DatabaseHelper;

    invoke-virtual {v4}, Lcom/google/android/gsf/settings/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 264
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 266
    :try_start_0
    array-length v3, p2

    .line 267
    .local v3, "numValues":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 268
    iget-object v4, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->table:Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v6, p2, v2

    invoke-virtual {v1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    const/4 v4, 0x0

    .line 274
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 278
    :goto_1
    return v4

    .line 270
    :cond_0
    :try_start_1
    aget-object v4, p2, v2

    invoke-direct {p0, p1, v4}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->checkNetworkLocationOptIn(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 267
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 277
    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->sendNotify(Landroid/net/Uri;)V

    .line 278
    array-length v4, p2

    goto :goto_1

    .line 274
    .end local v2    # "i":I
    .end local v3    # "numValues":I
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 300
    new-instance v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 301
    .local v0, "args":Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;
    invoke-direct {p0, v0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->checkWritePermissions(Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;)V

    .line 303
    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mOpenHelper:Lcom/google/android/gsf/settings/DatabaseHelper;

    invoke-virtual {v3}, Lcom/google/android/gsf/settings/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 304
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v3, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->table:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->where:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->args:[Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 305
    .local v1, "count":I
    if-lez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->sendNotify(Landroid/net/Uri;)V

    .line 307
    :cond_0
    return v1
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "url"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 212
    new-instance v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;

    invoke-direct {v0, p1, v1, v1}, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 213
    .local v0, "args":Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;
    iget-object v1, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->where:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "vnd.android.cursor.dir/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->table:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 216
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "vnd.android.cursor.item/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->table:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8
    .param p1, "initialUrl"    # Landroid/net/Uri;
    .param p2, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x0

    .line 283
    new-instance v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;

    invoke-direct {v0, p1}, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;-><init>(Landroid/net/Uri;)V

    .line 284
    .local v0, "args":Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;
    invoke-direct {p0, v0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->checkWritePermissions(Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;)V

    .line 286
    iget-object v5, p0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mOpenHelper:Lcom/google/android/gsf/settings/DatabaseHelper;

    invoke-virtual {v5}, Lcom/google/android/gsf/settings/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 287
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v5, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->table:Ljava/lang/String;

    invoke-virtual {v1, v5, v4, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 288
    .local v2, "rowId":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-gtz v5, :cond_0

    .line 295
    :goto_0
    return-object v4

    .line 291
    :cond_0
    invoke-direct {p0, p1, p2, v2, v3}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getUriFor(Landroid/net/Uri;Landroid/content/ContentValues;J)Landroid/net/Uri;

    move-result-object v4

    .line 292
    .local v4, "url":Landroid/net/Uri;
    invoke-direct {p0, v4}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->sendNotify(Landroid/net/Uri;)V

    .line 293
    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->checkNetworkLocationOptIn(Landroid/net/Uri;Landroid/content/ContentValues;)V

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 11

    .prologue
    const/4 v9, 0x1

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 143
    .local v1, "context":Landroid/content/Context;
    new-instance v8, Lcom/google/android/gsf/settings/DatabaseHelper;

    invoke-direct {v8, v1}, Lcom/google/android/gsf/settings/DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mOpenHelper:Lcom/google/android/gsf/settings/DatabaseHelper;

    .line 145
    const-string v8, "ro.gps.agps_provider"

    const/4 v10, 0x0

    invoke-static {v8, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "agpsProvider":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    const-string v10, "google"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_0
    move v8, v9

    :goto_0
    iput-boolean v8, p0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mLinkAssistedGps:Z

    .line 150
    invoke-static {v1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v6

    .line 151
    .local v6, "um":Landroid/os/UserManager;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    .line 152
    .local v2, "myUserId":I
    invoke-virtual {v6, v2}, Landroid/os/UserManager;->getProfileParent(I)Landroid/content/pm/UserInfo;

    move-result-object v3

    .line 153
    .local v3, "parent":Landroid/content/pm/UserInfo;
    if-eqz v3, :cond_1

    iget v8, v3, Landroid/content/pm/UserInfo;->id:I

    if-eq v8, v2, :cond_1

    .line 156
    iget v8, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v1, v8}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->createUserContext(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v4

    .line 157
    .local v4, "parentContext":Landroid/content/Context;
    if-eqz v4, :cond_1

    .line 158
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v10, "network_location_opt_in"

    invoke-static {v8, v10}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 161
    .local v5, "parentValue":Ljava/lang/String;
    new-instance v7, Landroid/content/ContentValues;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Landroid/content/ContentValues;-><init>(I)V

    .line 162
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "name"

    const-string v10, "network_location_opt_in"

    invoke-virtual {v7, v8, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v8, "value"

    invoke-virtual {v7, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    sget-object v8, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v8, v7}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 168
    .end local v4    # "parentContext":Landroid/content/Context;
    .end local v5    # "parentValue":Ljava/lang/String;
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_1
    return v9

    .line 146
    .end local v2    # "myUserId":I
    .end local v3    # "parent":Landroid/content/pm/UserInfo;
    .end local v6    # "um":Landroid/os/UserManager;
    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 19
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "select"    # [Ljava/lang/String;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;
    .param p5, "sort"    # Ljava/lang/String;

    .prologue
    .line 173
    new-instance v13, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v13, v0, v1, v2}, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 174
    .local v13, "args":Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mOpenHelper:Lcom/google/android/gsf/settings/DatabaseHelper;

    invoke-virtual {v6}, Lcom/google/android/gsf/settings/DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 177
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mOpenHelper:Lcom/google/android/gsf/settings/DatabaseHelper;

    invoke-virtual {v6}, Lcom/google/android/gsf/settings/DatabaseHelper;->assistedGpsSettingNeedsUpdate()Z

    move-result v6

    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mLinkAssistedGps:Z

    if-eqz v6, :cond_0

    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v16

    .line 181
    .local v16, "resolver":Landroid/content/ContentResolver;
    const-string v6, "SELECT value FROM partner WHERE name = ?"

    invoke-virtual {v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v18

    .line 183
    .local v18, "s":Landroid/database/sqlite/SQLiteStatement;
    const/4 v6, 0x1

    const-string v7, "network_location_opt_in"

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 184
    const/4 v15, 0x0

    .line 186
    .local v15, "optIn":Z
    :try_start_0
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    const/4 v15, 0x1

    .line 190
    :goto_0
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 192
    const-string v6, "assisted_gps_enabled"

    const/4 v7, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v6, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_2

    const/4 v12, 0x1

    .line 194
    .local v12, "agpsEnabled":Z
    :goto_1
    if-eq v12, v15, :cond_0

    .line 195
    const-string v7, "assisted_gps_enabled"

    if-eqz v15, :cond_3

    const/4 v6, 0x1

    :goto_2
    move-object/from16 v0, v16

    invoke-static {v0, v7, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 200
    .end local v12    # "agpsEnabled":Z
    .end local v15    # "optIn":Z
    .end local v16    # "resolver":Landroid/content/ContentResolver;
    .end local v18    # "s":Landroid/database/sqlite/SQLiteStatement;
    :cond_0
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 201
    .local v4, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    iget-object v6, v13, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->table:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 203
    iget-object v7, v13, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->where:Ljava/lang/String;

    iget-object v8, v13, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->args:[Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v6, p2

    move-object/from16 v11, p5

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 204
    .local v17, "ret":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-interface {v0, v6, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 205
    return-object v17

    .line 186
    .end local v4    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v17    # "ret":Landroid/database/Cursor;
    .restart local v15    # "optIn":Z
    .restart local v16    # "resolver":Landroid/content/ContentResolver;
    .restart local v18    # "s":Landroid/database/sqlite/SQLiteStatement;
    :cond_1
    const/4 v15, 0x0

    goto :goto_0

    .line 187
    :catch_0
    move-exception v14

    .line 188
    .local v14, "missing":Landroid/database/sqlite/SQLiteDoneException;
    const/4 v15, 0x0

    goto :goto_0

    .line 192
    .end local v14    # "missing":Landroid/database/sqlite/SQLiteDoneException;
    :cond_2
    const/4 v12, 0x0

    goto :goto_1

    .line 195
    .restart local v12    # "agpsEnabled":Z
    :cond_3
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "initialValues"    # Landroid/content/ContentValues;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 312
    new-instance v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 313
    .local v0, "args":Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;
    invoke-direct {p0, v0}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->checkWritePermissions(Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;)V

    .line 315
    iget-object v3, p0, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->mOpenHelper:Lcom/google/android/gsf/settings/DatabaseHelper;

    invoke-virtual {v3}, Lcom/google/android/gsf/settings/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 316
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v3, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->table:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->where:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gsf/settings/GoogleSettingsProvider$SqlArguments;->args:[Ljava/lang/String;

    invoke-virtual {v2, v3, p2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 317
    .local v1, "count":I
    if-lez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/GoogleSettingsProvider;->sendNotify(Landroid/net/Uri;)V

    .line 319
    :cond_0
    return v1
.end method
