.class public Lcom/google/android/gsf/update/SystemUpdateService;
.super Landroid/app/Service;
.source "SystemUpdateService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/update/SystemUpdateService$1;,
        Lcom/google/android/gsf/update/SystemUpdateService$ThreadPerTaskExecutor;,
        Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;,
        Lcom/google/android/gsf/update/SystemUpdateService$SecretCodeReceiver;,
        Lcom/google/android/gsf/update/SystemUpdateService$Receiver;
    }
.end annotation


# static fields
.field private static final EXECUTOR:Ljava/util/concurrent/Executor;

.field static MAINTENANCE_WINDOW_PATTERN:Ljava/util/regex/Pattern;

.field private static sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;

.field private static sUpdatesLockedUntil:J

.field private static sWakeLock:Landroid/os/PowerManager$WakeLock;

.field private static sWakeLockLock:Ljava/lang/Object;


# instance fields
.field private mDownloadRetry:Lcom/android/common/OperationScheduler;

.field private mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;

.field private mIntentPending:Landroid/content/Intent;

.field private mLastBroadcastProgress:I

.field private mLastBroadcastStatus:I

.field private mNetworkWasUp:Z

.field private mPending:Landroid/app/PendingIntent;

.field private mPendingLock:Ljava/lang/Object;

.field private mRunning:Z

.field private mSharedPrefs:Landroid/content/SharedPreferences;

.field private mStartPending:Z

.field private mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

.field private mVerifierLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 236
    const-string v0, "^([0-9][0-9])([0-9][0-9])-([0-9][0-9])([0-9][0-9])$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->MAINTENANCE_WINDOW_PATTERN:Ljava/util/regex/Pattern;

    .line 247
    sput-object v1, Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;

    .line 258
    new-instance v0, Lcom/google/android/gsf/update/SystemUpdateService$ThreadPerTaskExecutor;

    invoke-direct {v0}, Lcom/google/android/gsf/update/SystemUpdateService$ThreadPerTaskExecutor;-><init>()V

    sput-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->EXECUTOR:Ljava/util/concurrent/Executor;

    .line 260
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLockLock:Ljava/lang/Object;

    .line 261
    sput-object v1, Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 263
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 246
    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 248
    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;

    .line 249
    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;

    .line 250
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;

    .line 251
    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    .line 253
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mPendingLock:Ljava/lang/Object;

    .line 254
    iput-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mRunning:Z

    .line 255
    iput-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z

    .line 256
    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mIntentPending:Landroid/content/Intent;

    .line 265
    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;

    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mNetworkWasUp:Z

    .line 1174
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLockLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100()Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler$Options;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;

    return-object v0
.end method

.method static synthetic access$102(Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;
    .locals 0
    .param p0, "x0"    # Landroid/os/PowerManager$WakeLock;

    .prologue
    .line 47
    sput-object p0, Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mVerifierLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateVerifierTask;)Lcom/google/android/gsf/update/SystemUpdateVerifierTask;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;
    .param p1, "x1"    # Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mVerifier:Lcom/google/android/gsf/update/SystemUpdateVerifierTask;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/gsf/update/SystemUpdateService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gsf/update/SystemUpdateService;->isRunningOnTv()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400()Lcom/google/android/gsf/update/DownloadAttempt;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/android/gsf/update/DownloadAttempt;)Lcom/google/android/gsf/update/DownloadAttempt;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/update/DownloadAttempt;

    .prologue
    .line 47
    sput-object p0, Lcom/google/android/gsf/update/SystemUpdateService;->sAttempt:Lcom/google/android/gsf/update/DownloadAttempt;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/app/PendingIntent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/gsf/update/SystemUpdateService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mNetworkWasUp:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mNetworkWasUp:Z

    return p1
.end method

.method static synthetic access$1700()J
    .locals 2

    .prologue
    .line 47
    sget-wide v0, Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/gsf/update/SystemUpdateService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mPendingLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mRunning:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/gsf/update/SystemUpdateService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/gsf/update/SystemUpdateService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z

    return p1
.end method

.method static synthetic access$600()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->EXECUTOR:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mIntentPending:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/gsf/update/SystemUpdateService;)Lcom/android/common/OperationScheduler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/gsf/update/SystemUpdateService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/update/SystemUpdateService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static cancelNotifications(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1139
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1141
    .local v0, "nm":Landroid/app/NotificationManager;
    const v1, 0x7f020081

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1142
    const v1, 0x7f020082

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1143
    return-void
.end method

.method private isRunningOnTv()Z
    .locals 4

    .prologue
    .line 1207
    const-string v2, "uimode"

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/UiModeManager;

    .line 1208
    .local v1, "uiMgr":Landroid/app/UiModeManager;
    invoke-virtual {v1}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    .line 1209
    .local v0, "tvMode":Z
    :goto_0
    return v0

    .line 1208
    .end local v0    # "tvMode":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static processUpdateLock(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v2, 0x0

    .line 336
    if-nez p1, :cond_0

    .line 346
    :goto_0
    return-void

    .line 337
    :cond_0
    const-string v0, "nowisconvenient"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    sput-wide v2, Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J

    .line 340
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 343
    :cond_1
    const-string v0, "timestamp"

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb800

    add-long/2addr v0, v2

    sput-wide v0, Lcom/google/android/gsf/update/SystemUpdateService;->sUpdatesLockedUntil:J

    goto :goto_0
.end method

.method static whenMobileAllowed(Landroid/content/SharedPreferences;Landroid/content/Context;)J
    .locals 7
    .param p0, "sp"    # Landroid/content/SharedPreferences;
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x3

    const-wide/16 v2, 0x0

    .line 1162
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "update_urgency"

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v6, :cond_1

    .line 1171
    :cond_0
    :goto_0
    return-wide v2

    .line 1167
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "update_mobile_network_delay"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    int-to-long v0, v4

    .line 1169
    .local v0, "delay":J
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 1171
    const-string v4, "url_change"

    invoke-interface {p0, v4, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v0

    add-long/2addr v2, v4

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 307
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x2710

    const/4 v3, 0x0

    .line 310
    const-string v1, "SystemUpdateService"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mLastBroadcastStatus:I

    .line 314
    const/4 v1, -0x2

    iput v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mLastBroadcastProgress:I

    .line 316
    const-string v1, "update"

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gsf/update/SystemUpdateService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 318
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 320
    new-instance v1, Lcom/android/common/OperationScheduler;

    const-string v2, "update.download.scheduler"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/update/SystemUpdateService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetry:Lcom/android/common/OperationScheduler;

    .line 323
    new-instance v1, Lcom/android/common/OperationScheduler$Options;

    invoke-direct {v1}, Lcom/android/common/OperationScheduler$Options;-><init>()V

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;

    .line 324
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;

    iput-wide v4, v1, Lcom/android/common/OperationScheduler$Options;->backoffFixedMillis:J

    .line 325
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mDownloadRetryOptions:Lcom/android/common/OperationScheduler$Options;

    iput-wide v4, v1, Lcom/android/common/OperationScheduler$Options;->backoffIncrementalMillis:J

    .line 327
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gsf/update/SystemUpdateService$Receiver;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x8000000

    invoke-static {p0, v3, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mPending:Landroid/app/PendingIntent;

    .line 330
    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.os.UpdateLock.UPDATE_LOCK_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/update/SystemUpdateService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 332
    .local v0, "initial":Landroid/content/Intent;
    invoke-static {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService;->processUpdateLock(Landroid/content/Context;Landroid/content/Intent;)V

    .line 333
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 350
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5
    .param p1, "sharedPrefs"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 1181
    const-string v3, "status"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "verify_progress"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "download_progress"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1183
    :cond_0
    const-string v3, "status"

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1184
    .local v2, "status":I
    const/4 v1, -0x2

    .line 1185
    .local v1, "progress":I
    packed-switch v2, :pswitch_data_0

    .line 1193
    :goto_0
    iget v3, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mLastBroadcastStatus:I

    if-ne v2, v3, :cond_2

    iget v3, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mLastBroadcastProgress:I

    if-ne v1, v3, :cond_2

    .line 1204
    .end local v1    # "progress":I
    .end local v2    # "status":I
    :cond_1
    :goto_1
    return-void

    .line 1187
    .restart local v1    # "progress":I
    .restart local v2    # "status":I
    :pswitch_0
    const-string v3, "verify_progress"

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1188
    goto :goto_0

    .line 1190
    :pswitch_1
    const-string v3, "download_progress"

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    .line 1195
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.update.SYSTEM_UPDATE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1196
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "status"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1197
    const/4 v3, -0x2

    if-eq v1, v3, :cond_3

    .line 1198
    const-string v3, "progress"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1200
    :cond_3
    iput v2, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mLastBroadcastStatus:I

    .line 1201
    iput v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mLastBroadcastProgress:I

    .line 1202
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/update/SystemUpdateService;->sendStickyBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 1185
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v0, 0x1

    .line 353
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-lez v1, :cond_1

    .line 354
    sget-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    sget-object v0, Lcom/google/android/gsf/update/SystemUpdateService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 357
    :cond_0
    const/4 v0, 0x2

    .line 376
    :goto_0
    return v0

    .line 360
    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mPendingLock:Ljava/lang/Object;

    monitor-enter v1

    .line 361
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mRunning:Z

    if-nez v2, :cond_3

    .line 362
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mRunning:Z

    .line 363
    new-instance v2, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;-><init>(Lcom/google/android/gsf/update/SystemUpdateService;Lcom/google/android/gsf/update/SystemUpdateService$1;)V

    sget-object v3, Lcom/google/android/gsf/update/SystemUpdateService;->EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/content/Intent;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gsf/update/SystemUpdateService$UpdateTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 375
    :cond_2
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 370
    :cond_3
    :try_start_1
    iget-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mIntentPending:Landroid/content/Intent;

    if-nez v2, :cond_2

    .line 371
    :cond_4
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mStartPending:Z

    .line 372
    if-eqz p1, :cond_2

    iput-object p1, p0, Lcom/google/android/gsf/update/SystemUpdateService;->mIntentPending:Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
