.class public Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;
.super Landroid/app/DialogFragment;
.source "LiveWallpaperActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WallpaperDialog"
.end annotation


# instance fields
.field private mAdapter:Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter;

.field private mEmbedded:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method private generateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 200
    const v2, 0x7f030003

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 202
    .local v1, "layout":Landroid/view/View;
    new-instance v2, Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter;

    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->mAdapter:Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter;

    .line 203
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView;

    .line 205
    .local v0, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/BaseAdapter;>;"
    iget-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->mAdapter:Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 206
    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 207
    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    .line 208
    return-object v1
.end method

.method public static newInstance()Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;

    invoke-direct {v0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;-><init>()V

    .line 138
    .local v0, "dialog":Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->setCancelable(Z)V

    .line 139
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 145
    if-eqz p1, :cond_0

    const-string v0, "com.android.wallpaper.livepicker.LiveWallpaperActivity$WallpaperDialog.EMBEDDED_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "com.android.wallpaper.livepicker.LiveWallpaperActivity$WallpaperDialog.EMBEDDED_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->mEmbedded:Z

    .line 150
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->isInLayout()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->mEmbedded:Z

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 175
    .local v2, "contentInset":I
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-direct {p0, v3, v5}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->generateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 177
    .local v1, "view":Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 178
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f090008

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 179
    const v3, 0x7f090001

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move v3, v2

    move v4, v2

    move v5, v2

    .line 180
    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;IIII)Landroid/app/AlertDialog$Builder;

    .line 181
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->mEmbedded:Z

    if-eqz v0, :cond_0

    .line 193
    invoke-direct {p0, p1, p2}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->generateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 195
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 166
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 169
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->mAdapter:Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter;

    invoke-virtual {v2, p3}, Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter$LiveWallpaperInfo;

    .line 214
    .local v1, "wallpaperInfo":Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter$LiveWallpaperInfo;
    iget-object v0, v1, Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter$LiveWallpaperInfo;->intent:Landroid/content/Intent;

    .line 215
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, v1, Lcom/android/wallpaper/livepicker/LiveWallpaperListAdapter$LiveWallpaperInfo;->info:Landroid/app/WallpaperInfo;

    sput-object v2, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mWallpaperInfo:Landroid/app/WallpaperInfo;

    .line 216
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/16 v3, 0x64

    sget-object v4, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mWallpaperInfo:Landroid/app/WallpaperInfo;

    # getter for: Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mSetLockscreen:Z
    invoke-static {}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->access$000()Z

    move-result v5

    invoke-static {v2, v3, v0, v4, v5}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->showPreview(Landroid/app/Activity;ILandroid/content/Intent;Landroid/app/WallpaperInfo;Z)V

    .line 218
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    const-string v0, "com.android.wallpaper.livepicker.LiveWallpaperActivity$WallpaperDialog.EMBEDDED_KEY"

    iget-boolean v1, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->mEmbedded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 155
    return-void
.end method
