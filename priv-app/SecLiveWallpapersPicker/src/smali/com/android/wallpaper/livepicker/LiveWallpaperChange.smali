.class public Lcom/android/wallpaper/livepicker/LiveWallpaperChange;
.super Landroid/app/Activity;
.source "LiveWallpaperChange.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v0, -0x1

    .line 113
    if-ne p2, v0, :cond_0

    .line 114
    invoke-virtual {p0, v0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->setResult(I)V

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->finish()V

    .line 117
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    .line 45
    .local v10, "obj":Landroid/os/Parcelable;
    if-eqz v10, :cond_0

    instance-of v0, v10, Landroid/content/ComponentName;

    if-nez v0, :cond_1

    .line 46
    :cond_0
    const-string v0, "CHANGE_LIVE_WALLPAPER"

    const-string v1, "No LIVE_WALLPAPER_COMPONENT extra supplied"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->finish()V

    .line 89
    :goto_0
    return-void

    :cond_1
    move-object v6, v10

    .line 51
    check-cast v6, Landroid/content/ComponentName;

    .line 56
    .local v6, "comp":Landroid/content/ComponentName;
    new-instance v11, Landroid/content/Intent;

    const-string v0, "android.service.wallpaper.WallpaperService"

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .local v11, "queryIntent":Landroid/content/Intent;
    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v11, v1}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v9

    .line 60
    .local v9, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v9, :cond_3

    .line 61
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_3

    .line 62
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/pm/ResolveInfo;

    .line 63
    .local v12, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v0, v12, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    const/4 v4, 0x0

    .line 66
    .local v4, "info":Landroid/app/WallpaperInfo;
    :try_start_0
    new-instance v4, Landroid/app/WallpaperInfo;

    .end local v4    # "info":Landroid/app/WallpaperInfo;
    invoke-direct {v4, p0, v12}, Landroid/app/WallpaperInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 76
    .restart local v4    # "info":Landroid/app/WallpaperInfo;
    new-instance v3, Landroid/content/Intent;

    const-string v0, "android.service.wallpaper.WallpaperService"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v4}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Landroid/app/WallpaperInfo;->getServiceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SET_LOCKSCREEN_WALLPAPER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .local v5, "lock":Z
    move-object v0, p0

    move-object v1, p0

    .line 80
    invoke-virtual/range {v0 .. v5}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->showPreview(Landroid/app/Activity;ILandroid/content/Intent;Landroid/app/WallpaperInfo;Z)V

    goto :goto_0

    .line 67
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "info":Landroid/app/WallpaperInfo;
    .end local v5    # "lock":Z
    :catch_0
    move-exception v7

    .line 68
    .local v7, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v0, "CHANGE_LIVE_WALLPAPER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad wallpaper "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v12, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 69
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->finish()V

    goto :goto_0

    .line 71
    .end local v7    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v7

    .line 72
    .local v7, "e":Ljava/io/IOException;
    const-string v0, "CHANGE_LIVE_WALLPAPER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad wallpaper "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v12, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 73
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->finish()V

    goto/16 :goto_0

    .line 61
    .end local v7    # "e":Ljava/io/IOException;
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 87
    .end local v8    # "i":I
    .end local v12    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_3
    const-string v0, "CHANGE_LIVE_WALLPAPER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a live wallpaper: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperChange;->finish()V

    goto/16 :goto_0
.end method

.method showPreview(Landroid/app/Activity;ILandroid/content/Intent;Landroid/app/WallpaperInfo;Z)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "code"    # I
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "info"    # Landroid/app/WallpaperInfo;
    .param p5, "isLockScreen"    # Z

    .prologue
    .line 97
    if-nez p4, :cond_0

    .line 109
    :goto_0
    return-void

    .line 100
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    .local v0, "preview":Landroid/content/Intent;
    const-string v1, "android.live_wallpaper.intent"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 102
    if-eqz p4, :cond_1

    .line 103
    const-string v1, "android.live_wallpaper.settings"

    invoke-virtual {p4}, Landroid/app/WallpaperInfo;->getSettingsActivity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v1, "android.live_wallpaper.package"

    invoke-virtual {p4}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    :cond_1
    invoke-static {p5}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->setFlagLockWall(Z)V

    .line 108
    invoke-virtual {p1, v0, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
