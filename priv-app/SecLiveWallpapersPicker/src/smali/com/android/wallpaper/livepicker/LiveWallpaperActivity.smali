.class public Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;
.super Landroid/app/Activity;
.source "LiveWallpaperActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;
    }
.end annotation


# static fields
.field static final DEBUG:Z

.field private static mSetLockscreen:Z

.field public static mWallpaperInfo:Landroid/app/WallpaperInfo;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->DEBUG:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mContext:Landroid/content/Context;

    .line 127
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mSetLockscreen:Z

    return v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 113
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 115
    const/16 v1, 0x64

    if-ne p1, v1, :cond_1

    .line 116
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 117
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.wallpaper.livepicker.SET_LIVE_WALLPAPER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 118
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mWallpaperInfo:Landroid/app/WallpaperInfo;

    if-eqz v1, :cond_0

    .line 119
    const-string v1, "WALLPAPER_PACKAGE"

    sget-object v2, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mWallpaperInfo:Landroid/app/WallpaperInfo;

    invoke-virtual {v2}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    invoke-virtual {p0, v0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->finish()V

    .line 125
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    iput-object p0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mContext:Landroid/content/Context;

    .line 62
    const-string v5, "ro.csc.sales_code"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 63
    .local v4, "salesCode":Ljava/lang/String;
    const-string v5, "ATT"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 64
    iget-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mContext:Landroid/content/Context;

    const v6, 0x7f09000a

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 71
    .local v0, "SetLockscreenWallpaperIntent":Landroid/content/Intent;
    const-string v5, "SET_LOCKSCREEN_WALLPAPER"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->mSetLockscreen:Z

    .line 75
    const/high16 v5, 0x7f030000

    invoke-virtual {p0, v5}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->setContentView(I)V

    .line 77
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 78
    .local v1, "actionBar":Landroid/app/ActionBar;
    if-eqz v1, :cond_1

    .line 79
    invoke-virtual {v1, v8, v8}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 83
    invoke-virtual {v1, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 86
    :cond_1
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const/high16 v6, 0x7f0b0000

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    .line 87
    .local v3, "fragmentView":Landroid/app/Fragment;
    if-nez v3, :cond_2

    .line 92
    invoke-static {}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->newInstance()Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;

    move-result-object v2

    .line 93
    .local v2, "fragment":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "dialog"

    invoke-virtual {v2, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 95
    .end local v2    # "fragment":Landroid/app/DialogFragment;
    :cond_2
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 99
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 107
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 101
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 102
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->setResult(ILandroid/content/Intent;)V

    .line 103
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->finish()V

    .line 104
    const/4 v1, 0x1

    goto :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
