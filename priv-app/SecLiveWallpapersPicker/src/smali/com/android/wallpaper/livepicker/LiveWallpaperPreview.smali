.class public Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;
.super Landroid/app/Activity;
.source "LiveWallpaperPreview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;
    }
.end annotation


# static fields
.field static final DEBUG:Z

.field private static bFlagLockwall:Z

.field static isSetGlasslock:I


# instance fields
.field private mDialog:Landroid/app/Dialog;

.field private mPackageName:Ljava/lang/String;

.field private mSettings:Ljava/lang/String;

.field private mView:Landroid/view/View;

.field private mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

.field private mWallpaperIntent:Landroid/content/Intent;

.field private mWallpaperManager:Landroid/app/WallpaperManager;

.field private whenGlassLockTextView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 75
    const/4 v2, -0x1

    sput v2, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->isSetGlasslock:I

    .line 95
    sput-boolean v1, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->bFlagLockwall:Z

    .line 101
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->DEBUG:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 463
    return-void
.end method

.method static synthetic access$000(Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;)Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;
    .locals 1
    .param p0, "x0"    # Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;)Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;
    .locals 0
    .param p0, "x0"    # Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;
    .param p1, "x1"    # Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mView:Landroid/view/View;

    return-object v0
.end method

.method static setFlagLockWall(Z)V
    .locals 0
    .param p0, "lockwall"    # Z

    .prologue
    .line 132
    sput-boolean p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->bFlagLockwall:Z

    .line 133
    return-void
.end method

.method private showLoading()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 395
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 396
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030004

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 398
    .local v0, "content":Landroid/widget/TextView;
    new-instance v4, Landroid/app/Dialog;

    const v5, 0x1030008

    invoke-direct {v4, p0, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v4, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    .line 400
    iget-object v4, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 401
    .local v3, "window":Landroid/view/Window;
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 403
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 404
    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 405
    const/16 v4, 0x3e9

    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    .line 406
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 408
    iget-object v4, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v0, v5}, Landroid/app/Dialog;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 410
    iget-object v4, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    .line 411
    return-void
.end method

.method static showPreview(Landroid/app/Activity;ILandroid/content/Intent;Landroid/app/WallpaperInfo;Z)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "code"    # I
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "info"    # Landroid/app/WallpaperInfo;
    .param p4, "isLockScreen"    # Z

    .prologue
    .line 117
    if-nez p3, :cond_0

    .line 118
    const-string v1, "LiveWallpaperPreview"

    const-string v2, "Failure showing preview"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 129
    :goto_0
    return-void

    .line 121
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 122
    .local v0, "preview":Landroid/content/Intent;
    const-string v1, "android.live_wallpaper.intent"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 123
    if-eqz p3, :cond_1

    .line 124
    const-string v1, "android.live_wallpaper.settings"

    invoke-virtual {p3}, Landroid/app/WallpaperInfo;->getSettingsActivity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    const-string v1, "android.live_wallpaper.package"

    invoke-virtual {p3}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    :cond_1
    sput-boolean p4, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->bFlagLockwall:Z

    .line 128
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method


# virtual methods
.method public configureLiveWallpaper(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 327
    iget-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    .line 328
    iget-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    .line 330
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 331
    .local v1, "intent":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mPackageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mSettings:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 332
    const-string v2, "android.service.wallpaper.PREVIEW_MODE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 336
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    :goto_0
    return-void

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const v2, 0x7f09000b

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    if-eqz v0, :cond_0

    .line 429
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v7

    .line 431
    .local v7, "dup":Landroid/view/MotionEvent;
    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    invoke-interface {v0, v7}, Landroid/service/wallpaper/IWallpaperEngine;->dispatchPointer(Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 436
    .end local v7    # "dup":Landroid/view/MotionEvent;
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 437
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->onUserInteraction()V

    .line 439
    :cond_1
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    .line 440
    .local v8, "handled":Z
    if-nez v8, :cond_2

    .line 441
    invoke-virtual {p0, p1}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    .line 444
    :cond_2
    if-nez v8, :cond_3

    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    if-eqz v0, :cond_3

    .line 445
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    .line 447
    .local v6, "action":I
    const/4 v0, 0x1

    if-ne v6, v0, :cond_4

    .line 448
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    const-string v1, "android.wallpaper.tap"

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Landroid/service/wallpaper/IWallpaperEngine;->dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;)V

    .line 460
    .end local v6    # "action":I
    :cond_3
    :goto_1
    return v8

    .line 451
    .restart local v6    # "action":I
    :cond_4
    const/4 v0, 0x6

    if-ne v6, v0, :cond_3

    .line 452
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v9

    .line 453
    .local v9, "pointerIndex":I
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    const-string v1, "android.wallpaper.secondaryTap"

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Landroid/service/wallpaper/IWallpaperEngine;->dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 457
    .end local v9    # "pointerIndex":I
    :catch_0
    move-exception v0

    goto :goto_1

    .line 432
    .end local v6    # "action":I
    .end local v8    # "handled":Z
    .restart local v7    # "dup":Landroid/view/MotionEvent;
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    .prologue
    const/high16 v0, 0x7f040000

    .line 390
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 391
    invoke-virtual {p0, v0, v0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->overridePendingTransition(II)V

    .line 392
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 373
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    .line 375
    invoke-direct {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->showLoading()V

    .line 377
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mView:Landroid/view/View;

    new-instance v1, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$1;

    invoke-direct {v1, p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$1;-><init>(Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 386
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 137
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 139
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 140
    .local v1, "extras":Landroid/os/Bundle;
    const-string v2, "android.live_wallpaper.intent"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    iput-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperIntent:Landroid/content/Intent;

    .line 141
    iget-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperIntent:Landroid/content/Intent;

    if-nez v2, :cond_0

    .line 142
    invoke-virtual {p0, v4}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->setResult(I)V

    .line 143
    sput-boolean v4, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->bFlagLockwall:Z

    .line 144
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->finish()V

    .line 147
    :cond_0
    const-string v2, "android.live_wallpaper.settings"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mSettings:Ljava/lang/String;

    .line 148
    const-string v2, "android.live_wallpaper.package"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mPackageName:Ljava/lang/String;

    .line 149
    iget-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mSettings:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 150
    const v2, 0x7f030006

    invoke-virtual {p0, v2}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->setContentView(I)V

    .line 155
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    sput v2, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->isSetGlasslock:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :cond_1
    :goto_1
    const v2, 0x7f0b0007

    invoke-virtual {p0, v2}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mView:Landroid/view/View;

    .line 165
    const v2, 0x7f0b0006

    invoke-virtual {p0, v2}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->whenGlassLockTextView:Landroid/view/View;

    .line 167
    sget-boolean v2, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->bFlagLockwall:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 171
    iget-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->whenGlassLockTextView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 174
    :cond_2
    sget-boolean v2, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->bFlagLockwall:Z

    if-nez v2, :cond_3

    .line 175
    sget v2, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->isSetGlasslock:I

    if-nez v2, :cond_3

    .line 176
    iget-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->whenGlassLockTextView:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 182
    :cond_3
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperManager:Landroid/app/WallpaperManager;

    .line 184
    new-instance v2, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v3, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperIntent:Landroid/content/Intent;

    invoke-direct {v2, p0, v3}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;-><init>(Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;Landroid/content/Intent;)V

    iput-object v2, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    .line 190
    return-void

    .line 152
    :cond_4
    const v2, 0x7f030005

    invoke-virtual {p0, v2}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->setContentView(I)V

    goto :goto_0

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 158
    const-string v2, "zeroshuttle"

    const-string v3, "system.getInt Failed!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 415
    invoke-super {p0}, Landroid/app/Activity;->onDetachedFromWindow()V

    .line 417
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    invoke-virtual {v0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->disconnect()V

    .line 423
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    .line 424
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 361
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 362
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    if-eqz v0, :cond_1

    .line 364
    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/service/wallpaper/IWallpaperEngine;->setVisibility(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    :cond_1
    :goto_0
    return-void

    .line 365
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 345
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 346
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    if-eqz v0, :cond_0

    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v0, v0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/service/wallpaper/IWallpaperEngine;->setVisibility(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 349
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setLiveWallpaper(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 243
    iget-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    if-eqz v5, :cond_2

    .line 244
    iget-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v5, v5, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    if-eqz v5, :cond_1

    .line 245
    sget-boolean v5, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 246
    const-string v5, "LiveWallpaperPreview"

    const-string v6, "setLiveWallpaper: Destroy engine..."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    iget-object v5, v5, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    invoke-interface {v5}, Landroid/service/wallpaper/IWallpaperEngine;->destroy()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    invoke-virtual {v5}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;->disconnect()V

    .line 258
    :cond_2
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperConnection:Lcom/android/wallpaper/livepicker/LiveWallpaperPreview$WallpaperConnection;

    .line 260
    const-string v5, "enterprise_policy"

    invoke-virtual {p0, v5}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 262
    .local v2, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    if-eqz v2, :cond_3

    .line 263
    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v4

    .line 264
    .local v4, "rp":Landroid/app/enterprise/RestrictionPolicy;
    if-eqz v4, :cond_3

    invoke-virtual {v4, v8}, Landroid/app/enterprise/RestrictionPolicy;->isWallpaperChangeAllowed(Z)Z

    move-result v5

    if-nez v5, :cond_3

    .line 265
    invoke-virtual {p0, v9}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->setResult(I)V

    .line 266
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->finish()V

    .line 323
    .end local v4    # "rp":Landroid/app/enterprise/RestrictionPolicy;
    :goto_1
    return-void

    .line 250
    .end local v2    # "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    :catch_0
    move-exception v1

    .line 251
    .local v1, "e":Landroid/os/RemoteException;
    sget-boolean v5, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->DEBUG:Z

    if-eqz v5, :cond_1

    .line 252
    const-string v5, "LiveWallpaperPreview"

    const-string v6, "setLiveWallpaper: RemoteException in engine destroy"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 275
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v2    # "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v6, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mPackageName:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 277
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget v5, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v5, v5, 0x81

    if-nez v5, :cond_6

    .line 278
    const-string v5, "LiveWallpaperPreview"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setLiveWallpaper: installed live wallpaper:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const/4 v3, 0x0

    .line 287
    .local v3, "preloadedLive":I
    :goto_2
    sget-boolean v5, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->bFlagLockwall:Z

    if-ne v5, v8, :cond_4

    .line 288
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "lockscreen_wallpaper"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 295
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "lockscreen_wallpaper_transparent"

    invoke-static {v5, v6, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 299
    :cond_4
    iget-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {v5}, Landroid/app/WallpaperManager;->getIWallpaperManager()Landroid/app/IWallpaperManager;

    move-result-object v5

    iget-object v6, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/app/IWallpaperManager;->setWallpaperComponent(Landroid/content/ComponentName;)V

    .line 301
    iget-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperManager:Landroid/app/WallpaperManager;

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    .line 302
    iget-object v5, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v6

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V

    .line 303
    const/4 v5, -0x1

    invoke-virtual {p0, v5}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->setResult(I)V

    .line 308
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "android.wallpaper.settings_systemui_transparency"

    invoke-static {v5, v6, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .line 320
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "preloadedLive":I
    :cond_5
    :goto_3
    sput-boolean v10, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->bFlagLockwall:Z

    .line 321
    sput v9, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->isSetGlasslock:I

    .line 322
    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->finish()V

    goto/16 :goto_1

    .line 282
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_6
    :try_start_2
    const-string v5, "LiveWallpaperPreview"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setLiveWallpaper: preloaded live wallpaper:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 283
    const/4 v3, 0x1

    .restart local v3    # "preloadedLive":I
    goto :goto_2

    .line 312
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "preloadedLive":I
    :catch_1
    move-exception v1

    .line 313
    .local v1, "e":Ljava/lang/RuntimeException;
    sget-boolean v5, Lcom/android/wallpaper/livepicker/LiveWallpaperPreview;->DEBUG:Z

    if-eqz v5, :cond_5

    .line 314
    const-string v5, "LiveWallpaperPreview"

    const-string v6, "Failure setting wallpaper"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 316
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v1

    .line 318
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_3

    .line 310
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_3
    move-exception v5

    goto :goto_3
.end method
