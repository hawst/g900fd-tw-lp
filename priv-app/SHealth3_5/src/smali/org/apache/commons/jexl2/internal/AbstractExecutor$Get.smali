.class public abstract Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor;
.source "AbstractExecutor.java"

# interfaces
.implements Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/internal/AbstractExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Get"
.end annotation


# direct methods
.method protected constructor <init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V
    .locals 0
    .param p2, "theMethod"    # Ljava/lang/reflect/Method;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Method;",
            ")V"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, "theClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 192
    return-void
.end method


# virtual methods
.method public abstract execute(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation
.end method

.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;->execute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public tryExecute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 229
    sget-object v0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;->TRY_FAILED:Ljava/lang/Object;

    return-object v0
.end method

.method public final tryInvoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 201
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;->tryExecute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
