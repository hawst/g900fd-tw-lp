.class public Lorg/apache/commons/jexl2/parser/JexlParser;
.super Lorg/apache/commons/jexl2/parser/StringParser;
.source "JexlParser.java"


# instance fields
.field protected frame:Lorg/apache/commons/jexl2/JexlEngine$Scope;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/StringParser;-><init>()V

    return-void
.end method


# virtual methods
.method public final Identifier()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/jexl2/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/parser/JexlParser;->Identifier(Z)V

    .line 103
    return-void
.end method

.method public Identifier(Z)V
    .locals 0
    .param p1, "top"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/jexl2/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 99
    return-void
.end method

.method public checkVariable(Lorg/apache/commons/jexl2/parser/ASTIdentifier;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "identifier"    # Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .param p2, "image"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/JexlParser;->frame:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/JexlParser;->frame:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    invoke-virtual {v1, p2}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->getRegister(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 68
    .local v0, "register":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->setRegister(I)V

    .line 72
    .end local v0    # "register":Ljava/lang/Integer;
    :cond_0
    return-object p2
.end method

.method public declareVariable(Lorg/apache/commons/jexl2/parser/ASTVar;Ljava/lang/String;)V
    .locals 3
    .param p1, "identifier"    # Lorg/apache/commons/jexl2/parser/ASTVar;
    .param p2, "image"    # Ljava/lang/String;

    .prologue
    .line 84
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/JexlParser;->frame:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    if-nez v1, :cond_0

    .line 85
    new-instance v2, Lorg/apache/commons/jexl2/JexlEngine$Scope;

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/String;

    invoke-direct {v2, v1}, Lorg/apache/commons/jexl2/JexlEngine$Scope;-><init>([Ljava/lang/String;)V

    iput-object v2, p0, Lorg/apache/commons/jexl2/parser/JexlParser;->frame:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .line 87
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/JexlParser;->frame:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    invoke-virtual {v1, p2}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->declareVariable(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 88
    .local v0, "register":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTVar;->setRegister(I)V

    .line 89
    iput-object p2, p1, Lorg/apache/commons/jexl2/parser/ASTVar;->image:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public getFrame()Lorg/apache/commons/jexl2/JexlEngine$Scope;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/JexlParser;->frame:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    return-object v0
.end method

.method public getToken(I)Lorg/apache/commons/jexl2/parser/Token;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method jjtreeCloseNodeScope(Lorg/apache/commons/jexl2/parser/JexlNode;)V
    .locals 5
    .param p1, "n"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/jexl2/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 118
    instance-of v2, p1, Lorg/apache/commons/jexl2/parser/ASTAmbiguous;

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v2

    if-lez v2, :cond_1

    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "dbgInfo":Lorg/apache/commons/jexl2/DebugInfo;
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lorg/apache/commons/jexl2/parser/JexlParser;->getToken(I)Lorg/apache/commons/jexl2/parser/Token;

    move-result-object v1

    .line 121
    .local v1, "tok":Lorg/apache/commons/jexl2/parser/Token;
    if-eqz v1, :cond_0

    .line 122
    new-instance v0, Lorg/apache/commons/jexl2/DebugInfo;

    .end local v0    # "dbgInfo":Lorg/apache/commons/jexl2/DebugInfo;
    iget-object v2, v1, Lorg/apache/commons/jexl2/parser/Token;->image:Ljava/lang/String;

    iget v3, v1, Lorg/apache/commons/jexl2/parser/Token;->beginLine:I

    iget v4, v1, Lorg/apache/commons/jexl2/parser/Token;->beginColumn:I

    invoke-direct {v0, v2, v3, v4}, Lorg/apache/commons/jexl2/DebugInfo;-><init>(Ljava/lang/String;II)V

    .line 126
    .restart local v0    # "dbgInfo":Lorg/apache/commons/jexl2/DebugInfo;
    :goto_0
    new-instance v2, Lorg/apache/commons/jexl2/JexlException$Parsing;

    const-string v3, "Ambiguous statement, missing \';\' between expressions"

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4}, Lorg/apache/commons/jexl2/JexlException$Parsing;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/ParseException;)V

    throw v2

    .line 124
    :cond_0
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v0

    goto :goto_0

    .line 128
    .end local v0    # "dbgInfo":Lorg/apache/commons/jexl2/DebugInfo;
    .end local v1    # "tok":Lorg/apache/commons/jexl2/parser/Token;
    :cond_1
    return-void
.end method

.method jjtreeOpenNodeScope(Lorg/apache/commons/jexl2/parser/JexlNode;)V
    .locals 0
    .param p1, "n"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    .line 110
    return-void
.end method

.method public setFrame(Lorg/apache/commons/jexl2/JexlEngine$Scope;)V
    .locals 0
    .param p1, "theFrame"    # Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .prologue
    .line 43
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/JexlParser;->frame:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .line 44
    return-void
.end method
