.class Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;
.super Ljava/lang/Object;
.source "JexlThreadedArithmetic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/JexlThreadedArithmetic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Features"
.end annotation


# instance fields
.field private lenient:Ljava/lang/Boolean;

.field private mathContext:Ljava/math/MathContext;

.field private mathScale:Ljava/lang/Integer;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->lenient:Ljava/lang/Boolean;

    .line 35
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathContext:Ljava/math/MathContext;

    .line 37
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathScale:Ljava/lang/Integer;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    .prologue
    .line 29
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->lenient:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$002(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 29
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->lenient:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$100(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    .prologue
    .line 29
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathScale:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$102(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0, "x0"    # Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;
    .param p1, "x1"    # Ljava/lang/Integer;

    .prologue
    .line 29
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathScale:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$200(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;)Ljava/math/MathContext;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    .prologue
    .line 29
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathContext:Ljava/math/MathContext;

    return-object v0
.end method

.method static synthetic access$202(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;Ljava/math/MathContext;)Ljava/math/MathContext;
    .locals 0
    .param p0, "x0"    # Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;
    .param p1, "x1"    # Ljava/math/MathContext;

    .prologue
    .line 29
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathContext:Ljava/math/MathContext;

    return-object p1
.end method
