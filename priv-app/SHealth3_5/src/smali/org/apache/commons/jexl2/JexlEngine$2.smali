.class Lorg/apache/commons/jexl2/JexlEngine$2;
.super Ljava/util/LinkedHashMap;
.source "JexlEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/commons/jexl2/JexlEngine;->createCache(I)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field final synthetic this$0:Lorg/apache/commons/jexl2/JexlEngine;

.field final synthetic val$cacheSize:I


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;IFZI)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # F
    .param p4, "x2"    # Z

    .prologue
    .line 884
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlEngine$2;->this$0:Lorg/apache/commons/jexl2/JexlEngine;

    iput p5, p0, Lorg/apache/commons/jexl2/JexlEngine$2;->val$cacheSize:I

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 885
    .local p1, "eldest":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TK;TV;>;"
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlEngine$2;->size()I

    move-result v0

    iget v1, p0, Lorg/apache/commons/jexl2/JexlEngine$2;->val$cacheSize:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
