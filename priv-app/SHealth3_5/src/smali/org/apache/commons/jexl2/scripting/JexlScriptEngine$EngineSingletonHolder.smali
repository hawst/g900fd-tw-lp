.class Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$EngineSingletonHolder;
.super Ljava/lang/Object;
.source "JexlScriptEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EngineSingletonHolder"
.end annotation


# static fields
.field private static final DEFAULT_ENGINE:Lorg/apache/commons/jexl2/JexlEngine;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 282
    new-instance v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$EngineSingletonHolder$1;

    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->LOG:Lorg/apache/commons/logging/Log;
    invoke-static {}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->access$500()Lorg/apache/commons/logging/Log;

    move-result-object v1

    invoke-direct {v0, v2, v2, v2, v1}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$EngineSingletonHolder$1;-><init>(Lorg/apache/commons/jexl2/introspection/Uberspect;Lorg/apache/commons/jexl2/JexlArithmetic;Ljava/util/Map;Lorg/apache/commons/logging/Log;)V

    sput-object v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$EngineSingletonHolder;->DEFAULT_ENGINE:Lorg/apache/commons/jexl2/JexlEngine;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$600()Lorg/apache/commons/jexl2/JexlEngine;
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$EngineSingletonHolder;->DEFAULT_ENGINE:Lorg/apache/commons/jexl2/JexlEngine;

    return-object v0
.end method
