.class public final Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;
.super Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
.source "Sandbox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/introspection/Sandbox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BlackSet"
.end annotation


# instance fields
.field private names:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Lorg/apache/commons/jexl2/introspection/Sandbox$Names;-><init>()V

    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;->names:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;->names:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 238
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;->names:Ljava/util/Set;

    .line 240
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;->names:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;->names:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;->names:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .end local p1    # "name":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "name":Ljava/lang/String;
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method
