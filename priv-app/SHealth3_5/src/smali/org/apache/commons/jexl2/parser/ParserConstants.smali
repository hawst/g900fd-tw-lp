.class public interface abstract Lorg/apache/commons/jexl2/parser/ParserConstants;
.super Ljava/lang/Object;
.source "ParserConstants.java"


# static fields
.field public static final AND:I = 0x23

.field public static final COLON:I = 0x1e

.field public static final COMMA:I = 0x1f

.field public static final DEFAULT:I = 0x2

.field public static final DIGIT:I = 0x3a

.field public static final DOT:I = 0x20

.field public static final ELSE:I = 0xa

.field public static final ELVIS:I = 0x22

.field public static final EMPTY:I = 0x10

.field public static final EOF:I = 0x0

.field public static final FALSE:I = 0x14

.field public static final FLOAT_LITERAL:I = 0x3d

.field public static final FOR:I = 0xb

.field public static final FOREACH:I = 0xc

.field public static final FOR_EACH_IN:I = 0x1

.field public static final IDENTIFIER:I = 0x38

.field public static final IF:I = 0x9

.field public static final IN:I = 0x16

.field public static final INTEGER_LITERAL:I = 0x3c

.field public static final LBRACKET:I = 0x1b

.field public static final LCURLY:I = 0x19

.field public static final LETTER:I = 0x39

.field public static final LPAREN:I = 0x17

.field public static final NEW:I = 0xe

.field public static final NULL:I = 0x12

.field public static final OR:I = 0x24

.field public static final QMARK:I = 0x21

.field public static final RBRACKET:I = 0x1c

.field public static final RCURLY:I = 0x1a

.field public static final REGISTER:I = 0x3b

.field public static final REGISTERS:I = 0x0

.field public static final RETURN:I = 0x15

.field public static final RPAREN:I = 0x18

.field public static final SEMICOL:I = 0x1d

.field public static final SIZE:I = 0x11

.field public static final STRING_LITERAL:I = 0x3e

.field public static final TRUE:I = 0x13

.field public static final VAR:I = 0xf

.field public static final WHILE:I = 0xd

.field public static final and:I = 0x35

.field public static final assign:I = 0x2d

.field public static final div:I = 0x2f

.field public static final eq:I = 0x25

.field public static final ge:I = 0x2a

.field public static final gt:I = 0x29

.field public static final le:I = 0x2c

.field public static final lt:I = 0x2b

.field public static final minus:I = 0x32

.field public static final mod:I = 0x2e

.field public static final mult:I = 0x33

.field public static final ne:I = 0x26

.field public static final not:I = 0x30

.field public static final or:I = 0x36

.field public static final plus:I = 0x31

.field public static final req:I = 0x27

.field public static final rne:I = 0x28

.field public static final tilda:I = 0x34

.field public static final tokenImage:[Ljava/lang/String;

.field public static final xor:I = 0x37


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 130
    const/16 v0, 0x3f

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "<EOF>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "<token of kind 1>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "<token of kind 2>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "<token of kind 3>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "\" \""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\"\\t\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "\"\\n\""

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\"\\r\""

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\"\\f\""

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\"if\""

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\"else\""

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "\"for\""

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\"foreach\""

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\"while\""

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "\"new\""

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "\"var\""

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "\"empty\""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "\"size\""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "\"null\""

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "\"true\""

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "\"false\""

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "\"return\""

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "\"in\""

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "\"(\""

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "\")\""

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "\"{\""

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "\"}\""

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "\"[\""

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "\"]\""

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "\";\""

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "\":\""

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "\",\""

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "\".\""

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "\"?\""

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "\"?:\""

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "<AND>"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "<OR>"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "<eq>"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "<ne>"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "\"=~\""

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "\"!~\""

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "<gt>"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "<ge>"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "<lt>"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "<le>"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "\"=\""

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "<mod>"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "<div>"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "<not>"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "\"+\""

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "\"-\""

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "\"*\""

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "\"~\""

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "\"&\""

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "\"|\""

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "\"^\""

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "<IDENTIFIER>"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "<LETTER>"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "<DIGIT>"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "<REGISTER>"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "<INTEGER_LITERAL>"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "<FLOAT_LITERAL>"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "<STRING_LITERAL>"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserConstants;->tokenImage:[Ljava/lang/String;

    return-void
.end method
