.class public Lorg/apache/commons/jexl2/introspection/SandboxUberspectImpl;
.super Lorg/apache/commons/jexl2/introspection/UberspectImpl;
.source "SandboxUberspectImpl.java"


# instance fields
.field protected final sandbox:Lorg/apache/commons/jexl2/introspection/Sandbox;


# direct methods
.method public constructor <init>(Lorg/apache/commons/logging/Log;Lorg/apache/commons/jexl2/introspection/Sandbox;)V
    .locals 2
    .param p1, "runtimeLogger"    # Lorg/apache/commons/logging/Log;
    .param p2, "theSandbox"    # Lorg/apache/commons/jexl2/introspection/Sandbox;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;-><init>(Lorg/apache/commons/logging/Log;)V

    .line 37
    if-nez p2, :cond_0

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "sandbox can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object p2, p0, Lorg/apache/commons/jexl2/introspection/SandboxUberspectImpl;->sandbox:Lorg/apache/commons/jexl2/introspection/Sandbox;

    .line 41
    return-void
.end method


# virtual methods
.method public getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .locals 5
    .param p1, "ctorHandle"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    const/4 v2, 0x0

    .line 57
    instance-of v3, p1, Ljava/lang/Class;

    if-eqz v3, :cond_1

    move-object v1, p1

    .line 58
    check-cast v1, Ljava/lang/Class;

    .line 59
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 65
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local v0, "className":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/introspection/SandboxUberspectImpl;->sandbox:Lorg/apache/commons/jexl2/introspection/Sandbox;

    const-string v4, ""

    invoke-virtual {v3, v0, v4}, Lorg/apache/commons/jexl2/introspection/Sandbox;->execute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 66
    invoke-super {p0, v0, p2, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v2

    .line 68
    .end local v0    # "className":Ljava/lang/String;
    :cond_0
    return-object v2

    .line 60
    :cond_1
    if-eqz p1, :cond_0

    .line 61
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "className":Ljava/lang/String;
    goto :goto_0
.end method

.method public getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;
    .param p4, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 76
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 77
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/SandboxUberspectImpl;->sandbox:Lorg/apache/commons/jexl2/introspection/Sandbox;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/commons/jexl2/introspection/Sandbox;->execute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "actual":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0, p1, v0, p3}, Lorg/apache/commons/jexl2/introspection/SandboxUberspectImpl;->getMethodExecutor(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;

    move-result-object v1

    .line 82
    .end local v0    # "actual":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 90
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 91
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/SandboxUberspectImpl;->sandbox:Lorg/apache/commons/jexl2/introspection/Sandbox;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/jexl2/introspection/Sandbox;->read(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "actual":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 93
    invoke-super {p0, p1, v0, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    move-result-object v1

    .line 96
    .end local v0    # "actual":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPropertySet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;
    .param p3, "arg"    # Ljava/lang/Object;
    .param p4, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 104
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 105
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/SandboxUberspectImpl;->sandbox:Lorg/apache/commons/jexl2/introspection/Sandbox;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/jexl2/introspection/Sandbox;->write(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "actual":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 107
    invoke-super {p0, p1, v0, p3, p4}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getPropertySet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertySet;

    move-result-object v1

    .line 110
    .end local v0    # "actual":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLoader(Ljava/lang/ClassLoader;)V
    .locals 1
    .param p1, "cloader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 48
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/introspection/SandboxUberspectImpl;->base()Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->setLoader(Ljava/lang/ClassLoader;)V

    .line 49
    return-void
.end method
