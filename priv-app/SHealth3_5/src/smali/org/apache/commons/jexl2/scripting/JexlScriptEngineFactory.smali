.class public Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;
.super Ljava/lang/Object;
.source "JexlScriptEngineFactory.java"

# interfaces
.implements Ljavax/script/ScriptEngineFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEngineName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "JEXL Engine"

    return-object v0
.end method

.method public getEngineVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "2.0"

    return-object v0
.end method

.method public getExtensions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "jexl"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "jexl2"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLanguageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const-string v0, "JEXL"

    return-object v0
.end method

.method public getLanguageVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string v0, "2.0"

    return-object v0
.end method

.method public getMethodCallSyntax(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "obj"    # Ljava/lang/String;
    .param p2, "m"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 67
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .local v5, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    const/16 v6, 0x2e

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    const/16 v6, 0x28

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 72
    const/4 v4, 0x0

    .line 73
    .local v4, "needComma":Z
    move-object v1, p3

    .local v1, "arr$":[Ljava/lang/String;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 74
    .local v0, "arg":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 75
    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    :cond_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const/4 v4, 0x1

    .line 73
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    .end local v0    # "arg":Ljava/lang/String;
    :cond_1
    const/16 v6, 0x29

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public getMimeTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "application/x-jexl"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "application/x-jexl2"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNames()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "JEXL"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Jexl"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "jexl"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "JEXL2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Jexl2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "jexl2"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getOutputStatement(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "toDisplay"    # Ljava/lang/String;

    .prologue
    .line 101
    if-nez p1, :cond_0

    .line 102
    const-string v0, "JEXL.out.print(null)"

    .line 104
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JEXL.out.print("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-static {p1, v1}, Lorg/apache/commons/jexl2/parser/StringParser;->escapeString(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getParameter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 110
    const-string v1, "javax.script.engine"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;->getEngineName()Ljava/lang/String;

    move-result-object v0

    .line 129
    :cond_0
    :goto_0
    return-object v0

    .line 112
    :cond_1
    const-string v1, "javax.script.engine_version"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 113
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;->getEngineVersion()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_2
    const-string v1, "javax.script.name"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 115
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;->getNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 116
    :cond_3
    const-string v1, "javax.script.language"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 117
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;->getLanguageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 118
    :cond_4
    const-string v1, "javax.script.language_version"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 119
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;->getLanguageVersion()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_5
    const-string v1, "THREADING"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public getProgram([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "statements"    # [Ljava/lang/String;

    .prologue
    .line 134
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    .local v3, "sb":Ljava/lang/StringBuilder;
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 136
    .local v4, "statement":Ljava/lang/String;
    invoke-static {v4}, Lorg/apache/commons/jexl2/JexlEngine;->cleanExpression(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 138
    const/16 v5, 0x3b

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 135
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    .end local v4    # "statement":Ljava/lang/String;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public getScriptEngine()Ljavax/script/ScriptEngine;
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    invoke-direct {v0, p0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;-><init>(Ljavax/script/ScriptEngineFactory;)V

    .line 147
    .local v0, "engine":Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
    return-object v0
.end method
