.class Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;
.super Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImmediateExpression"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 0
    .param p2, "expr"    # Ljava/lang/CharSequence;
    .param p3, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p4, "source"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 532
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    .line 533
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 534
    return-void
.end method


# virtual methods
.method getType()Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
    .locals 1

    .prologue
    .line 539
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->IMMEDIATE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    return-object v0
.end method

.method protected prepare(Lorg/apache/commons/jexl2/Interpreter;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 4
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    .line 546
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;->evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;

    move-result-object v0

    .line 547
    .local v0, "value":Ljava/lang/Object;
    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;

    iget-object v2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;->source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    invoke-direct {v1, v2, v0, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/Object;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
