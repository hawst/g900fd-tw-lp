.class public Lorg/apache/commons/jexl2/ObjectContext;
.super Ljava/lang/Object;
.source "ObjectContext.java"

# interfaces
.implements Lorg/apache/commons/jexl2/JexlContext;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/commons/jexl2/JexlContext;"
    }
.end annotation


# instance fields
.field private final jexl:Lorg/apache/commons/jexl2/JexlEngine;

.field private final object:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;Ljava/lang/Object;)V
    .locals 0
    .param p1, "engine"    # Lorg/apache/commons/jexl2/JexlEngine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/JexlEngine;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lorg/apache/commons/jexl2/ObjectContext;, "Lorg/apache/commons/jexl2/ObjectContext<TT;>;"
    .local p2, "wrapped":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/commons/jexl2/ObjectContext;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    .line 37
    iput-object p2, p0, Lorg/apache/commons/jexl2/ObjectContext;->object:Ljava/lang/Object;

    .line 38
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    .local p0, "this":Lorg/apache/commons/jexl2/ObjectContext;, "Lorg/apache/commons/jexl2/ObjectContext<TT;>;"
    iget-object v0, p0, Lorg/apache/commons/jexl2/ObjectContext;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    iget-object v1, p0, Lorg/apache/commons/jexl2/ObjectContext;->object:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lorg/apache/commons/jexl2/JexlEngine;->getProperty(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public has(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 52
    .local p0, "this":Lorg/apache/commons/jexl2/ObjectContext;, "Lorg/apache/commons/jexl2/ObjectContext<TT;>;"
    iget-object v0, p0, Lorg/apache/commons/jexl2/ObjectContext;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine;->getUberspect()Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/commons/jexl2/ObjectContext;->object:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 47
    .local p0, "this":Lorg/apache/commons/jexl2/ObjectContext;, "Lorg/apache/commons/jexl2/ObjectContext<TT;>;"
    iget-object v0, p0, Lorg/apache/commons/jexl2/ObjectContext;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    iget-object v1, p0, Lorg/apache/commons/jexl2/ObjectContext;->object:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lorg/apache/commons/jexl2/JexlEngine;->setProperty(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    return-void
.end method
