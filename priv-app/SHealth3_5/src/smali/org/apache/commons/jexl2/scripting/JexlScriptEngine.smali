.class public Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
.super Ljavax/script/AbstractScriptEngine;
.source "JexlScriptEngine.java"

# interfaces
.implements Ljavax/script/Compilable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$1;,
        Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;,
        Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;,
        Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$EngineSingletonHolder;,
        Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$FactorySingletonHolder;,
        Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;
    }
.end annotation


# static fields
.field private static final CACHE_SIZE:I = 0x200

.field public static final CONTEXT_KEY:Ljava/lang/String; = "context"

.field public static final JEXL_OBJECT_KEY:Ljava/lang/String; = "JEXL"

.field private static final LOG:Lorg/apache/commons/logging/Log;


# instance fields
.field private final jexlEngine:Lorg/apache/commons/jexl2/JexlEngine;

.field private final jexlObject:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;

.field private final parentFactory:Ljavax/script/ScriptEngineFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->LOG:Lorg/apache/commons/logging/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$FactorySingletonHolder;->DEFAULT_FACTORY:Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;
    invoke-static {}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$FactorySingletonHolder;->access$000()Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;-><init>(Ljavax/script/ScriptEngineFactory;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Ljavax/script/ScriptEngineFactory;)V
    .locals 2
    .param p1, "factory"    # Ljavax/script/ScriptEngineFactory;

    .prologue
    .line 182
    invoke-direct {p0}, Ljavax/script/AbstractScriptEngine;-><init>()V

    .line 183
    if-nez p1, :cond_0

    .line 184
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "ScriptEngineFactory must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->parentFactory:Ljavax/script/ScriptEngineFactory;

    .line 187
    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$EngineSingletonHolder;->DEFAULT_ENGINE:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$EngineSingletonHolder;->access$600()Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->jexlEngine:Lorg/apache/commons/jexl2/JexlEngine;

    .line 188
    new-instance v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;

    invoke-direct {v0, p0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;-><init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)V

    iput-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->jexlObject:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;

    .line 189
    return-void
.end method

.method static synthetic access$100(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Lorg/apache/commons/jexl2/JexlEngine;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->jexlEngine:Lorg/apache/commons/jexl2/JexlEngine;

    return-object v0
.end method

.method static synthetic access$200(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Ljavax/script/ScriptContext;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->context:Ljavax/script/ScriptContext;

    return-object v0
.end method

.method static synthetic access$300(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Ljavax/script/ScriptContext;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->context:Ljavax/script/ScriptContext;

    return-object v0
.end method

.method static synthetic access$400(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Ljavax/script/ScriptContext;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->context:Ljavax/script/ScriptContext;

    return-object v0
.end method

.method static synthetic access$500()Lorg/apache/commons/logging/Log;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->LOG:Lorg/apache/commons/logging/Log;

    return-object v0
.end method

.method static synthetic access$900(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->jexlObject:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;

    return-object v0
.end method

.method private readerToString(Ljava/io/Reader;)Ljava/lang/String;
    .locals 2
    .param p1, "script"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/script/ScriptException;
        }
    .end annotation

    .prologue
    .line 258
    :try_start_0
    invoke-static {p1}, Lorg/apache/commons/jexl2/JexlEngine;->readerToString(Ljava/io/Reader;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 259
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljavax/script/ScriptException;

    invoke-direct {v1, v0}, Ljavax/script/ScriptException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method public compile(Ljava/io/Reader;)Ljavax/script/CompiledScript;
    .locals 2
    .param p1, "script"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/script/ScriptException;
        }
    .end annotation

    .prologue
    .line 244
    if-nez p1, :cond_0

    .line 245
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "script must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->readerToString(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->compile(Ljava/lang/String;)Ljavax/script/CompiledScript;

    move-result-object v0

    return-object v0
.end method

.method public compile(Ljava/lang/String;)Ljavax/script/CompiledScript;
    .locals 4
    .param p1, "script"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/script/ScriptException;
        }
    .end annotation

    .prologue
    .line 230
    if-nez p1, :cond_0

    .line 231
    new-instance v2, Ljava/lang/NullPointerException;

    const-string/jumbo v3, "script must be non-null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 234
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->jexlEngine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v2, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;

    move-result-object v1

    .line 235
    .local v1, "jexlScript":Lorg/apache/commons/jexl2/Script;
    new-instance v2, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v1, v3}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;-><init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Lorg/apache/commons/jexl2/Script;Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$1;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 236
    .end local v1    # "jexlScript":Lorg/apache/commons/jexl2/Script;
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljavax/script/ScriptException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/script/ScriptException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public createBindings()Ljavax/script/Bindings;
    .locals 1

    .prologue
    .line 193
    new-instance v0, Ljavax/script/SimpleBindings;

    invoke-direct {v0}, Ljavax/script/SimpleBindings;-><init>()V

    return-object v0
.end method

.method public eval(Ljava/io/Reader;Ljavax/script/ScriptContext;)Ljava/lang/Object;
    .locals 2
    .param p1, "reader"    # Ljava/io/Reader;
    .param p2, "context"    # Ljavax/script/ScriptContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/script/ScriptException;
        }
    .end annotation

    .prologue
    .line 199
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 200
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "script and context must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->readerToString(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->eval(Ljava/lang/String;Ljavax/script/ScriptContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public eval(Ljava/lang/String;Ljavax/script/ScriptContext;)Ljava/lang/Object;
    .locals 5
    .param p1, "script"    # Ljava/lang/String;
    .param p2, "context"    # Ljavax/script/ScriptContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/script/ScriptException;
        }
    .end annotation

    .prologue
    .line 208
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 209
    :cond_0
    new-instance v3, Ljava/lang/NullPointerException;

    const-string/jumbo v4, "script and context must be non-null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 212
    :cond_1
    const-string v3, "context"

    const/16 v4, 0x64

    invoke-interface {p2, v3, p2, v4}, Ljavax/script/ScriptContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 214
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->jexlEngine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v3, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;

    move-result-object v2

    .line 215
    .local v2, "jexlScript":Lorg/apache/commons/jexl2/Script;
    new-instance v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;

    const/4 v3, 0x0

    invoke-direct {v0, p0, p2, v3}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;-><init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Ljavax/script/ScriptContext;Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$1;)V

    .line 216
    .local v0, "ctxt":Lorg/apache/commons/jexl2/JexlContext;
    invoke-interface {v2, v0}, Lorg/apache/commons/jexl2/Script;->execute(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 217
    .end local v0    # "ctxt":Lorg/apache/commons/jexl2/JexlContext;
    .end local v2    # "jexlScript":Lorg/apache/commons/jexl2/Script;
    :catch_0
    move-exception v1

    .line 218
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljavax/script/ScriptException;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljavax/script/ScriptException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public getFactory()Ljavax/script/ScriptEngineFactory;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->parentFactory:Ljavax/script/ScriptEngineFactory;

    return-object v0
.end method
