.class public Lorg/apache/commons/jexl2/JexlEngine;
.super Ljava/lang/Object;
.source "JexlEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/JexlEngine$Frame;,
        Lorg/apache/commons/jexl2/JexlEngine$Scope;,
        Lorg/apache/commons/jexl2/JexlEngine$SoftCache;,
        Lorg/apache/commons/jexl2/JexlEngine$UberspectHolder;
    }
.end annotation


# static fields
.field public static final EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;

.field private static final LOAD_FACTOR:F = 0.75f


# instance fields
.field protected final arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

.field protected cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/jexl2/JexlEngine$SoftCache",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/commons/jexl2/parser/ASTJexlScript;",
            ">;"
        }
    .end annotation
.end field

.field protected volatile debug:Z

.field protected functions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected final logger:Lorg/apache/commons/logging/Log;

.field protected final parser:Lorg/apache/commons/jexl2/parser/Parser;

.field protected volatile silent:Z

.field protected final uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$1;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/JexlEngine$1;-><init>()V

    sput-object v0, Lorg/apache/commons/jexl2/JexlEngine;->EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 183
    invoke-direct {p0, v0, v0, v0, v0}, Lorg/apache/commons/jexl2/JexlEngine;-><init>(Lorg/apache/commons/jexl2/introspection/Uberspect;Lorg/apache/commons/jexl2/JexlArithmetic;Ljava/util/Map;Lorg/apache/commons/logging/Log;)V

    .line 184
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/introspection/Uberspect;Lorg/apache/commons/jexl2/JexlArithmetic;Ljava/util/Map;Lorg/apache/commons/logging/Log;)V
    .locals 4
    .param p1, "anUberspect"    # Lorg/apache/commons/jexl2/introspection/Uberspect;
    .param p2, "anArithmetic"    # Lorg/apache/commons/jexl2/JexlArithmetic;
    .param p4, "log"    # Lorg/apache/commons/logging/Log;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/introspection/Uberspect;",
            "Lorg/apache/commons/jexl2/JexlArithmetic;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/apache/commons/logging/Log;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "theFunctions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v3, 0x1

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    new-instance v0, Lorg/apache/commons/jexl2/parser/Parser;

    new-instance v1, Ljava/io/StringReader;

    const-string v2, ";"

    invoke-direct {v1, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/commons/jexl2/parser/Parser;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    .line 163
    iput-boolean v3, p0, Lorg/apache/commons/jexl2/JexlEngine;->debug:Z

    .line 168
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    .line 195
    if-nez p1, :cond_0

    invoke-static {p4}, Lorg/apache/commons/jexl2/JexlEngine;->getUberspect(Lorg/apache/commons/logging/Log;)Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-result-object p1

    .end local p1    # "anUberspect":Lorg/apache/commons/jexl2/introspection/Uberspect;
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    .line 196
    if-nez p4, :cond_1

    .line 197
    const-class v0, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    move-result-object p4

    .line 199
    :cond_1
    iput-object p4, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    .line 200
    if-nez p2, :cond_2

    new-instance p2, Lorg/apache/commons/jexl2/JexlArithmetic;

    .end local p2    # "anArithmetic":Lorg/apache/commons/jexl2/JexlArithmetic;
    invoke-direct {p2, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;-><init>(Z)V

    :cond_2
    iput-object p2, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    .line 201
    if-eqz p3, :cond_3

    .line 202
    iput-object p3, p0, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    .line 204
    :cond_3
    return-void
.end method

.method public static cleanExpression(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/CharSequence;

    .prologue
    const/16 v3, 0x20

    .line 1320
    if-eqz p0, :cond_3

    .line 1321
    const/4 v1, 0x0

    .line 1322
    .local v1, "start":I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 1323
    .local v0, "end":I
    if-lez v0, :cond_2

    .line 1325
    :goto_0
    if-ge v1, v0, :cond_0

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_0

    .line 1326
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1329
    :cond_0
    :goto_1
    if-lez v0, :cond_1

    add-int/lit8 v2, v0, -0x1

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_1

    .line 1330
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1332
    :cond_1
    invoke-interface {p0, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1336
    .end local v0    # "end":I
    .end local v1    # "start":I
    :goto_2
    return-object v2

    .line 1334
    .restart local v0    # "end":I
    .restart local v1    # "start":I
    :cond_2
    const-string v2, ""

    goto :goto_2

    .line 1336
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static getUberspect(Lorg/apache/commons/logging/Log;)Lorg/apache/commons/jexl2/introspection/Uberspect;
    .locals 1
    .param p0, "logger"    # Lorg/apache/commons/logging/Log;

    .prologue
    .line 216
    if-eqz p0, :cond_0

    const-class v0, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    :cond_0
    # getter for: Lorg/apache/commons/jexl2/JexlEngine$UberspectHolder;->UBERSPECT:Lorg/apache/commons/jexl2/introspection/Uberspect;
    invoke-static {}, Lorg/apache/commons/jexl2/JexlEngine$UberspectHolder;->access$000()Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-result-object v0

    .line 219
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;

    invoke-direct {v0, p0}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;-><init>(Lorg/apache/commons/logging/Log;)V

    goto :goto_0
.end method

.method public static readerToString(Ljava/io/Reader;)Ljava/lang/String;
    .locals 5
    .param p0, "scriptReader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1349
    .local v0, "buffer":Ljava/lang/StringBuilder;
    instance-of v3, p0, Ljava/io/BufferedReader;

    if-eqz v3, :cond_0

    move-object v2, p0

    .line 1350
    check-cast v2, Ljava/io/BufferedReader;

    .line 1356
    .local v2, "reader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .local v1, "line":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1357
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1361
    .end local v1    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v3

    .line 1362
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1365
    :goto_1
    throw v3

    .line 1352
    .end local v2    # "reader":Ljava/io/BufferedReader;
    :cond_0
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, p0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 1359
    .restart local v1    # "line":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 1362
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1365
    :goto_2
    return-object v3

    .line 1363
    :catch_0
    move-exception v4

    goto :goto_2

    .end local v1    # "line":Ljava/lang/String;
    :catch_1
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method public clearCache()V
    .locals 2

    .prologue
    .line 895
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    monitor-enter v1

    .line 896
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->clear()V

    .line 897
    monitor-exit v1

    .line 898
    return-void

    .line 897
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected createCache(I)Ljava/util/Map;
    .locals 6
    .param p1, "cacheSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I)",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 879
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$2;

    const/high16 v3, 0x3f400000    # 0.75f

    const/4 v4, 0x1

    move-object v1, p0

    move v2, p1

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/jexl2/JexlEngine$2;-><init>(Lorg/apache/commons/jexl2/JexlEngine;IFZI)V

    return-object v0
.end method

.method public createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;
    .locals 1
    .param p1, "expression"    # Ljava/lang/String;

    .prologue
    .line 419
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v0

    return-object v0
.end method

.method public createExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/Expression;
    .locals 4
    .param p1, "expression"    # Ljava/lang/String;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 435
    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v0

    .line 436
    .local v0, "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetNumChildren()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 437
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The JEXL Expression created will be a reference to the first expression from the supplied script: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    .line 440
    :cond_0
    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v1

    return-object v1
.end method

.method protected createExpression(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;
    .locals 1
    .param p1, "tree"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 405
    new-instance v0, Lorg/apache/commons/jexl2/ExpressionImpl;

    invoke-direct {v0, p0, p2, p1}, Lorg/apache/commons/jexl2/ExpressionImpl;-><init>(Lorg/apache/commons/jexl2/JexlEngine;Ljava/lang/String;Lorg/apache/commons/jexl2/parser/ASTJexlScript;)V

    return-object v0
.end method

.method protected createInfo(Ljava/lang/String;II)Lorg/apache/commons/jexl2/JexlInfo;
    .locals 1
    .param p1, "fn"    # Ljava/lang/String;
    .param p2, "l"    # I
    .param p3, "c"    # I

    .prologue
    .line 1276
    new-instance v0, Lorg/apache/commons/jexl2/DebugInfo;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/commons/jexl2/DebugInfo;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method protected createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;
    .locals 2
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 782
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlEngine;->isStrict()Z

    move-result v0

    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;ZZ)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v0

    return-object v0
.end method

.method protected createInterpreter(Lorg/apache/commons/jexl2/JexlContext;ZZ)Lorg/apache/commons/jexl2/Interpreter;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p2, "strictFlag"    # Z
    .param p3, "silentFlag"    # Z

    .prologue
    .line 794
    new-instance v0, Lorg/apache/commons/jexl2/Interpreter;

    if-nez p1, :cond_0

    sget-object p1, Lorg/apache/commons/jexl2/JexlEngine;->EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;

    .end local p1    # "context":Lorg/apache/commons/jexl2/JexlContext;
    :cond_0
    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/commons/jexl2/Interpreter;-><init>(Lorg/apache/commons/jexl2/JexlEngine;Lorg/apache/commons/jexl2/JexlContext;ZZ)V

    return-object v0
.end method

.method public createScript(Ljava/io/File;)Lorg/apache/commons/jexl2/Script;
    .locals 5
    .param p1, "scriptFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 532
    if-nez p1, :cond_0

    .line 533
    new-instance v2, Ljava/lang/NullPointerException;

    const-string/jumbo v3, "scriptFile is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 535
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v2

    if-nez v2, :cond_1

    .line 536
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t read scriptFile ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 538
    :cond_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 539
    .local v1, "reader":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 540
    .local v0, "info":Lorg/apache/commons/jexl2/JexlInfo;
    iget-boolean v2, p0, Lorg/apache/commons/jexl2/JexlEngine;->debug:Z

    if-eqz v2, :cond_2

    .line 541
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v3, v3}, Lorg/apache/commons/jexl2/JexlEngine;->createInfo(Ljava/lang/String;II)Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v0

    .line 543
    :cond_2
    invoke-static {v1}, Lorg/apache/commons/jexl2/JexlEngine;->readerToString(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v0, v3}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;[Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;

    move-result-object v2

    return-object v2
.end method

.method public createScript(Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;
    .locals 1
    .param p1, "scriptText"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 452
    invoke-virtual {p0, p1, v0, v0}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;[Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;

    move-result-object v0

    return-object v0
.end method

.method public createScript(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/Script;
    .locals 3
    .param p1, "scriptText"    # Ljava/lang/String;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 467
    if-nez p1, :cond_0

    .line 468
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "scriptText is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 471
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v0

    .line 472
    .local v0, "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;

    move-result-object v1

    return-object v1
.end method

.method public createScript(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;[Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;
    .locals 3
    .param p1, "scriptText"    # Ljava/lang/String;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;
    .param p3, "names"    # [Ljava/lang/String;

    .prologue
    .line 502
    if-nez p1, :cond_0

    .line 503
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "scriptText is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 506
    :cond_0
    new-instance v1, Lorg/apache/commons/jexl2/JexlEngine$Scope;

    invoke-direct {v1, p3}, Lorg/apache/commons/jexl2/JexlEngine$Scope;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v0

    .line 507
    .local v0, "tree":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;

    move-result-object v1

    return-object v1
.end method

.method public varargs createScript(Ljava/lang/String;[Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;
    .locals 1
    .param p1, "scriptText"    # Ljava/lang/String;
    .param p2, "names"    # [Ljava/lang/String;

    .prologue
    .line 485
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;[Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;

    move-result-object v0

    return-object v0
.end method

.method public createScript(Ljava/net/URL;)Lorg/apache/commons/jexl2/Script;
    .locals 6
    .param p1, "scriptUrl"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 558
    if-nez p1, :cond_0

    .line 559
    new-instance v3, Ljava/lang/NullPointerException;

    const-string/jumbo v4, "scriptUrl is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 561
    :cond_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 563
    .local v0, "connection":Ljava/net/URLConnection;
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 565
    .local v2, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 566
    .local v1, "info":Lorg/apache/commons/jexl2/JexlInfo;
    iget-boolean v3, p0, Lorg/apache/commons/jexl2/JexlEngine;->debug:Z

    if-eqz v3, :cond_1

    .line 567
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v5, v5}, Lorg/apache/commons/jexl2/JexlEngine;->createInfo(Ljava/lang/String;II)Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v1

    .line 569
    :cond_1
    invoke-static {v2}, Lorg/apache/commons/jexl2/JexlEngine;->readerToString(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v1, v4}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;[Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;

    move-result-object v3

    return-object v3
.end method

.method protected createScript(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/String;)Lorg/apache/commons/jexl2/Script;
    .locals 1
    .param p1, "tree"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 517
    new-instance v0, Lorg/apache/commons/jexl2/ExpressionImpl;

    invoke-direct {v0, p0, p2, p1}, Lorg/apache/commons/jexl2/ExpressionImpl;-><init>(Lorg/apache/commons/jexl2/JexlEngine;Ljava/lang/String;Lorg/apache/commons/jexl2/parser/ASTJexlScript;)V

    return-object v0
.end method

.method protected debugInfo()Lorg/apache/commons/jexl2/JexlInfo;
    .locals 10

    .prologue
    .line 1286
    const/4 v2, 0x0

    .line 1287
    .local v2, "info":Lorg/apache/commons/jexl2/DebugInfo;
    iget-boolean v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->debug:Z

    if-eqz v7, :cond_3

    .line 1288
    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    .line 1289
    .local v6, "xinfo":Ljava/lang/Throwable;
    invoke-virtual {v6}, Ljava/lang/Throwable;->fillInStackTrace()Ljava/lang/Throwable;

    .line 1290
    invoke-virtual {v6}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    .line 1291
    .local v5, "stack":[Ljava/lang/StackTraceElement;
    const/4 v4, 0x0

    .line 1292
    .local v4, "se":Ljava/lang/StackTraceElement;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 1293
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v3, 0x1

    .local v3, "s":I
    :goto_0
    array-length v7, v5

    if-ge v3, v7, :cond_2

    .line 1294
    aget-object v4, v5, v3

    .line 1295
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 1296
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1298
    const-class v7, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1299
    const-class v1, Lorg/apache/commons/jexl2/JexlEngine;

    .line 1293
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    const/4 v4, 0x0

    goto :goto_0

    .line 1300
    :cond_1
    const-class v7, Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1301
    const-class v1, Lorg/apache/commons/jexl2/UnifiedJEXL;

    goto :goto_1

    .line 1307
    .end local v0    # "className":Ljava/lang/String;
    :cond_2
    if-eqz v4, :cond_3

    .line 1308
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {p0, v7, v8, v9}, Lorg/apache/commons/jexl2/JexlEngine;->createInfo(Ljava/lang/String;II)Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/commons/jexl2/JexlInfo;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v2

    .line 1311
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "s":I
    .end local v4    # "se":Ljava/lang/StackTraceElement;
    .end local v5    # "stack":[Ljava/lang/StackTraceElement;
    .end local v6    # "xinfo":Ljava/lang/Throwable;
    :cond_3
    return-object v2
.end method

.method protected varargs doCreateInstance(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1, "clazz"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 749
    const/4 v4, 0x0

    .line 750
    .local v4, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const/4 v2, 0x0

    .line 751
    .local v2, "result":Ljava/lang/Object;
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlEngine;->debugInfo()Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v1

    .line 753
    .local v1, "info":Lorg/apache/commons/jexl2/JexlInfo;
    :try_start_0
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v7, p1, p2, v1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v0

    .line 754
    .local v0, "ctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-nez v0, :cond_0

    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v7, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 755
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v7, p1, p2, v1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v0

    .line 757
    :cond_0
    if-eqz v0, :cond_2

    .line 758
    invoke-interface {v0, p1, p2}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 765
    .end local v2    # "result":Ljava/lang/Object;
    :goto_0
    if-eqz v4, :cond_1

    .line 766
    iget-boolean v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    if-eqz v7, :cond_3

    .line 767
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v2, v6

    .line 773
    .end local v0    # "ctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :cond_1
    :goto_1
    return-object v2

    .line 760
    .restart local v0    # "ctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .restart local v2    # "result":Ljava/lang/Object;
    :cond_2
    :try_start_1
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed finding constructor for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v1, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .local v5, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object v4, v5

    .end local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_0

    .line 770
    .end local v2    # "result":Ljava/lang/Object;
    :cond_3
    throw v4

    .line 762
    .end local v0    # "ctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .restart local v2    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v3

    .line 763
    .local v3, "xany":Ljava/lang/Exception;
    :try_start_2
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed executing constructor for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v1, v7, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 765
    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    if-eqz v5, :cond_7

    .line 766
    iget-boolean v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    if-eqz v7, :cond_4

    .line 767
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v4, v5

    .end local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object v2, v6

    .line 768
    goto :goto_1

    .line 770
    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_4
    throw v5

    .line 765
    .end local v3    # "xany":Ljava/lang/Exception;
    .end local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :catchall_0
    move-exception v7

    if-eqz v4, :cond_6

    .line 766
    iget-boolean v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    if-eqz v7, :cond_5

    .line 767
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v2, v6

    .line 768
    goto :goto_1

    .line 770
    :cond_5
    throw v4

    :cond_6
    throw v7

    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v3    # "xany":Ljava/lang/Exception;
    .restart local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_7
    move-object v4, v5

    .end local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_1
.end method

.method public getArithmetic()Lorg/apache/commons/jexl2/JexlArithmetic;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    return-object v0
.end method

.method public getFunctions()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 395
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    return-object v0
.end method

.method protected getLocalVariables(Lorg/apache/commons/jexl2/Script;)[Ljava/lang/String;
    .locals 1
    .param p1, "script"    # Lorg/apache/commons/jexl2/Script;

    .prologue
    .line 1002
    instance-of v0, p1, Lorg/apache/commons/jexl2/ExpressionImpl;

    if-eqz v0, :cond_0

    .line 1003
    check-cast p1, Lorg/apache/commons/jexl2/ExpressionImpl;

    .end local p1    # "script":Lorg/apache/commons/jexl2/Script;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/ExpressionImpl;->getLocalVariables()[Ljava/lang/String;

    move-result-object v0

    .line 1005
    :goto_0
    return-object v0

    .restart local p1    # "script":Lorg/apache/commons/jexl2/Script;
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method protected getParameters(Lorg/apache/commons/jexl2/Script;)[Ljava/lang/String;
    .locals 1
    .param p1, "script"    # Lorg/apache/commons/jexl2/Script;

    .prologue
    .line 988
    instance-of v0, p1, Lorg/apache/commons/jexl2/ExpressionImpl;

    if-eqz v0, :cond_0

    .line 989
    check-cast p1, Lorg/apache/commons/jexl2/ExpressionImpl;

    .end local p1    # "script":Lorg/apache/commons/jexl2/Script;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/ExpressionImpl;->getParameters()[Ljava/lang/String;

    move-result-object v0

    .line 991
    :goto_0
    return-object v0

    .restart local p1    # "script":Lorg/apache/commons/jexl2/Script;
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getProperty(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "bean"    # Ljava/lang/Object;
    .param p2, "expr"    # Ljava/lang/String;

    .prologue
    .line 587
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lorg/apache/commons/jexl2/JexlEngine;->getProperty(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getProperty(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 10
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p2, "bean"    # Ljava/lang/Object;
    .param p3, "expr"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 602
    if-nez p1, :cond_0

    .line 603
    sget-object p1, Lorg/apache/commons/jexl2/JexlEngine;->EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;

    .line 606
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "#0"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3, v9}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v8, 0x5b

    if-ne v5, v8, :cond_1

    const-string v5, ""

    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ";"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 608
    :try_start_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    const/4 v7, 0x1

    iput-boolean v7, v5, Lorg/apache/commons/jexl2/parser/Parser;->ALLOW_REGISTERS:Z

    .line 609
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$Scope;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "#0"

    aput-object v8, v5, v7

    invoke-direct {v0, v5}, Lorg/apache/commons/jexl2/JexlEngine$Scope;-><init>([Ljava/lang/String;)V

    .line 610
    .local v0, "frame":Lorg/apache/commons/jexl2/JexlEngine$Scope;
    const/4 v5, 0x0

    invoke-virtual {p0, p3, v5, v0}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v3

    .line 611
    .local v3, "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 612
    .local v2, "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v1

    .line 614
    .local v1, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v5, v7

    invoke-virtual {v3, v5}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v5

    invoke-virtual {v1, v5}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 615
    const/4 v5, 0x0

    invoke-virtual {v2, v1, v5}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 623
    iget-object v6, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    iput-boolean v9, v6, Lorg/apache/commons/jexl2/parser/Parser;->ALLOW_REGISTERS:Z

    .end local v0    # "frame":Lorg/apache/commons/jexl2/JexlEngine$Scope;
    .end local v1    # "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    .end local v2    # "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v3    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    :goto_1
    return-object v5

    .line 606
    :cond_1
    const-string v5, "."

    goto :goto_0

    .line 616
    :catch_0
    move-exception v4

    .line 617
    .local v4, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_1
    iget-boolean v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    if-eqz v5, :cond_2

    .line 618
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v8

    invoke-interface {v5, v7, v8}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    iput-boolean v9, v5, Lorg/apache/commons/jexl2/parser/Parser;->ALLOW_REGISTERS:Z

    move-object v5, v6

    goto :goto_1

    .line 621
    :cond_2
    :try_start_2
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 623
    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    iput-boolean v9, v6, Lorg/apache/commons/jexl2/parser/Parser;->ALLOW_REGISTERS:Z

    throw v5
.end method

.method public getUberspect()Lorg/apache/commons/jexl2/introspection/Uberspect;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    return-object v0
.end method

.method public getVariables(Lorg/apache/commons/jexl2/Script;)Ljava/util/Set;
    .locals 3
    .param p1, "script"    # Lorg/apache/commons/jexl2/Script;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/Script;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 910
    instance-of v1, p1, Lorg/apache/commons/jexl2/ExpressionImpl;

    if-eqz v1, :cond_0

    .line 911
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 912
    .local v0, "refs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/List<Ljava/lang/String;>;>;"
    check-cast p1, Lorg/apache/commons/jexl2/ExpressionImpl;

    .end local p1    # "script":Lorg/apache/commons/jexl2/Script;
    iget-object v1, p1, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lorg/apache/commons/jexl2/JexlEngine;->getVariables(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/util/Set;Ljava/util/List;)V

    .line 915
    .end local v0    # "refs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/List<Ljava/lang/String;>;>;"
    :goto_0
    return-object v0

    .restart local p1    # "script":Lorg/apache/commons/jexl2/Script;
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method protected getVariables(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/util/Set;Ljava/util/List;)V
    .locals 12
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/parser/JexlNode;",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "refs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/List<Ljava/lang/String;>;>;"
    .local p3, "ref":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v11, 0x1

    .line 927
    instance-of v0, p1, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;

    .line 928
    .local v0, "array":Z
    instance-of v7, p1, Lorg/apache/commons/jexl2/parser/ASTReference;

    .line 929
    .local v7, "reference":Z
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v6

    .line 930
    .local v6, "num":I
    if-nez v0, :cond_0

    if-eqz v7, :cond_b

    .line 931
    :cond_0
    if-eqz p3, :cond_2

    move-object v8, p3

    .line 932
    .local v8, "var":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    const/4 v9, 0x1

    .line 933
    .local v9, "varf":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v6, :cond_9

    .line 934
    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    .line 935
    .local v1, "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    if-eqz v0, :cond_7

    .line 936
    instance-of v10, v1, Lorg/apache/commons/jexl2/parser/ASTReference;

    if-eqz v10, :cond_6

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v10

    if-ne v10, v11, :cond_6

    .line 937
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 938
    .local v2, "desc":Lorg/apache/commons/jexl2/parser/JexlNode;
    if-eqz v9, :cond_4

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->isConstant()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 939
    iget-object v5, v2, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    .line 940
    .local v5, "image":Ljava/lang/String;
    if-nez v5, :cond_3

    .line 941
    new-instance v10, Lorg/apache/commons/jexl2/Debugger;

    invoke-direct {v10}, Lorg/apache/commons/jexl2/Debugger;-><init>()V

    invoke-virtual {v10, v2}, Lorg/apache/commons/jexl2/Debugger;->data(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 933
    .end local v2    # "desc":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v5    # "image":Ljava/lang/String;
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 931
    .end local v1    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v4    # "i":I
    .end local v8    # "var":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "varf":Z
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 943
    .restart local v1    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    .restart local v2    # "desc":Lorg/apache/commons/jexl2/parser/JexlNode;
    .restart local v4    # "i":I
    .restart local v5    # "image":Ljava/lang/String;
    .restart local v8    # "var":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v9    # "varf":Z
    :cond_3
    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 945
    .end local v5    # "image":Ljava/lang/String;
    :cond_4
    instance-of v10, v2, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    if-eqz v10, :cond_1

    move-object v10, v2

    .line 946
    check-cast v10, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    invoke-virtual {v10}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v10

    if-gez v10, :cond_5

    .line 947
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 948
    .local v3, "di":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v10, v2, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 949
    invoke-interface {p2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 951
    .end local v3    # "di":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "var":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 952
    .restart local v8    # "var":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x0

    goto :goto_2

    .line 955
    .end local v2    # "desc":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_6
    instance-of v10, v1, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    if-eqz v10, :cond_8

    .line 956
    if-nez v4, :cond_1

    move-object v10, v1

    check-cast v10, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    invoke-virtual {v10}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v10

    if-gez v10, :cond_1

    .line 957
    iget-object v10, v1, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 962
    :cond_7
    instance-of v10, v1, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    if-eqz v10, :cond_8

    move-object v10, v1

    .line 963
    check-cast v10, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    invoke-virtual {v10}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v10

    if-gez v10, :cond_1

    .line 964
    iget-object v10, v1, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 969
    :cond_8
    invoke-virtual {p0, v1, p2, v8}, Lorg/apache/commons/jexl2/JexlEngine;->getVariables(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/util/Set;Ljava/util/List;)V

    goto :goto_2

    .line 971
    .end local v1    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_9
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_a

    if-eq v8, p3, :cond_a

    .line 972
    invoke-interface {p2, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 979
    .end local v8    # "var":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "varf":Z
    :cond_a
    return-void

    .line 975
    .end local v4    # "i":I
    :cond_b
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    if-ge v4, v6, :cond_a

    .line 976
    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {p0, v10, p2, v11}, Lorg/apache/commons/jexl2/JexlEngine;->getVariables(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/util/Set;Ljava/util/List;)V

    .line 975
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method public varargs invokeMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "meth"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 691
    const/4 v4, 0x0

    .line 692
    .local v4, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const/4 v2, 0x0

    .line 693
    .local v2, "result":Ljava/lang/Object;
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlEngine;->debugInfo()Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v0

    .line 695
    .local v0, "info":Lorg/apache/commons/jexl2/JexlInfo;
    :try_start_0
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v7, p1, p2, p3, v0}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v1

    .line 696
    .local v1, "method":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-nez v1, :cond_0

    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v7, p3}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 697
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v7, p1, p2, p3, v0}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v1

    .line 699
    :cond_0
    if-eqz v1, :cond_2

    .line 700
    invoke-interface {v1, p1, p3}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 707
    .end local v2    # "result":Ljava/lang/Object;
    :goto_0
    if-eqz v4, :cond_1

    .line 708
    iget-boolean v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    if-eqz v7, :cond_3

    .line 709
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v2, v6

    .line 715
    .end local v1    # "method":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :cond_1
    :goto_1
    return-object v2

    .line 702
    .restart local v1    # "method":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .restart local v2    # "result":Ljava/lang/Object;
    :cond_2
    :try_start_1
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed finding method "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v0, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .local v5, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object v4, v5

    .end local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_0

    .line 712
    .end local v2    # "result":Ljava/lang/Object;
    :cond_3
    throw v4

    .line 704
    .end local v1    # "method":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .restart local v2    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v3

    .line 705
    .local v3, "xany":Ljava/lang/Exception;
    :try_start_2
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed executing method "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v0, v7, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 707
    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    if-eqz v5, :cond_7

    .line 708
    iget-boolean v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    if-eqz v7, :cond_4

    .line 709
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v4, v5

    .end local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object v2, v6

    .line 710
    goto :goto_1

    .line 712
    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_4
    throw v5

    .line 707
    .end local v3    # "xany":Ljava/lang/Exception;
    .end local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :catchall_0
    move-exception v7

    if-eqz v4, :cond_6

    .line 708
    iget-boolean v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    if-eqz v7, :cond_5

    .line 709
    iget-object v7, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v2, v6

    .line 710
    goto :goto_1

    .line 712
    :cond_5
    throw v4

    :cond_6
    throw v7

    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v3    # "xany":Ljava/lang/Exception;
    .restart local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_7
    move-object v4, v5

    .end local v5    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_1
.end method

.method public isDebug()Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->debug:Z

    return v0
.end method

.method public isLenient()Z
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->isLenient()Z

    move-result v0

    return v0
.end method

.method public isSilent()Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    return v0
.end method

.method public final isStrict()Z
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlEngine;->isLenient()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs newInstance(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p2, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 727
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+TT;>;"
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlEngine;->doCreateInstance(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public varargs newInstance(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "clazz"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 738
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlEngine;->doCreateInstance(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .locals 1
    .param p1, "expression"    # Ljava/lang/CharSequence;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1214
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v0

    return-object v0
.end method

.method protected parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .locals 13
    .param p1, "expression"    # Ljava/lang/CharSequence;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;
    .param p3, "frame"    # Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .prologue
    .line 1226
    invoke-static {p1}, Lorg/apache/commons/jexl2/JexlEngine;->cleanExpression(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 1227
    .local v2, "expr":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1228
    .local v5, "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    const/4 v1, 0x0

    .line 1229
    .local v1, "dbgInfo":Lorg/apache/commons/jexl2/JexlInfo;
    iget-object v10, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    monitor-enter v10

    .line 1230
    :try_start_0
    iget-object v9, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-eqz v9, :cond_2

    .line 1231
    iget-object v9, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v9, v2}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-object v5, v0

    .line 1232
    if-eqz v5, :cond_2

    .line 1233
    invoke-virtual {v5}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->getScope()Lorg/apache/commons/jexl2/JexlEngine$Scope;

    move-result-object v3

    .line 1234
    .local v3, "f":Lorg/apache/commons/jexl2/JexlEngine$Scope;
    if-nez v3, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    if-eqz v3, :cond_2

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->equals(Lorg/apache/commons/jexl2/JexlEngine$Scope;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1235
    :cond_1
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object v6, v5

    .line 1265
    .end local v3    # "f":Lorg/apache/commons/jexl2/JexlEngine$Scope;
    .end local v5    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .local v6, "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    :goto_0
    return-object v6

    .line 1240
    .end local v6    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .restart local v5    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    :cond_2
    :try_start_1
    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1242
    .local v4, "reader":Ljava/io/Reader;
    if-nez p2, :cond_5

    .line 1243
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlEngine;->debugInfo()Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v1

    .line 1247
    :goto_1
    iget-object v9, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    move-object/from16 v0, p3

    invoke-virtual {v9, v0}, Lorg/apache/commons/jexl2/parser/Parser;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Scope;)V

    .line 1248
    iget-object v9, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    invoke-virtual {v9, v4, v1}, Lorg/apache/commons/jexl2/parser/Parser;->parse(Ljava/io/Reader;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v5

    .line 1250
    iget-object v9, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    invoke-virtual {v9}, Lorg/apache/commons/jexl2/parser/Parser;->getFrame()Lorg/apache/commons/jexl2/JexlEngine$Scope;

    move-result-object p3

    .line 1251
    if-eqz p3, :cond_3

    .line 1252
    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->setScope(Lorg/apache/commons/jexl2/JexlEngine$Scope;)V

    .line 1254
    :cond_3
    iget-object v9, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-eqz v9, :cond_4

    .line 1255
    iget-object v9, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v9, v2, v5}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/apache/commons/jexl2/parser/TokenMgrError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/commons/jexl2/parser/ParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1262
    :cond_4
    :try_start_2
    iget-object v9, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    const/4 v11, 0x0

    invoke-virtual {v9, v11}, Lorg/apache/commons/jexl2/parser/Parser;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Scope;)V

    .line 1264
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v6, v5

    .line 1265
    .end local v5    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .restart local v6    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    goto :goto_0

    .line 1245
    .end local v6    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .restart local v5    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    :cond_5
    :try_start_3
    invoke-interface {p2}, Lorg/apache/commons/jexl2/JexlInfo;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;
    :try_end_3
    .catch Lorg/apache/commons/jexl2/parser/TokenMgrError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/apache/commons/jexl2/parser/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    goto :goto_1

    .line 1257
    .end local v4    # "reader":Ljava/io/Reader;
    :catch_0
    move-exception v8

    .line 1258
    .local v8, "xtme":Lorg/apache/commons/jexl2/parser/TokenMgrError;
    :try_start_4
    new-instance v9, Lorg/apache/commons/jexl2/JexlException$Tokenization;

    invoke-direct {v9, v1, p1, v8}, Lorg/apache/commons/jexl2/JexlException$Tokenization;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/TokenMgrError;)V

    throw v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1262
    .end local v8    # "xtme":Lorg/apache/commons/jexl2/parser/TokenMgrError;
    :catchall_0
    move-exception v9

    :try_start_5
    iget-object v11, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lorg/apache/commons/jexl2/parser/Parser;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Scope;)V

    throw v9

    .line 1264
    :catchall_1
    move-exception v9

    monitor-exit v10
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v9

    .line 1259
    :catch_1
    move-exception v7

    .line 1260
    .local v7, "xparse":Lorg/apache/commons/jexl2/parser/ParseException;
    :try_start_6
    new-instance v9, Lorg/apache/commons/jexl2/JexlException$Parsing;

    invoke-direct {v9, v1, p1, v7}, Lorg/apache/commons/jexl2/JexlException$Parsing;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/ParseException;)V

    throw v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public setCache(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 347
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    monitor-enter v1

    .line 348
    if-gtz p1, :cond_1

    .line 349
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    .line 353
    :cond_0
    :goto_0
    monitor-exit v1

    .line 354
    return-void

    .line 350
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->size()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 351
    :cond_2
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;-><init>(Lorg/apache/commons/jexl2/JexlEngine;I)V

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    goto :goto_0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setClassLoader(Ljava/lang/ClassLoader;)V
    .locals 1
    .param p1, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 336
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v0, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 337
    return-void
.end method

.method public setDebug(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 248
    iput-boolean p1, p0, Lorg/apache/commons/jexl2/JexlEngine;->debug:Z

    .line 249
    return-void
.end method

.method public setFunctions(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 385
    .local p1, "funcs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .end local p1    # "funcs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    .line 386
    return-void

    .line 385
    .restart local p1    # "funcs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    goto :goto_0
.end method

.method public setLenient(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 292
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    instance-of v0, v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;

    if-eqz v0, :cond_0

    .line 293
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;->setLenient(Ljava/lang/Boolean;)V

    .line 297
    :goto_0
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->setLenient(Z)V

    goto :goto_0
.end method

.method public setProperty(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "bean"    # Ljava/lang/Object;
    .param p2, "expr"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 642
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lorg/apache/commons/jexl2/JexlEngine;->setProperty(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 643
    return-void
.end method

.method public setProperty(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p2, "bean"    # Ljava/lang/Object;
    .param p3, "expr"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    .line 657
    if-nez p1, :cond_0

    .line 658
    sget-object p1, Lorg/apache/commons/jexl2/JexlEngine;->EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;

    .line 661
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "#0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v7, 0x5b

    if-ne v5, v7, :cond_1

    const-string v5, ""

    :goto_0
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "#1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 663
    :try_start_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lorg/apache/commons/jexl2/parser/Parser;->ALLOW_REGISTERS:Z

    .line 664
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$Scope;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "#0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "#1"

    aput-object v7, v5, v6

    invoke-direct {v0, v5}, Lorg/apache/commons/jexl2/JexlEngine$Scope;-><init>([Ljava/lang/String;)V

    .line 665
    .local v0, "frame":Lorg/apache/commons/jexl2/JexlEngine$Scope;
    const/4 v5, 0x0

    invoke-virtual {p0, p3, v5, v0}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v3

    .line 666
    .local v3, "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 667
    .local v2, "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v1

    .line 669
    .local v1, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    aput-object p4, v5, v6

    invoke-virtual {v3, v5}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v5

    invoke-virtual {v1, v5}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 670
    const/4 v5, 0x0

    invoke-virtual {v2, v1, v5}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    iput-boolean v8, v5, Lorg/apache/commons/jexl2/parser/Parser;->ALLOW_REGISTERS:Z

    .line 680
    .end local v0    # "frame":Lorg/apache/commons/jexl2/JexlEngine$Scope;
    .end local v1    # "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    .end local v2    # "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v3    # "script":Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    :goto_1
    return-void

    .line 661
    :cond_1
    const-string v5, "."

    goto :goto_0

    .line 671
    :catch_0
    move-exception v4

    .line 672
    .local v4, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_1
    iget-boolean v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    if-eqz v5, :cond_2

    .line 673
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 678
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    iput-boolean v8, v5, Lorg/apache/commons/jexl2/parser/Parser;->ALLOW_REGISTERS:Z

    goto :goto_1

    .line 676
    :cond_2
    :try_start_2
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 678
    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lorg/apache/commons/jexl2/JexlEngine;->parser:Lorg/apache/commons/jexl2/parser/Parser;

    iput-boolean v8, v6, Lorg/apache/commons/jexl2/parser/Parser;->ALLOW_REGISTERS:Z

    throw v5
.end method

.method public setSilent(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 268
    iput-boolean p1, p0, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    .line 269
    return-void
.end method

.method public final setStrict(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 316
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/JexlEngine;->setLenient(Z)V

    .line 317
    return-void

    .line 316
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
