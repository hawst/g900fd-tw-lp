.class public final Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;
.super Ljava/lang/Object;
.source "UberspectImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/introspection/UberspectImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IndexedContainer"
.end annotation


# instance fields
.field private final object:Ljava/lang/Object;

.field private final type:Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;


# direct methods
.method private constructor <init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;)V
    .locals 0
    .param p1, "theType"    # Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;
    .param p2, "theObject"    # Ljava/lang/Object;

    .prologue
    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    iput-object p1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;->type:Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;

    .line 336
    iput-object p2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;->object:Ljava/lang/Object;

    .line 337
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;Lorg/apache/commons/jexl2/introspection/UberspectImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Lorg/apache/commons/jexl2/introspection/UberspectImpl$1;

    .prologue
    .line 323
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;-><init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;->type:Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;

    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;->object:Ljava/lang/Object;

    # invokes: Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->invokeGet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v0, v1, p1}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->access$200(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 357
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;->type:Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;

    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;->object:Ljava/lang/Object;

    # invokes: Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->invokeSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v0, v1, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->access$300(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
