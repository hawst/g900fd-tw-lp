.class Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;
.super Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConstantExpression"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

.field private final value:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/Object;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 2
    .param p2, "val"    # Ljava/lang/Object;
    .param p3, "source"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 441
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    .line 442
    invoke-direct {p0, p1, p3}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 443
    if-nez p2, :cond_0

    .line 444
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "constant can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446
    :cond_0
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 447
    check-cast p2, Ljava/lang/String;

    .end local p2    # "val":Ljava/lang/Object;
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lorg/apache/commons/jexl2/parser/StringParser;->buildString(Ljava/lang/CharSequence;Z)Ljava/lang/String;

    move-result-object p2

    .line 449
    :cond_1
    iput-object p2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;->value:Ljava/lang/Object;

    .line 450
    return-void
.end method


# virtual methods
.method public asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 1
    .param p1, "strb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 461
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;->value:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 464
    :cond_0
    return-object p1
.end method

.method protected evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;
    .locals 1
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    .line 470
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;->value:Ljava/lang/Object;

    return-object v0
.end method

.method getType()Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->CONSTANT:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    return-object v0
.end method
