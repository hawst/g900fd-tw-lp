.class public Lorg/apache/commons/jexl2/internal/EnumerationIterator;
.super Ljava/lang/Object;
.source "EnumerationIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final enumeration:Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Enumeration",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Enumeration;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Enumeration",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lorg/apache/commons/jexl2/internal/EnumerationIterator;, "Lorg/apache/commons/jexl2/internal/EnumerationIterator<TT;>;"
    .local p1, "enumer":Ljava/util/Enumeration;, "Ljava/util/Enumeration<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/commons/jexl2/internal/EnumerationIterator;->enumeration:Ljava/util/Enumeration;

    .line 43
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 60
    .local p0, "this":Lorg/apache/commons/jexl2/internal/EnumerationIterator;, "Lorg/apache/commons/jexl2/internal/EnumerationIterator<TT;>;"
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/EnumerationIterator;->enumeration:Ljava/util/Enumeration;

    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lorg/apache/commons/jexl2/internal/EnumerationIterator;, "Lorg/apache/commons/jexl2/internal/EnumerationIterator<TT;>;"
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/EnumerationIterator;->enumeration:Ljava/util/Enumeration;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 0

    .prologue
    .line 68
    .local p0, "this":Lorg/apache/commons/jexl2/internal/EnumerationIterator;, "Lorg/apache/commons/jexl2/internal/EnumerationIterator<TT;>;"
    return-void
.end method
