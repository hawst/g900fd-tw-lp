.class public abstract Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
.super Ljava/lang/Object;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "Expression"
.end annotation


# instance fields
.field protected final source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 0
    .param p2, "src"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 250
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    if-eqz p2, :cond_0

    .end local p2    # "src":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :goto_0
    iput-object p2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .line 252
    return-void

    .restart local p2    # "src":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_0
    move-object p2, p0

    .line 251
    goto :goto_0
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 300
    .local v0, "strb":Ljava/lang/StringBuilder;
    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 301
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public abstract asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
.end method

.method protected abstract evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;
.end method

.method public evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;
    .locals 6
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 382
    :try_start_0
    new-instance v0, Lorg/apache/commons/jexl2/Interpreter;

    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v4

    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/commons/jexl2/JexlEngine;->isLenient()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v5}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v5

    invoke-direct {v0, v4, p1, v3, v5}, Lorg/apache/commons/jexl2/Interpreter;-><init>(Lorg/apache/commons/jexl2/JexlEngine;Lorg/apache/commons/jexl2/JexlContext;ZZ)V

    .line 383
    .local v0, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    instance-of v3, p1, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;

    if-eqz v3, :cond_0

    .line 384
    check-cast p1, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;

    .end local p1    # "context":Lorg/apache/commons/jexl2/JexlContext;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->getFrame()Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 386
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 391
    .end local v0    # "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    :goto_1
    return-object v3

    .line 382
    .restart local p1    # "context":Lorg/apache/commons/jexl2/JexlContext;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 387
    .end local p1    # "context":Lorg/apache/commons/jexl2/JexlContext;
    :catch_0
    move-exception v1

    .line 388
    .local v1, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    const-string/jumbo v4, "prepare"

    # invokes: Lorg/apache/commons/jexl2/UnifiedJEXL;->createException(Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    invoke-static {v3, v4, p0, v1}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$200(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;

    move-result-object v2

    .line 389
    .local v2, "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 390
    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v3

    iget-object v3, v3, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 391
    const/4 v3, 0x0

    goto :goto_1

    .line 393
    :cond_2
    throw v2
.end method

.method public final getSource()Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    return-object v0
.end method

.method abstract getType()Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
.end method

.method public getVariables()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 320
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected getVariables(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 330
    .local p1, "refs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/List<Ljava/lang/String;>;>;"
    return-void
.end method

.method public final isDeferred()Z
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->isImmediate()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isImmediate()Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x1

    return v0
.end method

.method protected prepare(Lorg/apache/commons/jexl2/Interpreter;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 0
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    .line 415
    return-object p0
.end method

.method public prepare(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 6
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 354
    :try_start_0
    new-instance v0, Lorg/apache/commons/jexl2/Interpreter;

    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v4

    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/commons/jexl2/JexlEngine;->isLenient()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v5}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v5

    invoke-direct {v0, v4, p1, v3, v5}, Lorg/apache/commons/jexl2/Interpreter;-><init>(Lorg/apache/commons/jexl2/JexlEngine;Lorg/apache/commons/jexl2/JexlContext;ZZ)V

    .line 355
    .local v0, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    instance-of v3, p1, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;

    if-eqz v3, :cond_0

    .line 356
    check-cast p1, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;

    .end local p1    # "context":Lorg/apache/commons/jexl2/JexlContext;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->getFrame()Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 358
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->prepare(Lorg/apache/commons/jexl2/Interpreter;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 363
    .end local v0    # "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    :goto_1
    return-object v3

    .line 354
    .restart local p1    # "context":Lorg/apache/commons/jexl2/JexlContext;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 359
    .end local p1    # "context":Lorg/apache/commons/jexl2/JexlContext;
    :catch_0
    move-exception v1

    .line 360
    .local v1, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    const-string/jumbo v4, "prepare"

    # invokes: Lorg/apache/commons/jexl2/UnifiedJEXL;->createException(Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    invoke-static {v3, v4, p0, v1}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$200(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;

    move-result-object v2

    .line 361
    .local v2, "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 362
    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v3

    iget-object v3, v3, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 363
    const/4 v3, 0x0

    goto :goto_1

    .line 365
    :cond_2
    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 285
    .local v0, "strb":Ljava/lang/StringBuilder;
    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 286
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    if-eq v1, p0, :cond_0

    .line 287
    const-string v1, " /*= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    const-string v1, " */"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
