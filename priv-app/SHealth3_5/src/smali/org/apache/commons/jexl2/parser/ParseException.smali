.class public Lorg/apache/commons/jexl2/parser/ParseException;
.super Ljava/lang/Exception;
.source "ParseException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private after:Ljava/lang/String;

.field private column:I

.field private line:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 92
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ParseException;->after:Ljava/lang/String;

    .line 35
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParseException;->line:I

    .line 39
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParseException;->column:I

    .line 93
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 97
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ParseException;->after:Ljava/lang/String;

    .line 35
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParseException;->line:I

    .line 39
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParseException;->column:I

    .line 98
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/Token;[[I[Ljava/lang/String;)V
    .locals 3
    .param p1, "currentToken"    # Lorg/apache/commons/jexl2/parser/Token;
    .param p2, "expectedTokenSequences"    # [[I
    .param p3, "tokenImage"    # [Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 81
    const-string/jumbo v1, "parse error"

    invoke-direct {p0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 31
    const-string v1, ""

    iput-object v1, p0, Lorg/apache/commons/jexl2/parser/ParseException;->after:Ljava/lang/String;

    .line 35
    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParseException;->line:I

    .line 39
    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParseException;->column:I

    .line 82
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/Token;->next:Lorg/apache/commons/jexl2/parser/Token;

    .line 83
    .local v0, "tok":Lorg/apache/commons/jexl2/parser/Token;
    iget-object v1, v0, Lorg/apache/commons/jexl2/parser/Token;->image:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/commons/jexl2/parser/ParseException;->after:Ljava/lang/String;

    .line 84
    iget v1, v0, Lorg/apache/commons/jexl2/parser/Token;->beginLine:I

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParseException;->line:I

    .line 85
    iget v1, v0, Lorg/apache/commons/jexl2/parser/Token;->beginColumn:I

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParseException;->column:I

    .line 86
    return-void
.end method


# virtual methods
.method public getAfter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParseException;->after:Ljava/lang/String;

    return-object v0
.end method

.method public getColumn()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParseException;->column:I

    return v0
.end method

.method public getLine()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParseException;->line:I

    return v0
.end method
