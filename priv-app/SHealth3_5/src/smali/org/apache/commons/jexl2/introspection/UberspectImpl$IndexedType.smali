.class final Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;
.super Ljava/lang/Object;
.source "UberspectImpl.java"

# interfaces
.implements Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/introspection/UberspectImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IndexedType"
.end annotation


# instance fields
.field private final clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final container:Ljava/lang/String;

.field private final getters:[Ljava/lang/reflect/Method;

.field private final setters:[Ljava/lang/reflect/Method;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/reflect/Method;[Ljava/lang/reflect/Method;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p3, "gets"    # [Ljava/lang/reflect/Method;
    .param p4, "sets"    # [Ljava/lang/reflect/Method;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/reflect/Method;",
            "[",
            "Ljava/lang/reflect/Method;",
            ")V"
        }
    .end annotation

    .prologue
    .line 225
    .local p2, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-object p1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->container:Ljava/lang/String;

    .line 227
    iput-object p2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->clazz:Ljava/lang/Class;

    .line 228
    iput-object p3, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->getters:[Ljava/lang/reflect/Method;

    .line 229
    iput-object p4, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->setters:[Ljava/lang/reflect/Method;

    .line 230
    return-void
.end method

.method static synthetic access$200(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;
    .param p1, "x1"    # Ljava/lang/Object;
    .param p2, "x2"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 208
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->invokeGet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;
    .param p1, "x1"    # Ljava/lang/Object;
    .param p2, "x2"    # Ljava/lang/Object;
    .param p3, "x3"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 208
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->invokeSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private invokeGet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 276
    iget-object v2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->getters:[Ljava/lang/reflect/Method;

    if-eqz v2, :cond_1

    .line 277
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p2, v0, v4

    .line 279
    .local v0, "args":[Ljava/lang/Object;
    iget-object v2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->getters:[Ljava/lang/reflect/Method;

    array-length v2, v2

    if-ne v2, v3, :cond_0

    .line 280
    iget-object v2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->getters:[Ljava/lang/reflect/Method;

    aget-object v1, v2, v4

    .line 284
    .local v1, "jm":Ljava/lang/reflect/Method;
    :goto_0
    if-eqz v1, :cond_1

    .line 285
    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    .line 282
    .end local v1    # "jm":Ljava/lang/reflect/Method;
    :cond_0
    new-instance v2, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    iget-object v3, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->getters:[Ljava/lang/reflect/Method;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->getters:[Ljava/lang/reflect/Method;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMostSpecificMethod(Ljava/util/List;)Ljava/lang/reflect/Method;

    move-result-object v1

    .restart local v1    # "jm":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 288
    .end local v0    # "args":[Ljava/lang/Object;
    .end local v1    # "jm":Ljava/lang/reflect/Method;
    :cond_1
    new-instance v2, Ljava/beans/IntrospectionException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "property get error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/beans/IntrospectionException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private invokeSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 301
    iget-object v2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->setters:[Ljava/lang/reflect/Method;

    if-eqz v2, :cond_1

    .line 302
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p2, v0, v4

    aput-object p3, v0, v3

    .line 304
    .local v0, "args":[Ljava/lang/Object;
    iget-object v2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->setters:[Ljava/lang/reflect/Method;

    array-length v2, v2

    if-ne v2, v3, :cond_0

    .line 305
    iget-object v2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->setters:[Ljava/lang/reflect/Method;

    aget-object v1, v2, v4

    .line 309
    .local v1, "jm":Ljava/lang/reflect/Method;
    :goto_0
    if-eqz v1, :cond_1

    .line 310
    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    .line 307
    .end local v1    # "jm":Ljava/lang/reflect/Method;
    :cond_0
    new-instance v2, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    iget-object v3, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->setters:[Ljava/lang/reflect/Method;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->setters:[Ljava/lang/reflect/Method;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMostSpecificMethod(Ljava/util/List;)Ljava/lang/reflect/Method;

    move-result-object v1

    .restart local v1    # "jm":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 313
    .end local v0    # "args":[Ljava/lang/Object;
    .end local v1    # "jm":Ljava/lang/reflect/Method;
    :cond_1
    new-instance v2, Ljava/beans/IntrospectionException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "property set error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/beans/IntrospectionException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 236
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->clazz:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    new-instance v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;-><init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;Lorg/apache/commons/jexl2/introspection/UberspectImpl$1;)V

    return-object v0

    .line 239
    :cond_0
    new-instance v0, Ljava/beans/IntrospectionException;

    const-string/jumbo v1, "property resolution error"

    invoke-direct {v0, v1}, Ljava/beans/IntrospectionException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x1

    return v0
.end method

.method public tryFailed(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "rval"    # Ljava/lang/Object;

    .prologue
    .line 258
    sget-object v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tryInvoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 247
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->clazz:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;->container:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    new-instance v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;-><init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;Ljava/lang/Object;Lorg/apache/commons/jexl2/introspection/UberspectImpl$1;)V

    .line 250
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
