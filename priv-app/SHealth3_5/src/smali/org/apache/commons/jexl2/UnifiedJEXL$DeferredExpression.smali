.class Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;
.super Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeferredExpression"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 0
    .param p2, "expr"    # Ljava/lang/CharSequence;
    .param p3, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p4, "source"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 559
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    .line 560
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 561
    return-void
.end method


# virtual methods
.method getType()Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
    .locals 1

    .prologue
    .line 572
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->DEFERRED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    return-object v0
.end method

.method protected getVariables(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 585
    .local p1, "refs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/List<Ljava/lang/String;>;>;"
    return-void
.end method

.method public isImmediate()Z
    .locals 1

    .prologue
    .line 566
    const/4 v0, 0x0

    return v0
.end method

.method protected prepare(Lorg/apache/commons/jexl2/Interpreter;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 5
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    .line 578
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;

    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    iget-object v2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;->expr:Ljava/lang/CharSequence;

    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;->node:Lorg/apache/commons/jexl2/parser/JexlNode;

    iget-object v4, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;->source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    return-object v0
.end method
