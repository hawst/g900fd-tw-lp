.class final enum Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
.super Ljava/lang/Enum;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "BlockType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

.field public static final enum DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

.field public static final enum VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 972
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    const-string v1, "VERBATIM"

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    .line 974
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    const-string v1, "DIRECTIVE"

    invoke-direct {v0, v1, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    .line 970
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->$VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 970
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 970
    const-class v0, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    return-object v0
.end method

.method public static final values()[Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    .locals 1

    .prologue
    .line 970
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->$VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    invoke-virtual {v0}, [Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    return-object v0
.end method
