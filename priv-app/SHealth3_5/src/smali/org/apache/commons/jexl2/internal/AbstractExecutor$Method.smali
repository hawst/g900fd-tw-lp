.class public abstract Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor;
.source "AbstractExecutor.java"

# interfaces
.implements Lorg/apache/commons/jexl2/introspection/JexlMethod;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/internal/AbstractExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Method"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;
    }
.end annotation


# instance fields
.field protected final key:Lorg/apache/commons/jexl2/internal/introspection/MethodKey;


# direct methods
.method protected constructor <init>(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;)V
    .locals 1
    .param p2, "km"    # Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 321
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    # getter for: Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;->method:Ljava/lang/reflect/Method;
    invoke-static {p2}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;->access$000(Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 322
    # getter for: Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;->key:Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    invoke-static {p2}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;->access$100(Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method$Parameter;)Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->key:Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    .line 323
    return-void
.end method


# virtual methods
.method public abstract execute(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation
.end method

.method public final getReturnType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->key:Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "params"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 327
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->execute(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public tryExecute(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 370
    sget-object v0, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->TRY_FAILED:Ljava/lang/Object;

    return-object v0
.end method

.method public final tryInvoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "params"    # [Ljava/lang/Object;

    .prologue
    .line 332
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->tryExecute(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
