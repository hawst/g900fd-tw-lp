.class public final Lorg/apache/commons/jexl2/JexlEngine$Scope;
.super Ljava/lang/Object;
.source "JexlEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/JexlEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Scope"
.end annotation


# instance fields
.field private namedRegisters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final parms:I


# direct methods
.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 4
    .param p1, "parameters"    # [Ljava/lang/String;

    .prologue
    .line 1029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1023
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    .line 1030
    if-eqz p1, :cond_0

    .line 1031
    array-length v1, p1

    iput v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    .line 1032
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    .line 1033
    const/4 v0, 0x0

    .local v0, "p":I
    :goto_0
    iget v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    if-ge v0, v1, :cond_1

    .line 1034
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    aget-object v2, p1, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1033
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1037
    .end local v0    # "p":I
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    .line 1039
    :cond_1
    return-void
.end method


# virtual methods
.method public varargs createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;
    .locals 4
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 1103
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-eqz v1, :cond_1

    .line 1104
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 1105
    .local v0, "arguments":[Ljava/lang/Object;
    if-eqz p1, :cond_0

    .line 1106
    iget v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    array-length v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p1, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1108
    :cond_0
    new-instance v2, Lorg/apache/commons/jexl2/JexlEngine$Frame;

    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lorg/apache/commons/jexl2/JexlEngine$Frame;-><init>([Ljava/lang/Object;[Ljava/lang/String;)V

    move-object v1, v2

    .line 1110
    .end local v0    # "arguments":[Ljava/lang/Object;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declareVariable(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1086
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 1087
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    .line 1089
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1090
    .local v0, "register":Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 1091
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1092
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1094
    :cond_1
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1048
    instance-of v0, p1, Lorg/apache/commons/jexl2/JexlEngine$Scope;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->equals(Lorg/apache/commons/jexl2/JexlEngine$Scope;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Lorg/apache/commons/jexl2/JexlEngine$Scope;)Z
    .locals 4
    .param p1, "frame"    # Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1057
    if-ne p0, p1, :cond_1

    .line 1064
    :cond_0
    :goto_0
    return v0

    .line 1059
    :cond_1
    if-eqz p1, :cond_2

    iget v2, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    iget v3, p1, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1060
    goto :goto_0

    .line 1061
    :cond_3
    iget-object v2, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-nez v2, :cond_4

    .line 1062
    iget-object v2, p1, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1064
    :cond_4
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    iget-object v1, p1, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getArgCount()I
    .locals 1

    .prologue
    .line 1119
    iget v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    return v0
.end method

.method public getLocalVariables()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 1154
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-eqz v5, :cond_1

    iget v5, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    if-lez v5, :cond_1

    .line 1155
    iget v5, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    new-array v4, v5, [Ljava/lang/String;

    .line 1156
    .local v4, "pa":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 1157
    .local v2, "p":I
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1158
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget v6, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    if-lt v5, v6, :cond_0

    .line 1159
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "p":I
    .local v3, "p":I
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    aput-object v5, v4, v2

    move v2, v3

    .end local v3    # "p":I
    .restart local v2    # "p":I
    goto :goto_0

    .line 1164
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "p":I
    .end local v4    # "pa":[Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    :cond_2
    return-object v4
.end method

.method public getParameters()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 1135
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-eqz v5, :cond_1

    iget v5, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    if-lez v5, :cond_1

    .line 1136
    iget v5, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    new-array v4, v5, [Ljava/lang/String;

    .line 1137
    .local v4, "pa":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 1138
    .local v2, "p":I
    iget-object v5, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1139
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget v6, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    if-ge v5, v6, :cond_0

    .line 1140
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "p":I
    .local v3, "p":I
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    aput-object v5, v4, v2

    move v2, v3

    .end local v3    # "p":I
    .restart local v2    # "p":I
    goto :goto_0

    .line 1145
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "p":I
    .end local v4    # "pa":[Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    :cond_2
    return-object v4
.end method

.method public getRegister(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1074
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRegisters()[Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1127
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1043
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->parms:I

    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$Scope;->namedRegisters:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    goto :goto_0
.end method
