.class public Lorg/apache/commons/jexl2/parser/ParserTokenManager;
.super Ljava/lang/Object;
.source "ParserTokenManager.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/ParserConstants;


# static fields
.field static final jjbitVec0:[J

.field static final jjbitVec2:[J

.field static final jjbitVec3:[J

.field static final jjbitVec4:[J

.field public static final jjnewLexState:[I

.field static final jjnextStates:[I

.field public static final jjstrLiteralImages:[Ljava/lang/String;

.field static final jjtoSkip:[J

.field static final jjtoToken:[J

.field public static final lexStateNames:[Ljava/lang/String;


# instance fields
.field protected curChar:C

.field curLexState:I

.field public debugStream:Ljava/io/PrintStream;

.field defaultLexState:I

.field protected input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

.field jjmatchedKind:I

.field jjmatchedPos:I

.field jjnewStateCnt:I

.field jjround:I

.field private final jjrounds:[I

.field private final jjstateSet:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 390
    new-array v0, v2, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec0:[J

    .line 393
    new-array v0, v2, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec2:[J

    .line 396
    new-array v0, v2, [J

    fill-array-data v0, :array_2

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec3:[J

    .line 399
    new-array v0, v2, [J

    fill-array-data v0, :array_3

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec4:[J

    .line 3067
    const/16 v0, 0x3b

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnextStates:[I

    .line 3101
    const/16 v0, 0x3f

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v4

    aput-object v3, v0, v5

    aput-object v3, v0, v6

    const/4 v1, 0x3

    aput-object v3, v0, v1

    aput-object v3, v0, v2

    const/4 v1, 0x5

    aput-object v3, v0, v1

    const/4 v1, 0x6

    aput-object v3, v0, v1

    const/4 v1, 0x7

    aput-object v3, v0, v1

    const/16 v1, 0x8

    aput-object v3, v0, v1

    const/16 v1, 0x9

    const-string v2, "if"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "else"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "for"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "foreach"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "while"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "new"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "var"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "empty"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "size"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "true"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "false"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "return"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "in"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "{"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "}"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "["

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "]"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ";"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "?"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "?:"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    aput-object v3, v0, v1

    const/16 v1, 0x24

    aput-object v3, v0, v1

    const/16 v1, 0x25

    aput-object v3, v0, v1

    const/16 v1, 0x26

    aput-object v3, v0, v1

    const/16 v1, 0x27

    const-string v2, "=~"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "!~"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    aput-object v3, v0, v1

    const/16 v1, 0x2a

    aput-object v3, v0, v1

    const/16 v1, 0x2b

    aput-object v3, v0, v1

    const/16 v1, 0x2c

    aput-object v3, v0, v1

    const/16 v1, 0x2d

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    aput-object v3, v0, v1

    const/16 v1, 0x2f

    aput-object v3, v0, v1

    const/16 v1, 0x30

    aput-object v3, v0, v1

    const/16 v1, 0x31

    const-string v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "-"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "~"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "&"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "^"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    aput-object v3, v0, v1

    const/16 v1, 0x39

    aput-object v3, v0, v1

    const/16 v1, 0x3a

    aput-object v3, v0, v1

    const/16 v1, 0x3b

    aput-object v3, v0, v1

    const/16 v1, 0x3c

    aput-object v3, v0, v1

    const/16 v1, 0x3d

    aput-object v3, v0, v1

    const/16 v1, 0x3e

    aput-object v3, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    .line 3112
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "REGISTERS"

    aput-object v1, v0, v4

    const-string v1, "FOR_EACH_IN"

    aput-object v1, v0, v5

    const-string v1, "DEFAULT"

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->lexStateNames:[Ljava/lang/String;

    .line 3119
    const/16 v0, 0x3f

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    .line 3124
    new-array v0, v5, [J

    const-wide v1, 0x79fffffffffffe01L    # 4.5380154677664145E279

    aput-wide v1, v0, v4

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjtoToken:[J

    .line 3127
    new-array v0, v5, [J

    const-wide/16 v1, 0x1fe

    aput-wide v1, v0, v4

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjtoSkip:[J

    return-void

    .line 390
    nop

    :array_0
    .array-data 8
        -0x2
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 393
    :array_1
    .array-data 8
        0x0
        0x0
        -0x1
        -0x1
    .end array-data

    .line 396
    :array_2
    .array-data 8
        -0x100000002L
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 399
    :array_3
    .array-data 8
        -0x30000000001L
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 3067
    :array_4
    .array-data 4
        0x3f
        0x45
        0x36
        0x37
        0x39
        0x31
        0x32
        0x34
        0x3b
        0x3c
        0x28
        0x1
        0x2
        0x4
        0x2b
        0x2c
        0x2f
        0x41
        0x42
        0x44
        0x46
        0x47
        0x49
        0x52
        0x53
        0x4f
        0x50
        0x4b
        0x4d
        0x2d
        0x2e
        0x41
        0x47
        0x38
        0x39
        0x3b
        0x33
        0x34
        0x36
        0x3d
        0x3e
        0x2a
        0x2d
        0x2e
        0x31
        0x43
        0x44
        0x46
        0x48
        0x49
        0x4b
        0x54
        0x55
        0x51
        0x52
        0x4d
        0x4f
        0x2f
        0x30
    .end array-data

    .line 3119
    :array_5
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x2
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/SimpleCharStream;)V
    .locals 2
    .param p1, "stream"    # Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    .prologue
    const/4 v1, 0x2

    .line 3135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->debugStream:Ljava/io/PrintStream;

    .line 3131
    const/16 v0, 0x56

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjrounds:[I

    .line 3132
    const/16 v0, 0xac

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    .line 3203
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    .line 3204
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->defaultLexState:I

    .line 3138
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    .line 3139
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/SimpleCharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/commons/jexl2/parser/SimpleCharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 3143
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;-><init>(Lorg/apache/commons/jexl2/parser/SimpleCharStream;)V

    .line 3144
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->SwitchTo(I)V

    .line 3145
    return-void
.end method

.method private ReInitRounds()V
    .locals 4

    .prologue
    .line 3158
    const v2, -0x7fffffff

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    .line 3159
    const/16 v0, 0x56

    .local v0, "i":I
    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    if-lez v1, :cond_0

    .line 3160
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjrounds:[I

    const/high16 v3, -0x80000000

    aput v3, v2, v0

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 3161
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_0
    return-void
.end method

.method private jjAddStates(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 3315
    :goto_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    sget-object v3, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnextStates:[I

    aget v3, v3, p1

    aput v3, v1, v2

    .line 3316
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 3317
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private static final jjCanMove_0(IIIJJ)Z
    .locals 6
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3075
    packed-switch p0, :pswitch_data_0

    .line 3080
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec0:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 3082
    :cond_0
    :goto_0
    return v0

    .line 3078
    :pswitch_0
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec2:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 3082
    goto :goto_0

    .line 3075
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static final jjCanMove_1(IIIJJ)Z
    .locals 6
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3087
    sparse-switch p0, :sswitch_data_0

    .line 3094
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec3:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 3096
    :cond_0
    :goto_0
    return v0

    .line 3090
    :sswitch_0
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec2:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 3092
    :sswitch_1
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec4:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 3096
    goto :goto_0

    .line 3087
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch
.end method

.method private jjCheckNAdd(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 3306
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjrounds:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    if-eq v0, v1, :cond_0

    .line 3308
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    aput p1, v0, v1

    .line 3309
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjrounds:[I

    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    aput v1, v0, p1

    .line 3311
    :cond_0
    return-void
.end method

.method private jjCheckNAddStates(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 3327
    :goto_0
    sget-object v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnextStates:[I

    aget v1, v1, p1

    invoke-direct {p0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 3328
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 3329
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private jjCheckNAddTwoStates(II)V
    .locals 0
    .param p1, "state1"    # I
    .param p2, "state2"    # I

    .prologue
    .line 3320
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 3321
    invoke-direct {p0, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 3322
    return-void
.end method

.method private jjMoveNfa_0(II)I
    .locals 19
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 2420
    const/4 v14, 0x0

    .line 2421
    .local v14, "startsAt":I
    const/16 v15, 0x56

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    .line 2422
    const/4 v10, 0x1

    .line 2423
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 2424
    const v11, 0x7fffffff

    .line 2427
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 2428
    invoke-direct/range {p0 .. p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInitRounds()V

    .line 2429
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_28

    .line 2431
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    shl-long v12, v15, v17

    .line 2434
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 2771
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 3054
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 3056
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 3057
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 3058
    const v11, 0x7fffffff

    .line 3060
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 3061
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x56

    if-ne v10, v14, :cond_44

    .line 3064
    :goto_3
    return p2

    .line 2437
    .restart local v12    # "l":J
    :pswitch_1
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_6

    .line 2438
    const/16 v15, 0x2b

    const/16 v16, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 2471
    :cond_4
    :goto_4
    const-wide/high16 v15, 0x3fe000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_12

    .line 2473
    const/16 v15, 0x3c

    if-le v11, v15, :cond_5

    .line 2474
    const/16 v11, 0x3c

    .line 2475
    :cond_5
    const/16 v15, 0x29

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_1

    .line 2439
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 2440
    const/16 v15, 0x1f

    const/16 v16, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 2441
    :cond_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 2442
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 2443
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 2444
    const/16 v15, 0x24

    const/16 v16, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 2445
    :cond_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 2446
    const/16 v15, 0x27

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 2447
    :cond_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_c

    .line 2449
    const/16 v15, 0x38

    if-le v11, v15, :cond_b

    .line 2450
    const/16 v11, 0x38

    .line 2451
    :cond_b
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_4

    .line 2453
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    .line 2455
    const/16 v15, 0x30

    if-le v11, v15, :cond_4

    .line 2456
    const/16 v11, 0x30

    goto/16 :goto_4

    .line 2458
    :cond_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 2460
    const/16 v15, 0x2e

    if-le v11, v15, :cond_4

    .line 2461
    const/16 v11, 0x2e

    goto/16 :goto_4

    .line 2463
    :cond_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 2464
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_4

    .line 2465
    :cond_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_10

    .line 2466
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_4

    .line 2467
    :cond_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 2468
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_4

    .line 2469
    :cond_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 2470
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_4

    .line 2477
    :cond_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ne v15, v0, :cond_14

    .line 2479
    const/16 v15, 0x3c

    if-le v11, v15, :cond_13

    .line 2480
    const/16 v11, 0x3c

    .line 2481
    :cond_13
    const/16 v15, 0x27

    const/16 v16, 0x29

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2483
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_15

    .line 2485
    const/16 v15, 0x2f

    if-le v11, v15, :cond_2

    .line 2486
    const/16 v11, 0x2f

    goto/16 :goto_1

    .line 2488
    :cond_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 2490
    const/16 v15, 0x2b

    if-le v11, v15, :cond_2

    .line 2491
    const/16 v11, 0x2b

    goto/16 :goto_1

    .line 2493
    :cond_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_17

    .line 2495
    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 2496
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 2498
    :cond_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_18

    .line 2499
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2500
    :cond_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2501
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x0

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2505
    :pswitch_2
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2507
    const/16 v15, 0x38

    if-le v11, v15, :cond_19

    .line 2508
    const/16 v11, 0x38

    .line 2509
    :cond_19
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2512
    :pswitch_3
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2514
    const/16 v15, 0x38

    if-le v11, v15, :cond_1a

    .line 2515
    const/16 v11, 0x38

    .line 2516
    :cond_1a
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2519
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2521
    const/4 v15, 0x1

    if-le v11, v15, :cond_1b

    .line 2522
    const/4 v11, 0x1

    .line 2523
    :cond_1b
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2526
    :pswitch_5
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2528
    const/4 v15, 0x1

    if-le v11, v15, :cond_1c

    .line 2529
    const/4 v11, 0x1

    .line 2530
    :cond_1c
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2533
    :pswitch_6
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 2534
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 2537
    :pswitch_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 2538
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 2541
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2542
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2545
    :pswitch_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x23

    if-le v11, v15, :cond_2

    .line 2546
    const/16 v11, 0x23

    goto/16 :goto_1

    .line 2549
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2550
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2553
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x25

    if-le v11, v15, :cond_2

    .line 2554
    const/16 v11, 0x25

    goto/16 :goto_1

    .line 2557
    :pswitch_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2558
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2561
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x26

    if-le v11, v15, :cond_2

    .line 2562
    const/16 v11, 0x26

    goto/16 :goto_1

    .line 2565
    :pswitch_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2566
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2569
    :pswitch_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 2570
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 2573
    :pswitch_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2a

    if-le v11, v15, :cond_2

    .line 2574
    const/16 v11, 0x2a

    goto/16 :goto_1

    .line 2577
    :pswitch_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2578
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2581
    :pswitch_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2b

    if-le v11, v15, :cond_2

    .line 2582
    const/16 v11, 0x2b

    goto/16 :goto_1

    .line 2585
    :pswitch_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2c

    if-le v11, v15, :cond_2

    .line 2586
    const/16 v11, 0x2c

    goto/16 :goto_1

    .line 2589
    :pswitch_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2590
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2593
    :pswitch_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2e

    if-le v11, v15, :cond_2

    .line 2594
    const/16 v11, 0x2e

    goto/16 :goto_1

    .line 2597
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2f

    if-le v11, v15, :cond_2

    .line 2598
    const/16 v11, 0x2f

    goto/16 :goto_1

    .line 2601
    :pswitch_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x30

    if-le v11, v15, :cond_2

    .line 2602
    const/16 v11, 0x30

    goto/16 :goto_1

    .line 2605
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2607
    const/16 v15, 0x38

    if-le v11, v15, :cond_1d

    .line 2608
    const/16 v11, 0x38

    .line 2609
    :cond_1d
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2612
    :pswitch_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2613
    const/16 v15, 0x27

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2616
    :pswitch_1a
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2618
    const/16 v15, 0x3b

    if-le v11, v15, :cond_1e

    .line 2619
    const/16 v11, 0x3b

    .line 2620
    :cond_1e
    const/16 v15, 0x27

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2623
    :pswitch_1b
    const-wide/high16 v15, 0x3fe000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2625
    const/16 v15, 0x3c

    if-le v11, v15, :cond_1f

    .line 2626
    const/16 v11, 0x3c

    .line 2627
    :cond_1f
    const/16 v15, 0x29

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2630
    :pswitch_1c
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2632
    const/16 v15, 0x3c

    if-le v11, v15, :cond_20

    .line 2633
    const/16 v11, 0x3c

    .line 2634
    :cond_20
    const/16 v15, 0x29

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2637
    :pswitch_1d
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2638
    const/16 v15, 0x2b

    const/16 v16, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2641
    :pswitch_1e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2642
    const/16 v15, 0x2d

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2645
    :pswitch_1f
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2647
    const/16 v15, 0x3d

    if-le v11, v15, :cond_21

    .line 2648
    const/16 v11, 0x3d

    .line 2649
    :cond_21
    const/16 v15, 0x2a

    const/16 v16, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2652
    :pswitch_20
    const-wide v15, 0x280000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2653
    const/16 v15, 0x30

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2656
    :pswitch_21
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2658
    const/16 v15, 0x3d

    if-le v11, v15, :cond_22

    .line 2659
    const/16 v11, 0x3d

    .line 2660
    :cond_22
    const/16 v15, 0x30

    const/16 v16, 0x31

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2663
    :pswitch_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2664
    const/16 v15, 0x24

    const/16 v16, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2667
    :pswitch_23
    const-wide v15, -0x400002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2668
    const/16 v15, 0x24

    const/16 v16, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2671
    :pswitch_24
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2672
    const/16 v15, 0x24

    const/16 v16, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2675
    :pswitch_25
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3e

    if-le v11, v15, :cond_2

    .line 2676
    const/16 v11, 0x3e

    goto/16 :goto_1

    .line 2679
    :pswitch_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2680
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2683
    :pswitch_27
    const-wide v15, -0x8000002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2684
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2687
    :pswitch_28
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2688
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2691
    :pswitch_29
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3e

    if-le v11, v15, :cond_2

    .line 2692
    const/16 v11, 0x3e

    goto/16 :goto_1

    .line 2695
    :pswitch_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2697
    const/16 v15, 0x3c

    if-le v11, v15, :cond_23

    .line 2698
    const/16 v11, 0x3c

    .line 2699
    :cond_23
    const/16 v15, 0x27

    const/16 v16, 0x29

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2702
    :pswitch_2b
    const-wide/high16 v15, 0xff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2704
    const/16 v15, 0x3c

    if-le v11, v15, :cond_24

    .line 2705
    const/16 v11, 0x3c

    .line 2706
    :cond_24
    const/16 v15, 0x3d

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2709
    :pswitch_2c
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2711
    const/16 v15, 0x3c

    if-le v11, v15, :cond_25

    .line 2712
    const/16 v11, 0x3c

    .line 2713
    :cond_25
    const/16 v15, 0x3f

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2716
    :pswitch_2d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2717
    const/16 v15, 0x1f

    const/16 v16, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_1

    .line 2720
    :pswitch_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2721
    const/16 v15, 0x42

    const/16 v16, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2724
    :pswitch_2f
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2725
    const/16 v15, 0x42

    const/16 v16, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2728
    :pswitch_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2729
    const/16 v15, 0x2d

    const/16 v16, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2732
    :pswitch_31
    const-wide v15, -0x840000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2733
    const/16 v15, 0x45

    const/16 v16, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2736
    :pswitch_32
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2737
    const/16 v15, 0x45

    const/16 v16, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2740
    :pswitch_33
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x2

    if-le v11, v15, :cond_2

    .line 2741
    const/4 v11, 0x2

    goto/16 :goto_1

    .line 2744
    :pswitch_34
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2746
    const/4 v15, 0x3

    if-le v11, v15, :cond_26

    .line 2747
    const/4 v11, 0x3

    .line 2748
    :cond_26
    const/16 v15, 0x30

    const/16 v16, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2751
    :pswitch_35
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2753
    const/4 v15, 0x3

    if-le v11, v15, :cond_27

    .line 2754
    const/4 v11, 0x3

    .line 2755
    :cond_27
    const/16 v15, 0x30

    const/16 v16, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2758
    :pswitch_36
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 2759
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 2762
    :pswitch_37
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 2763
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 2766
    :pswitch_38
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2767
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4a

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2773
    .end local v12    # "l":J
    :cond_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_3f

    .line 2775
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v12, v15, v17

    .line 2778
    .restart local v12    # "l":J
    :cond_29
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 3004
    :cond_2a
    :goto_5
    :pswitch_39
    if-ne v10, v14, :cond_29

    goto/16 :goto_2

    .line 2781
    :pswitch_3a
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2d

    .line 2783
    const/16 v15, 0x38

    if-le v11, v15, :cond_2b

    .line 2784
    const/16 v11, 0x38

    .line 2785
    :cond_2b
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 2789
    :cond_2c
    :goto_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2e

    .line 2790
    const/16 v15, 0x33

    const/16 v16, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 2787
    :cond_2d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2c

    .line 2788
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto :goto_6

    .line 2791
    :cond_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_2f

    .line 2792
    const/16 v15, 0x35

    const/16 v16, 0x36

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 2793
    :cond_2f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_30

    .line 2794
    const/16 v15, 0x37

    const/16 v16, 0x38

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 2795
    :cond_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_31

    .line 2796
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2797
    :cond_31
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_32

    .line 2798
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2799
    :cond_32
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_33

    .line 2800
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2801
    :cond_33
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_34

    .line 2802
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2803
    :cond_34
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2804
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2807
    :pswitch_3b
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_36

    .line 2809
    const/16 v15, 0x38

    if-le v11, v15, :cond_35

    .line 2810
    const/16 v11, 0x38

    .line 2811
    :cond_35
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 2813
    :cond_36
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x71

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2815
    const/16 v15, 0x25

    if-le v11, v15, :cond_2a

    .line 2816
    const/16 v11, 0x25

    goto/16 :goto_5

    .line 2820
    :pswitch_3c
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_38

    .line 2822
    const/16 v15, 0x38

    if-le v11, v15, :cond_37

    .line 2823
    const/16 v11, 0x38

    .line 2824
    :cond_37
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 2826
    :cond_38
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_39

    .line 2827
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2828
    :cond_39
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2830
    const/16 v15, 0x26

    if-le v11, v15, :cond_2a

    .line 2831
    const/16 v11, 0x26

    goto/16 :goto_5

    .line 2835
    :pswitch_3d
    const/4 v15, 0x1

    if-le v11, v15, :cond_3a

    .line 2836
    const/4 v11, 0x1

    .line 2837
    :cond_3a
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2840
    :pswitch_3e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x23

    if-le v11, v15, :cond_2a

    .line 2841
    const/16 v11, 0x23

    goto/16 :goto_5

    .line 2844
    :pswitch_3f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2845
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2848
    :pswitch_40
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2849
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2852
    :pswitch_41
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x24

    if-le v11, v15, :cond_2a

    .line 2853
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 2856
    :pswitch_42
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2857
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2860
    :pswitch_43
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x72

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x24

    if-le v11, v15, :cond_2a

    .line 2861
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 2864
    :pswitch_44
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2865
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2868
    :pswitch_45
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2869
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2872
    :pswitch_46
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x2e

    if-le v11, v15, :cond_2a

    .line 2873
    const/16 v11, 0x2e

    goto/16 :goto_5

    .line 2876
    :pswitch_47
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2877
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1c

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2880
    :pswitch_48
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2881
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2884
    :pswitch_49
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x76

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x2f

    if-le v11, v15, :cond_2a

    .line 2885
    const/16 v11, 0x2f

    goto/16 :goto_5

    .line 2888
    :pswitch_4a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x69

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2889
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x20

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2892
    :pswitch_4b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2893
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2896
    :pswitch_4c
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 2898
    const/16 v15, 0x38

    if-le v11, v15, :cond_3b

    .line 2899
    const/16 v11, 0x38

    .line 2900
    :cond_3b
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 2903
    :pswitch_4d
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 2905
    const/16 v15, 0x38

    if-le v11, v15, :cond_3c

    .line 2906
    const/16 v11, 0x38

    .line 2907
    :cond_3c
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 2910
    :pswitch_4e
    const-wide v15, 0x110000001100L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    const/16 v15, 0x3c

    if-le v11, v15, :cond_2a

    .line 2911
    const/16 v11, 0x3c

    goto/16 :goto_5

    .line 2914
    :pswitch_4f
    const-wide v15, 0x2000000020L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 2915
    const/16 v15, 0x39

    const/16 v16, 0x3a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2918
    :pswitch_50
    const-wide v15, 0x5400000054L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    const/16 v15, 0x3d

    if-le v11, v15, :cond_2a

    .line 2919
    const/16 v11, 0x3d

    goto/16 :goto_5

    .line 2922
    :pswitch_51
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 2923
    const/16 v15, 0x24

    const/16 v16, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 2926
    :pswitch_52
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2927
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x35

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2930
    :pswitch_53
    const/16 v15, 0x24

    const/16 v16, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 2933
    :pswitch_54
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 2934
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 2937
    :pswitch_55
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2938
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3a

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2941
    :pswitch_56
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 2944
    :pswitch_57
    const-wide v15, 0x100000001000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 2945
    const/16 v15, 0x3f

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 2948
    :pswitch_58
    const-wide v15, 0x7e0000007eL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 2950
    const/16 v15, 0x3c

    if-le v11, v15, :cond_3d

    .line 2951
    const/16 v11, 0x3c

    .line 2952
    :cond_3d
    const/16 v15, 0x3f

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 2955
    :pswitch_59
    const/16 v15, 0x42

    const/16 v16, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 2959
    :pswitch_5a
    const/16 v15, 0x45

    const/16 v16, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 2962
    :pswitch_5b
    const/4 v15, 0x3

    if-le v11, v15, :cond_3e

    .line 2963
    const/4 v11, 0x3

    .line 2964
    :cond_3e
    const/16 v15, 0x30

    const/16 v16, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2967
    :pswitch_5c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2968
    const/16 v15, 0x37

    const/16 v16, 0x38

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2971
    :pswitch_5d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x30

    if-le v11, v15, :cond_2a

    .line 2972
    const/16 v11, 0x30

    goto/16 :goto_5

    .line 2975
    :pswitch_5e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2976
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2979
    :pswitch_5f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2980
    const/16 v15, 0x35

    const/16 v16, 0x36

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2983
    :pswitch_60
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x29

    if-le v11, v15, :cond_2a

    .line 2984
    const/16 v11, 0x29

    goto/16 :goto_5

    .line 2987
    :pswitch_61
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x2a

    if-le v11, v15, :cond_2a

    .line 2988
    const/16 v11, 0x2a

    goto/16 :goto_5

    .line 2991
    :pswitch_62
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2992
    const/16 v15, 0x33

    const/16 v16, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2995
    :pswitch_63
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x2b

    if-le v11, v15, :cond_2a

    .line 2996
    const/16 v11, 0x2b

    goto/16 :goto_5

    .line 2999
    :pswitch_64
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    const/16 v15, 0x2c

    if-le v11, v15, :cond_2a

    .line 3000
    const/16 v11, 0x2c

    goto/16 :goto_5

    .line 3008
    .end local v12    # "l":J
    :cond_3f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shr-int/lit8 v2, v15, 0x8

    .line 3009
    .local v2, "hiByte":I
    shr-int/lit8 v3, v2, 0x6

    .line 3010
    .local v3, "i1":I
    const-wide/16 v15, 0x1

    and-int/lit8 v17, v2, 0x3f

    shl-long v5, v15, v17

    .line 3011
    .local v5, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v4, v15, 0x6

    .line 3012
    .local v4, "i2":I
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v7, v15, v17

    .line 3015
    .local v7, "l2":J
    :cond_40
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 3052
    :cond_41
    :goto_7
    if-ne v10, v14, :cond_40

    goto/16 :goto_2

    .line 3018
    :sswitch_0
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_41

    .line 3020
    const/4 v15, 0x1

    if-le v11, v15, :cond_42

    .line 3021
    const/4 v11, 0x1

    .line 3022
    :cond_42
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 3026
    :sswitch_1
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_41

    .line 3027
    const/16 v15, 0x24

    const/16 v16, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 3031
    :sswitch_2
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_41

    .line 3032
    const/16 v15, 0x21

    const/16 v16, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 3035
    :sswitch_3
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_41

    .line 3036
    const/16 v15, 0x42

    const/16 v16, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 3040
    :sswitch_4
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_41

    .line 3041
    const/16 v15, 0x45

    const/16 v16, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 3044
    :sswitch_5
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_41

    .line 3046
    const/4 v15, 0x3

    if-le v11, v15, :cond_43

    .line 3047
    const/4 v11, 0x3

    .line 3048
    :cond_43
    const/16 v15, 0x30

    const/16 v16, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 3063
    .end local v2    # "hiByte":I
    .end local v3    # "i1":I
    .end local v4    # "i2":I
    .end local v5    # "l1":J
    .end local v7    # "l2":J
    :cond_44
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v15}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3064
    :catch_0
    move-exception v9

    .local v9, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 2434
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_2
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_2
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_23
        :pswitch_0
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_0
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_0
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 2778
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3d
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_3a
        :pswitch_39
        :pswitch_39
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_39
        :pswitch_39
        :pswitch_3b
        :pswitch_45
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_39
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_39
        :pswitch_4c
        :pswitch_4d
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_4e
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_4f
        :pswitch_39
        :pswitch_39
        :pswitch_50
        :pswitch_39
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_39
        :pswitch_39
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_57
        :pswitch_58
        :pswitch_39
        :pswitch_39
        :pswitch_59
        :pswitch_39
        :pswitch_5a
        :pswitch_5a
        :pswitch_39
        :pswitch_39
        :pswitch_5b
        :pswitch_39
        :pswitch_39
        :pswitch_39
        :pswitch_5c
        :pswitch_3c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_64
    .end packed-switch

    .line 3015
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x33 -> :sswitch_1
        0x35 -> :sswitch_1
        0x38 -> :sswitch_2
        0x3a -> :sswitch_2
        0x42 -> :sswitch_3
        0x44 -> :sswitch_4
        0x45 -> :sswitch_4
        0x48 -> :sswitch_5
    .end sparse-switch
.end method

.method private jjMoveNfa_1(II)I
    .locals 19
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 1414
    const/4 v14, 0x0

    .line 1415
    .local v14, "startsAt":I
    const/16 v15, 0x54

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    .line 1416
    const/4 v10, 0x1

    .line 1417
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 1418
    const v11, 0x7fffffff

    .line 1421
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 1422
    invoke-direct/range {p0 .. p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInitRounds()V

    .line 1423
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_26

    .line 1425
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    shl-long v12, v15, v17

    .line 1428
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 1752
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 2035
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 2037
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2038
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 2039
    const v11, 0x7fffffff

    .line 2041
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 2042
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x54

    if-ne v10, v14, :cond_42

    .line 2045
    :goto_3
    return p2

    .line 1432
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1434
    const/16 v15, 0x38

    if-le v11, v15, :cond_4

    .line 1435
    const/16 v11, 0x38

    .line 1436
    :cond_4
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 1439
    :pswitch_2
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1441
    const/16 v15, 0x38

    if-le v11, v15, :cond_5

    .line 1442
    const/16 v11, 0x38

    .line 1443
    :cond_5
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 1446
    :pswitch_3
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_8

    .line 1447
    const/16 v15, 0x29

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 1480
    :cond_6
    :goto_4
    const-wide/high16 v15, 0x3fe000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_14

    .line 1482
    const/16 v15, 0x3c

    if-le v11, v15, :cond_7

    .line 1483
    const/16 v11, 0x3c

    .line 1484
    :cond_7
    const/16 v15, 0x27

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1448
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 1449
    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 1450
    :cond_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 1451
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 1452
    :cond_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    .line 1453
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 1454
    :cond_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    .line 1456
    const/16 v15, 0x38

    if-le v11, v15, :cond_c

    .line 1457
    const/16 v11, 0x38

    .line 1458
    :cond_c
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 1460
    :cond_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 1462
    const/16 v15, 0x30

    if-le v11, v15, :cond_6

    .line 1463
    const/16 v11, 0x30

    goto/16 :goto_4

    .line 1465
    :cond_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 1467
    const/16 v15, 0x2e

    if-le v11, v15, :cond_6

    .line 1468
    const/16 v11, 0x2e

    goto/16 :goto_4

    .line 1470
    :cond_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_10

    .line 1471
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1472
    :cond_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 1473
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1474
    :cond_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_12

    .line 1475
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1476
    :cond_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_13

    .line 1477
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1478
    :cond_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 1479
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x0

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1486
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 1488
    const/16 v15, 0x3c

    if-le v11, v15, :cond_15

    .line 1489
    const/16 v11, 0x3c

    .line 1490
    :cond_15
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1492
    :cond_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_17

    .line 1494
    const/16 v15, 0x2f

    if-le v11, v15, :cond_2

    .line 1495
    const/16 v11, 0x2f

    goto/16 :goto_1

    .line 1497
    :cond_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_18

    .line 1499
    const/16 v15, 0x2b

    if-le v11, v15, :cond_2

    .line 1500
    const/16 v11, 0x2b

    goto/16 :goto_1

    .line 1502
    :cond_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_19

    .line 1504
    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 1505
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 1507
    :cond_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1508
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1511
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1513
    const/4 v15, 0x1

    if-le v11, v15, :cond_1a

    .line 1514
    const/4 v11, 0x1

    .line 1515
    :cond_1a
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1518
    :pswitch_5
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1520
    const/4 v15, 0x1

    if-le v11, v15, :cond_1b

    .line 1521
    const/4 v11, 0x1

    .line 1522
    :cond_1b
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1525
    :pswitch_6
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 1526
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 1529
    :pswitch_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 1530
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 1533
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1534
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1537
    :pswitch_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x23

    if-le v11, v15, :cond_2

    .line 1538
    const/16 v11, 0x23

    goto/16 :goto_1

    .line 1541
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1542
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1545
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x25

    if-le v11, v15, :cond_2

    .line 1546
    const/16 v11, 0x25

    goto/16 :goto_1

    .line 1549
    :pswitch_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1550
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1553
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x26

    if-le v11, v15, :cond_2

    .line 1554
    const/16 v11, 0x26

    goto/16 :goto_1

    .line 1557
    :pswitch_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1558
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1561
    :pswitch_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 1562
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 1565
    :pswitch_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2a

    if-le v11, v15, :cond_2

    .line 1566
    const/16 v11, 0x2a

    goto/16 :goto_1

    .line 1569
    :pswitch_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1570
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1573
    :pswitch_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2b

    if-le v11, v15, :cond_2

    .line 1574
    const/16 v11, 0x2b

    goto/16 :goto_1

    .line 1577
    :pswitch_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2c

    if-le v11, v15, :cond_2

    .line 1578
    const/16 v11, 0x2c

    goto/16 :goto_1

    .line 1581
    :pswitch_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1582
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1585
    :pswitch_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2e

    if-le v11, v15, :cond_2

    .line 1586
    const/16 v11, 0x2e

    goto/16 :goto_1

    .line 1589
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2f

    if-le v11, v15, :cond_2

    .line 1590
    const/16 v11, 0x2f

    goto/16 :goto_1

    .line 1593
    :pswitch_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x30

    if-le v11, v15, :cond_2

    .line 1594
    const/16 v11, 0x30

    goto/16 :goto_1

    .line 1597
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1599
    const/16 v15, 0x38

    if-le v11, v15, :cond_1c

    .line 1600
    const/16 v11, 0x38

    .line 1601
    :cond_1c
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1604
    :pswitch_19
    const-wide/high16 v15, 0x3fe000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1606
    const/16 v15, 0x3c

    if-le v11, v15, :cond_1d

    .line 1607
    const/16 v11, 0x3c

    .line 1608
    :cond_1d
    const/16 v15, 0x27

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1611
    :pswitch_1a
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1613
    const/16 v15, 0x3c

    if-le v11, v15, :cond_1e

    .line 1614
    const/16 v11, 0x3c

    .line 1615
    :cond_1e
    const/16 v15, 0x27

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1618
    :pswitch_1b
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1619
    const/16 v15, 0x29

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1622
    :pswitch_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1623
    const/16 v15, 0x2b

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1626
    :pswitch_1d
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1628
    const/16 v15, 0x3d

    if-le v11, v15, :cond_1f

    .line 1629
    const/16 v11, 0x3d

    .line 1630
    :cond_1f
    const/16 v15, 0xe

    const/16 v16, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1633
    :pswitch_1e
    const-wide v15, 0x280000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1634
    const/16 v15, 0x2e

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1637
    :pswitch_1f
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1639
    const/16 v15, 0x3d

    if-le v11, v15, :cond_20

    .line 1640
    const/16 v11, 0x3d

    .line 1641
    :cond_20
    const/16 v15, 0x2e

    const/16 v16, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1644
    :pswitch_20
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1645
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1648
    :pswitch_21
    const-wide v15, -0x400002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1649
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1652
    :pswitch_22
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1653
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1656
    :pswitch_23
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3e

    if-le v11, v15, :cond_2

    .line 1657
    const/16 v11, 0x3e

    goto/16 :goto_1

    .line 1660
    :pswitch_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1661
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1664
    :pswitch_25
    const-wide v15, -0x8000002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1665
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1668
    :pswitch_26
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1669
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1672
    :pswitch_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3e

    if-le v11, v15, :cond_2

    .line 1673
    const/16 v11, 0x3e

    goto/16 :goto_1

    .line 1676
    :pswitch_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1678
    const/16 v15, 0x3c

    if-le v11, v15, :cond_21

    .line 1679
    const/16 v11, 0x3c

    .line 1680
    :cond_21
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1683
    :pswitch_29
    const-wide/high16 v15, 0xff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1685
    const/16 v15, 0x3c

    if-le v11, v15, :cond_22

    .line 1686
    const/16 v11, 0x3c

    .line 1687
    :cond_22
    const/16 v15, 0x3b

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1690
    :pswitch_2a
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1692
    const/16 v15, 0x3c

    if-le v11, v15, :cond_23

    .line 1693
    const/16 v11, 0x3c

    .line 1694
    :cond_23
    const/16 v15, 0x3d

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1697
    :pswitch_2b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1698
    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_1

    .line 1701
    :pswitch_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1702
    const/16 v15, 0x40

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1705
    :pswitch_2d
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1706
    const/16 v15, 0x40

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1709
    :pswitch_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1710
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1713
    :pswitch_2f
    const-wide v15, -0x840000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1714
    const/16 v15, 0x43

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1717
    :pswitch_30
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1718
    const/16 v15, 0x43

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1721
    :pswitch_31
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x2

    if-le v11, v15, :cond_2

    .line 1722
    const/4 v11, 0x2

    goto/16 :goto_1

    .line 1725
    :pswitch_32
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1727
    const/4 v15, 0x3

    if-le v11, v15, :cond_24

    .line 1728
    const/4 v11, 0x3

    .line 1729
    :cond_24
    const/16 v15, 0x14

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1732
    :pswitch_33
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1734
    const/4 v15, 0x3

    if-le v11, v15, :cond_25

    .line 1735
    const/4 v11, 0x3

    .line 1736
    :cond_25
    const/16 v15, 0x14

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1739
    :pswitch_34
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 1740
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 1743
    :pswitch_35
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 1744
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 1747
    :pswitch_36
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1748
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x48

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1754
    .end local v12    # "l":J
    :cond_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_3d

    .line 1756
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v12, v15, v17

    .line 1759
    .restart local v12    # "l":J
    :cond_27
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 1985
    :cond_28
    :goto_5
    :pswitch_37
    if-ne v10, v14, :cond_27

    goto/16 :goto_2

    .line 1762
    :pswitch_38
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 1764
    const/16 v15, 0x38

    if-le v11, v15, :cond_29

    .line 1765
    const/16 v11, 0x38

    .line 1766
    :cond_29
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 1768
    :cond_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x71

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1770
    const/16 v15, 0x25

    if-le v11, v15, :cond_28

    .line 1771
    const/16 v11, 0x25

    goto :goto_5

    .line 1775
    :pswitch_39
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2c

    .line 1777
    const/16 v15, 0x38

    if-le v11, v15, :cond_2b

    .line 1778
    const/16 v11, 0x38

    .line 1779
    :cond_2b
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 1781
    :cond_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2d

    .line 1782
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4c

    aput v17, v15, v16

    goto :goto_5

    .line 1783
    :cond_2d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1785
    const/16 v15, 0x26

    if-le v11, v15, :cond_28

    .line 1786
    const/16 v11, 0x26

    goto :goto_5

    .line 1790
    :pswitch_3a
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_30

    .line 1792
    const/16 v15, 0x38

    if-le v11, v15, :cond_2e

    .line 1793
    const/16 v11, 0x38

    .line 1794
    :cond_2e
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 1798
    :cond_2f
    :goto_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_31

    .line 1799
    const/16 v15, 0x17

    const/16 v16, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1796
    :cond_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2f

    .line 1797
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto :goto_6

    .line 1800
    :cond_31
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_32

    .line 1801
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1802
    :cond_32
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_33

    .line 1803
    const/16 v15, 0x1b

    const/16 v16, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1804
    :cond_33
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_34

    .line 1805
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1806
    :cond_34
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_35

    .line 1807
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1808
    :cond_35
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_36

    .line 1809
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1810
    :cond_36
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_37

    .line 1811
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1812
    :cond_37
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1813
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1816
    :pswitch_3b
    const/4 v15, 0x1

    if-le v11, v15, :cond_38

    .line 1817
    const/4 v11, 0x1

    .line 1818
    :cond_38
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1821
    :pswitch_3c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x23

    if-le v11, v15, :cond_28

    .line 1822
    const/16 v11, 0x23

    goto/16 :goto_5

    .line 1825
    :pswitch_3d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1826
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1829
    :pswitch_3e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1830
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1833
    :pswitch_3f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x24

    if-le v11, v15, :cond_28

    .line 1834
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 1837
    :pswitch_40
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1838
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1841
    :pswitch_41
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x72

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x24

    if-le v11, v15, :cond_28

    .line 1842
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 1845
    :pswitch_42
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1846
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1849
    :pswitch_43
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1850
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1853
    :pswitch_44
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2e

    if-le v11, v15, :cond_28

    .line 1854
    const/16 v11, 0x2e

    goto/16 :goto_5

    .line 1857
    :pswitch_45
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1858
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1c

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1861
    :pswitch_46
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1862
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1865
    :pswitch_47
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x76

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2f

    if-le v11, v15, :cond_28

    .line 1866
    const/16 v11, 0x2f

    goto/16 :goto_5

    .line 1869
    :pswitch_48
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x69

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1870
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x20

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1873
    :pswitch_49
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1874
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1877
    :pswitch_4a
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 1879
    const/16 v15, 0x38

    if-le v11, v15, :cond_39

    .line 1880
    const/16 v11, 0x38

    .line 1881
    :cond_39
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 1884
    :pswitch_4b
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 1886
    const/16 v15, 0x38

    if-le v11, v15, :cond_3a

    .line 1887
    const/16 v11, 0x38

    .line 1888
    :cond_3a
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 1891
    :pswitch_4c
    const-wide v15, 0x110000001100L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    const/16 v15, 0x3c

    if-le v11, v15, :cond_28

    .line 1892
    const/16 v11, 0x3c

    goto/16 :goto_5

    .line 1895
    :pswitch_4d
    const-wide v15, 0x2000000020L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 1896
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1899
    :pswitch_4e
    const-wide v15, 0x5400000054L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    const/16 v15, 0x3d

    if-le v11, v15, :cond_28

    .line 1900
    const/16 v11, 0x3d

    goto/16 :goto_5

    .line 1903
    :pswitch_4f
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 1904
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 1907
    :pswitch_50
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1908
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x33

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1911
    :pswitch_51
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 1914
    :pswitch_52
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 1915
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 1918
    :pswitch_53
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1919
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x38

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1922
    :pswitch_54
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 1925
    :pswitch_55
    const-wide v15, 0x100000001000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 1926
    const/16 v15, 0x3d

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 1929
    :pswitch_56
    const-wide v15, 0x7e0000007eL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 1931
    const/16 v15, 0x3c

    if-le v11, v15, :cond_3b

    .line 1932
    const/16 v11, 0x3c

    .line 1933
    :cond_3b
    const/16 v15, 0x3d

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 1936
    :pswitch_57
    const/16 v15, 0x40

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 1940
    :pswitch_58
    const/16 v15, 0x43

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 1943
    :pswitch_59
    const/4 v15, 0x3

    if-le v11, v15, :cond_3c

    .line 1944
    const/4 v11, 0x3

    .line 1945
    :cond_3c
    const/16 v15, 0x14

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1948
    :pswitch_5a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1949
    const/16 v15, 0x1b

    const/16 v16, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1952
    :pswitch_5b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x30

    if-le v11, v15, :cond_28

    .line 1953
    const/16 v11, 0x30

    goto/16 :goto_5

    .line 1956
    :pswitch_5c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1957
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4c

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1960
    :pswitch_5d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1961
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1964
    :pswitch_5e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x29

    if-le v11, v15, :cond_28

    .line 1965
    const/16 v11, 0x29

    goto/16 :goto_5

    .line 1968
    :pswitch_5f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2a

    if-le v11, v15, :cond_28

    .line 1969
    const/16 v11, 0x2a

    goto/16 :goto_5

    .line 1972
    :pswitch_60
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1973
    const/16 v15, 0x17

    const/16 v16, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1976
    :pswitch_61
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2b

    if-le v11, v15, :cond_28

    .line 1977
    const/16 v11, 0x2b

    goto/16 :goto_5

    .line 1980
    :pswitch_62
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2c

    if-le v11, v15, :cond_28

    .line 1981
    const/16 v11, 0x2c

    goto/16 :goto_5

    .line 1989
    .end local v12    # "l":J
    :cond_3d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shr-int/lit8 v2, v15, 0x8

    .line 1990
    .local v2, "hiByte":I
    shr-int/lit8 v3, v2, 0x6

    .line 1991
    .local v3, "i1":I
    const-wide/16 v15, 0x1

    and-int/lit8 v17, v2, 0x3f

    shl-long v5, v15, v17

    .line 1992
    .local v5, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v4, v15, 0x6

    .line 1993
    .local v4, "i2":I
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v7, v15, v17

    .line 1996
    .local v7, "l2":J
    :cond_3e
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 2033
    :cond_3f
    :goto_7
    if-ne v10, v14, :cond_3e

    goto/16 :goto_2

    .line 1999
    :sswitch_0
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 2001
    const/4 v15, 0x1

    if-le v11, v15, :cond_40

    .line 2002
    const/4 v11, 0x1

    .line 2003
    :cond_40
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 2007
    :sswitch_1
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 2008
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 2012
    :sswitch_2
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 2013
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 2016
    :sswitch_3
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 2017
    const/16 v15, 0x40

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 2021
    :sswitch_4
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 2022
    const/16 v15, 0x43

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 2025
    :sswitch_5
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 2027
    const/4 v15, 0x3

    if-le v11, v15, :cond_41

    .line 2028
    const/4 v11, 0x3

    .line 2029
    :cond_41
    const/16 v15, 0x14

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 2044
    .end local v2    # "hiByte":I
    .end local v3    # "i1":I
    .end local v4    # "i2":I
    .end local v5    # "l1":J
    .end local v7    # "l2":J
    :cond_42
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v15}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2045
    :catch_0
    move-exception v9

    .local v9, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 1428
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_3
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_1
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_1
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_0
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 1759
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_3a
        :pswitch_37
        :pswitch_37
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_37
        :pswitch_37
        :pswitch_38
        :pswitch_43
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_37
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_37
        :pswitch_4a
        :pswitch_4b
        :pswitch_37
        :pswitch_37
        :pswitch_4c
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_4d
        :pswitch_37
        :pswitch_37
        :pswitch_4e
        :pswitch_37
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_37
        :pswitch_37
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_55
        :pswitch_56
        :pswitch_37
        :pswitch_37
        :pswitch_57
        :pswitch_37
        :pswitch_58
        :pswitch_58
        :pswitch_37
        :pswitch_37
        :pswitch_59
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_5a
        :pswitch_39
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
    .end packed-switch

    .line 1996
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x31 -> :sswitch_1
        0x33 -> :sswitch_1
        0x36 -> :sswitch_2
        0x38 -> :sswitch_2
        0x40 -> :sswitch_3
        0x42 -> :sswitch_4
        0x43 -> :sswitch_4
        0x46 -> :sswitch_5
    .end sparse-switch
.end method

.method private jjMoveNfa_2(II)I
    .locals 19
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 404
    const/4 v14, 0x0

    .line 405
    .local v14, "startsAt":I
    const/16 v15, 0x54

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    .line 406
    const/4 v10, 0x1

    .line 407
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 408
    const v11, 0x7fffffff

    .line 411
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 412
    invoke-direct/range {p0 .. p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInitRounds()V

    .line 413
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_26

    .line 415
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    shl-long v12, v15, v17

    .line 418
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 742
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 1025
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 1027
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1028
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 1029
    const v11, 0x7fffffff

    .line 1031
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 1032
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x54

    if-ne v10, v14, :cond_42

    .line 1035
    :goto_3
    return p2

    .line 422
    .restart local v12    # "l":J
    :pswitch_1
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 424
    const/16 v15, 0x38

    if-le v11, v15, :cond_4

    .line 425
    const/16 v11, 0x38

    .line 426
    :cond_4
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 429
    :pswitch_2
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 431
    const/16 v15, 0x38

    if-le v11, v15, :cond_5

    .line 432
    const/16 v11, 0x38

    .line 433
    :cond_5
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_1

    .line 436
    :pswitch_3
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_8

    .line 437
    const/16 v15, 0x29

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    .line 470
    :cond_6
    :goto_4
    const-wide/high16 v15, 0x3fe000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_14

    .line 472
    const/16 v15, 0x3c

    if-le v11, v15, :cond_7

    .line 473
    const/16 v11, 0x3c

    .line 474
    :cond_7
    const/16 v15, 0x27

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 438
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 439
    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 440
    :cond_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 441
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 442
    :cond_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    .line 443
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 444
    :cond_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    .line 446
    const/16 v15, 0x38

    if-le v11, v15, :cond_c

    .line 447
    const/16 v11, 0x38

    .line 448
    :cond_c
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 450
    :cond_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 452
    const/16 v15, 0x30

    if-le v11, v15, :cond_6

    .line 453
    const/16 v11, 0x30

    goto/16 :goto_4

    .line 455
    :cond_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 457
    const/16 v15, 0x2e

    if-le v11, v15, :cond_6

    .line 458
    const/16 v11, 0x2e

    goto/16 :goto_4

    .line 460
    :cond_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_10

    .line 461
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_4

    .line 462
    :cond_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 463
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_4

    .line 464
    :cond_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_12

    .line 465
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_4

    .line 466
    :cond_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_13

    .line 467
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_4

    .line 468
    :cond_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 469
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x0

    aput v17, v15, v16

    goto/16 :goto_4

    .line 476
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ne v15, v0, :cond_16

    .line 478
    const/16 v15, 0x3c

    if-le v11, v15, :cond_15

    .line 479
    const/16 v11, 0x3c

    .line 480
    :cond_15
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 482
    :cond_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_17

    .line 484
    const/16 v15, 0x2f

    if-le v11, v15, :cond_2

    .line 485
    const/16 v11, 0x2f

    goto/16 :goto_1

    .line 487
    :cond_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_18

    .line 489
    const/16 v15, 0x2b

    if-le v11, v15, :cond_2

    .line 490
    const/16 v11, 0x2b

    goto/16 :goto_1

    .line 492
    :cond_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_19

    .line 494
    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 495
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 497
    :cond_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 498
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 501
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 503
    const/4 v15, 0x1

    if-le v11, v15, :cond_1a

    .line 504
    const/4 v11, 0x1

    .line 505
    :cond_1a
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 508
    :pswitch_5
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 510
    const/4 v15, 0x1

    if-le v11, v15, :cond_1b

    .line 511
    const/4 v11, 0x1

    .line 512
    :cond_1b
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 515
    :pswitch_6
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 516
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 519
    :pswitch_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 520
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 523
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 524
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto/16 :goto_1

    .line 527
    :pswitch_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x23

    if-le v11, v15, :cond_2

    .line 528
    const/16 v11, 0x23

    goto/16 :goto_1

    .line 531
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 532
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_1

    .line 535
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x25

    if-le v11, v15, :cond_2

    .line 536
    const/16 v11, 0x25

    goto/16 :goto_1

    .line 539
    :pswitch_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 540
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_1

    .line 543
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x26

    if-le v11, v15, :cond_2

    .line 544
    const/16 v11, 0x26

    goto/16 :goto_1

    .line 547
    :pswitch_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 548
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 551
    :pswitch_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 552
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 555
    :pswitch_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2a

    if-le v11, v15, :cond_2

    .line 556
    const/16 v11, 0x2a

    goto/16 :goto_1

    .line 559
    :pswitch_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 560
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_1

    .line 563
    :pswitch_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2b

    if-le v11, v15, :cond_2

    .line 564
    const/16 v11, 0x2b

    goto/16 :goto_1

    .line 567
    :pswitch_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2c

    if-le v11, v15, :cond_2

    .line 568
    const/16 v11, 0x2c

    goto/16 :goto_1

    .line 571
    :pswitch_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 572
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_1

    .line 575
    :pswitch_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2e

    if-le v11, v15, :cond_2

    .line 576
    const/16 v11, 0x2e

    goto/16 :goto_1

    .line 579
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2f

    if-le v11, v15, :cond_2

    .line 580
    const/16 v11, 0x2f

    goto/16 :goto_1

    .line 583
    :pswitch_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x30

    if-le v11, v15, :cond_2

    .line 584
    const/16 v11, 0x30

    goto/16 :goto_1

    .line 587
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 589
    const/16 v15, 0x38

    if-le v11, v15, :cond_1c

    .line 590
    const/16 v11, 0x38

    .line 591
    :cond_1c
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 594
    :pswitch_19
    const-wide/high16 v15, 0x3fe000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 596
    const/16 v15, 0x3c

    if-le v11, v15, :cond_1d

    .line 597
    const/16 v11, 0x3c

    .line 598
    :cond_1d
    const/16 v15, 0x27

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 601
    :pswitch_1a
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 603
    const/16 v15, 0x3c

    if-le v11, v15, :cond_1e

    .line 604
    const/16 v11, 0x3c

    .line 605
    :cond_1e
    const/16 v15, 0x27

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 608
    :pswitch_1b
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 609
    const/16 v15, 0x29

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 612
    :pswitch_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 613
    const/16 v15, 0x2b

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 616
    :pswitch_1d
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 618
    const/16 v15, 0x3d

    if-le v11, v15, :cond_1f

    .line 619
    const/16 v11, 0x3d

    .line 620
    :cond_1f
    const/16 v15, 0xe

    const/16 v16, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 623
    :pswitch_1e
    const-wide v15, 0x280000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 624
    const/16 v15, 0x2e

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 627
    :pswitch_1f
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 629
    const/16 v15, 0x3d

    if-le v11, v15, :cond_20

    .line 630
    const/16 v11, 0x3d

    .line 631
    :cond_20
    const/16 v15, 0x2e

    const/16 v16, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 634
    :pswitch_20
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 635
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 638
    :pswitch_21
    const-wide v15, -0x400002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 639
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 642
    :pswitch_22
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 643
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 646
    :pswitch_23
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3e

    if-le v11, v15, :cond_2

    .line 647
    const/16 v11, 0x3e

    goto/16 :goto_1

    .line 650
    :pswitch_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 651
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 654
    :pswitch_25
    const-wide v15, -0x8000002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 655
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 658
    :pswitch_26
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 659
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 662
    :pswitch_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3e

    if-le v11, v15, :cond_2

    .line 663
    const/16 v11, 0x3e

    goto/16 :goto_1

    .line 666
    :pswitch_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 668
    const/16 v15, 0x3c

    if-le v11, v15, :cond_21

    .line 669
    const/16 v11, 0x3c

    .line 670
    :cond_21
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 673
    :pswitch_29
    const-wide/high16 v15, 0xff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 675
    const/16 v15, 0x3c

    if-le v11, v15, :cond_22

    .line 676
    const/16 v11, 0x3c

    .line 677
    :cond_22
    const/16 v15, 0x3b

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 680
    :pswitch_2a
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 682
    const/16 v15, 0x3c

    if-le v11, v15, :cond_23

    .line 683
    const/16 v11, 0x3c

    .line 684
    :cond_23
    const/16 v15, 0x3d

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 687
    :pswitch_2b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 688
    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_1

    .line 691
    :pswitch_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 692
    const/16 v15, 0x40

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 695
    :pswitch_2d
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 696
    const/16 v15, 0x40

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 699
    :pswitch_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 700
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 703
    :pswitch_2f
    const-wide v15, -0x840000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 704
    const/16 v15, 0x43

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 707
    :pswitch_30
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 708
    const/16 v15, 0x43

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 711
    :pswitch_31
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x2

    if-le v11, v15, :cond_2

    .line 712
    const/4 v11, 0x2

    goto/16 :goto_1

    .line 715
    :pswitch_32
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 717
    const/4 v15, 0x3

    if-le v11, v15, :cond_24

    .line 718
    const/4 v11, 0x3

    .line 719
    :cond_24
    const/16 v15, 0x14

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 722
    :pswitch_33
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 724
    const/4 v15, 0x3

    if-le v11, v15, :cond_25

    .line 725
    const/4 v11, 0x3

    .line 726
    :cond_25
    const/16 v15, 0x14

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 729
    :pswitch_34
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 730
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 733
    :pswitch_35
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 734
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 737
    :pswitch_36
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 738
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x48

    aput v17, v15, v16

    goto/16 :goto_1

    .line 744
    .end local v12    # "l":J
    :cond_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_3d

    .line 746
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v12, v15, v17

    .line 749
    .restart local v12    # "l":J
    :cond_27
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 975
    :cond_28
    :goto_5
    :pswitch_37
    if-ne v10, v14, :cond_27

    goto/16 :goto_2

    .line 752
    :pswitch_38
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2a

    .line 754
    const/16 v15, 0x38

    if-le v11, v15, :cond_29

    .line 755
    const/16 v11, 0x38

    .line 756
    :cond_29
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 758
    :cond_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x71

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 760
    const/16 v15, 0x25

    if-le v11, v15, :cond_28

    .line 761
    const/16 v11, 0x25

    goto :goto_5

    .line 765
    :pswitch_39
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2c

    .line 767
    const/16 v15, 0x38

    if-le v11, v15, :cond_2b

    .line 768
    const/16 v11, 0x38

    .line 769
    :cond_2b
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 771
    :cond_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2d

    .line 772
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4c

    aput v17, v15, v16

    goto :goto_5

    .line 773
    :cond_2d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 775
    const/16 v15, 0x26

    if-le v11, v15, :cond_28

    .line 776
    const/16 v11, 0x26

    goto :goto_5

    .line 780
    :pswitch_3a
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_30

    .line 782
    const/16 v15, 0x38

    if-le v11, v15, :cond_2e

    .line 783
    const/16 v11, 0x38

    .line 784
    :cond_2e
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 788
    :cond_2f
    :goto_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_31

    .line 789
    const/16 v15, 0x17

    const/16 v16, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 786
    :cond_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2f

    .line 787
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto :goto_6

    .line 790
    :cond_31
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_32

    .line 791
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 792
    :cond_32
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_33

    .line 793
    const/16 v15, 0x1b

    const/16 v16, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 794
    :cond_33
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_34

    .line 795
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 796
    :cond_34
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_35

    .line 797
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 798
    :cond_35
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_36

    .line 799
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 800
    :cond_36
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_37

    .line 801
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 802
    :cond_37
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 803
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 806
    :pswitch_3b
    const/4 v15, 0x1

    if-le v11, v15, :cond_38

    .line 807
    const/4 v11, 0x1

    .line 808
    :cond_38
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 811
    :pswitch_3c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x23

    if-le v11, v15, :cond_28

    .line 812
    const/16 v11, 0x23

    goto/16 :goto_5

    .line 815
    :pswitch_3d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 816
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 819
    :pswitch_3e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 820
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 823
    :pswitch_3f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x24

    if-le v11, v15, :cond_28

    .line 824
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 827
    :pswitch_40
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 828
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_5

    .line 831
    :pswitch_41
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x72

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x24

    if-le v11, v15, :cond_28

    .line 832
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 835
    :pswitch_42
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 836
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 839
    :pswitch_43
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 840
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 843
    :pswitch_44
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2e

    if-le v11, v15, :cond_28

    .line 844
    const/16 v11, 0x2e

    goto/16 :goto_5

    .line 847
    :pswitch_45
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 848
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1c

    aput v17, v15, v16

    goto/16 :goto_5

    .line 851
    :pswitch_46
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 852
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 855
    :pswitch_47
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x76

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2f

    if-le v11, v15, :cond_28

    .line 856
    const/16 v11, 0x2f

    goto/16 :goto_5

    .line 859
    :pswitch_48
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x69

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 860
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x20

    aput v17, v15, v16

    goto/16 :goto_5

    .line 863
    :pswitch_49
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 864
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 867
    :pswitch_4a
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 869
    const/16 v15, 0x38

    if-le v11, v15, :cond_39

    .line 870
    const/16 v11, 0x38

    .line 871
    :cond_39
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 874
    :pswitch_4b
    const-wide v15, 0x7fffffe87ffffffL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 876
    const/16 v15, 0x38

    if-le v11, v15, :cond_3a

    .line 877
    const/16 v11, 0x38

    .line 878
    :cond_3a
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 881
    :pswitch_4c
    const-wide v15, 0x110000001100L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    const/16 v15, 0x3c

    if-le v11, v15, :cond_28

    .line 882
    const/16 v11, 0x3c

    goto/16 :goto_5

    .line 885
    :pswitch_4d
    const-wide v15, 0x2000000020L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 886
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 889
    :pswitch_4e
    const-wide v15, 0x5400000054L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    const/16 v15, 0x3d

    if-le v11, v15, :cond_28

    .line 890
    const/16 v11, 0x3d

    goto/16 :goto_5

    .line 893
    :pswitch_4f
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 894
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 897
    :pswitch_50
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 898
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x33

    aput v17, v15, v16

    goto/16 :goto_5

    .line 901
    :pswitch_51
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 904
    :pswitch_52
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 905
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 908
    :pswitch_53
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 909
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x38

    aput v17, v15, v16

    goto/16 :goto_5

    .line 912
    :pswitch_54
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 915
    :pswitch_55
    const-wide v15, 0x100000001000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 916
    const/16 v15, 0x3d

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 919
    :pswitch_56
    const-wide v15, 0x7e0000007eL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_28

    .line 921
    const/16 v15, 0x3c

    if-le v11, v15, :cond_3b

    .line 922
    const/16 v11, 0x3c

    .line 923
    :cond_3b
    const/16 v15, 0x3d

    const/16 v16, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 926
    :pswitch_57
    const/16 v15, 0x40

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 930
    :pswitch_58
    const/16 v15, 0x43

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 933
    :pswitch_59
    const/4 v15, 0x3

    if-le v11, v15, :cond_3c

    .line 934
    const/4 v11, 0x3

    .line 935
    :cond_3c
    const/16 v15, 0x14

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 938
    :pswitch_5a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 939
    const/16 v15, 0x1b

    const/16 v16, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 942
    :pswitch_5b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x30

    if-le v11, v15, :cond_28

    .line 943
    const/16 v11, 0x30

    goto/16 :goto_5

    .line 946
    :pswitch_5c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 947
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x4c

    aput v17, v15, v16

    goto/16 :goto_5

    .line 950
    :pswitch_5d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 951
    const/16 v15, 0x19

    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 954
    :pswitch_5e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x29

    if-le v11, v15, :cond_28

    .line 955
    const/16 v11, 0x29

    goto/16 :goto_5

    .line 958
    :pswitch_5f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2a

    if-le v11, v15, :cond_28

    .line 959
    const/16 v11, 0x2a

    goto/16 :goto_5

    .line 962
    :pswitch_60
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 963
    const/16 v15, 0x17

    const/16 v16, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 966
    :pswitch_61
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2b

    if-le v11, v15, :cond_28

    .line 967
    const/16 v11, 0x2b

    goto/16 :goto_5

    .line 970
    :pswitch_62
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    const/16 v15, 0x2c

    if-le v11, v15, :cond_28

    .line 971
    const/16 v11, 0x2c

    goto/16 :goto_5

    .line 979
    .end local v12    # "l":J
    :cond_3d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shr-int/lit8 v2, v15, 0x8

    .line 980
    .local v2, "hiByte":I
    shr-int/lit8 v3, v2, 0x6

    .line 981
    .local v3, "i1":I
    const-wide/16 v15, 0x1

    and-int/lit8 v17, v2, 0x3f

    shl-long v5, v15, v17

    .line 982
    .local v5, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v4, v15, 0x6

    .line 983
    .local v4, "i2":I
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v7, v15, v17

    .line 986
    .local v7, "l2":J
    :cond_3e
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 1023
    :cond_3f
    :goto_7
    if-ne v10, v14, :cond_3e

    goto/16 :goto_2

    .line 989
    :sswitch_0
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 991
    const/4 v15, 0x1

    if-le v11, v15, :cond_40

    .line 992
    const/4 v11, 0x1

    .line 993
    :cond_40
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 997
    :sswitch_1
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 998
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 1002
    :sswitch_2
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 1003
    const/4 v15, 0x2

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 1006
    :sswitch_3
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 1007
    const/16 v15, 0x40

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 1011
    :sswitch_4
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 1012
    const/16 v15, 0x43

    const/16 v16, 0x41

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 1015
    :sswitch_5
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_3f

    .line 1017
    const/4 v15, 0x3

    if-le v11, v15, :cond_41

    .line 1018
    const/4 v11, 0x3

    .line 1019
    :cond_41
    const/16 v15, 0x14

    const/16 v16, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 1034
    .end local v2    # "hiByte":I
    .end local v3    # "i1":I
    .end local v4    # "i2":I
    .end local v5    # "l1":J
    .end local v7    # "l2":J
    :cond_42
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v15}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1035
    :catch_0
    move-exception v9

    .local v9, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_3
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_1
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_1
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_0
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 749
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_3a
        :pswitch_37
        :pswitch_37
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_37
        :pswitch_37
        :pswitch_38
        :pswitch_43
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_37
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_37
        :pswitch_4a
        :pswitch_4b
        :pswitch_37
        :pswitch_37
        :pswitch_4c
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_4d
        :pswitch_37
        :pswitch_37
        :pswitch_4e
        :pswitch_37
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_37
        :pswitch_37
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_55
        :pswitch_56
        :pswitch_37
        :pswitch_37
        :pswitch_57
        :pswitch_37
        :pswitch_58
        :pswitch_58
        :pswitch_37
        :pswitch_37
        :pswitch_59
        :pswitch_37
        :pswitch_37
        :pswitch_37
        :pswitch_5a
        :pswitch_39
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
    .end packed-switch

    .line 986
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x31 -> :sswitch_1
        0x33 -> :sswitch_1
        0x36 -> :sswitch_2
        0x38 -> :sswitch_2
        0x40 -> :sswitch_3
        0x42 -> :sswitch_4
        0x43 -> :sswitch_4
        0x46 -> :sswitch_5
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_0()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2149
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 2212
    const/4 v0, 0x5

    invoke-direct {p0, v0, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_0(II)I

    move-result v0

    :goto_0
    return v0

    .line 2152
    :sswitch_0
    const-wide v0, 0x10000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto :goto_0

    .line 2154
    :sswitch_1
    const/16 v0, 0x35

    const/4 v1, 0x6

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v0

    goto :goto_0

    .line 2156
    :sswitch_2
    const/16 v0, 0x17

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2158
    :sswitch_3
    const/16 v0, 0x18

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2160
    :sswitch_4
    const/16 v0, 0x33

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2162
    :sswitch_5
    const/16 v0, 0x31

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2164
    :sswitch_6
    const/16 v0, 0x1f

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2166
    :sswitch_7
    const/16 v0, 0x32

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2168
    :sswitch_8
    const/16 v0, 0x20

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2170
    :sswitch_9
    const/16 v0, 0x1e

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2172
    :sswitch_a
    const/16 v0, 0x1d

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2174
    :sswitch_b
    const/16 v0, 0x2d

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2175
    const-wide v0, 0x8000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto :goto_0

    .line 2177
    :sswitch_c
    const/16 v0, 0x21

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2178
    const-wide v0, 0x400000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto :goto_0

    .line 2180
    :sswitch_d
    const/16 v0, 0x1b

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2182
    :sswitch_e
    const/16 v0, 0x1c

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2184
    :sswitch_f
    const/16 v0, 0x37

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 2186
    :sswitch_10
    const-wide/32 v0, 0x10400

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2188
    :sswitch_11
    const-wide/32 v0, 0x101800

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2190
    :sswitch_12
    const-wide/16 v0, 0x200

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2192
    :sswitch_13
    const-wide/32 v0, 0x44000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2194
    :sswitch_14
    const-wide/32 v0, 0x200000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2196
    :sswitch_15
    const-wide/32 v0, 0x20000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2198
    :sswitch_16
    const-wide/32 v0, 0x80000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2200
    :sswitch_17
    const-wide/32 v0, 0x8000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2202
    :sswitch_18
    const-wide/16 v0, 0x2000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2204
    :sswitch_19
    const/16 v0, 0x19

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 2206
    :sswitch_1a
    const/16 v0, 0x36

    const/16 v1, 0xb

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v0

    goto/16 :goto_0

    .line 2208
    :sswitch_1b
    const/16 v0, 0x1a

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 2210
    :sswitch_1c
    const/16 v0, 0x34

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 2149
    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x26 -> :sswitch_1
        0x28 -> :sswitch_2
        0x29 -> :sswitch_3
        0x2a -> :sswitch_4
        0x2b -> :sswitch_5
        0x2c -> :sswitch_6
        0x2d -> :sswitch_7
        0x2e -> :sswitch_8
        0x3a -> :sswitch_9
        0x3b -> :sswitch_a
        0x3d -> :sswitch_b
        0x3f -> :sswitch_c
        0x5b -> :sswitch_d
        0x5d -> :sswitch_e
        0x5e -> :sswitch_f
        0x65 -> :sswitch_10
        0x66 -> :sswitch_11
        0x69 -> :sswitch_12
        0x6e -> :sswitch_13
        0x72 -> :sswitch_14
        0x73 -> :sswitch_15
        0x74 -> :sswitch_16
        0x76 -> :sswitch_17
        0x77 -> :sswitch_18
        0x7b -> :sswitch_19
        0x7c -> :sswitch_1a
        0x7d -> :sswitch_1b
        0x7e -> :sswitch_1c
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_1()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1139
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 1202
    const/4 v0, 0x5

    invoke-direct {p0, v0, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    :goto_0
    return v0

    .line 1142
    :sswitch_0
    const-wide v0, 0x10000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 1144
    :sswitch_1
    const/16 v0, 0x35

    const/4 v1, 0x6

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v0

    goto :goto_0

    .line 1146
    :sswitch_2
    const/16 v0, 0x17

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1148
    :sswitch_3
    const/16 v0, 0x18

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1150
    :sswitch_4
    const/16 v0, 0x33

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1152
    :sswitch_5
    const/16 v0, 0x31

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1154
    :sswitch_6
    const/16 v0, 0x1f

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1156
    :sswitch_7
    const/16 v0, 0x32

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1158
    :sswitch_8
    const/16 v0, 0x20

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1160
    :sswitch_9
    const/16 v0, 0x1e

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1162
    :sswitch_a
    const/16 v0, 0x1d

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1164
    :sswitch_b
    const/16 v0, 0x2d

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1165
    const-wide v0, 0x8000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 1167
    :sswitch_c
    const/16 v0, 0x21

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1168
    const-wide v0, 0x400000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 1170
    :sswitch_d
    const/16 v0, 0x1b

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1172
    :sswitch_e
    const/16 v0, 0x1c

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1174
    :sswitch_f
    const/16 v0, 0x37

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1176
    :sswitch_10
    const-wide/32 v0, 0x10400

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1178
    :sswitch_11
    const-wide/32 v0, 0x101800

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1180
    :sswitch_12
    const-wide/32 v0, 0x400200

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1182
    :sswitch_13
    const-wide/32 v0, 0x44000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1184
    :sswitch_14
    const-wide/32 v0, 0x200000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1186
    :sswitch_15
    const-wide/32 v0, 0x20000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1188
    :sswitch_16
    const-wide/32 v0, 0x80000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1190
    :sswitch_17
    const-wide/32 v0, 0x8000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1192
    :sswitch_18
    const-wide/16 v0, 0x2000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1194
    :sswitch_19
    const/16 v0, 0x19

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1196
    :sswitch_1a
    const/16 v0, 0x36

    const/16 v1, 0xb

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v0

    goto/16 :goto_0

    .line 1198
    :sswitch_1b
    const/16 v0, 0x1a

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1200
    :sswitch_1c
    const/16 v0, 0x34

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1139
    nop

    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x26 -> :sswitch_1
        0x28 -> :sswitch_2
        0x29 -> :sswitch_3
        0x2a -> :sswitch_4
        0x2b -> :sswitch_5
        0x2c -> :sswitch_6
        0x2d -> :sswitch_7
        0x2e -> :sswitch_8
        0x3a -> :sswitch_9
        0x3b -> :sswitch_a
        0x3d -> :sswitch_b
        0x3f -> :sswitch_c
        0x5b -> :sswitch_d
        0x5d -> :sswitch_e
        0x5e -> :sswitch_f
        0x65 -> :sswitch_10
        0x66 -> :sswitch_11
        0x69 -> :sswitch_12
        0x6e -> :sswitch_13
        0x72 -> :sswitch_14
        0x73 -> :sswitch_15
        0x74 -> :sswitch_16
        0x76 -> :sswitch_17
        0x77 -> :sswitch_18
        0x7b -> :sswitch_19
        0x7c -> :sswitch_1a
        0x7d -> :sswitch_1b
        0x7e -> :sswitch_1c
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_2()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 121
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 184
    const/4 v0, 0x5

    invoke-direct {p0, v0, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    :goto_0
    return v0

    .line 124
    :sswitch_0
    const-wide v0, 0x10000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 126
    :sswitch_1
    const/16 v0, 0x35

    const/4 v1, 0x6

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v0

    goto :goto_0

    .line 128
    :sswitch_2
    const/16 v0, 0x17

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 130
    :sswitch_3
    const/16 v0, 0x18

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 132
    :sswitch_4
    const/16 v0, 0x33

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 134
    :sswitch_5
    const/16 v0, 0x31

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 136
    :sswitch_6
    const/16 v0, 0x1f

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 138
    :sswitch_7
    const/16 v0, 0x32

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 140
    :sswitch_8
    const/16 v0, 0x20

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 142
    :sswitch_9
    const/16 v0, 0x1e

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 144
    :sswitch_a
    const/16 v0, 0x1d

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 146
    :sswitch_b
    const/16 v0, 0x2d

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 147
    const-wide v0, 0x8000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 149
    :sswitch_c
    const/16 v0, 0x21

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 150
    const-wide v0, 0x400000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 152
    :sswitch_d
    const/16 v0, 0x1b

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 154
    :sswitch_e
    const/16 v0, 0x1c

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 156
    :sswitch_f
    const/16 v0, 0x37

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 158
    :sswitch_10
    const-wide/32 v0, 0x10400

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 160
    :sswitch_11
    const-wide/32 v0, 0x101800

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 162
    :sswitch_12
    const-wide/16 v0, 0x200

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 164
    :sswitch_13
    const-wide/32 v0, 0x44000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 166
    :sswitch_14
    const-wide/32 v0, 0x200000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 168
    :sswitch_15
    const-wide/32 v0, 0x20000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 170
    :sswitch_16
    const-wide/32 v0, 0x80000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 172
    :sswitch_17
    const-wide/32 v0, 0x8000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 174
    :sswitch_18
    const-wide/16 v0, 0x2000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 176
    :sswitch_19
    const/16 v0, 0x19

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 178
    :sswitch_1a
    const/16 v0, 0x36

    const/16 v1, 0xb

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v0

    goto/16 :goto_0

    .line 180
    :sswitch_1b
    const/16 v0, 0x1a

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 182
    :sswitch_1c
    const/16 v0, 0x34

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 121
    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x26 -> :sswitch_1
        0x28 -> :sswitch_2
        0x29 -> :sswitch_3
        0x2a -> :sswitch_4
        0x2b -> :sswitch_5
        0x2c -> :sswitch_6
        0x2d -> :sswitch_7
        0x2e -> :sswitch_8
        0x3a -> :sswitch_9
        0x3b -> :sswitch_a
        0x3d -> :sswitch_b
        0x3f -> :sswitch_c
        0x5b -> :sswitch_d
        0x5d -> :sswitch_e
        0x5e -> :sswitch_f
        0x65 -> :sswitch_10
        0x66 -> :sswitch_11
        0x69 -> :sswitch_12
        0x6e -> :sswitch_13
        0x72 -> :sswitch_14
        0x73 -> :sswitch_15
        0x74 -> :sswitch_16
        0x76 -> :sswitch_17
        0x77 -> :sswitch_18
        0x7b -> :sswitch_19
        0x7c -> :sswitch_1a
        0x7d -> :sswitch_1b
        0x7e -> :sswitch_1c
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_0(J)I
    .locals 7
    .param p1, "active0"    # J

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 2217
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2222
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 2259
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 2218
    :catch_0
    move-exception v0

    .line 2219
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2225
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide v2, 0x400000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2226
    const/16 v2, 0x22

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 2229
    :sswitch_1
    const-wide/32 v1, 0x108000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2231
    :sswitch_2
    const-wide/32 v1, 0x204000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2233
    :sswitch_3
    const-wide/16 v2, 0x200

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2234
    const/16 v2, 0x9

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2237
    :sswitch_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2239
    :sswitch_5
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2241
    :sswitch_6
    const-wide/16 v1, 0x400

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2243
    :sswitch_7
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2245
    :sswitch_8
    const-wide/16 v1, 0x1800

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2247
    :sswitch_9
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2249
    :sswitch_a
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2251
    :sswitch_b
    const-wide v2, 0x8000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2252
    const/16 v2, 0x27

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 2253
    :cond_1
    const-wide v2, 0x10000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2254
    const/16 v2, 0x28

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto/16 :goto_0

    .line 2222
    :sswitch_data_0
    .sparse-switch
        0x3a -> :sswitch_0
        0x61 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x68 -> :sswitch_4
        0x69 -> :sswitch_5
        0x6c -> :sswitch_6
        0x6d -> :sswitch_7
        0x6f -> :sswitch_8
        0x72 -> :sswitch_9
        0x75 -> :sswitch_a
        0x7e -> :sswitch_b
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_1(J)I
    .locals 8
    .param p1, "active0"    # J

    .prologue
    const/16 v7, 0x25

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 1207
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1212
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 1253
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 1208
    :catch_0
    move-exception v0

    .line 1209
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1215
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide v2, 0x400000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1216
    const/16 v2, 0x22

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 1219
    :sswitch_1
    const-wide/32 v1, 0x108000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1221
    :sswitch_2
    const-wide/32 v1, 0x204000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1223
    :sswitch_3
    const-wide/16 v2, 0x200

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1224
    const/16 v2, 0x9

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1227
    :sswitch_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1229
    :sswitch_5
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1231
    :sswitch_6
    const-wide/16 v1, 0x400

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1233
    :sswitch_7
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1235
    :sswitch_8
    const-wide/32 v2, 0x400000

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1236
    const/16 v2, 0x16

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1239
    :sswitch_9
    const-wide/16 v1, 0x1800

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1241
    :sswitch_a
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1243
    :sswitch_b
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1245
    :sswitch_c
    const-wide v2, 0x8000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1246
    const/16 v2, 0x27

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto/16 :goto_0

    .line 1247
    :cond_1
    const-wide v2, 0x10000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1248
    const/16 v2, 0x28

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto/16 :goto_0

    .line 1212
    :sswitch_data_0
    .sparse-switch
        0x3a -> :sswitch_0
        0x61 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x68 -> :sswitch_4
        0x69 -> :sswitch_5
        0x6c -> :sswitch_6
        0x6d -> :sswitch_7
        0x6e -> :sswitch_8
        0x6f -> :sswitch_9
        0x72 -> :sswitch_a
        0x75 -> :sswitch_b
        0x7e -> :sswitch_c
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_2(J)I
    .locals 7
    .param p1, "active0"    # J

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 189
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 231
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 197
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide v2, 0x400000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 198
    const/16 v2, 0x22

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 201
    :sswitch_1
    const-wide/32 v1, 0x108000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 203
    :sswitch_2
    const-wide/32 v1, 0x204000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 205
    :sswitch_3
    const-wide/16 v2, 0x200

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 206
    const/16 v2, 0x9

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 209
    :sswitch_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 211
    :sswitch_5
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 213
    :sswitch_6
    const-wide/16 v1, 0x400

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 215
    :sswitch_7
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 217
    :sswitch_8
    const-wide/16 v1, 0x1800

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 219
    :sswitch_9
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 221
    :sswitch_a
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 223
    :sswitch_b
    const-wide v2, 0x8000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 224
    const/16 v2, 0x27

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 225
    :cond_1
    const-wide v2, 0x10000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 226
    const/16 v2, 0x28

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto/16 :goto_0

    .line 194
    :sswitch_data_0
    .sparse-switch
        0x3a -> :sswitch_0
        0x61 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x68 -> :sswitch_4
        0x69 -> :sswitch_5
        0x6c -> :sswitch_6
        0x6d -> :sswitch_7
        0x6f -> :sswitch_8
        0x72 -> :sswitch_9
        0x75 -> :sswitch_a
        0x7e -> :sswitch_b
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa2_0(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/16 v7, 0x25

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x2

    .line 2263
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 2264
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2302
    :goto_0
    return v1

    .line 2265
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2270
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 2302
    :cond_1
    :pswitch_0
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2266
    :catch_0
    move-exception v0

    .line 2267
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2273
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2275
    :pswitch_2
    const-wide/32 v1, 0x140000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2277
    :pswitch_3
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2279
    :pswitch_4
    const-wide/16 v2, 0x800

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 2281
    const/16 v2, 0xb

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2282
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 2286
    :cond_2
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2284
    :cond_3
    const-wide/32 v2, 0x8000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 2285
    const/16 v2, 0xf

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2288
    :pswitch_5
    const-wide/16 v1, 0x400

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2290
    :pswitch_6
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2292
    :pswitch_7
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2294
    :pswitch_8
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2295
    const/16 v2, 0xe

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2298
    :pswitch_9
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2270
    nop

    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa2_1(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/16 v7, 0x25

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x2

    .line 1257
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 1258
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1296
    :goto_0
    return v1

    .line 1259
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1264
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 1296
    :cond_1
    :pswitch_0
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1260
    :catch_0
    move-exception v0

    .line 1261
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1267
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1269
    :pswitch_2
    const-wide/32 v1, 0x140000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1271
    :pswitch_3
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1273
    :pswitch_4
    const-wide/16 v2, 0x800

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 1275
    const/16 v2, 0xb

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1276
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 1280
    :cond_2
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1278
    :cond_3
    const-wide/32 v2, 0x8000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1279
    const/16 v2, 0xf

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1282
    :pswitch_5
    const-wide/16 v1, 0x400

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1284
    :pswitch_6
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1286
    :pswitch_7
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1288
    :pswitch_8
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1289
    const/16 v2, 0xe

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1292
    :pswitch_9
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1264
    nop

    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa2_2(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/16 v7, 0x25

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x2

    .line 235
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 236
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 274
    :goto_0
    return v1

    .line 237
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 274
    :cond_1
    :pswitch_0
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 245
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 247
    :pswitch_2
    const-wide/32 v1, 0x140000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 249
    :pswitch_3
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 251
    :pswitch_4
    const-wide/16 v2, 0x800

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 253
    const/16 v2, 0xb

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 254
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 258
    :cond_2
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 256
    :cond_3
    const-wide/32 v2, 0x8000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 257
    const/16 v2, 0xf

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 260
    :pswitch_5
    const-wide/16 v1, 0x400

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 262
    :pswitch_6
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 264
    :pswitch_7
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 266
    :pswitch_8
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 267
    const/16 v2, 0xe

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 270
    :pswitch_9
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa3_0(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x2

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x3

    .line 2306
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 2307
    const/4 v1, 0x1

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2336
    :goto_0
    return v1

    .line 2308
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2313
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 2336
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2309
    :catch_0
    move-exception v0

    .line 2310
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2316
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v2, 0x400

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2317
    const/16 v2, 0xa

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2318
    :cond_1
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 2319
    const/16 v2, 0x11

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2320
    :cond_2
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 2321
    const/16 v2, 0x13

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2322
    :cond_3
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2324
    :sswitch_1
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 2325
    const/16 v2, 0x12

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2326
    :cond_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2328
    :sswitch_2
    const-wide/32 v1, 0x100000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2330
    :sswitch_3
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2332
    :sswitch_4
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2313
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x6c -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_3
        0x75 -> :sswitch_4
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa3_1(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x2

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x3

    .line 1300
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 1301
    const/4 v1, 0x1

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1330
    :goto_0
    return v1

    .line 1302
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1307
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 1330
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1303
    :catch_0
    move-exception v0

    .line 1304
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1310
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v2, 0x400

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1311
    const/16 v2, 0xa

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1312
    :cond_1
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1313
    const/16 v2, 0x11

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1314
    :cond_2
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 1315
    const/16 v2, 0x13

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1316
    :cond_3
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1318
    :sswitch_1
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 1319
    const/16 v2, 0x12

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1320
    :cond_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1322
    :sswitch_2
    const-wide/32 v1, 0x100000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1324
    :sswitch_3
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1326
    :sswitch_4
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1307
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x6c -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_3
        0x75 -> :sswitch_4
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa3_2(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x2

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x3

    .line 278
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 279
    const/4 v1, 0x1

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 308
    :goto_0
    return v1

    .line 280
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 308
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 288
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v2, 0x400

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 289
    const/16 v2, 0xa

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 290
    :cond_1
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 291
    const/16 v2, 0x11

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 292
    :cond_2
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 293
    const/16 v2, 0x13

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 294
    :cond_3
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 296
    :sswitch_1
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 297
    const/16 v2, 0x12

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 298
    :cond_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 300
    :sswitch_2
    const-wide/32 v1, 0x100000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 302
    :sswitch_3
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 304
    :sswitch_4
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 285
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x6c -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_3
        0x75 -> :sswitch_4
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa4_0(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x4

    .line 2340
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 2341
    const/4 v1, 0x2

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2366
    :goto_0
    return v1

    .line 2342
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2347
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 2366
    :cond_1
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2343
    :catch_0
    move-exception v0

    .line 2344
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2350
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2352
    :sswitch_1
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 2353
    const/16 v2, 0xd

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2354
    :cond_2
    const-wide/32 v2, 0x100000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2355
    const/16 v2, 0x14

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2358
    :sswitch_2
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2360
    :sswitch_3
    const-wide/32 v2, 0x10000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2361
    const/16 v2, 0x10

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2347
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_1
        0x72 -> :sswitch_2
        0x79 -> :sswitch_3
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa4_1(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x4

    .line 1334
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 1335
    const/4 v1, 0x2

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1360
    :goto_0
    return v1

    .line 1336
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1341
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 1360
    :cond_1
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1337
    :catch_0
    move-exception v0

    .line 1338
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1344
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1346
    :sswitch_1
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1347
    const/16 v2, 0xd

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1348
    :cond_2
    const-wide/32 v2, 0x100000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1349
    const/16 v2, 0x14

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1352
    :sswitch_2
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1354
    :sswitch_3
    const-wide/32 v2, 0x10000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1355
    const/16 v2, 0x10

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1341
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_1
        0x72 -> :sswitch_2
        0x79 -> :sswitch_3
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa4_2(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x4

    .line 312
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 313
    const/4 v1, 0x2

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 338
    :goto_0
    return v1

    .line 314
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 338
    :cond_1
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 315
    :catch_0
    move-exception v0

    .line 316
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 322
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_2(JJ)I

    move-result v1

    goto :goto_0

    .line 324
    :sswitch_1
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 325
    const/16 v2, 0xd

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 326
    :cond_2
    const-wide/32 v2, 0x100000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 327
    const/16 v2, 0x14

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 330
    :sswitch_2
    const-wide/32 v1, 0x200000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_2(JJ)I

    move-result v1

    goto :goto_0

    .line 332
    :sswitch_3
    const-wide/32 v2, 0x10000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 333
    const/16 v2, 0x10

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 319
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_1
        0x72 -> :sswitch_2
        0x79 -> :sswitch_3
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa5_0(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x5

    const/4 v4, 0x4

    .line 2370
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 2371
    const/4 v1, 0x3

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2388
    :goto_0
    return v1

    .line 2372
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2377
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 2388
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2373
    :catch_0
    move-exception v0

    .line 2374
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2380
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa6_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2382
    :sswitch_1
    const-wide/32 v2, 0x200000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 2383
    const/16 v2, 0x15

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2377
    :sswitch_data_0
    .sparse-switch
        0x63 -> :sswitch_0
        0x6e -> :sswitch_1
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa5_1(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x5

    const/4 v4, 0x4

    .line 1364
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 1365
    const/4 v1, 0x3

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1382
    :goto_0
    return v1

    .line 1366
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1371
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 1382
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1367
    :catch_0
    move-exception v0

    .line 1368
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1374
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa6_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1376
    :sswitch_1
    const-wide/32 v2, 0x200000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 1377
    const/16 v2, 0x15

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1371
    :sswitch_data_0
    .sparse-switch
        0x63 -> :sswitch_0
        0x6e -> :sswitch_1
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa5_2(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x5

    const/4 v4, 0x4

    .line 342
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 343
    const/4 v1, 0x3

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 360
    :goto_0
    return v1

    .line 344
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 360
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 352
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa6_2(JJ)I

    move-result v1

    goto :goto_0

    .line 354
    :sswitch_1
    const-wide/32 v2, 0x200000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 355
    const/16 v2, 0x15

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 349
    :sswitch_data_0
    .sparse-switch
        0x63 -> :sswitch_0
        0x6e -> :sswitch_1
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa6_0(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x6

    const/4 v4, 0x5

    .line 2392
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 2393
    const/4 v1, 0x4

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2408
    :goto_0
    return v1

    .line 2394
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2399
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 2408
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2395
    :catch_0
    move-exception v0

    .line 2396
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2402
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 2403
    const/16 v2, 0xc

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2399
    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa6_1(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x6

    const/4 v4, 0x5

    .line 1386
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 1387
    const/4 v1, 0x4

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1402
    :goto_0
    return v1

    .line 1388
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1393
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 1402
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1389
    :catch_0
    move-exception v0

    .line 1390
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1396
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 1397
    const/16 v2, 0xc

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1393
    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa6_2(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x6

    const/4 v4, 0x5

    .line 364
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 365
    const/4 v1, 0x4

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 380
    :goto_0
    return v1

    .line 366
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 380
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 367
    :catch_0
    move-exception v0

    .line 368
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 374
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 375
    const/16 v2, 0xc

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 371
    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_0
    .end packed-switch
.end method

.method private jjStartNfaWithStates_0(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 2412
    iput p2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2413
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 2414
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2416
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_0(II)I

    move-result v1

    :goto_0
    return v1

    .line 2415
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private jjStartNfaWithStates_1(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 1406
    iput p2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1407
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 1408
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1410
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_1(II)I

    move-result v1

    :goto_0
    return v1

    .line 1409
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private jjStartNfaWithStates_2(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 384
    iput p2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 385
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 386
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_2(II)I

    move-result v1

    :goto_0
    return v1

    .line 387
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private final jjStartNfa_0(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 2145
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_0(II)I

    move-result v0

    return v0
.end method

.method private final jjStartNfa_1(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 1135
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    return v0
.end method

.method private final jjStartNfa_2(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 111
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    return v0
.end method

.method private jjStopAtPos(II)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "kind"    # I

    .prologue
    .line 115
    iput p2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 116
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 117
    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private final jjStopStringLiteralDfa_0(IJ)I
    .locals 8
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v1, -0x1

    const/16 v6, 0x38

    const/16 v0, 0x25

    const-wide/16 v4, 0x0

    .line 2050
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 2140
    :cond_0
    :goto_0
    return v0

    .line 2053
    :pswitch_0
    const-wide/32 v2, 0x3aba00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2055
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    goto :goto_0

    .line 2058
    :cond_1
    const-wide/high16 v2, 0x20000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 2059
    const/4 v0, 0x6

    goto :goto_0

    .line 2060
    :cond_2
    const-wide/32 v2, 0x44000

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 2062
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2063
    const/16 v0, 0x4d

    goto :goto_0

    .line 2065
    :cond_3
    const-wide/high16 v2, 0x40000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 2066
    const/16 v0, 0xb

    goto :goto_0

    .line 2067
    :cond_4
    const-wide v2, 0x10000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    .line 2069
    const/16 v0, 0x30

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2070
    const/16 v0, 0x13

    goto :goto_0

    .line 2072
    :cond_5
    const-wide v2, 0x208000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 2073
    const/16 v0, 0xf

    goto :goto_0

    .line 2074
    :cond_6
    const-wide/32 v2, 0x10400

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7

    .line 2076
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2077
    const/16 v0, 0x11

    goto :goto_0

    :cond_7
    move v0, v1

    .line 2079
    goto :goto_0

    .line 2081
    :pswitch_1
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 2083
    const/16 v1, 0x26

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2084
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 2087
    :cond_8
    const-wide/32 v2, 0x3fbc00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 2089
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2090
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 2093
    :cond_9
    const-wide/16 v2, 0x200

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 2095
    goto :goto_0

    .line 2097
    :pswitch_2
    const-wide/32 v2, 0xd800

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 2099
    const-wide/32 v2, 0x3f2400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 2101
    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 2103
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2104
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 2108
    goto/16 :goto_0

    .line 2110
    :pswitch_3
    const-wide/32 v2, 0x313000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 2112
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2113
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 2116
    :cond_b
    const-wide/32 v2, 0xe0400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 2118
    goto/16 :goto_0

    .line 2120
    :pswitch_4
    const-wide/32 v2, 0x201000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 2122
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2123
    const/4 v1, 0x4

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 2126
    :cond_c
    const-wide/32 v2, 0x112000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 2128
    goto/16 :goto_0

    .line 2130
    :pswitch_5
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 2132
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2133
    const/4 v1, 0x5

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 2136
    :cond_d
    const-wide/32 v2, 0x200000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 2138
    goto/16 :goto_0

    .line 2050
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private final jjStopStringLiteralDfa_1(IJ)I
    .locals 8
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v1, -0x1

    const/16 v6, 0x38

    const/16 v0, 0x25

    const-wide/16 v4, 0x0

    .line 1040
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 1130
    :cond_0
    :goto_0
    return v0

    .line 1043
    :pswitch_0
    const-wide/32 v2, 0x7aba00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1045
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    goto :goto_0

    .line 1048
    :cond_1
    const-wide/high16 v2, 0x20000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 1049
    const/4 v0, 0x6

    goto :goto_0

    .line 1050
    :cond_2
    const-wide/32 v2, 0x44000

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 1052
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1053
    const/16 v0, 0x4b

    goto :goto_0

    .line 1055
    :cond_3
    const-wide/high16 v2, 0x40000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 1056
    const/16 v0, 0xb

    goto :goto_0

    .line 1057
    :cond_4
    const-wide v2, 0x10000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    .line 1059
    const/16 v0, 0x30

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1060
    const/16 v0, 0x13

    goto :goto_0

    .line 1062
    :cond_5
    const-wide v2, 0x208000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 1063
    const/16 v0, 0xf

    goto :goto_0

    .line 1064
    :cond_6
    const-wide/32 v2, 0x10400

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7

    .line 1066
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1067
    const/16 v0, 0x11

    goto :goto_0

    :cond_7
    move v0, v1

    .line 1069
    goto :goto_0

    .line 1071
    :pswitch_1
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 1073
    const/16 v1, 0x26

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1074
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 1077
    :cond_8
    const-wide/32 v2, 0x3fbc00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 1079
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1080
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 1083
    :cond_9
    const-wide/32 v2, 0x400200

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1085
    goto :goto_0

    .line 1087
    :pswitch_2
    const-wide/32 v2, 0xd800

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1089
    const-wide/32 v2, 0x3f2400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 1091
    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 1093
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1094
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 1098
    goto/16 :goto_0

    .line 1100
    :pswitch_3
    const-wide/32 v2, 0x313000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 1102
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1103
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 1106
    :cond_b
    const-wide/32 v2, 0xe0400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1108
    goto/16 :goto_0

    .line 1110
    :pswitch_4
    const-wide/32 v2, 0x201000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 1112
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1113
    const/4 v1, 0x4

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 1116
    :cond_c
    const-wide/32 v2, 0x112000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1118
    goto/16 :goto_0

    .line 1120
    :pswitch_5
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 1122
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1123
    const/4 v1, 0x5

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 1126
    :cond_d
    const-wide/32 v2, 0x200000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1128
    goto/16 :goto_0

    .line 1040
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private final jjStopStringLiteralDfa_2(IJ)I
    .locals 8
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v1, -0x1

    const/16 v6, 0x38

    const/16 v0, 0x25

    const-wide/16 v4, 0x0

    .line 16
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 19
    :pswitch_0
    const-wide/32 v2, 0x3aba00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 21
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    goto :goto_0

    .line 24
    :cond_1
    const-wide/high16 v2, 0x20000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 25
    const/4 v0, 0x6

    goto :goto_0

    .line 26
    :cond_2
    const-wide/32 v2, 0x44000

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 28
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 29
    const/16 v0, 0x4b

    goto :goto_0

    .line 31
    :cond_3
    const-wide/high16 v2, 0x40000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 32
    const/16 v0, 0xb

    goto :goto_0

    .line 33
    :cond_4
    const-wide v2, 0x10000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    .line 35
    const/16 v0, 0x30

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 36
    const/16 v0, 0x13

    goto :goto_0

    .line 38
    :cond_5
    const-wide v2, 0x208000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 39
    const/16 v0, 0xf

    goto :goto_0

    .line 40
    :cond_6
    const-wide/32 v2, 0x10400

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7

    .line 42
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 43
    const/16 v0, 0x11

    goto :goto_0

    :cond_7
    move v0, v1

    .line 45
    goto :goto_0

    .line 47
    :pswitch_1
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 49
    const/16 v1, 0x26

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 50
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 53
    :cond_8
    const-wide/32 v2, 0x3fbc00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 55
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 56
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 59
    :cond_9
    const-wide/16 v2, 0x200

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :pswitch_2
    const-wide/32 v2, 0xd800

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 65
    const-wide/32 v2, 0x3f2400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 67
    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 69
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 70
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 74
    goto/16 :goto_0

    .line 76
    :pswitch_3
    const-wide/32 v2, 0x313000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 78
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 79
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 82
    :cond_b
    const-wide/32 v2, 0xe0400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 84
    goto/16 :goto_0

    .line 86
    :pswitch_4
    const-wide/32 v2, 0x201000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 88
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 89
    const/4 v1, 0x4

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 92
    :cond_c
    const-wide/32 v2, 0x112000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 94
    goto/16 :goto_0

    .line 96
    :pswitch_5
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 98
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 99
    const/4 v1, 0x5

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 102
    :cond_d
    const-wide/32 v2, 0x200000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 104
    goto/16 :goto_0

    .line 16
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public ReInit(Lorg/apache/commons/jexl2/parser/SimpleCharStream;)V
    .locals 1
    .param p1, "stream"    # Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    .prologue
    .line 3150
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 3151
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->defaultLexState:I

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    .line 3152
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    .line 3153
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInitRounds()V

    .line 3154
    return-void
.end method

.method public ReInit(Lorg/apache/commons/jexl2/parser/SimpleCharStream;I)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/commons/jexl2/parser/SimpleCharStream;
    .param p2, "lexState"    # I

    .prologue
    .line 3166
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInit(Lorg/apache/commons/jexl2/parser/SimpleCharStream;)V

    .line 3167
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->SwitchTo(I)V

    .line 3168
    return-void
.end method

.method public SwitchTo(I)V
    .locals 3
    .param p1, "lexState"    # I

    .prologue
    .line 3173
    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 3174
    :cond_0
    new-instance v0, Lorg/apache/commons/jexl2/parser/TokenMgrError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Ignoring invalid lexical state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". State unchanged."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/jexl2/parser/TokenMgrError;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 3176
    :cond_1
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    .line 3177
    return-void
.end method

.method public getNextToken()Lorg/apache/commons/jexl2/parser/Token;
    .locals 15

    .prologue
    .line 3214
    const/4 v8, 0x0

    .line 3221
    .local v8, "curPos":I
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->BeginToken()C

    move-result v0

    iput-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3230
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    packed-switch v0, :pswitch_data_0

    .line 3263
    :goto_1
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_7

    .line 3265
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    add-int/lit8 v0, v0, 0x1

    if-ge v0, v8, :cond_1

    .line 3266
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    sub-int v2, v8, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 3267
    :cond_1
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjtoToken:[J

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    shr-int/lit8 v2, v2, 0x6

    aget-wide v6, v0, v2

    const-wide/16 v13, 0x1

    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    and-int/lit8 v0, v0, 0x3f

    shl-long/2addr v13, v0

    and-long/2addr v6, v13

    const-wide/16 v13, 0x0

    cmp-long v0, v6, v13

    if-eqz v0, :cond_6

    .line 3269
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjFillToken()Lorg/apache/commons/jexl2/parser/Token;

    move-result-object v11

    .line 3270
    .local v11, "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget v0, v0, v2

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 3271
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget v0, v0, v2

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    :cond_2
    move-object v12, v11

    .line 3272
    .end local v11    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    .local v12, "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    :goto_2
    return-object v12

    .line 3223
    .end local v12    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    :catch_0
    move-exception v9

    .line 3225
    .local v9, "e":Ljava/io/IOException;
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 3226
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjFillToken()Lorg/apache/commons/jexl2/parser/Token;

    move-result-object v11

    .restart local v11    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    move-object v12, v11

    .line 3227
    .end local v11    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    .restart local v12    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    goto :goto_2

    .line 3233
    .end local v9    # "e":Ljava/io/IOException;
    .end local v12    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 3234
    :goto_3
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0x20

    if-gt v0, v2, :cond_3

    const-wide v6, 0x100003600L

    const-wide/16 v13, 0x1

    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shl-long/2addr v13, v0

    and-long/2addr v6, v13

    const-wide/16 v13, 0x0

    cmp-long v0, v6, v13

    if-eqz v0, :cond_3

    .line 3235
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->BeginToken()C

    move-result v0

    iput-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 3237
    :catch_1
    move-exception v10

    .local v10, "e1":Ljava/io/IOException;
    goto/16 :goto_0

    .line 3238
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_3
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 3239
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 3240
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa0_0()I

    move-result v8

    .line 3241
    goto/16 :goto_1

    .line 3243
    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 3244
    :goto_4
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0x20

    if-gt v0, v2, :cond_4

    const-wide v6, 0x100003600L

    const-wide/16 v13, 0x1

    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shl-long/2addr v13, v0

    and-long/2addr v6, v13

    const-wide/16 v13, 0x0

    cmp-long v0, v6, v13

    if-eqz v0, :cond_4

    .line 3245
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->BeginToken()C

    move-result v0

    iput-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    .line 3247
    :catch_2
    move-exception v10

    .restart local v10    # "e1":Ljava/io/IOException;
    goto/16 :goto_0

    .line 3248
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_4
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 3249
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 3250
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa0_1()I

    move-result v8

    .line 3251
    goto/16 :goto_1

    .line 3253
    :pswitch_2
    :try_start_3
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 3254
    :goto_5
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0x20

    if-gt v0, v2, :cond_5

    const-wide v6, 0x100003600L

    const-wide/16 v13, 0x1

    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shl-long/2addr v13, v0

    and-long/2addr v6, v13

    const-wide/16 v13, 0x0

    cmp-long v0, v6, v13

    if-eqz v0, :cond_5

    .line 3255
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->BeginToken()C

    move-result v0

    iput-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_5

    .line 3257
    :catch_3
    move-exception v10

    .restart local v10    # "e1":Ljava/io/IOException;
    goto/16 :goto_0

    .line 3258
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_5
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 3259
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 3260
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa0_2()I

    move-result v8

    goto/16 :goto_1

    .line 3276
    :cond_6
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget v0, v0, v2

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3277
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget v0, v0, v2

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    goto/16 :goto_0

    .line 3281
    :cond_7
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getEndLine()I

    move-result v3

    .line 3282
    .local v3, "error_line":I
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getEndColumn()I

    move-result v4

    .line 3283
    .local v4, "error_column":I
    const/4 v5, 0x0

    .line 3284
    .local v5, "error_after":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3285
    .local v1, "EOFSeen":Z
    :try_start_4
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 3296
    :goto_6
    if-nez v1, :cond_8

    .line 3297
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 3298
    const/4 v0, 0x1

    if-gt v8, v0, :cond_c

    const-string v5, ""

    .line 3300
    :cond_8
    :goto_7
    new-instance v0, Lorg/apache/commons/jexl2/parser/TokenMgrError;

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    iget-char v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lorg/apache/commons/jexl2/parser/TokenMgrError;-><init>(ZIIILjava/lang/String;CI)V

    throw v0

    .line 3286
    :catch_4
    move-exception v10

    .line 3287
    .restart local v10    # "e1":Ljava/io/IOException;
    const/4 v1, 0x1

    .line 3288
    const/4 v0, 0x1

    if-gt v8, v0, :cond_a

    const-string v5, ""

    .line 3289
    :goto_8
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0xa

    if-eq v0, v2, :cond_9

    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0xd

    if-ne v0, v2, :cond_b

    .line 3290
    :cond_9
    add-int/lit8 v3, v3, 0x1

    .line 3291
    const/4 v4, 0x0

    goto :goto_6

    .line 3288
    :cond_a
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->GetImage()Ljava/lang/String;

    move-result-object v5

    goto :goto_8

    .line 3294
    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 3298
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_c
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->GetImage()Ljava/lang/String;

    move-result-object v5

    goto :goto_7

    .line 3230
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected jjFillToken()Lorg/apache/commons/jexl2/parser/Token;
    .locals 9

    .prologue
    .line 3187
    sget-object v7, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    iget v8, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget-object v5, v7, v8

    .line 3188
    .local v5, "im":Ljava/lang/String;
    if-nez v5, :cond_0

    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->GetImage()Ljava/lang/String;

    move-result-object v2

    .line 3189
    .local v2, "curTokenImage":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getBeginLine()I

    move-result v1

    .line 3190
    .local v1, "beginLine":I
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getBeginColumn()I

    move-result v0

    .line 3191
    .local v0, "beginColumn":I
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getEndLine()I

    move-result v4

    .line 3192
    .local v4, "endLine":I
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getEndColumn()I

    move-result v3

    .line 3193
    .local v3, "endColumn":I
    iget v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    invoke-static {v7, v2}, Lorg/apache/commons/jexl2/parser/Token;->newToken(ILjava/lang/String;)Lorg/apache/commons/jexl2/parser/Token;

    move-result-object v6

    .line 3195
    .local v6, "t":Lorg/apache/commons/jexl2/parser/Token;
    iput v1, v6, Lorg/apache/commons/jexl2/parser/Token;->beginLine:I

    .line 3196
    iput v4, v6, Lorg/apache/commons/jexl2/parser/Token;->endLine:I

    .line 3197
    iput v0, v6, Lorg/apache/commons/jexl2/parser/Token;->beginColumn:I

    .line 3198
    iput v3, v6, Lorg/apache/commons/jexl2/parser/Token;->endColumn:I

    .line 3200
    return-object v6

    .end local v0    # "beginColumn":I
    .end local v1    # "beginLine":I
    .end local v2    # "curTokenImage":Ljava/lang/String;
    .end local v3    # "endColumn":I
    .end local v4    # "endLine":I
    .end local v6    # "t":Lorg/apache/commons/jexl2/parser/Token;
    :cond_0
    move-object v2, v5

    .line 3188
    goto :goto_0
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1, "ds"    # Ljava/io/PrintStream;

    .prologue
    .line 13
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->debugStream:Ljava/io/PrintStream;

    return-void
.end method
