.class Lorg/apache/commons/jexl2/ExpressionImpl$1;
.super Ljava/lang/Object;
.source "ExpressionImpl.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/commons/jexl2/ExpressionImpl;->callable(Lorg/apache/commons/jexl2/JexlContext;[Ljava/lang/Object;)Ljava/util/concurrent/Callable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Ljava/lang/Object;

.field final synthetic this$0:Lorg/apache/commons/jexl2/ExpressionImpl;

.field final synthetic val$interpreter:Lorg/apache/commons/jexl2/Interpreter;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/ExpressionImpl;Lorg/apache/commons/jexl2/Interpreter;)V
    .locals 1

    .prologue
    .line 164
    iput-object p1, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->this$0:Lorg/apache/commons/jexl2/ExpressionImpl;

    iput-object p2, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->val$interpreter:Lorg/apache/commons/jexl2/Interpreter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->val$interpreter:Lorg/apache/commons/jexl2/Interpreter;

    iput-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->result:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->result:Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->val$interpreter:Lorg/apache/commons/jexl2/Interpreter;

    if-ne v0, v1, :cond_0

    .line 166
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->val$interpreter:Lorg/apache/commons/jexl2/Interpreter;

    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->this$0:Lorg/apache/commons/jexl2/ExpressionImpl;

    iget-object v1, v1, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->result:Ljava/lang/Object;

    .line 168
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl$1;->result:Ljava/lang/Object;

    return-object v0
.end method
