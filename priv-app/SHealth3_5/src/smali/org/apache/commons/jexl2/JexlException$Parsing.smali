.class public Lorg/apache/commons/jexl2/JexlException$Parsing;
.super Lorg/apache/commons/jexl2/JexlException;
.source "JexlException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/JexlException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Parsing"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/ParseException;)V
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/JexlInfo;
    .param p2, "expr"    # Ljava/lang/CharSequence;
    .param p3, "cause"    # Lorg/apache/commons/jexl2/parser/ParseException;

    .prologue
    .line 193
    invoke-static {p1, p3}, Lorg/apache/commons/jexl2/JexlException$Parsing;->merge(Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/parser/ParseException;)Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 194
    return-void
.end method

.method private static merge(Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/parser/ParseException;)Lorg/apache/commons/jexl2/DebugInfo;
    .locals 5
    .param p0, "node"    # Lorg/apache/commons/jexl2/JexlInfo;
    .param p1, "cause"    # Lorg/apache/commons/jexl2/parser/ParseException;

    .prologue
    .line 203
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/apache/commons/jexl2/JexlInfo;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v0

    .line 204
    .local v0, "dbgn":Lorg/apache/commons/jexl2/DebugInfo;
    :goto_0
    if-nez p1, :cond_1

    .line 209
    .end local v0    # "dbgn":Lorg/apache/commons/jexl2/DebugInfo;
    :goto_1
    return-object v0

    .line 203
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 206
    .restart local v0    # "dbgn":Lorg/apache/commons/jexl2/DebugInfo;
    :cond_1
    if-nez v0, :cond_2

    .line 207
    new-instance v0, Lorg/apache/commons/jexl2/DebugInfo;

    .end local v0    # "dbgn":Lorg/apache/commons/jexl2/DebugInfo;
    const-string v1, ""

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ParseException;->getLine()I

    move-result v2

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ParseException;->getColumn()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/commons/jexl2/DebugInfo;-><init>(Ljava/lang/String;II)V

    goto :goto_1

    .line 209
    .restart local v0    # "dbgn":Lorg/apache/commons/jexl2/DebugInfo;
    :cond_2
    new-instance v1, Lorg/apache/commons/jexl2/DebugInfo;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/DebugInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ParseException;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ParseException;->getColumn()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/commons/jexl2/DebugInfo;-><init>(Ljava/lang/String;II)V

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected detailedMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 222
    const-string/jumbo v0, "parsing"

    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlException$Parsing;->getExpression()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/jexl2/JexlException$Parsing;->parserError(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpression()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    invoke-super {p0}, Lorg/apache/commons/jexl2/JexlException;->detailedMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
