.class public final Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;
.super Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
.source "Sandbox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/introspection/Sandbox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WhiteSet"
.end annotation


# instance fields
.field private names:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 198
    invoke-direct {p0}, Lorg/apache/commons/jexl2/introspection/Sandbox$Names;-><init>()V

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    .line 207
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    invoke-interface {v0, p1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public alias(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "alias"    # Ljava/lang/String;

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 213
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    .line 215
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 223
    .end local p1    # "name":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "name":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;->names:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object p1, v0

    goto :goto_0
.end method
