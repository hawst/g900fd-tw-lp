.class public Lorg/apache/commons/jexl2/parser/ASTJexlScript;
.super Lorg/apache/commons/jexl2/parser/JexlNode;
.source "ASTJexlScript.java"


# instance fields
.field private scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/JexlNode;-><init>(I)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .line 30
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/Parser;I)V
    .locals 1
    .param p1, "p"    # Lorg/apache/commons/jexl2/parser/Parser;
    .param p2, "id"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;-><init>(Lorg/apache/commons/jexl2/parser/Parser;I)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .line 34
    return-void
.end method


# virtual methods
.method public varargs createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;
    .locals 1
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    invoke-virtual {v0, p1}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getArgCount()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->getArgCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLocalVariables()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->getLocalVariables()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParameters()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->getParameters()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRegisters()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine$Scope;->getRegisters()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScope()Lorg/apache/commons/jexl2/JexlEngine$Scope;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    return-object v0
.end method

.method public jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "visitor"    # Lorg/apache/commons/jexl2/parser/ParserVisitor;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 38
    invoke-interface {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/ParserVisitor;->visit(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setScope(Lorg/apache/commons/jexl2/JexlEngine$Scope;)V
    .locals 0
    .param p1, "theScope"    # Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .prologue
    .line 46
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->scope:Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .line 47
    return-void
.end method
