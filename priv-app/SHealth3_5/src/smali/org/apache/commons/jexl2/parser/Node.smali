.class public interface abstract Lorg/apache/commons/jexl2/parser/Node;
.super Ljava/lang/Object;
.source "Node.java"


# virtual methods
.method public abstract jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract jjtAddChild(Lorg/apache/commons/jexl2/parser/Node;I)V
.end method

.method public abstract jjtClose()V
.end method

.method public abstract jjtGetChild(I)Lorg/apache/commons/jexl2/parser/Node;
.end method

.method public abstract jjtGetNumChildren()I
.end method

.method public abstract jjtGetParent()Lorg/apache/commons/jexl2/parser/Node;
.end method

.method public abstract jjtOpen()V
.end method

.method public abstract jjtSetParent(Lorg/apache/commons/jexl2/parser/Node;)V
.end method
