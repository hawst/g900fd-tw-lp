.class public Lorg/apache/commons/jexl2/parser/ASTIdentifier;
.super Lorg/apache/commons/jexl2/parser/JexlNode;
.source "ASTIdentifier.java"


# instance fields
.field private register:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/JexlNode;-><init>(I)V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->register:I

    .line 26
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/Parser;I)V
    .locals 1
    .param p1, "p"    # Lorg/apache/commons/jexl2/parser/Parser;
    .param p2, "id"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;-><init>(Lorg/apache/commons/jexl2/parser/Parser;I)V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->register:I

    .line 30
    return-void
.end method


# virtual methods
.method public getRegister()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->register:I

    return v0
.end method

.method public jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "visitor"    # Lorg/apache/commons/jexl2/parser/ParserVisitor;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 48
    invoke-interface {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/ParserVisitor;->visit(Lorg/apache/commons/jexl2/parser/ASTIdentifier;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method setRegister(I)V
    .locals 0
    .param p1, "r"    # I

    .prologue
    .line 39
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->register:I

    .line 40
    return-void
.end method

.method setRegister(Ljava/lang/String;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/String;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_0

    .line 34
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->register:I

    .line 36
    :cond_0
    return-void
.end method
