.class final Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;
.super Ljava/lang/Object;
.source "JexlScriptEngine.java"

# interfaces
.implements Lorg/apache/commons/jexl2/JexlContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "JexlContextWrapper"
.end annotation


# instance fields
.field private final scriptContext:Ljavax/script/ScriptContext;

.field final synthetic this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;


# direct methods
.method private constructor <init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Ljavax/script/ScriptContext;)V
    .locals 0
    .param p2, "theContext"    # Ljavax/script/ScriptContext;

    .prologue
    .line 301
    iput-object p1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 302
    iput-object p2, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;->scriptContext:Ljavax/script/ScriptContext;

    .line 303
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Ljavax/script/ScriptContext;Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
    .param p2, "x1"    # Ljavax/script/ScriptContext;
    .param p3, "x2"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$1;

    .prologue
    .line 294
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;-><init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Ljavax/script/ScriptContext;)V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 307
    iget-object v1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;->scriptContext:Ljavax/script/ScriptContext;

    invoke-interface {v1, p1}, Ljavax/script/ScriptContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 308
    .local v0, "o":Ljava/lang/Object;
    const-string v1, "JEXL"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 309
    if-eqz v0, :cond_0

    .line 310
    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->LOG:Lorg/apache/commons/logging/Log;
    invoke-static {}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->access$500()Lorg/apache/commons/logging/Log;

    move-result-object v1

    const-string v2, "JEXL is a reserved variable name, user defined value is ignored"

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    .line 312
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->jexlObject:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;
    invoke-static {v1}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->access$900(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;

    move-result-object v0

    .line 314
    .end local v0    # "o":Ljava/lang/Object;
    :cond_1
    return-object v0
.end method

.method public has(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 328
    iget-object v1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;->scriptContext:Ljavax/script/ScriptContext;

    const/16 v2, 0x64

    invoke-interface {v1, v2}, Ljavax/script/ScriptContext;->getBindings(I)Ljavax/script/Bindings;

    move-result-object v0

    .line 329
    .local v0, "bnd":Ljavax/script/Bindings;
    invoke-interface {v0, p1}, Ljavax/script/Bindings;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 319
    iget-object v1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;->scriptContext:Ljavax/script/ScriptContext;

    invoke-interface {v1, p1}, Ljavax/script/ScriptContext;->getAttributesScope(Ljava/lang/String;)I

    move-result v0

    .line 320
    .local v0, "scope":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 321
    const/16 v0, 0x64

    .line 323
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;->scriptContext:Ljavax/script/ScriptContext;

    invoke-interface {v1, v0}, Ljavax/script/ScriptContext;->getBindings(I)Ljavax/script/Bindings;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Ljavax/script/Bindings;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    return-void
.end method
