.class public Lorg/apache/commons/jexl2/parser/StringParser;
.super Ljava/lang/Object;
.source "StringParser.java"


# static fields
.field private static final BASE10:I = 0xa

.field private static final FIRST_ASCII:C = ' '

.field private static final LAST_ASCII:C = '\u007f'

.field private static final SHIFT:I = 0xc

.field private static final UCHAR_LEN:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method public static buildString(Ljava/lang/CharSequence;Z)Ljava/lang/String;
    .locals 7
    .param p0, "str"    # Ljava/lang/CharSequence;
    .param p1, "eatsep"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 48
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 49
    .local v3, "strb":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    invoke-interface {p0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 50
    .local v2, "sep":C
    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-eqz p1, :cond_1

    move v5, v0

    :goto_1
    sub-int v1, v6, v5

    .line 51
    .local v1, "end":I
    if-eqz p1, :cond_2

    .line 52
    .local v0, "begin":I
    :goto_2
    invoke-static {v3, p0, v0, v1, v2}, Lorg/apache/commons/jexl2/parser/StringParser;->read(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;IIC)I

    .line 53
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .end local v0    # "begin":I
    .end local v1    # "end":I
    .end local v2    # "sep":C
    :cond_0
    move v2, v4

    .line 49
    goto :goto_0

    .restart local v2    # "sep":C
    :cond_1
    move v5, v4

    .line 50
    goto :goto_1

    .restart local v1    # "end":I
    :cond_2
    move v0, v4

    .line 51
    goto :goto_2
.end method

.method public static escapeString(Ljava/lang/String;C)Ljava/lang/String;
    .locals 7
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "delim"    # C

    .prologue
    .line 155
    if-nez p0, :cond_0

    .line 156
    const/4 v6, 0x0

    .line 206
    :goto_0
    return-object v6

    .line 158
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 159
    .local v4, "length":I
    new-instance v5, Ljava/lang/StringBuilder;

    add-int/lit8 v6, v4, 0x2

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 160
    .local v5, "strb":Ljava/lang/StringBuilder;
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v4, :cond_3

    .line 162
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 163
    .local v0, "c":C
    sparse-switch v0, :sswitch_data_0

    .line 191
    const/16 v6, 0x20

    if-lt v0, v6, :cond_1

    const/16 v6, 0x7f

    if-gt v0, v6, :cond_1

    .line 192
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    :goto_2
    :sswitch_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 167
    :sswitch_1
    const-string v6, "\\b"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 170
    :sswitch_2
    const-string v6, "\\t"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 173
    :sswitch_3
    const-string v6, "\\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 176
    :sswitch_4
    const-string v6, "\\f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 179
    :sswitch_5
    const-string v6, "\\r"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 182
    :sswitch_6
    const-string v6, "\\\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 185
    :sswitch_7
    const-string v6, "\\\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 188
    :sswitch_8
    const-string v6, "\\\\"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 195
    :cond_1
    const/16 v6, 0x5c

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 196
    const/16 v6, 0x75

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 197
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 198
    .local v2, "hex":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .local v1, "h":I
    :goto_3
    const/4 v6, 0x4

    if-ge v1, v6, :cond_2

    .line 199
    const/16 v6, 0x30

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 201
    :cond_2
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 205
    .end local v0    # "c":C
    .end local v1    # "h":I
    .end local v2    # "hex":Ljava/lang/String;
    :cond_3
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 206
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 163
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x9 -> :sswitch_2
        0xa -> :sswitch_3
        0xc -> :sswitch_4
        0xd -> :sswitch_5
        0x22 -> :sswitch_6
        0x27 -> :sswitch_7
        0x5c -> :sswitch_8
    .end sparse-switch
.end method

.method private static read(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;IIC)I
    .locals 8
    .param p0, "strb"    # Ljava/lang/StringBuilder;
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "begin"    # I
    .param p3, "end"    # I
    .param p4, "sep"    # C

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v7, 0x5c

    .line 82
    const/4 v1, 0x0

    .line 83
    .local v1, "escape":Z
    move v2, p2

    .line 84
    .local v2, "index":I
    :goto_0
    if-ge v2, p3, :cond_8

    .line 85
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 86
    .local v0, "c":C
    if-eqz v1, :cond_6

    .line 87
    const/16 v6, 0x75

    if-ne v0, v6, :cond_1

    add-int/lit8 v6, v2, 0x4

    if-ge v6, p3, :cond_1

    add-int/lit8 v6, v2, 0x1

    invoke-static {p0, p1, v6}, Lorg/apache/commons/jexl2/parser/StringParser;->readUnicodeChar(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;I)I

    move-result v6

    if-lez v6, :cond_1

    .line 88
    add-int/lit8 v2, v2, 0x4

    .line 97
    :goto_1
    const/4 v1, 0x0

    .line 84
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 91
    :cond_1
    if-nez p4, :cond_4

    const/16 v6, 0x27

    if-eq v0, v6, :cond_3

    const/16 v6, 0x22

    if-eq v0, v6, :cond_3

    move v3, v4

    .line 92
    .local v3, "notSeparator":Z
    :goto_3
    if-eqz v3, :cond_2

    if-eq v0, v7, :cond_2

    .line 93
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 95
    :cond_2
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .end local v3    # "notSeparator":Z
    :cond_3
    move v3, v5

    .line 91
    goto :goto_3

    :cond_4
    if-eq v0, p4, :cond_5

    move v3, v4

    goto :goto_3

    :cond_5
    move v3, v5

    goto :goto_3

    .line 100
    :cond_6
    if-ne v0, v7, :cond_7

    .line 101
    const/4 v1, 0x1

    .line 102
    goto :goto_2

    .line 104
    :cond_7
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105
    if-ne v0, p4, :cond_0

    .line 109
    .end local v0    # "c":C
    :cond_8
    return v2
.end method

.method public static readString(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;IC)I
    .locals 1
    .param p0, "strb"    # Ljava/lang/StringBuilder;
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "index"    # I
    .param p3, "sep"    # C

    .prologue
    .line 66
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-static {p0, p1, p2, v0, p3}, Lorg/apache/commons/jexl2/parser/StringParser;->read(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;IIC)I

    move-result v0

    return v0
.end method

.method private static readUnicodeChar(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;I)I
    .locals 7
    .param p0, "strb"    # Ljava/lang/StringBuilder;
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "begin"    # I

    .prologue
    const/4 v5, 0x4

    .line 124
    const/4 v4, 0x0

    .line 125
    .local v4, "xc":C
    const/16 v0, 0xc

    .line 126
    .local v0, "bits":I
    const/4 v3, 0x0

    .line 127
    .local v3, "value":I
    const/4 v2, 0x0

    .local v2, "offset":I
    :goto_0
    if-ge v2, v5, :cond_3

    .line 128
    add-int v6, p2, v2

    invoke-interface {p1, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 129
    .local v1, "c":C
    const/16 v6, 0x30

    if-lt v1, v6, :cond_0

    const/16 v6, 0x39

    if-gt v1, v6, :cond_0

    .line 130
    add-int/lit8 v3, v1, -0x30

    .line 138
    :goto_1
    shl-int v6, v3, v0

    or-int/2addr v6, v4

    int-to-char v4, v6

    .line 139
    add-int/lit8 v0, v0, -0x4

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 131
    :cond_0
    const/16 v6, 0x61

    if-lt v1, v6, :cond_1

    const/16 v6, 0x68

    if-gt v1, v6, :cond_1

    .line 132
    add-int/lit8 v6, v1, -0x61

    add-int/lit8 v3, v6, 0xa

    goto :goto_1

    .line 133
    :cond_1
    const/16 v6, 0x41

    if-lt v1, v6, :cond_2

    const/16 v6, 0x48

    if-gt v1, v6, :cond_2

    .line 134
    add-int/lit8 v6, v1, -0x41

    add-int/lit8 v3, v6, 0xa

    goto :goto_1

    .line 136
    :cond_2
    const/4 v5, 0x0

    .line 142
    .end local v1    # "c":C
    :goto_2
    return v5

    .line 141
    :cond_3
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2
.end method
