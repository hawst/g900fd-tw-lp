.class public Lorg/apache/commons/jexl2/introspection/UberspectImpl;
.super Lorg/apache/commons/jexl2/internal/Introspector;
.source "UberspectImpl.java"

# interfaces
.implements Lorg/apache/commons/jexl2/introspection/Uberspect;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/introspection/UberspectImpl$1;,
        Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;,
        Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;,
        Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;,
        Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedContainer;,
        Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;
    }
.end annotation


# static fields
.field public static final TRY_FAILED:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->TRY_FAILED:Ljava/lang/Object;

    sput-object v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/logging/Log;)V
    .locals 0
    .param p1, "runtimeLogger"    # Lorg/apache/commons/logging/Log;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/Introspector;-><init>(Lorg/apache/commons/logging/Log;)V

    .line 59
    return-void
.end method


# virtual methods
.method public getConstructor(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Constructor;
    .locals 1
    .param p1, "ctorHandle"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "[",
            "Ljava/lang/Object;",
            "Lorg/apache/commons/jexl2/JexlInfo;",
            ")",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getConstructor(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    return-object v0
.end method

.method public getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .locals 3
    .param p1, "ctorHandle"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    const/4 v2, 0x0

    .line 124
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getConstructor(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 125
    .local v0, "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    if-eqz v0, :cond_0

    .line 126
    new-instance v1, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;

    invoke-direct {v1, p0, v0, v2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;-><init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl;Ljava/lang/reflect/Constructor;Lorg/apache/commons/jexl2/introspection/UberspectImpl$1;)V

    .line 128
    :goto_0
    return-object v1

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method public getField(Ljava/lang/Object;Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Field;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 174
    instance-of v1, p1, Ljava/lang/Class;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/lang/Class;

    .end local p1    # "obj":Ljava/lang/Object;
    move-object v0, p1

    .line 175
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    return-object v1

    .line 174
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method protected getIndexedGet(Ljava/lang/Object;Ljava/lang/String;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    .locals 8
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 189
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 190
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {p2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "base":Ljava/lang/String;
    move-object v2, p2

    .line 192
    .local v2, "container":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 193
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getMethods(Ljava/lang/Class;Ljava/lang/String;)[Ljava/lang/reflect/Method;

    move-result-object v3

    .line 194
    .local v3, "getters":[Ljava/lang/reflect/Method;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "set"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getMethods(Ljava/lang/Class;Ljava/lang/String;)[Ljava/lang/reflect/Method;

    move-result-object v4

    .line 195
    .local v4, "setters":[Ljava/lang/reflect/Method;
    if-eqz v3, :cond_0

    .line 196
    new-instance v5, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;

    invoke-direct {v5, v2, v1, v3, v4}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$IndexedType;-><init>(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/reflect/Method;[Ljava/lang/reflect/Method;)V

    .line 199
    .end local v0    # "base":Ljava/lang/String;
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "container":Ljava/lang/String;
    .end local v3    # "getters":[Ljava/lang/reflect/Method;
    .end local v4    # "setters":[Ljava/lang/reflect/Method;
    :goto_0
    return-object v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public getIterator(Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/util/Iterator;
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lorg/apache/commons/jexl2/JexlInfo;",
            ")",
            "Ljava/util/Iterator",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75
    instance-of v3, p1, Ljava/util/Iterator;

    if-eqz v3, :cond_0

    .line 76
    check-cast p1, Ljava/util/Iterator;

    .line 101
    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p1

    .line 78
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 79
    new-instance v2, Lorg/apache/commons/jexl2/internal/ArrayIterator;

    invoke-direct {v2, p1}, Lorg/apache/commons/jexl2/internal/ArrayIterator;-><init>(Ljava/lang/Object;)V

    move-object p1, v2

    goto :goto_0

    .line 81
    :cond_1
    instance-of v3, p1, Ljava/util/Map;

    if-eqz v3, :cond_2

    .line 82
    check-cast p1, Ljava/util/Map;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    goto :goto_0

    .line 84
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_2
    instance-of v3, p1, Ljava/util/Enumeration;

    if-eqz v3, :cond_3

    .line 85
    new-instance v2, Lorg/apache/commons/jexl2/internal/EnumerationIterator;

    check-cast p1, Ljava/util/Enumeration;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-direct {v2, p1}, Lorg/apache/commons/jexl2/internal/EnumerationIterator;-><init>(Ljava/util/Enumeration;)V

    move-object p1, v2

    goto :goto_0

    .line 87
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_3
    instance-of v3, p1, Ljava/lang/Iterable;

    if-eqz v3, :cond_4

    .line 88
    check-cast p1, Ljava/lang/Iterable;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    goto :goto_0

    .line 94
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_4
    :try_start_0
    const-string v3, "iterator"

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v3, v4}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getMethodExecutor(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;

    move-result-object v0

    .line 95
    .local v0, "it":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    if-eqz v0, :cond_5

    const-class v3, Ljava/util/Iterator;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->getReturnType()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 96
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->execute(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v2

    goto :goto_0

    .line 98
    .end local v0    # "it":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    :catch_0
    move-exception v1

    .line 99
    .local v1, "xany":Ljava/lang/Exception;
    new-instance v2, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v3, "unable to generate iterator()"

    invoke-direct {v2, p2, v3, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .end local v1    # "xany":Ljava/lang/Exception;
    .restart local v0    # "it":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    :cond_5
    move-object p1, v2

    .line 101
    goto :goto_0
.end method

.method public getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;
    .param p4, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 108
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getMethodExecutor(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;

    move-result-object v0

    return-object v0
.end method

.method public getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 136
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getGetExecutor(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;

    move-result-object v1

    .line 137
    .local v1, "get":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 138
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getIndexedGet(Ljava/lang/Object;Ljava/lang/String;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    move-result-object v1

    .line 139
    if-nez v1, :cond_0

    .line 140
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getField(Ljava/lang/Object;Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 141
    .local v0, "field":Ljava/lang/reflect/Field;
    if-eqz v0, :cond_0

    .line 142
    new-instance v2, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;

    invoke-direct {v2, v0}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;-><init>(Ljava/lang/reflect/Field;)V

    .line 146
    .end local v0    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v1

    goto :goto_0
.end method

.method public getPropertySet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;
    .param p3, "arg"    # Ljava/lang/Object;
    .param p4, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getSetExecutor(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;

    move-result-object v1

    .line 154
    .local v1, "set":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 155
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2, p4}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getField(Ljava/lang/Object;Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 156
    .local v0, "field":Ljava/lang/reflect/Field;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p3, :cond_0

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->isInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    :cond_0
    new-instance v1, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;

    .end local v1    # "set":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    invoke-direct {v1, v0}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;-><init>(Ljava/lang/reflect/Field;)V

    .line 162
    .end local v0    # "field":Ljava/lang/reflect/Field;
    :cond_1
    return-object v1
.end method

.method public setLoader(Ljava/lang/ClassLoader;)V
    .locals 1
    .param p1, "cloader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 67
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->base()Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->setLoader(Ljava/lang/ClassLoader;)V

    .line 68
    return-void
.end method
