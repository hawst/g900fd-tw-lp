.class public Lorg/apache/commons/jexl2/parser/TokenMgrError;
.super Ljava/lang/Error;
.source "TokenMgrError.java"


# static fields
.field public static final INVALID_LEXICAL_STATE:I = 0x2

.field public static final LEXICAL_ERROR:I = 0x0

.field public static final LOOP_DETECTED:I = 0x3

.field public static final STATIC_LEXER_ERROR:I = 0x1

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private after:Ljava/lang/String;

.field private column:I

.field private current:C

.field private eof:Z

.field private errorCode:I

.field private line:I

.field private state:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "reason"    # I

    .prologue
    .line 131
    invoke-direct {p0, p1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    .line 132
    iput p2, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->errorCode:I

    .line 133
    return-void
.end method

.method public constructor <init>(ZIIILjava/lang/String;CI)V
    .locals 0
    .param p1, "EOFSeen"    # Z
    .param p2, "lexState"    # I
    .param p3, "errorLine"    # I
    .param p4, "errorColumn"    # I
    .param p5, "errorAfter"    # Ljava/lang/String;
    .param p6, "curChar"    # C
    .param p7, "reason"    # I

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Error;-><init>()V

    .line 137
    iput-boolean p1, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->eof:Z

    .line 138
    iput p2, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->state:I

    .line 139
    iput p3, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->line:I

    .line 140
    iput p4, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->column:I

    .line 141
    iput-object p5, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->after:Ljava/lang/String;

    .line 142
    iput-char p6, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->current:C

    .line 143
    iput p7, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->errorCode:I

    .line 144
    return-void
.end method


# virtual methods
.method public getAfter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->after:Ljava/lang/String;

    return-object v0
.end method

.method public getColumn()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->column:I

    return v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->errorCode:I

    return v0
.end method

.method public getLine()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->line:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Lexical error at line "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->line:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->column:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".  Encountered: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->eof:Z

    if-eqz v0, :cond_0

    const-string v0, "<EOF> "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "after : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->after:Ljava/lang/String;

    invoke-static {v1, v3}, Lorg/apache/commons/jexl2/parser/StringParser;->escapeString(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->current:C

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, Lorg/apache/commons/jexl2/parser/StringParser;->escapeString(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/TokenMgrError;->current:C

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "), "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
