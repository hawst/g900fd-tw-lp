.class Lorg/apache/commons/jexl2/internal/introspection/MethodKey$2;
.super Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;
.source "MethodKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters",
        "<",
        "Ljava/lang/reflect/Constructor",
        "<*>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 648
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;-><init>(Lorg/apache/commons/jexl2/internal/introspection/MethodKey$1;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic getParameterTypes(Ljava/lang/Object;)[Ljava/lang/Class;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 648
    check-cast p1, Ljava/lang/reflect/Constructor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$2;->getParameterTypes(Ljava/lang/reflect/Constructor;)[Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method protected getParameterTypes(Ljava/lang/reflect/Constructor;)[Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Constructor",
            "<*>;)[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 649
    .local p1, "app":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method
