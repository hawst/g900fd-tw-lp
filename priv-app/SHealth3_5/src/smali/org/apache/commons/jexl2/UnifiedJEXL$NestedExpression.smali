.class Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;
.super Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NestedExpression"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 2
    .param p2, "expr"    # Ljava/lang/CharSequence;
    .param p3, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p4, "source"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 600
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    .line 601
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 602
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    if-eq v0, p0, :cond_0

    .line 603
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Nested expression can not have a source"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 605
    :cond_0
    return-void
.end method


# virtual methods
.method public asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 1
    .param p1, "strb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 609
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->expr:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 610
    return-object p1
.end method

.method protected evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;
    .locals 1
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    .line 636
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->prepare(Lorg/apache/commons/jexl2/Interpreter;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method getType()Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
    .locals 1

    .prologue
    .line 622
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->NESTED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    return-object v0
.end method

.method public isImmediate()Z
    .locals 1

    .prologue
    .line 616
    const/4 v0, 0x0

    return v0
.end method

.method protected prepare(Lorg/apache/commons/jexl2/Interpreter;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 5
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    const/4 v3, 0x0

    .line 628
    iget-object v2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->node:Lorg/apache/commons/jexl2/parser/JexlNode;

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/Interpreter;->interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 629
    .local v1, "value":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v4

    iget-object v2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/JexlEngine;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->node:Lorg/apache/commons/jexl2/parser/JexlNode;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v2

    :goto_0
    invoke-virtual {v4, v1, v2, v3}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v0

    .line 630
    .local v0, "dnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v2, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;

    iget-object v3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-direct {v2, v3, v1, v0, p0}, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    return-object v2

    .end local v0    # "dnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_0
    move-object v2, v3

    .line 629
    goto :goto_0
.end method
