.class public Lorg/apache/commons/jexl2/ExpressionImpl;
.super Ljava/lang/Object;
.source "ExpressionImpl.java"

# interfaces
.implements Lorg/apache/commons/jexl2/Expression;
.implements Lorg/apache/commons/jexl2/Script;


# instance fields
.field protected final expression:Ljava/lang/String;

.field protected final jexl:Lorg/apache/commons/jexl2/JexlEngine;

.field protected final script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;


# direct methods
.method protected constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;Ljava/lang/String;Lorg/apache/commons/jexl2/parser/ASTJexlScript;)V
    .locals 0
    .param p1, "engine"    # Lorg/apache/commons/jexl2/JexlEngine;
    .param p2, "expr"    # Ljava/lang/String;
    .param p3, "ref"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    .line 52
    iput-object p2, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->expression:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    .line 54
    return-void
.end method


# virtual methods
.method public callable(Lorg/apache/commons/jexl2/JexlContext;)Ljava/util/concurrent/Callable;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/JexlContext;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/jexl2/ExpressionImpl;->callable(Lorg/apache/commons/jexl2/JexlContext;[Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object v0

    return-object v0
.end method

.method public varargs callable(Lorg/apache/commons/jexl2/JexlContext;[Ljava/lang/Object;)Ljava/util/concurrent/Callable;
    .locals 2
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p2, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/JexlContext;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v1, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v0

    .line 158
    .local v0, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v1, p2}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 160
    new-instance v1, Lorg/apache/commons/jexl2/ExpressionImpl$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/commons/jexl2/ExpressionImpl$1;-><init>(Lorg/apache/commons/jexl2/ExpressionImpl;Lorg/apache/commons/jexl2/Interpreter;)V

    return-object v1
.end method

.method public dump()Ljava/lang/String;
    .locals 5

    .prologue
    .line 72
    new-instance v1, Lorg/apache/commons/jexl2/Debugger;

    invoke-direct {v1}, Lorg/apache/commons/jexl2/Debugger;-><init>()V

    .line 73
    .local v1, "debug":Lorg/apache/commons/jexl2/Debugger;
    iget-object v2, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v1, v2}, Lorg/apache/commons/jexl2/Debugger;->debug(Lorg/apache/commons/jexl2/parser/JexlNode;)Z

    move-result v0

    .line 74
    .local v0, "d":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->data()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " /*"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->start()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->end()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "*/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_0
    const-string v2, "/*?:?*/ "

    goto :goto_0
.end method

.method public evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;
    .locals 4
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v2, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetNumChildren()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_0

    .line 65
    :goto_0
    return-object v1

    .line 63
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v2, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v0

    .line 64
    .local v0, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    iget-object v2, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v2, v1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 65
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public execute(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;
    .locals 3
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 105
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v1, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v0

    .line 106
    .local v0, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    iget-object v2, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v2, v1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 107
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public varargs execute(Lorg/apache/commons/jexl2/JexlContext;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 115
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v1, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v0

    .line 116
    .local v0, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v1, p2}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 117
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public getExpression()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->expression:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalVariables()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->getLocalVariables()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParameters()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->getParameters()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/ExpressionImpl;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVariables()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0, p0}, Lorg/apache/commons/jexl2/JexlEngine;->getVariables(Lorg/apache/commons/jexl2/Script;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/ExpressionImpl;->getExpression()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "expr":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v0, ""

    .end local v0    # "expr":Ljava/lang/String;
    :cond_0
    return-object v0
.end method
