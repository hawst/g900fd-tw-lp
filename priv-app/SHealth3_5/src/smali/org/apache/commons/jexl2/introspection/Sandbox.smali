.class public final Lorg/apache/commons/jexl2/introspection/Sandbox;
.super Ljava/lang/Object;
.source "Sandbox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;,
        Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;,
        Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;,
        Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    }
.end annotation


# static fields
.field private static final ALL_WHITE:Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

.field private static final WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;


# instance fields
.field private final sandbox:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 188
    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$1;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$1;-><init>()V

    sput-object v0, Lorg/apache/commons/jexl2/introspection/Sandbox;->WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    .line 349
    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    sget-object v1, Lorg/apache/commons/jexl2/introspection/Sandbox;->WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    sget-object v2, Lorg/apache/commons/jexl2/introspection/Sandbox;->WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    sget-object v3, Lorg/apache/commons/jexl2/introspection/Sandbox;->WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;-><init>(Lorg/apache/commons/jexl2/introspection/Sandbox$Names;Lorg/apache/commons/jexl2/introspection/Sandbox$Names;Lorg/apache/commons/jexl2/introspection/Sandbox$Names;)V

    sput-object v0, Lorg/apache/commons/jexl2/introspection/Sandbox;->ALL_WHITE:Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/commons/jexl2/introspection/Sandbox;-><init>(Ljava/util/Map;)V

    .line 69
    return-void
.end method

.method protected constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lorg/apache/commons/jexl2/introspection/Sandbox;->sandbox:Ljava/util/Map;

    .line 77
    return-void
.end method

.method static synthetic access$000()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lorg/apache/commons/jexl2/introspection/Sandbox;->WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    return-object v0
.end method


# virtual methods
.method public black(Ljava/lang/String;)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    .locals 1
    .param p1, "clazz"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 380
    invoke-virtual {p0, p1, v0, v0, v0}, Lorg/apache/commons/jexl2/introspection/Sandbox;->permissions(Ljava/lang/String;ZZZ)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    move-result-object v0

    return-object v0
.end method

.method public execute(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/jexl2/introspection/Sandbox;->execute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public execute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "clazz"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 146
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/Sandbox;->sandbox:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    .line 147
    .local v0, "permissions":Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    if-nez v0, :cond_0

    .line 150
    .end local p2    # "name":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->execute()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/commons/jexl2/introspection/Sandbox$Names;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public get(Ljava/lang/String;)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    .locals 2
    .param p1, "clazz"    # Ljava/lang/String;

    .prologue
    .line 389
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/Sandbox;->sandbox:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    .line 390
    .local v0, "permissions":Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    if-nez v0, :cond_0

    .line 391
    sget-object v0, Lorg/apache/commons/jexl2/introspection/Sandbox;->ALL_WHITE:Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    .line 393
    .end local v0    # "permissions":Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    :cond_0
    return-object v0
.end method

.method public permissions(Ljava/lang/String;ZZZ)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    .locals 2
    .param p1, "clazz"    # Ljava/lang/String;
    .param p2, "readFlag"    # Z
    .param p3, "writeFlag"    # Z
    .param p4, "executeFlag"    # Z

    .prologue
    .line 360
    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    invoke-direct {v0, p2, p3, p4}, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;-><init>(ZZZ)V

    .line 361
    .local v0, "box":Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/Sandbox;->sandbox:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    return-object v0
.end method

.method public read(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/jexl2/introspection/Sandbox;->read(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public read(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "clazz"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/Sandbox;->sandbox:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    .line 97
    .local v0, "permissions":Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    if-nez v0, :cond_0

    .line 100
    .end local p2    # "name":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->read()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/commons/jexl2/introspection/Sandbox$Names;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public white(Ljava/lang/String;)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    .locals 1
    .param p1, "clazz"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 371
    invoke-virtual {p0, p1, v0, v0, v0}, Lorg/apache/commons/jexl2/introspection/Sandbox;->permissions(Ljava/lang/String;ZZZ)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/jexl2/introspection/Sandbox;->write(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "clazz"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 121
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/Sandbox;->sandbox:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;

    .line 122
    .local v0, "permissions":Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    if-nez v0, :cond_0

    .line 125
    .end local p2    # "name":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->write()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/commons/jexl2/introspection/Sandbox$Names;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method
