.class public abstract Lorg/apache/commons/jexl2/parser/JexlNode;
.super Lorg/apache/commons/jexl2/parser/SimpleNode;
.source "JexlNode.java"

# interfaces
.implements Lorg/apache/commons/jexl2/JexlInfo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/parser/JexlNode$Literal;
    }
.end annotation


# instance fields
.field public image:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/SimpleNode;-><init>(I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/Parser;I)V
    .locals 0
    .param p1, "p"    # Lorg/apache/commons/jexl2/parser/Parser;
    .param p2, "id"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/parser/SimpleNode;-><init>(Lorg/apache/commons/jexl2/parser/Parser;I)V

    .line 43
    return-void
.end method


# virtual methods
.method public debugInfo()Lorg/apache/commons/jexl2/DebugInfo;
    .locals 2

    .prologue
    .line 47
    move-object v0, p0

    .line 48
    .local v0, "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    :goto_0
    if-eqz v0, :cond_1

    .line 49
    iget-object v1, v0, Lorg/apache/commons/jexl2/parser/JexlNode;->value:Ljava/lang/Object;

    instance-of v1, v1, Lorg/apache/commons/jexl2/DebugInfo;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, v0, Lorg/apache/commons/jexl2/parser/JexlNode;->value:Ljava/lang/Object;

    check-cast v1, Lorg/apache/commons/jexl2/DebugInfo;

    .line 54
    :goto_1
    return-object v1

    .line 52
    :cond_0
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public debugString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/parser/JexlNode;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v0

    .line 60
    .local v0, "info":Lorg/apache/commons/jexl2/DebugInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/DebugInfo;->debugString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public final isConstant()Z
    .locals 1

    .prologue
    .line 69
    instance-of v0, p0, Lorg/apache/commons/jexl2/parser/JexlNode$Literal;

    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->isConstant(Z)Z

    move-result v0

    return v0
.end method

.method protected isConstant(Z)Z
    .locals 8
    .param p1, "literal"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 73
    if-eqz p1, :cond_0

    .line 74
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/JexlNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    if-eqz v7, :cond_4

    .line 75
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/JexlNode;->children:[Lorg/apache/commons/jexl2/parser/JexlNode;

    .local v0, "arr$":[Lorg/apache/commons/jexl2/parser/JexlNode;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v1, v0, v2

    .line 76
    .local v1, "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    instance-of v7, v1, Lorg/apache/commons/jexl2/parser/ASTReference;

    if-eqz v7, :cond_1

    .line 77
    invoke-virtual {v1, v6}, Lorg/apache/commons/jexl2/parser/JexlNode;->isConstant(Z)Z

    move-result v3

    .line 78
    .local v3, "is":Z
    if-nez v3, :cond_2

    .line 93
    .end local v0    # "arr$":[Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v1    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v2    # "i$":I
    .end local v3    # "is":Z
    .end local v4    # "len$":I
    :cond_0
    :goto_1
    return v5

    .line 81
    .restart local v0    # "arr$":[Lorg/apache/commons/jexl2/parser/JexlNode;
    .restart local v1    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    .restart local v2    # "i$":I
    .restart local v4    # "len$":I
    :cond_1
    instance-of v7, v1, Lorg/apache/commons/jexl2/parser/ASTMapEntry;

    if-eqz v7, :cond_3

    .line 82
    invoke-virtual {v1, v6}, Lorg/apache/commons/jexl2/parser/JexlNode;->isConstant(Z)Z

    move-result v3

    .line 83
    .restart local v3    # "is":Z
    if-eqz v3, :cond_0

    .line 75
    .end local v3    # "is":Z
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 86
    :cond_3
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->isConstant()Z

    move-result v7

    if-nez v7, :cond_2

    goto :goto_1

    .end local v0    # "arr$":[Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v1    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    :cond_4
    move v5, v6

    .line 91
    goto :goto_1
.end method
