.class final enum Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
.super Ljava/lang/Enum;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ExpressionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

.field public static final enum COMPOSITE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

.field public static final enum CONSTANT:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

.field public static final enum DEFERRED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

.field public static final enum IMMEDIATE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

.field public static final enum NESTED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;


# instance fields
.field private final index:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 124
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    const-string v1, "CONSTANT"

    invoke-direct {v0, v1, v4, v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->CONSTANT:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    .line 126
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    const-string v1, "IMMEDIATE"

    invoke-direct {v0, v1, v5, v5}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->IMMEDIATE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    .line 128
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    const-string v1, "DEFERRED"

    invoke-direct {v0, v1, v3, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->DEFERRED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    .line 130
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    const-string v1, "NESTED"

    invoke-direct {v0, v1, v6, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->NESTED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    .line 132
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    const-string v1, "COMPOSITE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v7, v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->COMPOSITE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    .line 122
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->CONSTANT:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->IMMEDIATE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->DEFERRED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->NESTED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->COMPOSITE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    aput-object v1, v0, v7

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->$VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "idx"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 141
    iput p3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->index:I

    .line 142
    return-void
.end method

.method static synthetic access$000(Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    .prologue
    .line 122
    iget v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->index:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 122
    const-class v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    return-object v0
.end method

.method public static final values()[Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->$VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    invoke-virtual {v0}, [Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    return-object v0
.end method
