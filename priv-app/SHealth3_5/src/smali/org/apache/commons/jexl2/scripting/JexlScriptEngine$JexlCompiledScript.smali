.class final Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;
.super Ljavax/script/CompiledScript;
.source "JexlScriptEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "JexlCompiledScript"
.end annotation


# instance fields
.field private final script:Lorg/apache/commons/jexl2/Script;

.field final synthetic this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;


# direct methods
.method private constructor <init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Lorg/apache/commons/jexl2/Script;)V
    .locals 0
    .param p2, "theScript"    # Lorg/apache/commons/jexl2/Script;

    .prologue
    .line 345
    iput-object p1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    invoke-direct {p0}, Ljavax/script/CompiledScript;-><init>()V

    .line 346
    iput-object p2, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;->script:Lorg/apache/commons/jexl2/Script;

    .line 347
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Lorg/apache/commons/jexl2/Script;Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
    .param p2, "x1"    # Lorg/apache/commons/jexl2/Script;
    .param p3, "x2"    # Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$1;

    .prologue
    .line 337
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;-><init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Lorg/apache/commons/jexl2/Script;)V

    return-void
.end method


# virtual methods
.method public eval(Ljavax/script/ScriptContext;)Ljava/lang/Object;
    .locals 4
    .param p1, "context"    # Ljavax/script/ScriptContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/script/ScriptException;
        }
    .end annotation

    .prologue
    .line 359
    const-string v2, "context"

    const/16 v3, 0x64

    invoke-interface {p1, v2, p1, v3}, Ljavax/script/ScriptContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 361
    :try_start_0
    new-instance v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;

    iget-object v2, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    const/4 v3, 0x0

    invoke-direct {v0, v2, p1, v3}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlContextWrapper;-><init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;Ljavax/script/ScriptContext;Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$1;)V

    .line 362
    .local v0, "ctxt":Lorg/apache/commons/jexl2/JexlContext;
    iget-object v2, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;->script:Lorg/apache/commons/jexl2/Script;

    invoke-interface {v2, v0}, Lorg/apache/commons/jexl2/Script;->execute(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 363
    .end local v0    # "ctxt":Lorg/apache/commons/jexl2/JexlContext;
    :catch_0
    move-exception v1

    .line 364
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljavax/script/ScriptException;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/script/ScriptException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getEngine()Ljavax/script/ScriptEngine;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlCompiledScript;->script:Lorg/apache/commons/jexl2/Script;

    invoke-interface {v0}, Lorg/apache/commons/jexl2/Script;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
