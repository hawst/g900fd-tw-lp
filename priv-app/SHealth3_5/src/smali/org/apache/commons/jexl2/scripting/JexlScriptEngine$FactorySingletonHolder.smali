.class Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$FactorySingletonHolder;
.super Ljava/lang/Object;
.source "JexlScriptEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FactorySingletonHolder"
.end annotation


# static fields
.field private static final DEFAULT_FACTORY:Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 271
    new-instance v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;-><init>()V

    sput-object v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$FactorySingletonHolder;->DEFAULT_FACTORY:Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$FactorySingletonHolder;->DEFAULT_FACTORY:Lorg/apache/commons/jexl2/scripting/JexlScriptEngineFactory;

    return-object v0
.end method
