.class public final Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
.super Ljava/lang/Object;
.source "Sandbox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/introspection/Sandbox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Permissions"
.end annotation


# instance fields
.field private final execute:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

.field private final read:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

.field private final write:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/introspection/Sandbox$Names;Lorg/apache/commons/jexl2/introspection/Sandbox$Names;Lorg/apache/commons/jexl2/introspection/Sandbox$Names;)V
    .locals 0
    .param p1, "nread"    # Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    .param p2, "nwrite"    # Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    .param p3, "nexecute"    # Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    if-eqz p1, :cond_0

    .end local p1    # "nread":Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    :goto_0
    iput-object p1, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->read:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    .line 280
    if-eqz p2, :cond_1

    .end local p2    # "nwrite":Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    :goto_1
    iput-object p2, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->write:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    .line 281
    if-eqz p3, :cond_2

    .end local p3    # "nexecute":Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    :goto_2
    iput-object p3, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->execute:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    .line 282
    return-void

    .line 279
    .restart local p1    # "nread":Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    .restart local p2    # "nwrite":Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    .restart local p3    # "nexecute":Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    :cond_0
    # getter for: Lorg/apache/commons/jexl2/introspection/Sandbox;->WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    invoke-static {}, Lorg/apache/commons/jexl2/introspection/Sandbox;->access$000()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    move-result-object p1

    goto :goto_0

    .line 280
    .end local p1    # "nread":Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    :cond_1
    # getter for: Lorg/apache/commons/jexl2/introspection/Sandbox;->WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    invoke-static {}, Lorg/apache/commons/jexl2/introspection/Sandbox;->access$000()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    move-result-object p2

    goto :goto_1

    .line 281
    .end local p2    # "nwrite":Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    :cond_2
    # getter for: Lorg/apache/commons/jexl2/introspection/Sandbox;->WHITE_NAMES:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    invoke-static {}, Lorg/apache/commons/jexl2/introspection/Sandbox;->access$000()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    move-result-object p3

    goto :goto_2
.end method

.method constructor <init>(ZZZ)V
    .locals 3
    .param p1, "readFlag"    # Z
    .param p2, "writeFlag"    # Z
    .param p3, "executeFlag"    # Z

    .prologue
    .line 267
    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;-><init>()V

    move-object v2, v0

    :goto_0
    if-eqz p2, :cond_1

    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;-><init>()V

    move-object v1, v0

    :goto_1
    if-eqz p3, :cond_2

    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$WhiteSet;-><init>()V

    :goto_2
    invoke-direct {p0, v2, v1, v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;-><init>(Lorg/apache/commons/jexl2/introspection/Sandbox$Names;Lorg/apache/commons/jexl2/introspection/Sandbox$Names;Lorg/apache/commons/jexl2/introspection/Sandbox$Names;)V

    .line 270
    return-void

    .line 267
    :cond_0
    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;-><init>()V

    move-object v2, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;-><init>()V

    move-object v1, v0

    goto :goto_1

    :cond_2
    new-instance v0, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/introspection/Sandbox$BlackSet;-><init>()V

    goto :goto_2
.end method


# virtual methods
.method public execute()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->execute:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    return-object v0
.end method

.method public varargs execute([Ljava/lang/String;)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    .locals 5
    .param p1, "mnames"    # [Ljava/lang/String;

    .prologue
    .line 315
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 316
    .local v3, "mname":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->execute:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    invoke-virtual {v4, v3}, Lorg/apache/commons/jexl2/introspection/Sandbox$Names;->add(Ljava/lang/String;)Z

    .line 315
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 318
    .end local v3    # "mname":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public read()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->read:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    return-object v0
.end method

.method public varargs read([Ljava/lang/String;)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    .locals 5
    .param p1, "pnames"    # [Ljava/lang/String;

    .prologue
    .line 290
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 291
    .local v3, "pname":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->read:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    invoke-virtual {v4, v3}, Lorg/apache/commons/jexl2/introspection/Sandbox$Names;->add(Ljava/lang/String;)Z

    .line 290
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 293
    .end local v3    # "pname":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public write()Lorg/apache/commons/jexl2/introspection/Sandbox$Names;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->write:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    return-object v0
.end method

.method public varargs write([Ljava/lang/String;)Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;
    .locals 5
    .param p1, "pnames"    # [Ljava/lang/String;

    .prologue
    .line 302
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 303
    .local v3, "pname":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/commons/jexl2/introspection/Sandbox$Permissions;->write:Lorg/apache/commons/jexl2/introspection/Sandbox$Names;

    invoke-virtual {v4, v3}, Lorg/apache/commons/jexl2/introspection/Sandbox$Names;->add(Ljava/lang/String;)Z

    .line 302
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 305
    .end local v3    # "pname":Ljava/lang/String;
    :cond_0
    return-object p0
.end method
