.class Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;
.super Ljava/lang/Object;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExpressionBuilder"
.end annotation


# instance fields
.field private final counts:[I

.field private final expressions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    const/4 v0, 0x3

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-array v1, v0, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->counts:[I

    .line 161
    new-instance v1, Ljava/util/ArrayList;

    if-gtz p1, :cond_0

    move p1, v0

    .end local p1    # "size":I
    :cond_0
    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->expressions:Ljava/util/ArrayList;

    .line 162
    return-void

    .line 160
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method add(Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 3
    .param p1, "expr"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->counts:[I

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->getType()Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    move-result-object v1

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->index:I
    invoke-static {v1}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->access$000(Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;)I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 170
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->expressions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    return-void
.end method

.method build(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 9
    .param p1, "el"    # Lorg/apache/commons/jexl2/UnifiedJEXL;
    .param p2, "source"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 180
    const/4 v5, 0x0

    .line 181
    .local v5, "sum":I
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->counts:[I

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget v1, v0, v3

    .line 182
    .local v1, "count":I
    add-int/2addr v5, v1

    .line 181
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 184
    .end local v1    # "count":I
    :cond_0
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->expressions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eq v6, v5, :cond_1

    .line 185
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "parsing algorithm error, exprs: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 186
    .local v2, "error":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->expressions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 187
    const-string v6, ", constant:"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->counts:[I

    sget-object v7, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->CONSTANT:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->index:I
    invoke-static {v7}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->access$000(Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;)I

    move-result v7

    aget v6, v6, v7

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 189
    const-string v6, ", immediate:"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->counts:[I

    sget-object v7, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->IMMEDIATE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->index:I
    invoke-static {v7}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->access$000(Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;)I

    move-result v7

    aget v6, v6, v7

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 191
    const-string v6, ", deferred:"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->counts:[I

    sget-object v7, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->DEFERRED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->index:I
    invoke-static {v7}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->access$000(Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;)I

    move-result v7

    aget v6, v6, v7

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 193
    new-instance v6, Ljava/lang/IllegalStateException;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 196
    .end local v2    # "error":Ljava/lang/StringBuilder;
    :cond_1
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->expressions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 197
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->expressions:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .line 199
    :goto_1
    return-object v6

    :cond_2
    new-instance v6, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v7, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->counts:[I

    iget-object v8, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->expressions:Ljava/util/ArrayList;

    invoke-direct {v6, p1, v7, v8, p2}, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;[ILjava/util/ArrayList;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    goto :goto_1
.end method
