.class Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;
.super Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CompositeExpression"
.end annotation


# instance fields
.field protected final exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

.field private final meta:I

.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;[ILjava/util/ArrayList;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 3
    .param p2, "counters"    # [I
    .param p4, "src"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;",
            ">;",
            "Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;>;"
    const/4 v1, 0x0

    .line 653
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    .line 654
    invoke-direct {p0, p1, p4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 655
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    iput-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .line 656
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->DEFERRED:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->index:I
    invoke-static {v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->access$000(Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;)I

    move-result v0

    aget v0, p2, v0

    if-lez v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    sget-object v2, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->IMMEDIATE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->index:I
    invoke-static {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->access$000(Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;)I

    move-result v2

    aget v2, p2, v2

    if-lez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    or-int/2addr v0, v1

    iput v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->meta:I

    .line 658
    return-void

    :cond_1
    move v0, v1

    .line 656
    goto :goto_0
.end method


# virtual methods
.method public asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 4
    .param p1, "strb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 676
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .local v0, "arr$":[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 677
    .local v1, "e":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v1, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 676
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 679
    .end local v1    # "e":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_0
    return-object p1
.end method

.method protected evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;
    .locals 5
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    .line 721
    iget-object v4, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    array-length v1, v4

    .line 722
    .local v1, "size":I
    const/4 v3, 0x0

    .line 724
    .local v3, "value":Ljava/lang/Object;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 725
    .local v2, "strb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .end local v3    # "value":Ljava/lang/Object;
    .local v0, "e":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 726
    iget-object v4, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    aget-object v4, v4, v0

    invoke-virtual {v4, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;

    move-result-object v3

    .line 727
    .restart local v3    # "value":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 728
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 725
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 731
    .end local v3    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 732
    .local v3, "value":Ljava/lang/String;
    return-object v3
.end method

.method getType()Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
    .locals 1

    .prologue
    .line 670
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;->COMPOSITE:Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;

    return-object v0
.end method

.method public getVariables()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 685
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 686
    .local v4, "refs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .local v0, "arr$":[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 687
    .local v1, "expr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v1, v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->getVariables(Ljava/util/Set;)V

    .line 686
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 689
    .end local v1    # "expr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_0
    return-object v4
.end method

.method public isImmediate()Z
    .locals 1

    .prologue
    .line 664
    iget v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->meta:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected prepare(Lorg/apache/commons/jexl2/Interpreter;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 8
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    .line 696
    iget-object v7, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->source:Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    if-eq v7, p0, :cond_0

    .line 715
    .end local p0    # "this":Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;
    :goto_0
    return-object p0

    .line 700
    .restart local p0    # "this":Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;
    :cond_0
    iget-object v7, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    array-length v6, v7

    .line 701
    .local v6, "size":I
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;

    invoke-direct {v0, v6}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;-><init>(I)V

    .line 703
    .local v0, "builder":Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;
    const/4 v2, 0x1

    .line 704
    .local v2, "eq":Z
    const/4 v1, 0x0

    .local v1, "e":I
    :goto_1
    if-ge v1, v6, :cond_3

    .line 705
    iget-object v7, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    aget-object v3, v7, v1

    .line 706
    .local v3, "expr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v3, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->prepare(Lorg/apache/commons/jexl2/Interpreter;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v4

    .line 708
    .local v4, "prepared":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    if-eqz v4, :cond_1

    .line 709
    invoke-virtual {v0, v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->add(Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 712
    :cond_1
    if-ne v3, v4, :cond_2

    const/4 v7, 0x1

    :goto_2
    and-int/2addr v2, v7

    .line 704
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 712
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    .line 714
    .end local v3    # "expr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .end local v4    # "prepared":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_3
    if-eqz v2, :cond_4

    move-object v5, p0

    .local v5, "ready":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :goto_3
    move-object p0, v5

    .line 715
    goto :goto_0

    .line 714
    .end local v5    # "ready":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_4
    iget-object v7, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-virtual {v0, v7, p0}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->build(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v5

    goto :goto_3
.end method
