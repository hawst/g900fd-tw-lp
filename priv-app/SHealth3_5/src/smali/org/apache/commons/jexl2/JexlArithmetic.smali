.class public Lorg/apache/commons/jexl2/JexlArithmetic;
.super Ljava/lang/Object;
.source "JexlArithmetic.java"


# static fields
.field protected static final BIGD_DOUBLE_MAX_VALUE:Ljava/math/BigDecimal;

.field protected static final BIGD_DOUBLE_MIN_VALUE:Ljava/math/BigDecimal;

.field protected static final BIGD_SCALE:I = -0x1

.field protected static final BIGI_LONG_MAX_VALUE:Ljava/math/BigInteger;

.field protected static final BIGI_LONG_MIN_VALUE:Ljava/math/BigInteger;


# instance fields
.field protected final mathContext:Ljava/math/MathContext;

.field protected final mathScale:I

.field private volatile strict:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGD_DOUBLE_MAX_VALUE:Ljava/math/BigDecimal;

    .line 50
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGD_DOUBLE_MIN_VALUE:Ljava/math/BigDecimal;

    .line 52
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MAX_VALUE:Ljava/math/BigInteger;

    .line 54
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MIN_VALUE:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "lenient"    # Z

    .prologue
    .line 80
    sget-object v0, Ljava/math/MathContext;->DECIMAL128:Ljava/math/MathContext;

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;-><init>(ZLjava/math/MathContext;I)V

    .line 81
    return-void
.end method

.method public constructor <init>(ZLjava/math/MathContext;I)V
    .locals 1
    .param p1, "lenient"    # Z
    .param p2, "bigdContext"    # Ljava/math/MathContext;
    .param p3, "bigdScale"    # I

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->strict:Z

    .line 92
    iput-object p2, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->mathContext:Ljava/math/MathContext;

    .line 93
    iput p3, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->mathScale:I

    .line 94
    return-void

    .line 91
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 384
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 385
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v6

    .line 411
    :goto_0
    return-object v6

    .line 390
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 391
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 392
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v3

    .line 393
    .local v3, "r":D
    new-instance v6, Ljava/lang/Double;

    add-double v7, v0, v3

    invoke-direct {v6, v7, v8}, Ljava/lang/Double;-><init>(D)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 409
    .end local v0    # "l":D
    .end local v3    # "r":D
    :catch_0
    move-exception v2

    .line 411
    .local v2, "nfe":Ljava/lang/NumberFormatException;
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 397
    .end local v2    # "nfe":Ljava/lang/NumberFormatException;
    :cond_2
    :try_start_1
    instance-of v6, p1, Ljava/math/BigDecimal;

    if-nez v6, :cond_3

    instance-of v6, p2, Ljava/math/BigDecimal;

    if-eqz v6, :cond_4

    .line 398
    :cond_3
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 399
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 400
    .local v3, "r":Ljava/math/BigDecimal;
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v6

    invoke-virtual {v0, v3, v6}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v5

    .line 401
    .local v5, "result":Ljava/math/BigDecimal;
    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigDecimal(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/lang/Number;

    move-result-object v6

    goto :goto_0

    .line 405
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v3    # "r":Ljava/math/BigDecimal;
    .end local v5    # "result":Ljava/math/BigDecimal;
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 406
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v3

    .line 407
    .local v3, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    .line 408
    .local v5, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    goto :goto_0
.end method

.method public bitwiseAnd(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 637
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 638
    .local v0, "l":J
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v2

    .line 639
    .local v2, "r":J
    and-long v4, v0, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4
.end method

.method public bitwiseComplement(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 675
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 676
    .local v0, "l":J
    const-wide/16 v2, -0x1

    xor-long/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    return-object v2
.end method

.method public bitwiseOr(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 650
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 651
    .local v0, "l":J
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v2

    .line 652
    .local v2, "r":J
    or-long v4, v0, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4
.end method

.method public bitwiseXor(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 663
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 664
    .local v0, "l":J
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v2

    .line 665
    .local v2, "r":J
    xor-long v4, v0, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4
.end method

.method protected compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)I
    .locals 11
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;
    .param p3, "operator"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v8, -0x1

    .line 689
    if-eqz p1, :cond_11

    if-eqz p2, :cond_11

    .line 690
    instance-of v10, p1, Ljava/math/BigDecimal;

    if-nez v10, :cond_0

    instance-of v10, p2, Ljava/math/BigDecimal;

    if-eqz v10, :cond_2

    .line 691
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 692
    .local v1, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 693
    .local v4, "r":Ljava/math/BigDecimal;
    invoke-virtual {v1, v4}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v7

    .line 738
    .end local v1    # "l":Ljava/math/BigDecimal;
    .end local v4    # "r":Ljava/math/BigDecimal;
    :cond_1
    :goto_0
    return v7

    .line 694
    :cond_2
    instance-of v10, p1, Ljava/math/BigInteger;

    if-nez v10, :cond_3

    instance-of v10, p2, Ljava/math/BigInteger;

    if-eqz v10, :cond_4

    .line 695
    :cond_3
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v1

    .line 696
    .local v1, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v4

    .line 697
    .local v4, "r":Ljava/math/BigInteger;
    invoke-virtual {v1, v4}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v7

    goto :goto_0

    .line 698
    .end local v1    # "l":Ljava/math/BigInteger;
    .end local v4    # "r":Ljava/math/BigInteger;
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPoint(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPoint(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 699
    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    .line 700
    .local v2, "lhs":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v5

    .line 701
    .local v5, "rhs":D
    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 702
    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v9

    if-nez v9, :cond_1

    move v7, v8

    .line 705
    goto :goto_0

    .line 707
    :cond_6
    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v10

    if-eqz v10, :cond_7

    move v7, v9

    .line 709
    goto :goto_0

    .line 710
    :cond_7
    cmpg-double v10, v2, v5

    if-gez v10, :cond_8

    move v7, v8

    .line 711
    goto :goto_0

    .line 712
    :cond_8
    cmpl-double v8, v2, v5

    if-lez v8, :cond_1

    move v7, v9

    .line 713
    goto :goto_0

    .line 717
    .end local v2    # "lhs":D
    .end local v5    # "rhs":D
    :cond_9
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isNumberable(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_a

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isNumberable(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 718
    :cond_a
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v2

    .line 719
    .local v2, "lhs":J
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v5

    .line 720
    .local v5, "rhs":J
    cmp-long v10, v2, v5

    if-gez v10, :cond_b

    move v7, v8

    .line 721
    goto :goto_0

    .line 722
    :cond_b
    cmp-long v8, v2, v5

    if-lez v8, :cond_1

    move v7, v9

    .line 723
    goto :goto_0

    .line 727
    .end local v2    # "lhs":J
    .end local v5    # "rhs":J
    :cond_c
    instance-of v9, p1, Ljava/lang/String;

    if-nez v9, :cond_d

    instance-of v9, p2, Ljava/lang/String;

    if-eqz v9, :cond_e

    .line 728
    :cond_d
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    goto :goto_0

    .line 729
    :cond_e
    const-string v9, "=="

    invoke-virtual {v9, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 730
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    move v7, v8

    goto/16 :goto_0

    .line 731
    :cond_f
    instance-of v7, p1, Ljava/lang/Comparable;

    if-eqz v7, :cond_10

    move-object v0, p1

    .line 733
    check-cast v0, Ljava/lang/Comparable;

    .line 734
    .local v0, "comparable":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Ljava/lang/Object;>;"
    invoke-interface {v0, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v7

    goto/16 :goto_0

    .line 735
    .end local v0    # "comparable":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Ljava/lang/Object;>;"
    :cond_10
    instance-of v7, p2, Ljava/lang/Comparable;

    if-eqz v7, :cond_11

    move-object v0, p2

    .line 737
    check-cast v0, Ljava/lang/Comparable;

    .line 738
    .restart local v0    # "comparable":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Ljava/lang/Object;>;"
    invoke-interface {v0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v7

    goto/16 :goto_0

    .line 741
    .end local v0    # "comparable":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Ljava/lang/Object;>;"
    :cond_11
    new-instance v7, Ljava/lang/ArithmeticException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Object comparison:("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method protected controlNullNullOperands()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 161
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->isLenient()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    new-instance v0, Ljava/lang/ArithmeticException;

    const-string v1, "jexl.null"

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected controlNullOperand()V
    .locals 2

    .prologue
    .line 172
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->isLenient()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    new-instance v0, Ljava/lang/ArithmeticException;

    const-string v1, "jexl.null"

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_0
    return-void
.end method

.method public divide(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 423
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 424
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v5

    .line 455
    :goto_0
    return-object v5

    .line 428
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 429
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 430
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    .line 431
    .local v2, "r":D
    const-wide/16 v5, 0x0

    cmpl-double v5, v2, v5

    if-nez v5, :cond_2

    .line 432
    new-instance v5, Ljava/lang/ArithmeticException;

    const-string v6, "/"

    invoke-direct {v5, v6}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 434
    :cond_2
    new-instance v5, Ljava/lang/Double;

    div-double v6, v0, v2

    invoke-direct {v5, v6, v7}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 438
    .end local v0    # "l":D
    .end local v2    # "r":D
    :cond_3
    instance-of v5, p1, Ljava/math/BigDecimal;

    if-nez v5, :cond_4

    instance-of v5, p2, Ljava/math/BigDecimal;

    if-eqz v5, :cond_6

    .line 439
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 440
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 441
    .local v2, "r":Ljava/math/BigDecimal;
    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v5, v2}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 442
    new-instance v5, Ljava/lang/ArithmeticException;

    const-string v6, "/"

    invoke-direct {v5, v6}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 444
    :cond_5
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 445
    .local v4, "result":Ljava/math/BigDecimal;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigDecimal(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/lang/Number;

    move-result-object v5

    goto :goto_0

    .line 449
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v2    # "r":Ljava/math/BigDecimal;
    .end local v4    # "result":Ljava/math/BigDecimal;
    :cond_6
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 450
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 451
    .local v2, "r":Ljava/math/BigInteger;
    sget-object v5, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v5, v2}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 452
    new-instance v5, Ljava/lang/ArithmeticException;

    const-string v6, "/"

    invoke-direct {v5, v6}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 454
    :cond_7
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    .line 455
    .local v4, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;

    move-result-object v5

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 752
    if-ne p1, p2, :cond_1

    .line 759
    :cond_0
    :goto_0
    return v0

    .line 754
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    :cond_2
    move v0, v1

    .line 755
    goto :goto_0

    .line 756
    :cond_3
    instance-of v2, p1, Ljava/lang/Boolean;

    if-nez v2, :cond_4

    instance-of v2, p2, Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 757
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 759
    :cond_5
    const-string v2, "=="

    invoke-virtual {p0, p1, p2, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public getMathContext()Ljava/math/MathContext;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->mathContext:Ljava/math/MathContext;

    return-object v0
.end method

.method public getMathScale()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->mathScale:I

    return v0
.end method

.method public greaterThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 787
    if-eq p1, p2, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 790
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, ">"

    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public greaterThanOrEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 819
    if-ne p1, p2, :cond_1

    .line 824
    :cond_0
    :goto_0
    return v0

    .line 821
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    :cond_2
    move v0, v1

    .line 822
    goto :goto_0

    .line 824
    :cond_3
    const-string v2, ">="

    invoke-virtual {p0, p1, p2, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected isFloatingPoint(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 212
    instance-of v0, p1, Ljava/lang/Float;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isFloatingPointNumber(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 195
    instance-of v3, p1, Ljava/lang/Float;

    if-nez v3, :cond_0

    instance-of v3, p1, Ljava/lang/Double;

    if-eqz v3, :cond_2

    :cond_0
    move v1, v2

    .line 202
    :cond_1
    :goto_0
    return v1

    .line 198
    :cond_2
    instance-of v3, p1, Ljava/lang/String;

    if-eqz v3, :cond_1

    move-object v0, p1

    .line 199
    check-cast v0, Ljava/lang/String;

    .line 200
    .local v0, "string":Ljava/lang/String;
    const/16 v3, 0x2e

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ne v3, v4, :cond_3

    const/16 v3, 0x65

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ne v3, v4, :cond_3

    const/16 v3, 0x45

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v4, :cond_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method protected isFloatingPointType(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 184
    instance-of v0, p1, Ljava/lang/Float;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Double;

    if-nez v0, :cond_0

    instance-of v0, p2, Ljava/lang/Float;

    if-nez v0, :cond_0

    instance-of v0, p2, Ljava/lang/Double;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLenient()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->strict:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isNumberable(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 222
    instance-of v0, p1, Ljava/lang/Integer;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Long;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Byte;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Short;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 771
    if-eq p1, p2, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 774
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "<"

    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public lessThanOrEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 802
    if-ne p1, p2, :cond_1

    .line 807
    :cond_0
    :goto_0
    return v0

    .line 804
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    :cond_2
    move v0, v1

    .line 805
    goto :goto_0

    .line 807
    :cond_3
    const-string v2, "<="

    invoke-virtual {p0, p1, p2, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public matches(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 613
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 615
    const/4 v1, 0x1

    .line 625
    .end local p2    # "right":Ljava/lang/Object;
    :goto_0
    return v1

    .line 617
    .restart local p2    # "right":Ljava/lang/Object;
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    .line 619
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 621
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 622
    .local v0, "arg":Ljava/lang/String;
    instance-of v1, p2, Ljava/util/regex/Pattern;

    if-eqz v1, :cond_3

    .line 623
    check-cast p2, Ljava/util/regex/Pattern;

    .end local p2    # "right":Ljava/lang/Object;
    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    goto :goto_0

    .line 625
    .restart local p2    # "right":Ljava/lang/Object;
    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public mod(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 466
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 467
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v6

    .line 498
    :goto_0
    return-object v6

    .line 471
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 472
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 473
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    .line 474
    .local v2, "r":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v2, v6

    if-nez v6, :cond_2

    .line 475
    new-instance v6, Ljava/lang/ArithmeticException;

    const-string v7, "%"

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 477
    :cond_2
    new-instance v6, Ljava/lang/Double;

    rem-double v7, v0, v2

    invoke-direct {v6, v7, v8}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 481
    .end local v0    # "l":D
    .end local v2    # "r":D
    :cond_3
    instance-of v6, p1, Ljava/math/BigDecimal;

    if-nez v6, :cond_4

    instance-of v6, p2, Ljava/math/BigDecimal;

    if-eqz v6, :cond_6

    .line 482
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 483
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 484
    .local v2, "r":Ljava/math/BigDecimal;
    sget-object v6, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v6, v2}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 485
    new-instance v6, Ljava/lang/ArithmeticException;

    const-string v7, "%"

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 487
    :cond_5
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v6

    invoke-virtual {v0, v2, v6}, Ljava/math/BigDecimal;->remainder(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 488
    .local v4, "remainder":Ljava/math/BigDecimal;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigDecimal(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/lang/Number;

    move-result-object v6

    goto :goto_0

    .line 492
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v2    # "r":Ljava/math/BigDecimal;
    .end local v4    # "remainder":Ljava/math/BigDecimal;
    :cond_6
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 493
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 494
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    .line 495
    .local v5, "result":Ljava/math/BigInteger;
    sget-object v6, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v6, v2}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 496
    new-instance v6, Ljava/lang/ArithmeticException;

    const-string v7, "%"

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 498
    :cond_7
    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;

    move-result-object v6

    goto :goto_0
.end method

.method public multiply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 508
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 509
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v5

    .line 531
    :goto_0
    return-object v5

    .line 513
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 514
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 515
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    .line 516
    .local v2, "r":D
    new-instance v5, Ljava/lang/Double;

    mul-double v6, v0, v2

    invoke-direct {v5, v6, v7}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 520
    .end local v0    # "l":D
    .end local v2    # "r":D
    :cond_2
    instance-of v5, p1, Ljava/math/BigDecimal;

    if-nez v5, :cond_3

    instance-of v5, p2, Ljava/math/BigDecimal;

    if-eqz v5, :cond_4

    .line 521
    :cond_3
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 522
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 523
    .local v2, "r":Ljava/math/BigDecimal;
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 524
    .local v4, "result":Ljava/math/BigDecimal;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigDecimal(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/lang/Number;

    move-result-object v5

    goto :goto_0

    .line 528
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v2    # "r":Ljava/math/BigDecimal;
    .end local v4    # "result":Ljava/math/BigDecimal;
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 529
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 530
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    .line 531
    .local v4, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;

    move-result-object v5

    goto :goto_0
.end method

.method public narrow(Ljava/lang/Number;)Ljava/lang/Number;
    .locals 1
    .param p1, "original"    # Ljava/lang/Number;

    .prologue
    .line 1061
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowNumber(Ljava/lang/Number;Ljava/lang/Class;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method protected narrowAccept(Ljava/lang/Class;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1072
    .local p1, "narrow":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p2, "source":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected narrowArguments([Ljava/lang/Object;)Z
    .locals 5
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 359
    const/4 v3, 0x0

    .line 360
    .local v3, "narrowed":Z
    const/4 v0, 0x0

    .local v0, "a":I
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_2

    .line 361
    aget-object v1, p1, v0

    .line 362
    .local v1, "arg":Ljava/lang/Object;
    instance-of v4, v1, Ljava/lang/Number;

    if-eqz v4, :cond_1

    move-object v4, v1

    .line 363
    check-cast v4, Ljava/lang/Number;

    invoke-virtual {p0, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrow(Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    .line 364
    .local v2, "narg":Ljava/lang/Number;
    if-eq v2, v1, :cond_0

    .line 365
    const/4 v3, 0x1

    .line 367
    :cond_0
    aput-object v2, p1, v0

    .line 360
    .end local v2    # "narg":Ljava/lang/Number;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 370
    .end local v1    # "arg":Ljava/lang/Object;
    :cond_2
    return v3
.end method

.method protected narrowArrayType([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1, "untyped"    # [Ljava/lang/Object;

    .prologue
    .line 299
    array-length v5, p1

    .line 300
    .local v5, "size":I
    const/4 v1, 0x0

    .line 301
    .local v1, "commonClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-lez v5, :cond_7

    .line 302
    const/4 v4, 0x1

    .line 304
    .local v4, "isNumber":Z
    const/4 v8, 0x0

    .local v8, "u":I
    :goto_0
    if-ge v8, v5, :cond_5

    const-class v9, Ljava/lang/Object;

    invoke-virtual {v9, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 305
    aget-object v9, p1, v8

    if-eqz v9, :cond_4

    .line 306
    aget-object v9, p1, v8

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 308
    .local v2, "eclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez v1, :cond_1

    .line 309
    move-object v1, v2

    .line 310
    const-class v9, Ljava/lang/Number;

    invoke-virtual {v9, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v9

    and-int/2addr v4, v9

    .line 304
    .end local v2    # "eclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 311
    .restart local v2    # "eclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 313
    if-eqz v4, :cond_2

    const-class v9, Ljava/lang/Number;

    invoke-virtual {v9, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 314
    const-class v1, Ljava/lang/Number;

    goto :goto_1

    .line 318
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    .line 319
    if-nez v2, :cond_3

    .line 320
    const-class v1, Ljava/lang/Object;

    .line 321
    goto :goto_1

    .line 323
    :cond_3
    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v9

    if-eqz v9, :cond_2

    goto :goto_1

    .line 327
    .end local v2    # "eclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    .line 331
    :cond_5
    if-eqz v1, :cond_7

    const-class v9, Ljava/lang/Object;

    invoke-virtual {v9, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 333
    if-eqz v4, :cond_6

    .line 335
    :try_start_0
    const-string v9, "TYPE"

    invoke-virtual {v1, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 336
    .local v6, "type":Ljava/lang/reflect/Field;
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/lang/Class;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    .end local v6    # "type":Ljava/lang/reflect/Field;
    :cond_6
    :goto_2
    invoke-static {v1, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v7

    .line 343
    .local v7, "typed":Ljava/lang/Object;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    if-ge v3, v5, :cond_8

    .line 344
    aget-object v9, p1, v3

    invoke-static {v7, v3, v9}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 343
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .end local v3    # "i":I
    .end local v4    # "isNumber":Z
    .end local v7    # "typed":Ljava/lang/Object;
    .end local v8    # "u":I
    :cond_7
    move-object v7, p1

    .line 349
    :cond_8
    return-object v7

    .line 337
    .restart local v4    # "isNumber":Z
    .restart local v8    # "u":I
    :catch_0
    move-exception v9

    goto :goto_2
.end method

.method protected narrowBigDecimal(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/lang/Number;
    .locals 4
    .param p1, "lhs"    # Ljava/lang/Object;
    .param p2, "rhs"    # Ljava/lang/Object;
    .param p3, "bigd"    # Ljava/math/BigDecimal;

    .prologue
    .line 271
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isNumberable(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isNumberable(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 273
    :cond_0
    :try_start_0
    invoke-virtual {p3}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    .line 275
    .local v0, "l":J
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gtz v2, :cond_2

    const-wide/32 v2, -0x80000000

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    .line 276
    long-to-int v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    .line 284
    .end local v0    # "l":J
    .end local p3    # "bigd":Ljava/math/BigDecimal;
    :cond_1
    :goto_0
    return-object p3

    .line 278
    .restart local v0    # "l":J
    .restart local p3    # "bigd":Ljava/math/BigDecimal;
    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    goto :goto_0

    .line 280
    .end local v0    # "l":J
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;
    .locals 4
    .param p1, "lhs"    # Ljava/lang/Object;
    .param p2, "rhs"    # Ljava/lang/Object;
    .param p3, "bigi"    # Ljava/math/BigInteger;

    .prologue
    .line 244
    instance-of v2, p1, Ljava/math/BigInteger;

    if-nez v2, :cond_0

    instance-of v2, p2, Ljava/math/BigInteger;

    if-nez v2, :cond_0

    sget-object v2, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MAX_VALUE:Ljava/math/BigInteger;

    invoke-virtual {p3, v2}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v2

    if-gtz v2, :cond_0

    sget-object v2, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MIN_VALUE:Ljava/math/BigInteger;

    invoke-virtual {p3, v2}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v2

    if-ltz v2, :cond_0

    .line 248
    invoke-virtual {p3}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    .line 250
    .local v0, "l":J
    instance-of v2, p1, Ljava/lang/Long;

    if-nez v2, :cond_1

    instance-of v2, p2, Ljava/lang/Long;

    if-nez v2, :cond_1

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    const-wide/32 v2, -0x80000000

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 253
    long-to-int v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    .line 257
    .end local v0    # "l":J
    .end local p3    # "bigi":Ljava/math/BigInteger;
    :cond_0
    :goto_0
    return-object p3

    .line 255
    .restart local v0    # "l":J
    .restart local p3    # "bigi":Ljava/math/BigInteger;
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    goto :goto_0
.end method

.method protected narrowNumber(Ljava/lang/Number;Ljava/lang/Class;)Ljava/lang/Number;
    .locals 13
    .param p1, "original"    # Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Number;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Number;"
        }
    .end annotation

    .prologue
    .local p2, "narrow":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-wide/32 v11, 0x7fffffff

    const-wide/32 v9, -0x80000000

    .line 1083
    if-nez p1, :cond_1

    .line 1142
    .end local p1    # "original":Ljava/lang/Number;
    :cond_0
    :goto_0
    return-object p1

    .line 1086
    .restart local p1    # "original":Ljava/lang/Number;
    :cond_1
    move-object v4, p1

    .line 1087
    .local v4, "result":Ljava/lang/Number;
    instance-of v7, p1, Ljava/math/BigDecimal;

    if-eqz v7, :cond_3

    move-object v0, p1

    .line 1088
    check-cast v0, Ljava/math/BigDecimal;

    .line 1090
    .local v0, "bigd":Ljava/math/BigDecimal;
    sget-object v7, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGD_DOUBLE_MAX_VALUE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v7}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v7

    if-gtz v7, :cond_0

    .line 1094
    :try_start_0
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v2

    .line 1096
    .local v2, "l":J
    const-class v7, Ljava/lang/Integer;

    invoke-virtual {p0, p2, v7}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowAccept(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_2

    cmp-long v7, v2, v11

    if-gtz v7, :cond_2

    cmp-long v7, v2, v9

    if-ltz v7, :cond_2

    .line 1099
    long-to-int v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    .line 1100
    :cond_2
    const-class v7, Ljava/lang/Long;

    invoke-virtual {p0, p2, v7}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowAccept(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1101
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 1103
    .end local v2    # "l":J
    :catch_0
    move-exception v7

    .line 1108
    .end local v0    # "bigd":Ljava/math/BigDecimal;
    :cond_3
    instance-of v7, p1, Ljava/lang/Double;

    if-nez v7, :cond_4

    instance-of v7, p1, Ljava/lang/Float;

    if-nez v7, :cond_4

    instance-of v7, p1, Ljava/math/BigDecimal;

    if-eqz v7, :cond_6

    .line 1109
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v5

    .line 1110
    .local v5, "value":D
    const-class v7, Ljava/lang/Float;

    invoke-virtual {p0, p2, v7}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowAccept(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_5

    const-wide v7, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpg-double v7, v5, v7

    if-gtz v7, :cond_5

    const-wide/high16 v7, 0x36a0000000000000L    # 1.401298464324817E-45

    cmpl-double v7, v5, v7

    if-ltz v7, :cond_5

    .line 1113
    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    .end local v5    # "value":D
    :cond_5
    :goto_1
    move-object p1, v4

    .line 1142
    goto :goto_0

    .line 1117
    :cond_6
    instance-of v7, p1, Ljava/math/BigInteger;

    if-eqz v7, :cond_7

    move-object v1, p1

    .line 1118
    check-cast v1, Ljava/math/BigInteger;

    .line 1120
    .local v1, "bigi":Ljava/math/BigInteger;
    sget-object v7, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MAX_VALUE:Ljava/math/BigInteger;

    invoke-virtual {v1, v7}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v7

    if-gtz v7, :cond_0

    sget-object v7, Lorg/apache/commons/jexl2/JexlArithmetic;->BIGI_LONG_MIN_VALUE:Ljava/math/BigInteger;

    invoke-virtual {v1, v7}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v7

    if-ltz v7, :cond_0

    .line 1125
    .end local v1    # "bigi":Ljava/math/BigInteger;
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v5

    .line 1126
    .local v5, "value":J
    const-class v7, Ljava/lang/Byte;

    invoke-virtual {p0, p2, v7}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowAccept(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_8

    const-wide/16 v7, 0x7f

    cmp-long v7, v5, v7

    if-gtz v7, :cond_8

    const-wide/16 v7, -0x80

    cmp-long v7, v5, v7

    if-ltz v7, :cond_8

    .line 1130
    long-to-int v7, v5

    int-to-byte v7, v7

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    goto :goto_1

    .line 1131
    :cond_8
    const-class v7, Ljava/lang/Short;

    invoke-virtual {p0, p2, v7}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowAccept(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_9

    const-wide/16 v7, 0x7fff

    cmp-long v7, v5, v7

    if-gtz v7, :cond_9

    const-wide/16 v7, -0x8000

    cmp-long v7, v5, v7

    if-ltz v7, :cond_9

    .line 1134
    long-to-int v7, v5

    int-to-short v7, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    goto :goto_1

    .line 1135
    :cond_9
    const-class v7, Ljava/lang/Integer;

    invoke-virtual {p0, p2, v7}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowAccept(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_5

    cmp-long v7, v5, v11

    if-gtz v7, :cond_5

    cmp-long v7, v5, v9

    if-ltz v7, :cond_5

    .line 1138
    long-to-int v7, v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_1
.end method

.method public negate(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 574
    instance-of v10, p1, Ljava/lang/Integer;

    if-eqz v10, :cond_0

    .line 575
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 576
    .local v6, "valueAsInt":I
    neg-int v10, v6

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 599
    .end local v6    # "valueAsInt":I
    :goto_0
    return-object v10

    .line 577
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_0
    instance-of v10, p1, Ljava/lang/Double;

    if-eqz v10, :cond_1

    .line 578
    check-cast p1, Ljava/lang/Double;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    .line 579
    .local v3, "valueAsDouble":D
    new-instance v10, Ljava/lang/Double;

    neg-double v11, v3

    invoke-direct {v10, v11, v12}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 580
    .end local v3    # "valueAsDouble":D
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v10, p1, Ljava/lang/Long;

    if-eqz v10, :cond_2

    .line 581
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    neg-long v7, v10

    .line 582
    .local v7, "valueAsLong":J
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    goto :goto_0

    .line 583
    .end local v7    # "valueAsLong":J
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v10, p1, Ljava/math/BigDecimal;

    if-eqz v10, :cond_3

    move-object v0, p1

    .line 584
    check-cast v0, Ljava/math/BigDecimal;

    .line 585
    .local v0, "valueAsBigD":Ljava/math/BigDecimal;
    invoke-virtual {v0}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v10

    goto :goto_0

    .line 586
    .end local v0    # "valueAsBigD":Ljava/math/BigDecimal;
    :cond_3
    instance-of v10, p1, Ljava/math/BigInteger;

    if-eqz v10, :cond_4

    move-object v1, p1

    .line 587
    check-cast v1, Ljava/math/BigInteger;

    .line 588
    .local v1, "valueAsBigI":Ljava/math/BigInteger;
    invoke-virtual {v1}, Ljava/math/BigInteger;->negate()Ljava/math/BigInteger;

    move-result-object v10

    goto :goto_0

    .line 589
    .end local v1    # "valueAsBigI":Ljava/math/BigInteger;
    :cond_4
    instance-of v10, p1, Ljava/lang/Float;

    if-eqz v10, :cond_5

    .line 590
    check-cast p1, Ljava/lang/Float;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 591
    .local v5, "valueAsFloat":F
    new-instance v10, Ljava/lang/Float;

    neg-float v11, v5

    invoke-direct {v10, v11}, Ljava/lang/Float;-><init>(F)V

    goto :goto_0

    .line 592
    .end local v5    # "valueAsFloat":F
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_5
    instance-of v10, p1, Ljava/lang/Short;

    if-eqz v10, :cond_6

    .line 593
    check-cast p1, Ljava/lang/Short;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Short;->shortValue()S

    move-result v9

    .line 594
    .local v9, "valueAsShort":S
    neg-int v10, v9

    int-to-short v10, v10

    invoke-static {v10}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v10

    goto :goto_0

    .line 595
    .end local v9    # "valueAsShort":S
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_6
    instance-of v10, p1, Ljava/lang/Byte;

    if-eqz v10, :cond_7

    .line 596
    check-cast p1, Ljava/lang/Byte;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    .line 597
    .local v2, "valueAsByte":B
    neg-int v10, v2

    int-to-byte v10, v10

    invoke-static {v10}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v10

    goto :goto_0

    .line 598
    .end local v2    # "valueAsByte":B
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_7
    instance-of v10, p1, Ljava/lang/Boolean;

    if-eqz v10, :cond_9

    .line 599
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_8

    sget-object v10, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_8
    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 601
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_9
    new-instance v10, Ljava/lang/ArithmeticException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Object negation:("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v10
.end method

.method public roundBigDecimal(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 2
    .param p1, "number"    # Ljava/math/BigDecimal;

    .prologue
    .line 147
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathScale()I

    move-result v0

    .line 148
    .local v0, "mscale":I
    if-ltz v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/MathContext;->getRoundingMode()Ljava/math/RoundingMode;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 151
    .end local p1    # "number":Ljava/math/BigDecimal;
    :cond_0
    return-object p1
.end method

.method setLenient(Z)V
    .locals 1
    .param p1, "flag"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 110
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/commons/jexl2/JexlArithmetic;->strict:Z

    .line 111
    return-void

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "left"    # Ljava/lang/Object;
    .param p2, "right"    # Ljava/lang/Object;

    .prologue
    .line 541
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 542
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullNullOperands()Ljava/lang/Object;

    move-result-object v5

    .line 564
    :goto_0
    return-object v5

    .line 546
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->isFloatingPointNumber(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 547
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 548
    .local v0, "l":D
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v2

    .line 549
    .local v2, "r":D
    new-instance v5, Ljava/lang/Double;

    sub-double v6, v0, v2

    invoke-direct {v5, v6, v7}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 553
    .end local v0    # "l":D
    .end local v2    # "r":D
    :cond_2
    instance-of v5, p1, Ljava/math/BigDecimal;

    if-nez v5, :cond_3

    instance-of v5, p2, Ljava/math/BigDecimal;

    if-eqz v5, :cond_4

    .line 554
    :cond_3
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 555
    .local v0, "l":Ljava/math/BigDecimal;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 556
    .local v2, "r":Ljava/math/BigDecimal;
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 557
    .local v4, "result":Ljava/math/BigDecimal;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigDecimal(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/lang/Number;

    move-result-object v5

    goto :goto_0

    .line 561
    .end local v0    # "l":Ljava/math/BigDecimal;
    .end local v2    # "r":Ljava/math/BigDecimal;
    .end local v4    # "result":Ljava/math/BigDecimal;
    :cond_4
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v0

    .line 562
    .local v0, "l":Ljava/math/BigInteger;
    invoke-virtual {p0, p2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;

    move-result-object v2

    .line 563
    .local v2, "r":Ljava/math/BigInteger;
    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    .line 564
    .local v4, "result":Ljava/math/BigInteger;
    invoke-virtual {p0, p1, p2, v4}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowBigInteger(Ljava/lang/Object;Ljava/lang/Object;Ljava/math/BigInteger;)Ljava/lang/Number;

    move-result-object v5

    goto :goto_0
.end method

.method public toBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .locals 5
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 963
    instance-of v2, p1, Ljava/math/BigDecimal;

    if-eqz v2, :cond_0

    .line 964
    check-cast p1, Ljava/math/BigDecimal;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->roundBigDecimal(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 984
    :goto_0
    return-object v2

    .line 965
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_0
    if-nez p1, :cond_1

    .line 966
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 967
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    goto :goto_0

    .line 968
    :cond_1
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 969
    check-cast p1, Ljava/lang/String;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 970
    .local v1, "string":Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 971
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    goto :goto_0

    .line 973
    :cond_2
    new-instance v2, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;Ljava/math/MathContext;)V

    invoke-virtual {p0, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->roundBigDecimal(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    goto :goto_0

    .line 974
    .end local v1    # "string":Ljava/lang/String;
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v2, p1, Ljava/lang/Double;

    if-eqz v2, :cond_5

    move-object v2, p1

    .line 975
    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_4

    .line 976
    new-instance v2, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;Ljava/math/MathContext;)V

    invoke-virtual {p0, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->roundBigDecimal(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    goto :goto_0

    .line 978
    :cond_4
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    goto :goto_0

    .line 980
    :cond_5
    instance-of v2, p1, Ljava/lang/Number;

    if-eqz v2, :cond_6

    .line 981
    new-instance v2, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;Ljava/math/MathContext;)V

    invoke-virtual {p0, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->roundBigDecimal(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    goto :goto_0

    .line 982
    :cond_6
    instance-of v2, p1, Ljava/lang/Character;

    if-eqz v2, :cond_7

    .line 983
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 984
    .local v0, "i":I
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(I)V

    goto :goto_0

    .line 987
    .end local v0    # "i":I
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_7
    new-instance v2, Ljava/lang/ArithmeticException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BigDecimal coercion: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public toBigInteger(Ljava/lang/Object;)Ljava/math/BigInteger;
    .locals 5
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 926
    if-nez p1, :cond_0

    .line 927
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 928
    sget-object p1, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    .line 948
    .end local p1    # "val":Ljava/lang/Object;
    :goto_0
    return-object p1

    .line 929
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_0
    instance-of v2, p1, Ljava/math/BigInteger;

    if-eqz v2, :cond_1

    .line 930
    check-cast p1, Ljava/math/BigInteger;

    goto :goto_0

    .line 931
    :cond_1
    instance-of v2, p1, Ljava/lang/Double;

    if-eqz v2, :cond_3

    move-object v2, p1

    .line 932
    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_2

    .line 933
    new-instance v2, Ljava/math/BigInteger;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    move-object p1, v2

    goto :goto_0

    .line 935
    :cond_2
    sget-object p1, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    goto :goto_0

    .line 937
    :cond_3
    instance-of v2, p1, Ljava/lang/Number;

    if-eqz v2, :cond_4

    .line 938
    new-instance v2, Ljava/math/BigInteger;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    move-object p1, v2

    goto :goto_0

    .line 939
    :cond_4
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_6

    move-object v1, p1

    .line 940
    check-cast v1, Ljava/lang/String;

    .line 941
    .local v1, "string":Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 942
    sget-object p1, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    goto :goto_0

    .line 944
    :cond_5
    new-instance p1, Ljava/math/BigInteger;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p1, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 946
    .end local v1    # "string":Ljava/lang/String;
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_6
    instance-of v2, p1, Ljava/lang/Character;

    if-eqz v2, :cond_7

    .line 947
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 948
    .local v0, "i":I
    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object p1

    goto :goto_0

    .line 951
    .end local v0    # "i":I
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_7
    new-instance v2, Ljava/lang/ArithmeticException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BigInteger coercion: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public toBoolean(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 835
    if-nez p1, :cond_1

    .line 836
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 848
    .end local p1    # "val":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v4

    .line 838
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v5, p1, Ljava/lang/Boolean;

    if-eqz v5, :cond_2

    .line 839
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    goto :goto_0

    .line 840
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v5, p1, Ljava/lang/Number;

    if-eqz v5, :cond_4

    .line 841
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 842
    .local v0, "number":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v5

    if-nez v5, :cond_3

    const-wide/16 v5, 0x0

    cmpl-double v5, v0, v5

    if-eqz v5, :cond_3

    :goto_1
    move v4, v3

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1

    .line 843
    .end local v0    # "number":D
    :cond_4
    instance-of v5, p1, Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 844
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 845
    .local v2, "strval":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_5

    const-string v5, "false"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    :goto_2
    move v4, v3

    goto :goto_0

    :cond_5
    move v3, v4

    goto :goto_2
.end method

.method public toDouble(Ljava/lang/Object;)D
    .locals 5
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v2, 0x0

    .line 999
    if-nez p1, :cond_1

    .line 1000
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 1020
    .end local p1    # "val":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-wide v2

    .line 1002
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v4, p1, Ljava/lang/Double;

    if-eqz v4, :cond_2

    .line 1003
    check-cast p1, Ljava/lang/Double;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    goto :goto_0

    .line 1004
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v4, p1, Ljava/lang/Number;

    if-eqz v4, :cond_3

    .line 1007
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    goto :goto_0

    .line 1008
    :cond_3
    instance-of v4, p1, Ljava/lang/Boolean;

    if-eqz v4, :cond_4

    .line 1009
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    .line 1010
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_4
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 1011
    check-cast p1, Ljava/lang/String;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1012
    .local v1, "string":Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1013
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    goto :goto_0

    .line 1016
    :cond_5
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    goto :goto_0

    .line 1018
    .end local v1    # "string":Ljava/lang/String;
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_6
    instance-of v2, p1, Ljava/lang/Character;

    if-eqz v2, :cond_7

    .line 1019
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 1020
    .local v0, "i":I
    int-to-double v2, v0

    goto :goto_0

    .line 1023
    .end local v0    # "i":I
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_7
    new-instance v2, Ljava/lang/ArithmeticException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Double coercion: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public toInteger(Ljava/lang/Object;)I
    .locals 4
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 858
    if-nez p1, :cond_1

    .line 859
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 877
    .end local p1    # "val":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .line 861
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 862
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865
    check-cast p1, Ljava/lang/Double;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Double;->intValue()I

    move-result v1

    goto :goto_0

    .line 867
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_3

    .line 868
    check-cast p1, Ljava/lang/Number;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v1

    goto :goto_0

    .line 869
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 870
    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 873
    check-cast p1, Ljava/lang/String;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 874
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_4
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 875
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    .line 876
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_6
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_7

    .line 877
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v1

    goto :goto_0

    .line 880
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_7
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Integer coercion: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toLong(Ljava/lang/Object;)J
    .locals 5
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v1, 0x0

    .line 891
    if-nez p1, :cond_1

    .line 892
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 911
    .end local p1    # "val":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-wide v1

    .line 894
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 895
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 898
    check-cast p1, Ljava/lang/Double;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Double;->longValue()J

    move-result-wide v1

    goto :goto_0

    .line 900
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_3

    .line 901
    check-cast p1, Ljava/lang/Number;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    goto :goto_0

    .line 902
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 903
    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 906
    check-cast p1, Ljava/lang/String;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    goto :goto_0

    .line 908
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_4
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 909
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const-wide/16 v0, 0x1

    :goto_1
    move-wide v1, v0

    goto :goto_0

    :cond_5
    move-wide v0, v1

    goto :goto_1

    .line 910
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_6
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_7

    .line 911
    check-cast p1, Ljava/lang/Character;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    int-to-long v1, v0

    goto :goto_0

    .line 914
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_7
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Long coercion: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 1035
    if-nez p1, :cond_0

    .line 1036
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->controlNullOperand()V

    .line 1037
    const-string v1, ""

    .line 1046
    :goto_0
    return-object v1

    .line 1038
    :cond_0
    instance-of v1, p1, Ljava/lang/Double;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 1039
    check-cast v0, Ljava/lang/Double;

    .line 1040
    .local v0, "dval":Ljava/lang/Double;
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1041
    const-string v1, ""

    goto :goto_0

    .line 1043
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1046
    .end local v0    # "dval":Ljava/lang/Double;
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
