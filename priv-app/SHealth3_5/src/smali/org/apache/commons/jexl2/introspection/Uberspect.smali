.class public interface abstract Lorg/apache/commons/jexl2/introspection/Uberspect;
.super Ljava/lang/Object;
.source "Uberspect.java"


# virtual methods
.method public abstract getConstructor(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "[",
            "Ljava/lang/Object;",
            "Lorg/apache/commons/jexl2/JexlInfo;",
            ")",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;
.end method

.method public abstract getIterator(Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lorg/apache/commons/jexl2/JexlInfo;",
            ")",
            "Ljava/util/Iterator",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;
.end method

.method public abstract getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
.end method

.method public abstract getPropertySet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
.end method

.method public abstract setClassLoader(Ljava/lang/ClassLoader;)V
.end method
