.class public final Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;
.super Ljava/lang/Object;
.source "UnifiedJEXL.java"

# interfaces
.implements Lorg/apache/commons/jexl2/JexlContext;
.implements Lorg/apache/commons/jexl2/NamespaceResolver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "TemplateContext"
.end annotation


# instance fields
.field private final exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

.field private final frame:Lorg/apache/commons/jexl2/JexlEngine$Frame;

.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

.field private final wrap:Lorg/apache/commons/jexl2/JexlContext;

.field private final writer:Ljava/io/Writer;


# direct methods
.method protected constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/JexlContext;Lorg/apache/commons/jexl2/JexlEngine$Frame;[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/io/Writer;)V
    .locals 0
    .param p2, "jcontext"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p3, "jframe"    # Lorg/apache/commons/jexl2/JexlEngine$Frame;
    .param p4, "expressions"    # [Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .param p5, "out"    # Ljava/io/Writer;

    .prologue
    .line 1223
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224
    iput-object p2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->wrap:Lorg/apache/commons/jexl2/JexlContext;

    .line 1225
    iput-object p3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->frame:Lorg/apache/commons/jexl2/JexlEngine$Frame;

    .line 1226
    iput-object p4, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .line 1227
    iput-object p5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->writer:Ljava/io/Writer;

    .line 1228
    return-void
.end method

.method private doPrint(Ljava/lang/Object;)V
    .locals 9
    .param p1, "arg"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    .line 1320
    :try_start_0
    instance-of v5, p1, Ljava/lang/CharSequence;

    if-eqz v5, :cond_1

    .line 1321
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->writer:Ljava/io/Writer;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1337
    :cond_0
    :goto_0
    return-void

    .line 1322
    :cond_1
    if-eqz p1, :cond_0

    .line 1323
    const/4 v5, 0x1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v2, v5

    .line 1324
    .local v2, "value":[Ljava/lang/Object;
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/UnifiedJEXL;->getEngine()Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/JexlEngine;->getUberspect()Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-result-object v1

    .line 1325
    .local v1, "uber":Lorg/apache/commons/jexl2/introspection/Uberspect;
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->writer:Ljava/io/Writer;

    const-string/jumbo v6, "print"

    const/4 v7, 0x0

    invoke-interface {v1, v5, v6, v2, v7}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v0

    .line 1326
    .local v0, "method":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-eqz v0, :cond_2

    .line 1327
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->writer:Ljava/io/Writer;

    invoke-interface {v0, v5, v2}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1332
    .end local v0    # "method":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .end local v1    # "uber":Lorg/apache/commons/jexl2/introspection/Uberspect;
    .end local v2    # "value":[Ljava/lang/Object;
    :catch_0
    move-exception v4

    .line 1333
    .local v4, "xio":Ljava/io/IOException;
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    const-string v6, "call print"

    # invokes: Lorg/apache/commons/jexl2/UnifiedJEXL;->createException(Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    invoke-static {v5, v6, v8, v4}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$200(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;

    move-result-object v5

    throw v5

    .line 1329
    .end local v4    # "xio":Ljava/io/IOException;
    .restart local v0    # "method":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .restart local v1    # "uber":Lorg/apache/commons/jexl2/introspection/Uberspect;
    .restart local v2    # "value":[Ljava/lang/Object;
    :cond_2
    :try_start_1
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->writer:Ljava/io/Writer;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1334
    .end local v0    # "method":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .end local v1    # "uber":Lorg/apache/commons/jexl2/introspection/Uberspect;
    .end local v2    # "value":[Ljava/lang/Object;
    :catch_1
    move-exception v3

    .line 1335
    .local v3, "xany":Ljava/lang/Exception;
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    const-string v6, "invoke print"

    # invokes: Lorg/apache/commons/jexl2/UnifiedJEXL;->createException(Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    invoke-static {v5, v6, v8, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$200(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;

    move-result-object v5

    throw v5
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1240
    const-string v0, "$jexl"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1241
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->writer:Ljava/io/Writer;

    .line 1243
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->wrap:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v0, p1}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getFrame()Lorg/apache/commons/jexl2/JexlEngine$Frame;
    .locals 1

    .prologue
    .line 1235
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->frame:Lorg/apache/commons/jexl2/JexlEngine$Frame;

    return-object v0
.end method

.method public has(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1254
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->wrap:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v0, p1}, Lorg/apache/commons/jexl2/JexlContext;->has(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public varargs include(Lorg/apache/commons/jexl2/UnifiedJEXL$Template;[Ljava/lang/Object;)V
    .locals 2
    .param p1, "template"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Template;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 1275
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->wrap:Lorg/apache/commons/jexl2/JexlContext;

    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->writer:Ljava/io/Writer;

    invoke-virtual {p1, v0, v1, p2}, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->evaluate(Lorg/apache/commons/jexl2/JexlContext;Ljava/io/Writer;[Ljava/lang/Object;)V

    .line 1276
    return-void
.end method

.method public print(I)V
    .locals 2
    .param p1, "e"    # I

    .prologue
    .line 1283
    if-ltz p1, :cond_0

    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    array-length v1, v1

    if-lt p1, v1, :cond_1

    .line 1295
    :cond_0
    :goto_0
    return-void

    .line 1286
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    aget-object v0, v1, p1

    .line 1287
    .local v0, "expr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->isDeferred()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1288
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->wrap:Lorg/apache/commons/jexl2/JexlContext;

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->prepare(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v0

    .line 1290
    :cond_2
    instance-of v1, v0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;

    if-eqz v1, :cond_3

    .line 1291
    check-cast v0, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;

    .end local v0    # "expr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->printComposite(Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;)V

    goto :goto_0

    .line 1293
    .restart local v0    # "expr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_3
    invoke-virtual {v0, p0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->doPrint(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected printComposite(Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;)V
    .locals 5
    .param p1, "composite"    # Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;

    .prologue
    .line 1302
    iget-object v0, p1, Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .line 1303
    .local v0, "cexprs":[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    array-length v2, v0

    .line 1304
    .local v2, "size":I
    const/4 v3, 0x0

    .line 1305
    .local v3, "value":Ljava/lang/Object;
    const/4 v1, 0x0

    .end local v3    # "value":Ljava/lang/Object;
    .local v1, "e":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 1306
    aget-object v4, v0, v1

    invoke-virtual {v4, p0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;

    move-result-object v3

    .line 1307
    .restart local v3    # "value":Ljava/lang/Object;
    invoke-direct {p0, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->doPrint(Ljava/lang/Object;)V

    .line 1305
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1309
    .end local v3    # "value":Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method public resolveNamespace(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "ns"    # Ljava/lang/String;

    .prologue
    .line 1259
    const-string v0, "jexl"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1264
    .end local p0    # "this":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;
    :goto_0
    return-object p0

    .line 1261
    .restart local p0    # "this":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->wrap:Lorg/apache/commons/jexl2/JexlContext;

    instance-of v0, v0, Lorg/apache/commons/jexl2/NamespaceResolver;

    if-eqz v0, :cond_1

    .line 1262
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->wrap:Lorg/apache/commons/jexl2/JexlContext;

    check-cast v0, Lorg/apache/commons/jexl2/NamespaceResolver;

    invoke-interface {v0, p1}, Lorg/apache/commons/jexl2/NamespaceResolver;->resolveNamespace(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    .line 1264
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 1249
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;->wrap:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/jexl2/JexlContext;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1250
    return-void
.end method
