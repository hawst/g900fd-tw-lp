.class public final Lorg/apache/commons/jexl2/internal/MapGetExecutor;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
.source "MapGetExecutor.java"


# static fields
.field private static final MAP_GET:Ljava/lang/reflect/Method;


# instance fields
.field private final property:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 28
    const-class v0, Ljava/util/Map;

    const-string v1, "get"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->initMarker(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->MAP_GET:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 1
    .param p1, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p3, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p2}, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->discover(Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 41
    iput-object p3, p0, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->property:Ljava/lang/Object;

    .line 42
    return-void
.end method

.method static discover(Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 81
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->MAP_GET:Ljava/lang/reflect/Method;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public execute(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 58
    move-object v0, p1

    check-cast v0, Ljava/util/Map;

    .line 59
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;*>;"
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->property:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->property:Ljava/lang/Object;

    return-object v0
.end method

.method public tryExecute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 65
    if-eqz p1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->objectClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->property:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, p1

    .line 69
    check-cast v0, Ljava/util/Map;

    .line 70
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;*>;"
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 72
    .end local v0    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;*>;"
    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lorg/apache/commons/jexl2/internal/MapGetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
