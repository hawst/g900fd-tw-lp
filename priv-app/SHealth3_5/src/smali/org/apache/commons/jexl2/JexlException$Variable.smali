.class public Lorg/apache/commons/jexl2/JexlException$Variable;
.super Lorg/apache/commons/jexl2/JexlException;
.source "JexlException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/JexlException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Variable"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V
    .locals 0
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "var"    # Ljava/lang/String;

    .prologue
    .line 237
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    .line 238
    return-void
.end method


# virtual methods
.method protected detailedMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "undefined variable "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlException$Variable;->getVariable()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVariable()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    invoke-super {p0}, Lorg/apache/commons/jexl2/JexlException;->detailedMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
