.class public Lorg/apache/commons/jexl2/JexlException;
.super Ljava/lang/RuntimeException;
.source "JexlException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/JexlException$Cancel;,
        Lorg/apache/commons/jexl2/JexlException$Return;,
        Lorg/apache/commons/jexl2/JexlException$Method;,
        Lorg/apache/commons/jexl2/JexlException$Property;,
        Lorg/apache/commons/jexl2/JexlException$Variable;,
        Lorg/apache/commons/jexl2/JexlException$Parsing;,
        Lorg/apache/commons/jexl2/JexlException$Tokenization;
    }
.end annotation


# static fields
.field private static final MAX_EXCHARLOC:I = 0xa

.field private static final MIN_EXCHARLOC:I = 0x5

.field public static final NULL_OPERAND:Ljava/lang/String; = "jexl.null"


# instance fields
.field protected final transient info:Lorg/apache/commons/jexl2/JexlInfo;

.field protected final transient mark:Lorg/apache/commons/jexl2/parser/JexlNode;


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;)V
    .locals 1
    .param p1, "dbg"    # Lorg/apache/commons/jexl2/JexlInfo;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 73
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    .line 74
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "dbg"    # Lorg/apache/commons/jexl2/JexlInfo;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 83
    invoke-static {p3}, Lorg/apache/commons/jexl2/JexlException;->unwrap(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 85
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    .line 86
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 49
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    .line 51
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 60
    invoke-static {p3}, Lorg/apache/commons/jexl2/JexlException;->unwrap(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 62
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    .line 63
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static unwrap(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 1
    .param p0, "xthrow"    # Ljava/lang/Throwable;

    .prologue
    .line 94
    instance-of v0, p0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v0, :cond_1

    .line 95
    check-cast p0, Ljava/lang/reflect/InvocationTargetException;

    .end local p0    # "xthrow":Ljava/lang/Throwable;
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object p0

    .line 99
    :cond_0
    :goto_0
    return-object p0

    .line 96
    .restart local p0    # "xthrow":Ljava/lang/Throwable;
    :cond_1
    instance-of v0, p0, Ljava/lang/reflect/UndeclaredThrowableException;

    if-eqz v0, :cond_0

    .line 97
    check-cast p0, Ljava/lang/reflect/UndeclaredThrowableException;

    .end local p0    # "xthrow":Ljava/lang/Throwable;
    invoke-virtual {p0}, Ljava/lang/reflect/UndeclaredThrowableException;->getUndeclaredThrowable()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method protected detailedMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInfo([I)Ljava/lang/String;
    .locals 3
    .param p1, "offsets"    # [I

    .prologue
    .line 359
    new-instance v0, Lorg/apache/commons/jexl2/Debugger;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/Debugger;-><init>()V

    .line 360
    .local v0, "dbg":Lorg/apache/commons/jexl2/Debugger;
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Debugger;->debug(Lorg/apache/commons/jexl2/parser/JexlNode;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 361
    if-eqz p1, :cond_0

    array-length v1, p1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 362
    const/4 v1, 0x0

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/Debugger;->start()I

    move-result v2

    aput v2, p1, v1

    .line 363
    const/4 v1, 0x1

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/Debugger;->end()I

    move-result v2

    aput v2, p1, v1

    .line 365
    :cond_0
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/Debugger;->data()Ljava/lang/String;

    move-result-object v1

    .line 367
    :goto_0
    return-object v1

    :cond_1
    const-string v1, ""

    goto :goto_0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 5

    .prologue
    .line 381
    new-instance v1, Lorg/apache/commons/jexl2/Debugger;

    invoke-direct {v1}, Lorg/apache/commons/jexl2/Debugger;-><init>()V

    .line 382
    .local v1, "dbg":Lorg/apache/commons/jexl2/Debugger;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 383
    .local v2, "msg":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    if-eqz v3, :cond_0

    .line 384
    iget-object v3, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    invoke-interface {v3}, Lorg/apache/commons/jexl2/JexlInfo;->debugString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    :cond_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/JexlException;->mark:Lorg/apache/commons/jexl2/parser/JexlNode;

    invoke-virtual {v1, v3}, Lorg/apache/commons/jexl2/Debugger;->debug(Lorg/apache/commons/jexl2/parser/JexlNode;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 387
    const-string v3, "!["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->start()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 389
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->end()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 391
    const-string v3, "]: \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/Debugger;->data()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    :cond_1
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 396
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlException;->detailedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 398
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_2

    const-string v3, "jexl.null"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    if-ne v3, v4, :cond_2

    .line 399
    const-string v3, " caused by null operand"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method protected parserError(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "expr"    # Ljava/lang/String;

    .prologue
    .line 120
    iget-object v3, p0, Lorg/apache/commons/jexl2/JexlException;->info:Lorg/apache/commons/jexl2/JexlInfo;

    invoke-interface {v3}, Lorg/apache/commons/jexl2/JexlInfo;->debugInfo()Lorg/apache/commons/jexl2/DebugInfo;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/commons/jexl2/DebugInfo;->getColumn()I

    move-result v0

    .line 121
    .local v0, "begin":I
    add-int/lit8 v1, v0, 0x5

    .line 122
    .local v1, "end":I
    add-int/lit8 v0, v0, -0x5

    .line 123
    if-gez v0, :cond_0

    .line 124
    add-int/lit8 v1, v1, 0x5

    .line 125
    const/4 v0, 0x0

    .line 127
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    .line 128
    .local v2, "length":I
    const/16 v3, 0xa

    if-ge v2, v3, :cond_1

    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " error in \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 131
    .end local v2    # "length":I
    :goto_0
    return-object v3

    .restart local v2    # "length":I
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " error near \'... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-le v1, v2, :cond_2

    .end local v2    # "length":I
    :goto_1
    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ...\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .restart local v2    # "length":I
    :cond_2
    move v2, v1

    goto :goto_1
.end method
