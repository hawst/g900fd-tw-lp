.class public interface abstract Lorg/apache/commons/jexl2/parser/ParserTreeConstants;
.super Ljava/lang/Object;
.source "ParserTreeConstants.java"


# static fields
.field public static final JJTADDITIVENODE:I = 0x19

.field public static final JJTADDITIVEOPERATOR:I = 0x1a

.field public static final JJTAMBIGUOUS:I = 0x3

.field public static final JJTANDNODE:I = 0xd

.field public static final JJTARRAYACCESS:I = 0x30

.field public static final JJTARRAYLITERAL:I = 0x27

.field public static final JJTASSIGNMENT:I = 0x8

.field public static final JJTBITWISEANDNODE:I = 0x10

.field public static final JJTBITWISECOMPLNODE:I = 0x1f

.field public static final JJTBITWISEORNODE:I = 0xe

.field public static final JJTBITWISEXORNODE:I = 0xf

.field public static final JJTBLOCK:I = 0x2

.field public static final JJTCONSTRUCTORNODE:I = 0x2f

.field public static final JJTDIVNODE:I = 0x1c

.field public static final JJTEMPTYFUNCTION:I = 0x2a

.field public static final JJTEQNODE:I = 0x11

.field public static final JJTERNODE:I = 0x17

.field public static final JJTFALSENODE:I = 0x24

.field public static final JJTFOREACHSTATEMENT:I = 0x6

.field public static final JJTFUNCTIONNODE:I = 0x2c

.field public static final JJTGENODE:I = 0x16

.field public static final JJTGTNODE:I = 0x14

.field public static final JJTIDENTIFIER:I = 0x21

.field public static final JJTIFSTATEMENT:I = 0x4

.field public static final JJTJEXLSCRIPT:I = 0x0

.field public static final JJTLENODE:I = 0x15

.field public static final JJTLTNODE:I = 0x13

.field public static final JJTMAPENTRY:I = 0x29

.field public static final JJTMAPLITERAL:I = 0x28

.field public static final JJTMETHODNODE:I = 0x2d

.field public static final JJTMODNODE:I = 0x1d

.field public static final JJTMULNODE:I = 0x1b

.field public static final JJTNENODE:I = 0x12

.field public static final JJTNOTNODE:I = 0x20

.field public static final JJTNRNODE:I = 0x18

.field public static final JJTNULLLITERAL:I = 0x22

.field public static final JJTNUMBERLITERAL:I = 0x25

.field public static final JJTORNODE:I = 0xc

.field public static final JJTREFERENCE:I = 0xa

.field public static final JJTREFERENCEEXPRESSION:I = 0x31

.field public static final JJTRETURNSTATEMENT:I = 0x7

.field public static final JJTSIZEFUNCTION:I = 0x2b

.field public static final JJTSIZEMETHOD:I = 0x2e

.field public static final JJTSTRINGLITERAL:I = 0x26

.field public static final JJTTERNARYNODE:I = 0xb

.field public static final JJTTRUENODE:I = 0x23

.field public static final JJTUNARYMINUSNODE:I = 0x1e

.field public static final JJTVAR:I = 0x9

.field public static final JJTVOID:I = 0x1

.field public static final JJTWHILESTATEMENT:I = 0x5

.field public static final jjtNodeName:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 58
    const/16 v0, 0x32

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "JexlScript"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "void"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Block"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Ambiguous"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "IfStatement"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "WhileStatement"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ForeachStatement"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ReturnStatement"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Assignment"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Var"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Reference"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TernaryNode"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "OrNode"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "AndNode"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "BitwiseOrNode"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "BitwiseXorNode"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "BitwiseAndNode"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "EQNode"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "NENode"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "LTNode"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "GTNode"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "LENode"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "GENode"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "ERNode"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "NRNode"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "AdditiveNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "AdditiveOperator"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "MulNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "DivNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "ModNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "UnaryMinusNode"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "BitwiseComplNode"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "NotNode"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "Identifier"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "NullLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "TrueNode"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "FalseNode"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "NumberLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "StringLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "ArrayLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "MapLiteral"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "MapEntry"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "EmptyFunction"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "SizeFunction"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "FunctionNode"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "MethodNode"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "SizeMethod"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "ConstructorNode"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "ArrayAccess"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "ReferenceExpression"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTreeConstants;->jjtNodeName:[Ljava/lang/String;

    return-void
.end method
