.class public final Lorg/apache/commons/jexl2/UnifiedJEXL$Template;
.super Ljava/lang/Object;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Template"
.end annotation


# instance fields
.field private final exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

.field private final prefix:Ljava/lang/String;

.field private final script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

.field private final source:[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;


# direct methods
.method public varargs constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Ljava/io/Reader;[Ljava/lang/String;)V
    .locals 13
    .param p2, "directive"    # Ljava/lang/String;
    .param p3, "reader"    # Ljava/io/Reader;
    .param p4, "parms"    # [Ljava/lang/String;

    .prologue
    .line 1068
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069
    if-nez p2, :cond_0

    .line 1070
    new-instance v10, Ljava/lang/NullPointerException;

    const-string/jumbo v11, "null prefix"

    invoke-direct {v10, v11}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1072
    :cond_0
    const-string v10, "$"

    invoke-virtual {v10, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "${"

    invoke-virtual {v10, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "#"

    invoke-virtual {v10, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "#{"

    invoke-virtual {v10, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1076
    :cond_1
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": is not a valid directive pattern"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1078
    :cond_2
    if-nez p3, :cond_3

    .line 1079
    new-instance v10, Ljava/lang/NullPointerException;

    const-string/jumbo v11, "null input"

    invoke-direct {v10, v11}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1081
    :cond_3
    new-instance v7, Lorg/apache/commons/jexl2/JexlEngine$Scope;

    move-object/from16 v0, p4

    invoke-direct {v7, v0}, Lorg/apache/commons/jexl2/JexlEngine$Scope;-><init>([Ljava/lang/String;)V

    .line 1082
    .local v7, "scope":Lorg/apache/commons/jexl2/JexlEngine$Scope;
    iput-object p2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->prefix:Ljava/lang/String;

    .line 1083
    iget-object v10, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->prefix:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {p1, v10, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL;->readTemplate(Ljava/lang/String;Ljava/io/Reader;)Ljava/util/List;

    move-result-object v3

    .line 1084
    .local v3, "blocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1085
    .local v9, "uexprs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1086
    .local v8, "strb":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .line 1087
    .local v5, "nuexpr":I
    const/4 v4, -0x1

    .line 1088
    .local v4, "codeStart":I
    const/4 v1, 0x0

    .local v1, "b":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    if-ge v1, v10, :cond_6

    .line 1089
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    .line 1090
    .local v2, "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->type:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    invoke-static {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->access$300(Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;)Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    move-result-object v10

    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    if-ne v10, v11, :cond_4

    .line 1091
    const-string v10, "jexl:print("

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1092
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "nuexpr":I
    .local v6, "nuexpr":I
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1093
    const-string v10, ");"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v5, v6

    .line 1088
    .end local v6    # "nuexpr":I
    .restart local v5    # "nuexpr":I
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1096
    :cond_4
    if-gez v4, :cond_5

    .line 1097
    move v4, v1

    .line 1099
    :cond_5
    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->body:Ljava/lang/String;
    invoke-static {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->access$400(Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1103
    .end local v2    # "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    :cond_6
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/UnifiedJEXL;->getEngine()Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v10

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12, v7}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    .line 1104
    iget-object v10, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v10}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->getScope()Lorg/apache/commons/jexl2/JexlEngine$Scope;

    move-result-object v7

    .line 1106
    const/4 v1, 0x0

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    if-ge v1, v10, :cond_9

    .line 1107
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    .line 1108
    .restart local v2    # "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->type:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    invoke-static {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->access$300(Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;)Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    move-result-object v10

    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    if-ne v10, v11, :cond_7

    .line 1109
    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->body:Ljava/lang/String;
    invoke-static {v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->access$400(Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;)Ljava/lang/String;

    move-result-object v11

    if-le v1, v4, :cond_8

    move-object v10, v7

    :goto_3
    # invokes: Lorg/apache/commons/jexl2/UnifiedJEXL;->parseExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-static {p1, v11, v10}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$500(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1106
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1109
    :cond_8
    const/4 v10, 0x0

    goto :goto_3

    .line 1112
    .end local v2    # "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    :cond_9
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    invoke-interface {v3, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    iput-object v10, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->source:[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    .line 1113
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    invoke-interface {v9, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    iput-object v10, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .line 1114
    return-void
.end method

.method private constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;Lorg/apache/commons/jexl2/parser/ASTJexlScript;[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 0
    .param p2, "thePrefix"    # Ljava/lang/String;
    .param p3, "theSource"    # [Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    .param p4, "theScript"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .param p5, "theExprs"    # [Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 1123
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1124
    iput-object p2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->prefix:Ljava/lang/String;

    .line 1125
    iput-object p3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->source:[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    .line 1126
    iput-object p4, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    .line 1127
    iput-object p5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .line 1128
    return-void
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 1148
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1149
    .local v4, "strb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 1150
    .local v2, "e":I
    const/4 v0, 0x0

    .local v0, "b":I
    :goto_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->source:[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 1151
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->source:[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    aget-object v1, v5, v0

    .line 1152
    .local v1, "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->type:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    invoke-static {v1}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->access$300(Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;)Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    move-result-object v5

    sget-object v6, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    if-ne v5, v6, :cond_0

    .line 1153
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->prefix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1150
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1155
    :cond_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "e":I
    .local v3, "e":I
    aget-object v5, v5, v2

    invoke-virtual {v5, v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move v2, v3

    .end local v3    # "e":I
    .restart local v2    # "e":I
    goto :goto_1

    .line 1158
    .end local v1    # "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public evaluate(Lorg/apache/commons/jexl2/JexlContext;Ljava/io/Writer;)V
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p2, "writer"    # Ljava/io/Writer;

    .prologue
    .line 1182
    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->evaluate(Lorg/apache/commons/jexl2/JexlContext;Ljava/io/Writer;[Ljava/lang/Object;)V

    .line 1183
    return-void
.end method

.method public varargs evaluate(Lorg/apache/commons/jexl2/JexlContext;Ljava/io/Writer;[Ljava/lang/Object;)V
    .locals 8
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    .line 1192
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v1, p3}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v3

    .line 1193
    .local v3, "frame":Lorg/apache/commons/jexl2/JexlEngine$Frame;
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;

    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    iget-object v4, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/JexlContext;Lorg/apache/commons/jexl2/JexlEngine$Frame;[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/io/Writer;)V

    .line 1194
    .local v0, "tcontext":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v1}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v2

    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v1}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/JexlEngine;->isLenient()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1, v7}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;ZZ)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v6

    .line 1195
    .local v6, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    invoke-virtual {v6, v3}, Lorg/apache/commons/jexl2/Interpreter;->setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V

    .line 1196
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v6, v1}, Lorg/apache/commons/jexl2/Interpreter;->interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    .line 1197
    return-void

    .end local v6    # "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    :cond_0
    move v1, v7

    .line 1194
    goto :goto_0
.end method

.method public prepare(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/UnifiedJEXL$Template;
    .locals 11
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    const/4 v5, 0x0

    .line 1167
    iget-object v2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-object v1, v5

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v2, v1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->createFrame([Ljava/lang/Object;)Lorg/apache/commons/jexl2/JexlEngine$Frame;

    move-result-object v3

    .line 1168
    .local v3, "frame":Lorg/apache/commons/jexl2/JexlEngine$Frame;
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;

    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    iget-object v4, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/JexlContext;Lorg/apache/commons/jexl2/JexlEngine$Frame;[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/io/Writer;)V

    .line 1169
    .local v0, "tcontext":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    array-length v1, v1

    new-array v9, v1, [Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .line 1170
    .local v9, "immediates":[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    const/4 v10, 0x0

    .local v10, "e":I
    :goto_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    array-length v1, v1

    if-ge v10, v1, :cond_0

    .line 1171
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->exprs:[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    aget-object v1, v1, v10

    invoke-virtual {v1, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->prepare(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v1

    aput-object v1, v9, v10

    .line 1170
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1173
    :cond_0
    new-instance v4, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;

    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->prefix:Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->source:[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    iget-object v8, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-direct/range {v4 .. v9}, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;Lorg/apache/commons/jexl2/parser/ASTJexlScript;[Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    return-object v4
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 1132
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1133
    .local v4, "strb":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->source:[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    .local v0, "arr$":[Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 1134
    .local v1, "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->type:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    invoke-static {v1}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->access$300(Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;)Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    move-result-object v5

    sget-object v6, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    if-ne v5, v6, :cond_0

    .line 1135
    iget-object v5, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;->prefix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137
    :cond_0
    invoke-virtual {v1}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1138
    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1133
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1140
    .end local v1    # "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
