.class final Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
.super Ljava/lang/Object;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TemplateBlock"
.end annotation


# instance fields
.field private final body:Ljava/lang/String;

.field private final type:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;Ljava/lang/String;)V
    .locals 0
    .param p1, "theType"    # Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    .param p2, "theBlock"    # Ljava/lang/String;

    .prologue
    .line 992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 993
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->type:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    .line 994
    iput-object p2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->body:Ljava/lang/String;

    .line 995
    return-void
.end method

.method static synthetic access$300(Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;)Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    .prologue
    .line 981
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->type:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    return-object v0
.end method

.method static synthetic access$400(Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    .prologue
    .line 981
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->body:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 999
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;->body:Ljava/lang/String;

    return-object v0
.end method
