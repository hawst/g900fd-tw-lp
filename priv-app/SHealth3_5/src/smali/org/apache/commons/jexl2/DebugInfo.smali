.class public Lorg/apache/commons/jexl2/DebugInfo;
.super Ljava/lang/Object;
.source "DebugInfo.java"

# interfaces
.implements Lorg/apache/commons/jexl2/JexlInfo;


# instance fields
.field private final column:I

.field private final line:I

.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "tn"    # Ljava/lang/String;
    .param p2, "l"    # I
    .param p3, "c"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/commons/jexl2/DebugInfo;->name:Ljava/lang/String;

    .line 39
    iput p2, p0, Lorg/apache/commons/jexl2/DebugInfo;->line:I

    .line 40
    iput p3, p0, Lorg/apache/commons/jexl2/DebugInfo;->column:I

    .line 41
    return-void
.end method


# virtual methods
.method public debugInfo()Lorg/apache/commons/jexl2/DebugInfo;
    .locals 0

    .prologue
    .line 70
    return-object p0
.end method

.method public debugString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/DebugInfo;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColumn()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/commons/jexl2/DebugInfo;->column:I

    return v0
.end method

.method public getLine()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lorg/apache/commons/jexl2/DebugInfo;->line:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/commons/jexl2/DebugInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->name:Ljava/lang/String;

    :goto_0
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 50
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->line:I

    if-lez v1, :cond_0

    .line 51
    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    iget v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->line:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    iget v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->column:I

    if-lez v1, :cond_0

    .line 54
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    iget v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->column:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 58
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 49
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method
