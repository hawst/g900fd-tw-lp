.class abstract Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;
.super Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "JexlBasedExpression"
.end annotation


# instance fields
.field protected final expr:Ljava/lang/CharSequence;

.field protected final node:Lorg/apache/commons/jexl2/parser/JexlNode;

.field final synthetic this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;


# direct methods
.method protected constructor <init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V
    .locals 0
    .param p2, "theExpr"    # Ljava/lang/CharSequence;
    .param p3, "theNode"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p4, "theSource"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    .prologue
    .line 487
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    .line 488
    invoke-direct {p0, p1, p4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 489
    iput-object p2, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->expr:Ljava/lang/CharSequence;

    .line 490
    iput-object p3, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->node:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 491
    return-void
.end method


# virtual methods
.method public asString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 1
    .param p1, "strb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 496
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->isImmediate()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x24

    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 497
    const-string/jumbo v0, "{"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->expr:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 499
    const-string/jumbo v0, "}"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 500
    return-object p1

    .line 496
    :cond_0
    const/16 v0, 0x23

    goto :goto_0
.end method

.method protected evaluate(Lorg/apache/commons/jexl2/Interpreter;)Ljava/lang/Object;
    .locals 1
    .param p1, "interpreter"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    .line 506
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->node:Lorg/apache/commons/jexl2/parser/JexlNode;

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/Interpreter;->interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getVariables()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 512
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 513
    .local v0, "refs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->getVariables(Ljava/util/Set;)V

    .line 514
    return-object v0
.end method

.method protected getVariables(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 520
    .local p1, "refs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->this$0:Lorg/apache/commons/jexl2/UnifiedJEXL;

    # getter for: Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v0}, Lorg/apache/commons/jexl2/UnifiedJEXL;->access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;->node:Lorg/apache/commons/jexl2/parser/JexlNode;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lorg/apache/commons/jexl2/JexlEngine;->getVariables(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/util/Set;Ljava/util/List;)V

    .line 521
    return-void
.end method
