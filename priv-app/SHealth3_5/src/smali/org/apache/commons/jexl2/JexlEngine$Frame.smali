.class public final Lorg/apache/commons/jexl2/JexlEngine$Frame;
.super Ljava/lang/Object;
.source "JexlEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/JexlEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Frame"
.end annotation


# instance fields
.field private parameters:[Ljava/lang/String;

.field private registers:[Ljava/lang/Object;


# direct methods
.method constructor <init>([Ljava/lang/Object;[Ljava/lang/String;)V
    .locals 1
    .param p1, "r"    # [Ljava/lang/Object;
    .param p2, "p"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1175
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Frame;->registers:[Ljava/lang/Object;

    .line 1177
    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Frame;->parameters:[Ljava/lang/String;

    .line 1185
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlEngine$Frame;->registers:[Ljava/lang/Object;

    .line 1186
    iput-object p2, p0, Lorg/apache/commons/jexl2/JexlEngine$Frame;->parameters:[Ljava/lang/String;

    .line 1187
    return-void
.end method


# virtual methods
.method public getParameters()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1200
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Frame;->parameters:[Ljava/lang/String;

    return-object v0
.end method

.method public getRegisters()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1193
    iget-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$Frame;->registers:[Ljava/lang/Object;

    return-object v0
.end method
