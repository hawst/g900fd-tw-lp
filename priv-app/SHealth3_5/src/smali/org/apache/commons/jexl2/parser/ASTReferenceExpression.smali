.class public final Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;
.super Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
.source "ASTReferenceExpression.java"


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;-><init>(I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/Parser;I)V
    .locals 0
    .param p1, "p"    # Lorg/apache/commons/jexl2/parser/Parser;
    .param p2, "id"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;-><init>(Lorg/apache/commons/jexl2/parser/Parser;I)V

    .line 26
    return-void
.end method


# virtual methods
.method public jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "visitor"    # Lorg/apache/commons/jexl2/parser/ParserVisitor;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 31
    invoke-interface {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/ParserVisitor;->visit(Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
