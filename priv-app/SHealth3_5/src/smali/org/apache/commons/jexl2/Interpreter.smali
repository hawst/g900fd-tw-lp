.class public Lorg/apache/commons/jexl2/Interpreter;
.super Ljava/lang/Object;
.source "Interpreter.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/ParserVisitor;


# static fields
.field protected static final EMPTY_PARAMS:[Ljava/lang/Object;


# instance fields
.field protected final arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

.field protected final cache:Z

.field private volatile cancelled:Z

.field protected final context:Lorg/apache/commons/jexl2/JexlContext;

.field protected final functions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected functors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected final logger:Lorg/apache/commons/logging/Log;

.field private parameters:[Ljava/lang/String;

.field protected registers:[Ljava/lang/Object;

.field protected silent:Z

.field protected strict:Z

.field protected final uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lorg/apache/commons/jexl2/Interpreter;->EMPTY_PARAMS:[Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/commons/jexl2/Interpreter;)V
    .locals 1
    .param p1, "base"    # Lorg/apache/commons/jexl2/Interpreter;

    .prologue
    const/4 v0, 0x0

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    .line 122
    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->cancelled:Z

    .line 171
    iget-object v0, p1, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    .line 172
    iget-object v0, p1, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    .line 173
    iget-object v0, p1, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    .line 174
    iget-object v0, p1, Lorg/apache/commons/jexl2/Interpreter;->functions:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->functions:Ljava/util/Map;

    .line 175
    iget-boolean v0, p1, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    .line 176
    iget-boolean v0, p1, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    .line 177
    iget-boolean v0, p1, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    .line 178
    iget-object v0, p1, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    .line 179
    iget-object v0, p1, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 180
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;Lorg/apache/commons/jexl2/JexlContext;)V
    .locals 2
    .param p1, "jexl"    # Lorg/apache/commons/jexl2/JexlEngine;
    .param p2, "aContext"    # Lorg/apache/commons/jexl2/JexlContext;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlEngine;->isLenient()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v1

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;-><init>(Lorg/apache/commons/jexl2/JexlEngine;Lorg/apache/commons/jexl2/JexlContext;ZZ)V

    .line 143
    return-void

    .line 142
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;Lorg/apache/commons/jexl2/JexlContext;ZZ)V
    .locals 3
    .param p1, "jexl"    # Lorg/apache/commons/jexl2/JexlEngine;
    .param p2, "aContext"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p3, "strictFlag"    # Z
    .param p4, "silentFlag"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object v2, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    .line 122
    iput-object v2, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 129
    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->cancelled:Z

    .line 154
    iget-object v1, p1, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    iput-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    .line 155
    iget-object v1, p1, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    iput-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    .line 156
    iget-object v1, p1, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    iput-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    .line 157
    iget-object v1, p1, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    iput-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->functions:Ljava/util/Map;

    .line 158
    iput-boolean p3, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    .line 159
    iput-boolean p4, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    .line 160
    iget-object v1, p1, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    .line 161
    if-eqz p2, :cond_1

    .end local p2    # "aContext":Lorg/apache/commons/jexl2/JexlContext;
    :goto_0
    iput-object p2, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    .line 162
    iput-object v2, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 163
    return-void

    .line 161
    .restart local p2    # "aContext":Lorg/apache/commons/jexl2/JexlContext;
    :cond_1
    sget-object p2, Lorg/apache/commons/jexl2/JexlEngine;->EMPTY_CONTEXT:Lorg/apache/commons/jexl2/JexlContext;

    goto :goto_0
.end method

.method private call(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/ASTIdentifier;I)Ljava/lang/Object;
    .locals 21
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "bean"    # Ljava/lang/Object;
    .param p3, "methodNode"    # Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .param p4, "argb"    # I

    .prologue
    .line 1008
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/jexl2/Interpreter;->isCancelled()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 1009
    new-instance v19, Lorg/apache/commons/jexl2/JexlException$Cancel;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/apache/commons/jexl2/JexlException$Cancel;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V

    throw v19

    .line 1011
    :cond_0
    move-object/from16 v0, p3

    iget-object v14, v0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    .line 1013
    .local v14, "methodName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v19

    sub-int v4, v19, p4

    .line 1014
    .local v4, "argc":I
    new-array v5, v4, [Ljava/lang/Object;

    .line 1015
    .local v5, "argv":[Ljava/lang/Object;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-ge v12, v4, :cond_1

    .line 1016
    add-int v19, v12, p4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    aput-object v19, v5, v12

    .line 1015
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 1019
    :cond_1
    const/16 v17, 0x0

    .line 1022
    .local v17, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    move/from16 v19, v0

    if-eqz v19, :cond_3

    .line 1023
    invoke-virtual/range {p1 .. p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetValue()Ljava/lang/Object;

    move-result-object v7

    .line 1024
    .local v7, "cached":Ljava/lang/Object;
    instance-of v0, v7, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move/from16 v19, v0

    if-eqz v19, :cond_3

    .line 1025
    move-object v0, v7

    check-cast v0, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-object v13, v0

    .line 1026
    .local v13, "me":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    move-object/from16 v0, p2

    invoke-interface {v13, v14, v0, v5}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->tryInvoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 1027
    .local v9, "eval":Ljava/lang/Object;
    invoke-interface {v13, v9}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->tryFailed(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_3

    .line 1080
    .end local v5    # "argv":[Ljava/lang/Object;
    .end local v7    # "cached":Ljava/lang/Object;
    .end local v9    # "eval":Ljava/lang/Object;
    .end local v13    # "me":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :cond_2
    :goto_1
    return-object v9

    .line 1032
    .restart local v5    # "argv":[Ljava/lang/Object;
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    .line 1033
    .local v6, "cacheable":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-interface {v0, v1, v14, v5, v2}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v16

    .line 1035
    .local v16, "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-nez v16, :cond_a

    .line 1036
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1037
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-interface {v0, v1, v14, v5, v2}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v16

    .line 1039
    :cond_4
    if-nez v16, :cond_a

    .line 1040
    const/4 v10, 0x0

    .line 1042
    .local v10, "functor":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 1043
    invoke-virtual/range {p3 .. p3}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v15

    .line 1044
    .local v15, "register":I
    if-ltz v15, :cond_6

    .line 1045
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    move-object/from16 v19, v0

    aget-object v10, v19, v15

    .line 1056
    .end local v10    # "functor":Ljava/lang/Object;
    .end local v15    # "register":I
    :cond_5
    :goto_2
    instance-of v0, v10, Lorg/apache/commons/jexl2/Script;

    move/from16 v19, v0

    if-eqz v19, :cond_9

    .line 1057
    check-cast v10, Lorg/apache/commons/jexl2/Script;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    move-object/from16 v19, v0

    array-length v0, v5

    move/from16 v20, v0

    if-lez v20, :cond_8

    .end local v5    # "argv":[Ljava/lang/Object;
    :goto_3
    move-object/from16 v0, v19

    invoke-interface {v10, v0, v5}, Lorg/apache/commons/jexl2/Script;->execute(Lorg/apache/commons/jexl2/JexlContext;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    goto :goto_1

    .line 1047
    .restart local v5    # "argv":[Ljava/lang/Object;
    .restart local v10    # "functor":Ljava/lang/Object;
    .restart local v15    # "register":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v14}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    goto :goto_2

    .line 1050
    .end local v15    # "register":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-interface {v0, v1, v14, v2}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    move-result-object v11

    .line 1051
    .local v11, "gfunctor":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    if-eqz v11, :cond_5

    .line 1052
    move-object/from16 v0, p2

    invoke-interface {v11, v0, v14}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->tryInvoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    goto :goto_2

    .line 1057
    .end local v10    # "functor":Ljava/lang/Object;
    .end local v11    # "gfunctor":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    :cond_8
    const/4 v5, 0x0

    goto :goto_3

    .line 1058
    :cond_9
    instance-of v0, v10, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move/from16 v19, v0

    if-eqz v19, :cond_c

    .line 1059
    move-object v0, v10

    check-cast v0, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-object/from16 v16, v0

    .line 1060
    const/4 v6, 0x0

    .line 1066
    :cond_a
    :goto_4
    if-nez v17, :cond_b

    .line 1068
    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-interface {v0, v1, v5}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 1070
    .restart local v9    # "eval":Ljava/lang/Object;
    if-eqz v6, :cond_2

    invoke-interface/range {v16 .. v16}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->isCacheable()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1071
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtSetValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 1075
    .end local v5    # "argv":[Ljava/lang/Object;
    .end local v6    # "cacheable":Z
    .end local v9    # "eval":Ljava/lang/Object;
    .end local v16    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :catch_0
    move-exception v8

    .line 1076
    .local v8, "e":Ljava/lang/reflect/InvocationTargetException;
    new-instance v17, Lorg/apache/commons/jexl2/JexlException;

    .end local v17    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v19, "method invocation error"

    invoke-virtual {v8}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1080
    .end local v8    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v17    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_b
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->invocationFailed(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v9

    goto/16 :goto_1

    .line 1062
    .restart local v5    # "argv":[Ljava/lang/Object;
    .restart local v6    # "cacheable":Z
    .restart local v16    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :cond_c
    :try_start_1
    new-instance v18, Lorg/apache/commons/jexl2/JexlException$Method;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14}, Lorg/apache/commons/jexl2/JexlException$Method;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .end local v17    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .local v18, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object/from16 v17, v18

    .end local v18    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v17    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_4

    .line 1077
    .end local v5    # "argv":[Ljava/lang/Object;
    .end local v6    # "cacheable":Z
    .end local v16    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :catch_1
    move-exception v8

    .line 1078
    .local v8, "e":Ljava/lang/Exception;
    new-instance v17, Lorg/apache/commons/jexl2/JexlException;

    .end local v17    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v19, "method error"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v8}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .restart local v17    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_5
.end method

.method private isTernaryProtected(Lorg/apache/commons/jexl2/parser/JexlNode;)Z
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    .line 1377
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .local v0, "walk":Lorg/apache/commons/jexl2/parser/JexlNode;
    :goto_0
    if-eqz v0, :cond_1

    .line 1378
    instance-of v1, v0, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;

    if-eqz v1, :cond_0

    .line 1379
    const/4 v1, 0x1

    .line 1384
    :goto_1
    return v1

    .line 1380
    :cond_0
    instance-of v1, v0, Lorg/apache/commons/jexl2/parser/ASTReference;

    if-nez v1, :cond_2

    instance-of v1, v0, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;

    if-nez v1, :cond_2

    .line 1384
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1377
    :cond_2
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    goto :goto_0
.end method

.method private sizeOf(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)I
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "val"    # Ljava/lang/Object;

    .prologue
    .line 1472
    instance-of v4, p2, Ljava/util/Collection;

    if-eqz v4, :cond_0

    .line 1473
    check-cast p2, Ljava/util/Collection;

    .end local p2    # "val":Ljava/lang/Object;
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v4

    .line 1492
    :goto_0
    return v4

    .line 1474
    .restart local p2    # "val":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1475
    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v4

    goto :goto_0

    .line 1476
    :cond_1
    instance-of v4, p2, Ljava/util/Map;

    if-eqz v4, :cond_2

    .line 1477
    check-cast p2, Ljava/util/Map;

    .end local p2    # "val":Ljava/lang/Object;
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v4

    goto :goto_0

    .line 1478
    .restart local p2    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v4, p2, Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 1479
    check-cast p2, Ljava/lang/String;

    .end local p2    # "val":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_0

    .line 1483
    .restart local p2    # "val":Ljava/lang/Object;
    :cond_3
    const/4 v4, 0x0

    new-array v1, v4, [Ljava/lang/Object;

    .line 1484
    .local v1, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    const-string/jumbo v5, "size"

    sget-object v6, Lorg/apache/commons/jexl2/Interpreter;->EMPTY_PARAMS:[Ljava/lang/Object;

    invoke-interface {v4, p2, v5, v6, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v3

    .line 1485
    .local v3, "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-eqz v3, :cond_4

    invoke-interface {v3}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->getReturnType()Ljava/lang/Class;

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v4, v5, :cond_4

    .line 1488
    :try_start_0
    invoke-interface {v3, p2, v1}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1492
    .local v2, "result":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_0

    .line 1489
    .end local v2    # "result":Ljava/lang/Integer;
    :catch_0
    move-exception v0

    .line 1490
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v5, "size() : error executing"

    invoke-direct {v4, p1, v5, v0}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 1494
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "size() : unsupported type : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, p1, v5, v6}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method


# virtual methods
.method protected findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;
    .locals 2
    .param p1, "xrt"    # Ljava/lang/RuntimeException;
    .param p2, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p3, "left"    # Ljava/lang/Object;
    .param p4, "right"    # Ljava/lang/Object;

    .prologue
    .line 307
    instance-of v0, p1, Ljava/lang/ArithmeticException;

    if-eqz v0, :cond_0

    const-string v0, "jexl.null"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 309
    if-nez p3, :cond_1

    .line 310
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object p2

    .line 316
    .end local p2    # "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_0
    :goto_0
    return-object p2

    .line 312
    .restart local p2    # "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_1
    if-nez p4, :cond_0

    .line 313
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object p2

    goto :goto_0
.end method

.method public getAttribute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "attribute"    # Ljava/lang/Object;

    .prologue
    .line 1507
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;
    .locals 7
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "attribute"    # Ljava/lang/Object;
    .param p3, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    .line 1520
    if-nez p1, :cond_0

    .line 1521
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v6, "object is null"

    invoke-direct {v5, p3, v6}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v5

    .line 1523
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/Interpreter;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1524
    new-instance v5, Lorg/apache/commons/jexl2/JexlException$Cancel;

    invoke-direct {v5, p3}, Lorg/apache/commons/jexl2/JexlException$Cancel;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V

    throw v5

    .line 1527
    :cond_1
    if-eqz p3, :cond_3

    iget-boolean v5, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v5, :cond_3

    .line 1528
    invoke-virtual {p3}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetValue()Ljava/lang/Object;

    move-result-object v0

    .line 1529
    .local v0, "cached":Ljava/lang/Object;
    instance-of v5, v0, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    if-eqz v5, :cond_3

    move-object v2, v0

    .line 1530
    check-cast v2, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    .line 1531
    .local v2, "vg":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    invoke-interface {v2, p1, p2}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->tryInvoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1532
    .local v1, "value":Ljava/lang/Object;
    invoke-interface {v2, v1}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->tryFailed(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1560
    .end local v0    # "cached":Ljava/lang/Object;
    .end local v1    # "value":Ljava/lang/Object;
    :cond_2
    :goto_0
    return-object v1

    .line 1537
    .end local v2    # "vg":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    :cond_3
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v5, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    move-result-object v2

    .line 1538
    .restart local v2    # "vg":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    if-eqz v2, :cond_6

    .line 1540
    :try_start_0
    invoke-interface {v2, p1}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1542
    .restart local v1    # "value":Ljava/lang/Object;
    if-eqz p3, :cond_2

    iget-boolean v5, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v5, :cond_2

    invoke-interface {v2}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->isCacheable()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1543
    invoke-virtual {p3, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtSetValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1546
    .end local v1    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v3

    .line 1547
    .local v3, "xany":Ljava/lang/Exception;
    if-nez p3, :cond_4

    .line 1548
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 1550
    :cond_4
    new-instance v4, Lorg/apache/commons/jexl2/JexlException$Property;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p3, v5}, Lorg/apache/commons/jexl2/JexlException$Property;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    .line 1551
    .local v4, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    iget-boolean v5, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-eqz v5, :cond_5

    .line 1552
    throw v4

    .line 1554
    :cond_5
    iget-boolean v5, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-nez v5, :cond_6

    .line 1555
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    .line 1560
    .end local v3    # "xany":Ljava/lang/Exception;
    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getContext()Lorg/apache/commons/jexl2/JexlContext;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    return-object v0
.end method

.method protected getUberspect()Lorg/apache/commons/jexl2/introspection/Uberspect;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    return-object v0
.end method

.method public interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    const/4 v3, 0x0

    .line 232
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p1, p0, v4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException$Return; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 243
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 244
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 245
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    :goto_0
    return-object v0

    .line 233
    :catch_0
    move-exception v2

    .line 234
    .local v2, "xreturn":Lorg/apache/commons/jexl2/JexlException$Return;
    :try_start_1
    invoke-virtual {v2}, Lorg/apache/commons/jexl2/JexlException$Return;->getValue()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 243
    .local v0, "value":Ljava/lang/Object;
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 244
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 245
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    goto :goto_0

    .line 236
    .end local v0    # "value":Ljava/lang/Object;
    .end local v2    # "xreturn":Lorg/apache/commons/jexl2/JexlException$Return;
    :catch_1
    move-exception v1

    .line 237
    .local v1, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_2
    iget-boolean v4, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-eqz v4, :cond_0

    .line 238
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 243
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 244
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 245
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    move-object v0, v3

    goto :goto_0

    .line 241
    :cond_0
    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 243
    .end local v1    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :catchall_0
    move-exception v4

    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 244
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 245
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    throw v4
.end method

.method protected invocationFailed(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;
    .locals 3
    .param p1, "xjexl"    # Lorg/apache/commons/jexl2/JexlException;

    .prologue
    .line 340
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-nez v0, :cond_0

    instance-of v0, p1, Lorg/apache/commons/jexl2/JexlException$Return;

    if-eqz v0, :cond_1

    .line 341
    :cond_0
    throw p1

    .line 343
    :cond_1
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-nez v0, :cond_2

    .line 344
    iget-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 346
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isCancelled()Z
    .locals 2

    .prologue
    .line 355
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->cancelled:Z

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->cancelled:Z

    .line 358
    :cond_0
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->cancelled:Z

    return v0
.end method

.method public isSilent()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    return v0
.end method

.method public isStrict()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    return v0
.end method

.method protected resolveNamespace(Ljava/lang/String;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;
    .locals 7
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    .line 370
    const/4 v2, 0x0

    .line 372
    .local v2, "namespace":Ljava/lang/Object;
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 373
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 374
    if-eqz v2, :cond_1

    .line 404
    .end local v2    # "namespace":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v2

    .line 379
    :cond_1
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    instance-of v4, v4, Lorg/apache/commons/jexl2/NamespaceResolver;

    if-eqz v4, :cond_2

    .line 380
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    check-cast v4, Lorg/apache/commons/jexl2/NamespaceResolver;

    invoke-interface {v4, p1}, Lorg/apache/commons/jexl2/NamespaceResolver;->resolveNamespace(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 382
    :cond_2
    if-nez v2, :cond_3

    .line 383
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->functions:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 384
    .restart local v2    # "namespace":Ljava/lang/Object;
    if-eqz p1, :cond_3

    if-nez v2, :cond_3

    .line 385
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "no such function namespace "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v4

    .line 389
    .end local v2    # "namespace":Ljava/lang/Object;
    :cond_3
    instance-of v4, v2, Ljava/lang/Class;

    if-eqz v4, :cond_0

    .line 390
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    aput-object v5, v0, v4

    .line 391
    .local v0, "args":[Ljava/lang/Object;
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v4, v2, v0, p2}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v1

    .line 392
    .local v1, "ctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-eqz v1, :cond_0

    .line 394
    :try_start_0
    invoke-interface {v1, v2, v0}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 395
    .restart local v2    # "namespace":Ljava/lang/Object;
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    if-nez v4, :cond_4

    .line 396
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 398
    :cond_4
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    invoke-interface {v4, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 399
    .end local v2    # "namespace":Ljava/lang/Object;
    :catch_0
    move-exception v3

    .line 400
    .local v3, "xinst":Ljava/lang/Exception;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "unable to instantiate namespace "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public setAttribute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "attribute"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 1572
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/commons/jexl2/Interpreter;->setAttribute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)V

    .line 1573
    return-void
.end method

.method protected setAttribute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)V
    .locals 11
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "attribute"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;
    .param p4, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    const/4 v10, 0x0

    .line 1585
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/Interpreter;->isCancelled()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1586
    new-instance v9, Lorg/apache/commons/jexl2/JexlException$Cancel;

    invoke-direct {v9, p4}, Lorg/apache/commons/jexl2/JexlException$Cancel;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V

    throw v9

    .line 1589
    :cond_0
    if-eqz p4, :cond_2

    iget-boolean v9, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v9, :cond_2

    .line 1590
    invoke-virtual {p4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetValue()Ljava/lang/Object;

    move-result-object v0

    .line 1591
    .local v0, "cached":Ljava/lang/Object;
    instance-of v9, v0, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;

    if-eqz v9, :cond_2

    move-object v4, v0

    .line 1592
    check-cast v4, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;

    .line 1593
    .local v4, "setter":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    invoke-interface {v4, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;->tryInvoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1594
    .local v2, "eval":Ljava/lang/Object;
    invoke-interface {v4, v2}, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;->tryFailed(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 1645
    .end local v0    # "cached":Ljava/lang/Object;
    .end local v2    # "eval":Ljava/lang/Object;
    .end local v4    # "setter":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    :cond_1
    :goto_0
    return-void

    .line 1599
    :cond_2
    const/4 v7, 0x0

    .line 1600
    .local v7, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    iget-object v9, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v9, p1, p2, p3, p4}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getPropertySet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertySet;

    move-result-object v5

    .line 1602
    .local v5, "vs":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    if-nez v5, :cond_3

    .line 1604
    const/4 v9, 0x1

    new-array v3, v9, [Ljava/lang/Object;

    aput-object p3, v3, v10

    .line 1605
    .local v3, "narrow":[Ljava/lang/Object;
    iget-object v9, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v9, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1606
    iget-object v9, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    aget-object v10, v3, v10

    invoke-interface {v9, p1, p2, v10, p4}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getPropertySet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertySet;

    move-result-object v5

    .line 1609
    .end local v3    # "narrow":[Ljava/lang/Object;
    :cond_3
    if-eqz v5, :cond_5

    .line 1612
    :try_start_0
    invoke-interface {v5, p1, p3}, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1613
    if-eqz p4, :cond_1

    iget-boolean v9, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v9, :cond_1

    invoke-interface {v5}, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;->isCacheable()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1614
    invoke-virtual {p4, v5}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtSetValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1617
    :catch_0
    move-exception v8

    .line 1618
    .local v8, "xrt":Ljava/lang/RuntimeException;
    if-nez p4, :cond_4

    .line 1619
    throw v8

    .line 1621
    :cond_4
    new-instance v7, Lorg/apache/commons/jexl2/JexlException;

    .end local v7    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v9, "set object property error"

    invoke-direct {v7, p4, v9, v8}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1629
    .end local v8    # "xrt":Ljava/lang/RuntimeException;
    .restart local v7    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_5
    :goto_1
    if-nez v7, :cond_8

    .line 1630
    if-nez p4, :cond_7

    .line 1631
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "unable to set object property, class: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", property: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", argument: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1635
    .local v1, "error":Ljava/lang/String;
    new-instance v9, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v9, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 1622
    .end local v1    # "error":Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 1623
    .local v6, "xany":Ljava/lang/Exception;
    if-nez p4, :cond_6

    .line 1624
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 1626
    :cond_6
    new-instance v7, Lorg/apache/commons/jexl2/JexlException;

    .end local v7    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v9, "set object property error"

    invoke-direct {v7, p4, v9, v6}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .restart local v7    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_1

    .line 1637
    .end local v6    # "xany":Ljava/lang/Exception;
    :cond_7
    new-instance v7, Lorg/apache/commons/jexl2/JexlException$Property;

    .end local v7    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, p4, v9}, Lorg/apache/commons/jexl2/JexlException$Property;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    .line 1639
    .restart local v7    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_8
    iget-boolean v9, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-eqz v9, :cond_9

    .line 1640
    throw v7

    .line 1642
    :cond_9
    iget-boolean v9, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-nez v9, :cond_1

    .line 1643
    iget-object v9, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method protected setFrame(Lorg/apache/commons/jexl2/JexlEngine$Frame;)V
    .locals 1
    .param p1, "frame"    # Lorg/apache/commons/jexl2/JexlEngine$Frame;

    .prologue
    const/4 v0, 0x0

    .line 289
    if-eqz p1, :cond_0

    .line 290
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlEngine$Frame;->getParameters()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 291
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlEngine$Frame;->getRegisters()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    .line 296
    :goto_0
    return-void

    .line 293
    :cond_0
    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 294
    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    goto :goto_0
.end method

.method protected varargs setRegisters([Ljava/lang/Object;)V
    .locals 4
    .param p1, "theRegisters"    # [Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 273
    if-eqz p1, :cond_1

    .line 274
    array-length v2, p1

    new-array v1, v2, [Ljava/lang/String;

    .line 275
    .local v1, "regStrs":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "r":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 276
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278
    :cond_0
    iput-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->parameters:[Ljava/lang/String;

    .line 280
    .end local v0    # "r":I
    .end local v1    # "regStrs":[Ljava/lang/String;
    :cond_1
    iput-object p1, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    .line 281
    return-void
.end method

.method public setSilent(Z)V
    .locals 0
    .param p1, "flag"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 201
    iput-boolean p1, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    .line 202
    return-void
.end method

.method public setStrict(Z)V
    .locals 0
    .param p1, "flag"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 191
    iput-boolean p1, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    .line 192
    return-void
.end method

.method protected unknownVariable(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;
    .locals 2
    .param p1, "xjexl"    # Lorg/apache/commons/jexl2/JexlException;

    .prologue
    .line 325
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-eqz v0, :cond_0

    .line 326
    throw p1

    .line 328
    :cond_0
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-nez v0, :cond_1

    .line 329
    iget-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    .line 331
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 416
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 417
    .local v1, "left":Ljava/lang/Object;
    const/4 v0, 0x2

    .local v0, "c":I
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetNumChildren()I

    move-result v4

    .local v4, "size":I
    :goto_0
    if-ge v0, v4, :cond_3

    .line 418
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 420
    .local v3, "right":Ljava/lang/Object;
    add-int/lit8 v8, v0, -0x1

    :try_start_0
    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 421
    .local v2, "op":Lorg/apache/commons/jexl2/parser/JexlNode;
    instance-of v8, v2, Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;

    if-eqz v8, :cond_2

    .line 422
    iget-object v5, v2, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    .line 423
    .local v5, "which":Ljava/lang/String;
    const-string v8, "+"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 424
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v1, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 417
    :goto_1
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 427
    :cond_0
    const-string v8, "-"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 428
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v1, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;->subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_1

    .line 431
    :cond_1
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "unknown operator "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    .end local v2    # "op":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v5    # "which":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 435
    .local v7, "xrt":Ljava/lang/ArithmeticException;
    invoke-virtual {p0, v7, p1, v1, v3}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    .line 436
    .local v6, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    const-string v9, "+/- error"

    invoke-direct {v8, v6, v9, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 433
    .end local v6    # "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v7    # "xrt":Ljava/lang/ArithmeticException;
    .restart local v2    # "op":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_2
    :try_start_1
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "unknown operator "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_0

    .line 439
    .end local v2    # "op":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v3    # "right":Ljava/lang/Object;
    :cond_3
    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 444
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Shoud not be called."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAmbiguous;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAmbiguous;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1664
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "unexpected type of node"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAndNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAndNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 449
    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 451
    .local v0, "left":Ljava/lang/Object;
    :try_start_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    .line 452
    .local v1, "leftValue":Z
    if-nez v1, :cond_0

    .line 453
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 467
    :goto_0
    return-object v5

    .line 455
    .end local v1    # "leftValue":Z
    :catch_0
    move-exception v4

    .line 456
    .local v4, "xrt":Ljava/lang/RuntimeException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string v7, "boolean coercion error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 458
    .end local v4    # "xrt":Ljava/lang/RuntimeException;
    .restart local v1    # "leftValue":Z
    :cond_0
    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 460
    .local v2, "right":Ljava/lang/Object;
    :try_start_1
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v3

    .line 461
    .local v3, "rightValue":Z
    if-nez v3, :cond_1

    .line 462
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 464
    .end local v3    # "rightValue":Z
    :catch_1
    move-exception v4

    .line 465
    .local v4, "xrt":Ljava/lang/ArithmeticException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string v7, "boolean coercion error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 467
    .end local v4    # "xrt":Ljava/lang/ArithmeticException;
    .restart local v3    # "rightValue":Z
    :cond_1
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTArrayAccess;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 473
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 475
    .local v4, "object":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetNumChildren()I

    move-result v3

    .line 476
    .local v3, "numChildren":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 477
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 478
    .local v2, "nindex":Lorg/apache/commons/jexl2/parser/JexlNode;
    instance-of v5, v2, Lorg/apache/commons/jexl2/parser/JexlNode$Literal;

    if-eqz v5, :cond_0

    .line 479
    invoke-virtual {v2, p0, v4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 476
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 481
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v2, p0, v5}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 482
    .local v1, "index":Ljava/lang/Object;
    invoke-virtual {p0, v4, v1, v2}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_1

    .line 486
    .end local v1    # "index":Ljava/lang/Object;
    .end local v2    # "nindex":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_1
    return-object v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 491
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->getLiteral()Ljava/lang/Object;

    move-result-object v4

    .line 492
    .local v4, "literal":Ljava/lang/Object;
    if-nez v4, :cond_1

    .line 493
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetNumChildren()I

    move-result v1

    .line 494
    .local v1, "childCount":I
    new-array v0, v1, [Ljava/lang/Object;

    .line 495
    .local v0, "array":[Ljava/lang/Object;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 496
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 497
    .local v2, "entry":Ljava/lang/Object;
    aput-object v2, v0, v3

    .line 495
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 499
    .end local v2    # "entry":Ljava/lang/Object;
    :cond_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArrayType([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 500
    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->setLiteral(Ljava/lang/Object;)V

    .line 502
    .end local v0    # "array":[Ljava/lang/Object;
    .end local v1    # "childCount":I
    .end local v3    # "i":I
    :cond_1
    return-object v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAssignment;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 28
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAssignment;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 508
    const/16 v19, -0x1

    .line 509
    .local v19, "register":I
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/parser/ASTAssignment;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v12

    .line 510
    .local v12, "left":Lorg/apache/commons/jexl2/parser/JexlNode;
    instance-of v0, v12, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    move/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v22, v12

    .line 511
    check-cast v22, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    .line 512
    .local v22, "var":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    invoke-virtual/range {v22 .. v22}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v19

    .line 513
    if-gez v19, :cond_1

    .line 514
    new-instance v25, Lorg/apache/commons/jexl2/JexlException;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "unknown variable "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    iget-object v0, v12, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v12, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v25

    .line 516
    .end local v22    # "var":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    :cond_0
    instance-of v0, v12, Lorg/apache/commons/jexl2/parser/ASTReference;

    move/from16 v25, v0

    if-nez v25, :cond_1

    .line 517
    new-instance v25, Lorg/apache/commons/jexl2/JexlException;

    const-string v26, "illegal assignment form 0"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v12, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v25

    .line 520
    :cond_1
    const/16 v25, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/parser/ASTAssignment;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    .line 523
    .local v20, "right":Ljava/lang/Object;
    const/16 v16, 0x0

    .line 524
    .local v16, "objectNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    if-ltz v19, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    move-object/from16 v25, v0

    aget-object v15, v25, v19

    .line 525
    .local v15, "object":Ljava/lang/Object;
    :goto_0
    const/16 v18, 0x0

    .line 526
    .local v18, "propertyNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    const/16 v17, 0x0

    .line 527
    .local v17, "property":Ljava/lang/Object;
    const/4 v10, 0x1

    .line 528
    .local v10, "isVariable":Z
    const/16 v21, 0x0

    .line 529
    .local v21, "v":I
    const/16 v23, 0x0

    .line 531
    .local v23, "variableName":Ljava/lang/StringBuilder;
    invoke-virtual {v12}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v25

    add-int/lit8 v11, v25, -0x1

    .line 533
    .local v11, "last":I
    if-gez v11, :cond_4

    if-ltz v19, :cond_4

    const/4 v9, 0x1

    .line 535
    .local v9, "isRegister":Z
    :goto_1
    if-ltz v19, :cond_5

    const/4 v5, 0x1

    .local v5, "c":I
    :goto_2
    if-ge v5, v11, :cond_c

    .line 536
    invoke-virtual {v12, v5}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v16

    .line 538
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v15}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .line 539
    if-eqz v15, :cond_6

    .line 535
    :cond_2
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 524
    .end local v5    # "c":I
    .end local v9    # "isRegister":Z
    .end local v10    # "isVariable":Z
    .end local v11    # "last":I
    .end local v15    # "object":Ljava/lang/Object;
    .end local v17    # "property":Ljava/lang/Object;
    .end local v18    # "propertyNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v21    # "v":I
    .end local v23    # "variableName":Ljava/lang/StringBuilder;
    :cond_3
    const/4 v15, 0x0

    goto :goto_0

    .line 533
    .restart local v10    # "isVariable":Z
    .restart local v11    # "last":I
    .restart local v15    # "object":Ljava/lang/Object;
    .restart local v17    # "property":Ljava/lang/Object;
    .restart local v18    # "propertyNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    .restart local v21    # "v":I
    .restart local v23    # "variableName":Ljava/lang/StringBuilder;
    :cond_4
    const/4 v9, 0x0

    goto :goto_1

    .line 535
    .restart local v9    # "isRegister":Z
    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    .line 542
    .restart local v5    # "c":I
    :cond_6
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    move/from16 v25, v0

    if-nez v25, :cond_7

    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    move/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v25, v16

    check-cast v25, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    invoke-virtual/range {v25 .. v25}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->isInteger()Z

    move-result v25

    if-eqz v25, :cond_9

    :cond_7
    const/16 v25, 0x1

    :goto_4
    and-int v10, v10, v25

    .line 545
    if-eqz v10, :cond_b

    .line 546
    if-nez v21, :cond_8

    .line 547
    new-instance v23, Ljava/lang/StringBuilder;

    .end local v23    # "variableName":Ljava/lang/StringBuilder;
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 548
    .restart local v23    # "variableName":Ljava/lang/StringBuilder;
    const/16 v21, 0x1

    .line 550
    :cond_8
    :goto_5
    move/from16 v0, v21

    if-gt v0, v5, :cond_a

    .line 551
    const/16 v25, 0x2e

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552
    move/from16 v0, v21

    invoke-virtual {v12, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    add-int/lit8 v21, v21, 0x1

    goto :goto_5

    .line 542
    :cond_9
    const/16 v25, 0x0

    goto :goto_4

    .line 554
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    move-object/from16 v25, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-interface/range {v25 .. v26}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    .line 556
    if-eqz v15, :cond_2

    .line 557
    const/4 v10, 0x0

    goto :goto_3

    .line 560
    :cond_b
    new-instance v25, Lorg/apache/commons/jexl2/JexlException;

    const-string v26, "illegal assignment form"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v25

    .line 564
    :cond_c
    if-eqz v9, :cond_e

    const/16 v18, 0x0

    .line 565
    :goto_6
    const/4 v4, 0x0

    .line 566
    .local v4, "antVar":Z
    move-object/from16 v0, v18

    instance-of v0, v0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    move/from16 v25, v0

    if-eqz v25, :cond_10

    move-object/from16 v7, v18

    .line 567
    check-cast v7, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    .line 568
    .local v7, "identifier":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v19

    .line 569
    if-ltz v19, :cond_f

    .line 570
    const/4 v9, 0x1

    .line 605
    .end local v7    # "identifier":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .end local v17    # "property":Ljava/lang/Object;
    :cond_d
    :goto_7
    if-eqz v9, :cond_16

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    move-object/from16 v25, v0

    aput-object v20, v25, v19

    .line 635
    :goto_8
    return-object v20

    .line 564
    .end local v4    # "antVar":Z
    .restart local v17    # "property":Ljava/lang/Object;
    :cond_e
    invoke-virtual {v12, v11}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v18

    goto :goto_6

    .line 572
    .restart local v4    # "antVar":Z
    .restart local v7    # "identifier":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    :cond_f
    iget-object v0, v7, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 573
    .local v17, "property":Ljava/lang/String;
    const/4 v4, 0x1

    goto :goto_7

    .line 575
    .end local v7    # "identifier":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .local v17, "property":Ljava/lang/Object;
    :cond_10
    move-object/from16 v0, v18

    instance-of v0, v0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    move/from16 v25, v0

    if-eqz v25, :cond_11

    move-object/from16 v25, v18

    check-cast v25, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    invoke-virtual/range {v25 .. v25}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->isInteger()Z

    move-result v25

    if-eqz v25, :cond_11

    move-object/from16 v25, v18

    .line 576
    check-cast v25, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    invoke-virtual/range {v25 .. v25}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->getLiteral()Ljava/lang/Number;

    move-result-object v17

    .line 577
    .local v17, "property":Ljava/lang/Number;
    const/4 v4, 0x1

    goto :goto_7

    .line 578
    .local v17, "property":Ljava/lang/Object;
    :cond_11
    move-object/from16 v0, v18

    instance-of v0, v0, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;

    move/from16 v25, v0

    if-eqz v25, :cond_15

    .line 580
    move-object/from16 v16, v18

    move-object/from16 v13, v16

    .line 581
    check-cast v13, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;

    .line 582
    .local v13, "narray":Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v13, v0}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v15}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .line 583
    .local v14, "nobject":Ljava/lang/Object;
    if-nez v14, :cond_12

    .line 584
    new-instance v25, Lorg/apache/commons/jexl2/JexlException;

    const-string v26, "array element is null"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v25

    .line 586
    :cond_12
    move-object v15, v14

    .line 590
    invoke-virtual {v13}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetNumChildren()I

    move-result v25

    add-int/lit8 v11, v25, -0x1

    .line 591
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_9
    if-ge v6, v11, :cond_14

    .line 592
    invoke-virtual {v13, v6}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v16

    .line 593
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/apache/commons/jexl2/parser/JexlNode$Literal;

    move/from16 v25, v0

    if-eqz v25, :cond_13

    .line 594
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v15}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .line 591
    :goto_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 596
    :cond_13
    const/16 v25, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 597
    .local v8, "index":Ljava/lang/Object;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v8, v1}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v15

    goto :goto_a

    .line 600
    .end local v8    # "index":Ljava/lang/Object;
    :cond_14
    invoke-virtual {v13, v11}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v25

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    .line 601
    goto/16 :goto_7

    .end local v6    # "i":I
    .end local v13    # "narray":Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    .end local v14    # "nobject":Ljava/lang/Object;
    :cond_15
    if-nez v9, :cond_d

    .line 602
    new-instance v25, Lorg/apache/commons/jexl2/JexlException;

    const-string v26, "illegal assignment form"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v25

    .line 608
    .end local v17    # "property":Ljava/lang/Object;
    :cond_16
    if-eqz v4, :cond_19

    .line 609
    if-eqz v10, :cond_19

    if-nez v15, :cond_19

    .line 610
    if-eqz v23, :cond_18

    .line 611
    if-lez v11, :cond_17

    .line 612
    const/16 v25, 0x2e

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 614
    :cond_17
    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 615
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 618
    :cond_18
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    move-object/from16 v25, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/jexl2/JexlContext;->set(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_8

    .line 619
    :catch_0
    move-exception v24

    .line 620
    .local v24, "xsupport":Ljava/lang/UnsupportedOperationException;
    new-instance v25, Lorg/apache/commons/jexl2/JexlException;

    const-string v26, "context is readonly"

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v25

    .line 625
    .end local v24    # "xsupport":Ljava/lang/UnsupportedOperationException;
    :cond_19
    if-nez v17, :cond_1a

    .line 627
    new-instance v25, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v26, "property is null"

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v25

    .line 629
    :cond_1a
    if-nez v15, :cond_1b

    .line 631
    new-instance v25, Lorg/apache/commons/jexl2/JexlException;

    const-string v26, "bean is null"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v25

    .line 634
    :cond_1b
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    move-object/from16 v3, v18

    invoke-virtual {v0, v15, v1, v2, v3}, Lorg/apache/commons/jexl2/Interpreter;->setAttribute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)V

    goto/16 :goto_8
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 640
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 641
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 643
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->bitwiseAnd(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 644
    :catch_0
    move-exception v2

    .line 645
    .local v2, "xrt":Ljava/lang/ArithmeticException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string v4, "& error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 651
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 653
    .local v0, "left":Ljava/lang/Object;
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v2, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->bitwiseComplement(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 654
    :catch_0
    move-exception v1

    .line 655
    .local v1, "xrt":Ljava/lang/ArithmeticException;
    new-instance v2, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v3, "~ error"

    invoke-direct {v2, p1, v3, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 661
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 662
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 664
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->bitwiseOr(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 665
    :catch_0
    move-exception v2

    .line 666
    .local v2, "xrt":Ljava/lang/ArithmeticException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v4, "| error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 672
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 673
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 675
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->bitwiseXor(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 676
    :catch_0
    move-exception v2

    .line 677
    .local v2, "xrt":Ljava/lang/ArithmeticException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string v4, "^ error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBlock;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBlock;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 683
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTBlock;->jjtGetNumChildren()I

    move-result v1

    .line 684
    .local v1, "numChildren":I
    const/4 v2, 0x0

    .line 685
    .local v2, "result":Ljava/lang/Object;
    const/4 v0, 0x0

    .end local v2    # "result":Ljava/lang/Object;
    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 686
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTBlock;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 685
    .restart local v2    # "result":Ljava/lang/Object;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 688
    .end local v2    # "result":Ljava/lang/Object;
    :cond_0
    return-object v2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTConstructorNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 15
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTConstructorNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1115
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/Interpreter;->isCancelled()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1116
    new-instance v13, Lorg/apache/commons/jexl2/JexlException$Cancel;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lorg/apache/commons/jexl2/JexlException$Cancel;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V

    throw v13

    .line 1119
    :cond_0
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, p0, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 1121
    .local v4, "cobject":Ljava/lang/Object;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetNumChildren()I

    move-result v13

    add-int/lit8 v1, v13, -0x1

    .line 1122
    .local v1, "argc":I
    new-array v2, v1, [Ljava/lang/Object;

    .line 1123
    .local v2, "argv":[Ljava/lang/Object;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v1, :cond_1

    .line 1124
    add-int/lit8 v13, v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, p0, v14}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v2, v8

    .line 1123
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1127
    :cond_1
    const/4 v11, 0x0

    .line 1130
    .local v11, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_0
    iget-boolean v13, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v13, :cond_2

    .line 1131
    invoke-virtual/range {p1 .. p1}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetValue()Ljava/lang/Object;

    move-result-object v3

    .line 1132
    .local v3, "cached":Ljava/lang/Object;
    instance-of v13, v3, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    if-eqz v13, :cond_2

    .line 1133
    move-object v0, v3

    check-cast v0, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-object v10, v0

    .line 1134
    .local v10, "mctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    const/4 v13, 0x0

    invoke-interface {v10, v13, v4, v2}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->tryInvoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 1135
    .local v7, "eval":Ljava/lang/Object;
    invoke-interface {v10, v7}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->tryFailed(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    .line 1163
    .end local v3    # "cached":Ljava/lang/Object;
    .end local v7    # "eval":Ljava/lang/Object;
    .end local v10    # "mctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :goto_1
    return-object v7

    .line 1140
    :cond_2
    iget-object v13, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v0, p1

    invoke-interface {v13, v4, v2, v0}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v5

    .line 1142
    .local v5, "ctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-nez v5, :cond_4

    .line 1143
    iget-object v13, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v13, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1144
    iget-object v13, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v0, p1

    invoke-interface {v13, v4, v2, v0}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getConstructorMethod(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v5

    .line 1146
    :cond_3
    if-nez v5, :cond_4

    .line 1147
    new-instance v12, Lorg/apache/commons/jexl2/JexlException$Method;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v13}, Lorg/apache/commons/jexl2/JexlException$Method;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    .end local v11    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .local v12, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object v11, v12

    .line 1150
    .end local v12    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v11    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_4
    if-nez v11, :cond_6

    .line 1151
    invoke-interface {v5, v4, v2}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 1153
    .local v9, "instance":Ljava/lang/Object;
    iget-boolean v13, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v13, :cond_5

    invoke-interface {v5}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->isCacheable()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1154
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtSetValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_5
    move-object v7, v9

    .line 1156
    goto :goto_1

    .line 1158
    .end local v5    # "ctor":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .end local v9    # "instance":Ljava/lang/Object;
    :catch_0
    move-exception v6

    .line 1159
    .local v6, "e":Ljava/lang/reflect/InvocationTargetException;
    new-instance v11, Lorg/apache/commons/jexl2/JexlException;

    .end local v11    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string v13, "constructor invocation error"

    invoke-virtual {v6}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v13, v14}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1163
    .end local v6    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v11    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_6
    :goto_2
    invoke-virtual {p0, v11}, Lorg/apache/commons/jexl2/Interpreter;->invocationFailed(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v7

    goto :goto_1

    .line 1160
    :catch_1
    move-exception v6

    .line 1161
    .local v6, "e":Ljava/lang/Exception;
    new-instance v11, Lorg/apache/commons/jexl2/JexlException;

    .end local v11    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string v13, "constructor error"

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v13, v6}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .restart local v11    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTDivNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTDivNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 693
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTDivNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 694
    .local v0, "left":Ljava/lang/Object;
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTDivNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 696
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->divide(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 699
    :goto_0
    return-object v4

    .line 697
    :catch_0
    move-exception v3

    .line 698
    .local v3, "xrt":Ljava/lang/ArithmeticException;
    iget-boolean v4, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-nez v4, :cond_0

    .line 699
    new-instance v4, Ljava/lang/Double;

    const-wide/16 v5, 0x0

    invoke-direct {v4, v5, v6}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 701
    :cond_0
    invoke-virtual {p0, v3, p1, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 702
    .local v2, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string v5, "divide error"

    invoke-direct {v4, v2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTEQNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTEQNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 730
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTEQNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 731
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTEQNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 733
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 734
    :catch_0
    move-exception v2

    .line 735
    .local v2, "xrt":Ljava/lang/ArithmeticException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string v4, "== error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTERNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTERNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 804
    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTERNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 805
    .local v3, "left":Ljava/lang/Object;
    invoke-virtual {p1, v9}, Lorg/apache/commons/jexl2/parser/ASTERNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 808
    .local v5, "right":Ljava/lang/Object;
    :try_start_0
    instance-of v8, v5, Ljava/util/regex/Pattern;

    if-nez v8, :cond_0

    instance-of v8, v5, Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 809
    :cond_0
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v3, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->matches(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 853
    .end local v5    # "right":Ljava/lang/Object;
    :goto_0
    return-object v8

    .line 809
    .restart local v5    # "right":Ljava/lang/Object;
    :cond_1
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 813
    :cond_2
    instance-of v8, v5, Ljava/util/Set;

    if-eqz v8, :cond_4

    .line 814
    check-cast v5, Ljava/util/Set;

    .end local v5    # "right":Ljava/lang/Object;
    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_3
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 817
    .restart local v5    # "right":Ljava/lang/Object;
    :cond_4
    instance-of v8, v5, Ljava/util/Map;

    if-eqz v8, :cond_6

    .line 818
    check-cast v5, Ljava/util/Map;

    .end local v5    # "right":Ljava/lang/Object;
    invoke-interface {v5, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_5
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 821
    .restart local v5    # "right":Ljava/lang/Object;
    :cond_6
    instance-of v8, v5, Ljava/util/Collection;

    if-eqz v8, :cond_8

    .line 822
    check-cast v5, Ljava/util/Collection;

    .end local v5    # "right":Ljava/lang/Object;
    invoke-interface {v5, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_7
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 826
    .restart local v5    # "right":Ljava/lang/Object;
    :cond_8
    const/4 v8, 0x1

    :try_start_1
    new-array v0, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v0, v8

    .line 827
    .local v0, "argv":[Ljava/lang/Object;
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    const-string v9, "contains"

    invoke-interface {v8, v5, v9, v0, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v6

    .line 828
    .local v6, "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-eqz v6, :cond_a

    .line 829
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-interface {v6, v5, v0}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_9
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 830
    :cond_a
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 831
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    const-string v9, "contains"

    invoke-interface {v8, v5, v9, v0, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v6

    .line 832
    if-eqz v6, :cond_c

    .line 833
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-interface {v6, v5, v0}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_b
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 836
    .end local v0    # "argv":[Ljava/lang/Object;
    .end local v6    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :catch_0
    move-exception v1

    .line 837
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_2
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    const-string v9, "=~ invocation error"

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v10

    invoke-direct {v8, p1, v9, v10}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
    :try_end_2
    .catch Ljava/lang/ArithmeticException; {:try_start_2 .. :try_end_2} :catch_1

    .line 854
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v5    # "right":Ljava/lang/Object;
    :catch_1
    move-exception v7

    .line 855
    .local v7, "xrt":Ljava/lang/ArithmeticException;
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    const-string v9, "=~ error"

    invoke-direct {v8, p1, v9, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 838
    .end local v7    # "xrt":Ljava/lang/ArithmeticException;
    .restart local v5    # "right":Ljava/lang/Object;
    :catch_2
    move-exception v1

    .line 839
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    const-string v9, "=~ error"

    invoke-direct {v8, p1, v9, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 842
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "argv":[Ljava/lang/Object;
    .restart local v6    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :cond_c
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v8, v5, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getIterator(Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/util/Iterator;

    move-result-object v2

    .line 843
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    if-eqz v2, :cond_10

    .line 844
    :cond_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 845
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 846
    .local v4, "next":Ljava/lang/Object;
    if-eq v4, v3, :cond_e

    if-eqz v4, :cond_d

    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 847
    :cond_e
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 850
    .end local v4    # "next":Ljava/lang/Object;
    :cond_f
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 853
    :cond_10
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v3, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto/16 :goto_0

    :cond_11
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_3
    .catch Ljava/lang/ArithmeticException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 708
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 709
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 710
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 725
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 712
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 713
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 715
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    array-length v1, v1

    if-nez v1, :cond_2

    .line 716
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 718
    :cond_2
    instance-of v1, v0, Ljava/util/Collection;

    if-eqz v1, :cond_4

    .line 719
    check-cast v0, Ljava/util/Collection;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_3
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 722
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_4
    instance-of v1, v0, Ljava/util/Map;

    if-eqz v1, :cond_6

    .line 723
    check-cast v0, Ljava/util/Map;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_5
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 725
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_6
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFalseNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFalseNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 741
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;
    .param p2, "data"    # Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 889
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Method should not be called; only present for API compatibiltiy"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTForeachStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTForeachStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    .line 746
    const/4 v5, 0x0

    .line 748
    .local v5, "result":Ljava/lang/Object;
    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    check-cast v2, Lorg/apache/commons/jexl2/parser/ASTReference;

    .line 749
    .local v2, "loopReference":Lorg/apache/commons/jexl2/parser/ASTReference;
    invoke-virtual {v2, v8}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    check-cast v3, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    .line 750
    .local v3, "loopVariable":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    invoke-virtual {v3}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v4

    .line 752
    .local v4, "register":I
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 754
    .local v1, "iterableValue":Ljava/lang/Object;
    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetNumChildren()I

    move-result v8

    const/4 v9, 0x3

    if-lt v8, v9, :cond_2

    .line 756
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    .line 759
    .local v6, "statement":Lorg/apache/commons/jexl2/parser/JexlNode;
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v8, v1, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getIterator(Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/util/Iterator;

    move-result-object v0

    .line 760
    .local v0, "itemsIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    if-eqz v0, :cond_2

    .line 761
    .end local v5    # "result":Ljava/lang/Object;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 762
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/Interpreter;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 763
    new-instance v8, Lorg/apache/commons/jexl2/JexlException$Cancel;

    invoke-direct {v8, p1}, Lorg/apache/commons/jexl2/JexlException$Cancel;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V

    throw v8

    .line 766
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 767
    .local v7, "value":Ljava/lang/Object;
    if-gez v4, :cond_1

    .line 768
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    iget-object v9, v3, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    invoke-interface {v8, v9, v7}, Lorg/apache/commons/jexl2/JexlContext;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 773
    :goto_1
    invoke-virtual {v6, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 774
    .restart local v5    # "result":Ljava/lang/Object;
    goto :goto_0

    .line 770
    .end local v5    # "result":Ljava/lang/Object;
    :cond_1
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    aput-object v7, v8, v4

    goto :goto_1

    .line 777
    .end local v0    # "itemsIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    .end local v6    # "statement":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v7    # "value":Ljava/lang/Object;
    :cond_2
    return-object v5
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFunctionNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFunctionNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1106
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    iget-object v2, v3, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    .line 1107
    .local v2, "prefix":Ljava/lang/String;
    invoke-virtual {p0, v2, p1}, Lorg/apache/commons/jexl2/Interpreter;->resolveNamespace(Ljava/lang/String;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v1

    .line 1109
    .local v1, "namespace":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    .line 1110
    .local v0, "functionNode":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    const/4 v3, 0x2

    invoke-direct {p0, p1, v1, v0, v3}, Lorg/apache/commons/jexl2/Interpreter;->call(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/ASTIdentifier;I)Ljava/lang/Object;

    move-result-object v3

    return-object v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTGENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTGENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 782
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTGENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 783
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTGENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 785
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->greaterThanOrEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 786
    :catch_0
    move-exception v2

    .line 787
    .local v2, "xrt":Ljava/lang/ArithmeticException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string v4, ">= error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTGTNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTGTNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 793
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTGTNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 794
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTGTNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 796
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->greaterThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 797
    :catch_0
    move-exception v2

    .line 798
    .local v2, "xrt":Ljava/lang/ArithmeticException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string v4, "> error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIdentifier;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 861
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/Interpreter;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 862
    new-instance v4, Lorg/apache/commons/jexl2/JexlException$Cancel;

    invoke-direct {v4, p1}, Lorg/apache/commons/jexl2/JexlException$Cancel;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V

    throw v4

    .line 864
    :cond_0
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    .line 865
    .local v0, "name":Ljava/lang/String;
    if-nez p2, :cond_3

    .line 866
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v1

    .line 867
    .local v1, "register":I
    if-ltz v1, :cond_2

    .line 868
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    aget-object v2, v4, v1

    .line 880
    .end local v1    # "register":I
    :cond_1
    :goto_0
    return-object v2

    .line 870
    .restart local v1    # "register":I
    :cond_2
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v4, v0}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 871
    .local v2, "value":Ljava/lang/Object;
    if-nez v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/commons/jexl2/parser/ASTReference;

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v4, v0}, Lorg/apache/commons/jexl2/JexlContext;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/Interpreter;->isTernaryProtected(Lorg/apache/commons/jexl2/parser/JexlNode;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 875
    new-instance v3, Lorg/apache/commons/jexl2/JexlException$Variable;

    invoke-direct {v3, p1, v0}, Lorg/apache/commons/jexl2/JexlException$Variable;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    .line 876
    .local v3, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    invoke-virtual {p0, v3}, Lorg/apache/commons/jexl2/Interpreter;->unknownVariable(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 880
    .end local v1    # "register":I
    .end local v2    # "value":Ljava/lang/Object;
    .end local v3    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_3
    invoke-virtual {p0, p2, v0, p1}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIfStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIfStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 907
    const/4 v2, 0x0

    .line 909
    .local v2, "n":I
    const/4 v3, 0x0

    .line 911
    .local v3, "result":Ljava/lang/Object;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 912
    .local v1, "expression":Ljava/lang/Object;
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 914
    const/4 v2, 0x1

    .line 915
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 924
    .end local v3    # "result":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v3

    .line 919
    .restart local v3    # "result":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetNumChildren()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 920
    const/4 v2, 0x2

    .line 921
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    goto :goto_0

    .line 925
    .end local v1    # "expression":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 926
    .local v0, "error":Lorg/apache/commons/jexl2/JexlException;
    throw v0

    .line 927
    .end local v0    # "error":Lorg/apache/commons/jexl2/JexlException;
    :catch_1
    move-exception v4

    .line 928
    .local v4, "xrt":Ljava/lang/ArithmeticException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string v7, "if error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;
    .param p2, "data"    # Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 897
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Method should not be called; only present for API compatibiltiy"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 942
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetNumChildren()I

    move-result v2

    .line 943
    .local v2, "numChildren":I
    const/4 v3, 0x0

    .line 944
    .local v3, "result":Ljava/lang/Object;
    const/4 v1, 0x0

    .end local v3    # "result":Ljava/lang/Object;
    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 945
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 946
    .local v0, "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-virtual {v0, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 944
    .restart local v3    # "result":Ljava/lang/Object;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 948
    .end local v0    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v3    # "result":Ljava/lang/Object;
    :cond_0
    return-object v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTLENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTLENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 953
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTLENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 954
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTLENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 956
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->lessThanOrEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 957
    :catch_0
    move-exception v2

    .line 958
    .local v2, "xrt":Ljava/lang/ArithmeticException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string v4, "<= error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTLTNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTLTNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 964
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTLTNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 965
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTLTNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 967
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 968
    :catch_0
    move-exception v2

    .line 969
    .local v2, "xrt":Ljava/lang/ArithmeticException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string v4, "< error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMapEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMapEntry;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 975
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTMapEntry;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 976
    .local v0, "key":Ljava/lang/Object;
    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTMapEntry;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 977
    .local v1, "value":Ljava/lang/Object;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    aput-object v1, v2, v4

    return-object v2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMapLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMapLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 982
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetNumChildren()I

    move-result v0

    .line 983
    .local v0, "childCount":I
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 985
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 986
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v1, v4

    check-cast v1, [Ljava/lang/Object;

    .line 987
    .local v1, "entry":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v4, v1, v4

    const/4 v5, 0x1

    aget-object v5, v1, v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 985
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 990
    .end local v1    # "entry":[Ljava/lang/Object;
    :cond_0
    return-object v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMethodNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMethodNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1086
    if-nez p2, :cond_0

    .line 1089
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    if-ne v1, p1, :cond_1

    .line 1090
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1}, Lorg/apache/commons/jexl2/Interpreter;->resolveNamespace(Ljava/lang/String;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object p2

    .line 1091
    if-nez p2, :cond_0

    .line 1092
    iget-object p2, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    .line 1099
    .end local p2    # "data":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    .line 1100
    .local v0, "methodNode":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->call(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/ASTIdentifier;I)Ljava/lang/Object;

    move-result-object v1

    return-object v1

    .line 1095
    .end local v0    # "methodNode":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .restart local p2    # "data":Ljava/lang/Object;
    :cond_1
    new-instance v1, Lorg/apache/commons/jexl2/JexlException;

    const-string v2, "attempting to call method on null"

    invoke-direct {v1, p1, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTModNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTModNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1168
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTModNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1169
    .local v0, "left":Ljava/lang/Object;
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTModNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1171
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->mod(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1174
    :goto_0
    return-object v4

    .line 1172
    :catch_0
    move-exception v3

    .line 1173
    .local v3, "xrt":Ljava/lang/ArithmeticException;
    iget-boolean v4, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-nez v4, :cond_0

    .line 1174
    new-instance v4, Ljava/lang/Double;

    const-wide/16 v5, 0x0

    invoke-direct {v4, v5, v6}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 1176
    :cond_0
    invoke-virtual {p0, v3, p1, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 1177
    .local v2, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string v5, "% error"

    invoke-direct {v4, v2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMulNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMulNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1183
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTMulNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1184
    .local v0, "left":Ljava/lang/Object;
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTMulNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1186
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->multiply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 1187
    :catch_0
    move-exception v3

    .line 1188
    .local v3, "xrt":Ljava/lang/ArithmeticException;
    invoke-virtual {p0, v3, p1, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 1189
    .local v2, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string v5, "* error"

    invoke-direct {v4, v2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1195
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTNENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1196
    .local v0, "left":Ljava/lang/Object;
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTNENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1198
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_0
    return-object v4

    :cond_0
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1199
    :catch_0
    move-exception v3

    .line 1200
    .local v3, "xrt":Ljava/lang/ArithmeticException;
    invoke-virtual {p0, v3, p1, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 1201
    .local v2, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string v5, "!= error"

    invoke-direct {v4, v2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNRNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNRNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 1207
    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTNRNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 1208
    .local v3, "left":Ljava/lang/Object;
    invoke-virtual {p1, v9}, Lorg/apache/commons/jexl2/parser/ASTNRNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 1210
    .local v5, "right":Ljava/lang/Object;
    :try_start_0
    instance-of v8, v5, Ljava/util/regex/Pattern;

    if-nez v8, :cond_0

    instance-of v8, v5, Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 1212
    :cond_0
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v3, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->matches(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 1255
    .end local v5    # "right":Ljava/lang/Object;
    :goto_0
    return-object v8

    .line 1212
    .restart local v5    # "right":Ljava/lang/Object;
    :cond_1
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1215
    :cond_2
    instance-of v8, v5, Ljava/util/Set;

    if-eqz v8, :cond_4

    .line 1216
    check-cast v5, Ljava/util/Set;

    .end local v5    # "right":Ljava/lang/Object;
    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_3
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1219
    .restart local v5    # "right":Ljava/lang/Object;
    :cond_4
    instance-of v8, v5, Ljava/util/Map;

    if-eqz v8, :cond_6

    .line 1220
    check-cast v5, Ljava/util/Map;

    .end local v5    # "right":Ljava/lang/Object;
    invoke-interface {v5, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_5
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1223
    .restart local v5    # "right":Ljava/lang/Object;
    :cond_6
    instance-of v8, v5, Ljava/util/Collection;

    if-eqz v8, :cond_8

    .line 1224
    check-cast v5, Ljava/util/Collection;

    .end local v5    # "right":Ljava/lang/Object;
    invoke-interface {v5, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_7
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1228
    .restart local v5    # "right":Ljava/lang/Object;
    :cond_8
    const/4 v8, 0x1

    :try_start_1
    new-array v0, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v0, v8

    .line 1229
    .local v0, "argv":[Ljava/lang/Object;
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    const-string v9, "contains"

    invoke-interface {v8, v5, v9, v0, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v6

    .line 1230
    .local v6, "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-eqz v6, :cond_a

    .line 1231
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-interface {v6, v5, v0}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_9
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1232
    :cond_a
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1233
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    const-string v9, "contains"

    invoke-interface {v8, v5, v9, v0, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v6

    .line 1234
    if-eqz v6, :cond_c

    .line 1235
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-interface {v6, v5, v0}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_b
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1238
    .end local v0    # "argv":[Ljava/lang/Object;
    .end local v6    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :catch_0
    move-exception v1

    .line 1239
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_2
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    const-string v9, "!~ invocation error"

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v10

    invoke-direct {v8, p1, v9, v10}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
    :try_end_2
    .catch Ljava/lang/ArithmeticException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1256
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v5    # "right":Ljava/lang/Object;
    :catch_1
    move-exception v7

    .line 1257
    .local v7, "xrt":Ljava/lang/ArithmeticException;
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    const-string v9, "!~ error"

    invoke-direct {v8, p1, v9, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 1240
    .end local v7    # "xrt":Ljava/lang/ArithmeticException;
    .restart local v5    # "right":Ljava/lang/Object;
    :catch_2
    move-exception v1

    .line 1241
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    const-string v9, "!~ error"

    invoke-direct {v8, p1, v9, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 1244
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "argv":[Ljava/lang/Object;
    .restart local v6    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :cond_c
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    const/4 v9, 0x1

    invoke-virtual {p1, v9}, Lorg/apache/commons/jexl2/parser/ASTNRNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    invoke-interface {v8, v5, v9}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getIterator(Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/util/Iterator;

    move-result-object v2

    .line 1245
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    if-eqz v2, :cond_10

    .line 1246
    :cond_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 1247
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 1248
    .local v4, "next":Ljava/lang/Object;
    if-eq v4, v3, :cond_e

    if-eqz v4, :cond_d

    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 1249
    :cond_e
    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1252
    .end local v4    # "next":Ljava/lang/Object;
    :cond_f
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1255
    :cond_10
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v3, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto/16 :goto_0

    :cond_11
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_3
    .catch Ljava/lang/ArithmeticException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNotNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNotNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1263
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTNotNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1264
    .local v0, "val":Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v1, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNullLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNullLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1269
    const/4 v0, 0x0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 934
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->isInteger()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 935
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->getLiteral()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {p0, p2, v0, p1}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v0

    .line 937
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->getLiteral()Ljava/lang/Number;

    move-result-object v0

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTOrNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTOrNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1274
    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1276
    .local v0, "left":Ljava/lang/Object;
    :try_start_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    .line 1277
    .local v1, "leftValue":Z
    if-eqz v1, :cond_0

    .line 1278
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1292
    :goto_0
    return-object v5

    .line 1280
    .end local v1    # "leftValue":Z
    :catch_0
    move-exception v4

    .line 1281
    .local v4, "xrt":Ljava/lang/ArithmeticException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string v7, "boolean coercion error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1283
    .end local v4    # "xrt":Ljava/lang/ArithmeticException;
    .restart local v1    # "leftValue":Z
    :cond_0
    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1285
    .local v2, "right":Ljava/lang/Object;
    :try_start_1
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v3

    .line 1286
    .local v3, "rightValue":Z
    if-eqz v3, :cond_1

    .line 1287
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1289
    .end local v3    # "rightValue":Z
    :catch_1
    move-exception v4

    .line 1290
    .restart local v4    # "xrt":Ljava/lang/ArithmeticException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string v7, "boolean coercion error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1292
    .end local v4    # "xrt":Ljava/lang/ArithmeticException;
    .restart local v3    # "rightValue":Z
    :cond_1
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTReference;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTReference;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1300
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetNumChildren()I

    move-result v2

    .line 1302
    .local v2, "numChildren":I
    const/4 v4, 0x0

    .line 1303
    .local v4, "result":Ljava/lang/Object;
    const/4 v7, 0x0

    .line 1304
    .local v7, "variableName":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 1305
    .local v3, "propertyName":Ljava/lang/String;
    const/4 v1, 0x1

    .line 1306
    .local v1, "isVariable":Z
    const/4 v6, 0x0

    .line 1307
    .local v6, "v":I
    const/4 v0, 0x0

    .local v0, "c":I
    move-object v10, v4

    .end local v4    # "result":Ljava/lang/Object;
    :goto_0
    if-ge v0, v2, :cond_6

    .line 1308
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/Interpreter;->isCancelled()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1309
    new-instance v9, Lorg/apache/commons/jexl2/JexlException$Cancel;

    invoke-direct {v9, p1}, Lorg/apache/commons/jexl2/JexlException$Cancel;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V

    throw v9

    .line 1311
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    .line 1313
    .local v5, "theNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    if-nez v10, :cond_3

    instance-of v9, v5, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    if-eqz v9, :cond_3

    move-object v9, v5

    check-cast v9, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    invoke-virtual {v9}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->isInteger()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1314
    if-lez v6, :cond_2

    const/4 v9, 0x1

    :goto_1
    and-int/2addr v1, v9

    move-object v4, v10

    .line 1320
    :goto_2
    if-nez v4, :cond_5

    if-eqz v1, :cond_5

    .line 1321
    if-nez v6, :cond_1

    .line 1322
    new-instance v7, Ljava/lang/StringBuilder;

    .end local v7    # "variableName":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    iget-object v9, v9, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1323
    .restart local v7    # "variableName":Ljava/lang/StringBuilder;
    const/4 v6, 0x1

    .line 1325
    :cond_1
    :goto_3
    if-gt v6, v0, :cond_4

    .line 1326
    const/16 v9, 0x2e

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1327
    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    iget-object v9, v9, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1325
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1314
    :cond_2
    const/4 v9, 0x0

    goto :goto_1

    .line 1316
    :cond_3
    instance-of v9, v5, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    and-int/2addr v1, v9

    .line 1317
    invoke-virtual {v5, p0, v10}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .restart local v4    # "result":Ljava/lang/Object;
    goto :goto_2

    .line 1329
    .end local v4    # "result":Ljava/lang/Object;
    :cond_4
    iget-object v9, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 1307
    :goto_4
    add-int/lit8 v0, v0, 0x1

    move-object v10, v4

    goto :goto_0

    .line 1331
    :cond_5
    iget-object v3, v5, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    goto :goto_4

    .line 1334
    .end local v5    # "theNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_6
    if-nez v10, :cond_9

    .line 1335
    if-eqz v1, :cond_9

    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/Interpreter;->isTernaryProtected(Lorg/apache/commons/jexl2/parser/JexlNode;)Z

    move-result v9

    if-nez v9, :cond_9

    iget-object v9, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v11}, Lorg/apache/commons/jexl2/JexlContext;->has(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_9

    const/4 v9, 0x1

    if-ne v2, v9, :cond_7

    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    instance-of v9, v9, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    if-eqz v9, :cond_7

    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    check-cast v9, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    invoke-virtual {v9}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->getRegister()I

    move-result v9

    if-gez v9, :cond_9

    .line 1341
    :cond_7
    if-eqz v3, :cond_8

    new-instance v8, Lorg/apache/commons/jexl2/JexlException$Property;

    invoke-direct {v8, p1, v3}, Lorg/apache/commons/jexl2/JexlException$Property;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    .line 1344
    .local v8, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :goto_5
    invoke-virtual {p0, v8}, Lorg/apache/commons/jexl2/Interpreter;->unknownVariable(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v9

    .line 1347
    .end local v8    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :goto_6
    return-object v9

    .line 1341
    :cond_8
    new-instance v8, Lorg/apache/commons/jexl2/JexlException$Variable;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p1, v9}, Lorg/apache/commons/jexl2/JexlException$Variable;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    goto :goto_5

    :cond_9
    move-object v9, v10

    .line 1347
    goto :goto_6
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1355
    move-object v0, p1

    .line 1356
    .local v0, "upper":Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/jexl2/Interpreter;->visit(Lorg/apache/commons/jexl2/parser/ASTArrayAccess;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTReturnStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTReturnStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1364
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTReturnStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1365
    .local v0, "val":Ljava/lang/Object;
    new-instance v1, Lorg/apache/commons/jexl2/JexlException$Return;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v0}, Lorg/apache/commons/jexl2/JexlException$Return;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)V

    throw v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTSizeFunction;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTSizeFunction;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1389
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTSizeFunction;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1390
    .local v0, "val":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 1391
    new-instance v1, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v2, "size() : argument is null"

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1393
    :cond_0
    invoke-direct {p0, p1, v0}, Lorg/apache/commons/jexl2/Interpreter;->sizeOf(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTSizeMethod;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTSizeMethod;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1398
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/Interpreter;->sizeOf(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTStringLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTStringLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1403
    if-eqz p2, :cond_0

    .line 1404
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTStringLiteral;->getLiteral()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0, p1}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v0

    .line 1406
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTStringLiteral;->image:Ljava/lang/String;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTTernaryNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTTernaryNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    .line 1411
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1412
    .local v0, "condition":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetNumChildren()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 1413
    if-eqz v0, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v1, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1414
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1422
    .end local v0    # "condition":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v0

    .line 1416
    .restart local v0    # "condition":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1419
    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v1, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1422
    :cond_3
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTTrueNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTTrueNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1428
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1433
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    .line 1434
    .local v3, "valNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1436
    .local v2, "val":Ljava/lang/Object;
    :try_start_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->negate(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1438
    .local v1, "number":Ljava/lang/Object;
    instance-of v5, v3, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    if-eqz v5, :cond_0

    instance-of v5, v1, Ljava/lang/Number;

    if-eqz v5, :cond_0

    .line 1439
    iget-object v6, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    check-cast v1, Ljava/lang/Number;

    .end local v1    # "number":Ljava/lang/Object;
    move-object v0, v3

    check-cast v0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;

    move-object v5, v0

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->getLiteralClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v6, v1, v5}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowNumber(Ljava/lang/Number;Ljava/lang/Class;)Ljava/lang/Number;
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1441
    :cond_0
    return-object v1

    .line 1442
    :catch_0
    move-exception v4

    .line 1443
    .local v4, "xrt":Ljava/lang/ArithmeticException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    const-string v6, "arithmetic error"

    invoke-direct {v5, v3, v6, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTVar;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTVar;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 902
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/Interpreter;->visit(Lorg/apache/commons/jexl2/parser/ASTIdentifier;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTWhileStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTWhileStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 1449
    const/4 v1, 0x0

    .line 1451
    .local v1, "result":Ljava/lang/Object;
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 1452
    .end local v1    # "result":Ljava/lang/Object;
    .local v0, "expressionNode":Lorg/apache/commons/jexl2/parser/Node;
    :cond_0
    :goto_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-interface {v0, p0, p2}, Lorg/apache/commons/jexl2/parser/Node;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1453
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/Interpreter;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1454
    new-instance v2, Lorg/apache/commons/jexl2/JexlException$Cancel;

    invoke-direct {v2, p1}, Lorg/apache/commons/jexl2/JexlException$Cancel;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V

    throw v2

    .line 1457
    :cond_1
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetNumChildren()I

    move-result v2

    if-le v2, v4, :cond_0

    .line 1458
    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .restart local v1    # "result":Ljava/lang/Object;
    goto :goto_0

    .line 1461
    .end local v1    # "result":Ljava/lang/Object;
    :cond_2
    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/SimpleNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/SimpleNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1654
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
