.class public interface abstract Lorg/apache/commons/jexl2/Script;
.super Ljava/lang/Object;
.source "Script.java"


# virtual methods
.method public abstract callable(Lorg/apache/commons/jexl2/JexlContext;)Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/JexlContext;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public varargs abstract callable(Lorg/apache/commons/jexl2/JexlContext;[Ljava/lang/Object;)Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/JexlContext;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract execute(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;
.end method

.method public varargs abstract execute(Lorg/apache/commons/jexl2/JexlContext;[Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract getLocalVariables()[Ljava/lang/String;
.end method

.method public abstract getParameters()[Ljava/lang/String;
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract getVariables()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method
