.class final enum Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;
.super Ljava/lang/Enum;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/UnifiedJEXL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ParseState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

.field public static final enum CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

.field public static final enum DEFERRED0:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

.field public static final enum DEFERRED1:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

.field public static final enum ESCAPE:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

.field public static final enum IMMEDIATE0:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

.field public static final enum IMMEDIATE1:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 805
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    const-string v1, "CONST"

    invoke-direct {v0, v1, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 807
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    const-string v1, "IMMEDIATE0"

    invoke-direct {v0, v1, v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->IMMEDIATE0:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 809
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    const-string v1, "DEFERRED0"

    invoke-direct {v0, v1, v5}, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->DEFERRED0:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 811
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    const-string v1, "IMMEDIATE1"

    invoke-direct {v0, v1, v6}, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->IMMEDIATE1:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 813
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    const-string v1, "DEFERRED1"

    invoke-direct {v0, v1, v7}, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->DEFERRED1:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 815
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    const-string v1, "ESCAPE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->ESCAPE:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 803
    const/4 v0, 0x6

    new-array v0, v0, [Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->IMMEDIATE0:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->DEFERRED0:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->IMMEDIATE1:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->DEFERRED1:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->ESCAPE:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->$VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 803
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 803
    const-class v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    return-object v0
.end method

.method public static final values()[Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;
    .locals 1

    .prologue
    .line 803
    sget-object v0, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->$VALUES:[Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    invoke-virtual {v0}, [Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    return-object v0
.end method
