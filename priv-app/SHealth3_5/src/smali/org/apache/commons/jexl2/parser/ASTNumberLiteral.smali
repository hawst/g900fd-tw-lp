.class public Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;
.super Lorg/apache/commons/jexl2/parser/JexlNode;
.source "ASTNumberLiteral.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/JexlNode$Literal;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/commons/jexl2/parser/JexlNode;",
        "Lorg/apache/commons/jexl2/parser/JexlNode$Literal",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field literal:Ljava/lang/Number;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/JexlNode;-><init>(I)V

    .line 24
    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->literal:Ljava/lang/Number;

    .line 26
    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->clazz:Ljava/lang/Class;

    .line 30
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/Parser;I)V
    .locals 1
    .param p1, "p"    # Lorg/apache/commons/jexl2/parser/Parser;
    .param p2, "id"    # I

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;-><init>(Lorg/apache/commons/jexl2/parser/Parser;I)V

    .line 24
    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->literal:Ljava/lang/Number;

    .line 26
    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->clazz:Ljava/lang/Class;

    .line 34
    return-void
.end method


# virtual methods
.method public getLiteral()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->literal:Ljava/lang/Number;

    return-object v0
.end method

.method public bridge synthetic getLiteral()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->getLiteral()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public getLiteralClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->clazz:Ljava/lang/Class;

    return-object v0
.end method

.method protected isConstant(Z)Z
    .locals 1
    .param p1, "literal"    # Z

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method public isInteger()Z
    .locals 2

    .prologue
    .line 61
    const-class v0, Ljava/lang/Integer;

    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->clazz:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "visitor"    # Lorg/apache/commons/jexl2/parser/ParserVisitor;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 53
    invoke-interface {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/ParserVisitor;->visit(Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setNatural(Ljava/lang/String;)V
    .locals 10
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 74
    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x30

    if-ne v6, v7, :cond_2

    .line 75
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v9, :cond_1

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x78

    if-eq v6, v7, :cond_0

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x58

    if-ne v6, v7, :cond_1

    .line 76
    :cond_0
    const/16 v0, 0x10

    .line 77
    .local v0, "base":I
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 84
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .line 85
    .local v1, "last":I
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 99
    const-class v2, Ljava/lang/Integer;

    .line 101
    .local v2, "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-static {p1, v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 111
    .local v3, "result":Ljava/lang/Number;
    :goto_1
    iput-object v3, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->literal:Ljava/lang/Number;

    .line 112
    iput-object v2, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->clazz:Ljava/lang/Class;

    .line 113
    return-void

    .line 79
    .end local v0    # "base":I
    .end local v1    # "last":I
    .end local v2    # "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "result":Ljava/lang/Number;
    :cond_1
    const/16 v0, 0x8

    .restart local v0    # "base":I
    goto :goto_0

    .line 82
    .end local v0    # "base":I
    :cond_2
    const/16 v0, 0xa

    .restart local v0    # "base":I
    goto :goto_0

    .line 88
    .restart local v1    # "last":I
    :sswitch_0
    const-class v2, Ljava/lang/Long;

    .line 89
    .restart local v2    # "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p1, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v3

    .line 90
    .restart local v3    # "result":Ljava/lang/Number;
    goto :goto_1

    .line 94
    .end local v2    # "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "result":Ljava/lang/Number;
    :sswitch_1
    const-class v2, Ljava/math/BigInteger;

    .line 95
    .restart local v2    # "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v3, Ljava/math/BigInteger;

    invoke-virtual {p1, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6, v0}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 96
    .restart local v3    # "result":Ljava/lang/Number;
    goto :goto_1

    .line 102
    .end local v3    # "result":Ljava/lang/Number;
    :catch_0
    move-exception v4

    .line 104
    .local v4, "take2":Ljava/lang/NumberFormatException;
    :try_start_1
    invoke-static {p1, v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .restart local v3    # "result":Ljava/lang/Number;
    goto :goto_1

    .line 105
    .end local v3    # "result":Ljava/lang/Number;
    :catch_1
    move-exception v5

    .line 106
    .local v5, "take3":Ljava/lang/NumberFormatException;
    new-instance v3, Ljava/math/BigInteger;

    invoke-direct {v3, p1, v0}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .restart local v3    # "result":Ljava/lang/Number;
    goto :goto_1

    .line 85
    nop

    :sswitch_data_0
    .sparse-switch
        0x48 -> :sswitch_1
        0x4c -> :sswitch_0
        0x68 -> :sswitch_1
        0x6c -> :sswitch_0
    .end sparse-switch
.end method

.method public setReal(Ljava/lang/String;)V
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .line 124
    .local v0, "last":I
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 140
    const-class v1, Ljava/lang/Float;

    .line 142
    .local v1, "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 153
    .local v2, "result":Ljava/lang/Number;
    :goto_0
    iput-object v2, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->literal:Ljava/lang/Number;

    .line 154
    iput-object v1, p0, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->clazz:Ljava/lang/Class;

    .line 155
    return-void

    .line 127
    .end local v1    # "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "result":Ljava/lang/Number;
    :sswitch_0
    new-instance v2, Ljava/math/BigDecimal;

    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 128
    .restart local v2    # "result":Ljava/lang/Number;
    const-class v1, Ljava/math/BigDecimal;

    .line 129
    .restart local v1    # "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_0

    .line 133
    .end local v1    # "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "result":Ljava/lang/Number;
    :sswitch_1
    const-class v1, Ljava/lang/Double;

    .line 134
    .restart local v1    # "rclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    .line 135
    .restart local v2    # "result":Ljava/lang/Number;
    goto :goto_0

    .line 143
    .end local v2    # "result":Ljava/lang/Number;
    :catch_0
    move-exception v3

    .line 145
    .local v3, "take2":Ljava/lang/NumberFormatException;
    :try_start_1
    invoke-static {p1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .restart local v2    # "result":Ljava/lang/Number;
    goto :goto_0

    .line 146
    .end local v2    # "result":Ljava/lang/Number;
    :catch_1
    move-exception v4

    .line 147
    .local v4, "take3":Ljava/lang/NumberFormatException;
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .restart local v2    # "result":Ljava/lang/Number;
    goto :goto_0

    .line 124
    nop

    :sswitch_data_0
    .sparse-switch
        0x42 -> :sswitch_0
        0x44 -> :sswitch_1
        0x62 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method
