.class public Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;
.super Ljava/lang/Object;
.source "IntrospectorBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase$CacheMiss;
    }
.end annotation


# static fields
.field private static final CTOR_MISS:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final classMethodMaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/jexl2/internal/introspection/ClassMap;",
            ">;"
        }
    .end annotation
.end field

.field private final constructibleClasses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final constructorsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            "Ljava/lang/reflect/Constructor",
            "<*>;>;"
        }
    .end annotation
.end field

.field private loader:Ljava/lang/ClassLoader;

.field protected final rlog:Lorg/apache/commons/logging/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 178
    const-class v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase$CacheMiss;

    invoke-virtual {v0}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->CTOR_MISS:Ljava/lang/reflect/Constructor;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/logging/Log;)V
    .locals 1
    .param p1, "log"    # Lorg/apache/commons/logging/Log;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructibleClasses:Ljava/util/Map;

    .line 76
    iput-object p1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    .line 77
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->loader:Ljava/lang/ClassLoader;

    .line 78
    return-void
.end method

.method private getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/apache/commons/jexl2/internal/introspection/ClassMap;"
        }
    .end annotation

    .prologue
    .line 318
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    monitor-enter v2

    .line 319
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    .line 320
    .local v0, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    if-nez v0, :cond_0

    .line 321
    new-instance v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    .end local v0    # "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    invoke-direct {v0, p1, v1}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;-><init>(Ljava/lang/Class;Lorg/apache/commons/logging/Log;)V

    .line 322
    .restart local v0    # "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    :cond_0
    monitor-exit v2

    return-object v0

    .line 325
    .end local v0    # "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static isLoadedBy(Ljava/lang/ClassLoader;Ljava/lang/Class;)Z
    .locals 2
    .param p0, "loader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ClassLoader;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 226
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz p0, :cond_1

    .line 227
    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 228
    .local v0, "cloader":Ljava/lang/ClassLoader;
    :goto_0
    if-eqz v0, :cond_1

    .line 229
    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    const/4 v1, 0x1

    .line 236
    .end local v0    # "cloader":Ljava/lang/ClassLoader;
    :goto_1
    return v1

    .line 232
    .restart local v0    # "cloader":Ljava/lang/ClassLoader;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    move-result-object v0

    goto :goto_0

    .line 236
    .end local v0    # "cloader":Ljava/lang/ClassLoader;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getClassByName(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p1, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 87
    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->loader:Ljava/lang/ClassLoader;

    invoke-static {p1, v1, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 89
    :goto_0
    return-object v1

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "xignore":Ljava/lang/ClassNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getConstructor(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Constructor;
    .locals 15
    .param p2, "key"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            ")",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 258
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v4, 0x0

    .line 259
    .local v4, "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    iget-object v12, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    monitor-enter v12

    .line 260
    :try_start_0
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    move-object/from16 v0, p2

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Ljava/lang/reflect/Constructor;

    move-object v4, v0

    .line 262
    sget-object v11, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->CTOR_MISS:Ljava/lang/reflect/Constructor;

    invoke-virtual {v11, v4}, Ljava/lang/reflect/Constructor;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 263
    const/4 v11, 0x0

    monitor-exit v12

    .line 308
    :goto_0
    return-object v11

    .line 266
    :cond_0
    if-nez v4, :cond_4

    .line 267
    invoke-virtual/range {p2 .. p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMethod()Ljava/lang/String;

    move-result-object v3

    .line 269
    .local v3, "cname":Ljava/lang/String;
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructibleClasses:Ljava/util/Map;

    invoke-interface {v11, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez v2, :cond_1

    .line 273
    if-eqz p1, :cond_2

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMethod()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 274
    move-object/from16 v2, p1

    .line 279
    :goto_1
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructibleClasses:Ljava/util/Map;

    invoke-interface {v11, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    :cond_1
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 282
    .local v7, "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    invoke-virtual {v2}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/reflect/Constructor;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_2
    if-ge v5, v8, :cond_3

    aget-object v6, v1, v5

    .line 283
    .local v6, "ictor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 276
    .end local v1    # "arr$":[Ljava/lang/reflect/Constructor;
    .end local v5    # "i$":I
    .end local v6    # "ictor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    .end local v7    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .end local v8    # "len$":I
    :cond_2
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->loader:Ljava/lang/ClassLoader;

    invoke-virtual {v11, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    goto :goto_1

    .line 286
    .restart local v1    # "arr$":[Ljava/lang/reflect/Constructor;
    .restart local v5    # "i$":I
    .restart local v7    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .restart local v8    # "len$":I
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMostSpecificConstructor(Ljava/util/List;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    .line 287
    if-eqz v4, :cond_5

    .line 288
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    move-object/from16 v0, p2

    invoke-interface {v11, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    .end local v1    # "arr$":[Ljava/lang/reflect/Constructor;
    .end local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "cname":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v7    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .end local v8    # "len$":I
    :cond_4
    :goto_3
    :try_start_2
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v11, v4

    goto :goto_0

    .line 290
    .restart local v1    # "arr$":[Ljava/lang/reflect/Constructor;
    .restart local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v3    # "cname":Ljava/lang/String;
    .restart local v5    # "i$":I
    .restart local v7    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .restart local v8    # "len$":I
    :cond_5
    :try_start_3
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    sget-object v13, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->CTOR_MISS:Ljava/lang/reflect/Constructor;

    move-object/from16 v0, p2

    invoke-interface {v11, v0, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 292
    .end local v1    # "arr$":[Ljava/lang/reflect/Constructor;
    .end local v5    # "i$":I
    .end local v7    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Constructor<*>;>;"
    .end local v8    # "len$":I
    :catch_0
    move-exception v10

    .line 293
    .local v10, "xnotfound":Ljava/lang/ClassNotFoundException;
    :try_start_4
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    if-eqz v11, :cond_6

    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    invoke-interface {v11}, Lorg/apache/commons/logging/Log;->isInfoEnabled()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 294
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "unable to find class: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->debugString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v13, v10}, Lorg/apache/commons/logging/Log;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 298
    :cond_6
    const/4 v4, 0x0

    .line 306
    goto :goto_3

    .line 299
    .end local v10    # "xnotfound":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v9

    .line 300
    .local v9, "xambiguous":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    if-eqz v11, :cond_7

    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    invoke-interface {v11}, Lorg/apache/commons/logging/Log;->isInfoEnabled()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 301
    iget-object v11, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "ambiguous constructor invocation: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->debugString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v13, v9}, Lorg/apache/commons/logging/Log;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 305
    :cond_7
    const/4 v4, 0x0

    goto :goto_3

    .line 309
    .end local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "cname":Ljava/lang/String;
    .end local v9    # "xambiguous":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v11
.end method

.method public getConstructor(Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Constructor;
    .locals 1
    .param p1, "key"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            ")",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 247
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getConstructor(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    return-object v0
.end method

.method public getField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;
    .locals 2
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    move-result-object v0

    .line 125
    .local v0, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    invoke-virtual {v0, p1, p2}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->findField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    return-object v1
.end method

.method public getFieldNames(Ljava/lang/Class;)[Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez p1, :cond_0

    .line 135
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    .line 138
    :goto_0
    return-object v1

    .line 137
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    move-result-object v0

    .line 138
    .local v0, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->getFieldNames()[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getMethod(Ljava/lang/Class;Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;
    .locals 5
    .param p2, "key"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    move-result-object v0

    .line 104
    .local v0, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    invoke-virtual {v0, p2}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->findMethod(Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 112
    .end local v0    # "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    :goto_0
    return-object v2

    .line 105
    :catch_0
    move-exception v1

    .line 107
    .local v1, "xambiguous":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    invoke-interface {v2}, Lorg/apache/commons/logging/Log;->isInfoEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->rlog:Lorg/apache/commons/logging/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ambiguous method invocation: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->debugString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lorg/apache/commons/logging/Log;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 112
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMethodNames(Ljava/lang/Class;)[Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez p1, :cond_0

    .line 148
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    .line 151
    :goto_0
    return-object v1

    .line 150
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    move-result-object v0

    .line 151
    .local v0, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->getMethodNames()[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getMethods(Ljava/lang/Class;Ljava/lang/String;)[Ljava/lang/reflect/Method;
    .locals 2
    .param p2, "methodName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")[",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez p1, :cond_0

    .line 162
    const/4 v1, 0x0

    .line 165
    :goto_0
    return-object v1

    .line 164
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->getMap(Ljava/lang/Class;)Lorg/apache/commons/jexl2/internal/introspection/ClassMap;

    move-result-object v0

    .line 165
    .local v0, "classMap":Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
    invoke-virtual {v0, p2}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap;->get(Ljava/lang/String;)[Ljava/lang/reflect/Method;

    move-result-object v1

    goto :goto_0
.end method

.method public setLoader(Ljava/lang/ClassLoader;)V
    .locals 9
    .param p1, "cloader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 186
    iget-object v5, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->loader:Ljava/lang/ClassLoader;

    .line 187
    .local v5, "previous":Ljava/lang/ClassLoader;
    if-nez p1, :cond_0

    .line 188
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    .line 190
    :cond_0
    iget-object v6, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->loader:Ljava/lang/ClassLoader;

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 192
    iget-object v7, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    monitor-enter v7

    .line 193
    :try_start_0
    iget-object v6, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructorsMap:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 194
    .local v2, "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/commons/jexl2/internal/introspection/MethodKey;Ljava/lang/reflect/Constructor<*>;>;>;"
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 195
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 196
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/commons/jexl2/internal/introspection/MethodKey;Ljava/lang/reflect/Constructor<*>;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/reflect/Constructor;

    invoke-virtual {v6}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    .line 197
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v5, v0}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->isLoadedBy(Ljava/lang/ClassLoader;Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 198
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 200
    iget-object v8, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->constructibleClasses:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    invoke-virtual {v6}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMethod()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v8, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 203
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/commons/jexl2/internal/introspection/MethodKey;Ljava/lang/reflect/Constructor<*>;>;>;"
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/commons/jexl2/internal/introspection/MethodKey;Ljava/lang/reflect/Constructor<*>;>;"
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v2    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/commons/jexl2/internal/introspection/MethodKey;Ljava/lang/reflect/Constructor<*>;>;>;"
    :cond_2
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    iget-object v7, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    monitor-enter v7

    .line 206
    :try_start_2
    iget-object v6, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->classMethodMaps:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 207
    .local v1, "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/commons/jexl2/internal/introspection/ClassMap;>;>;"
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 208
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 209
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/commons/jexl2/internal/introspection/ClassMap;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 210
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v5, v0}, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->isLoadedBy(Ljava/lang/ClassLoader;Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 211
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 214
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/commons/jexl2/internal/introspection/ClassMap;>;>;"
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/commons/jexl2/internal/introspection/ClassMap;>;"
    :catchall_1
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v6

    .restart local v1    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/commons/jexl2/internal/introspection/ClassMap;>;>;"
    :cond_4
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 215
    iput-object p1, p0, Lorg/apache/commons/jexl2/internal/introspection/IntrospectorBase;->loader:Ljava/lang/ClassLoader;

    .line 217
    .end local v1    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/commons/jexl2/internal/introspection/ClassMap;>;>;"
    .end local v2    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/commons/jexl2/internal/introspection/MethodKey;Ljava/lang/reflect/Constructor<*>;>;>;"
    :cond_5
    return-void
.end method
