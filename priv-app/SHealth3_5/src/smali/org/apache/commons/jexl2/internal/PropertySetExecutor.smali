.class public final Lorg/apache/commons/jexl2/internal/PropertySetExecutor;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
.source "PropertySetExecutor.java"


# static fields
.field private static final SET_START_INDEX:I = 0x3


# instance fields
.field private final property:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p3, "identifier"    # Ljava/lang/String;
    .param p4, "arg"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p1, p2, p3, p4}, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 39
    iput-object p3, p0, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->property:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private static discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/reflect/Method;
    .locals 6
    .param p0, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p2, "property"    # Ljava/lang/String;
    .param p3, "arg"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x3

    .line 94
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v2, v4

    .line 95
    .local v2, "params":[Ljava/lang/Object;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "set"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 96
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 99
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v4

    invoke-virtual {v3, v5, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 100
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, v4, v2}, Lorg/apache/commons/jexl2/internal/Introspector;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 102
    .local v1, "method":Ljava/lang/reflect/Method;
    if-nez v1, :cond_0

    .line 103
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    invoke-virtual {v3, v5, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 104
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, v4, v2}, Lorg/apache/commons/jexl2/internal/Introspector;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 107
    :cond_0
    return-object v1
.end method


# virtual methods
.method public execute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "arg"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 53
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 54
    .local v0, "pargs":[Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    :cond_0
    return-object p2
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->property:Ljava/lang/String;

    return-object v0
.end method

.method public tryExecute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;
    .param p3, "arg"    # Ljava/lang/Object;

    .prologue
    .line 63
    if-eqz p1, :cond_1

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->property:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->objectClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p3, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1, p3}, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->execute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 78
    :goto_0
    return-object v2

    .line 72
    :catch_0
    move-exception v1

    .line 73
    .local v1, "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    sget-object v2, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 74
    .end local v1    # "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v0

    .line 75
    .local v0, "xill":Ljava/lang/IllegalAccessException;
    sget-object v2, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 78
    .end local v0    # "xill":Ljava/lang/IllegalAccessException;
    :cond_1
    sget-object v2, Lorg/apache/commons/jexl2/internal/PropertySetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
