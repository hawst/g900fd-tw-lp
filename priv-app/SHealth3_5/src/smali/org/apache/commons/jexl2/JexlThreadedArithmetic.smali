.class public Lorg/apache/commons/jexl2/JexlThreadedArithmetic;
.super Lorg/apache/commons/jexl2/JexlArithmetic;
.source "JexlThreadedArithmetic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;
    }
.end annotation


# static fields
.field static final FEATURES:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$1;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$1;-><init>()V

    sput-object v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;->FEATURES:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0
    .param p1, "lenient"    # Z

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/JexlArithmetic;-><init>(Z)V

    .line 46
    return-void
.end method

.method public constructor <init>(ZLjava/math/MathContext;I)V
    .locals 0
    .param p1, "lenient"    # Z
    .param p2, "bigdContext"    # Ljava/math/MathContext;
    .param p3, "bigdScale"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/JexlArithmetic;-><init>(ZLjava/math/MathContext;I)V

    .line 56
    return-void
.end method

.method public static setLenient(Ljava/lang/Boolean;)V
    .locals 1
    .param p0, "flag"    # Ljava/lang/Boolean;

    .prologue
    .line 78
    sget-object v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;->FEATURES:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    # setter for: Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->lenient:Ljava/lang/Boolean;
    invoke-static {v0, p0}, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->access$002(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 79
    return-void
.end method

.method public static setMathContext(Ljava/math/MathContext;)V
    .locals 1
    .param p0, "mc"    # Ljava/math/MathContext;

    .prologue
    .line 96
    sget-object v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;->FEATURES:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    # setter for: Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathContext:Ljava/math/MathContext;
    invoke-static {v0, p0}, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->access$202(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;Ljava/math/MathContext;)Ljava/math/MathContext;

    .line 97
    return-void
.end method

.method public static setMathScale(Ljava/lang/Integer;)V
    .locals 1
    .param p0, "scale"    # Ljava/lang/Integer;

    .prologue
    .line 87
    sget-object v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;->FEATURES:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    # setter for: Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathScale:Ljava/lang/Integer;
    invoke-static {v0, p0}, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->access$102(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 88
    return-void
.end method


# virtual methods
.method public getMathContext()Ljava/math/MathContext;
    .locals 2

    .prologue
    .line 114
    sget-object v1, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;->FEATURES:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    # getter for: Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathContext:Ljava/math/MathContext;
    invoke-static {v1}, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->access$200(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;)Ljava/math/MathContext;

    move-result-object v0

    .line 115
    .local v0, "mc":Ljava/math/MathContext;
    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathContext()Ljava/math/MathContext;

    move-result-object v0

    .end local v0    # "mc":Ljava/math/MathContext;
    :cond_0
    return-object v0
.end method

.method public getMathScale()I
    .locals 2

    .prologue
    .line 108
    sget-object v1, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;->FEATURES:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    # getter for: Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->mathScale:Ljava/lang/Integer;
    invoke-static {v1}, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->access$100(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;)Ljava/lang/Integer;

    move-result-object v0

    .line 109
    .local v0, "scale":Ljava/lang/Integer;
    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->getMathScale()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public isLenient()Z
    .locals 2

    .prologue
    .line 102
    sget-object v1, Lorg/apache/commons/jexl2/JexlThreadedArithmetic;->FEATURES:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;

    # getter for: Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->lenient:Ljava/lang/Boolean;
    invoke-static {v1}, Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;->access$000(Lorg/apache/commons/jexl2/JexlThreadedArithmetic$Features;)Ljava/lang/Boolean;

    move-result-object v0

    .line 103
    .local v0, "lenient":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/apache/commons/jexl2/JexlArithmetic;->isLenient()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method
