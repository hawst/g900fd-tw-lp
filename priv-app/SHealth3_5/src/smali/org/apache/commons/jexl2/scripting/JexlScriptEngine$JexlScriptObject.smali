.class public Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;
.super Ljava/lang/Object;
.source "JexlScriptEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "JexlScriptObject"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEngine()Lorg/apache/commons/jexl2/JexlEngine;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->jexlEngine:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {v0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->access$100(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v0

    return-object v0
.end method

.method public getErr()Ljava/io/PrintWriter;
    .locals 3

    .prologue
    .line 140
    iget-object v1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->context:Ljavax/script/ScriptContext;
    invoke-static {v1}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->access$300(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Ljavax/script/ScriptContext;

    move-result-object v1

    invoke-interface {v1}, Ljavax/script/ScriptContext;->getErrorWriter()Ljava/io/Writer;

    move-result-object v0

    .line 141
    .local v0, "error":Ljava/io/Writer;
    instance-of v1, v0, Ljava/io/PrintWriter;

    if-eqz v1, :cond_0

    .line 142
    check-cast v0, Ljava/io/PrintWriter;

    .line 146
    .end local v0    # "error":Ljava/io/Writer;
    :goto_0
    return-object v0

    .line 143
    .restart local v0    # "error":Ljava/io/Writer;
    :cond_0
    if-eqz v0, :cond_1

    .line 144
    new-instance v1, Ljava/io/PrintWriter;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V

    move-object v0, v1

    goto :goto_0

    .line 146
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIn()Ljava/io/Reader;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->context:Ljavax/script/ScriptContext;
    invoke-static {v0}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->access$400(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Ljavax/script/ScriptContext;

    move-result-object v0

    invoke-interface {v0}, Ljavax/script/ScriptContext;->getReader()Ljava/io/Reader;

    move-result-object v0

    return-object v0
.end method

.method public getLogger()Lorg/apache/commons/logging/Log;
    .locals 1

    .prologue
    .line 171
    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->LOG:Lorg/apache/commons/logging/Log;
    invoke-static {}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->access$500()Lorg/apache/commons/logging/Log;

    move-result-object v0

    return-object v0
.end method

.method public getOut()Ljava/io/PrintWriter;
    .locals 3

    .prologue
    .line 125
    iget-object v1, p0, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine$JexlScriptObject;->this$0:Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;

    # getter for: Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->context:Ljavax/script/ScriptContext;
    invoke-static {v1}, Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;->access$200(Lorg/apache/commons/jexl2/scripting/JexlScriptEngine;)Ljavax/script/ScriptContext;

    move-result-object v1

    invoke-interface {v1}, Ljavax/script/ScriptContext;->getWriter()Ljava/io/Writer;

    move-result-object v0

    .line 126
    .local v0, "out":Ljava/io/Writer;
    instance-of v1, v0, Ljava/io/PrintWriter;

    if-eqz v1, :cond_0

    .line 127
    check-cast v0, Ljava/io/PrintWriter;

    .line 131
    .end local v0    # "out":Ljava/io/Writer;
    :goto_0
    return-object v0

    .line 128
    .restart local v0    # "out":Ljava/io/Writer;
    :cond_0
    if-eqz v0, :cond_1

    .line 129
    new-instance v1, Ljava/io/PrintWriter;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V

    move-object v0, v1

    goto :goto_0

    .line 131
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSystem()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/System;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    const-class v0, Ljava/lang/System;

    return-object v0
.end method
