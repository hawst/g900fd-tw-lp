.class public Lorg/apache/commons/jexl2/JexlException$Cancel;
.super Lorg/apache/commons/jexl2/JexlException;
.source "JexlException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/JexlException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Cancel"
.end annotation


# direct methods
.method protected constructor <init>(Lorg/apache/commons/jexl2/parser/JexlNode;)V
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    .line 344
    const-string v0, "execution cancelled"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 345
    return-void
.end method
