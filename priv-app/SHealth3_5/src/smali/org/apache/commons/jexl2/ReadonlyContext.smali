.class public final Lorg/apache/commons/jexl2/ReadonlyContext;
.super Ljava/lang/Object;
.source "ReadonlyContext.java"

# interfaces
.implements Lorg/apache/commons/jexl2/JexlContext;


# instance fields
.field private final wrapped:Lorg/apache/commons/jexl2/JexlContext;


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/JexlContext;)V
    .locals 0
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/apache/commons/jexl2/ReadonlyContext;->wrapped:Lorg/apache/commons/jexl2/JexlContext;

    .line 33
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/apache/commons/jexl2/ReadonlyContext;->wrapped:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v0, p1}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public has(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/commons/jexl2/ReadonlyContext;->wrapped:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v0, p1}, Lorg/apache/commons/jexl2/JexlContext;->has(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 46
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
