.class final Lorg/apache/commons/jexl2/Debugger;
.super Ljava/lang/Object;
.source "Debugger.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/ParserVisitor;


# static fields
.field private static final QUOTED_IDENTIFIER:Ljava/util/regex/Pattern;


# instance fields
.field private final builder:Ljava/lang/StringBuilder;

.field private cause:Lorg/apache/commons/jexl2/parser/JexlNode;

.field private end:I

.field private start:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 424
    const-string v0, "[\'\"\\s\\\\]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/Debugger;->QUOTED_IDENTIFIER:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 100
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 101
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 102
    return-void
.end method

.method private accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 172
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    if-ne p1, v1, :cond_0

    .line 173
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 175
    :cond_0
    invoke-virtual {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 176
    .local v0, "value":Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    if-ne p1, v1, :cond_1

    .line 177
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 179
    :cond_1
    return-object v0
.end method

.method private acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "child"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 189
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 191
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, p1, Lorg/apache/commons/jexl2/parser/ASTBlock;

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/commons/jexl2/parser/ASTIfStatement;

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;

    if-eqz v1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-object v0

    .line 197
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "image"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 210
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    if-ne p1, v0, :cond_0

    .line 211
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 213
    :cond_0
    if-eqz p2, :cond_2

    .line 214
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    if-ne p1, v0, :cond_1

    .line 219
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 221
    :cond_1
    return-object p3

    .line 216
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "infix"    # Ljava/lang/String;
    .param p3, "paren"    # Z
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 234
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v1

    .line 235
    .local v1, "num":I
    if-eqz p3, :cond_0

    .line 236
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 239
    if-lez v0, :cond_1

    .line 240
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_1
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p4}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_2
    if-eqz p3, :cond_3

    .line 245
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_3
    return-object p4
.end method

.method private prefixChild(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 259
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v2

    if-le v2, v0, :cond_2

    .line 260
    .local v0, "paren":Z
    :goto_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    if-eqz v0, :cond_0

    .line 262
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_0
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-direct {p0, v1, p3}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    if-eqz v0, :cond_1

    .line 266
    iget-object v1, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :cond_1
    return-object p3

    .end local v0    # "paren":Z
    :cond_2
    move v0, v1

    .line 259
    goto :goto_0
.end method


# virtual methods
.method public data()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public data(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/String;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    const/4 v1, 0x0

    .line 140
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 141
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 142
    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 144
    iput-object p1, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 145
    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public debug(Lorg/apache/commons/jexl2/parser/JexlNode;)Z
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    const/4 v1, 0x0

    .line 110
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    .line 111
    iput v1, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    .line 112
    if-eqz p1, :cond_1

    .line 113
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 114
    iput-object p1, p0, Lorg/apache/commons/jexl2/Debugger;->cause:Lorg/apache/commons/jexl2/parser/JexlNode;

    .line 116
    move-object v0, p1

    .line 117
    .local v0, "root":Lorg/apache/commons/jexl2/parser/JexlNode;
    :goto_0
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 118
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    .end local v0    # "root":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_1
    iget v2, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    if-lez v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public end()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lorg/apache/commons/jexl2/Debugger;->end:I

    return v0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lorg/apache/commons/jexl2/Debugger;->start:I

    return v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 274
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/commons/jexl2/parser/ASTMulNode;

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/commons/jexl2/parser/ASTDivNode;

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/commons/jexl2/parser/ASTModNode;

    if-eqz v4, :cond_2

    :cond_0
    const/4 v2, 0x1

    .line 277
    .local v2, "paren":Z
    :goto_0
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetNumChildren()I

    move-result v1

    .line 278
    .local v1, "num":I
    if-eqz v2, :cond_1

    .line 279
    iget-object v4, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_1
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 283
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "num":I
    .end local v2    # "paren":Z
    :cond_2
    move v2, v3

    .line 274
    goto :goto_0

    .line 285
    .restart local v0    # "i":I
    .restart local v1    # "num":I
    .restart local v2    # "paren":Z
    :cond_3
    if-eqz v2, :cond_4

    .line 286
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    :cond_4
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/16 v2, 0x20

    .line 293
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 294
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    iget-object v1, p1, Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;->image:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 296
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAmbiguous;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAmbiguous;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 698
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "unexpected type of node"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAndNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAndNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 301
    const-string v0, " && "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTArrayAccess;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 306
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetNumChildren()I

    move-result v1

    .line 308
    .local v1, "num":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 309
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    :cond_0
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 318
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetNumChildren()I

    move-result v1

    .line 319
    .local v1, "num":I
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "[ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    if-lez v1, :cond_0

    .line 321
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 323
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 327
    .end local v0    # "i":I
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, " ]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAssignment;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAssignment;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 333
    const-string v0, " = "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 338
    const-string v0, " & "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 343
    const-string/jumbo v0, "~"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->prefixChild(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 348
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    instance-of v0, v1, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;

    .line 349
    .local v0, "paren":Z
    const-string v1, " | "

    invoke-direct {p0, p1, v1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 354
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    instance-of v0, v1, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;

    .line 355
    .local v0, "paren":Z
    const-string v1, " ^ "

    invoke-direct {p0, p1, v1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBlock;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBlock;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 360
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v4, "{ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTBlock;->jjtGetNumChildren()I

    move-result v2

    .line 362
    .local v2, "num":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 363
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTBlock;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 364
    .local v0, "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 366
    .end local v0    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v4, " }"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTConstructorNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTConstructorNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 507
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetNumChildren()I

    move-result v1

    .line 508
    .local v1, "num":I
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "new "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 512
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 515
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTDivNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTDivNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 372
    const-string v0, " / "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTEQNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTEQNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 385
    const-string v0, " == "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTERNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTERNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 390
    const-string v0, " =~ "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 377
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, "empty("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFalseNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFalseNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 395
    const-string v0, "false"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTForeachStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTForeachStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x2

    .line 400
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, "for("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 406
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    :goto_0
    return-object p2

    .line 408
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFunctionNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFunctionNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 521
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetNumChildren()I

    move-result v1

    .line 522
    .local v1, "num":I
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 527
    const/4 v2, 0x2

    if-le v0, v2, :cond_0

    .line 528
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 532
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 533
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTGENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTGENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 415
    const-string v0, " >= "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTGTNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTGTNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 420
    const-string v0, " > "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIdentifier;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 428
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    .line 429
    .local v0, "image":Ljava/lang/String;
    sget-object v1, Lorg/apache/commons/jexl2/Debugger;->QUOTED_IDENTIFIER:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 431
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    const-string v3, "\'"

    const-string v4, "\\\'"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 433
    :cond_0
    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIfStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIfStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/16 v4, 0x3b

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 438
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, "if ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 442
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 444
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, " else "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    :goto_0
    return-object p2

    .line 447
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 450
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 462
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetNumChildren()I

    move-result v2

    .line 463
    .local v2, "num":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 464
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 465
    .local v0, "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 467
    .end local v0    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_0
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTLENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTLENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 472
    const-string v0, " <= "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTLTNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTLTNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 477
    const-string v0, " < "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMapEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMapEntry;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 482
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTMapEntry;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTMapEntry;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMapLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMapLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 490
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetNumChildren()I

    move-result v1

    .line 491
    .local v1, "num":I
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v3, "{ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    if-lez v1, :cond_0

    .line 493
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 495
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 499
    .end local v0    # "i":I
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 501
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, " }"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMethodNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMethodNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 538
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetNumChildren()I

    move-result v1

    .line 539
    .local v1, "num":I
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 541
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 542
    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 543
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 545
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 547
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTModNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTModNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 553
    const-string v0, " % "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMulNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMulNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 558
    const-string v0, " * "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 563
    const-string v0, " != "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNRNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNRNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 568
    const-string v0, " !~ "

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNotNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNotNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 573
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, "!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTNotNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 575
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNullLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNullLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 580
    const-string/jumbo v0, "null"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 457
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTNumberLiteral;->image:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTOrNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTOrNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 587
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    instance-of v0, v1, Lorg/apache/commons/jexl2/parser/ASTAndNode;

    .line 588
    .local v0, "paren":Z
    const-string v1, " || "

    invoke-direct {p0, p1, v1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->infixChildren(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTReference;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTReference;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 593
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetNumChildren()I

    move-result v1

    .line 594
    .local v1, "num":I
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 596
    iget-object v2, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 599
    :cond_0
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 604
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 605
    .local v0, "first":Lorg/apache/commons/jexl2/parser/JexlNode;
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 606
    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 608
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;->jjtGetNumChildren()I

    move-result v2

    .line 609
    .local v2, "num":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 610
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 611
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTReferenceExpression;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 612
    iget-object v3, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 609
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 614
    :cond_0
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTReturnStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTReturnStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "return "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTReturnStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTSizeFunction;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTSizeFunction;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 626
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "size("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTSizeFunction;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 628
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 629
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTSizeMethod;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTSizeMethod;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 634
    const-string/jumbo v0, "size()"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 635
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTStringLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTStringLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 640
    iget-object v1, p1, Lorg/apache/commons/jexl2/parser/ASTStringLiteral;->image:Ljava/lang/String;

    const-string v2, "\'"

    const-string v3, "\\\'"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 641
    .local v0, "img":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTTernaryNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTTernaryNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 646
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 648
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, "? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 650
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 651
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 657
    :goto_0
    return-object p2

    .line 653
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, "?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 654
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTTrueNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTTrueNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 662
    const-string/jumbo v0, "true"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 663
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 668
    const-string v0, "-"

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->prefixChild(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTVar;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTVar;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 673
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "var "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTVar;->image:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->check(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 675
    return-object p2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTWhileStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTWhileStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 680
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "while ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->accept(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 683
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetNumChildren()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 684
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/jexl2/Debugger;->acceptStatement(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 688
    :goto_0
    return-object p2

    .line 686
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/Debugger;->builder:Ljava/lang/StringBuilder;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/SimpleNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/SimpleNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 693
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "unexpected type of node"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
