.class final Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;
.super Ljava/lang/Object;
.source "UberspectImpl.java"

# interfaces
.implements Lorg/apache/commons/jexl2/introspection/JexlMethod;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/introspection/UberspectImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ConstructorMethod"
.end annotation


# instance fields
.field private final ctor:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/commons/jexl2/introspection/UberspectImpl;


# direct methods
.method private constructor <init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl;Ljava/lang/reflect/Constructor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Constructor",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 373
    .local p2, "theCtor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    iput-object p1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->this$0:Lorg/apache/commons/jexl2/introspection/UberspectImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374
    iput-object p2, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->ctor:Ljava/lang/reflect/Constructor;

    .line 375
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl;Ljava/lang/reflect/Constructor;Lorg/apache/commons/jexl2/introspection/UberspectImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/commons/jexl2/introspection/UberspectImpl;
    .param p2, "x1"    # Ljava/lang/reflect/Constructor;
    .param p3, "x2"    # Lorg/apache/commons/jexl2/introspection/UberspectImpl$1;

    .prologue
    .line 365
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;-><init>(Lorg/apache/commons/jexl2/introspection/UberspectImpl;Ljava/lang/reflect/Constructor;)V

    return-void
.end method


# virtual methods
.method public getReturnType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 443
    iget-object v0, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->ctor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v0}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "params"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 381
    const/4 v0, 0x0

    .line 382
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    instance-of v1, p1, Ljava/lang/Class;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 383
    check-cast v0, Ljava/lang/Class;

    .line 389
    :goto_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->ctor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v1}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 390
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->ctor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v1, p2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1

    .line 384
    :cond_0
    if-eqz p1, :cond_1

    .line 385
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->this$0:Lorg/apache/commons/jexl2/introspection/UberspectImpl;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getClassByName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 387
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->ctor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v1}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 392
    :cond_2
    new-instance v1, Ljava/beans/IntrospectionException;

    const-string v2, "constructor resolution error"

    invoke-direct {v1, v2}, Ljava/beans/IntrospectionException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x1

    return v0
.end method

.method public tryFailed(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "rval"    # Ljava/lang/Object;

    .prologue
    .line 429
    sget-object v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tryInvoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "params"    # [Ljava/lang/Object;

    .prologue
    .line 400
    const/4 v0, 0x0

    .line 401
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    instance-of v5, p2, Ljava/lang/Class;

    if-eqz v5, :cond_1

    move-object v0, p2

    .line 402
    check-cast v0, Ljava/lang/Class;

    .line 408
    :goto_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->ctor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 411
    :cond_0
    :try_start_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->ctor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v5, p3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v5

    .line 422
    :goto_1
    return-object v5

    .line 403
    :cond_1
    if-eqz p2, :cond_2

    .line 404
    iget-object v5, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->this$0:Lorg/apache/commons/jexl2/introspection/UberspectImpl;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getClassByName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 406
    :cond_2
    iget-object v5, p0, Lorg/apache/commons/jexl2/introspection/UberspectImpl$ConstructorMethod;->ctor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v3

    .line 413
    .local v3, "xinstance":Ljava/lang/InstantiationException;
    sget-object v5, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_1

    .line 414
    .end local v3    # "xinstance":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v1

    .line 415
    .local v1, "xaccess":Ljava/lang/IllegalAccessException;
    sget-object v5, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_1

    .line 416
    .end local v1    # "xaccess":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .line 417
    .local v2, "xargument":Ljava/lang/IllegalArgumentException;
    sget-object v5, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_1

    .line 418
    .end local v2    # "xargument":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v4

    .line 419
    .local v4, "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    sget-object v5, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_1

    .line 422
    .end local v4    # "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    :cond_3
    sget-object v5, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_1
.end method
