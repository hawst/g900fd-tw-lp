.class public Lorg/apache/commons/jexl2/Main;
.super Ljava/lang/Object;
.source "Main.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 11
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v3, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-direct {v3}, Lorg/apache/commons/jexl2/JexlEngine;-><init>()V

    .line 46
    .local v3, "engine":Lorg/apache/commons/jexl2/JexlEngine;
    new-instance v1, Lorg/apache/commons/jexl2/MapContext;

    invoke-direct {v1}, Lorg/apache/commons/jexl2/MapContext;-><init>()V

    .line 47
    .local v1, "context":Lorg/apache/commons/jexl2/JexlContext;
    const-string v8, "args"

    invoke-interface {v1, v8, p0}, Lorg/apache/commons/jexl2/JexlContext;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    array-length v8, p0

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 49
    new-instance v8, Ljava/io/File;

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Lorg/apache/commons/jexl2/JexlEngine;->createScript(Ljava/io/File;)Lorg/apache/commons/jexl2/Script;

    move-result-object v6

    .line 50
    .local v6, "script":Lorg/apache/commons/jexl2/Script;
    invoke-interface {v6, v1}, Lorg/apache/commons/jexl2/Script;->execute(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;

    move-result-object v7

    .line 51
    .local v7, "value":Ljava/lang/Object;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Return value: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 67
    .end local v6    # "script":Lorg/apache/commons/jexl2/Script;
    .end local v7    # "value":Ljava/lang/Object;
    :cond_0
    return-void

    .line 53
    :cond_1
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    sget-object v9, Ljava/lang/System;->in:Ljava/io/InputStream;

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 55
    .local v0, "console":Ljava/io/BufferedReader;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "> "

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 56
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, "line":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 58
    :try_start_0
    invoke-virtual {v3, v5}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v4

    .line 59
    .local v4, "expression":Lorg/apache/commons/jexl2/Expression;
    invoke-interface {v4, v1}, Lorg/apache/commons/jexl2/Expression;->evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;

    move-result-object v7

    .line 60
    .restart local v7    # "value":Ljava/lang/Object;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Return value: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v4    # "expression":Lorg/apache/commons/jexl2/Expression;
    .end local v7    # "value":Ljava/lang/Object;
    :goto_1
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "> "

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :catch_0
    move-exception v2

    .line 62
    .local v2, "e":Lorg/apache/commons/jexl2/JexlException;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/JexlException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1
.end method
