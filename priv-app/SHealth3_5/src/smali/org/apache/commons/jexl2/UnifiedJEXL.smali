.class public final Lorg/apache/commons/jexl2/UnifiedJEXL;
.super Ljava/lang/Object;
.source "UnifiedJEXL.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/UnifiedJEXL$1;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateContext;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$Template;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$CompositeExpression;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$JexlBasedExpression;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;,
        Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionType;
    }
.end annotation


# static fields
.field private static final CACHE_SIZE:I = 0x100

.field private static final DEF_CHAR:C = '#'

.field private static final IMM_CHAR:C = '$'


# instance fields
.field private final cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/jexl2/JexlEngine$SoftCache",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;",
            ">;"
        }
    .end annotation
.end field

.field private final jexl:Lorg/apache/commons/jexl2/JexlEngine;


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;)V
    .locals 1
    .param p1, "aJexl"    # Lorg/apache/commons/jexl2/JexlEngine;

    .prologue
    .line 104
    const/16 v0, 0x100

    invoke-direct {p0, p1, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL;-><init>(Lorg/apache/commons/jexl2/JexlEngine;I)V

    .line 105
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;I)V
    .locals 1
    .param p1, "aJexl"    # Lorg/apache/commons/jexl2/JexlEngine;
    .param p2, "cacheSize"    # I

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    .line 114
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p1, p2}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;-><init>(Lorg/apache/commons/jexl2/JexlEngine;I)V

    iput-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    .line 115
    return-void
.end method

.method static synthetic access$100(Lorg/apache/commons/jexl2/UnifiedJEXL;)Lorg/apache/commons/jexl2/JexlEngine;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/UnifiedJEXL;

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    return-object v0
.end method

.method static synthetic access$200(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/UnifiedJEXL;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .param p3, "x3"    # Ljava/lang/Exception;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/UnifiedJEXL;->createException(Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/UnifiedJEXL;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/UnifiedJEXL;->parseExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v0

    return-object v0
.end method

.method private createException(Ljava/lang/String;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;Ljava/lang/Exception;)Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    .locals 5
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "expr"    # Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .param p3, "xany"    # Ljava/lang/Exception;

    .prologue
    .line 784
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "failed to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 785
    .local v2, "strb":Ljava/lang/StringBuilder;
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 786
    if-eqz p2, :cond_0

    .line 787
    const-string v3, " \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 788
    invoke-virtual {p2}, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 791
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 792
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_1

    .line 793
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 794
    .local v1, "causeMsg":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 795
    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 799
    .end local v1    # "causeMsg":Ljava/lang/String;
    :cond_1
    new-instance v3, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p3}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v3
.end method

.method private parseExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 16
    .param p1, "expr"    # Ljava/lang/String;
    .param p2, "scope"    # Lorg/apache/commons/jexl2/JexlEngine$Scope;

    .prologue
    .line 826
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v10

    .line 827
    .local v10, "size":I
    new-instance v1, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;

    const/4 v13, 0x0

    invoke-direct {v1, v13}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;-><init>(I)V

    .line 828
    .local v1, "builder":Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 829
    .local v12, "strb":Ljava/lang/StringBuilder;
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 830
    .local v11, "state":Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;
    const/4 v8, 0x0

    .line 831
    .local v8, "inner":I
    const/4 v9, 0x0

    .line 832
    .local v9, "nested":Z
    const/4 v7, -0x1

    .line 833
    .local v7, "inested":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v10, :cond_f

    .line 834
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 835
    .local v2, "c":C
    sget-object v13, Lorg/apache/commons/jexl2/UnifiedJEXL$1;->$SwitchMap$org$apache$commons$jexl2$UnifiedJEXL$ParseState:[I

    invoke-virtual {v11}, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 837
    new-instance v13, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v14, "unexpected expression type"

    invoke-direct {v13, v14}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 839
    :pswitch_0
    const/16 v13, 0x24

    if-ne v2, v13, :cond_1

    .line 840
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->IMMEDIATE0:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 833
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 841
    :cond_1
    const/16 v13, 0x23

    if-ne v2, v13, :cond_2

    .line 842
    move v7, v5

    .line 843
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->DEFERRED0:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    goto :goto_1

    .line 844
    :cond_2
    const/16 v13, 0x5c

    if-ne v2, v13, :cond_3

    .line 845
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->ESCAPE:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    goto :goto_1

    .line 848
    :cond_3
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 852
    :pswitch_1
    const/16 v13, 0x7b

    if-ne v2, v13, :cond_4

    .line 853
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->IMMEDIATE1:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 855
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-lez v13, :cond_0

    .line 856
    new-instance v3, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v13, v14}, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/Object;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 857
    .local v3, "cexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v1, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->add(Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 858
    const/4 v13, 0x0

    const v14, 0x7fffffff

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 862
    .end local v3    # "cexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_4
    const/16 v13, 0x24

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 863
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 864
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 866
    goto :goto_1

    .line 868
    :pswitch_2
    const/16 v13, 0x7b

    if-ne v2, v13, :cond_5

    .line 869
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->DEFERRED1:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 871
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-lez v13, :cond_0

    .line 872
    new-instance v3, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v13, v14}, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/Object;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 873
    .restart local v3    # "cexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v1, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->add(Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 874
    const/4 v13, 0x0

    const v14, 0x7fffffff

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 878
    .end local v3    # "cexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_5
    const/16 v13, 0x23

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 879
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 880
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 882
    goto :goto_1

    .line 884
    :pswitch_3
    const/16 v13, 0x7d

    if-ne v2, v13, :cond_6

    .line 886
    new-instance v6, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v14, v12, v15, v0}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v13, v14, v15}, Lorg/apache/commons/jexl2/UnifiedJEXL$ImmediateExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 890
    .local v6, "iexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v1, v6}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->add(Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 891
    const/4 v13, 0x0

    const v14, 0x7fffffff

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 892
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 893
    goto/16 :goto_1

    .line 895
    .end local v6    # "iexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_6
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 900
    :pswitch_4
    const/16 v13, 0x22

    if-eq v2, v13, :cond_7

    const/16 v13, 0x27

    if-ne v2, v13, :cond_8

    .line 901
    :cond_7
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 902
    add-int/lit8 v13, v5, 0x1

    move-object/from16 v0, p1

    invoke-static {v12, v0, v13, v2}, Lorg/apache/commons/jexl2/parser/StringParser;->readString(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;IC)I

    move-result v5

    .line 903
    goto/16 :goto_1

    .line 906
    :cond_8
    const/16 v13, 0x7b

    if-ne v2, v13, :cond_9

    .line 907
    add-int/lit8 v13, v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0x24

    if-ne v13, v14, :cond_0

    .line 908
    add-int/lit8 v8, v8, 0x1

    .line 909
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 910
    const/4 v9, 0x1

    goto/16 :goto_1

    .line 915
    :cond_9
    const/16 v13, 0x7d

    if-ne v2, v13, :cond_c

    .line 917
    if-lez v8, :cond_a

    .line 918
    add-int/lit8 v8, v8, -0x1

    goto/16 :goto_1

    .line 921
    :cond_a
    const/4 v4, 0x0

    .line 922
    .local v4, "dexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    if-eqz v9, :cond_b

    .line 923
    new-instance v4, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;

    .end local v4    # "dexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    add-int/lit8 v13, v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v14, v12, v15, v0}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v13, v14, v15}, Lorg/apache/commons/jexl2/UnifiedJEXL$NestedExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 933
    .restart local v4    # "dexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :goto_2
    invoke-virtual {v1, v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->add(Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 934
    const/4 v13, 0x0

    const v14, 0x7fffffff

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 935
    const/4 v9, 0x0

    .line 936
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    .line 937
    goto/16 :goto_1

    .line 928
    :cond_b
    new-instance v4, Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;

    .end local v4    # "dexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v14, v12, v15, v0}, Lorg/apache/commons/jexl2/JexlEngine;->parse(Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/JexlInfo;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v13, v14, v15}, Lorg/apache/commons/jexl2/UnifiedJEXL$DeferredExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/CharSequence;Lorg/apache/commons/jexl2/parser/JexlNode;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .restart local v4    # "dexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    goto :goto_2

    .line 940
    .end local v4    # "dexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_c
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 944
    :pswitch_5
    const/16 v13, 0x23

    if-ne v2, v13, :cond_d

    .line 945
    const/16 v13, 0x23

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 952
    :goto_3
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    goto/16 :goto_1

    .line 946
    :cond_d
    const/16 v13, 0x24

    if-ne v2, v13, :cond_e

    .line 947
    const/16 v13, 0x24

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 949
    :cond_e
    const/16 v13, 0x5c

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 950
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 956
    .end local v2    # "c":C
    :cond_f
    sget-object v13, Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;->CONST:Lorg/apache/commons/jexl2/UnifiedJEXL$ParseState;

    if-eq v11, v13, :cond_10

    .line 957
    new-instance v13, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "malformed expression: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v13, v14, v15}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v13

    .line 960
    :cond_10
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    if-lez v13, :cond_11

    .line 961
    new-instance v3, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v13, v14}, Lorg/apache/commons/jexl2/UnifiedJEXL$ConstantExpression;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/Object;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 962
    .restart local v3    # "cexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    invoke-virtual {v1, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->add(Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)V

    .line 964
    .end local v3    # "cexpr":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :cond_11
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v1, v0, v13}, Lorg/apache/commons/jexl2/UnifiedJEXL$ExpressionBuilder;->build(Lorg/apache/commons/jexl2/UnifiedJEXL;Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v13

    return-object v13

    .line 835
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public clearCache()V
    .locals 2

    .prologue
    .line 217
    iget-object v1, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    monitor-enter v1

    .line 218
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->clear()V

    .line 219
    monitor-exit v1

    .line 220
    return-void

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public createTemplate(Ljava/lang/String;)Lorg/apache/commons/jexl2/UnifiedJEXL$Template;
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 1462
    new-instance v1, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;

    const-string v2, "$$"

    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3, v0}, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Ljava/io/Reader;[Ljava/lang/String;)V

    return-object v1
.end method

.method public varargs createTemplate(Ljava/lang/String;Ljava/io/Reader;[Ljava/lang/String;)Lorg/apache/commons/jexl2/UnifiedJEXL$Template;
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "source"    # Ljava/io/Reader;
    .param p3, "parms"    # [Ljava/lang/String;

    .prologue
    .line 1441
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Ljava/io/Reader;[Ljava/lang/String;)V

    return-object v0
.end method

.method public varargs createTemplate(Ljava/lang/String;[Ljava/lang/String;)Lorg/apache/commons/jexl2/UnifiedJEXL$Template;
    .locals 3
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "parms"    # [Ljava/lang/String;

    .prologue
    .line 1452
    new-instance v0, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;

    const-string v1, "$$"

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, p0, v1, v2, p2}, Lorg/apache/commons/jexl2/UnifiedJEXL$Template;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL;Ljava/lang/String;Ljava/io/Reader;[Ljava/lang/String;)V

    return-object v0
.end method

.method public getEngine()Lorg/apache/commons/jexl2/JexlEngine;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    return-object v0
.end method

.method public parse(Ljava/lang/String;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    .locals 10
    .param p1, "expression"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 746
    const/4 v4, 0x0

    .line 747
    .local v4, "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    const/4 v1, 0x0

    .line 749
    .local v1, "stmt":Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :try_start_0
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-nez v6, :cond_0

    .line 750
    const/4 v6, 0x0

    invoke-direct {p0, p1, v6}, Lorg/apache/commons/jexl2/UnifiedJEXL;->parseExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/commons/jexl2/UnifiedJEXL$Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 765
    :goto_0
    if-eqz v4, :cond_8

    .line 766
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v6}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 767
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    iget-object v6, v6, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v6, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v6, v7

    .line 773
    :goto_1
    return-object v6

    .line 752
    :cond_0
    :try_start_1
    iget-object v8, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    monitor-enter v8
    :try_end_1
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/commons/jexl2/UnifiedJEXL$Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 753
    :try_start_2
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v6, p1}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-object v1, v0

    .line 754
    if-nez v1, :cond_1

    .line 755
    const/4 v6, 0x0

    invoke-direct {p0, p1, v6}, Lorg/apache/commons/jexl2/UnifiedJEXL;->parseExpression(Ljava/lang/String;Lorg/apache/commons/jexl2/JexlEngine$Scope;)Lorg/apache/commons/jexl2/UnifiedJEXL$Expression;

    move-result-object v1

    .line 756
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    invoke-virtual {v6, p1, v1}, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 758
    :cond_1
    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v6
    :try_end_3
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/apache/commons/jexl2/UnifiedJEXL$Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 760
    :catch_0
    move-exception v3

    .line 761
    .local v3, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_4
    new-instance v5, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to parse \'"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\'"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v3}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 765
    .end local v4    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    .local v5, "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    if-eqz v5, :cond_7

    .line 766
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v6}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 767
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    iget-object v6, v6, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v6, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v4, v5

    .end local v5    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    .restart local v4    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    move-object v6, v7

    .line 768
    goto :goto_1

    .line 770
    .end local v3    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_2
    throw v4

    .end local v4    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    .restart local v3    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v5    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    :cond_3
    throw v5

    .line 762
    .end local v3    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .end local v5    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    .restart local v4    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    :catch_1
    move-exception v2

    .line 763
    .local v2, "xany":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    move-object v4, v2

    .line 765
    if-eqz v4, :cond_8

    .line 766
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v6}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 767
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    iget-object v6, v6, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v6, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v6, v7

    .line 768
    goto :goto_1

    .line 770
    :cond_4
    throw v4

    .line 765
    .end local v2    # "xany":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    :catchall_1
    move-exception v6

    if-eqz v4, :cond_6

    .line 766
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v6}, Lorg/apache/commons/jexl2/JexlEngine;->isSilent()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 767
    iget-object v6, p0, Lorg/apache/commons/jexl2/UnifiedJEXL;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    iget-object v6, v6, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-interface {v6, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v6, v7

    .line 768
    goto/16 :goto_1

    .line 770
    :cond_5
    throw v4

    :cond_6
    throw v6

    .end local v4    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    .restart local v3    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v5    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    :cond_7
    move-object v4, v5

    .end local v3    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .end local v5    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    .restart local v4    # "xuel":Lorg/apache/commons/jexl2/UnifiedJEXL$Exception;
    :cond_8
    move-object v6, v1

    .line 773
    goto/16 :goto_1
.end method

.method protected readTemplate(Ljava/lang/String;Ljava/io/Reader;)Ljava/util/List;
    .locals 13
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "source"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/Reader;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1371
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    .line 1372
    .local v5, "prefixLen":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1374
    .local v2, "blocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;>;"
    instance-of v11, p2, Ljava/io/BufferedReader;

    if-eqz v11, :cond_1

    .line 1375
    move-object v0, p2

    check-cast v0, Ljava/io/BufferedReader;

    move-object v6, v0

    .line 1379
    .local v6, "reader":Ljava/io/BufferedReader;
    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1380
    .local v7, "strb":Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    .line 1382
    .local v8, "type":Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    :cond_0
    :goto_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 1383
    .local v4, "line":Ljava/lang/CharSequence;
    if-nez v4, :cond_2

    .line 1385
    new-instance v1, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v1, v8, v11}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;Ljava/lang/String;)V

    .line 1386
    .local v1, "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1428
    .end local v1    # "block":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    .end local v2    # "blocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;>;"
    .end local v4    # "line":Ljava/lang/CharSequence;
    .end local v5    # "prefixLen":I
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .end local v7    # "strb":Ljava/lang/StringBuilder;
    .end local v8    # "type":Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    :goto_2
    return-object v2

    .line 1377
    .restart local v2    # "blocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;>;"
    .restart local v5    # "prefixLen":I
    :cond_1
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, p2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .restart local v6    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 1388
    .restart local v4    # "line":Ljava/lang/CharSequence;
    .restart local v7    # "strb":Ljava/lang/StringBuilder;
    .restart local v8    # "type":Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    :cond_2
    if-nez v8, :cond_4

    .line 1390
    invoke-virtual {p0, v4, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL;->startsWith(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v5

    .line 1391
    if-ltz v5, :cond_3

    .line 1392
    sget-object v8, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    .line 1393
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v11

    invoke-interface {v4, v5, v11}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1427
    .end local v2    # "blocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;>;"
    .end local v4    # "line":Ljava/lang/CharSequence;
    .end local v5    # "prefixLen":I
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .end local v7    # "strb":Ljava/lang/StringBuilder;
    .end local v8    # "type":Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    :catch_0
    move-exception v10

    .line 1428
    .local v10, "xio":Ljava/io/IOException;
    const/4 v2, 0x0

    goto :goto_2

    .line 1395
    .end local v10    # "xio":Ljava/io/IOException;
    .restart local v2    # "blocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;>;"
    .restart local v4    # "line":Ljava/lang/CharSequence;
    .restart local v5    # "prefixLen":I
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "strb":Ljava/lang/StringBuilder;
    .restart local v8    # "type":Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;
    :cond_3
    sget-object v8, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    .line 1396
    const/4 v11, 0x0

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v12

    invoke-interface {v4, v11, v12}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1397
    const/16 v11, 0xa

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1399
    :cond_4
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    if-ne v8, v11, :cond_6

    .line 1401
    invoke-virtual {p0, v4, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL;->startsWith(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v5

    .line 1402
    if-gez v5, :cond_5

    .line 1403
    new-instance v3, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v3, v11, v12}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;Ljava/lang/String;)V

    .line 1404
    .local v3, "code":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    const/4 v11, 0x0

    const v12, 0x7fffffff

    invoke-virtual {v7, v11, v12}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1405
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1406
    sget-object v8, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    .line 1407
    const/4 v11, 0x0

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v12

    invoke-interface {v4, v11, v12}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1409
    .end local v3    # "code":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    :cond_5
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v11

    invoke-interface {v4, v5, v11}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1411
    :cond_6
    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    if-ne v8, v11, :cond_0

    .line 1413
    invoke-virtual {p0, v4, p1}, Lorg/apache/commons/jexl2/UnifiedJEXL;->startsWith(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v5

    .line 1414
    if-ltz v5, :cond_7

    .line 1415
    const/16 v11, 0xa

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1416
    new-instance v9, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;

    sget-object v11, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->VERBATIM:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v11, v12}, Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;-><init>(Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;Ljava/lang/String;)V

    .line 1417
    .local v9, "verbatim":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    const/4 v11, 0x0

    const v12, 0x7fffffff

    invoke-virtual {v7, v11, v12}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1418
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1419
    sget-object v8, Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;->DIRECTIVE:Lorg/apache/commons/jexl2/UnifiedJEXL$BlockType;

    .line 1420
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v11

    invoke-interface {v4, v5, v11}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1422
    .end local v9    # "verbatim":Lorg/apache/commons/jexl2/UnifiedJEXL$TemplateBlock;
    :cond_7
    const/4 v11, 0x0

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v12

    invoke-interface {v4, v11, v12}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1
.end method

.method protected startsWith(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
    .locals 3
    .param p1, "sequence"    # Ljava/lang/CharSequence;
    .param p2, "pattern"    # Ljava/lang/CharSequence;

    .prologue
    .line 1349
    const/4 v0, 0x0

    .line 1350
    .local v0, "s":I
    :goto_0
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1351
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1353
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    .line 1354
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-gt v1, v2, :cond_1

    const/4 v1, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {p1, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1356
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v1, v0

    .line 1358
    :goto_1
    return v1

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method
