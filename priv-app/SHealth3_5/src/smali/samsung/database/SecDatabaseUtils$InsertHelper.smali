.class public Lsamsung/database/SecDatabaseUtils$InsertHelper;
.super Ljava/lang/Object;
.source "SecDatabaseUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsamsung/database/SecDatabaseUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InsertHelper"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final TABLE_INFO_PRAGMA_COLUMNNAME_INDEX:I = 0x1

.field public static final TABLE_INFO_PRAGMA_DEFAULT_INDEX:I = 0x4


# instance fields
.field private mColumns:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

.field private mInsertSQL:Ljava/lang/String;

.field private mInsertStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

.field private mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

.field private mReplaceStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

.field private final mTableName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;)V
    .locals 1
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 950
    iput-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertSQL:Ljava/lang/String;

    .line 951
    iput-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 952
    iput-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mReplaceStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 953
    iput-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 976
    iput-object p1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 977
    iput-object p2, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mTableName:Ljava/lang/String;

    .line 978
    return-void
.end method

.method private buildSQL()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x80

    .line 981
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 982
    .local v4, "sb":Ljava/lang/StringBuilder;
    const-string v6, "INSERT INTO "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 983
    iget-object v6, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mTableName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 984
    const-string v6, " ("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 986
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 987
    .local v5, "sbv":Ljava/lang/StringBuilder;
    const-string v6, "VALUES ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 989
    const/4 v3, 0x1

    .line 990
    .local v3, "i":I
    const/4 v1, 0x0

    .line 992
    .local v1, "cur":Landroid/database/Cursor;
    :try_start_0
    iget-object v6, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "PRAGMA table_info("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mTableName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 993
    new-instance v6, Ljava/util/HashMap;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/HashMap;-><init>(I)V

    iput-object v6, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mColumns:Ljava/util/HashMap;

    .line 994
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_1

    .line 1016
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1019
    :cond_0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1021
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertSQL:Ljava/lang/String;

    .line 1023
    return-void

    .line 995
    :cond_1
    const/4 v6, 0x1

    :try_start_1
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 996
    .local v0, "columnName":Ljava/lang/String;
    const/4 v6, 0x4

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 998
    .local v2, "defaultValue":Ljava/lang/String;
    iget-object v6, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mColumns:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 999
    const-string v6, "\'"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1000
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1001
    const-string v6, "\'"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1003
    if-nez v2, :cond_2

    .line 1004
    const-string v6, "?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1011
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ne v3, v6, :cond_4

    const-string v6, ") "

    :goto_2
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1012
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ne v3, v6, :cond_5

    const-string v6, ");"

    :goto_3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1013
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1006
    :cond_2
    const-string v6, "COALESCE(?, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1007
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1008
    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1015
    .end local v0    # "columnName":Ljava/lang/String;
    .end local v2    # "defaultValue":Ljava/lang/String;
    :catchall_0
    move-exception v6

    .line 1016
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1017
    :cond_3
    throw v6

    .line 1011
    .restart local v0    # "columnName":Ljava/lang/String;
    .restart local v2    # "defaultValue":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v6, ", "

    goto :goto_2

    .line 1012
    :cond_5
    const-string v6, ", "
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private getStatement(Z)Lsamsung/database/sqlite/SecSQLiteStatement;
    .locals 4
    .param p1, "allowReplace"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1026
    if-eqz p1, :cond_2

    .line 1027
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mReplaceStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    if-nez v1, :cond_1

    .line 1028
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertSQL:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->buildSQL()V

    .line 1030
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "INSERT OR REPLACE"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertSQL:Ljava/lang/String;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1031
    .local v0, "replaceSQL":Ljava/lang/String;
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->compileStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mReplaceStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1033
    .end local v0    # "replaceSQL":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mReplaceStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1039
    :goto_0
    return-object v1

    .line 1035
    :cond_2
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    if-nez v1, :cond_4

    .line 1036
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertSQL:Ljava/lang/String;

    if-nez v1, :cond_3

    invoke-direct {p0}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->buildSQL()V

    .line 1037
    :cond_3
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    iget-object v2, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertSQL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->compileStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1039
    :cond_4
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    goto :goto_0
.end method

.method private insertInternal(Landroid/content/ContentValues;Z)J
    .locals 10
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "allowReplace"    # Z

    .prologue
    .line 1062
    iget-object v7, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v7}, Lsamsung/database/sqlite/SecSQLiteDatabase;->beginTransactionNonExclusive()V

    .line 1064
    :try_start_0
    invoke-direct {p0, p2}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->getStatement(Z)Lsamsung/database/sqlite/SecSQLiteStatement;

    move-result-object v6

    .line 1065
    .local v6, "stmt":Lsamsung/database/sqlite/SecSQLiteStatement;
    invoke-virtual {v6}, Lsamsung/database/sqlite/SecSQLiteStatement;->clearBindings()V

    .line 1067
    invoke-virtual {p1}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1076
    invoke-virtual {v6}, Lsamsung/database/sqlite/SecSQLiteStatement;->executeInsert()J

    move-result-wide v4

    .line 1077
    .local v4, "result":J
    iget-object v7, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v7}, Lsamsung/database/sqlite/SecSQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1083
    iget-object v7, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v7}, Lsamsung/database/sqlite/SecSQLiteDatabase;->endTransaction()V

    .line 1081
    .end local v4    # "result":J
    .end local v6    # "stmt":Lsamsung/database/sqlite/SecSQLiteStatement;
    :goto_1
    return-wide v4

    .line 1067
    .restart local v6    # "stmt":Lsamsung/database/sqlite/SecSQLiteStatement;
    :cond_0
    :try_start_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1068
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1069
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1070
    .local v2, "i":I
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-static {v6, v2, v8}, Lsamsung/database/SecDatabaseUtils;->bindObjectToProgram(Lsamsung/database/sqlite/SecSQLiteProgram;ILjava/lang/Object;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1079
    .end local v1    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "i":I
    .end local v3    # "key":Ljava/lang/String;
    .end local v6    # "stmt":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catch_0
    move-exception v0

    .line 1080
    .local v0, "e":Landroid/database/SQLException;
    :try_start_2
    const-string v7, "SecDatabaseUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Error inserting "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " into table  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mTableName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1083
    iget-object v7, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v7}, Lsamsung/database/sqlite/SecSQLiteDatabase;->endTransaction()V

    .line 1081
    const-wide/16 v4, -0x1

    goto :goto_1

    .line 1082
    .end local v0    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v7

    .line 1083
    iget-object v8, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mDb:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v8}, Lsamsung/database/sqlite/SecSQLiteDatabase;->endTransaction()V

    .line 1084
    throw v7
.end method


# virtual methods
.method public bind(ID)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # D

    .prologue
    .line 1109
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindDouble(ID)V

    .line 1110
    return-void
.end method

.method public bind(IF)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "value"    # F

    .prologue
    .line 1119
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    float-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindDouble(ID)V

    .line 1120
    return-void
.end method

.method public bind(II)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "value"    # I

    .prologue
    .line 1139
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    int-to-long v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindLong(IJ)V

    .line 1140
    return-void
.end method

.method public bind(IJ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 1129
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindLong(IJ)V

    .line 1130
    return-void
.end method

.method public bind(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1182
    if-nez p2, :cond_0

    .line 1183
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindNull(I)V

    .line 1187
    :goto_0
    return-void

    .line 1185
    :cond_0
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public bind(IZ)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "value"    # Z

    .prologue
    .line 1149
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-long v2, v0

    invoke-virtual {v1, p1, v2, v3}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindLong(IJ)V

    .line 1150
    return-void

    .line 1149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bind(I[B)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # [B

    .prologue
    .line 1168
    if-nez p2, :cond_0

    .line 1169
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindNull(I)V

    .line 1173
    :goto_0
    return-void

    .line 1171
    :cond_0
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindBlob(I[B)V

    goto :goto_0
.end method

.method public bindNull(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1158
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteStatement;->bindNull(I)V

    .line 1159
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1284
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    if-eqz v0, :cond_0

    .line 1285
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V

    .line 1286
    iput-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1288
    :cond_0
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mReplaceStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    if-eqz v0, :cond_1

    .line 1289
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mReplaceStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V

    .line 1290
    iput-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mReplaceStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1292
    :cond_1
    iput-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mInsertSQL:Ljava/lang/String;

    .line 1293
    iput-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mColumns:Ljava/util/HashMap;

    .line 1294
    return-void
.end method

.method public execute()J
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1215
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    if-nez v1, :cond_0

    .line 1216
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "you must prepare this inserter before calling execute"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1221
    :cond_0
    :try_start_0
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v1}, Lsamsung/database/sqlite/SecSQLiteStatement;->executeInsert()J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    .line 1227
    iput-object v4, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1224
    :goto_0
    return-wide v1

    .line 1222
    :catch_0
    move-exception v0

    .line 1223
    .local v0, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v1, "SecDatabaseUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing InsertHelper with table "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mTableName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1227
    iput-object v4, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1224
    const-wide/16 v1, -0x1

    goto :goto_0

    .line 1225
    .end local v0    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    .line 1227
    iput-object v4, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1228
    throw v1
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1094
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->getStatement(Z)Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1095
    iget-object v1, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mColumns:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1096
    .local v0, "index":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 1097
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "column \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' is invalid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1099
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1
.end method

.method public insert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1201
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->insertInternal(Landroid/content/ContentValues;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public prepareForInsert()V
    .locals 1

    .prologue
    .line 1243
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->getStatement(Z)Lsamsung/database/sqlite/SecSQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1244
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->clearBindings()V

    .line 1245
    return-void
.end method

.method public prepareForReplace()V
    .locals 1

    .prologue
    .line 1259
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->getStatement(Z)Lsamsung/database/sqlite/SecSQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    .line 1260
    iget-object v0, p0, Lsamsung/database/SecDatabaseUtils$InsertHelper;->mPreparedStatement:Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->clearBindings()V

    .line 1261
    return-void
.end method

.method public replace(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1275
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lsamsung/database/SecDatabaseUtils$InsertHelper;->insertInternal(Landroid/content/ContentValues;Z)J

    move-result-wide v0

    return-wide v0
.end method
