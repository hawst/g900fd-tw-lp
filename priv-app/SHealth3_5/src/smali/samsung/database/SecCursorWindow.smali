.class public Lsamsung/database/SecCursorWindow;
.super Landroid/database/sqlite/SQLiteClosable;
.source "SecCursorWindow.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lsamsung/database/SecCursorWindow;",
            ">;"
        }
    .end annotation
.end field

.field private static final STATS_TAG:Ljava/lang/String; = "CursorWindowStats"

.field private static final sCursorWindowSize:I = 0x200000

.field private static final sWindowToPidMap:Landroid/util/SparseIntArray;


# instance fields
.field private final mName:Ljava/lang/String;

.field private mStartPos:I

.field public mWindowPtr:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 670
    new-instance v0, Lsamsung/database/SecCursorWindow$1;

    invoke-direct {v0}, Lsamsung/database/SecCursorWindow$1;-><init>()V

    .line 669
    sput-object v0, Lsamsung/database/SecCursorWindow;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 707
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    .line 40
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 127
    invoke-direct {p0}, Landroid/database/sqlite/SQLiteClosable;-><init>()V

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    .line 129
    invoke-static {p1}, Lsamsung/database/SecCursorWindow;->nativeCreateFromParcel(Landroid/os/Parcel;)I

    move-result v0

    iput v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    .line 130
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lsamsung/database/SecCursorWindowAllocationException;

    const-string v1, "Cursor window could not be created from binder."

    invoke-direct {v0, v1}, Lsamsung/database/SecCursorWindowAllocationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v0}, Lsamsung/database/SecCursorWindow;->nativeGetName(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/SecCursorWindow;->mName:Ljava/lang/String;

    .line 135
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lsamsung/database/SecCursorWindow;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lsamsung/database/SecCursorWindow;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 95
    const/high16 v0, 0x200000

    invoke-direct {p0, p1, v0}, Lsamsung/database/SecCursorWindow;-><init>(Ljava/lang/String;I)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "windowSize"    # I

    .prologue
    .line 98
    invoke-direct {p0}, Landroid/database/sqlite/SQLiteClosable;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    .line 100
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .end local p1    # "name":Ljava/lang/String;
    :goto_0
    iput-object p1, p0, Lsamsung/database/SecCursorWindow;->mName:Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lsamsung/database/SecCursorWindow;->mName:Ljava/lang/String;

    invoke-static {v0, p2}, Lsamsung/database/SecCursorWindow;->nativeCreate(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    .line 102
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    if-nez v0, :cond_1

    .line 103
    new-instance v0, Lsamsung/database/SecCursorWindowAllocationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cursor window allocation of 2048 kb failed. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 104
    invoke-direct {p0}, Lsamsung/database/SecCursorWindow;->printStats()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-direct {v0, v1}, Lsamsung/database/SecCursorWindowAllocationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    .restart local p1    # "name":Ljava/lang/String;
    :cond_0
    const-string p1, "<unnamed>"

    goto :goto_0

    .line 106
    .end local p1    # "name":Ljava/lang/String;
    :cond_1
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-direct {p0, v0, v1}, Lsamsung/database/SecCursorWindow;->recordNewWindow(II)V

    .line 107
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "localWindow"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lsamsung/database/SecCursorWindow;-><init>(Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method private dispose()V
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    if-eqz v0, :cond_0

    .line 148
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-direct {p0, v0}, Lsamsung/database/SecCursorWindow;->recordClosingOfWindow(I)V

    .line 149
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v0}, Lsamsung/database/SecCursorWindow;->nativeDispose(I)V

    .line 150
    const/4 v0, 0x0

    iput v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    .line 152
    :cond_0
    return-void
.end method

.method private static native nativeAllocRow(I)Z
.end method

.method private static native nativeClear(I)V
.end method

.method private static native nativeCopyStringToBuffer(IIILandroid/database/CharArrayBuffer;)V
.end method

.method private static native nativeCreate(Ljava/lang/String;I)I
.end method

.method private static native nativeCreateFromParcel(Landroid/os/Parcel;)I
.end method

.method private static native nativeDispose(I)V
.end method

.method private static native nativeFreeLastRow(I)V
.end method

.method private static native nativeGetBlob(III)[B
.end method

.method private static native nativeGetDouble(III)D
.end method

.method private static native nativeGetLong(III)J
.end method

.method private static native nativeGetName(I)Ljava/lang/String;
.end method

.method private static native nativeGetNumRows(I)I
.end method

.method private static native nativeGetString(III)Ljava/lang/String;
.end method

.method private static native nativeGetType(III)I
.end method

.method private static native nativePutBlob(I[BII)Z
.end method

.method private static native nativePutDouble(IDII)Z
.end method

.method private static native nativePutLong(IJII)Z
.end method

.method private static native nativePutNull(III)Z
.end method

.method private static native nativePutString(ILjava/lang/String;II)Z
.end method

.method private static native nativeSetNumColumns(II)Z
.end method

.method private static native nativeWriteToParcel(ILandroid/os/Parcel;)V
.end method

.method public static newFromParcel(Landroid/os/Parcel;)Lsamsung/database/SecCursorWindow;
    .locals 1
    .param p0, "p"    # Landroid/os/Parcel;

    .prologue
    .line 681
    sget-object v0, Lsamsung/database/SecCursorWindow;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/SecCursorWindow;

    return-object v0
.end method

.method private printStats()Ljava/lang/String;
    .locals 15

    .prologue
    const/16 v14, 0x3d4

    .line 729
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 730
    .local v0, "buff":Ljava/lang/StringBuilder;
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    .line 731
    .local v3, "myPid":I
    const/4 v10, 0x0

    .line 732
    .local v10, "total":I
    new-instance v7, Landroid/util/SparseIntArray;

    invoke-direct {v7}, Landroid/util/SparseIntArray;-><init>()V

    .line 733
    .local v7, "pidCounts":Landroid/util/SparseIntArray;
    sget-object v13, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    monitor-enter v13

    .line 734
    :try_start_0
    sget-object v12, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    invoke-virtual {v12}, Landroid/util/SparseIntArray;->size()I

    move-result v9

    .line 735
    .local v9, "size":I
    if-nez v9, :cond_0

    .line 737
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v12, ""

    .line 760
    :goto_0
    return-object v12

    .line 739
    :cond_0
    const/4 v2, 0x0

    .local v2, "indx":I
    :goto_1
    if-lt v2, v9, :cond_1

    .line 733
    :try_start_1
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 745
    invoke-virtual {v7}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    .line 746
    .local v5, "numPids":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-lt v1, v5, :cond_2

    .line 759
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-le v12, v14, :cond_4

    const/4 v12, 0x0

    invoke-virtual {v0, v12, v14}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 760
    .local v8, "s":Ljava/lang/String;
    :goto_3
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "# Open Cursors="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto :goto_0

    .line 740
    .end local v1    # "i":I
    .end local v5    # "numPids":I
    .end local v8    # "s":Ljava/lang/String;
    :cond_1
    :try_start_2
    sget-object v12, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    invoke-virtual {v12, v2}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v6

    .line 741
    .local v6, "pid":I
    invoke-virtual {v7, v6}, Landroid/util/SparseIntArray;->get(I)I

    move-result v11

    .line 742
    .local v11, "value":I
    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v7, v6, v11}, Landroid/util/SparseIntArray;->put(II)V

    .line 739
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 733
    .end local v2    # "indx":I
    .end local v6    # "pid":I
    .end local v9    # "size":I
    .end local v11    # "value":I
    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v12

    .line 747
    .restart local v1    # "i":I
    .restart local v2    # "indx":I
    .restart local v5    # "numPids":I
    .restart local v9    # "size":I
    :cond_2
    const-string v12, " (# cursors opened by "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 748
    invoke-virtual {v7, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v6

    .line 749
    .restart local v6    # "pid":I
    if-ne v6, v3, :cond_3

    .line 750
    const-string/jumbo v12, "this proc="

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 754
    :goto_4
    invoke-virtual {v7, v6}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    .line 755
    .local v4, "num":I
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 756
    add-int/2addr v10, v4

    .line 746
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 752
    .end local v4    # "num":I
    :cond_3
    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "pid "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 759
    .end local v6    # "pid":I
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_3
.end method

.method private recordClosingOfWindow(I)V
    .locals 2
    .param p1, "window"    # I

    .prologue
    .line 719
    sget-object v1, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    monitor-enter v1

    .line 720
    :try_start_0
    sget-object v0, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 722
    monitor-exit v1

    .line 726
    :goto_0
    return-void

    .line 724
    :cond_0
    sget-object v0, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->delete(I)V

    .line 719
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private recordNewWindow(II)V
    .locals 4
    .param p1, "pid"    # I
    .param p2, "window"    # I

    .prologue
    .line 710
    sget-object v1, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    monitor-enter v1

    .line 711
    :try_start_0
    sget-object v0, Lsamsung/database/SecCursorWindow;->sWindowToPidMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p2, p1}, Landroid/util/SparseIntArray;->put(II)V

    .line 712
    const-string v0, "CursorWindowStats"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 713
    const-string v0, "CursorWindowStats"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Created a new Cursor. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lsamsung/database/SecCursorWindow;->printStats()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    :cond_0
    monitor-exit v1

    .line 716
    return-void

    .line 710
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public allocRow()Z
    .locals 1

    .prologue
    .line 246
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 248
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v0}, Lsamsung/database/SecCursorWindow;->nativeAllocRow(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 250
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 248
    return v0

    .line 249
    :catchall_0
    move-exception v0

    .line 250
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 251
    throw v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 173
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    .line 174
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v0}, Lsamsung/database/SecCursorWindow;->nativeClear(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 178
    return-void

    .line 175
    :catchall_0
    move-exception v0

    .line 176
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 177
    throw v0
.end method

.method public copyStringToBuffer(IILandroid/database/CharArrayBuffer;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "buffer"    # Landroid/database/CharArrayBuffer;

    .prologue
    .line 462
    if-nez p3, :cond_0

    .line 463
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CharArrayBuffer should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 465
    :cond_0
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 467
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p1, v1

    invoke-static {v0, v1, p2, p3}, Lsamsung/database/SecCursorWindow;->nativeCopyStringToBuffer(IIILandroid/database/CharArrayBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 471
    return-void

    .line 468
    :catchall_0
    move-exception v0

    .line 469
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 470
    throw v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 685
    const/4 v0, 0x0

    return v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 140
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/SecCursorWindow;->dispose()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 144
    return-void

    .line 141
    :catchall_0
    move-exception v0

    .line 142
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 143
    throw v0
.end method

.method public freeLastRow()V
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 260
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v0}, Lsamsung/database/SecCursorWindow;->nativeFreeLastRow(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 264
    return-void

    .line 261
    :catchall_0
    move-exception v0

    .line 262
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 263
    throw v0
.end method

.method public getBlob(II)[B
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 388
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 390
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p1, v1

    invoke-static {v0, v1, p2}, Lsamsung/database/SecCursorWindow;->nativeGetBlob(III)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 392
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 390
    return-object v0

    .line 391
    :catchall_0
    move-exception v0

    .line 392
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 393
    throw v0
.end method

.method public getDouble(II)D
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 528
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 530
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p1, v1

    invoke-static {v0, v1, p2}, Lsamsung/database/SecCursorWindow;->nativeGetDouble(III)D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 532
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 530
    return-wide v0

    .line 531
    :catchall_0
    move-exception v0

    .line 532
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 533
    throw v0
.end method

.method public getFloat(II)F
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 581
    invoke-virtual {p0, p1, p2}, Lsamsung/database/SecCursorWindow;->getDouble(II)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public getInt(II)I
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 565
    invoke-virtual {p0, p1, p2}, Lsamsung/database/SecCursorWindow;->getLong(II)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getLong(II)J
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 496
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 498
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p1, v1

    invoke-static {v0, v1, p2}, Lsamsung/database/SecCursorWindow;->nativeGetLong(III)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 500
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 498
    return-wide v0

    .line 499
    :catchall_0
    move-exception v0

    .line 500
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 501
    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lsamsung/database/SecCursorWindow;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNumRows()I
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 214
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v0}, Lsamsung/database/SecCursorWindow;->nativeGetNumRows(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 216
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 214
    return v0

    .line 215
    :catchall_0
    move-exception v0

    .line 216
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 217
    throw v0
.end method

.method public getShort(II)S
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 549
    invoke-virtual {p0, p1, p2}, Lsamsung/database/SecCursorWindow;->getLong(II)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-short v0, v0

    return v0
.end method

.method public getStartPosition()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    return v0
.end method

.method public getString(II)Ljava/lang/String;
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 423
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 425
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p1, v1

    invoke-static {v0, v1, p2}, Lsamsung/database/SecCursorWindow;->nativeGetString(III)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 427
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 425
    return-object v0

    .line 426
    :catchall_0
    move-exception v0

    .line 427
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 428
    throw v0
.end method

.method public getType(II)I
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 358
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 360
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p1, v1

    invoke-static {v0, v1, p2}, Lsamsung/database/SecCursorWindow;->nativeGetType(III)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 362
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 360
    return v0

    .line 361
    :catchall_0
    move-exception v0

    .line 362
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 363
    throw v0
.end method

.method public isBlob(II)Z
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 292
    invoke-virtual {p0, p1, p2}, Lsamsung/database/SecCursorWindow;->getType(II)I

    move-result v0

    .line 293
    .local v0, "type":I
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isFloat(II)Z
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 321
    invoke-virtual {p0, p1, p2}, Lsamsung/database/SecCursorWindow;->getType(II)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLong(II)Z
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 307
    invoke-virtual {p0, p1, p2}, Lsamsung/database/SecCursorWindow;->getType(II)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNull(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 277
    invoke-virtual {p0, p1, p2}, Lsamsung/database/SecCursorWindow;->getType(II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isString(II)Z
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 336
    invoke-virtual {p0, p1, p2}, Lsamsung/database/SecCursorWindow;->getType(II)I

    move-result v0

    .line 337
    .local v0, "type":I
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onAllReferencesReleased()V
    .locals 0

    .prologue
    .line 704
    invoke-direct {p0}, Lsamsung/database/SecCursorWindow;->dispose()V

    .line 705
    return-void
.end method

.method public putBlob([BII)Z
    .locals 2
    .param p1, "value"    # [B
    .param p2, "row"    # I
    .param p3, "column"    # I

    .prologue
    .line 593
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 595
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p2, v1

    invoke-static {v0, p1, v1, p3}, Lsamsung/database/SecCursorWindow;->nativePutBlob(I[BII)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 597
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 595
    return v0

    .line 596
    :catchall_0
    move-exception v0

    .line 597
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 598
    throw v0
.end method

.method public putDouble(DII)Z
    .locals 2
    .param p1, "value"    # D
    .param p3, "row"    # I
    .param p4, "column"    # I

    .prologue
    .line 645
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 647
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p3, v1

    invoke-static {v0, p1, p2, v1, p4}, Lsamsung/database/SecCursorWindow;->nativePutDouble(IDII)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 649
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 647
    return v0

    .line 648
    :catchall_0
    move-exception v0

    .line 649
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 650
    throw v0
.end method

.method public putLong(JII)Z
    .locals 2
    .param p1, "value"    # J
    .param p3, "row"    # I
    .param p4, "column"    # I

    .prologue
    .line 627
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 629
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p3, v1

    invoke-static {v0, p1, p2, v1, p4}, Lsamsung/database/SecCursorWindow;->nativePutLong(IJII)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 631
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 629
    return v0

    .line 630
    :catchall_0
    move-exception v0

    .line 631
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 632
    throw v0
.end method

.method public putNull(II)Z
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 661
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 663
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p1, v1

    invoke-static {v0, v1, p2}, Lsamsung/database/SecCursorWindow;->nativePutNull(III)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 665
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 663
    return v0

    .line 664
    :catchall_0
    move-exception v0

    .line 665
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 666
    throw v0
.end method

.method public putString(Ljava/lang/String;II)Z
    .locals 2
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "row"    # I
    .param p3, "column"    # I

    .prologue
    .line 610
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 612
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    sub-int v1, p2, v1

    invoke-static {v0, p1, v1, p3}, Lsamsung/database/SecCursorWindow;->nativePutString(ILjava/lang/String;II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 614
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 612
    return v0

    .line 613
    :catchall_0
    move-exception v0

    .line 614
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 615
    throw v0
.end method

.method public setNumColumns(I)Z
    .locals 1
    .param p1, "columnNum"    # I

    .prologue
    .line 232
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 234
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v0, p1}, Lsamsung/database/SecCursorWindow;->nativeSetNumColumns(II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 236
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 234
    return v0

    .line 235
    :catchall_0
    move-exception v0

    .line 236
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 237
    throw v0
.end method

.method public setStartPosition(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 203
    iput p1, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    .line 204
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 765
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 689
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 691
    :try_start_0
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mStartPos:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 692
    iget v0, p0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    invoke-static {v0, p1}, Lsamsung/database/SecCursorWindow;->nativeWriteToParcel(ILandroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 694
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 697
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 698
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 700
    :cond_0
    return-void

    .line 693
    :catchall_0
    move-exception v0

    .line 694
    invoke-virtual {p0}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 695
    throw v0
.end method
