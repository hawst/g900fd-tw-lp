.class public final Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;
.super Ljava/lang/Object;
.source "SecSQLiteDirectCursorDriver.java"

# interfaces
.implements Lsamsung/database/sqlite/SecSQLiteCursorDriver;


# instance fields
.field private final mCancellationSignal:Landroid/os/CancellationSignal;

.field private final mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

.field private final mEditTable:Ljava/lang/String;

.field private mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

.field private final mSql:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)V
    .locals 0
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "sql"    # Ljava/lang/String;
    .param p3, "editTable"    # Ljava/lang/String;
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 38
    iput-object p3, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mEditTable:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mSql:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 41
    return-void
.end method


# virtual methods
.method public cursorClosed()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public cursorDeactivated()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public cursorRequeried(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 77
    return-void
.end method

.method public query(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 44
    new-instance v2, Lsamsung/database/sqlite/SecSQLiteQuery;

    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mSql:Ljava/lang/String;

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-direct {v2, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteQuery;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Landroid/os/CancellationSignal;)V

    .line 47
    .local v2, "query":Lsamsung/database/sqlite/SecSQLiteQuery;
    :try_start_0
    invoke-virtual {v2, p2}, Lsamsung/database/sqlite/SecSQLiteQuery;->bindAllArgsAsStrings([Ljava/lang/String;)V

    .line 49
    if-nez p1, :cond_0

    .line 50
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteCursor;

    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mEditTable:Ljava/lang/String;

    invoke-direct {v0, p0, v3, v2}, Lsamsung/database/sqlite/SecSQLiteCursor;-><init>(Lsamsung/database/sqlite/SecSQLiteCursorDriver;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteQuery;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    .local v0, "cursor":Landroid/database/Cursor;
    :goto_0
    iput-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

    .line 60
    return-object v0

    .line 52
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_0
    :try_start_1
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mEditTable:Ljava/lang/String;

    invoke-interface {p1, v3, p0, v4, v2}, Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;->newCursor(Lsamsung/database/sqlite/SecSQLiteDatabase;Lsamsung/database/sqlite/SecSQLiteCursorDriver;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteQuery;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .restart local v0    # "cursor":Landroid/database/Cursor;
    goto :goto_0

    .line 54
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v1

    .line 55
    .local v1, "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v2}, Lsamsung/database/sqlite/SecSQLiteQuery;->close()V

    .line 56
    throw v1
.end method

.method public setBindArguments([Ljava/lang/String;)V
    .locals 1
    .param p1, "bindArgs"    # [Ljava/lang/String;

    .prologue
    .line 68
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteQuery;->bindAllArgsAsStrings([Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SecSQLiteDirectCursorDriver: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;->mSql:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
