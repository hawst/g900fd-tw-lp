.class public final Lsamsung/database/sqlite/SecSQLiteDatabase;
.super Lsamsung/database/sqlite/SecSQLiteClosable;
.source "SecSQLiteDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;,
        Lsamsung/database/sqlite/SecSQLiteDatabase$CustomFunction;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CONFLICT_ABORT:I = 0x2

.field public static final CONFLICT_FAIL:I = 0x3

.field public static final CONFLICT_IGNORE:I = 0x4

.field public static final CONFLICT_NONE:I = 0x0

.field public static final CONFLICT_REPLACE:I = 0x5

.field public static final CONFLICT_ROLLBACK:I = 0x1

.field private static final CONFLICT_VALUES:[Ljava/lang/String;

.field public static final CREATE_IF_NECESSARY:I = 0x10000000

.field public static final ENABLE_WRITE_AHEAD_LOGGING:I = 0x20000000

.field private static final EVENT_DB_CORRUPT:I = 0x124fc

.field public static final MAX_SQL_CACHE_SIZE:I = 0x64

.field public static final NO_LOCALIZED_COLLATORS:I = 0x10

.field public static final OPEN_READONLY:I = 0x1

.field public static final OPEN_READWRITE:I = 0x0

.field private static final OPEN_READ_MASK:I = 0x1

.field public static final SQLITE_MAX_LIKE_PATTERN_LENGTH:I = 0xc350

.field private static final TAG:Ljava/lang/String; = "SecSQLiteDatabase"

.field private static sActiveDatabases:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteDatabase;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

.field private mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

.field private mCorrupt_code:I

.field private final mCursorFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

.field private final mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

.field private mHasAttachedDbsLocked:Z

.field private final mLock:Ljava/lang/Object;

.field private final mThreadSession:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteSession;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    const-class v0, Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lsamsung/database/sqlite/SecSQLiteDatabase;->$assertionsDisabled:Z

    .line 74
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 73
    sput-object v0, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    .line 178
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v0, v2

    const-string v2, " OR ROLLBACK "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, " OR ABORT "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, " OR FAIL "

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, " OR IGNORE "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, " OR REPLACE "

    aput-object v2, v0, v1

    .line 177
    sput-object v0, Lsamsung/database/sqlite/SecSQLiteDatabase;->CONFLICT_VALUES:[Ljava/lang/String;

    .line 2025
    const-string v0, "SecSQLiteDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Android Version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2026
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "4.1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    .line 2027
    const-string v0, "SecSQLiteDatabase"

    const-string v1, "Android Verion is too low!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2028
    const-string/jumbo v0, "secsqlite4_1"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 2034
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 65
    goto :goto_0

    .line 2029
    :cond_1
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "4.1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_2

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "4.2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_2

    .line 2030
    const-string v0, "SecSQLiteDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Load library secsqlite4_1.so  for Android "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2031
    const-string/jumbo v0, "secsqlite4_1"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_1

    .line 2033
    :cond_2
    const-string v0, "SecSQLiteDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Load library secsqlite.so for Android "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2034
    const-string/jumbo v0, "secsqlite"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;ILsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Lsamsung/database/SecDatabaseErrorHandler;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "openFlags"    # I
    .param p3, "cursorFactory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p4, "errorHandler"    # Lsamsung/database/SecDatabaseErrorHandler;

    .prologue
    .line 248
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteClosable;-><init>()V

    .line 79
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDatabase$1;

    invoke-direct {v0, p0}, Lsamsung/database/sqlite/SecSQLiteDatabase$1;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mThreadSession:Ljava/lang/ThreadLocal;

    .line 108
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    .line 1780
    const/4 v0, 0x0

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mCorrupt_code:I

    .line 250
    iput-object p3, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mCursorFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .line 251
    if-eqz p4, :cond_0

    .end local p4    # "errorHandler":Lsamsung/database/SecDatabaseErrorHandler;
    :goto_0
    iput-object p4, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    .line 252
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-direct {v0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .line 253
    return-void

    .line 251
    .restart local p4    # "errorHandler":Lsamsung/database/SecDatabaseErrorHandler;
    :cond_0
    new-instance p4, Lsamsung/database/SecDefaultDatabaseErrorHandler;

    .end local p4    # "errorHandler":Lsamsung/database/SecDatabaseErrorHandler;
    invoke-direct {p4}, Lsamsung/database/SecDefaultDatabaseErrorHandler;-><init>()V

    goto :goto_0
.end method

.method public static backupDatabase(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "srcPath"    # Ljava/lang/String;
    .param p1, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 1776
    invoke-static {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->native_backup(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private beginTransaction(Lsamsung/database/sqlite/SecSQLiteTransactionListener;Z)V
    .locals 4
    .param p1, "transactionListener"    # Lsamsung/database/sqlite/SecSQLiteTransactionListener;
    .param p2, "exclusive"    # Z

    .prologue
    .line 477
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 479
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v1

    .line 480
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    .line 483
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadDefaultConnectionFlags(Z)I

    move-result v2

    const/4 v3, 0x0

    .line 479
    invoke-virtual {v1, v0, p1, v2, v3}, Lsamsung/database/sqlite/SecSQLiteSession;->beginTransaction(ILsamsung/database/sqlite/SecSQLiteTransactionListener;ILandroid/os/CancellationSignal;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 487
    return-void

    .line 481
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 484
    :catchall_0
    move-exception v0

    .line 485
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 486
    throw v0
.end method

.method private collectDbStats(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1811
    .local p1, "dbStatsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;>;"
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1812
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    if-eqz v0, :cond_0

    .line 1813
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->collectDbStats(Ljava/util/ArrayList;)V

    .line 1811
    :cond_0
    monitor-exit v1

    .line 1816
    return-void

    .line 1811
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static convert2PlainDB(Ljava/io/File;[BLjava/io/File;)V
    .locals 5
    .param p0, "sourceDbFile"    # Ljava/io/File;
    .param p1, "password"    # [B
    .param p2, "destDbFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 684
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    new-instance v4, Lsamsung/database/SecDefaultDatabaseErrorHandler;

    invoke-direct {v4}, Lsamsung/database/SecDefaultDatabaseErrorHandler;-><init>()V

    invoke-static {v1, v2, v3, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    .line 685
    .local v0, "destDb":Lsamsung/database/sqlite/SecSQLiteDatabase;
    const-string v1, "attach database \'%s\' as secureDb key \'%s\'"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([B)V

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 686
    const-string/jumbo v1, "secureDb"

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->exportDB(Ljava/lang/String;)V

    .line 687
    const-string v1, "detach database secureDb"

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 688
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    .line 689
    return-void
.end method

.method public static convert2SecureDB(Ljava/io/File;Ljava/io/File;[B)V
    .locals 5
    .param p0, "sourceDbFile"    # Ljava/io/File;
    .param p1, "destDbFile"    # Ljava/io/File;
    .param p2, "password"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 692
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    new-instance v4, Lsamsung/database/SecDefaultDatabaseErrorHandler;

    invoke-direct {v4}, Lsamsung/database/SecDefaultDatabaseErrorHandler;-><init>()V

    invoke-static {v1, v2, v3, v4, p2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openSecureDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;[B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    .line 693
    .local v0, "destDb":Lsamsung/database/sqlite/SecSQLiteDatabase;
    const-string v1, "attach database \'%s\' as plainDb key \'\'"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 694
    const-string/jumbo v1, "plainDb"

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->exportDB(Ljava/lang/String;)V

    .line 695
    const-string v1, "detach database plainDb"

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 696
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    .line 697
    return-void
.end method

.method public static create(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 2
    .param p0, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .prologue
    .line 850
    const-string v0, ":memory:"

    .line 851
    const/high16 v1, 0x10000000

    .line 850
    invoke-static {v0, p0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;I)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static createSecureDatabase(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;[B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 3
    .param p0, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p1, "password"    # [B

    .prologue
    .line 864
    const-string v0, ":memory:"

    .line 866
    const/high16 v1, 0x10000000

    new-instance v2, Lsamsung/database/SecDefaultDatabaseErrorHandler;

    invoke-direct {v2}, Lsamsung/database/SecDefaultDatabaseErrorHandler;-><init>()V

    .line 864
    invoke-static {v0, p0, v1, v2, p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openSecureDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;[B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static deleteDatabase(Ljava/io/File;)Z
    .locals 9
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 717
    if-nez p0, :cond_0

    .line 718
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "file must not be null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 721
    :cond_0
    const/4 v0, 0x0

    .line 722
    .local v0, "deleted":Z
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v5

    or-int/2addr v0, v5

    .line 723
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "-journal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    or-int/2addr v0, v5

    .line 724
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "-shm"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    or-int/2addr v0, v5

    .line 725
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "-wal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    or-int/2addr v0, v5

    .line 727
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 728
    .local v1, "dir":Ljava/io/File;
    if-eqz v1, :cond_1

    .line 729
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "-mj"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 730
    .local v4, "prefix":Ljava/lang/String;
    new-instance v2, Lsamsung/database/sqlite/SecSQLiteDatabase$2;

    invoke-direct {v2, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase$2;-><init>(Ljava/lang/String;)V

    .line 736
    .local v2, "filter":Ljava/io/FileFilter;
    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v6

    array-length v7, v6

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v7, :cond_2

    .line 740
    .end local v2    # "filter":Ljava/io/FileFilter;
    .end local v4    # "prefix":Ljava/lang/String;
    :cond_1
    return v0

    .line 736
    .restart local v2    # "filter":Ljava/io/FileFilter;
    .restart local v4    # "prefix":Ljava/lang/String;
    :cond_2
    aget-object v3, v6, v5

    .line 737
    .local v3, "masterJournal":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v8

    or-int/2addr v0, v8

    .line 736
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public static deleteDatabaseFile(Ljava/lang/String;)Z
    .locals 4
    .param p0, "file"    # Ljava/lang/String;

    .prologue
    .line 2004
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2005
    .local v0, "fi":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 2007
    .local v1, "ret":Z
    if-eqz v1, :cond_2

    .line 2008
    new-instance v0, Ljava/io/File;

    .end local v0    # "fi":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "-journal"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2009
    .restart local v0    # "fi":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2010
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2012
    :cond_0
    new-instance v0, Ljava/io/File;

    .end local v0    # "fi":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "-wal"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2013
    .restart local v0    # "fi":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2014
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2016
    :cond_1
    new-instance v0, Ljava/io/File;

    .end local v0    # "fi":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "-shm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2017
    .restart local v0    # "fi":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2018
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2021
    :cond_2
    return v1
.end method

.method private dispose(Z)V
    .locals 3
    .param p1, "finalized"    # Z

    .prologue
    .line 271
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 272
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    .line 273
    .local v0, "pool":Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    const/4 v1, 0x0

    iput-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    .line 271
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    if-nez p1, :cond_0

    .line 277
    sget-object v2, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    monitor-enter v2

    .line 278
    :try_start_1
    sget-object v1, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 281
    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->close()V

    .line 285
    :cond_0
    return-void

    .line 271
    .end local v0    # "pool":Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 277
    .restart local v0    # "pool":Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method private dump(Landroid/util/Printer;Z)V
    .locals 2
    .param p1, "printer"    # Landroid/util/Printer;
    .param p2, "verbose"    # Z

    .prologue
    .line 1837
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1838
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    if-eqz v0, :cond_0

    .line 1839
    const-string v0, ""

    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 1840
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    invoke-virtual {v0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->dump(Landroid/util/Printer;Z)V

    .line 1837
    :cond_0
    monitor-exit v1

    .line 1843
    return-void

    .line 1837
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static dumpAll(Landroid/util/Printer;Z)V
    .locals 3
    .param p0, "printer"    # Landroid/util/Printer;
    .param p1, "verbose"    # Z

    .prologue
    .line 1831
    invoke-static {}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getActiveDatabases()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1834
    return-void

    .line 1831
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 1832
    .local v0, "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    invoke-direct {v0, p0, p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->dump(Landroid/util/Printer;Z)V

    goto :goto_0
.end method

.method private executeSql(Ljava/lang/String;[Ljava/lang/Object;)I
    .locals 3
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1660
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 1662
    :try_start_0
    invoke-static {p1}, Lsamsung/database/SecDatabaseUtils;->getSqlStatementType(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 1663
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1664
    :try_start_1
    iget-boolean v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mHasAttachedDbsLocked:Z

    if-nez v1, :cond_0

    .line 1665
    const/4 v1, 0x1

    iput-boolean v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mHasAttachedDbsLocked:Z

    .line 1663
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1670
    :cond_1
    :try_start_2
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-direct {v0, p0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteStatement;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1672
    .local v0, "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :try_start_3
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->executeUpdateDelete()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v1

    .line 1674
    :try_start_4
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1677
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1672
    return v1

    .line 1663
    .end local v0    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catchall_0
    move-exception v1

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1676
    :catchall_1
    move-exception v1

    .line 1677
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1678
    throw v1

    .line 1673
    .restart local v0    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catchall_2
    move-exception v1

    .line 1674
    :try_start_7
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V

    .line 1675
    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1
.end method

.method private exportDB(Ljava/lang/String;)V
    .locals 2
    .param p1, "attachedDB"    # Ljava/lang/String;

    .prologue
    .line 830
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 831
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->throwIfNotOpenLocked()V

    .line 833
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->exportDB(Ljava/lang/String;)V

    .line 830
    monitor-exit v1

    .line 835
    return-void

    .line 830
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static findEditTable(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "tables"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 970
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 972
    const/16 v2, 0x20

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 973
    .local v1, "spacepos":I
    const/16 v2, 0x2c

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 975
    .local v0, "commapos":I
    if-lez v1, :cond_2

    if-lt v1, v0, :cond_0

    if-gez v0, :cond_2

    .line 976
    :cond_0
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 980
    .end local p0    # "tables":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p0

    .line 977
    .restart local p0    # "tables":Ljava/lang/String;
    :cond_2
    if-lez v0, :cond_1

    if-lt v0, v1, :cond_3

    if-gez v1, :cond_1

    .line 978
    :cond_3
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 982
    .end local v0    # "commapos":I
    .end local v1    # "spacepos":I
    :cond_4
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Invalid tables"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static getActiveDatabases()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteDatabase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1819
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1820
    .local v0, "databases":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteDatabase;>;"
    sget-object v2, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    monitor-enter v2

    .line 1821
    :try_start_0
    sget-object v1, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1820
    monitor-exit v2

    .line 1823
    return-object v0

    .line 1820
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method static getDbStats()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1803
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1804
    .local v1, "dbStatsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;>;"
    invoke-static {}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getActiveDatabases()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1807
    return-object v1

    .line 1804
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 1805
    .local v0, "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->collectDbStats(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private static isMainThread()Z
    .locals 2

    .prologue
    .line 364
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 365
    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isReadOnlyLocked()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1693
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v1, v1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private open()V
    .locals 4

    .prologue
    .line 778
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openInner()V
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 788
    :goto_0
    return-void

    .line 779
    :catch_0
    move-exception v0

    .line 780
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;->getCorruptCode()I

    move-result v1

    invoke-virtual {p0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->onCorruption(I)V

    .line 781
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openInner()V
    :try_end_1
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 783
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catch_1
    move-exception v0

    .line 784
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteException;
    const-string v1, "SecSQLiteDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to open database \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 785
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    .line 786
    throw v0
.end method

.method public static openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;I)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "flags"    # I

    .prologue
    .line 606
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "flags"    # I
    .param p3, "errorHandler"    # Lsamsung/database/SecDatabaseErrorHandler;

    .prologue
    .line 630
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-direct {v0, p0, p2, p1, p3}, Lsamsung/database/sqlite/SecSQLiteDatabase;-><init>(Ljava/lang/String;ILsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Lsamsung/database/SecDatabaseErrorHandler;)V

    .line 631
    .local v0, "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    invoke-direct {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->open()V

    .line 632
    return-object v0
.end method

.method private openInner()V
    .locals 3

    .prologue
    .line 808
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 809
    :try_start_0
    sget-boolean v0, Lsamsung/database/sqlite/SecSQLiteDatabase;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 808
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 810
    :cond_0
    :try_start_1
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-static {v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->open(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    .line 808
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 813
    sget-object v1, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 814
    :try_start_2
    sget-object v0, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    monitor-exit v1

    .line 816
    return-void

    .line 813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private openInnerSecureDatabase([B)V
    .locals 3
    .param p1, "password"    # [B

    .prologue
    .line 819
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 820
    :try_start_0
    sget-boolean v0, Lsamsung/database/sqlite/SecSQLiteDatabase;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 819
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 821
    :cond_0
    :try_start_1
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-static {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->openSecure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;[B)Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    .line 819
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 824
    sget-object v1, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 825
    :try_start_2
    sget-object v0, Lsamsung/database/sqlite/SecSQLiteDatabase;->sActiveDatabases:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    monitor-exit v1

    .line 827
    return-void

    .line 824
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static openOrCreateDatabase(Ljava/io/File;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1
    .param p0, "file"    # Ljava/io/File;
    .param p1, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .prologue
    .line 639
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static openOrCreateDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .prologue
    .line 646
    const/high16 v0, 0x10000000

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static openOrCreateDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Lsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "errorHandler"    # Lsamsung/database/SecDatabaseErrorHandler;

    .prologue
    .line 654
    const/high16 v0, 0x10000000

    invoke-static {p0, p1, v0, p2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public static openSecureDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;[B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "flags"    # I
    .param p3, "errorHandler"    # Lsamsung/database/SecDatabaseErrorHandler;
    .param p4, "password"    # [B

    .prologue
    .line 676
    const-string v1, "SecSQLiteDatabase"

    const-string/jumbo v2, "openSecureDatabase..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-direct {v0, p0, p2, p1, p3}, Lsamsung/database/sqlite/SecSQLiteDatabase;-><init>(Ljava/lang/String;ILsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Lsamsung/database/SecDatabaseErrorHandler;)V

    .line 678
    .local v0, "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    invoke-direct {v0, p4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openSecureDatabase([B)V

    .line 679
    const-string v1, "SecSQLiteDatabase"

    const-string v2, "...openSecureDatabase"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    return-object v0
.end method

.method private openSecureDatabase([B)V
    .locals 4
    .param p1, "password"    # [B

    .prologue
    .line 791
    const-string v1, "SecSQLiteDatabase"

    const-string/jumbo v2, "private openSecureDatabase..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openInnerSecureDatabase([B)V
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 804
    :goto_0
    const-string v1, "SecSQLiteDatabase"

    const-string v2, "...private openSecureDatabase"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    return-void

    .line 795
    :catch_0
    move-exception v0

    .line 796
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;->getCorruptCode()I

    move-result v1

    invoke-virtual {p0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->onCorruption(I)V

    .line 797
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openInnerSecureDatabase([B)V
    :try_end_1
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 799
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :catch_1
    move-exception v0

    .line 800
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteException;
    const-string v1, "SecSQLiteDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to open database \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 801
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    .line 802
    throw v0
.end method

.method public static releaseMemory()I
    .locals 1

    .prologue
    .line 294
    invoke-static {}, Lsamsung/database/sqlite/SecSQLiteGlobal;->releaseMemory()I

    move-result v0

    return v0
.end method

.method public static renameDatabaseFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "from"    # Ljava/lang/String;
    .param p1, "to"    # Ljava/lang/String;

    .prologue
    .line 1990
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1991
    .local v0, "fi":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1993
    new-instance v0, Ljava/io/File;

    .end local v0    # "fi":Ljava/io/File;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-wal"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1994
    .restart local v0    # "fi":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1995
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "-wal"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1997
    :cond_0
    new-instance v0, Ljava/io/File;

    .end local v0    # "fi":Ljava/io/File;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-shm"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1998
    .restart local v0    # "fi":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1999
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "-shm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 2000
    :cond_1
    return-void
.end method

.method private throwIfNotOpenLocked()V
    .locals 3

    .prologue
    .line 1957
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    if-nez v0, :cond_0

    .line 1958
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The database \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v2, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1959
    const-string v2, "\' is not open."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1958
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1961
    :cond_0
    return-void
.end method

.method private yieldIfContendedHelper(ZJ)Z
    .locals 2
    .param p1, "throwIfUnsafe"    # Z
    .param p2, "sleepAfterYieldDelay"    # J

    .prologue
    .line 583
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 585
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, p1, v1}, Lsamsung/database/sqlite/SecSQLiteSession;->yieldTransaction(JZLandroid/os/CancellationSignal;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 587
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 585
    return v0

    .line 586
    :catchall_0
    move-exception v0

    .line 587
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 588
    throw v0
.end method


# virtual methods
.method public addCustomFunction(Ljava/lang/String;ILsamsung/database/sqlite/SecSQLiteDatabase$CustomFunction;)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "numArgs"    # I
    .param p3, "function"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CustomFunction;

    .prologue
    .line 881
    new-instance v1, Lsamsung/database/sqlite/SecSQLiteCustomFunction;

    invoke-direct {v1, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteCustomFunction;-><init>(Ljava/lang/String;ILsamsung/database/sqlite/SecSQLiteDatabase$CustomFunction;)V

    .line 883
    .local v1, "wrapper":Lsamsung/database/sqlite/SecSQLiteCustomFunction;
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 884
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->throwIfNotOpenLocked()V

    .line 886
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v2, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 888
    :try_start_1
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v2, v4}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->reconfigure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 883
    :try_start_2
    monitor-exit v3

    .line 894
    return-void

    .line 889
    :catch_0
    move-exception v0

    .line 890
    .local v0, "ex":Ljava/lang/RuntimeException;
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v2, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 891
    throw v0

    .line 883
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public beginTransaction()V
    .locals 2

    .prologue
    .line 390
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->beginTransaction(Lsamsung/database/sqlite/SecSQLiteTransactionListener;Z)V

    .line 391
    return-void
.end method

.method public beginTransactionNonExclusive()V
    .locals 2

    .prologue
    .line 414
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->beginTransaction(Lsamsung/database/sqlite/SecSQLiteTransactionListener;Z)V

    .line 415
    return-void
.end method

.method public beginTransactionWithListener(Lsamsung/database/sqlite/SecSQLiteTransactionListener;)V
    .locals 1
    .param p1, "transactionListener"    # Lsamsung/database/sqlite/SecSQLiteTransactionListener;

    .prologue
    .line 443
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->beginTransaction(Lsamsung/database/sqlite/SecSQLiteTransactionListener;Z)V

    .line 444
    return-void
.end method

.method public beginTransactionWithListenerNonExclusive(Lsamsung/database/sqlite/SecSQLiteTransactionListener;)V
    .locals 1
    .param p1, "transactionListener"    # Lsamsung/database/sqlite/SecSQLiteTransactionListener;

    .prologue
    .line 472
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->beginTransaction(Lsamsung/database/sqlite/SecSQLiteTransactionListener;Z)V

    .line 473
    return-void
.end method

.method public changeDBPassword([B)I
    .locals 2
    .param p1, "newPassword"    # [B

    .prologue
    .line 700
    const-string v0, "SecSQLiteDatabase"

    const-string v1, "changeDBPassword..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 702
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DB is not open"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 704
    :cond_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v0

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteSession;->changePassword([B)V

    .line 705
    const-string v0, "SecSQLiteDatabase"

    const-string v1, "...changeDBPassword"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    const/4 v0, 0x0

    return v0
.end method

.method public compileStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteStatement;
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1001
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 1003
    :try_start_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteStatement;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lsamsung/database/sqlite/SecSQLiteStatement;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1005
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1003
    return-object v0

    .line 1004
    :catchall_0
    move-exception v0

    .line 1005
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1006
    throw v0
.end method

.method createSession()Lsamsung/database/sqlite/SecSQLiteSession;
    .locals 3

    .prologue
    .line 338
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 339
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->throwIfNotOpenLocked()V

    .line 340
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    .line 338
    .local v0, "pool":Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    new-instance v1, Lsamsung/database/sqlite/SecSQLiteSession;

    invoke-direct {v1, v0}, Lsamsung/database/sqlite/SecSQLiteSession;-><init>(Lsamsung/database/sqlite/SecSQLiteConnectionPool;)V

    return-object v1

    .line 338
    .end local v0    # "pool":Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "whereClause"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 1501
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 1503
    :try_start_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteStatement;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DELETE FROM "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1504
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, " WHERE "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1503
    invoke-direct {v0, p0, v1, p3}, Lsamsung/database/sqlite/SecSQLiteStatement;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1506
    .local v0, "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->executeUpdateDelete()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 1508
    :try_start_2
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1511
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1506
    return v1

    .line 1504
    .end local v0    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :cond_0
    :try_start_3
    const-string v1, ""

    goto :goto_0

    .line 1507
    .restart local v0    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catchall_0
    move-exception v1

    .line 1508
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V

    .line 1509
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1510
    .end local v0    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catchall_1
    move-exception v1

    .line 1511
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1512
    throw v1
.end method

.method public endTransaction()V
    .locals 2

    .prologue
    .line 494
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 496
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteSession;->endTransaction(Landroid/os/CancellationSignal;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 498
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 500
    return-void

    .line 497
    :catchall_0
    move-exception v0

    .line 498
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 499
    throw v0
.end method

.method public execSQL(Ljava/lang/String;)V
    .locals 1
    .param p1, "sql"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1606
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->executeSql(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1607
    return-void
.end method

.method public execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1653
    if-nez p2, :cond_0

    .line 1654
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Empty bindArgs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1656
    :cond_0
    invoke-direct {p0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->executeSql(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1657
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 258
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->dispose(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 262
    return-void

    .line 259
    :catchall_0
    move-exception v0

    .line 260
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 261
    throw v0
.end method

.method public getAttachedDbs()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1853
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1854
    .local v0, "attachedDbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1855
    :try_start_0
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    if-nez v4, :cond_0

    .line 1856
    monitor-exit v3

    move-object v0, v2

    .line 1894
    .end local v0    # "attachedDbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :goto_0
    return-object v0

    .line 1859
    .restart local v0    # "attachedDbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_0
    iget-boolean v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mHasAttachedDbsLocked:Z

    if-nez v2, :cond_1

    .line 1869
    new-instance v2, Landroid/util/Pair;

    const-string/jumbo v4, "main"

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v5, v5, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    invoke-direct {v2, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1870
    monitor-exit v3

    goto :goto_0

    .line 1854
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1873
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 1854
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1878
    const/4 v1, 0x0

    .line 1880
    .local v1, "c":Landroid/database/Cursor;
    :try_start_2
    const-string/jumbo v2, "pragma database_list;"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1881
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-nez v2, :cond_3

    .line 1890
    if-eqz v1, :cond_2

    .line 1891
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1896
    :cond_2
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    goto :goto_0

    .line 1887
    :cond_3
    :try_start_4
    new-instance v2, Landroid/util/Pair;

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 1889
    :catchall_1
    move-exception v2

    .line 1890
    if-eqz v1, :cond_4

    .line 1891
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1893
    :cond_4
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1895
    :catchall_2
    move-exception v2

    .line 1896
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1897
    throw v2
.end method

.method public getCorruptCode()I
    .locals 1

    .prologue
    .line 1785
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mCorrupt_code:I

    return v0
.end method

.method getLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 302
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 303
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    .line 302
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMaximumSize()J
    .locals 4

    .prologue
    .line 920
    const-string v2, "PRAGMA max_page_count;"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lsamsung/database/SecDatabaseUtils;->longForQuery(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 921
    .local v0, "pageCount":J
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPageSize()J

    move-result-wide v2

    mul-long/2addr v2, v0

    return-wide v2
.end method

.method public getPageSize()J
    .locals 2

    .prologue
    .line 949
    const-string v0, "PRAGMA page_size;"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lsamsung/database/SecDatabaseUtils;->longForQuery(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1735
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1736
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    .line 1735
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getThreadDefaultConnectionFlags(Z)I
    .locals 2
    .param p1, "readOnly"    # Z

    .prologue
    .line 353
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 355
    .local v0, "flags":I
    :goto_0
    invoke-static {}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isMainThread()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 356
    or-int/lit8 v0, v0, 0x4

    .line 358
    :cond_0
    return v0

    .line 354
    .end local v0    # "flags":I
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mThreadSession:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteSession;

    return-object v0
.end method

.method public getVersion()I
    .locals 2

    .prologue
    .line 902
    const-string v0, "PRAGMA user_version;"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lsamsung/database/SecDatabaseUtils;->longForQuery(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    return v0
.end method

.method public inTransaction()Z
    .locals 1

    .prologue
    .line 526
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 528
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v0

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteSession;->hasTransaction()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 530
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 528
    return v0

    .line 529
    :catchall_0
    move-exception v0

    .line 530
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 531
    throw v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 4
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "nullColumnHack"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1351
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 1354
    :goto_0
    return-wide v1

    .line 1352
    :catch_0
    move-exception v0

    .line 1353
    .local v0, "e":Landroid/database/SQLException;
    const-string v1, "SecSQLiteDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error inserting "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1354
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method public insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "nullColumnHack"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1377
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    .locals 9
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "nullColumnHack"    # Ljava/lang/String;
    .param p3, "initialValues"    # Landroid/content/ContentValues;
    .param p4, "conflictAlgorithm"    # I

    .prologue
    .line 1449
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 1451
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1452
    .local v5, "sql":Ljava/lang/StringBuilder;
    const-string v7, "INSERT"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1453
    sget-object v7, Lsamsung/database/sqlite/SecSQLiteDatabase;->CONFLICT_VALUES:[Ljava/lang/String;

    aget-object v7, v7, p4

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1454
    const-string v7, " INTO "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1455
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1456
    const/16 v7, 0x28

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1458
    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    .line 1459
    .local v0, "bindArgs":[Ljava/lang/Object;
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/ContentValues;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 1460
    invoke-virtual {p3}, Landroid/content/ContentValues;->size()I

    move-result v4

    .line 1461
    .local v4, "size":I
    :goto_0
    if-lez v4, :cond_5

    .line 1462
    new-array v0, v4, [Ljava/lang/Object;

    .line 1463
    const/4 v2, 0x0

    .line 1464
    .local v2, "i":I
    invoke-virtual {p3}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1469
    const/16 v7, 0x29

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1470
    const-string v7, " VALUES ("

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1471
    const/4 v2, 0x0

    .end local v3    # "i":I
    .restart local v2    # "i":I
    :goto_2
    if-lt v2, v4, :cond_3

    .line 1477
    .end local v2    # "i":I
    :goto_3
    const/16 v7, 0x29

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1479
    new-instance v6, Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p0, v7, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1481
    .local v6, "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :try_start_1
    invoke-virtual {v6}, Lsamsung/database/sqlite/SecSQLiteStatement;->executeInsert()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v7

    .line 1483
    :try_start_2
    invoke-virtual {v6}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1486
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1481
    return-wide v7

    .line 1460
    .end local v4    # "size":I
    .end local v6    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 1464
    .restart local v3    # "i":I
    .restart local v4    # "size":I
    :cond_1
    :try_start_3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1465
    .local v1, "colName":Ljava/lang/String;
    if-lez v3, :cond_2

    const-string v7, ","

    :goto_4
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1466
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1467
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v0, v3

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_1

    .line 1465
    :cond_2
    const-string v7, ""

    goto :goto_4

    .line 1472
    .end local v1    # "colName":Ljava/lang/String;
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_3
    if-lez v2, :cond_4

    const-string v7, ",?"

    :goto_5
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1471
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1472
    :cond_4
    const-string v7, "?"

    goto :goto_5

    .line 1475
    .end local v2    # "i":I
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ") VALUES (NULL"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 1485
    .end local v0    # "bindArgs":[Ljava/lang/Object;
    .end local v4    # "size":I
    .end local v5    # "sql":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v7

    .line 1486
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1487
    throw v7

    .line 1482
    .restart local v0    # "bindArgs":[Ljava/lang/Object;
    .restart local v4    # "size":I
    .restart local v5    # "sql":Ljava/lang/StringBuilder;
    .restart local v6    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catchall_1
    move-exception v7

    .line 1483
    :try_start_4
    invoke-virtual {v6}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V

    .line 1484
    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public isDatabaseIntegrityOk()Z
    .locals 10

    .prologue
    .line 1914
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 1916
    const/4 v0, 0x0

    .line 1918
    .local v0, "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getAttachedDbs()Ljava/util/List;

    move-result-object v0

    .line 1919
    if-nez v0, :cond_0

    .line 1920
    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "databaselist for: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " couldn\'t "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1921
    const-string v9, "be retrieved. probably because the database is closed"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1920
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1923
    :catch_0
    move-exception v2

    move-object v1, v0

    .line 1925
    .end local v0    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .local v1, "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .local v2, "e":Lsamsung/database/sqlite/SecSQLiteException;
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1926
    .end local v1    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v0    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :try_start_2
    new-instance v7, Landroid/util/Pair;

    const-string/jumbo v8, "main"

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1929
    .end local v2    # "e":Lsamsung/database/sqlite/SecSQLiteException;
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v7

    if-lt v3, v7, :cond_1

    .line 1946
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1948
    const/4 v7, 0x1

    :goto_1
    return v7

    .line 1930
    :cond_1
    :try_start_3
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1931
    .local v4, "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 1933
    .local v5, "prog":Lsamsung/database/sqlite/SecSQLiteStatement;
    :try_start_4
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v7, "PRAGMA "

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".integrity_check(1);"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lsamsung/database/sqlite/SecSQLiteDatabase;->compileStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteStatement;

    move-result-object v5

    .line 1935
    invoke-virtual {v5}, Lsamsung/database/sqlite/SecSQLiteStatement;->simpleQueryForIntegrityCheck()Ljava/lang/String;

    move-result-object v6

    .line 1936
    .local v6, "rslt":Ljava/lang/String;
    const-string/jumbo v7, "ok"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1938
    const-string v8, "SecSQLiteDatabase"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v7, "PRAGMA integrity_check on "

    invoke-direct {v9, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " returned: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1942
    if-eqz v5, :cond_2

    :try_start_5
    invoke-virtual {v5}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1946
    :cond_2
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1939
    const/4 v7, 0x0

    goto :goto_1

    .line 1941
    .end local v6    # "rslt":Ljava/lang/String;
    :catchall_0
    move-exception v7

    .line 1942
    if-eqz v5, :cond_3

    :try_start_6
    invoke-virtual {v5}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V

    .line 1943
    :cond_3
    throw v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1945
    .end local v3    # "i":I
    .end local v4    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "prog":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catchall_1
    move-exception v7

    .line 1946
    :goto_2
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1947
    throw v7

    .line 1942
    .restart local v3    # "i":I
    .restart local v4    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5    # "prog":Lsamsung/database/sqlite/SecSQLiteStatement;
    .restart local v6    # "rslt":Ljava/lang/String;
    :cond_4
    if-eqz v5, :cond_5

    :try_start_7
    invoke-virtual {v5}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1929
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1945
    .end local v0    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v3    # "i":I
    .end local v4    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "prog":Lsamsung/database/sqlite/SecSQLiteStatement;
    .end local v6    # "rslt":Ljava/lang/String;
    .restart local v1    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v2    # "e":Lsamsung/database/sqlite/SecSQLiteException;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v0    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    goto :goto_2
.end method

.method public isDbLockedByCurrentThread()Z
    .locals 1

    .prologue
    .line 547
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 549
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v0

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteSession;->hasConnection()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 551
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 549
    return v0

    .line 550
    :catchall_0
    move-exception v0

    .line 551
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 552
    throw v0
.end method

.method public isInMemoryDatabase()Z
    .locals 2

    .prologue
    .line 1703
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1704
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->isInMemoryDb()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1703
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isOpen()Z
    .locals 2

    .prologue
    .line 1714
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1715
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1714
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isReadOnly()Z
    .locals 2

    .prologue
    .line 1687
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1688
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnlyLocked()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1687
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public needUpgrade(I)Z
    .locals 1
    .param p1, "newVersion"    # I

    .prologue
    .line 1726
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getVersion()I

    move-result v0

    if-le p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAllReferencesReleased()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->dispose(Z)V

    .line 267
    return-void
.end method

.method onCorruption()V
    .locals 2

    .prologue
    .line 311
    const v0, 0x124fc

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 312
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    invoke-interface {v0, p0}, Lsamsung/database/SecDatabaseErrorHandler;->onCorruption(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    .line 313
    return-void
.end method

.method onCorruption(I)V
    .locals 2
    .param p1, "error_code"    # I

    .prologue
    .line 1792
    iput p1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mCorrupt_code:I

    .line 1794
    const v0, 0x124fc

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 1795
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    invoke-interface {v0, p0}, Lsamsung/database/SecDatabaseErrorHandler;->onCorruption(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    .line 1796
    return-void
.end method

.method public query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "columns"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "groupBy"    # Ljava/lang/String;
    .param p6, "having"    # Ljava/lang/String;
    .param p7, "orderBy"    # Ljava/lang/String;

    .prologue
    .line 1212
    const/4 v1, 0x0

    .line 1213
    const/4 v9, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    .line 1212
    invoke-virtual/range {v0 .. v9}, Lsamsung/database/sqlite/SecSQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "columns"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "groupBy"    # Ljava/lang/String;
    .param p6, "having"    # Ljava/lang/String;
    .param p7, "orderBy"    # Ljava/lang/String;
    .param p8, "limit"    # Ljava/lang/String;

    .prologue
    .line 1250
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v0 .. v9}, Lsamsung/database/sqlite/SecSQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "distinct"    # Z
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "groupBy"    # Ljava/lang/String;
    .param p7, "having"    # Ljava/lang/String;
    .param p8, "orderBy"    # Ljava/lang/String;
    .param p9, "limit"    # Ljava/lang/String;

    .prologue
    .line 1043
    const/4 v1, 0x0

    .line 1044
    const/4 v11, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    .line 1043
    invoke-virtual/range {v0 .. v11}, Lsamsung/database/sqlite/SecSQLiteDatabase;->queryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 12
    .param p1, "distinct"    # Z
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "groupBy"    # Ljava/lang/String;
    .param p7, "having"    # Ljava/lang/String;
    .param p8, "orderBy"    # Ljava/lang/String;
    .param p9, "limit"    # Ljava/lang/String;
    .param p10, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 1084
    const/4 v1, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-virtual/range {v0 .. v11}, Lsamsung/database/sqlite/SecSQLiteDatabase;->queryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "cursorFactory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "distinct"    # Z
    .param p3, "table"    # Ljava/lang/String;
    .param p4, "columns"    # [Ljava/lang/String;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "groupBy"    # Ljava/lang/String;
    .param p8, "having"    # Ljava/lang/String;
    .param p9, "orderBy"    # Ljava/lang/String;
    .param p10, "limit"    # Ljava/lang/String;

    .prologue
    .line 1124
    .line 1125
    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    .line 1124
    invoke-virtual/range {v0 .. v11}, Lsamsung/database/sqlite/SecSQLiteDatabase;->queryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 8
    .param p1, "cursorFactory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "distinct"    # Z
    .param p3, "table"    # Ljava/lang/String;
    .param p4, "columns"    # [Ljava/lang/String;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "groupBy"    # Ljava/lang/String;
    .param p8, "having"    # Ljava/lang/String;
    .param p9, "orderBy"    # Ljava/lang/String;
    .param p10, "limit"    # Ljava/lang/String;
    .param p11, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1168
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    move v0, p2

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    .line 1170
    :try_start_0
    invoke-static/range {v0 .. v7}, Lsamsung/database/sqlite/SecSQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1174
    .local v2, "sql":Ljava/lang/String;
    invoke-static {p3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->findEditTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p6

    move-object/from16 v5, p11

    .line 1173
    invoke-virtual/range {v0 .. v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQueryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1176
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1173
    return-object v0

    .line 1175
    .end local v2    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v0

    .line 1176
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1177
    throw v0
.end method

.method public rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1265
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQueryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public rawQuery(Ljava/lang/String;[Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 6
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    const/4 v1, 0x0

    .line 1283
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQueryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public rawQueryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "cursorFactory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "sql"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "editTable"    # Ljava/lang/String;

    .prologue
    .line 1301
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQueryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public rawQueryWithFactory(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 2
    .param p1, "cursorFactory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p2, "sql"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "editTable"    # Ljava/lang/String;
    .param p5, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 1322
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 1324
    :try_start_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;

    invoke-direct {v0, p0, p2, p4, p5}, Lsamsung/database/sqlite/SecSQLiteDirectCursorDriver;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)V

    .line 1326
    .local v0, "driver":Lsamsung/database/sqlite/SecSQLiteCursorDriver;
    if-eqz p1, :cond_0

    .end local p1    # "cursorFactory":Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    :goto_0
    invoke-interface {v0, p1, p3}, Lsamsung/database/sqlite/SecSQLiteCursorDriver;->query(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1329
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1326
    return-object v1

    .restart local p1    # "cursorFactory":Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    :cond_0
    :try_start_1
    iget-object p1, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mCursorFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1328
    .end local v0    # "driver":Lsamsung/database/sqlite/SecSQLiteCursorDriver;
    .end local p1    # "cursorFactory":Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    :catchall_0
    move-exception v1

    .line 1329
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1330
    throw v1
.end method

.method public reopenReadWrite()V
    .locals 5

    .prologue
    .line 755
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 756
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->throwIfNotOpenLocked()V

    .line 758
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnlyLocked()Z

    move-result v2

    if-nez v2, :cond_0

    .line 759
    monitor-exit v3

    .line 773
    :goto_0
    return-void

    .line 763
    :cond_0
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v1, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    .line 764
    .local v1, "oldOpenFlags":I
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v4, v4, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    and-int/lit8 v4, v4, -0x2

    iput v4, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 767
    :try_start_1
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v2, v4}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->reconfigure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 755
    :try_start_2
    monitor-exit v3

    goto :goto_0

    .end local v1    # "oldOpenFlags":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 768
    .restart local v1    # "oldOpenFlags":I
    :catch_0
    move-exception v0

    .line 769
    .local v0, "ex":Ljava/lang/RuntimeException;
    :try_start_3
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iput v1, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    .line 770
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 4
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "nullColumnHack"    # Ljava/lang/String;
    .param p3, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    .line 1397
    .line 1398
    const/4 v1, 0x5

    .line 1397
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 1401
    :goto_0
    return-wide v1

    .line 1399
    :catch_0
    move-exception v0

    .line 1400
    .local v0, "e":Landroid/database/SQLException;
    const-string v1, "SecSQLiteDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error inserting "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1401
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method public replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "nullColumnHack"    # Ljava/lang/String;
    .param p3, "initialValues"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1423
    .line 1424
    const/4 v0, 0x5

    .line 1423
    invoke-virtual {p0, p1, p2, p3, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public setMaxSqlCacheSize(I)V
    .locals 5
    .param p1, "cacheSize"    # I

    .prologue
    .line 1754
    const/16 v2, 0x64

    if-gt p1, v2, :cond_0

    if-gez p1, :cond_1

    .line 1755
    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    .line 1756
    const-string v3, "expected value between 0 and 100"

    .line 1755
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1759
    :cond_1
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1760
    :try_start_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->throwIfNotOpenLocked()V

    .line 1762
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v1, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->maxSqlCacheSize:I

    .line 1763
    .local v1, "oldMaxSqlCacheSize":I
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iput p1, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->maxSqlCacheSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1765
    :try_start_1
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConnectionPoolLocked:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v2, v4}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->reconfigure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1759
    :try_start_2
    monitor-exit v3

    .line 1771
    return-void

    .line 1766
    :catch_0
    move-exception v0

    .line 1767
    .local v0, "ex":Ljava/lang/RuntimeException;
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteDatabase;->mConfigurationLocked:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iput v1, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->maxSqlCacheSize:I

    .line 1768
    throw v0

    .line 1759
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    .end local v1    # "oldMaxSqlCacheSize":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public setMaximumSize(J)J
    .locals 10
    .param p1, "numBytes"    # J

    .prologue
    .line 932
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPageSize()J

    move-result-wide v4

    .line 933
    .local v4, "pageSize":J
    div-long v2, p1, v4

    .line 935
    .local v2, "numPages":J
    rem-long v6, p1, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 936
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 938
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PRAGMA max_page_count = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 939
    const/4 v7, 0x0

    .line 938
    invoke-static {p0, v6, v7}, Lsamsung/database/SecDatabaseUtils;->longForQuery(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 940
    .local v0, "newPageCount":J
    mul-long v6, v0, v4

    return-wide v6
.end method

.method public setPageSize(J)V
    .locals 2
    .param p1, "numBytes"    # J

    .prologue
    .line 960
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PRAGMA page_size = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 961
    return-void
.end method

.method public setTransactionSuccessful()V
    .locals 1

    .prologue
    .line 512
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 514
    :try_start_0
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v0

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteSession;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 516
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 518
    return-void

    .line 515
    :catchall_0
    move-exception v0

    .line 516
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 517
    throw v0
.end method

.method public setVersion(I)V
    .locals 2
    .param p1, "version"    # I

    .prologue
    .line 911
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PRAGMA user_version = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 912
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1953
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SecSQLiteDatabase: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "whereClause"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 1526
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->updateWithOnConflict(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public updateWithOnConflict(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I
    .locals 10
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "whereClause"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;
    .param p5, "conflictAlgorithm"    # I

    .prologue
    .line 1542
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    move-result v8

    if-nez v8, :cond_1

    .line 1543
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Empty values"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1546
    :cond_1
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->acquireReference()V

    .line 1548
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v8, 0x78

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1549
    .local v6, "sql":Ljava/lang/StringBuilder;
    const-string v8, "UPDATE "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1550
    sget-object v8, Lsamsung/database/sqlite/SecSQLiteDatabase;->CONFLICT_VALUES:[Ljava/lang/String;

    aget-object v8, v8, p5

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1551
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1552
    const-string v8, " SET "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1555
    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    move-result v5

    .line 1556
    .local v5, "setValuesSize":I
    if-nez p4, :cond_3

    move v1, v5

    .line 1557
    .local v1, "bindArgsSize":I
    :goto_0
    new-array v0, v1, [Ljava/lang/Object;

    .line 1558
    .local v0, "bindArgs":[Ljava/lang/Object;
    const/4 v3, 0x0

    .line 1559
    .local v3, "i":I
    invoke-virtual {p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v4, v3

    .end local v3    # "i":I
    .local v4, "i":I
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1565
    if-eqz p4, :cond_7

    .line 1566
    move v3, v5

    .end local v4    # "i":I
    .restart local v3    # "i":I
    :goto_2
    if-lt v3, v1, :cond_6

    .line 1570
    :goto_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1571
    const-string v8, " WHERE "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1572
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1575
    :cond_2
    new-instance v7, Lsamsung/database/sqlite/SecSQLiteStatement;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, p0, v8, v0}, Lsamsung/database/sqlite/SecSQLiteStatement;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1577
    .local v7, "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :try_start_1
    invoke-virtual {v7}, Lsamsung/database/sqlite/SecSQLiteStatement;->executeUpdateDelete()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    .line 1579
    :try_start_2
    invoke-virtual {v7}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1582
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1577
    return v8

    .line 1556
    .end local v0    # "bindArgs":[Ljava/lang/Object;
    .end local v1    # "bindArgsSize":I
    .end local v3    # "i":I
    .end local v7    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :cond_3
    :try_start_3
    array-length v8, p4

    add-int v1, v5, v8

    goto :goto_0

    .line 1559
    .restart local v0    # "bindArgs":[Ljava/lang/Object;
    .restart local v1    # "bindArgsSize":I
    .restart local v4    # "i":I
    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1560
    .local v2, "colName":Ljava/lang/String;
    if-lez v4, :cond_5

    const-string v8, ","

    :goto_4
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1561
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1562
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v0, v4

    .line 1563
    const-string v8, "=?"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_1

    .line 1560
    :cond_5
    const-string v8, ""

    goto :goto_4

    .line 1567
    .end local v2    # "colName":Ljava/lang/String;
    .end local v4    # "i":I
    .restart local v3    # "i":I
    :cond_6
    sub-int v8, v3, v5

    aget-object v8, p4, v8

    aput-object v8, v0, v3

    .line 1566
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1578
    .restart local v7    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catchall_0
    move-exception v8

    .line 1579
    invoke-virtual {v7}, Lsamsung/database/sqlite/SecSQLiteStatement;->close()V

    .line 1580
    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1581
    .end local v0    # "bindArgs":[Ljava/lang/Object;
    .end local v1    # "bindArgsSize":I
    .end local v3    # "i":I
    .end local v5    # "setValuesSize":I
    .end local v6    # "sql":Ljava/lang/StringBuilder;
    .end local v7    # "statement":Lsamsung/database/sqlite/SecSQLiteStatement;
    :catchall_1
    move-exception v8

    .line 1582
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->releaseReference()V

    .line 1583
    throw v8

    .restart local v0    # "bindArgs":[Ljava/lang/Object;
    .restart local v1    # "bindArgsSize":I
    .restart local v4    # "i":I
    .restart local v5    # "setValuesSize":I
    .restart local v6    # "sql":Ljava/lang/StringBuilder;
    :cond_7
    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_3
.end method

.method public yieldIfContendedSafely()Z
    .locals 3

    .prologue
    .line 564
    const/4 v0, 0x1

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->yieldIfContendedHelper(ZJ)Z

    move-result v0

    return v0
.end method

.method public yieldIfContendedSafely(J)Z
    .locals 1
    .param p1, "sleepAfterYieldDelay"    # J

    .prologue
    .line 579
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->yieldIfContendedHelper(ZJ)Z

    move-result v0

    return v0
.end method
