.class public final Lsamsung/database/sqlite/SecSQLiteSession;
.super Ljava/lang/Object;
.source "SecSQLiteSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final TRANSACTION_MODE_DEFERRED:I = 0x0

.field public static final TRANSACTION_MODE_EXCLUSIVE:I = 0x2

.field public static final TRANSACTION_MODE_IMMEDIATE:I = 0x1


# instance fields
.field private mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

.field private mConnectionFlags:I

.field private final mConnectionPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

.field private mConnectionUseCount:I

.field private mTransactionPool:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

.field private mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163
    const-class v0, Lsamsung/database/sqlite/SecSQLiteSession;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lsamsung/database/sqlite/SecSQLiteSession;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lsamsung/database/sqlite/SecSQLiteConnectionPool;)V
    .locals 2
    .param p1, "connectionPool"    # Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228
    if-nez p1, :cond_0

    .line 229
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "connectionPool must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    .line 233
    return-void
.end method

.method private acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V
    .locals 1
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "connectionFlags"    # I
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 903
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-nez v0, :cond_1

    .line 904
    sget-boolean v0, Lsamsung/database/sqlite/SecSQLiteSession;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionUseCount:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 905
    :cond_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    invoke-virtual {v0, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)Lsamsung/database/sqlite/SecSQLiteConnection;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 907
    iput p2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionFlags:I

    .line 909
    :cond_1
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionUseCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionUseCount:I

    .line 910
    return-void
.end method

.method private beginTransactionUnchecked(ILsamsung/database/sqlite/SecSQLiteTransactionListener;ILandroid/os/CancellationSignal;)V
    .locals 5
    .param p1, "transactionMode"    # I
    .param p2, "transactionListener"    # Lsamsung/database/sqlite/SecSQLiteTransactionListener;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    const/4 v3, 0x0

    .line 305
    if-eqz p4, :cond_0

    .line 306
    invoke-virtual {p4}, Landroid/os/CancellationSignal;->throwIfCanceled()V

    .line 309
    :cond_0
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-nez v2, :cond_1

    .line 310
    invoke-direct {p0, v3, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 315
    :cond_1
    :try_start_0
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-nez v2, :cond_2

    .line 317
    packed-switch p1, :pswitch_data_0

    .line 327
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    const-string v3, "BEGIN;"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->execute(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    :cond_2
    :goto_0
    if-eqz p2, :cond_3

    .line 335
    :try_start_1
    invoke-interface {p2}, Lsamsung/database/sqlite/SecSQLiteTransactionListener;->onBegin()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    :cond_3
    :try_start_2
    invoke-direct {p0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteSession;->obtainTransaction(ILsamsung/database/sqlite/SecSQLiteTransactionListener;)Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    move-result-object v1

    .line 346
    .local v1, "transaction":Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iput-object v2, v1, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mParent:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .line 347
    iput-object v1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 349
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-nez v2, :cond_4

    .line 350
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 353
    :cond_4
    return-void

    .line 319
    .end local v1    # "transaction":Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    :pswitch_0
    :try_start_3
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    const-string v3, "BEGIN IMMEDIATE;"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->execute(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v2

    .line 349
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-nez v3, :cond_5

    .line 350
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 352
    :cond_5
    throw v2

    .line 323
    :pswitch_1
    :try_start_4
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    const-string v3, "BEGIN EXCLUSIVE;"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->execute(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V

    goto :goto_0

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "ex":Ljava/lang/RuntimeException;
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-nez v2, :cond_6

    .line 338
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    const-string v3, "ROLLBACK;"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->execute(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V

    .line 340
    :cond_6
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 317
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private endTransactionUnchecked(Landroid/os/CancellationSignal;Z)V
    .locals 8
    .param p1, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p2, "yielding"    # Z

    .prologue
    const/4 v5, 0x1

    .line 405
    if-eqz p1, :cond_0

    .line 406
    invoke-virtual {p1}, Landroid/os/CancellationSignal;->throwIfCanceled()V

    .line 409
    :cond_0
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .line 410
    .local v4, "top":Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    iget-boolean v6, v4, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mMarkedSuccessful:Z

    if-nez v6, :cond_1

    if-eqz p2, :cond_4

    :cond_1
    iget-boolean v6, v4, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mChildFailed:Z

    if-nez v6, :cond_4

    move v3, v5

    .line 412
    .local v3, "successful":Z
    :goto_0
    const/4 v2, 0x0

    .line 413
    .local v2, "listenerException":Ljava/lang/RuntimeException;
    iget-object v1, v4, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mListener:Lsamsung/database/sqlite/SecSQLiteTransactionListener;

    .line 414
    .local v1, "listener":Lsamsung/database/sqlite/SecSQLiteTransactionListener;
    if-eqz v1, :cond_2

    .line 416
    if-eqz v3, :cond_5

    .line 417
    :try_start_0
    invoke-interface {v1}, Lsamsung/database/sqlite/SecSQLiteTransactionListener;->onCommit()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    :cond_2
    :goto_1
    iget-object v6, v4, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mParent:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iput-object v6, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .line 428
    invoke-direct {p0, v4}, Lsamsung/database/sqlite/SecSQLiteSession;->recycleTransaction(Lsamsung/database/sqlite/SecSQLiteSession$Transaction;)V

    .line 430
    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-eqz v6, :cond_6

    .line 431
    if-nez v3, :cond_3

    .line 432
    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iput-boolean v5, v6, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mChildFailed:Z

    .line 446
    :cond_3
    :goto_2
    if-eqz v2, :cond_8

    .line 447
    throw v2

    .line 410
    .end local v1    # "listener":Lsamsung/database/sqlite/SecSQLiteTransactionListener;
    .end local v2    # "listenerException":Ljava/lang/RuntimeException;
    .end local v3    # "successful":Z
    :cond_4
    const/4 v3, 0x0

    goto :goto_0

    .line 419
    .restart local v1    # "listener":Lsamsung/database/sqlite/SecSQLiteTransactionListener;
    .restart local v2    # "listenerException":Ljava/lang/RuntimeException;
    .restart local v3    # "successful":Z
    :cond_5
    :try_start_1
    invoke-interface {v1}, Lsamsung/database/sqlite/SecSQLiteTransactionListener;->onRollback()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 421
    :catch_0
    move-exception v0

    .line 422
    .local v0, "ex":Ljava/lang/RuntimeException;
    move-object v2, v0

    .line 423
    const/4 v3, 0x0

    goto :goto_1

    .line 436
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :cond_6
    if-eqz v3, :cond_7

    .line 437
    :try_start_2
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    const-string v6, "COMMIT;"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->execute(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 442
    :goto_3
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    goto :goto_2

    .line 439
    :cond_7
    :try_start_3
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    const-string v6, "ROLLBACK;"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->execute(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 441
    :catchall_0
    move-exception v5

    .line 442
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 443
    throw v5

    .line 449
    :cond_8
    return-void
.end method

.method private executeSpecial(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Z
    .locals 4
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    const/4 v1, 0x1

    .line 878
    if-eqz p4, :cond_0

    .line 879
    invoke-virtual {p4}, Landroid/os/CancellationSignal;->throwIfCanceled()V

    .line 882
    :cond_0
    invoke-static {p1}, Lsamsung/database/SecDatabaseUtils;->getSqlStatementType(Ljava/lang/String;)I

    move-result v0

    .line 883
    .local v0, "type":I
    packed-switch v0, :pswitch_data_0

    .line 898
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 885
    :pswitch_0
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->beginTransaction(ILsamsung/database/sqlite/SecSQLiteTransactionListener;ILandroid/os/CancellationSignal;)V

    goto :goto_0

    .line 890
    :pswitch_1
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->setTransactionSuccessful()V

    .line 891
    invoke-virtual {p0, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->endTransaction(Landroid/os/CancellationSignal;)V

    goto :goto_0

    .line 895
    :pswitch_2
    invoke-virtual {p0, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->endTransaction(Landroid/os/CancellationSignal;)V

    goto :goto_0

    .line 883
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private obtainTransaction(ILsamsung/database/sqlite/SecSQLiteTransactionListener;)Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    .locals 4
    .param p1, "mode"    # I
    .param p2, "listener"    # Lsamsung/database/sqlite/SecSQLiteTransactionListener;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 947
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionPool:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .line 948
    .local v0, "transaction":Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    if-eqz v0, :cond_0

    .line 949
    iget-object v1, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mParent:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iput-object v1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionPool:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .line 950
    iput-object v3, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mParent:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .line 951
    iput-boolean v2, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mMarkedSuccessful:Z

    .line 952
    iput-boolean v2, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mChildFailed:Z

    .line 956
    :goto_0
    iput p1, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mMode:I

    .line 957
    iput-object p2, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mListener:Lsamsung/database/sqlite/SecSQLiteTransactionListener;

    .line 958
    return-object v0

    .line 954
    :cond_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .end local v0    # "transaction":Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    invoke-direct {v0, v3}, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;-><init>(Lsamsung/database/sqlite/SecSQLiteSession$Transaction;)V

    .restart local v0    # "transaction":Lsamsung/database/sqlite/SecSQLiteSession$Transaction;
    goto :goto_0
.end method

.method private recycleTransaction(Lsamsung/database/sqlite/SecSQLiteSession$Transaction;)V
    .locals 1
    .param p1, "transaction"    # Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .prologue
    .line 962
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionPool:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iput-object v0, p1, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mParent:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .line 963
    const/4 v0, 0x0

    iput-object v0, p1, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mListener:Lsamsung/database/sqlite/SecSQLiteTransactionListener;

    .line 964
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionPool:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    .line 965
    return-void
.end method

.method private releaseConnection()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 913
    sget-boolean v0, Lsamsung/database/sqlite/SecSQLiteSession;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 914
    :cond_0
    sget-boolean v0, Lsamsung/database/sqlite/SecSQLiteSession;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionUseCount:I

    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 915
    :cond_1
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionUseCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionUseCount:I

    if-nez v0, :cond_2

    .line 917
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->releaseConnection(Lsamsung/database/sqlite/SecSQLiteConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 919
    iput-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 922
    :cond_2
    return-void

    .line 918
    :catchall_0
    move-exception v0

    .line 919
    iput-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 920
    throw v0
.end method

.method private throwIfNestedTransaction()V
    .locals 2

    .prologue
    .line 940
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mParent:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-eqz v0, :cond_0

    .line 941
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot perform this operation because a nested transaction is in progress."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 944
    :cond_0
    return-void
.end method

.method private throwIfNoTransaction()V
    .locals 2

    .prologue
    .line 925
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-nez v0, :cond_0

    .line 926
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot perform this operation because there is no current transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 929
    :cond_0
    return-void
.end method

.method private throwIfTransactionMarkedSuccessful()V
    .locals 2

    .prologue
    .line 932
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iget-boolean v0, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mMarkedSuccessful:Z

    if-eqz v0, :cond_0

    .line 933
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot perform this operation because the transaction has already been marked successful.  The only thing you can do now is call endTransaction()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 937
    :cond_0
    return-void
.end method

.method private yieldTransactionUnchecked(JLandroid/os/CancellationSignal;)Z
    .locals 7
    .param p1, "sleepAfterYieldDelayMillis"    # J
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    const/4 v3, 0x1

    .line 526
    if-eqz p3, :cond_0

    .line 527
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->throwIfCanceled()V

    .line 530
    :cond_0
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    iget v6, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionFlags:I

    invoke-virtual {v4, v5, v6}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->shouldYieldConnection(Lsamsung/database/sqlite/SecSQLiteConnection;I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 531
    const/4 v3, 0x0

    .line 549
    :goto_0
    return v3

    .line 534
    :cond_1
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iget v2, v4, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mMode:I

    .line 535
    .local v2, "transactionMode":I
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iget-object v1, v4, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mListener:Lsamsung/database/sqlite/SecSQLiteTransactionListener;

    .line 536
    .local v1, "listener":Lsamsung/database/sqlite/SecSQLiteTransactionListener;
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionFlags:I

    .line 537
    .local v0, "connectionFlags":I
    invoke-direct {p0, p3, v3}, Lsamsung/database/sqlite/SecSQLiteSession;->endTransactionUnchecked(Landroid/os/CancellationSignal;Z)V

    .line 539
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-lez v4, :cond_2

    .line 541
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 547
    :cond_2
    :goto_1
    invoke-direct {p0, v2, v1, v0, p3}, Lsamsung/database/sqlite/SecSQLiteSession;->beginTransactionUnchecked(ILsamsung/database/sqlite/SecSQLiteTransactionListener;ILandroid/os/CancellationSignal;)V

    goto :goto_0

    .line 542
    :catch_0
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method public beginTransaction(ILsamsung/database/sqlite/SecSQLiteTransactionListener;ILandroid/os/CancellationSignal;)V
    .locals 0
    .param p1, "transactionMode"    # I
    .param p2, "transactionListener"    # Lsamsung/database/sqlite/SecSQLiteTransactionListener;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 297
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->throwIfTransactionMarkedSuccessful()V

    .line 298
    invoke-direct {p0, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->beginTransactionUnchecked(ILsamsung/database/sqlite/SecSQLiteTransactionListener;ILandroid/os/CancellationSignal;)V

    .line 300
    return-void
.end method

.method public changePassword([B)V
    .locals 2
    .param p1, "newPassword"    # [B

    .prologue
    const/4 v1, 0x0

    .line 846
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnectionFlags:I

    invoke-direct {p0, v1, v0, v1}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 847
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-eqz v0, :cond_0

    .line 848
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->changePassword([B)V

    .line 850
    :cond_0
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 852
    return-void
.end method

.method public endTransaction(Landroid/os/CancellationSignal;)V
    .locals 1
    .param p1, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 398
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->throwIfNoTransaction()V

    .line 399
    sget-boolean v0, Lsamsung/database/sqlite/SecSQLiteSession;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 401
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteSession;->endTransactionUnchecked(Landroid/os/CancellationSignal;Z)V

    .line 402
    return-void
.end method

.method public execute(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)V
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 609
    if-nez p1, :cond_0

    .line 610
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sql must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 613
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->executeSpecial(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 623
    :goto_0
    return-void

    .line 617
    :cond_1
    invoke-direct {p0, p1, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 619
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1, p2, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->execute(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 621
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    goto :goto_0

    .line 620
    :catchall_0
    move-exception v0

    .line 621
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 622
    throw v0
.end method

.method public executeForBlobFileDescriptor(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 710
    if-nez p1, :cond_0

    .line 711
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sql must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 714
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->executeSpecial(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 715
    const/4 v0, 0x0

    .line 720
    :goto_0
    return-object v0

    .line 718
    :cond_1
    invoke-direct {p0, p1, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 720
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1, p2, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForBlobFileDescriptor(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 723
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    goto :goto_0

    .line 722
    :catchall_0
    move-exception v0

    .line 723
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 724
    throw v0
.end method

.method public executeForChangedRowCount(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)I
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 744
    if-nez p1, :cond_0

    .line 745
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sql must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 748
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->executeSpecial(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    const/4 v0, 0x0

    .line 754
    :goto_0
    return v0

    .line 752
    :cond_1
    invoke-direct {p0, p1, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 754
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1, p2, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForChangedRowCount(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 757
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    goto :goto_0

    .line 756
    :catchall_0
    move-exception v0

    .line 757
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 758
    throw v0
.end method

.method public executeForCursorWindow(Ljava/lang/String;[Ljava/lang/Object;Lsamsung/database/SecCursorWindow;IIZILandroid/os/CancellationSignal;)I
    .locals 10
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "window"    # Lsamsung/database/SecCursorWindow;
    .param p4, "startPos"    # I
    .param p5, "requiredPos"    # I
    .param p6, "countAllRows"    # Z
    .param p7, "connectionFlags"    # I
    .param p8, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 822
    if-nez p1, :cond_0

    .line 823
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "sql must not be null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 825
    :cond_0
    if-nez p3, :cond_1

    .line 826
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "window must not be null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 829
    :cond_1
    move/from16 v0, p7

    move-object/from16 v1, p8

    invoke-direct {p0, p1, p2, v0, v1}, Lsamsung/database/sqlite/SecSQLiteSession;->executeSpecial(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 830
    invoke-virtual {p3}, Lsamsung/database/SecCursorWindow;->clear()V

    .line 831
    const/4 v2, 0x0

    .line 836
    :goto_0
    return v2

    .line 834
    :cond_2
    move/from16 v0, p7

    move-object/from16 v1, p8

    invoke-direct {p0, p1, v0, v1}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 836
    :try_start_0
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p8

    invoke-virtual/range {v2 .. v9}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForCursorWindow(Ljava/lang/String;[Ljava/lang/Object;Lsamsung/database/SecCursorWindow;IIZLandroid/os/CancellationSignal;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 840
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    goto :goto_0

    .line 839
    :catchall_0
    move-exception v2

    .line 840
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 841
    throw v2
.end method

.method public executeForLastInsertedRowId(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)J
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 778
    if-nez p1, :cond_0

    .line 779
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sql must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 782
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->executeSpecial(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 783
    const-wide/16 v0, 0x0

    .line 788
    :goto_0
    return-wide v0

    .line 786
    :cond_1
    invoke-direct {p0, p1, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 788
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1, p2, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForLastInsertedRowId(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 791
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    goto :goto_0

    .line 790
    :catchall_0
    move-exception v0

    .line 791
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 792
    throw v0
.end method

.method public executeForLong(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)J
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 642
    if-nez p1, :cond_0

    .line 643
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sql must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 646
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->executeSpecial(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 647
    const-wide/16 v0, 0x0

    .line 652
    :goto_0
    return-wide v0

    .line 650
    :cond_1
    invoke-direct {p0, p1, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 652
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1, p2, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForLong(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 654
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    goto :goto_0

    .line 653
    :catchall_0
    move-exception v0

    .line 654
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 655
    throw v0
.end method

.method public executeForString(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Ljava/lang/String;
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "connectionFlags"    # I
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 675
    if-nez p1, :cond_0

    .line 676
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sql must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 679
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->executeSpecial(Ljava/lang/String;[Ljava/lang/Object;ILandroid/os/CancellationSignal;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 680
    const/4 v0, 0x0

    .line 685
    :goto_0
    return-object v0

    .line 683
    :cond_1
    invoke-direct {p0, p1, p3, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 685
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1, p2, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForString(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 687
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    goto :goto_0

    .line 686
    :catchall_0
    move-exception v0

    .line 687
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 688
    throw v0
.end method

.method public hasConnection()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNestedTransaction()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mParent:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTransaction()Z
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepare(Ljava/lang/String;ILandroid/os/CancellationSignal;Lsamsung/database/sqlite/SecSQLiteStatementInfo;)V
    .locals 2
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "connectionFlags"    # I
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "outStatementInfo"    # Lsamsung/database/sqlite/SecSQLiteStatementInfo;

    .prologue
    .line 578
    if-nez p1, :cond_0

    .line 579
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sql must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 582
    :cond_0
    if-eqz p3, :cond_1

    .line 583
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->throwIfCanceled()V

    .line 586
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteSession;->acquireConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)V

    .line 588
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-virtual {v0, p1, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->prepare(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteStatementInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 590
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 592
    return-void

    .line 589
    :catchall_0
    move-exception v0

    .line 590
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->releaseConnection()V

    .line 591
    throw v0
.end method

.method public setTransactionSuccessful()V
    .locals 2

    .prologue
    .line 371
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->throwIfNoTransaction()V

    .line 372
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->throwIfTransactionMarkedSuccessful()V

    .line 374
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mMarkedSuccessful:Z

    .line 375
    return-void
.end method

.method public yieldTransaction(JZLandroid/os/CancellationSignal;)Z
    .locals 2
    .param p1, "sleepAfterYieldDelayMillis"    # J
    .param p3, "throwIfUnsafe"    # Z
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    const/4 v0, 0x0

    .line 504
    if-eqz p3, :cond_1

    .line 505
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->throwIfNoTransaction()V

    .line 506
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->throwIfTransactionMarkedSuccessful()V

    .line 507
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteSession;->throwIfNestedTransaction()V

    .line 514
    :cond_0
    sget-boolean v1, Lsamsung/database/sqlite/SecSQLiteSession;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mConnection:Lsamsung/database/sqlite/SecSQLiteConnection;

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 509
    :cond_1
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iget-boolean v1, v1, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mMarkedSuccessful:Z

    if-nez v1, :cond_2

    .line 510
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iget-object v1, v1, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mParent:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    if-eqz v1, :cond_0

    .line 520
    :cond_2
    :goto_0
    return v0

    .line 516
    :cond_3
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteSession;->mTransactionStack:Lsamsung/database/sqlite/SecSQLiteSession$Transaction;

    iget-boolean v1, v1, Lsamsung/database/sqlite/SecSQLiteSession$Transaction;->mChildFailed:Z

    if-nez v1, :cond_2

    .line 520
    invoke-direct {p0, p1, p2, p4}, Lsamsung/database/sqlite/SecSQLiteSession;->yieldTransactionUnchecked(JLandroid/os/CancellationSignal;)Z

    move-result v0

    goto :goto_0
.end method
