.class public final Lsamsung/database/sqlite/SecSQLiteGlobal;
.super Ljava/lang/Object;
.source "SecSQLiteGlobal.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SecSQLiteGlobal"

.field private static sDefaultPageSize:I

.field private static final sLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lsamsung/database/sqlite/SecSQLiteGlobal;->sLock:Ljava/lang/Object;

    .line 33
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method private static native nativeReleaseMemory()I
.end method

.method public static releaseMemory()I
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lsamsung/database/sqlite/SecSQLiteGlobal;->nativeReleaseMemory()I

    move-result v0

    return v0
.end method
