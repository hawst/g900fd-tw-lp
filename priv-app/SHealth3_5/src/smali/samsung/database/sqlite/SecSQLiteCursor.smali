.class public Lsamsung/database/sqlite/SecSQLiteCursor;
.super Lsamsung/database/SecAbstractWindowedCursor;
.source "SecSQLiteCursor.java"


# static fields
.field static final NO_COUNT:I = -0x1

.field static final TAG:Ljava/lang/String; = "SecSQLiteCursor"


# instance fields
.field private mColumnNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mColumns:[Ljava/lang/String;

.field private mCount:I

.field private mCursorWindowCapacity:I

.field private final mDriver:Lsamsung/database/sqlite/SecSQLiteCursorDriver;

.field private final mEditTable:Ljava/lang/String;

.field private final mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;


# direct methods
.method public constructor <init>(Lsamsung/database/sqlite/SecSQLiteCursorDriver;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteQuery;)V
    .locals 2
    .param p1, "driver"    # Lsamsung/database/sqlite/SecSQLiteCursorDriver;
    .param p2, "editTable"    # Ljava/lang/String;
    .param p3, "query"    # Lsamsung/database/sqlite/SecSQLiteQuery;

    .prologue
    .line 91
    invoke-direct {p0}, Lsamsung/database/SecAbstractWindowedCursor;-><init>()V

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCount:I

    .line 92
    if-nez p3, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "query object cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mDriver:Lsamsung/database/sqlite/SecSQLiteCursorDriver;

    .line 96
    iput-object p2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mEditTable:Ljava/lang/String;

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mColumnNameMap:Ljava/util/Map;

    .line 98
    iput-object p3, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

    .line 100
    invoke-virtual {p3}, Lsamsung/database/sqlite/SecSQLiteQuery;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mColumns:[Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mColumns:[Ljava/lang/String;

    invoke-static {v0}, Lsamsung/database/SecDatabaseUtils;->findRowIdColumnIndex([Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mRowIdColumnIndex:I

    .line 102
    return-void
.end method

.method public constructor <init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Lsamsung/database/sqlite/SecSQLiteCursorDriver;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteQuery;)V
    .locals 0
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "driver"    # Lsamsung/database/sqlite/SecSQLiteCursorDriver;
    .param p3, "editTable"    # Ljava/lang/String;
    .param p4, "query"    # Lsamsung/database/sqlite/SecSQLiteQuery;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteCursor;-><init>(Lsamsung/database/sqlite/SecSQLiteCursorDriver;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteQuery;)V

    .line 78
    return-void
.end method

.method private fillWindow(I)V
    .locals 4
    .param p1, "requiredPos"    # I

    .prologue
    const/4 v3, 0x0

    .line 132
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteCursor;->getDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lsamsung/database/sqlite/SecSQLiteCursor;->clearOrCreateWindow(Ljava/lang/String;)V

    .line 134
    iget v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCount:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 135
    invoke-static {p1, v3}, Lsamsung/database/SecDatabaseUtils;->cursorPickFillWindowStartPosition(II)I

    move-result v0

    .line 136
    .local v0, "startPos":I
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, p1, v3}, Lsamsung/database/sqlite/SecSQLiteQuery;->fillWindow(Lsamsung/database/SecCursorWindow;IIZ)I

    move-result v1

    iput v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCount:I

    .line 137
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v1}, Lsamsung/database/SecCursorWindow;->getNumRows()I

    move-result v1

    iput v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCursorWindowCapacity:I

    .line 138
    const-string v1, "SecSQLiteCursor"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    const-string v1, "SecSQLiteCursor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "received count(*) from native_fill_window: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 143
    .end local v0    # "startPos":I
    :cond_1
    iget v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCursorWindowCapacity:I

    .line 142
    invoke-static {p1, v1}, Lsamsung/database/SecDatabaseUtils;->cursorPickFillWindowStartPosition(II)I

    move-result v0

    .line 144
    .restart local v0    # "startPos":I
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v1, v2, v0, p1, v3}, Lsamsung/database/sqlite/SecSQLiteQuery;->fillWindow(Lsamsung/database/SecCursorWindow;IIZ)I

    goto :goto_0
.end method


# virtual methods
.method protected clearOrCreateWindow(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 261
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-nez v0, :cond_0

    .line 262
    invoke-super {p0, p1}, Lsamsung/database/SecAbstractWindowedCursor;->clearOrCreateWindow(Ljava/lang/String;)V

    .line 267
    :goto_0
    return-void

    .line 264
    :cond_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v0}, Lsamsung/database/SecCursorWindow;->clear()V

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0}, Lsamsung/database/SecAbstractWindowedCursor;->close()V

    .line 192
    invoke-super {p0}, Lsamsung/database/SecAbstractWindowedCursor;->closeWindow()V

    .line 193
    monitor-enter p0

    .line 194
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteQuery;->close()V

    .line 195
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mDriver:Lsamsung/database/sqlite/SecSQLiteCursorDriver;

    invoke-interface {v0}, Lsamsung/database/sqlite/SecSQLiteCursorDriver;->cursorClosed()V

    .line 193
    monitor-exit p0

    .line 197
    return-void

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 184
    invoke-super {p0}, Lsamsung/database/SecAbstractWindowedCursor;->deactivate()V

    .line 185
    invoke-super {p0}, Lsamsung/database/SecAbstractWindowedCursor;->closeWindow()V

    .line 186
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mDriver:Lsamsung/database/sqlite/SecSQLiteCursorDriver;

    invoke-interface {v0}, Lsamsung/database/sqlite/SecSQLiteCursorDriver;->cursorDeactivated()V

    .line 187
    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 248
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteCursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :cond_0
    invoke-super {p0}, Lsamsung/database/SecAbstractWindowedCursor;->finalize()V

    .line 254
    return-void

    .line 251
    :catchall_0
    move-exception v0

    .line 252
    invoke-super {p0}, Lsamsung/database/SecAbstractWindowedCursor;->finalize()V

    .line 253
    throw v0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 10
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    const/4 v6, -0x1

    .line 151
    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mColumnNameMap:Ljava/util/Map;

    if-nez v7, :cond_0

    .line 152
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mColumns:[Ljava/lang/String;

    .line 153
    .local v1, "columns":[Ljava/lang/String;
    array-length v0, v1

    .line 154
    .local v0, "columnCount":I
    new-instance v4, Ljava/util/HashMap;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v4, v0, v7}, Ljava/util/HashMap;-><init>(IF)V

    .line 155
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_3

    .line 158
    iput-object v4, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mColumnNameMap:Ljava/util/Map;

    .line 162
    .end local v0    # "columnCount":I
    .end local v1    # "columns":[Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_0
    const/16 v7, 0x2e

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 163
    .local v5, "periodIndex":I
    if-eq v5, v6, :cond_1

    .line 164
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    .line 165
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "SecSQLiteCursor"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "requesting column name with table name -- "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 166
    add-int/lit8 v7, v5, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 169
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mColumnNameMap:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 170
    .local v3, "i":Ljava/lang/Integer;
    if-eqz v3, :cond_2

    .line 171
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 173
    :cond_2
    return v6

    .line 156
    .end local v5    # "periodIndex":I
    .restart local v0    # "columnCount":I
    .restart local v1    # "columns":[Ljava/lang/String;
    .local v3, "i":I
    .restart local v4    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_3
    aget-object v7, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mColumns:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 125
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteCursor;->fillWindow(I)V

    .line 128
    :cond_0
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCount:I

    return v0
.end method

.method public getDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteQuery;->getDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public onMove(II)Z
    .locals 2
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v0}, Lsamsung/database/SecCursorWindow;->getStartPosition()I

    move-result v0

    if-lt p2, v0, :cond_0

    .line 116
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v0}, Lsamsung/database/SecCursorWindow;->getStartPosition()I

    move-result v0

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v1}, Lsamsung/database/SecCursorWindow;->getNumRows()I

    move-result v1

    add-int/2addr v0, v1

    if-lt p2, v0, :cond_1

    .line 117
    :cond_0
    invoke-direct {p0, p2}, Lsamsung/database/sqlite/SecSQLiteCursor;->fillWindow(I)V

    .line 120
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public requery()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 201
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteCursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 224
    :goto_0
    return v1

    .line 205
    :cond_0
    monitor-enter p0

    .line 206
    :try_start_0
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mQuery:Lsamsung/database/sqlite/SecSQLiteQuery;

    invoke-virtual {v2}, Lsamsung/database/sqlite/SecSQLiteQuery;->getDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_1

    .line 207
    monitor-exit p0

    goto :goto_0

    .line 205
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 210
    :cond_1
    :try_start_1
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    if-eqz v2, :cond_2

    .line 211
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mWindow:Lsamsung/database/SecCursorWindow;

    invoke-virtual {v2}, Lsamsung/database/SecCursorWindow;->clear()V

    .line 213
    :cond_2
    const/4 v2, -0x1

    iput v2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mPos:I

    .line 214
    const/4 v2, -0x1

    iput v2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCount:I

    .line 216
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mDriver:Lsamsung/database/sqlite/SecSQLiteCursorDriver;

    invoke-interface {v2, p0}, Lsamsung/database/sqlite/SecSQLiteCursorDriver;->cursorRequeried(Landroid/database/Cursor;)V

    .line 205
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    :try_start_2
    invoke-super {p0}, Lsamsung/database/SecAbstractWindowedCursor;->requery()Z
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v1

    goto :goto_0

    .line 221
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "SecSQLiteCursor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "requery() failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setSelectionArguments([Ljava/lang/String;)V
    .locals 1
    .param p1, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 238
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mDriver:Lsamsung/database/sqlite/SecSQLiteCursorDriver;

    invoke-interface {v0, p1}, Lsamsung/database/sqlite/SecSQLiteCursorDriver;->setBindArguments([Ljava/lang/String;)V

    .line 239
    return-void
.end method

.method public setWindow(Lsamsung/database/SecCursorWindow;)V
    .locals 1
    .param p1, "window"    # Lsamsung/database/SecCursorWindow;

    .prologue
    .line 230
    invoke-super {p0, p1}, Lsamsung/database/SecAbstractWindowedCursor;->setWindow(Lsamsung/database/SecCursorWindow;)V

    .line 231
    const/4 v0, -0x1

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteCursor;->mCount:I

    .line 232
    return-void
.end method
