.class Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;
.super Ljava/lang/Object;
.source "SecSQLiteConnectionPool.java"

# interfaces
.implements Landroid/os/CancellationSignal$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsamsung/database/sqlite/SecSQLiteConnectionPool;->waitForConnection(Ljava/lang/String;ILandroid/os/CancellationSignal;)Lsamsung/database/sqlite/SecSQLiteConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

.field private final synthetic val$nonce:I

.field private final synthetic val$waiter:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;


# direct methods
.method constructor <init>(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;->this$0:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    iput-object p2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;->val$waiter:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    iput p3, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;->val$nonce:I

    .line 655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 3

    .prologue
    .line 658
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;->this$0:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    # getter for: Lsamsung/database/sqlite/SecSQLiteConnectionPool;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->access$0(Lsamsung/database/sqlite/SecSQLiteConnectionPool;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 659
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;->val$waiter:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    iget v0, v0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;->mNonce:I

    iget v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;->val$nonce:I

    if-ne v0, v2, :cond_0

    .line 660
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;->this$0:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnectionPool$1;->val$waiter:Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;

    # invokes: Lsamsung/database/sqlite/SecSQLiteConnectionPool;->cancelConnectionWaiterLocked(Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;)V
    invoke-static {v0, v2}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->access$1(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteConnectionPool$ConnectionWaiter;)V

    .line 658
    :cond_0
    monitor-exit v1

    .line 663
    return-void

    .line 658
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
