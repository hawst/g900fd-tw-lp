.class public final Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;
.super Ljava/lang/Object;
.source "SecSQLiteDatabaseConfiguration.java"


# static fields
.field private static final EMAIL_IN_DB_PATTERN:Ljava/util/regex/Pattern;

.field public static final MEMORY_DB_PATH:Ljava/lang/String; = ":memory:"


# instance fields
.field public final customFunctions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteCustomFunction;",
            ">;"
        }
    .end annotation
.end field

.field public final label:Ljava/lang/String;

.field public maxSqlCacheSize:I

.field public openFlags:I

.field public final path:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "[\\w\\.\\-]+@[\\w\\.\\-]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 39
    sput-object v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->EMAIL_IN_DB_PATTERN:Ljava/util/regex/Pattern;

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "openFlags"    # I

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    .line 85
    if-nez p1, :cond_0

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "path must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    .line 90
    invoke-static {p1}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->stripPathForLogs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    .line 91
    iput p2, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    .line 94
    const/16 v0, 0x19

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->maxSqlCacheSize:I

    .line 95
    return-void
.end method

.method public constructor <init>(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    .locals 2
    .param p1, "other"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    .line 103
    if-nez p1, :cond_0

    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "other must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    iget-object v0, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    .line 108
    iget-object v0, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    .line 109
    invoke-virtual {p0, p1}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->updateParametersFrom(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V

    .line 110
    return-void
.end method

.method private static stripPathForLogs(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 142
    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 145
    .end local p0    # "path":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "path":Ljava/lang/String;
    :cond_0
    sget-object v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->EMAIL_IN_DB_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "XX@YY"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public isInMemoryDb()Z
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    const-string v1, ":memory:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public updateParametersFrom(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    .locals 2
    .param p1, "other"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .prologue
    .line 119
    if-nez p1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "other must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    iget-object v1, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "other configuration must refer to the same database."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_1
    iget v0, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    .line 128
    iget v0, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->maxSqlCacheSize:I

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->maxSqlCacheSize:I

    .line 129
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 130
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    iget-object v1, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 131
    return-void
.end method
