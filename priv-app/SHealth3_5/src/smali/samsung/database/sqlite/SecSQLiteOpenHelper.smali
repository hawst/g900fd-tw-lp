.class public abstract Lsamsung/database/sqlite/SecSQLiteOpenHelper;
.super Ljava/lang/Object;
.source "SecSQLiteOpenHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

.field private final mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

.field private final mFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

.field private mIsInitializing:Z

.field private final mName:Ljava/lang/String;

.field private final mNewVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p4, "version"    # I

    .prologue
    .line 61
    new-instance v5, Lsamsung/database/SecDefaultDatabaseErrorHandler;

    invoke-direct {v5}, Lsamsung/database/SecDefaultDatabaseErrorHandler;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;
    .param p4, "version"    # I
    .param p5, "errorHandler"    # Lsamsung/database/SecDatabaseErrorHandler;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v0, 0x1

    if-ge p4, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Version must be >= 1, was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mContext:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    .line 87
    iput-object p3, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .line 88
    iput p4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    .line 89
    iput-object p5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    .line 90
    return-void
.end method

.method private getDatabaseLocked(Z)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 8
    .param p1, "writable"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 150
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    if-eqz v4, :cond_0

    .line 151
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isOpen()Z

    move-result v4

    if-nez v4, :cond_1

    .line 153
    iput-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 160
    :cond_0
    iget-boolean v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    if-eqz v4, :cond_4

    .line 161
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "getDatabase called recursively"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 154
    :cond_1
    if-eqz p1, :cond_2

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnly()Z

    move-result v4

    if-nez v4, :cond_0

    .line 156
    :cond_2
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 231
    :cond_3
    :goto_0
    return-object v0

    .line 164
    :cond_4
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 166
    .local v0, "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    const/4 v4, 0x1

    :try_start_0
    iput-boolean v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    .line 168
    if-eqz v0, :cond_7

    .line 169
    if-eqz p1, :cond_5

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnly()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 170
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->reopenReadWrite()V

    .line 197
    :cond_5
    :goto_1
    invoke-virtual {p0, v0}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onConfigure(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    .line 199
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getVersion()I

    move-result v3

    .line 200
    .local v3, "version":I
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    if-eq v3, v4, :cond_c

    .line 201
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnly()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 202
    new-instance v4, Lsamsung/database/sqlite/SecSQLiteException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Can\'t upgrade read-only database from version "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 203
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getVersion()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 202
    invoke-direct {v4, v5}, Lsamsung/database/sqlite/SecSQLiteException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    .end local v3    # "version":I
    :catchall_0
    move-exception v4

    .line 233
    iput-boolean v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    .line 234
    if-eqz v0, :cond_6

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    if-eq v0, v5, :cond_6

    .line 235
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    .line 237
    :cond_6
    throw v4

    .line 172
    :cond_7
    :try_start_1
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    if-nez v4, :cond_8

    .line 173
    const/4 v4, 0x0

    invoke-static {v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->create(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 176
    :cond_8
    if-nez p1, :cond_9

    .line 177
    :try_start_2
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 178
    .local v2, "path":Ljava/lang/String;
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .line 179
    const/4 v5, 0x1

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    .line 178
    invoke-static {v2, v4, v5, v6}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    goto :goto_1

    .line 181
    .end local v2    # "path":Ljava/lang/String;
    :cond_9
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 182
    .restart local v2    # "path":Ljava/lang/String;
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .line 183
    const/high16 v5, 0x10000000

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    .line 182
    invoke-static {v2, v4, v5, v6}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_2
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto/16 :goto_1

    .line 185
    .end local v2    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 186
    .local v1, "ex":Lsamsung/database/sqlite/SecSQLiteException;
    if-eqz p1, :cond_a

    .line 187
    :try_start_3
    throw v1

    .line 189
    :cond_a
    sget-object v4, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Couldn\'t open "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 190
    const-string v6, " for writing (will try read-only):"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 189
    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 191
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 192
    .restart local v2    # "path":Ljava/lang/String;
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .line 193
    const/4 v5, 0x1

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    .line 192
    invoke-static {v2, v4, v5, v6}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    goto/16 :goto_1

    .line 206
    .end local v1    # "ex":Lsamsung/database/sqlite/SecSQLiteException;
    .end local v2    # "path":Ljava/lang/String;
    .restart local v3    # "version":I
    :cond_b
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->beginTransaction()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 208
    if-nez v3, :cond_e

    .line 209
    :try_start_4
    invoke-virtual {p0, v0}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onCreate(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    .line 217
    :goto_2
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    invoke-virtual {v0, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->setVersion(I)V

    .line 218
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 220
    :try_start_5
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->endTransaction()V

    .line 224
    :cond_c
    invoke-virtual {p0, v0}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onOpen(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    .line 226
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnly()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 227
    sget-object v4, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Opened "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in read-only mode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_d
    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 233
    iput-boolean v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    .line 234
    if-eqz v0, :cond_3

    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    if-eq v0, v4, :cond_3

    .line 235
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    goto/16 :goto_0

    .line 211
    :cond_e
    :try_start_6
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    if-le v3, v4, :cond_f

    .line 212
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    invoke-virtual {p0, v0, v3, v4}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onDowngrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 219
    :catchall_1
    move-exception v4

    .line 220
    :try_start_7
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->endTransaction()V

    .line 221
    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 214
    :cond_f
    :try_start_8
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    invoke-virtual {p0, v0, v3, v4}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onUpgrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2
.end method

.method private getDatabaseLocked(ZZ[B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 9
    .param p1, "writable"    # Z
    .param p2, "secure"    # Z
    .param p3, "password"    # [B

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 256
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    const-string v6, "getDatabaseLocked(b,b,pwd)..."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    if-eqz v5, :cond_0

    .line 258
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isOpen()Z

    move-result v5

    if-nez v5, :cond_1

    .line 260
    iput-object v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 267
    :cond_0
    iget-boolean v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    if-eqz v5, :cond_3

    .line 268
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "getDatabase called recursively"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 261
    :cond_1
    if-eqz p1, :cond_2

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnly()Z

    move-result v5

    if-nez v5, :cond_0

    .line 263
    :cond_2
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 348
    :goto_0
    return-object v0

    .line 271
    :cond_3
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 273
    .local v0, "db":Lsamsung/database/sqlite/SecSQLiteDatabase;
    const/4 v5, 0x1

    :try_start_0
    iput-boolean v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    .line 275
    if-eqz v0, :cond_7

    .line 276
    if-eqz p1, :cond_4

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnly()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 277
    if-nez p2, :cond_6

    .line 278
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->reopenReadWrite()V

    .line 315
    :cond_4
    :goto_1
    invoke-virtual {p0, v0}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onConfigure(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    .line 317
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getVersion()I

    move-result v4

    .line 318
    .local v4, "version":I
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    if-eq v4, v5, :cond_d

    .line 319
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnly()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 320
    new-instance v5, Lsamsung/database/sqlite/SecSQLiteException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Can\'t upgrade read-only database from version "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 321
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getVersion()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 320
    invoke-direct {v5, v6}, Lsamsung/database/sqlite/SecSQLiteException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    .end local v4    # "version":I
    :catchall_0
    move-exception v5

    .line 350
    iput-boolean v8, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    .line 351
    if-eqz v0, :cond_5

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    if-eq v0, v6, :cond_5

    .line 352
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    .line 354
    :cond_5
    sget-object v6, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    const-string v7, "...getDatabaseLocked(b,b,pwd)"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    throw v5

    .line 281
    :cond_6
    :try_start_1
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->reopenReadWrite()V

    .line 282
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TODO: Opening  "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in read-write mode"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 285
    :cond_7
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    if-nez v5, :cond_9

    .line 286
    if-nez p2, :cond_8

    .line 287
    const/4 v5, 0x0

    invoke-static {v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->create(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    goto/16 :goto_1

    .line 289
    :cond_8
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Creating "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in secure mode"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const/4 v5, 0x0

    invoke-static {v5, p3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->createSecureDatabase(Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;[B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto/16 :goto_1

    .line 294
    :cond_9
    if-nez p2, :cond_a

    .line 295
    :try_start_2
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 296
    .local v3, "path":Ljava/lang/String;
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    .line 297
    const/4 v6, 0x1

    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    .line 296
    invoke-static {v3, v5, v6, v7}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    goto/16 :goto_1

    .line 299
    .end local v3    # "path":Ljava/lang/String;
    :cond_a
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 300
    .restart local v3    # "path":Ljava/lang/String;
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Open "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in secure mode"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 302
    .local v1, "dbFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_b

    .line 303
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    const-string v6, "DB Directory does not exist"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 306
    :cond_b
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mFactory:Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;

    const/high16 v6, 0x10000000

    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mErrorHandler:Lsamsung/database/SecDatabaseErrorHandler;

    invoke-static {v3, v5, v6, v7, p3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->openSecureDatabase(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;ILsamsung/database/SecDatabaseErrorHandler;[B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_2
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto/16 :goto_1

    .line 308
    .end local v1    # "dbFile":Ljava/io/File;
    .end local v3    # "path":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 309
    .local v2, "ex":Lsamsung/database/sqlite/SecSQLiteException;
    if-eqz p1, :cond_4

    .line 310
    :try_start_3
    throw v2

    .line 324
    .end local v2    # "ex":Lsamsung/database/sqlite/SecSQLiteException;
    .restart local v4    # "version":I
    :cond_c
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->beginTransaction()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 326
    if-nez v4, :cond_10

    .line 327
    :try_start_4
    invoke-virtual {p0, v0}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onCreate(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    .line 335
    :goto_2
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    invoke-virtual {v0, v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->setVersion(I)V

    .line 336
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 338
    :try_start_5
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->endTransaction()V

    .line 342
    :cond_d
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isReadOnly()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 343
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Opened "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in read-only mode"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_e
    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 347
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    const-string v6, "...getDatabaseLocked(b,b,pwd)"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 350
    iput-boolean v8, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    .line 351
    if-eqz v0, :cond_f

    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    if-eq v0, v5, :cond_f

    .line 352
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    .line 354
    :cond_f
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    const-string v6, "...getDatabaseLocked(b,b,pwd)"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 329
    :cond_10
    :try_start_6
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    if-le v4, v5, :cond_11

    .line 330
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    invoke-virtual {p0, v0, v4, v5}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onDowngrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 337
    :catchall_1
    move-exception v5

    .line 338
    :try_start_7
    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->endTransaction()V

    .line 339
    throw v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 332
    :cond_11
    :try_start_8
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mNewVersion:I

    invoke-virtual {p0, v0, v4, v5}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->onUpgrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 362
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mIsInitializing:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Closed during initialization"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 364
    :cond_0
    :try_start_1
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->close()V

    .line 366
    const/4 v0, 0x0

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public getDatabaseName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getReadableDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1

    .prologue
    .line 144
    monitor-enter p0

    .line 145
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->getDatabaseLocked(Z)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getReadableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 2
    .param p1, "password"    # [B

    .prologue
    .line 248
    monitor-enter p0

    .line 249
    :try_start_0
    sget-object v0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    const-string v1, "getReadableDatabase(pwd)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p1}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->getDatabaseLocked(ZZ[B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getWritableDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    .line 121
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->getDatabaseLocked(Z)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getWritableDatabase([B)Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 2
    .param p1, "password"    # [B

    .prologue
    .line 241
    monitor-enter p0

    .line 242
    :try_start_0
    sget-object v0, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->TAG:Ljava/lang/String;

    const-string v1, "getWritableDatabase(pwd)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p1}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;->getDatabaseLocked(ZZ[B)Lsamsung/database/sqlite/SecSQLiteDatabase;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onConfigure(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 387
    return-void
.end method

.method public abstract onCreate(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
.end method

.method public onDowngrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 436
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t downgrade database from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 437
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 436
    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onOpen(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 453
    return-void
.end method

.method public abstract onUpgrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
.end method
