.class public final Lsamsung/database/sqlite/SecSQLiteDebug;
.super Ljava/lang/Object;
.source "SecSQLiteDebug.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;,
        Lsamsung/database/sqlite/SecSQLiteDebug$PagerStats;
    }
.end annotation


# static fields
.field public static final DEBUG_LOG_SLOW_QUERIES:Z

.field public static final DEBUG_SQL_LOG:Z

.field public static final DEBUG_SQL_STATEMENTS:Z

.field public static final DEBUG_SQL_TIME:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 38
    const-string v0, "SQLiteLog"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 37
    sput-boolean v0, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_SQL_LOG:Z

    .line 46
    const-string v0, "SecSQLiteStatements"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 45
    sput-boolean v0, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_SQL_STATEMENTS:Z

    .line 55
    const-string v0, "SQLiteTime"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 54
    sput-boolean v0, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_SQL_TIME:Z

    .line 63
    const-string v0, "SQLiteSlowQuery"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 62
    sput-boolean v0, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_LOG_SLOW_QUERIES:Z

    .line 29
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method

.method public static dump(Landroid/util/Printer;[Ljava/lang/String;)V
    .locals 5
    .param p0, "printer"    # Landroid/util/Printer;
    .param p1, "args"    # [Ljava/lang/String;

    .prologue
    .line 163
    const/4 v1, 0x0

    .line 164
    .local v1, "verbose":Z
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 170
    invoke-static {p0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->dumpAll(Landroid/util/Printer;Z)V

    .line 171
    return-void

    .line 164
    :cond_0
    aget-object v0, p1, v2

    .line 165
    .local v0, "arg":Ljava/lang/String;
    const-string v4, "-v"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 166
    const/4 v1, 0x1

    .line 164
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getDatabaseInfo()Lsamsung/database/sqlite/SecSQLiteDebug$PagerStats;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDebug$PagerStats;

    invoke-direct {v0}, Lsamsung/database/sqlite/SecSQLiteDebug$PagerStats;-><init>()V

    .line 152
    .local v0, "stats":Lsamsung/database/sqlite/SecSQLiteDebug$PagerStats;
    invoke-static {v0}, Lsamsung/database/sqlite/SecSQLiteDebug;->nativeGetPagerStats(Lsamsung/database/sqlite/SecSQLiteDebug$PagerStats;)V

    .line 153
    invoke-static {}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getDbStats()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lsamsung/database/sqlite/SecSQLiteDebug$PagerStats;->dbStats:Ljava/util/ArrayList;

    .line 154
    return-object v0
.end method

.method private static native nativeGetPagerStats(Lsamsung/database/sqlite/SecSQLiteDebug$PagerStats;)V
.end method

.method public static final shouldLogSlowQuery(J)Z
    .locals 3
    .param p0, "elapsedTimeMillis"    # J

    .prologue
    .line 82
    const/16 v0, 0xc8

    .line 83
    .local v0, "slowQueryMillis":I
    if-ltz v0, :cond_0

    int-to-long v1, v0

    cmp-long v1, p0, v1

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
