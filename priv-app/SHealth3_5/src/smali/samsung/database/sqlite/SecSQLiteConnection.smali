.class public final Lsamsung/database/sqlite/SecSQLiteConnection;
.super Ljava/lang/Object;
.source "SecSQLiteConnection.java"

# interfaces
.implements Landroid/os/CancellationSignal$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsamsung/database/sqlite/SecSQLiteConnection$Operation;,
        Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;,
        Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;,
        Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final EMPTY_BYTE_ARRAY:[B

.field private static final EMPTY_STRING_ARRAY:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SecSQLiteConnection"

.field private static final TRIM_SQL_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private mCancellationSignalAttachCount:I

.field private final mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

.field private final mConnectionId:I

.field private mConnectionPtr:I

.field private final mIsPrimaryConnection:Z

.field private final mIsReadOnlyConnection:Z

.field private mOnlyAllowReadOnlyOperations:Z

.field private final mPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

.field private final mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

.field private mPreparedStatementPool:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

.field private final mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lsamsung/database/sqlite/SecSQLiteConnection;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    .line 94
    new-array v0, v1, [B

    sput-object v0, Lsamsung/database/sqlite/SecSQLiteConnection;->EMPTY_BYTE_ARRAY:[B

    .line 96
    const-string v0, "[\\s]*\\n+[\\s]*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lsamsung/database/sqlite/SecSQLiteConnection;->TRIM_SQL_PATTERN:Ljava/util/regex/Pattern;

    .line 89
    return-void
.end method

.method private constructor <init>(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;IZ)V
    .locals 2
    .param p1, "pool"    # Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    .param p2, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;
    .param p3, "connectionId"    # I
    .param p4, "primaryConnection"    # Z

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;-><init>(Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;)V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    .line 172
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    .line 173
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-direct {v0, p2}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .line 174
    iput p3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionId:I

    .line 175
    iput-boolean p4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mIsPrimaryConnection:Z

    .line 176
    iget v0, p2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mIsReadOnlyConnection:Z

    .line 177
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    .line 178
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v1, v1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->maxSqlCacheSize:I

    invoke-direct {v0, p0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;-><init>(Lsamsung/database/sqlite/SecSQLiteConnection;I)V

    .line 177
    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    .line 179
    return-void

    .line 176
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$0(Lsamsung/database/sqlite/SecSQLiteConnection;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    .locals 0

    .prologue
    .line 804
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->finalizePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    return-void
.end method

.method static synthetic access$1(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1044
    invoke-static {p0}, Lsamsung/database/sqlite/SecSQLiteConnection;->trimSqlForDisplay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2()[B
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lsamsung/database/sqlite/SecSQLiteConnection;->EMPTY_BYTE_ARRAY:[B

    return-object v0
.end method

.method private acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    .locals 11
    .param p1, "sql"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 747
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .line 748
    .local v8, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    const/4 v7, 0x0

    .line 749
    .local v7, "skipCache":Z
    if-eqz v8, :cond_1

    .line 750
    iget-boolean v0, v8, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInUse:Z

    if-nez v0, :cond_0

    move-object v9, v8

    .line 778
    .end local v8    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    .local v9, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :goto_0
    return-object v9

    .line 756
    .end local v9    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    .restart local v8    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :cond_0
    const/4 v7, 0x1

    .line 759
    :cond_1
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativePrepareStatement(ILjava/lang/String;)I

    move-result v2

    .line 761
    .local v2, "statementPtr":I
    :try_start_0
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeGetParameterCount(II)I

    move-result v3

    .line 762
    .local v3, "numParameters":I
    invoke-static {p1}, Lsamsung/database/SecDatabaseUtils;->getSqlStatementType(Ljava/lang/String;)I

    move-result v4

    .line 763
    .local v4, "type":I
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeIsReadOnly(II)Z

    move-result v5

    .local v5, "readOnly":Z
    move-object v0, p0

    move-object v1, p1

    .line 764
    invoke-direct/range {v0 .. v5}, Lsamsung/database/sqlite/SecSQLiteConnection;->obtainPreparedStatement(Ljava/lang/String;IIIZ)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    move-result-object v8

    .line 765
    if-nez v7, :cond_2

    invoke-static {v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->isCacheable(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 766
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    invoke-virtual {v0, p1, v8}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 767
    const/4 v0, 0x1

    iput-boolean v0, v8, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInCache:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 777
    :cond_2
    iput-boolean v10, v8, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInUse:Z

    move-object v9, v8

    .line 778
    .end local v8    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    .restart local v9    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    goto :goto_0

    .line 769
    .end local v3    # "numParameters":I
    .end local v4    # "type":I
    .end local v5    # "readOnly":Z
    .end local v9    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    .restart local v8    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v6

    .line 772
    .local v6, "ex":Ljava/lang/RuntimeException;
    if-eqz v8, :cond_3

    iget-boolean v0, v8, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInCache:Z

    if-nez v0, :cond_4

    .line 773
    :cond_3
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeFinalizeStatement(II)V

    .line 775
    :cond_4
    throw v6
.end method

.method private applyBlockGuardPolicy(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    .locals 0
    .param p1, "statement"    # Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .prologue
    .line 885
    return-void
.end method

.method private attachCancellationSignal(Landroid/os/CancellationSignal;)V
    .locals 0
    .param p1, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 810
    return-void
.end method

.method private bindArguments(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;[Ljava/lang/Object;)V
    .locals 10
    .param p1, "statement"    # Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    .param p2, "bindArgs"    # [Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 826
    if-eqz p2, :cond_0

    array-length v1, p2

    .line 827
    .local v1, "count":I
    :goto_0
    iget v5, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mNumParameters:I

    if-eq v1, v5, :cond_1

    .line 828
    new-instance v4, Lsamsung/database/sqlite/SecSQLiteBindOrColumnIndexOutOfRangeException;

    .line 829
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Expected "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mNumParameters:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bind arguments but "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 830
    array-length v6, p2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " were provided."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 829
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 828
    invoke-direct {v4, v5}, Lsamsung/database/sqlite/SecSQLiteBindOrColumnIndexOutOfRangeException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v1    # "count":I
    :cond_0
    move v1, v4

    .line 826
    goto :goto_0

    .line 832
    .restart local v1    # "count":I
    :cond_1
    if-nez v1, :cond_3

    .line 867
    :cond_2
    return-void

    .line 836
    :cond_3
    iget v3, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    .line 837
    .local v3, "statementPtr":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_2

    .line 838
    aget-object v0, p2, v2

    .line 839
    .local v0, "arg":Ljava/lang/Object;
    invoke-static {v0}, Lsamsung/database/SecDatabaseUtils;->getTypeOfObject(Ljava/lang/Object;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 856
    :pswitch_0
    instance-of v5, v0, Ljava/lang/Boolean;

    if-eqz v5, :cond_5

    .line 859
    iget v6, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    add-int/lit8 v7, v2, 0x1

    .line 860
    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "arg":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    int-to-long v8, v5

    .line 859
    invoke-static {v6, v3, v7, v8, v9}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeBindLong(IIIJ)V

    .line 837
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 841
    .restart local v0    # "arg":Ljava/lang/Object;
    :pswitch_1
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    add-int/lit8 v6, v2, 0x1

    invoke-static {v5, v3, v6}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeBindNull(III)V

    goto :goto_3

    .line 844
    :pswitch_2
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    add-int/lit8 v6, v2, 0x1

    .line 845
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "arg":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v7

    .line 844
    invoke-static {v5, v3, v6, v7, v8}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeBindLong(IIIJ)V

    goto :goto_3

    .line 848
    .restart local v0    # "arg":Ljava/lang/Object;
    :pswitch_3
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    add-int/lit8 v6, v2, 0x1

    .line 849
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "arg":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v7

    .line 848
    invoke-static {v5, v3, v6, v7, v8}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeBindDouble(IIID)V

    goto :goto_3

    .line 852
    .restart local v0    # "arg":Ljava/lang/Object;
    :pswitch_4
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    add-int/lit8 v6, v2, 0x1

    check-cast v0, [B

    .end local v0    # "arg":Ljava/lang/Object;
    invoke-static {v5, v3, v6, v0}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeBindBlob(III[B)V

    goto :goto_3

    :cond_4
    move v5, v4

    .line 860
    goto :goto_2

    .line 862
    .restart local v0    # "arg":Ljava/lang/Object;
    :cond_5
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v3, v6, v7}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeBindString(IIILjava/lang/String;)V

    goto :goto_3

    .line 839
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private detachCancellationSignal(Landroid/os/CancellationSignal;)V
    .locals 0
    .param p1, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 813
    return-void
.end method

.method private dispose(Z)V
    .locals 4
    .param p1, "finalized"    # Z

    .prologue
    const/4 v3, 0x0

    .line 250
    iget v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    if-eqz v1, :cond_0

    .line 251
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string v2, "close"

    invoke-virtual {v1, v2, v3, v3}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 253
    .local v0, "cookie":I
    :try_start_0
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    invoke-virtual {v1}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->evictAll()V

    .line 254
    iget v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeClose(I)V

    .line 255
    const/4 v1, 0x0

    iput v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v1, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 260
    .end local v0    # "cookie":I
    :cond_0
    return-void

    .line 256
    .restart local v0    # "cookie":I
    :catchall_0
    move-exception v1

    .line 257
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v2, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 258
    throw v1
.end method

.method private finalizePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    .locals 2
    .param p1, "statement"    # Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .prologue
    .line 805
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v1, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    invoke-static {v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeFinalizeStatement(II)V

    .line 806
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->recyclePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 807
    return-void
.end method

.method private getMainDbStatsUnsafe(IJJ)Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;
    .locals 10
    .param p1, "lookaside"    # I
    .param p2, "pageCount"    # J
    .param p4, "pageSize"    # J

    .prologue
    .line 1005
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v1, v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    .line 1006
    .local v1, "label":Ljava/lang/String;
    iget-boolean v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mIsPrimaryConnection:Z

    if-nez v0, :cond_0

    .line 1007
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1009
    :cond_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;

    .line 1010
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    invoke-virtual {v2}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->hitCount()I

    move-result v7

    .line 1011
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    invoke-virtual {v2}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->missCount()I

    move-result v8

    .line 1012
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    invoke-virtual {v2}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->size()I

    move-result v9

    move-wide v2, p2

    move-wide v4, p4

    move v6, p1

    .line 1009
    invoke-direct/range {v0 .. v9}, Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;-><init>(Ljava/lang/String;JJIIII)V

    return-object v0
.end method

.method private static isCacheable(I)Z
    .locals 2
    .param p0, "statementType"    # I

    .prologue
    const/4 v0, 0x1

    .line 877
    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    .line 878
    if-ne p0, v0, :cond_1

    .line 881
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native nativeBindBlob(III[B)V
.end method

.method private static native nativeBindDouble(IIID)V
.end method

.method private static native nativeBindLong(IIIJ)V
.end method

.method private static native nativeBindNull(III)V
.end method

.method private static native nativeBindString(IIILjava/lang/String;)V
.end method

.method private static native nativeCancel(I)V
.end method

.method private static native nativeChangePassword(I[B)V
.end method

.method private static native nativeClose(I)V
.end method

.method private static native nativeExecute(II)V
.end method

.method private static native nativeExecuteForBlobFileDescriptor(II)I
.end method

.method private static native nativeExecuteForChangedRowCount(II)I
.end method

.method private static native nativeExecuteForCursorWindow(IIIIIZ)J
.end method

.method private static native nativeExecuteForLastInsertedRowId(II)J
.end method

.method private static native nativeExecuteForLong(II)J
.end method

.method private static native nativeExecuteForString(II)Ljava/lang/String;
.end method

.method private static native nativeExportDB(ILjava/lang/String;)V
.end method

.method private static native nativeFinalizeStatement(II)V
.end method

.method private static native nativeGetColumnCount(II)I
.end method

.method private static native nativeGetColumnName(III)Ljava/lang/String;
.end method

.method private static native nativeGetDbLookaside(I)I
.end method

.method private static native nativeGetParameterCount(II)I
.end method

.method private static native nativeIsReadOnly(II)Z
.end method

.method private static native nativeOpen(Ljava/lang/String;ILjava/lang/String;ZZ)I
.end method

.method private static native nativePrepareStatement(ILjava/lang/String;)I
.end method

.method private static native nativeRegisterCustomFunction(ILsamsung/database/sqlite/SecSQLiteCustomFunction;)V
.end method

.method private static native nativeRegisterLocalizedCollators(ILjava/lang/String;)V
.end method

.method private static native nativeResetCancel(IZ)V
.end method

.method private static native nativeResetStatementAndClearBindings(II)V
.end method

.method private static native nativeSetPassword(I[B)V
.end method

.method public static native native_backup(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private obtainPreparedStatement(Ljava/lang/String;IIIZ)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    .locals 3
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "statementPtr"    # I
    .param p3, "numParameters"    # I
    .param p4, "type"    # I
    .param p5, "readOnly"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1022
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementPool:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .line 1023
    .local v0, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    if-eqz v0, :cond_0

    .line 1024
    iget-object v1, v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mPoolNext:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    iput-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementPool:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .line 1025
    iput-object v2, v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mPoolNext:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .line 1026
    const/4 v1, 0x0

    iput-boolean v1, v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInCache:Z

    .line 1030
    :goto_0
    iput-object p1, v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mSql:Ljava/lang/String;

    .line 1031
    iput p2, v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    .line 1032
    iput p3, v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mNumParameters:I

    .line 1033
    iput p4, v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mType:I

    .line 1034
    iput-boolean p5, v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mReadOnly:Z

    .line 1035
    return-object v0

    .line 1028
    :cond_0
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .end local v0    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    invoke-direct {v0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;-><init>(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .restart local v0    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    goto :goto_0
.end method

.method static open(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;IZ)Lsamsung/database/sqlite/SecSQLiteConnection;
    .locals 3
    .param p0, "pool"    # Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    .param p1, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;
    .param p2, "connectionId"    # I
    .param p3, "primaryConnection"    # Z

    .prologue
    .line 198
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-direct {v0, p0, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;-><init>(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;IZ)V

    .line 201
    .local v0, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :try_start_0
    invoke-direct {v0}, Lsamsung/database/sqlite/SecSQLiteConnection;->open()V
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    return-object v0

    .line 203
    :catch_0
    move-exception v1

    .line 204
    .local v1, "ex":Lsamsung/database/sqlite/SecSQLiteException;
    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->dispose(Z)V

    .line 205
    throw v1
.end method

.method private open()V
    .locals 5

    .prologue
    .line 240
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v1, v1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    .line 241
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v2, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    .line 242
    sget-boolean v3, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_SQL_STATEMENTS:Z

    sget-boolean v4, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_SQL_TIME:Z

    .line 240
    invoke-static {v0, v1, v2, v3, v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeOpen(Ljava/lang/String;ILjava/lang/String;ZZ)I

    move-result v0

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    .line 244
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    if-nez v0, :cond_0

    .line 247
    :cond_0
    return-void
.end method

.method private open([B)V
    .locals 5
    .param p1, "password"    # [B

    .prologue
    .line 233
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v0, v0, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget v1, v1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->openFlags:I

    .line 234
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v2, v2, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->label:Ljava/lang/String;

    .line 235
    sget-boolean v3, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_SQL_STATEMENTS:Z

    sget-boolean v4, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_SQL_TIME:Z

    .line 233
    invoke-static {v0, v1, v2, v3, v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeOpen(Ljava/lang/String;ILjava/lang/String;ZZ)I

    move-result v0

    iput v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    .line 236
    return-void
.end method

.method static openSecure(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;IZ[B)Lsamsung/database/sqlite/SecSQLiteConnection;
    .locals 3
    .param p0, "pool"    # Lsamsung/database/sqlite/SecSQLiteConnectionPool;
    .param p1, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;
    .param p2, "connectionId"    # I
    .param p3, "primaryConnection"    # Z
    .param p4, "password"    # [B

    .prologue
    .line 214
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteConnection;

    invoke-direct {v0, p0, p1, p2, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;-><init>(Lsamsung/database/sqlite/SecSQLiteConnectionPool;Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;IZ)V

    .line 217
    .local v0, "connection":Lsamsung/database/sqlite/SecSQLiteConnection;
    :try_start_0
    invoke-direct {v0, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->open([B)V

    .line 218
    invoke-virtual {v0, p4}, Lsamsung/database/sqlite/SecSQLiteConnection;->setPassword([B)V
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    return-object v0

    .line 220
    :catch_0
    move-exception v1

    .line 221
    .local v1, "ex":Lsamsung/database/sqlite/SecSQLiteException;
    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->dispose(Z)V

    .line 222
    throw v1
.end method

.method private recyclePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    .locals 1
    .param p1, "statement"    # Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .prologue
    .line 1039
    const/4 v0, 0x0

    iput-object v0, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mSql:Ljava/lang/String;

    .line 1040
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementPool:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    iput-object v0, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mPoolNext:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .line 1041
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementPool:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .line 1042
    return-void
.end method

.method private releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    .locals 3
    .param p1, "statement"    # Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .prologue
    .line 782
    const/4 v1, 0x0

    iput-boolean v1, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInUse:Z

    .line 783
    iget-boolean v1, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInCache:Z

    if-eqz v1, :cond_0

    .line 785
    :try_start_0
    iget v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v2, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    invoke-static {v1, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeResetStatementAndClearBindings(II)V
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 802
    :goto_0
    return-void

    .line 786
    :catch_0
    move-exception v0

    .line 797
    .local v0, "ex":Lsamsung/database/sqlite/SecSQLiteException;
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    iget-object v2, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mSql:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 800
    .end local v0    # "ex":Lsamsung/database/sqlite/SecSQLiteException;
    :cond_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->finalizePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    goto :goto_0
.end method

.method private throwIfStatementForbidden(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    .locals 2
    .param p1, "statement"    # Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .prologue
    .line 870
    iget-boolean v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mOnlyAllowReadOnlyOperations:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mReadOnly:Z

    if-nez v0, :cond_0

    .line 871
    new-instance v0, Lsamsung/database/sqlite/SecSQLiteException;

    const-string v1, "Cannot execute this statement because it might modify the database but the connection is read-only."

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 874
    :cond_0
    return-void
.end method

.method private static trimSqlForDisplay(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "sql"    # Ljava/lang/String;

    .prologue
    .line 1045
    sget-object v0, Lsamsung/database/sqlite/SecSQLiteConnection;->TRIM_SQL_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public changePassword([B)V
    .locals 1
    .param p1, "newPassword"    # [B

    .prologue
    .line 732
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeChangePassword(I[B)V

    .line 733
    return-void
.end method

.method close()V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteConnection;->dispose(Z)V

    .line 230
    return-void
.end method

.method collectDbStats(Ljava/util/ArrayList;)V
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 951
    .local p1, "dbStatsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;>;"
    move-object/from16 v0, p0

    iget v1, v0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeGetDbLookaside(I)I

    move-result v2

    .line 952
    .local v2, "lookaside":I
    const-wide/16 v3, 0x0

    .line 953
    .local v3, "pageCount":J
    const-wide/16 v5, 0x0

    .line 955
    .local v5, "pageSize":J
    :try_start_0
    const-string v1, "PRAGMA page_count;"

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v7, v8}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForLong(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J

    move-result-wide v3

    .line 956
    const-string v1, "PRAGMA page_size;"

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v7, v8}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForLong(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v5

    :goto_0
    move-object/from16 v1, p0

    .line 960
    invoke-direct/range {v1 .. v6}, Lsamsung/database/sqlite/SecSQLiteConnection;->getMainDbStatsUnsafe(IJJ)Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 965
    new-instance v10, Lsamsung/database/SecCursorWindow;

    const-string v1, "collectDbStats"

    invoke-direct {v10, v1}, Lsamsung/database/SecCursorWindow;-><init>(Ljava/lang/String;)V

    .line 967
    .local v10, "window":Lsamsung/database/SecCursorWindow;
    :try_start_1
    const-string v8, "PRAGMA database_list;"

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v7, p0

    invoke-virtual/range {v7 .. v14}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForCursorWindow(Ljava/lang/String;[Ljava/lang/Object;Lsamsung/database/SecCursorWindow;IIZLandroid/os/CancellationSignal;)I

    .line 968
    const/16 v21, 0x1

    .local v21, "i":I
    :goto_1
    invoke-virtual {v10}, Lsamsung/database/SecCursorWindow;->getNumRows()I
    :try_end_1
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    move/from16 v0, v21

    if-lt v0, v1, :cond_0

    .line 988
    invoke-virtual {v10}, Lsamsung/database/SecCursorWindow;->close()V

    .line 990
    .end local v21    # "i":I
    :goto_2
    return-void

    .line 969
    .restart local v21    # "i":I
    :cond_0
    const/4 v1, 0x1

    :try_start_2
    move/from16 v0, v21

    invoke-virtual {v10, v0, v1}, Lsamsung/database/SecCursorWindow;->getString(II)Ljava/lang/String;

    move-result-object v22

    .line 970
    .local v22, "name":Ljava/lang/String;
    const/4 v1, 0x2

    move/from16 v0, v21

    invoke-virtual {v10, v0, v1}, Lsamsung/database/SecCursorWindow;->getString(II)Ljava/lang/String;
    :try_end_2
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v23

    .line 971
    .local v23, "path":Ljava/lang/String;
    const-wide/16 v3, 0x0

    .line 972
    const-wide/16 v5, 0x0

    .line 974
    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "PRAGMA "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ".page_count;"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v7, v8}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForLong(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J

    move-result-wide v3

    .line 975
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "PRAGMA "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ".page_size;"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v7, v8}, Lsamsung/database/sqlite/SecSQLiteConnection;->executeForLong(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
    :try_end_3
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-wide v5

    .line 979
    :goto_3
    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "  (attached) "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 980
    .local v12, "label":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 981
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ": "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 983
    :cond_1
    new-instance v11, Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-wide v13, v3

    move-wide v15, v5

    invoke-direct/range {v11 .. v20}, Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;-><init>(Ljava/lang/String;JJIIII)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lsamsung/database/sqlite/SecSQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 968
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_1

    .line 985
    .end local v12    # "label":Ljava/lang/String;
    .end local v21    # "i":I
    .end local v22    # "name":Ljava/lang/String;
    .end local v23    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 988
    invoke-virtual {v10}, Lsamsung/database/SecCursorWindow;->close()V

    goto/16 :goto_2

    .line 987
    :catchall_0
    move-exception v1

    .line 988
    invoke-virtual {v10}, Lsamsung/database/SecCursorWindow;->close()V

    .line 989
    throw v1

    .line 976
    .restart local v21    # "i":I
    .restart local v22    # "name":Ljava/lang/String;
    .restart local v23    # "path":Ljava/lang/String;
    :catch_1
    move-exception v1

    goto :goto_3

    .line 957
    .end local v10    # "window":Lsamsung/database/SecCursorWindow;
    .end local v21    # "i":I
    .end local v22    # "name":Ljava/lang/String;
    .end local v23    # "path":Ljava/lang/String;
    :catch_2
    move-exception v1

    goto/16 :goto_0
.end method

.method collectDbStatsUnsafe(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "dbStatsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;>;"
    const-wide/16 v2, 0x0

    .line 999
    const/4 v1, 0x0

    move-object v0, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lsamsung/database/sqlite/SecSQLiteConnection;->getMainDbStatsUnsafe(IJJ)Lsamsung/database/sqlite/SecSQLiteDebug$DbStats;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1000
    return-void
.end method

.method describeCurrentOperationUnsafe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->describeCurrentOperation()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public dump(Landroid/util/Printer;Z)V
    .locals 0
    .param p1, "printer"    # Landroid/util/Printer;
    .param p2, "verbose"    # Z

    .prologue
    .line 894
    invoke-virtual {p0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnection;->dumpUnsafe(Landroid/util/Printer;Z)V

    .line 895
    return-void
.end method

.method dumpUnsafe(Landroid/util/Printer;Z)V
    .locals 2
    .param p1, "printer"    # Landroid/util/Printer;
    .param p2, "verbose"    # Z

    .prologue
    .line 912
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Connection #"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 913
    if-eqz p2, :cond_0

    .line 914
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  connectionPtr: 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 916
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  isPrimaryConnection: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mIsPrimaryConnection:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 917
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  onlyAllowReadOnlyOperations: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mOnlyAllowReadOnlyOperations:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 919
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->dump(Landroid/util/Printer;)V

    .line 921
    if-eqz p2, :cond_1

    .line 922
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->dump(Landroid/util/Printer;)V

    .line 924
    :cond_1
    return-void
.end method

.method public execute(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
    .locals 5
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 382
    if-nez p1, :cond_0

    .line 383
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "sql must not be null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 386
    :cond_0
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string v4, "execute"

    invoke-virtual {v3, v4, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 388
    .local v0, "cookie":I
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 390
    .local v2, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_start_1
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->throwIfStatementForbidden(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 391
    invoke-direct {p0, v2, p2}, Lsamsung/database/sqlite/SecSQLiteConnection;->bindArguments(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;[Ljava/lang/Object;)V

    .line 392
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->applyBlockGuardPolicy(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 393
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->attachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 395
    :try_start_2
    iget v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v4, v2, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    invoke-static {v3, v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeExecute(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 397
    :try_start_3
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 400
    :try_start_4
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 406
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v3, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 408
    return-void

    .line 396
    :catchall_0
    move-exception v3

    .line 397
    :try_start_5
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V

    .line 398
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 399
    :catchall_1
    move-exception v3

    .line 400
    :try_start_6
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 401
    throw v3
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 402
    .end local v2    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v1

    .line 403
    .local v1, "ex":Ljava/lang/RuntimeException;
    :try_start_7
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v3, v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->failOperation(ILjava/lang/Exception;)V

    .line 404
    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 405
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_2
    move-exception v3

    .line 406
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v4, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 407
    throw v3
.end method

.method public executeForBlobFileDescriptor(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    .locals 6
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 509
    if-nez p1, :cond_0

    .line 510
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "sql must not be null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 513
    :cond_0
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string v5, "executeForBlobFileDescriptor"

    invoke-virtual {v4, v5, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 516
    .local v0, "cookie":I
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v3

    .line 518
    .local v3, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_start_1
    invoke-direct {p0, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->throwIfStatementForbidden(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 519
    invoke-direct {p0, v3, p2}, Lsamsung/database/sqlite/SecSQLiteConnection;->bindArguments(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;[Ljava/lang/Object;)V

    .line 520
    invoke-direct {p0, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->applyBlockGuardPolicy(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 521
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->attachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 524
    :try_start_2
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v5, v3, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    .line 523
    invoke-static {v4, v5}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeExecuteForBlobFileDescriptor(II)I

    move-result v2

    .line 525
    .local v2, "fd":I
    if-ltz v2, :cond_1

    invoke-static {v2}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 527
    :goto_0
    :try_start_3
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 530
    :try_start_4
    invoke-direct {p0, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 536
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v5, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 525
    return-object v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 526
    .end local v2    # "fd":I
    :catchall_0
    move-exception v4

    .line 527
    :try_start_5
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V

    .line 528
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 529
    :catchall_1
    move-exception v4

    .line 530
    :try_start_6
    invoke-direct {p0, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 531
    throw v4
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 532
    .end local v3    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v1

    .line 533
    .local v1, "ex":Ljava/lang/RuntimeException;
    :try_start_7
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v4, v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->failOperation(ILjava/lang/Exception;)V

    .line 534
    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 535
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_2
    move-exception v4

    .line 536
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v5, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 537
    throw v4
.end method

.method public executeForChangedRowCount(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)I
    .locals 8
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 555
    if-nez p1, :cond_0

    .line 556
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "sql must not be null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 559
    :cond_0
    const/4 v0, 0x0

    .line 560
    .local v0, "changedRows":I
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string v5, "executeForChangedRowCount"

    invoke-virtual {v4, v5, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v1

    .line 563
    .local v1, "cookie":I
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v3

    .line 565
    .local v3, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_start_1
    invoke-direct {p0, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->throwIfStatementForbidden(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 566
    invoke-direct {p0, v3, p2}, Lsamsung/database/sqlite/SecSQLiteConnection;->bindArguments(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;[Ljava/lang/Object;)V

    .line 567
    invoke-direct {p0, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->applyBlockGuardPolicy(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 568
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->attachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 571
    :try_start_2
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v5, v3, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    .line 570
    invoke-static {v4, v5}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeExecuteForChangedRowCount(II)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 574
    :try_start_3
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 577
    :try_start_4
    invoke-direct {p0, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 583
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v4, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperationDeferLog(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 584
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "changedRows="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->logOperation(ILjava/lang/String;)V

    .line 572
    :cond_1
    return v0

    .line 573
    :catchall_0
    move-exception v4

    .line 574
    :try_start_5
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V

    .line 575
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 576
    :catchall_1
    move-exception v4

    .line 577
    :try_start_6
    invoke-direct {p0, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 578
    throw v4
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 579
    .end local v3    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v2

    .line 580
    .local v2, "ex":Ljava/lang/RuntimeException;
    :try_start_7
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v4, v1, v2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->failOperation(ILjava/lang/Exception;)V

    .line 581
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 582
    .end local v2    # "ex":Ljava/lang/RuntimeException;
    :catchall_2
    move-exception v4

    .line 583
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v5, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperationDeferLog(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 584
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "changedRows="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->logOperation(ILjava/lang/String;)V

    .line 586
    :cond_2
    throw v4
.end method

.method public executeForCursorWindow(Ljava/lang/String;[Ljava/lang/Object;Lsamsung/database/SecCursorWindow;IIZLandroid/os/CancellationSignal;)I
    .locals 16
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "window"    # Lsamsung/database/SecCursorWindow;
    .param p4, "startPos"    # I
    .param p5, "requiredPos"    # I
    .param p6, "countAllRows"    # Z
    .param p7, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 659
    if-nez p1, :cond_0

    .line 660
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "sql must not be null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 662
    :cond_0
    if-nez p3, :cond_1

    .line 663
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "window must not be null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 666
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lsamsung/database/SecCursorWindow;->acquireReference()V

    .line 668
    const/4 v8, -0x1

    .line 669
    .local v8, "actualPos":I
    const/4 v10, -0x1

    .line 670
    .local v10, "countedRows":I
    const/4 v12, -0x1

    .line 671
    .local v12, "filledRows":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string v3, "executeForCursorWindow"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v3, v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-result v9

    .line 674
    .local v9, "cookie":I
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v15

    .line 676
    .local v15, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_start_2
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lsamsung/database/sqlite/SecSQLiteConnection;->throwIfStatementForbidden(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 677
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v15, v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->bindArguments(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;[Ljava/lang/Object;)V

    .line 678
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lsamsung/database/sqlite/SecSQLiteConnection;->applyBlockGuardPolicy(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 679
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->attachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 682
    :try_start_3
    move-object/from16 v0, p0

    iget v2, v0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v3, v15, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    move-object/from16 v0, p3

    iget v4, v0, Lsamsung/database/SecCursorWindow;->mWindowPtr:I

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    .line 681
    invoke-static/range {v2 .. v7}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeExecuteForCursorWindow(IIIIIZ)J

    move-result-wide v13

    .line 684
    .local v13, "result":J
    const/16 v2, 0x20

    shr-long v2, v13, v2

    long-to-int v8, v2

    .line 685
    long-to-int v10, v13

    .line 686
    invoke-virtual/range {p3 .. p3}, Lsamsung/database/SecCursorWindow;->getNumRows()I

    move-result v12

    .line 687
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Lsamsung/database/SecCursorWindow;->setStartPosition(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 690
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 693
    :try_start_5
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 699
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v2, v9}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperationDeferLog(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 700
    move-object/from16 v0, p0

    iget-object v2, v0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "window=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 701
    const-string v4, "\', startPos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 702
    const-string v4, ", actualPos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 703
    const-string v4, ", filledRows="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 704
    const-string v4, ", countedRows="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 700
    invoke-virtual {v2, v9, v3}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->logOperation(ILjava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 708
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 688
    return v10

    .line 689
    .end local v13    # "result":J
    :catchall_0
    move-exception v2

    .line 690
    :try_start_7
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V

    .line 691
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 692
    :catchall_1
    move-exception v2

    .line 693
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 694
    throw v2
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 695
    .end local v15    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v11

    .line 696
    .local v11, "ex":Ljava/lang/RuntimeException;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v2, v9, v11}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->failOperation(ILjava/lang/Exception;)V

    .line 697
    throw v11
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 698
    .end local v11    # "ex":Ljava/lang/RuntimeException;
    :catchall_2
    move-exception v2

    .line 699
    :try_start_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v3, v9}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperationDeferLog(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 700
    move-object/from16 v0, p0

    iget-object v3, v0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "window=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 701
    const-string v5, "\', startPos="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 702
    const-string v5, ", actualPos="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 703
    const-string v5, ", filledRows="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 704
    const-string v5, ", countedRows="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 700
    invoke-virtual {v3, v9, v4}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->logOperation(ILjava/lang/String;)V

    .line 706
    :cond_3
    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 707
    .end local v9    # "cookie":I
    :catchall_3
    move-exception v2

    .line 708
    invoke-virtual/range {p3 .. p3}, Lsamsung/database/SecCursorWindow;->releaseReference()V

    .line 709
    throw v2
.end method

.method public executeForLastInsertedRowId(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
    .locals 6
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 604
    if-nez p1, :cond_0

    .line 605
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "sql must not be null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 608
    :cond_0
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string v4, "executeForLastInsertedRowId"

    invoke-virtual {v3, v4, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 611
    .local v0, "cookie":I
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 613
    .local v2, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_start_1
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->throwIfStatementForbidden(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 614
    invoke-direct {p0, v2, p2}, Lsamsung/database/sqlite/SecSQLiteConnection;->bindArguments(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;[Ljava/lang/Object;)V

    .line 615
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->applyBlockGuardPolicy(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 616
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->attachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 619
    :try_start_2
    iget v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v4, v2, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    .line 618
    invoke-static {v3, v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeExecuteForLastInsertedRowId(II)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v3

    .line 621
    :try_start_3
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 624
    :try_start_4
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 630
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v5, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 618
    return-wide v3

    .line 620
    :catchall_0
    move-exception v3

    .line 621
    :try_start_5
    invoke-direct {p0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->detachCancellationSignal(Landroid/os/CancellationSignal;)V

    .line 622
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 623
    :catchall_1
    move-exception v3

    .line 624
    :try_start_6
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 625
    throw v3
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 626
    .end local v2    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v1

    .line 627
    .local v1, "ex":Ljava/lang/RuntimeException;
    :try_start_7
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v3, v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->failOperation(ILjava/lang/Exception;)V

    .line 628
    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 629
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_2
    move-exception v3

    .line 630
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v4, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 631
    throw v3
.end method

.method public executeForLong(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
    .locals 6
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 425
    if-nez p1, :cond_0

    .line 426
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "sql must not be null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 429
    :cond_0
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string v4, "executeForLong"

    invoke-virtual {v3, v4, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 431
    .local v0, "cookie":I
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 433
    .local v2, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_start_1
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->throwIfStatementForbidden(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 434
    invoke-direct {p0, v2, p2}, Lsamsung/database/sqlite/SecSQLiteConnection;->bindArguments(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;[Ljava/lang/Object;)V

    .line 436
    iget v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v4, v2, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    invoke-static {v3, v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeExecuteForLong(II)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v3

    .line 440
    :try_start_2
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 446
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v5, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 436
    return-wide v3

    .line 439
    :catchall_0
    move-exception v3

    .line 440
    :try_start_3
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 441
    throw v3
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 442
    .end local v2    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v1

    .line 443
    .local v1, "ex":Ljava/lang/RuntimeException;
    :try_start_4
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v3, v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->failOperation(ILjava/lang/Exception;)V

    .line 444
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 445
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v3

    .line 446
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v4, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 447
    throw v3
.end method

.method public executeForString(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)Ljava/lang/String;
    .locals 5
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "bindArgs"    # [Ljava/lang/Object;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 466
    if-nez p1, :cond_0

    .line 467
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "sql must not be null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 470
    :cond_0
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string v4, "executeForString"

    invoke-virtual {v3, v4, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 472
    .local v0, "cookie":I
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 474
    .local v2, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_start_1
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->throwIfStatementForbidden(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 475
    invoke-direct {p0, v2, p2}, Lsamsung/database/sqlite/SecSQLiteConnection;->bindArguments(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;[Ljava/lang/Object;)V

    .line 476
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->applyBlockGuardPolicy(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 478
    iget v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v4, v2, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    invoke-static {v3, v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeExecuteForString(II)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 482
    :try_start_2
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 488
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v4, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 478
    return-object v3

    .line 481
    :catchall_0
    move-exception v3

    .line 482
    :try_start_3
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 483
    throw v3
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 484
    .end local v2    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v1

    .line 485
    .local v1, "ex":Ljava/lang/RuntimeException;
    :try_start_4
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v3, v0, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->failOperation(ILjava/lang/Exception;)V

    .line 486
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 487
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v3

    .line 488
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v4, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 489
    throw v3
.end method

.method public exportDB(Ljava/lang/String;)V
    .locals 1
    .param p1, "attachedDB"    # Ljava/lang/String;

    .prologue
    .line 743
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeExportDB(ILjava/lang/String;)V

    .line 744
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 184
    :try_start_0
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    if-eqz v0, :cond_0

    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPool:Lsamsung/database/sqlite/SecSQLiteConnectionPool;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteConnectionPool;->onConnectionLeaked()V

    .line 188
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lsamsung/database/sqlite/SecSQLiteConnection;->dispose(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 192
    return-void

    .line 189
    :catchall_0
    move-exception v0

    .line 190
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 191
    throw v0
.end method

.method public getConnectionId()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionId:I

    return v0
.end method

.method isPreparedStatementInCache(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sql"    # Ljava/lang/String;

    .prologue
    .line 289
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mPreparedStatementCache:Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrimaryConnection()Z
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mIsPrimaryConnection:Z

    return v0
.end method

.method public onCancel()V
    .locals 1

    .prologue
    .line 822
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v0}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeCancel(I)V

    .line 823
    return-void
.end method

.method public prepare(Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteStatementInfo;)V
    .locals 8
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "outStatementInfo"    # Lsamsung/database/sqlite/SecSQLiteStatementInfo;

    .prologue
    .line 333
    if-nez p1, :cond_0

    .line 334
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "sql must not be null."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 337
    :cond_0
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    const-string/jumbo v6, "prepare"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, p1, v7}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v1

    .line 339
    .local v1, "cookie":I
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->acquirePreparedStatement(Ljava/lang/String;)Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 341
    .local v4, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    if-eqz p2, :cond_1

    .line 342
    :try_start_1
    iget v5, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mNumParameters:I

    iput v5, p2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;->numParameters:I

    .line 343
    iget-boolean v5, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mReadOnly:Z

    iput-boolean v5, p2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;->readOnly:Z

    .line 346
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v6, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    .line 345
    invoke-static {v5, v6}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeGetColumnCount(II)I

    move-result v0

    .line 347
    .local v0, "columnCount":I
    if-nez v0, :cond_2

    .line 348
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteConnection;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v5, p2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;->columnNames:[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358
    .end local v0    # "columnCount":I
    :cond_1
    :try_start_2
    invoke-direct {p0, v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 364
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v5, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 366
    return-void

    .line 350
    .restart local v0    # "columnCount":I
    :cond_2
    :try_start_3
    new-array v5, v0, [Ljava/lang/String;

    iput-object v5, p2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;->columnNames:[Ljava/lang/String;

    .line 351
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_1

    .line 352
    iget-object v5, p2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;->columnNames:[Ljava/lang/String;

    .line 353
    iget v6, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    iget v7, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    .line 352
    invoke-static {v6, v7, v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeGetColumnName(III)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 351
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 357
    .end local v0    # "columnCount":I
    .end local v3    # "i":I
    :catchall_0
    move-exception v5

    .line 358
    :try_start_4
    invoke-direct {p0, v4}, Lsamsung/database/sqlite/SecSQLiteConnection;->releasePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 359
    throw v5
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 360
    .end local v4    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :catch_0
    move-exception v2

    .line 361
    .local v2, "ex":Ljava/lang/RuntimeException;
    :try_start_5
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v5, v1, v2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->failOperation(ILjava/lang/Exception;)V

    .line 362
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 363
    .end local v2    # "ex":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v5

    .line 364
    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mRecentOperations:Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;

    invoke-virtual {v6, v1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperation(I)V

    .line 365
    throw v5
.end method

.method reconfigure(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V
    .locals 4
    .param p1, "configuration"    # Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    .prologue
    .line 264
    const/4 v3, 0x0

    iput-boolean v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mOnlyAllowReadOnlyOperations:Z

    .line 267
    iget-object v3, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 268
    .local v1, "functionCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 275
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    invoke-virtual {v3, p1}, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->updateParametersFrom(Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;)V

    .line 277
    return-void

    .line 269
    :cond_0
    iget-object v3, p1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsamsung/database/sqlite/SecSQLiteCustomFunction;

    .line 270
    .local v0, "function":Lsamsung/database/sqlite/SecSQLiteCustomFunction;
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v3, v3, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->customFunctions:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 271
    iget v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v3, v0}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeRegisterCustomFunction(ILsamsung/database/sqlite/SecSQLiteCustomFunction;)V

    .line 268
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method setOnlyAllowReadOnlyOperations(Z)V
    .locals 0
    .param p1, "readOnly"    # Z

    .prologue
    .line 283
    iput-boolean p1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mOnlyAllowReadOnlyOperations:Z

    .line 284
    return-void
.end method

.method public setPassword([B)V
    .locals 1
    .param p1, "password"    # [B

    .prologue
    .line 720
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionPtr:I

    invoke-static {v0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection;->nativeSetPassword(I[B)V

    .line 721
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1017
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SecSQLiteConnection: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConfiguration:Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;

    iget-object v1, v1, Lsamsung/database/sqlite/SecSQLiteDatabaseConfiguration;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection;->mConnectionId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
