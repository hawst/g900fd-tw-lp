.class public abstract Lsamsung/database/sqlite/SecSQLiteProgram;
.super Lsamsung/database/sqlite/SecSQLiteClosable;
.source "SecSQLiteProgram.java"


# static fields
.field private static final EMPTY_STRING_ARRAY:[Ljava/lang/String;


# instance fields
.field private final mBindArgs:[Ljava/lang/Object;

.field private final mColumnNames:[Ljava/lang/String;

.field private final mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

.field private final mNumParameters:I

.field private final mReadOnly:Z

.field private final mSql:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lsamsung/database/sqlite/SecSQLiteProgram;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    .line 30
    return-void
.end method

.method constructor <init>(Lsamsung/database/sqlite/SecSQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
    .locals 8
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "sql"    # Ljava/lang/String;
    .param p3, "bindArgs"    # [Ljava/lang/Object;
    .param p4, "cancellationSignalForPrepare"    # Landroid/os/CancellationSignal;

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 40
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteClosable;-><init>()V

    .line 42
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    .line 43
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mSql:Ljava/lang/String;

    .line 45
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mSql:Ljava/lang/String;

    invoke-static {v5}, Lsamsung/database/SecDatabaseUtils;->getSqlStatementType(Ljava/lang/String;)I

    move-result v3

    .line 46
    .local v3, "n":I
    packed-switch v3, :pswitch_data_0

    .line 56
    if-ne v3, v0, :cond_0

    .line 58
    .local v0, "assumeReadOnly":Z
    :goto_0
    :try_start_0
    new-instance v2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;

    invoke-direct {v2}, Lsamsung/database/sqlite/SecSQLiteStatementInfo;-><init>()V

    .line 59
    .local v2, "info":Lsamsung/database/sqlite/SecSQLiteStatementInfo;
    invoke-virtual {p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v5

    iget-object v6, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mSql:Ljava/lang/String;

    .line 60
    invoke-virtual {p1, v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadDefaultConnectionFlags(Z)I

    move-result v7

    .line 59
    invoke-virtual {v5, v6, v7, p4, v2}, Lsamsung/database/sqlite/SecSQLiteSession;->prepare(Ljava/lang/String;ILandroid/os/CancellationSignal;Lsamsung/database/sqlite/SecSQLiteStatementInfo;)V

    .line 62
    iget-boolean v5, v2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;->readOnly:Z

    iput-boolean v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mReadOnly:Z

    .line 63
    iget-object v5, v2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;->columnNames:[Ljava/lang/String;

    iput-object v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mColumnNames:[Ljava/lang/String;

    .line 64
    iget v5, v2, Lsamsung/database/sqlite/SecSQLiteStatementInfo;->numParameters:I

    iput v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mNumParameters:I
    :try_end_0
    .catch Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .end local v0    # "assumeReadOnly":Z
    .end local v2    # "info":Lsamsung/database/sqlite/SecSQLiteStatementInfo;
    :goto_1
    if-eqz p3, :cond_1

    array-length v5, p3

    iget v6, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mNumParameters:I

    if-le v5, v6, :cond_1

    .line 73
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Too many bind arguments.  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 74
    array-length v6, p3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " arguments were provided but the statement needs "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 75
    iget v6, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mNumParameters:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " arguments."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 73
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 50
    :pswitch_0
    iput-boolean v4, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mReadOnly:Z

    .line 51
    sget-object v5, Lsamsung/database/sqlite/SecSQLiteProgram;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mColumnNames:[Ljava/lang/String;

    .line 52
    iput v4, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mNumParameters:I

    goto :goto_1

    :cond_0
    move v0, v4

    .line 56
    goto :goto_0

    .line 65
    .restart local v0    # "assumeReadOnly":Z
    :catch_0
    move-exception v1

    .line 66
    .local v1, "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    invoke-virtual {v1}, Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;->getCorruptCode()I

    move-result v4

    invoke-virtual {p0, v4}, Lsamsung/database/sqlite/SecSQLiteProgram;->onCorruption(I)V

    .line 67
    throw v1

    .line 78
    .end local v0    # "assumeReadOnly":Z
    .end local v1    # "ex":Lsamsung/database/sqlite/SecSQLiteDatabaseCorruptException;
    :cond_1
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mNumParameters:I

    if-eqz v5, :cond_3

    .line 79
    iget v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mNumParameters:I

    new-array v5, v5, [Ljava/lang/Object;

    iput-object v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mBindArgs:[Ljava/lang/Object;

    .line 80
    if-eqz p3, :cond_2

    .line 81
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mBindArgs:[Ljava/lang/Object;

    array-length v6, p3

    invoke-static {p3, v4, v5, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    :cond_2
    :goto_2
    return-void

    .line 84
    :cond_3
    const/4 v4, 0x0

    iput-object v4, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mBindArgs:[Ljava/lang/Object;

    goto :goto_2

    .line 46
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private bind(ILjava/lang/Object;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 212
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mNumParameters:I

    if-le p1, v0, :cond_1

    .line 213
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot bind argument at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 214
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because the index is out of range.  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 215
    const-string v2, "The statement has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mNumParameters:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " parameters."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 213
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_1
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mBindArgs:[Ljava/lang/Object;

    add-int/lit8 v1, p1, -0x1

    aput-object p2, v0, v1

    .line 218
    return-void
.end method


# virtual methods
.method public bindAllArgsAsStrings([Ljava/lang/String;)V
    .locals 2
    .param p1, "bindArgs"    # [Ljava/lang/String;

    .prologue
    .line 199
    if-eqz p1, :cond_0

    .line 200
    array-length v0, p1

    .local v0, "i":I
    :goto_0
    if-nez v0, :cond_1

    .line 204
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 201
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v1, v0, -0x1

    aget-object v1, p1, v1

    invoke-virtual {p0, v0, v1}, Lsamsung/database/sqlite/SecSQLiteProgram;->bindString(ILjava/lang/String;)V

    .line 200
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public bindBlob(I[B)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "value"    # [B

    .prologue
    .line 178
    if-nez p2, :cond_0

    .line 179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "the bind value at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    invoke-direct {p0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteProgram;->bind(ILjava/lang/Object;)V

    .line 182
    return-void
.end method

.method public bindDouble(ID)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # D

    .prologue
    .line 153
    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteProgram;->bind(ILjava/lang/Object;)V

    .line 154
    return-void
.end method

.method public bindLong(IJ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 142
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteProgram;->bind(ILjava/lang/Object;)V

    .line 143
    return-void
.end method

.method public bindNull(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteProgram;->bind(ILjava/lang/Object;)V

    .line 132
    return-void
.end method

.method public bindString(ILjava/lang/String;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 164
    if-nez p2, :cond_0

    .line 165
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "the bind value at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    invoke-direct {p0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteProgram;->bind(ILjava/lang/Object;)V

    .line 168
    return-void
.end method

.method public clearBindings()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mBindArgs:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mBindArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 191
    :cond_0
    return-void
.end method

.method final getBindArgs()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mBindArgs:[Ljava/lang/Object;

    return-object v0
.end method

.method final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mColumnNames:[Ljava/lang/String;

    return-object v0
.end method

.method protected final getConnectionFlags()I
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    iget-boolean v1, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mReadOnly:Z

    invoke-virtual {v0, v1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadDefaultConnectionFlags(Z)I

    move-result v0

    return v0
.end method

.method final getDatabase()Lsamsung/database/sqlite/SecSQLiteDatabase;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    return-object v0
.end method

.method protected final getSession()Lsamsung/database/sqlite/SecSQLiteSession;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->getThreadSession()Lsamsung/database/sqlite/SecSQLiteSession;

    move-result-object v0

    return-object v0
.end method

.method final getSql()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mSql:Ljava/lang/String;

    return-object v0
.end method

.method protected onAllReferencesReleased()V
    .locals 0

    .prologue
    .line 208
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteProgram;->clearBindings()V

    .line 209
    return-void
.end method

.method protected final onCorruption()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0}, Lsamsung/database/sqlite/SecSQLiteDatabase;->onCorruption()V

    .line 117
    return-void
.end method

.method protected final onCorruption(I)V
    .locals 1
    .param p1, "err_code"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteProgram;->mDatabase:Lsamsung/database/sqlite/SecSQLiteDatabase;

    invoke-virtual {v0, p1}, Lsamsung/database/sqlite/SecSQLiteDatabase;->onCorruption(I)V

    .line 123
    return-void
.end method
