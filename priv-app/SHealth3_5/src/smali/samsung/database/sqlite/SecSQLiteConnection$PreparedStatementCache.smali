.class final Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;
.super Landroid/util/LruCache;
.source "SecSQLiteConnection.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsamsung/database/sqlite/SecSQLiteConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PreparedStatementCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lsamsung/database/sqlite/SecSQLiteConnection;


# direct methods
.method public constructor <init>(Lsamsung/database/sqlite/SecSQLiteConnection;I)V
    .locals 0
    .param p2, "size"    # I

    .prologue
    .line 1091
    iput-object p1, p0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->this$0:Lsamsung/database/sqlite/SecSQLiteConnection;

    .line 1092
    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    .line 1093
    return-void
.end method


# virtual methods
.method public dump(Landroid/util/Printer;)V
    .locals 8
    .param p1, "printer"    # Landroid/util/Printer;

    .prologue
    .line 1105
    const-string v5, "  Prepared statement cache:"

    invoke-interface {p1, v5}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 1106
    invoke-virtual {p0}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    .line 1107
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;>;"
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1108
    const/4 v2, 0x0

    .line 1109
    .local v2, "i":I
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1125
    .end local v2    # "i":I
    :goto_1
    return-void

    .line 1109
    .restart local v2    # "i":I
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1110
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .line 1111
    .local v4, "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    iget-boolean v6, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInCache:Z

    if-eqz v6, :cond_1

    .line 1112
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1113
    .local v3, "sql":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "    "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": statementPtr=0x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1114
    iget v7, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mStatementPtr:I

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1115
    const-string v7, ", numParameters="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mNumParameters:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1116
    const-string v7, ", type="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1117
    const-string v7, ", readOnly="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mReadOnly:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1118
    const-string v7, ", sql=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # invokes: Lsamsung/database/sqlite/SecSQLiteConnection;->trimSqlForDisplay(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3}, Lsamsung/database/sqlite/SecSQLiteConnection;->access$1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1113
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 1120
    .end local v3    # "sql":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1123
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;>;"
    .end local v2    # "i":I
    .end local v4    # "statement":Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    :cond_2
    const-string v5, "    <none>"

    invoke-interface {p1, v5}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p2, Ljava/lang/String;

    check-cast p3, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    check-cast p4, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    invoke-virtual {p0, p1, p2, p3, p4}, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->entryRemoved(ZLjava/lang/String;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    .locals 1
    .param p1, "evicted"    # Z
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "oldValue"    # Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;
    .param p4, "newValue"    # Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;

    .prologue
    .line 1098
    const/4 v0, 0x0

    iput-boolean v0, p3, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInCache:Z

    .line 1099
    iget-boolean v0, p3, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;->mInUse:Z

    if-nez v0, :cond_0

    .line 1100
    iget-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatementCache;->this$0:Lsamsung/database/sqlite/SecSQLiteConnection;

    # invokes: Lsamsung/database/sqlite/SecSQLiteConnection;->finalizePreparedStatement(Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V
    invoke-static {v0, p3}, Lsamsung/database/sqlite/SecSQLiteConnection;->access$0(Lsamsung/database/sqlite/SecSQLiteConnection;Lsamsung/database/sqlite/SecSQLiteConnection$PreparedStatement;)V

    .line 1102
    :cond_0
    return-void
.end method
