.class final Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;
.super Ljava/lang/Object;
.source "SecSQLiteConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsamsung/database/sqlite/SecSQLiteConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "OperationLog"
.end annotation


# static fields
.field private static final COOKIE_GENERATION_SHIFT:I = 0x8

.field private static final COOKIE_INDEX_MASK:I = 0xff

.field private static final MAX_RECENT_OPERATIONS:I = 0x14


# instance fields
.field private mGeneration:I

.field private mIndex:I

.field private final mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133
    const/16 v0, 0x14

    new-array v0, v0, [Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    iput-object v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    .line 1128
    return-void
.end method

.method synthetic constructor <init>(Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;)V
    .locals 0

    .prologue
    .line 1128
    invoke-direct {p0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;-><init>()V

    return-void
.end method

.method private endOperationDeferLogLocked(I)Z
    .locals 7
    .param p1, "cookie"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1207
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->getOperationLocked(I)Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    move-result-object v0

    .line 1208
    .local v0, "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    if-eqz v0, :cond_1

    .line 1209
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v0, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mEndTime:J

    .line 1210
    iput-boolean v1, v0, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mFinished:Z

    .line 1211
    sget-boolean v3, Lsamsung/database/sqlite/SecSQLiteDebug;->DEBUG_LOG_SLOW_QUERIES:Z

    if-eqz v3, :cond_0

    .line 1212
    iget-wide v3, v0, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mEndTime:J

    iget-wide v5, v0, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mStartTime:J

    sub-long/2addr v3, v5

    .line 1211
    invoke-static {v3, v4}, Lsamsung/database/sqlite/SecSQLiteDebug;->shouldLogSlowQuery(J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1214
    :goto_0
    return v1

    :cond_0
    move v1, v2

    .line 1211
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1214
    goto :goto_0
.end method

.method private getOperationLocked(I)Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    .locals 3
    .param p1, "cookie"    # I

    .prologue
    .line 1233
    and-int/lit16 v0, p1, 0xff

    .line 1234
    .local v0, "index":I
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    aget-object v1, v2, v0

    .line 1235
    .local v1, "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    iget v2, v1, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mCookie:I

    if-ne v2, p1, :cond_0

    .end local v1    # "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    :goto_0
    return-object v1

    .restart local v1    # "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private logOperationLocked(ILjava/lang/String;)V
    .locals 4
    .param p1, "cookie"    # I
    .param p2, "detail"    # Ljava/lang/String;

    .prologue
    .line 1218
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->getOperationLocked(I)Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    move-result-object v1

    .line 1219
    .local v1, "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1220
    .local v0, "msg":Ljava/lang/StringBuilder;
    invoke-virtual {v1, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->describe(Ljava/lang/StringBuilder;)V

    .line 1221
    if-eqz p2, :cond_0

    .line 1222
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1224
    :cond_0
    const-string v2, "SecSQLiteConnection"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1225
    return-void
.end method

.method private newOperationCookieLocked(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1228
    iget v0, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mGeneration:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mGeneration:I

    .line 1229
    .local v0, "generation":I
    shl-int/lit8 v1, v0, 0x8

    or-int/2addr v1, p1

    return v1
.end method


# virtual methods
.method public beginOperation(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    .locals 8
    .param p1, "kind"    # Ljava/lang/String;
    .param p2, "sql"    # Ljava/lang/String;
    .param p3, "bindArgs"    # [Ljava/lang/Object;

    .prologue
    .line 1138
    iget-object v5, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    monitor-enter v5

    .line 1139
    :try_start_0
    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mIndex:I

    add-int/lit8 v4, v4, 0x1

    rem-int/lit8 v2, v4, 0x14

    .line 1140
    .local v2, "index":I
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    aget-object v3, v4, v2

    .line 1141
    .local v3, "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    if-nez v3, :cond_2

    .line 1142
    new-instance v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    .end local v3    # "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;-><init>(Lsamsung/database/sqlite/SecSQLiteConnection$Operation;)V

    .line 1143
    .restart local v3    # "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    iget-object v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    aput-object v3, v4, v2

    .line 1151
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mStartTime:J

    .line 1152
    iput-object p1, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mKind:Ljava/lang/String;

    .line 1153
    iput-object p2, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mSql:Ljava/lang/String;

    .line 1154
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    iput v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mPid:I

    .line 1155
    if-eqz p3, :cond_1

    .line 1156
    iget-object v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mBindArgs:Ljava/util/ArrayList;

    if-nez v4, :cond_3

    .line 1157
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mBindArgs:Ljava/util/ArrayList;

    .line 1161
    :goto_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    array-length v4, p3

    if-lt v1, v4, :cond_4

    .line 1171
    .end local v1    # "i":I
    :cond_1
    invoke-direct {p0, v2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->newOperationCookieLocked(I)I

    move-result v4

    iput v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mCookie:I

    .line 1172
    iput v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mIndex:I

    .line 1173
    iget v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mCookie:I

    monitor-exit v5

    return v4

    .line 1145
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mFinished:Z

    .line 1146
    const/4 v4, 0x0

    iput-object v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mException:Ljava/lang/Exception;

    .line 1147
    iget-object v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mBindArgs:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1148
    iget-object v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mBindArgs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 1138
    .end local v2    # "index":I
    .end local v3    # "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 1159
    .restart local v2    # "index":I
    .restart local v3    # "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    :cond_3
    :try_start_1
    iget-object v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mBindArgs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 1162
    .restart local v1    # "i":I
    :cond_4
    aget-object v0, p3, v1

    .line 1163
    .local v0, "arg":Ljava/lang/Object;
    if-eqz v0, :cond_5

    instance-of v4, v0, [B

    if-eqz v4, :cond_5

    .line 1165
    iget-object v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mBindArgs:Ljava/util/ArrayList;

    # getter for: Lsamsung/database/sqlite/SecSQLiteConnection;->EMPTY_BYTE_ARRAY:[B
    invoke-static {}, Lsamsung/database/sqlite/SecSQLiteConnection;->access$2()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1161
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1167
    :cond_5
    iget-object v4, v3, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mBindArgs:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method public describeCurrentOperation()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1239
    iget-object v3, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    monitor-enter v3

    .line 1240
    :try_start_0
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    iget v4, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mIndex:I

    aget-object v1, v2, v4

    .line 1241
    .local v1, "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mFinished:Z

    if-nez v2, :cond_0

    .line 1242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1243
    .local v0, "msg":Ljava/lang/StringBuilder;
    invoke-virtual {v1, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->describe(Ljava/lang/StringBuilder;)V

    .line 1244
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    monitor-exit v3

    .line 1246
    .end local v0    # "msg":Ljava/lang/StringBuilder;
    :goto_0
    return-object v2

    :cond_0
    monitor-exit v3

    const/4 v2, 0x0

    goto :goto_0

    .line 1239
    .end local v1    # "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public dump(Landroid/util/Printer;)V
    .locals 0
    .param p1, "printer"    # Landroid/util/Printer;

    .prologue
    .line 1251
    return-void
.end method

.method public endOperation(I)V
    .locals 2
    .param p1, "cookie"    # I

    .prologue
    .line 1187
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    monitor-enter v1

    .line 1188
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperationDeferLogLocked(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1189
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->logOperationLocked(ILjava/lang/String;)V

    .line 1187
    :cond_0
    monitor-exit v1

    .line 1192
    return-void

    .line 1187
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public endOperationDeferLog(I)Z
    .locals 2
    .param p1, "cookie"    # I

    .prologue
    .line 1195
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    monitor-enter v1

    .line 1196
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->endOperationDeferLogLocked(I)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1195
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public failOperation(ILjava/lang/Exception;)V
    .locals 3
    .param p1, "cookie"    # I
    .param p2, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 1178
    iget-object v2, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    monitor-enter v2

    .line 1179
    :try_start_0
    invoke-direct {p0, p1}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->getOperationLocked(I)Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    move-result-object v0

    .line 1180
    .local v0, "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    if-eqz v0, :cond_0

    .line 1181
    iput-object p2, v0, Lsamsung/database/sqlite/SecSQLiteConnection$Operation;->mException:Ljava/lang/Exception;

    .line 1178
    :cond_0
    monitor-exit v2

    .line 1184
    return-void

    .line 1178
    .end local v0    # "operation":Lsamsung/database/sqlite/SecSQLiteConnection$Operation;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public logOperation(ILjava/lang/String;)V
    .locals 2
    .param p1, "cookie"    # I
    .param p2, "detail"    # Ljava/lang/String;

    .prologue
    .line 1201
    iget-object v1, p0, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->mOperations:[Lsamsung/database/sqlite/SecSQLiteConnection$Operation;

    monitor-enter v1

    .line 1202
    :try_start_0
    invoke-direct {p0, p1, p2}, Lsamsung/database/sqlite/SecSQLiteConnection$OperationLog;->logOperationLocked(ILjava/lang/String;)V

    .line 1201
    monitor-exit v1

    .line 1204
    return-void

    .line 1201
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
